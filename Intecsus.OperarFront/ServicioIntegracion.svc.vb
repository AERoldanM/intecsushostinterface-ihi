﻿Imports Intecsus.Entidades
Imports Intecsus.MSMQ
Imports Intecusus.OperarBD
Imports System.Threading
Imports Intecsus.OperarXML
Imports Intecsus.Seguridad

' NOTE: You can use the "Rename" command on the context menu to change the class name "ServicioIntegracion" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select ServicioIntegracion.svc or ServicioIntegracion.svc.vb at the Solution Explorer and start debugging.
Class ServicioIntegracion
    Implements IServicioIntegracion

    Dim validar As Validadores = New Validadores()
    Dim operacionesBD As OperacionesBase
    Dim obtenerObjeto As GeneradorXML = New GeneradorXML()

    Public Function Integrador(numeroProducto As String, numeroTarjeta As String, tipoPersona As String, tipoIdentificacion As String, numeroIdentificacion As String,
                                 primerApellido As String, segundoApellido As String, primerNombre As String, segundoNombre As String, nombreRealce As String,
                                 nombreLargo As String, fechaNacimiento As String, sexo As String, estadoCivil As String, direccionResidencia1 As String, direccionResidencia2 As String,
                                 codigoDepartResidencia As String, codigoCiudadResisdencia As String, zonaPostal As String,
                                 direccionCorresondencia1 As String, direccionCorresondencia2 As String,
                                 codigDepartDirCor As String, codigoCiudadDirCor As String, zonaPostalCorres As String,
                                 oficinaRadicacion As String,
                                 telResidencia As String, telOficina As String, afinidadTarjeta As String, cupoAsignado As String, fechaSolicitud As String, tipoSolicitud As String,
                                 manejoCuotas As String, tipoTarjeta As String, codigoVendedor As String, estadoRegistroRecibido As String, numeroSolicitud As String,
                                 tarjetaAnterior As String, codigoPuntoDistribucion As String, codigoMercadeo As String, tipoCuenta As String,
                                 valorCuotaFija As String, tipoIdAmparador As String, numeroIDAmparador As String, oficinaDistribucion As String, indicativo As String,
                                 correoElectronico As String, actividadEconomica As String, origenIngresos As String, codigoBarras As String,
                                 idenDebitoAut As String, codBancoDebito As String, numCuentaDebito As String, tipoCuentaDebito As String,
                                 indicadorCesacion As String, fechaCesacion As String, cicloFacturacion As String,
                                 BIN As String, NIT As String, subtipo As String, codigoCompensacion As String, codigoNovedad As String, codigoProceso As String,
                                filler1 As String, filler2 As String, filler3 As String, filler4 As String,
                                printName As String, idTransaccion As String, tipoProducto As String) As Respuesta Implements IServicioIntegracion.Integrador
        Dim respuesta = New Respuesta()
        Try
            operacionesBD = New OperacionesBase()
            operacionesBD.GuardarLogXNombreConectionString(19, 0, 2, "Recibe transacción: " + idTransaccion,
                                                   ConfigurationManager.AppSettings("BD"))
            Dim peticion As Peticion = New Peticion()

            peticion.numeroProducto = Me.validarDATO(numeroProducto)
            peticion.numeroTarjeta = Me.validarDATO(numeroTarjeta)
            peticion.tipoPersona = Me.validarDATO(tipoPersona)
            peticion.tipoPersona = Me.validarDATO(tipoPersona)
            peticion.primerApellido = Me.validarDATO(primerApellido)
            peticion.segundoApellido = Me.validarDATO(segundoApellido)
            peticion.primerNombre = Me.validarDATO(primerNombre)
            peticion.segundoNombre = Me.validarDATO(segundoNombre)
            peticion.nombreRealce = Me.validarDATO(nombreRealce)
            peticion.nombreLargo = Me.validarDATO(nombreLargo)
            peticion.fechaNacimiento = Me.validarDATO(fechaNacimiento)
            peticion.sexo = Me.validarDATO(sexo)
            peticion.direccionResidencia1 = Me.validarDATO(direccionResidencia1)
            peticion.direccionResidencia2 = Me.validarDATO(direccionResidencia2)
            peticion.telResidencia = Me.validarDATO(telResidencia)
            peticion.telOficina = Me.validarDATO(telOficina)
            peticion.fechaSolicitud = Me.validarDATO(fechaSolicitud)
            peticion.codigoVendedor = Me.validarDATO(codigoVendedor)
            peticion.tarjetaAnterior = Me.validarDATO(tarjetaAnterior)
            peticion.codigoPuntoDistribucion = Me.validarDATO(codigoPuntoDistribucion)
            peticion.codigoMercadeo = Me.validarDATO(codigoMercadeo)
            peticion.correoElectronico = Me.validarDATO(correoElectronico)
            peticion.codigoBarras = Me.validarDATO(codigoBarras)
            peticion.idenDebitoAut = Me.validarDATO(idenDebitoAut)
            peticion.numCuentaDebito = Me.validarDATO(numCuentaDebito)
            peticion.tipoCuentaDebito = Me.validarDATO(tipoCuentaDebito)
            peticion.indicadorCesacion = Me.validarDATO(indicadorCesacion)
            peticion.subtipo = Me.validarDATO(subtipo)
            peticion.codigoNovedad = Me.validarDATO(codigoNovedad)
            peticion.codigoProceso = Me.validarDATO(codigoProceso)
            peticion.tipoIdentificacion = Me.validarDATO(tipoIdentificacion)
            peticion.numeroIdentificacion = Me.validarDATO(numeroIdentificacion)
            peticion.estadoCivil = Me.validarDATO(estadoCivil)
            peticion.codigoDepartResidencia = Me.validarDATO(codigoDepartResidencia)
            peticion.codigoCiudadResisdencia = Me.validarDATO(codigoCiudadResisdencia)
            peticion.zonaPostal = Me.validarDATO(zonaPostal)
            peticion.codigoDepartamentoDirCorrespondencia = Me.validarDATO(codigDepartDirCor)
            peticion.codigoCiudadDirCorrespondencia = Me.validarDATO(codigoCiudadDirCor)
            peticion.zonaPostalDirCorres = Me.validarDATO(zonaPostalCorres)
            peticion.oficinaRadicacion = Me.validarDATO(oficinaRadicacion)
            peticion.afinidadTarjeta = Me.validarDATO(afinidadTarjeta)
            peticion.cupoAsignado = Me.validarDATO(cupoAsignado)
            peticion.tipoSolicitud = Me.validarDATO(tipoSolicitud)
            peticion.manejoCuotas = Me.validarDATO(manejoCuotas)
            peticion.tipoTarjeta = Me.validarDATO(tipoTarjeta)
            peticion.numeroSolicitud = Me.validarDATO(numeroSolicitud)
            peticion.tipoCuenta = Me.validarDATO(tipoCuenta) '00
            peticion.valorCuotaFija = Me.validarDATO(valorCuotaFija)
            peticion.tipoIdAmparador = Me.validarDATO(tipoIdAmparador)
            peticion.numeroIDAmparador = Me.validarDATO(numeroIDAmparador) 'en ceros
            peticion.oficinaDistribucion = Me.validarDATO(oficinaDistribucion)
            peticion.indicativo = Me.validarDATO(indicativo)
            peticion.actividadEconomica = Me.validarDATO(actividadEconomica)
            peticion.origenIngresos = Me.validarDATO(origenIngresos)
            peticion.codBancoDebito = Me.validarDATO(codBancoDebito)
            peticion.fechaCesacion = Me.validarDATO(fechaCesacion)
            peticion.cicloFacturacion = Me.validarDATO(cicloFacturacion)
            peticion.BIN = Me.validarDATO(BIN)
            peticion.NIT = Me.validarDATO(NIT)
            peticion.codigoCompensacion = Me.validarDATO(codigoCompensacion)
            peticion.direccionCorrespondenciaLinea1 = Me.validarDATO(direccionCorresondencia1)
            peticion.direccionCorrespondenciaLinea2 = Me.validarDATO(direccionCorresondencia2)
            peticion.estadoRegistroRecibido = Me.validarDATO(estadoRegistroRecibido)
            peticion.codigoProceso = Me.validarDATO(codigoProceso)
            peticion.filler1 = Me.validarDATO(filler1)
            peticion.filler2 = Me.validarDATO(filler2)
            peticion.filler3 = Me.validarDATO(filler3)
            peticion.filler4 = Me.validarDATO(filler4)

            peticion.printName = Me.validarDATO(printName)
            peticion.idTransaccion = Me.validarDATO(idTransaccion)
            peticion.tipoProducto = Me.validarDATO(tipoProducto)
            peticion.cliente = "COLSUBSIDIO" 'If(String.IsNullOrEmpty(cliente), "UNICO", cliente.ToUpper)  '"Unico"
            peticion.idTransaccion = Me.ValidarTransaccion(peticion)
            Me.ValidarCliente(peticion)
            respuesta = Me.AdjuntarACola(peticion)

            Return respuesta
        Catch ex As Exception

            respuesta = Me.RespuestaErronea("02", ex.Message)
        End Try

        Return respuesta
    End Function

    ''' <summary>
    ''' Metood encargado de adjuntar una peticion a la cola
    ''' </summary>
    ''' <param name="peticion">Objeto de peticion que se agrega a la cola</param>
    ''' <remarks>AERM 19/03/2015</remarks>
    Private Function AdjuntarACola(peticion As Peticion) As Respuesta
        Try
            Dim m_OperarCOla As ICola = New InstanciaCola().IntanciarCola(ConfigurationManager.AppSettings("ColaInstancia"))
            m_OperarCOla.GuardarObjetoPeticionCliente(peticion, ConfigurationManager.AppSettings("colaCliente"))
            Return Me.ConvertTORespuesta(Me.EsperarRespuesta(peticion))
        Catch ex As Exception
            Throw New Exception("ServicioIntegracion- AdjuntarACola : " + peticion.idTransaccion + "  " + ex.Message)
        End Try
    End Function

    Private Function ConvertTORespuesta(respuesta As RespuestaTuya) As Respuesta
        Dim m_resp As Respuesta = New Respuesta()
        m_resp.codigo = respuesta.codigo
        m_resp.mensaje = respuesta.mensaje
        m_resp.numeroAutorizacion = respuesta.numeroAutorizacion
        Return m_resp
    End Function


    Private Function ValidarDatoInteger(dato As String, nombre As String) As String
        Try
            If (String.IsNullOrEmpty(dato)) Then
                Return String.Empty
            End If

            Dim valdiacion As Boolean = validar.ValidadorExpresionesRegulares(dato, "^[0-9]*$")
            If valdiacion Then
                Return dato
            End If

            Throw New Exception
        Catch ex As Exception
            Throw New Exception("Error a convertir el atributo " + nombre)
        End Try

        Return String.Empty
    End Function

    Private Function validarDATO(dato As String) As String
        If (String.IsNullOrEmpty(dato)) Then
            Return String.Empty
        End If

        Return dato
    End Function

    Private Function EsperarRespuesta(peticion As Peticion) As RespuestaTuya
        Dim m_Respuesta As RespuestaTuya = New RespuestaTuya()
        Try
            Dim m_Peticion As Boolean = True
            Dim m_TiempoMAX As Int32 = Convert.ToInt32(operacionesBD.ObtenerValoresConfigurador("TmeOutServicioMiliSegundos", ConfigurationManager.AppSettings("BD")))
            Dim m_TimpoEspera As Int32 = 0
            Dim m_TiempoRespuesta As Int32 = Convert.ToInt32(operacionesBD.ObtenerValoresConfigurador("BuscarRespuestaMiliSegundos", ConfigurationManager.AppSettings("BD")))
            While (m_Peticion)
                Thread.Sleep(m_TiempoRespuesta)
                m_TimpoEspera += m_TiempoRespuesta
                Dim m_RespuestaWS As RespuestaWSASNET = operacionesBD.ObtenerRespuesta(peticion.cliente, peticion.idTransaccion, ConfigurationManager.AppSettings("BD"))
                If (IsNothing(m_RespuestaWS) = False) Then
                    operacionesBD.GuardarLogXNombreConectionString(19, 0, 1, "Respuesta -" + peticion.idTransaccion + "  " + m_RespuestaWS.Respuestas,
                                                              ConfigurationManager.AppSettings("BD"), peticion.cliente)
                    m_Respuesta = obtenerObjeto.Deserialize(Of RespuestaTuya)(m_RespuestaWS.Respuestas)
                    m_Peticion = False
                End If
                If (m_TimpoEspera >= m_TiempoMAX And m_Peticion) Then
                    m_Respuesta.numeroAutorizacion = "ERROR"
                    Dim m_peticionR As String = obtenerObjeto.Serializar(Of Peticion)(peticion)
                    operacionesBD.GuardarRespuestaWS(peticion, m_peticionR, "Time Out -" + peticion.idTransaccion, m_Respuesta, ConfigurationManager.AppSettings("BD"))
                    Throw New Exception("Supero el tiempo de espera")
                End If
            End While

        Catch ex As Exception
            m_Respuesta.codigo = "01"
            m_Respuesta.mensaje = ex.Message
            operacionesBD.GuardarLogXNombreConectionString(19, 0, 2, "ServicioIntegracion- EsperarRespuesta : - " + peticion.idTransaccion + "  " + ex.Message,
                                                       ConfigurationManager.AppSettings("BD"))
        End Try

        Return m_Respuesta
    End Function


    ''' <summary>
    ''' Metodo encargado de cargar los datos de inicializacion de PCNAME, CardName y XML
    ''' </summary>
    ''' <param name="peticion">Peticion inicial enviada por el cliente</param>
    ''' <returns>Vlaida si existe pcname o producto</returns>
    ''' <remarks>AERM 29/07/2015</remarks>
    Public Function ValidarCliente(peticion As Peticion) As Boolean
        Try
            If (peticion.codigoNovedad <> "03") Then
                Return True
            End If

            Dim m_respuestaWS As RespuestaWS = New RespuestaWS()
            Dim m_List As List(Of String) = operacionesBD.ObtenerCardFormatName(peticion.cliente, peticion.tipoProducto, ConfigurationManager.AppSettings("BD"))
            If (m_List.Count >= 2) Then
                m_respuestaWS.cardWizardName = m_List(0)
                m_respuestaWS.xmlPeticion = m_List(1)
            End If

            Dim m_BuscarImpresora As String = operacionesBD.ObtenerValoresConfigurador("BuscarImpresora" + peticion.cliente, ConfigurationManager.AppSettings("BD"))
            ''Obtenemos el PCNAME deacuerdo a configuracion base de datos tabla Impresoras si lo tiene habilitado
            If (Convert.ToBoolean(Convert.ToInt32(m_BuscarImpresora)) = True) Then
                m_respuestaWS.pCName = operacionesBD.ObtenerPCNAME(peticion.printName, ConfigurationManager.AppSettings("BD"))
            Else
                m_respuestaWS.pCName = peticion.printName
            End If

            If (String.IsNullOrEmpty(m_respuestaWS.cardWizardName) Or String.IsNullOrEmpty(m_respuestaWS.pCName)) Then
                Throw New Exception("No se encuentra PCName y/ó CardFormat para la transacción ")
            End If

            Return True
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(19, 0, 2, "ServicioIntegracion: " + peticion.idTransaccion + "  " + ex.Message,
                                                     ConfigurationManager.AppSettings("BD"))
            Throw New Exception("Error PCName y/ó CardFormat para la transacción ")
        End Try

    End Function

    ''' <summary>
    ''' Metodo encargado de retornar un error en la respuesta
    ''' </summary>
    ''' <param name="mensaje">Mensaje a enviar</param>
    ''' <returns>Respuesta al cliente</returns>
    ''' <remarks>AERM 11/09/2015</remarks>
    Private Function RespuestaErronea(codigoError As String, mensaje As String) As Respuesta
        Dim Respuesta = New Respuesta()
        Respuesta.codigo = codigoError
        Respuesta.mensaje = "Solicitud Errada-  " + mensaje
        Respuesta.numeroAutorizacion = String.Empty
        Return Respuesta
    End Function

    Private Function ValidarTransaccion(peticion As Peticion) As String
        Dim m_StringObjeto As String = obtenerObjeto.Serializar(Of Peticion)(peticion)
        Try
            Dim m_Secuencia As Int32 = operacionesBD.ObtenerSecuencia(ConfigurationManager.AppSettings("BD"))
            If (String.IsNullOrEmpty(peticion.idTransaccion)) Then
                If (peticion.codigoNovedad <> "03") Then
                    peticion.idTransaccion = String.Empty
                Else
                    Throw New Exception("No existe ID transacción")
                End If
            End If
            peticion.idTransaccion = m_Secuencia.ToString() + "-" + peticion.idTransaccion
            m_StringObjeto = obtenerObjeto.Serializar(Of Peticion)(peticion)
            operacionesBD.GuardarLogXNombreConectionString(19, 0, 1, "Peticion -" + peticion.idTransaccion + "  " + m_StringObjeto,
                                                      ConfigurationManager.AppSettings("BD"), peticion.cliente)
            Return peticion.idTransaccion
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(19, 0, 1, "Peticion -" + peticion.idTransaccion + "  " + m_StringObjeto,
                                                      ConfigurationManager.AppSettings("BD"), peticion.cliente)
            operacionesBD.GuardarLogXNombreConectionString(19, 0, 2, "ServicioIntegracion- IDTransacción : " + ex.Message,
                                                  ConfigurationManager.AppSettings("BD"))
            Throw ex
        End Try
    End Function

End Class
