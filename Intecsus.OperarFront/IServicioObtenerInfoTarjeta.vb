﻿Imports System.ServiceModel

' NOTE: You can use the "Rename" command on the context menu to change the interface name "IServicioObtenerInfoTarjeta" in both code and config file together.
<ServiceContract()>
Public Interface IServicioObtenerInfoTarjeta

    <OperationContract()>
    Function ObtenerInformacionChip(datos As InformacionTarjeta) As RespuestaInfo

End Interface
