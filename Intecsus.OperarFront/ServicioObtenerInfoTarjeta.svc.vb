﻿Imports Intecusus.OperarBD
Imports Intecsus.Entidades
Imports Intecsus.MSMQ

' NOTE: You can use the "Rename" command on the context menu to change the class name "ServicioObtenerInfoTarjeta" in code, svc and config file together.
' NOTE: In order to launch WCF Test Client for testing this service, please select ServicioObtenerInfoTarjeta.svc or ServicioObtenerInfoTarjeta.svc.vb at the Solution Explorer and start debugging.
Public Class ServicioObtenerInfoTarjeta
    Implements IServicioObtenerInfoTarjeta

#Region "variables"

    Private operarCola As ICola = New InstanciaCola().IntanciarCola(ConfigurationManager.AppSettings("ColaInstancia"))
#End Region


    Public Function ObtenerInformacionChip(datos As InformacionTarjeta) As RespuestaInfo Implements IServicioObtenerInfoTarjeta.ObtenerInformacionChip
        Dim m_Respuesta As RespuestaInfo = New RespuestaInfo()
        Dim operacionesBD As OperacionesBase = New OperacionesBase()
        Try
            Dim m_notificar As EstadoTransaccion = operacionesBD.ObtenerNoticiXSha(datos.Identificador, datos.IdChipContacto, "COLSUBSIDIO", ConfigurationManager.AppSettings("BD"))
            Me.ValidarNotificacion(m_notificar, datos.Identificador)
            Dim m_notificacion As RespuestaDeviceStatus = New RespuestaDeviceStatus()
            m_notificacion.IdChip = datos.IdChipContacto
            m_notificacion.hostIdentifier = m_notificar.IDTransaccion
            m_notificacion.DataInteger = "000"
            m_notificacion.EstadoF = "0"
            m_notificacion.Mensaje = "Completado sin errores"
            m_notificacion.TransaccionCliente = m_notificar.IdTransaccionCl
            m_notificacion.Cliente = m_notificar.Cliente
            Dim colaNotificacion As String = operacionesBD.ObtenerValoresConfigurador("ColaNotificacion", ConfigurationManager.AppSettings("BD"))
            operarCola.GuardarObjetoNotificacion(m_notificacion, colaNotificacion)
            operacionesBD.GuardarLogXNombreConectionString(22, m_notificar.IDTransaccion, 1, "ServicioObtenerInfoTarjeta- ObtenerInformacionChip : Se almacena notificacion" + datos.IdChipContacto,
                                                     ConfigurationManager.AppSettings("BD"))
            m_Respuesta.Codigo = "00"
            m_Respuesta.Mensaje = "Transacción reportada correctamente"
        Catch ex As Exception
            m_Respuesta.Codigo = "02"
            m_Respuesta.Mensaje = ex.Message
            operacionesBD.GuardarLogXNombreConectionString(22, 0, 2, "ServicioObtenerInfoTarjeta- ObtenerInformacionChip : " + ex.Message,
                                                     ConfigurationManager.AppSettings("BD"))
        End Try

        Return m_Respuesta
    End Function

    Private Sub ValidarNotificacion(m_notificar As EstadoTransaccion, identificador As String)
        If (m_notificar Is Nothing) Then
            Throw New Exception("No existe transacción reportada para " + identificador)
        End If

        If (m_notificar.Final) Then
            Throw New Exception("Notificacion ya fue previamente realizada " + identificador)
        End If

    End Sub


End Class
