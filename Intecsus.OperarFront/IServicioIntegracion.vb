﻿Imports System.ServiceModel
Imports Intecsus.Entidades

' NOTE: You can use the "Rename" command on the context menu to change the interface name "IServicioIntegracion" in both code and config file together.
<ServiceContract()>
Public Interface IServicioIntegracion


    <OperationContract>
    Function Integrador(numeroProducto As String, numeroTarjeta As String, tipoPersona As String, tipoIdentificacion As String, numeroIdentificacion As String,
                                 primerApellido As String, segundoApellido As String, primerNombre As String, segundoNombre As String, nombreRealce As String,
                                 nombreLargo As String, fechaNacimiento As String, sexo As String, estadoCivil As String, direccionResidencia1 As String, direccionResidencia2 As String,
                                 codigoDepartResidencia As String, codigoCiudadResisdencia As String, zonaPostal As String,
                                 direccionCorresondencia1 As String, direccionCorresondencia2 As String,
                                 codigDepartDirCor As String, codigoCiudadDirCor As String, zonaPostalCorres As String,
                                 oficinaRadicacion As String,
                                 telResidencia As String, telOficina As String, afinidadTarjeta As String, cupoAsignado As String, fechaSolicitud As String, tipoSolicitud As String,
                                 manejoCuotas As String, tipoTarjeta As String, codigoVendedor As String, estadoRegistroRecibido As String, numeroSolicitud As String,
                                 tarjetaAnterior As String, codigoPuntoDistribucion As String, codigoMercadeo As String, tipoCuenta As String,
                                 valorCuotaFija As String, tipoIdAmparador As String, numeroIDAmparador As String, oficinaDistribucion As String, indicativo As String,
                                 correoElectronico As String, actividadEconomica As String, origenIngresos As String, codigoBarras As String,
                                 idenDebitoAut As String, codBancoDebito As String, numCuentaDebito As String, tipoCuentaDebito As String,
                                 indicadorCesacion As String, fechaCesacion As String, cicloFacturacion As String,
                                 BIN As String, NIT As String, subtipo As String, codigoCompensacion As String, codigoNovedad As String, codigoProceso As String,
                                filler1 As String, filler2 As String, filler3 As String, filler4 As String,
                                printName As String, idTransaccion As String, tipoProducto As String) As Respuesta

End Interface
