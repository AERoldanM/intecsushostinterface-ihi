﻿Imports System.ServiceModel
Imports System.ServiceModel.Channels
Imports System.ServiceModel.Security

Public Class CustomBindingI
    Implements IBinging

    Public Function CreateBinding(url As String, maxTamañoPaquete As Integer, timeOut As Integer) As Binding Implements IBinging.CreateBinding
        Dim m_binding As CustomBinding = New CustomBinding()

        Dim m_security = TransportSecurityBindingElement.CreateUserNameOverTransportBindingElement()
        m_security.AllowInsecureTransport = True
        m_security.IncludeTimestamp = False
        m_security.DefaultAlgorithmSuite = SecurityAlgorithmSuite.Basic256
        m_security.MessageSecurityVersion = MessageSecurityVersion.WSSecurity10WSTrustFebruary2005WSSecureConversationFebruary2005WSSecurityPolicy11BasicSecurityProfile10

        Dim m_encoding = New TextMessageEncodingBindingElement()
        m_encoding.MessageVersion = MessageVersion.Soap11
        Dim m_transport

        If (Me.IsHttps(url.Split(":")(0))) Then
            m_transport = New HttpsTransportBindingElement()
        Else
            m_transport = New HttpTransportBindingElement()
        End If

        m_transport.MaxReceivedMessageSize = maxTamañoPaquete '20000000 es 20 megs
        m_transport.RequestInitializationTimeout = New TimeSpan(0, 0, timeOut) ' Horas, minutos, segundos
        m_binding.OpenTimeout = New TimeSpan(0, 0, timeOut)
        m_binding.CloseTimeout = New TimeSpan(0, 0, timeOut)
        m_binding.SendTimeout = New TimeSpan(0, 0, timeOut)
        m_binding.ReceiveTimeout = New TimeSpan(0, 0, timeOut)
        m_binding.Elements.Add(m_security)
        m_binding.Elements.Add(m_encoding)
        m_binding.Elements.Add(m_transport)

        Return m_binding
    End Function

    ''' <summary>
    ''' Retorna si es HTTPS o HTTP
    ''' </summary>
    ''' <param name="url">Url a retornar</param>
    ''' <returns>Retroa true si es HTTPS</returns>
    ''' <remarks>AERM 08/09/2015</remarks>
    Private Function IsHttps(url As String) As Boolean
        If (url.ToUpper() = "HTTPS") Then
            Return True
        End If

        Return False
    End Function
End Class
