﻿Imports System.ServiceModel.Channels

Public Interface IBinging
    ''' <summary>
    ''' Crea un Binding CustomBinding
    ''' </summary>
    ''' <param name="url">Url donde se aloja el WS</param>
    ''' <param name="maxTamañoPaquete"> Maximo tamaño de nevio y recibido </param>
    ''' <param name="timeOut">Tiempod e expera en segundos</param>
    ''' <returns>Retorna el binding con sus propiedad para poder crearle una cabecera</returns>
    ''' <remarks>AERM 08/09/2015</remarks>
    Function CreateBinding(url As String, maxTamañoPaquete As Integer, timeOut As Integer) As Binding
End Interface
