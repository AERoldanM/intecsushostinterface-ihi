﻿''' <summary>
''' Clase encargada de instanciar un binding
''' </summary>
''' <remarks>AERM 19/03/2015</remarks>
Public Class FactoryBinding
    Public Function RetornarBindig(ByVal cliente As String) As IBinging
        Try
            Select Case cliente.ToUpper
                Case "CUSTOM"
                    Return New CustomBindingI()
                Case Else
                    Throw New Exception("No está disponible el bindig solicitado. (FactoryBinding - RetornarBindig)")
            End Select
        Catch ex As Exception
            Throw New Exception("RetornarCliente " + ex.Message)
        End Try
    End Function
End Class
