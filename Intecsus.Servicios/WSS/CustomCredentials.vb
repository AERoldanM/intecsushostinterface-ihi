﻿Imports System.ServiceModel.Description
Imports System.ServiceModel.Security
Imports System.IdentityModel.Tokens
Imports System.ServiceModel
Imports System.Security.Cryptography
Imports System.Text

''' <summary>
''' Clases encargadas de sobre escribir las credenciales de un servicio Consumido 
''' Se encarga de agregar el encabezado soap requerido
''' </summary>
''' <remarks>AERM 08/09/2015</remarks>
Public Class CustomCredentials
    Inherits ClientCredentials

    Public Sub New()
    End Sub

    Protected Sub New(cc As CustomCredentials)
        MyBase.New(cc)
    End Sub

    Public Overrides Function CreateSecurityTokenManager() As System.IdentityModel.Selectors.SecurityTokenManager
        Return New CustomSecurityTokenManager(Me)
    End Function

    Protected Overrides Function CloneCore() As ClientCredentials
        Return New CustomCredentials(Me)
    End Function

End Class

Friend Class CustomSecurityTokenManager
    Inherits ClientCredentialsSecurityTokenManager

    Public Sub New(cred As CustomCredentials)

        MyBase.New(cred)
    End Sub

    Public Overrides Function CreateSecurityTokenSerializer(version As System.IdentityModel.Selectors.SecurityTokenVersion) As System.IdentityModel.Selectors.SecurityTokenSerializer
        Return New CustomTokenSerializer(System.ServiceModel.Security.SecurityVersion.WSSecurity11)
    End Function
End Class

Friend Class CustomTokenSerializer
    Inherits WSSecurityTokenSerializer

    Public Sub New(sv As SecurityVersion)
        MyBase.New(sv)
    End Sub

    Protected Overrides Sub WriteTokenCore(writer As System.Xml.XmlWriter, token As System.IdentityModel.Tokens.SecurityToken)
        Dim userToken As UserNameSecurityToken = TryCast(token, UserNameSecurityToken)

        Dim tokennamespace As String = "o"

        Dim created As DateTime = DateTime.Now
        Dim createdStr As String = created.ToString("yyyy-MM-ddThh:mm:ss.fffZ")

        ' unique Nonce value - encode with SHA-1 for 'randomness'
        ' in theory the nonce could just be the GUID by itself
        Dim phrase As String = Guid.NewGuid().ToString()
        Dim nonce = GetSHA1String(phrase)

        ' in this case password is plain text
        ' for digest mode password needs to be encoded as:
        ' PasswordAsDigest = Base64(SHA-1(Nonce + Created + Password))
        ' and profile needs to change to
        'string password = GetSHA1String(nonce + createdStr + userToken.Password);
        Dim password As String = userToken.Password

        writer.WriteRaw(String.Format((Convert.ToString((Convert.ToString((Convert.ToString("<{0}:UsernameToken u:Id=""" + token.Id + """ xmlns:u=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"">" + "<{0}:Username>" + userToken.UserName + "</{0}:Username>" + "<{0}:Password Type=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText"">") & password) + "</{0}:Password>" + "<{0}:Nonce EncodingType=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary"">") & nonce) + "</{0}:Nonce>" + "<u:Created>") & createdStr) + "</u:Created></{0}:UsernameToken>", tokennamespace))
    End Sub

    Protected Function GetSHA1String(phrase As String) As String
        Dim sha1Hasher As New SHA1CryptoServiceProvider()
        Dim hashedDataBytes As Byte() = sha1Hasher.ComputeHash(Encoding.UTF8.GetBytes(phrase))
        Return Convert.ToBase64String(hashedDataBytes)
    End Function
End Class
