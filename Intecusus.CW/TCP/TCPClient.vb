﻿Imports System.Net
Imports System.Net.Sockets
Imports System.Text
Imports System.Threading
Imports CardWizard.Security.Cryptography

Namespace TCP
    ''' <summary>
    ''' TCPclient
    ''' </summary>
    ''' <remarks></remarks>
    Public Class TCPClient
        Inherits TCPBase

        ' need this to support "Dispose" method
        'Implements IDisposable

        Friend Structure TCP_REQUEST_RESPONSE
            Dim Status As TCP_RESPONSE_STATUS
            Dim Response As String
            Dim otherErrorMessage As String
        End Structure

        Friend Enum TCP_RESPONSE_STATUS
            noError
            timedOut
            otherError
        End Enum

        ' ====================
        '   PUBLIC EVENTS
        ' ====================
        ' raised when a server request has been received from a TCP client
        ' where "sClientID" is the client's IP address and port number identifier e.g. 200.300.0.5:7654
        ' "sXMLrequest" is the clear text XML request
        ' the event will return "sXMLresponse" which gets encrypted and returned to the client
        Public Event MessageFromServer(ByVal sMessage As String)

        ' raised when we lost the server's TCP connection
        Public Event ClientDisconnected()

        ' raised when an error occurs within this class
        Public Event ErrorEncountered(ByVal sErrorMessage As String)

        ' 04/21/08 Public Shared socket As System.Net.Sockets.Socket   ' 12/04/06
        ' 04/21/08 Public Shared connection As AsyncConnectionInfo     ' 12/04/06

        Private socket As System.Net.Sockets.Socket     ' 04/21/08
        Friend connection As AsyncConnectionInfo        ' 04/21/08

        ' if TRUE, indicates this object is being disposed  ' 12/18/06
        ' 08/03/07 Private mObjectDisposed As Boolean = False          ' 12/18/06

        ' server and port number
        Private sServerName As String
        Private sServerIP As String '03/1109
        Private iPortNumber As Integer

        ' 04/21/08 start changes
        ' Friend Shared bMessageReceived As Boolean     ' TRUE = we've received a message to be processed
        ' Friend Shared sExpectedMessageIdentifier As String    ' expected identifier for message sent/rcvd to CW enterprise server
        ' Friend Shared sMessageReceived As String      ' the message received

        Private bMessageReceived As Boolean     ' TRUE = we've received a message to be processed
        Friend sExpectedMessageIdentifier As String    ' expected identifier for message sent/rcvd to CW enterprise server
        Private sMessageReceived As String      ' the message received
        ' 04/21/08 end changes

        Private mConnected As Boolean           ' TRUE = indicates we can connect to web server
        'Private mDefaultEncryption As ENCRYPTION_OPTION = ENCRYPTION_OPTION.None ' default encryption option (none, session)

        Private mEncryptionActivated As Boolean ' TRUE=triple DES encryption enabled for STD messages
        Private mSessionKeys As String          ' if encryption actived, contains the session keys
        Private mServerPublicKey As String      ' server's public encryption RSA key
        'Private mMessageIdentifier As Integer = 0
        Private mWaitForReplyLimit As Integer = 60  ' default is 60 seconds

        ' 04/21/08 start changes
#If True = True Then
        ' single instance that is used for processing Excrypt requests serially
        Private WithEvents oTCPrequestProcessor As TCPrequestProcessor

        ' the thread that does the work
        Private oTCPrequestProcessorThread As Thread

        ' shared AutoResetEvent object
        Friend are_ProcessRequest As New Threading.AutoResetEvent(False)

        ' used for indicating completion of a receive wait (timeout or data received)
        Friend are_ResponseReceivedorTimeout As AutoResetEvent

        ' shared AutoResetEvent objects (allows up to 10 threads)
        Friend are_ProcessRequestCompleted(10) As Threading.AutoResetEvent

        ' response for the last message received from the TCP server
        ' indexed by the instance number for the "clsTCPclient" connection
        ' normally this would be "1", but could also be "2"
        ' for additional instances. Allow 10 for room for growth however.
        ' this means up to 10 instances of "clsTCPclient" can exist. 
        Friend ProcessRequest_Response(10) As TCP_REQUEST_RESPONSE

        ' information regarding the request currently being process
        ' will eventually be copied into ProcessRequest_Response()
        Friend CurrentRequestResponse As TCP_REQUEST_RESPONSE

        ' message from TCP server we just received
        'Friend Shared sMessageReceived As String

        ' TRUE indicates message was received and placed into "sMessageReceived"
        'Friend Shared bMessageReceived As Boolean

        ' TRUE indicates we timed out waiting for a response to a message sent to TCP server
        Private bTimedOut As Boolean

        ' 12/04/06 end of changes
#Else

        ' 12/04/06  start of changes
        ' single instance that is used for processing Excrypt requests serially
        Public Shared WithEvents oTCPrequestProcessor As TCPrequestProcessor

        ' the thread that does the work
        Friend Shared oTCPrequestProcessorThread As Thread

        ' shared AutoResetEvent object
        Friend Shared are_ProcessRequest As New Threading.AutoResetEvent(False)

        ' used for indicating completion of a receive wait (timeout or data received)
        Friend Shared are_ResponseReceivedorTimeout As AutoResetEvent

        ' shared AutoResetEvent objects (allows up to 10 threads)
        Friend Shared are_ProcessRequestCompleted(10) As Threading.AutoResetEvent

        ' response for the last message received from the TCP server
        ' indexed by the instance number for the "clsTCPclient" connection
        ' normally this would be "1", but could also be "2"
        ' for additional instances. Allow 10 for room for growth however.
        ' this means up to 10 instances of "clsTCPclient" can exist. 
        Friend Shared ProcessRequest_Response(10) As TCP_REQUEST_RESPONSE

        ' information regarding the request currently being process
        ' will eventually be copied into ProcessRequest_Response()
        Friend Shared CurrentRequestResponse As TCP_REQUEST_RESPONSE

        ' message from TCP server we just received
        'Friend Shared sMessageReceived As String

        ' TRUE indicates message was received and placed into "sMessageReceived"
        'Friend Shared bMessageReceived As Boolean

        ' TRUE indicates we timed out waiting for a response to a message sent to TCP server
        Friend Shared bTimedOut As Boolean

        ' 12/04/06 end of changes
#End If
        ' 04/21/08 end changes

        Public Property ServerName() As String
            Get
                Return sServerName
            End Get
            Set(ByVal value As String)
                sServerName = value
            End Set
        End Property

        Public Property ServerIP() As String  '03/11/09
            Get
                Return sServerIP
            End Get
            Set(ByVal value As String)
                sServerIP = value
            End Set
        End Property

        Public Property PortNumber() As Integer
            Get
                Return iPortNumber
            End Get
            Set(ByVal value As Integer)
                iPortNumber = value
            End Set
        End Property

        Public Overrides Sub Dispose()
            ' terminate TCP socket connection if necessary
            Try
                MyBase.Dispose()

                ' indicate we are being disposed. thus when we loose the TCP
                ' connection with server, we will not think it was an error
                mObjectDisposed = True      ' 12/18/06

                ' 02/27/07
                ' 04/21/08 oTCPrequestProcessor.oTCPclient = Nothing
                oTCPrequestProcessor.StopProcessor()
                If Not socket Is Nothing Then
                    socket.Shutdown(SocketShutdown.Both)    ' 02/27/07
                    socket.Disconnect(True)
                    socket = Nothing
                End If

                ' 02/21/07 start of changes
                connection.socket = Nothing
                connection = Nothing
                ' 02/21/07 end of changes

            Catch ex As Exception

            End Try
        End Sub

        Public ReadOnly Property Connected() As Boolean
            Get
                Return mConnected
            End Get
        End Property

        Public Property WaitForReplyLimit() As Integer
            ' number of seconds maximum to wait for reply in SendMessageGetResponse method
            Get
                Return mWaitForReplyLimit
            End Get
            Set(ByVal value As Integer)
                mWaitForReplyLimit = value
            End Set
        End Property

        ''' <summary>
        ''' attempts to active encryption using session keys with the server
        ''' </summary>
        ''' <returns>TRUE if successful</returns>
        ''' <remarks></remarks>
        Public Function ActivateEncryption() As Boolean
            Dim response As String = String.Empty
            Dim errorMessage As String = String.Empty
            Dim cipherText As String = String.Empty

            Try
                ' 1. get the public key from the server
                If SendMessageGetResponse(MESSAGE_TYPE.TCP, "GPK", mServerPublicKey, errorMessage, ENCRYPTION_OPTION.None) Then
                    ' extract the key
                    connection.PublicKey = mServerPublicKey
                Else
                    Return False
                End If

                ' 2. generate a set of random session keys
                oDESServices = New DESServices
                Me.mSessionKeys = oDESServices.GenerateSessionKeys(CardWizard.Security.OUTPUT_FORMAT.HexDisplay)

                ' 3. send the session keys to the server encrypted with the public RSA key
                If SendMessageGetResponse(MESSAGE_TYPE.TCP, "SSK" & Me.mSessionKeys, response, errorMessage, ENCRYPTION_OPTION.PublicKey) Then
                    ' see if session keys processed without error
                    If response = "OK" Then
                        connection.DefaultEncryption = ENCRYPTION_OPTION.Session
                        'mDefaultEncryption = ENCRYPTION_OPTION.Session
                    Else
                        connection.DefaultEncryption = ENCRYPTION_OPTION.None
                        'mDefaultEncryption = ENCRYPTION_OPTION.None
                        Throw New System.Exception("failed")
                    End If
                Else
                    Return False
                End If

                Me.mEncryptionActivated = True
                Return True

            Catch ex As Exception
                Me.mEncryptionActivated = False
                Return False

            End Try
        End Function

        ''' <summary>
        ''' connects to the TCP server with the name "sServerName" at port number "iPortNumber"
        ''' also starts the TCP listener for this client
        ''' </summary>
        ''' <returns>TRUE if able to connect to TCP server</returns>
        ''' <remarks></remarks>
        Public Function Connect() As Boolean
            Try
                Dim localMachineInfo As IPHostEntry
                Dim remoteMachineInfo As IPHostEntry
                Dim serverEndpoint As IPEndPoint
                Dim myEndpoint As IPEndPoint
                Dim callback As New AsyncCallback(AddressOf ReceiveCallback)

                ' insure the socket is closed
                If Not socket Is Nothing Then
                    socket.Close()
                End If

                ' create a new connection object
                connection = New AsyncConnectionInfo()

                connection.Buffer = New Byte() {}

                ' setup a receive buffer for the client connection  
                ReDim connection.Buffer(AsyncConnectionInfo.BufferSize)

                ' 06/10/08 Begin MB
                ' Check what version of remote ip address we are connecting to IPv4 or IPv6
                '  Then match the local address version to the remote address version
                Dim iLocalMachineAddressListIndex As Integer = 0
                Dim i As Integer
                Dim AddFam As AddressFamily
                Dim IP As IPAddress = Nothing
                '03/11/09 -- if ServerIP is set use
                If Not String.IsNullOrEmpty(sServerIP) Then
                    remoteMachineInfo = Dns.GetHostEntry(sServerIP)
                    For Each tmpIP As IPAddress In remoteMachineInfo.AddressList
                        If tmpIP.ToString = sServerIP Then
                            AddFam = tmpIP.AddressFamily
                            IP = tmpIP
                        End If
                    Next
                Else
                    remoteMachineInfo = Dns.GetHostEntry(sServerName)
                    AddFam = remoteMachineInfo.AddressList(0).AddressFamily
                    IP = remoteMachineInfo.AddressList(0)
                End If

                localMachineInfo = Dns.GetHostEntry(Dns.GetHostName())
                For i = 0 To UBound(localMachineInfo.AddressList)
                    If localMachineInfo.AddressList(i).AddressFamily = AddFam Then
                        iLocalMachineAddressListIndex = i
                        Exit For
                    End If
                Next
                myEndpoint = New IPEndPoint(localMachineInfo.AddressList(iLocalMachineAddressListIndex), 0)
                ' 06/10/08 End
                '06/10/08 myEndpoint = New IPEndPoint(localMachineInfo.AddressList(0), 0)

                serverEndpoint = New IPEndPoint(IP, iPortNumber)

                socket = New Socket(myEndpoint.Address.AddressFamily, SocketType.Stream, ProtocolType.Tcp)
                socket.Connect(serverEndpoint)

                connection.socket = socket

                ' connection made...
                mConnected = True

                ' 12/04/06  DEBUG ONLY
                'Debug.WriteLine(Now.TimeOfDay.ToString & " ** TCP Connection made **")

                ' wait for incoming messages...
                socket.BeginReceive(connection.Buffer, 0, AsyncConnectionInfo.BufferSize, System.Net.Sockets.SocketFlags.None, callback, connection)
                Return True

            Catch exception As Exception
                RaiseEvent ErrorEncountered("Connect: " & exception.Message.ToString)
                mConnected = False
                Return False

            End Try
        End Function

        Private Sub ReceiveCallback(ByVal result As IAsyncResult)
            ' this gets called when a response is received from a TCP client
            ' note that this also gets called when the client breaks the TCP
            ' connection.

            Dim handler As Socket                           ' 08/03/07
            Dim oAsyncConnectionInfo As AsyncConnectionInfo ' 08/03/07

            Try
                Dim sServerResponse As String = String.Empty

                ' Retrieve the state object and the handler socket
                ' from the asynchronous state object.
                oAsyncConnectionInfo = CType(result.AsyncState, AsyncConnectionInfo)    ' 08/03/07

                ' attach any session keys
                oAsyncConnectionInfo.SessionKeys = Me.mSessionKeys

                ' extract the "socket" object
                handler = oAsyncConnectionInfo.socket      ' 08/03/07

                ' insure the handler exists
                If TypeName(handler) = "Nothing" Then
                    Exit Sub
                End If

                ' if disconnected, exit. this can occur during termination
                If Not handler.Connected Then
                    Exit Sub
                End If

                ' Read data from client socket. 
                Dim bytesRead As Integer = handler.EndReceive(result)

                Dim MessageType As MESSAGE_TYPE
                Dim MessageIdentifier As String = String.Empty
                Dim MessageData As String = String.Empty
                Dim FailureCode As FAILURE_CODE

                If bytesRead > 0 Then
                    ' There might be more data, so store the data received so far.
                    oAsyncConnectionInfo.sb.Append(Encoding.ASCII.GetString(oAsyncConnectionInfo.Buffer, 0, bytesRead))
EXAMINE_DATA:
                    ' inspect the beginning of the message for proper structure
                    If InspectMessage(oAsyncConnectionInfo, MessageData, MessageType, MessageIdentifier, FailureCode) Then
                        ' process the data...

                        ' 12/04/06  DEBUG ONLY
                        'Debug.WriteLine(Now.TimeOfDay.ToString & " RCV: Type=" & MessageType.ToString & ", ID=" & MessageIdentifier & " msg=" & MessageData)

                        Select Case MessageType
                            Case MESSAGE_TYPE.Standard
                                ' response to a message previously sent to the CW enterprise serve

                                ' check message's identifier to see if its what we expected to receive
                                If MessageIdentifier = sExpectedMessageIdentifier Then

                                    ' no error
                                    CurrentRequestResponse.Status = TCP_RESPONSE_STATUS.noError

                                    ' save the response
                                    CurrentRequestResponse.Response = MessageData

                                    ' set auto reset event to have process request thread restart
                                    are_ResponseReceivedorTimeout.Set()
                                Else
                                    ' out of sync response, ignore it and wait for another
                                    RaiseEvent ErrorEncountered("ReceiveCallback: " & "message ignored, invalid message identifier: exp=" & sExpectedMessageIdentifier & ", rcv=" & MessageIdentifier)
                                End If

                            Case MESSAGE_TYPE.TCP
                                ' response to a message previously sent to the TCP server
                                ' no error
                                CurrentRequestResponse.Status = TCP_RESPONSE_STATUS.noError

                                ' save the response
                                CurrentRequestResponse.Response = MessageData

                                ' set auto reset event to have process request thread restart
                                are_ResponseReceivedorTimeout.Set()

                            Case MESSAGE_TYPE.Server
                                ' unsolicited message arrived from server for normal client
                                ' raise an event to have the request processed on a thread from the thread pool

                                Dim task As New UnsolicitedMessageHandler

                                ' add an event handler so we can raise the event within this class
                                AddHandler task.MessageFromServer, AddressOf UnsolicitedMessageHandler

                                System.Threading.ThreadPool.QueueUserWorkItem(AddressOf task.Execute, MessageData)

                            Case MESSAGE_TYPE.CMC
                                ' unsolicited message arrived from server for SuperCMC
                                ' add this in the future if we Eliminate the clsTCPlistener

                            Case Else

                        End Select

                        ' if there's more data to process, do it now.
                        If oAsyncConnectionInfo.sb.Length > 0 Then GoTo EXAMINE_DATA

                        ' wait for next read
                        handler.BeginReceive(oAsyncConnectionInfo.Buffer, 0, AsyncConnectionInfo.BufferSize, _
                            0, New AsyncCallback(AddressOf ReceiveCallback), oAsyncConnectionInfo)

                    Else
                        Select Case FailureCode
                            Case FAILURE_CODE.WaitForMoreData
                                handler.BeginReceive(oAsyncConnectionInfo.Buffer, 0, AsyncConnectionInfo.BufferSize, _
                                    0, New AsyncCallback(AddressOf ReceiveCallback), oAsyncConnectionInfo)
                                Exit Sub

                            Case FAILURE_CODE.BadFormat
                                RaiseEvent ErrorEncountered("ReceiveCallback: invalid msg received. Ignored.")

                                handler.BeginReceive(oAsyncConnectionInfo.Buffer, 0, AsyncConnectionInfo.BufferSize, _
                                    0, New AsyncCallback(AddressOf ReceiveCallback), oAsyncConnectionInfo)
                                Exit Sub

                        End Select
                    End If

                Else
                    ' if no data received, we may have lost our connection from server
                    ' so close our socket connection

                    Dim sIPaddress As String = CType(handler.RemoteEndPoint, IPEndPoint).Address.ToString()
                    Dim sPort As String = CType(handler.RemoteEndPoint, IPEndPoint).Port.ToString()

                    ' 12/18/06 set flag saying we are disconnected
                    Me.mConnected = False       '12/18/06

                    ' RAISE EVENT (but only if we are not in process of disposing this object)
                    If Not Me.mObjectDisposed Then          ' 12/18/06
                        RaiseEvent ClientDisconnected()     ' 12/18/06
                    End If                                  ' 12/18/06

                    If handler.Connected Then
                        Try
                            handler.Shutdown(SocketShutdown.Both)
                            handler.Close()

                        Catch ex As Exception

                        End Try
                    End If

                End If

                Exit Sub

            Catch ex As Exception
                ' RAISE EVENT
                RaiseEvent ErrorEncountered("ReceiveCallback: " & ex.Message.ToString)

            End Try
        End Sub

        Private Sub UnsolicitedMessageHandler(ByVal MessageData As String)
            ' called by the raised event from the thread from the thread pool
            Try
                RaiseEvent MessageFromServer(MessageData)
            Catch ex As Exception

            End Try
        End Sub

        ''' <summary>
        ''' sends the XML to the TCP server and returns the response string to caller
        ''' </summary>
        ''' <param name="MessageType">type of message to be sent</param>
        ''' <param name="Message">message to be sent</param>
        ''' <returns>response from server</returns>
        ''' <remarks></remarks>
        Public Function ServerRequest(ByVal MessageType As MESSAGE_TYPE, ByRef Message As String) As String
            Dim sResponse As String = String.Empty
            Dim sErrorMessage As String = String.Empty

            Try
                ' if not connected to server, establish the connection now
                If Not mConnected Then
                    If Not Connect() Then
                        Return "!unable to connect to server"   ' 12/26/06
                    End If
                End If

                If Me.SendMessageGetResponse(MessageType, Message, sResponse, sErrorMessage) Then
                    ' worked, return the response to caller
                    Return sResponse
                Else
                    ' failed
                    Return sErrorMessage
                End If

            Catch ex As Exception
                RaiseEvent ErrorEncountered("ServerRequest: " & ex.Message.ToString)
                Return ex.Message.ToString

            End Try
        End Function



        ''' <summary>
        ''' sends a TCP message to the TCP server
        ''' </summary>
        ''' <param name="MessageType">type of message: STD=normal message for CW server, TCP=message for TCP server</param>
        ''' <param name="sData">message to send</param>
        ''' <param name="sResponse">response from the server</param>
        ''' <param name="sErrorMessage">if function returns FALSE, these has description of failure</param>
        ''' <param name="TypeOfEncryption">optional type of encryption to use</param>
        ''' <returns>TRUE if successful</returns>
        ''' <remarks></remarks>
        Public Function SendMessageGetResponse(ByVal MessageType As MESSAGE_TYPE, ByVal sData As String, ByRef sResponse As String, ByRef sErrorMessage As String, Optional ByVal TypeOfEncryption As ENCRYPTION_OPTION = ENCRYPTION_OPTION.UseDefault) As Boolean
            ' sends a message and waits for a response
            ' 12/04/06  new version that uses queuing with separate thread for processing

            Try
                Dim sLength As String = String.Empty
                Dim sType As String = String.Empty

                sErrorMessage = String.Empty

                ' 12/04/06  DEBUG ONLY
                'Debug.WriteLine(Now.TimeOfDay.ToString & " SendMessageGetResponse: thread=" & Thread.GetDomainID & ", msg=" & sData)

                ' create request object
                Dim oTCPrequest As New TCPrequest

                With oTCPrequest
                    .MessageType = MessageType
                    .sData = sData
                    .TypeOfEncryption = TypeOfEncryption
                    '.CallerIndex = 1    ' <== may need to set this in "AddRequest" method
                End With

                ' queue the request and wait for reply or timeout
                oTCPrequestProcessor.AddRequest(oTCPrequest)

                are_ProcessRequestCompleted(oTCPrequest.CallerIndex) = New AutoResetEvent(False)

                ' activate the processor so it will process the request(s)
                are_ProcessRequest.Set()

                ' wait for the reply
                are_ProcessRequestCompleted(oTCPrequest.CallerIndex).WaitOne()

                ' see if there were any errors or a time out
                Select Case ProcessRequest_Response(oTCPrequest.CallerIndex).Status
                    Case TCP_RESPONSE_STATUS.noError
                        sResponse = ProcessRequest_Response(oTCPrequest.CallerIndex).Response
                        Return True

                    Case TCP_RESPONSE_STATUS.timedOut
                        sErrorMessage = "timed out waiting for response from server"
                        Return False

                    Case TCP_RESPONSE_STATUS.otherError
                        sErrorMessage = ProcessRequest_Response(oTCPrequest.CallerIndex).otherErrorMessage
                        Return False

                End Select

            Catch ex As Exception
                RaiseEvent ErrorEncountered("SendMessageGetResponse: " & ex.Message.ToString)
                sErrorMessage = ex.Message.ToString
                Return False
            End Try

            Return False
        End Function

        Public Sub New(ByVal identifier As String, ByVal timeout As Integer)  ' 04/21/08
            ' argument is used to uniqely identify the tread name for debugging purposes  04/21/08
            Try
                mWaitForReplyLimit = timeout
                ' start separate thread for processing TCP requests
                oTCPrequestProcessor = New TCPrequestProcessor
                oTCPrequestProcessorThread = New Thread(AddressOf oTCPrequestProcessor.RequestProcessorThread)
                oTCPrequestProcessorThread.Name = identifier & ":TCPrequestProcessorThread"  ' 04/21/08

                ' pass reference to ourself
                oTCPrequestProcessor.oTCPclient = Me

                ' start the thread, and let it do the work
                oTCPrequestProcessor.TCPrequestProcessorState = True     ' turn on processor
                oTCPrequestProcessorThread.Start()  ' start the thread

            Catch ex As Exception

            End Try
        End Sub
    End Class

    Public Class TCPrequestProcessor
        ' 12/04/06
        ' this class defines the single thread that is used
        ' for communicating with TCP server

        ' =====================
        '   PUBLIC EVENTS
        ' =====================

        Public Event ErrorMessage(ByVal sMessage As String)
        Public Event ResponseReceived(ByVal sResponse As String, ByVal iIndex As Integer)

        ' if TRUE, processor running, if FALSE, processor stopped
        Public TCPrequestProcessorState As Boolean = False

        ' queue for processing Excrypt requests serially
        ' 04/21/08 Private Shared TCPrequestProcessorQueue As New System.Collections.Queue
        Private TCPrequestProcessorQueue As New System.Collections.Queue ' 04/21/08  DOES NOT NEED TO BE SHARED

        ' reference to the TCP client class
        Public oTCPclient As TCPClient

        ' counter (0-9) for identifying TCP transmissions
        Private mMessageIdentifier As Integer = 0

        ' caller's index (assigned sequentially from 1-9 then repeats)
        Private mCallerIndex As Integer = 0     ' 12/04/06

        ' =====================
        '   PUBLIC FUNCTIONS
        ' =====================
        Friend Sub RequestProcessorThread()
            ' runs on a separate thread and is "single processing"
            ' requests are queued to this thread's request queue
            ' and this thread will process TCP requests one at a time
            ' to prevent synchronization problems

            Dim threadName As String = "RequestProcessorThread: "   ' 04/21/08

            Try
                Do While TCPrequestProcessorState
                    ' wait for a request to process
                    ' we do a "WaitOne" on the auto reset event object
                    ' someone else will set this object to TRUE
                    ' 04/21/08 TCPClient.are_ProcessRequest.WaitOne()
                    oTCPclient.are_ProcessRequest.WaitOne()     ' 04/21/08

                    ' process all items currently in the queue
                    Do While TCPrequestProcessorQueue.Count > 0
                        Dim oTCPrequest As TCPrequest = CType(TCPrequestProcessorQueue.Dequeue(), TCPrequest)
                        If oTCPrequest.sData.Length > 0 Then

                            ' handle special messages
                            If oTCPrequest.sData = "STOP" Then
                                ' terminate this thread
                                GoTo TERMINATE_THREAD   ' 04/21/08
                            End If

                            ' need this when we get the response from server to insure we are in sync
                            ' 04/21/08 TCPclient.sExpectedMessageIdentifier = Me.GetNextIdentifier
                            oTCPclient.sExpectedMessageIdentifier = Me.GetNextIdentifier.ToString    ' 04/21/08

                            ' 12/04/06  DEBUG ONLY
                            'Debug.WriteLine(Now.TimeOfDay.ToString & " SEND: ID=" & clsTCPclient.sExpectedMessageIdentifier & ", index=" & oTCPrequest.CallerIndex & ", msg=" & oTCPrequest.sData)

                            ' now wait for reply...but only for so long (uses timer)
                            ' 04/19/07 Dim dueTime As New TimeSpan(0, 0, 5)    5 seconds
                            Dim dueTime As New TimeSpan(0, 0, oTCPclient.WaitForReplyLimit) '04/19/07 USE cfg option to set time out, default is 30
                            Dim period As New TimeSpan(-1)          ' only fire event one time

                            Using localTimer As New Timer(AddressOf TimeoutCallback, Nothing, dueTime, period)

                                ' 04/21/08 TCPClient.are_ResponseReceivedorTimeout = New AutoResetEvent(False)
                                oTCPclient.are_ResponseReceivedorTimeout = New AutoResetEvent(False) ' 04/21/08

                                ' send the request to TCP server
                                ' 04/21/08 If Not oTCPclient.SendMessage(TCPClient.connection, oTCPrequest.MessageType, TCPClient.sExpectedMessageIdentifier, oTCPrequest.sData, oTCPrequest.TypeOfEncryption) Then
                                If Not oTCPclient.SendMessage(oTCPclient.connection, oTCPrequest.MessageType, oTCPclient.sExpectedMessageIdentifier, oTCPrequest.sData, oTCPrequest.TypeOfEncryption) Then   ' 04/21/08
                                    RaiseEvent ErrorMessage("SendMessageGetResponse: " & "unable to send message: " & oTCPrequest.sData)
                                    'Return False
                                End If

                                ' if no response received in 5 seconds, the flag "bTimedOut" will be set
                                ' 04/21/08 TCPClient.are_ResponseReceivedorTimeout.WaitOne()
                                oTCPclient.are_ResponseReceivedorTimeout.WaitOne()   ' 04/21/08

                                ' get the results for caller
                                ' 04/21/08 TCPClient.ProcessRequest_Response(oTCPrequest.CallerIndex) = TCPClient.CurrentRequestResponse
                                oTCPclient.ProcessRequest_Response(oTCPrequest.CallerIndex) = oTCPclient.CurrentRequestResponse   ' 04/21/08

                                ' notify requesting thread that we are done and the caller thread can continue
                                ' 04/21/08 TCPClient.are_ProcessRequestCompleted(oTCPrequest.CallerIndex).Set()
                                oTCPclient.are_ProcessRequestCompleted(oTCPrequest.CallerIndex).Set()    ' 04/21/08

#If True = False Then
                            ' did we get a response?
                            If TCPclient.bMessageReceived Then
                                ' return the response using an event
                                'RaiseEvent ResponseReceived(oTCPrequest.sResponse, oTCPrequest.CallerIndex)

                                ' notify requesting thread that we are done and the caller thread can continue
                                clsTCPclient.are_ProcessRequestCompleted(oTCPrequest.CallerIndex).Set()
                            End If

                            ' did we time out?
                            If TCPclient.bTimedOut Then
                                'sReply = String.Empty
                                Throw New System.Exception("Receive operation timed out.")
                            End If

                            ' anything else is an error
                            Throw New System.Exception("no reply, no timeout?")
#End If
                            End Using

#If True = False Then
                        ' return the response using an event
                        RaiseEvent ResponseReceived(oTCPrequest.sResponse, oTCPrequest.CallerIndex)

                        ' notify requesting thread that we are done and the caller thread can continue
                        clsTCPclient.are_ProcessRequestCompleted(oTCPrequest.CallerIndex).Set()
#End If
                        End If
                    Loop
                Loop

                ' 04/21/08 start changes
TERMINATE_THREAD:
                oTCPclient = Nothing
                RaiseEvent ErrorMessage(threadName & "TCP request processor terminated normally")
                ' 04/21/08 end changes

            Catch ex As Exception
                RaiseEvent ErrorMessage(threadName & ex.Message.ToString)   ' 04/21/08

            End Try
        End Sub

        Private Function GetNextCallerIndex() As Integer
            Try
                ' increment our client (goes from 1-9)
                If mCallerIndex > 8 Then
                    mCallerIndex = 1
                Else
                    mCallerIndex += 1
                End If

                Return mCallerIndex

            Catch ex As Exception

            End Try

            Return Nothing
        End Function

        Private Function GetNextIdentifier() As Integer
            Try
                ' increment our static message identifer (goes from 1-9)
                If mMessageIdentifier > 8 Then
                    mMessageIdentifier = 1
                Else
                    mMessageIdentifier += 1
                End If

                Return mMessageIdentifier

            Catch ex As Exception

            End Try
            Return Nothing
        End Function

        Private Sub TimeoutCallback(ByVal state As Object)
            ' timer has fired
            Try
                ' set global flag saying the operation "timed out"
                'clsTCPclient.bTimedOut = True

                ' indicate the operation "timed out"
                ' 04/21/08 TCPClient.CurrentRequestResponse.Status = TCPClient.TCP_RESPONSE_STATUS.timedOut
                oTCPclient.CurrentRequestResponse.Status = TCPClient.TCP_RESPONSE_STATUS.timedOut    ' 04/21/08

                ' set flag to have process request thread restart so it will notice the "timed out" condition
                ' 04/21/08 TCPClient.are_ResponseReceivedorTimeout.Set()
                oTCPclient.are_ResponseReceivedorTimeout.Set()   ' 04/21/08

            Catch ex As Exception

            End Try
        End Sub

        Friend Function StopProcessor() As Boolean
            ' stops the processor thread
            Try
                ' add a request
                Dim oTCPrequest As New TCPrequest
                oTCPrequest.sData = "STOP"
                AddRequest(oTCPrequest)

                ' force the other thread ("RequestProcessorThread" method) to process the request
                ' 04/21/08 TCPClient.are_ProcessRequest.Set()
                oTCPclient.are_ProcessRequest.Set()  ' 04/21/08

                Return True

            Catch ex As Exception
                RaiseEvent ErrorMessage("StopProcessor: " & ex.Message.ToString)
                Return False

            End Try
        End Function

        Friend Function AddRequest(ByRef oTCPrequest As TCPrequest) As Boolean
            ' add a request to the Excrypt processor queue
            Try
                ' get the next available caller index
                oTCPrequest.CallerIndex = GetNextCallerIndex()

                TCPrequestProcessorQueue.Enqueue(oTCPrequest)

            Catch ex As Exception
                RaiseEvent ErrorMessage("AddRequest: " & ex.Message.ToString)
                Return False

            End Try
            Return False
        End Function
    End Class

    Friend Class TCPrequest
        ' defines the standard TCP request object
        Public MessageType As TCPBase.MESSAGE_TYPE
        Public sData As String
        Public sResponse As String
        Public sErrorMessage As String
        Public TypeOfEncryption As TCPBase.ENCRYPTION_OPTION = TCPBase.ENCRYPTION_OPTION.UseDefault
        Public CallerIndex As Integer      ' callers unique index  (1,2,...)
    End Class

    Friend Class UnsolicitedMessageHandler
        ' special class used for handling unsolicited server messages using
        ' a thread from the thread pool
        Public Event MessageFromServer(ByVal sMessage As String)

        Sub Execute(ByVal MessageReceived As Object)
            Try
                RaiseEvent MessageFromServer(MessageReceived.ToString)
            Catch ex As Exception

            End Try
        End Sub

    End Class
End Namespace