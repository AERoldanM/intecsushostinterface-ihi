﻿Imports System.Text
Imports System.Net
Imports System.Net.Sockets
Imports System.Threading
Imports CardWizard.Security.Cryptography
Imports CardWizard.Security

Namespace TCP
    ''' <summary>
    ''' this is used by other classes "TCPServer" and "TCPclient"
    ''' contains common methods, constants and enumerations
    ''' </summary>
    ''' <remarks></remarks>
    Public MustInherit Class TCPBase

        ' support "Dispose" method
        Implements IDisposable

        Enum MESSAGE_TYPE
            ' describes the type of message
            ' "CMC" indicates an unsolicited SuperCMC message from the CW enterprise server
            ' "STD" indicates a standard request to/from the CW Enterprise server
            ' "SVR" indicates an unsolicited message from the CW Enterprise server for the TCP client
            ' "TCP" indicates a message to/from the TCP client and TCP server
            Standard
            TCP
            CMC
            Server
        End Enum

        Enum FAILURE_CODE
            ' used by the inspect method
            None
            WaitForMoreData
            BadFormat
        End Enum

        Enum ENCRYPTION_OPTION
            ' <DC1>:= hex 11    <message> NOT encrypted
            ' <DC2>:= hex 12    <message> encrypted with session keys
            ' <DC3>:= hex 13    <message> encrypted with RSA public key
            None
            UseDefault
            Session
            PublicKey
        End Enum

        Friend Structure PERFORMANCE_MONITOR        ' 09/18/06
            Dim MessageIdentifier As String         ' single character identifier for msg
            Dim MessageType As MESSAGE_TYPE         ' message type
            Dim MessageLength As Integer            ' length of message sent/received
            Dim SendMessageTime As TimeSpan         ' time to do "SendMessage" method
            Dim EncryptionType As String            ' type of encryption "none", "public key", "session"
            Dim EncryptionTime As TimeSpan          ' time to do encryption operation
        End Structure

        ' 10/27/07 Friend PM_SendMessage As PERFORMANCE_MONITOR    ' 09/18/06

        ' control characters used in the protocol
        Friend Const SOH As Char = Chr(1)
        Friend Const STX As Char = Chr(2)
        Friend Const DC1 As Char = Chr(17)
        Friend Const DC2 As Char = Chr(18)
        Friend Const DC3 As Char = Chr(19)

        Friend Const LENGTH_messageType As Integer = 3
        Friend Const LENGTH_messageIdentifier As Integer = 1

        ' used for RSA encryption services
        Friend oRSAServices As RSAServices

        ' used for DES encryption services
        Friend oDESServices As DESServices

        ' if TRUE, indicates this object is being disposed  ' 08/03/07
        Friend mObjectDisposed As Boolean = False          ' 08/03/07
        ' ============================
        '   private variables
        ' ============================
        Private SendMessageBUSY As Boolean = False       ' TRUE says SendMessage method is busy

        ' ================================
        '  Locks used for synchronization
        ' ================================
        Private rwlSendMessage As New ReaderWriterLock   ' 10/27/07 prevents more than one thread sending a TCP message

        ''' <summary>
        ''' Determines if we have received the full message from the TCP sender
        ''' and if the message is in the proper format.
        ''' "FailureCode" explains reason for failure.
        ''' </summary>
        ''' <param name="oAsyncConnectionInfo">caller provides this object</param>
        ''' <param name="MessageData">complete message returned to caller if function returns TRUE</param>
        ''' <param name="MessageType">returned to caller if function returns TRUE</param>
        ''' <param name="MessageIdentifier">returned to caller if function returns TRUE</param>
        ''' <param name="FailureCode">returned to caller if function returns FALSE</param>
        ''' <returns>Returns TRUE if complete message received and processed without error. Returns FALSE if incomplete message or bad format message. </returns>
        ''' <remarks>If data needs to be decrypted, this is done and returned in "sData". If data does not need to be decrypted, it is also returned in "sData".</remarks>
        Public Function InspectMessage(ByRef oAsyncConnectionInfo As AsyncConnectionInfo, ByRef MessageData As String, ByRef MessageType As MESSAGE_TYPE, ByRef MessageIdentifier As String, ByRef FailureCode As FAILURE_CODE) As Boolean
            ' this method is used to determine if we have received the full message from the TCP sender
            ' and if the message is in the proper format
            ' if data needs to be decrypted, this is done and returned in "sData"
            ' if data does not need to be decrypted, it is also returned in "sData"
            ' function returns TRUE if complete message received and processed without error
            ' function returns FALSE if incomplete message or bad format message. 
            ' "FailureCode" explains reason for failure.

            ' TCP messages have this BNF format
            ' <SOH><type><identifier><encrypt><length><STX><message>
            '
            ' where:
            ' <SOH>:= hex 01
            ' <STX>:= hex 02
            ' <encrypt>:= <DC1>|<DC2>|<DC3>
            '   <DC1>:= hex 11    <message> NOT encrypted
            '   <DC2>:= hex 12    <message> encrypted with session keys
            '   <DC3>:= hex 13    <message> encrypted with RSA public key
            ' <message>:= <data>
            ' <type>:= CMC|STD|SVR
            '   "CMC" indicates a SuperCMC request from the enterprise server
            '   "STD" indicates a standard request to/from the TCP client and Enterprise server
            '   "SVR" indicates an unsolicited message from the Enterprise server for the TCP client
            '   "TCP" indicates a message to/from the TCP client and TCP server
            ' <identifier>:= 1 character identifier (0-9, A-Z) used to match responses for "STD" and "TCP" types
            '                required, but not used for other types
            '
            ' <length>:= length of <message> in hex  (e.g. if length is 300 then use "12C"
            ' <data>:= one or more characters

            ' TCP messages
            ' for messages with the <type> of TCP, the following functions are defined
            '
            ' (from client) <data>:=VER         asks the TCP server for its version information
            ' (to client)   <data>:=version 1.0.0.1

            ' (from client) <data>:=GPK         asks the TCP server for its public RSA key
            ' (to client)   <data>:=(public key string)

            ' (from client) <data>:=SSK<key 1><key 2><iv>     sets a session key with the TCP server
            '               where:  <key 1>, <key 2> and <iv> are 16 hex character values
            ' (to client)   <data>:=<status>
            '               where:  <status>:=OK|ERR    
            '                       OK=session key established, or ERR=bad data, etc.

            Dim _SOH As Integer                 ' index where <SOH> is located in .sb
            Dim _STX As Integer                 ' index where <STX> is located in .sb
            Dim _length As Integer              ' <length>
            Dim _messageType As String          ' message type e.g. "STD"
            Dim _message As String              ' <message> before decryption
            Dim _encryptionOption As ENCRYPTION_OPTION
            Dim clearText As String = String.Empty
            Dim _messageIdentifier As String = String.Empty ' message identifier

            Try
BEGIN_INSPECTION:

                If oAsyncConnectionInfo.MessageLength > 0 Then
                    If oAsyncConnectionInfo.MessageLength >= oAsyncConnectionInfo.sb.ToString.Length Then
                        GoTo WAIT_FOR_MORE
                    End If
                End If


                ' special case where message starts with "[" and ends with "]"
                ' if so, then handle in raw format (used for Excrypt testing)
                If Left(oAsyncConnectionInfo.sb.ToString, 1) = "[" Then
                    If Right(oAsyncConnectionInfo.sb.ToString, 1) = "]" Then
                        MessageData = oAsyncConnectionInfo.sb.ToString

                        ' clear the string builder
                        oAsyncConnectionInfo.sb = New StringBuilder

                        FailureCode = FAILURE_CODE.None
                        Return True
                    End If
                End If
                ' 1. see if SOH present
                _SOH = oAsyncConnectionInfo.sb.ToString.IndexOf(SOH, 0)

                If _SOH < 0 Then
                    ' no, ignore data received so far
                    EventLog.WriteEntry("CWTCPbase", "InspectMessage, invalid SOH:" & oAsyncConnectionInfo.sb.ToString, EventLogEntryType.Warning)
                    GoTo FLUSH_BAD_DATA
                End If

                ' 1.1  insure we have received at least 7 characters, otherwise wait for more
                If oAsyncConnectionInfo.sb.ToString.Length < 7 Then
                    GoTo WAIT_FOR_MORE
                End If

                ' 2. get message type
                _messageType = oAsyncConnectionInfo.sb.ToString.Substring(_SOH + 1, LENGTH_messageType)

                ' 2.1 return proper message type code
                Select Case _messageType
                    Case "STD"
                        MessageType = MESSAGE_TYPE.Standard
                    Case "SVR"
                        MessageType = MESSAGE_TYPE.Server
                    Case "TCP"
                        MessageType = MESSAGE_TYPE.TCP
                    Case "CMC"
                        MessageType = MESSAGE_TYPE.CMC
                    Case Else
                        ' anything else is invalid
                        EventLog.WriteEntry("CWTCPbase", "InspectMessage, invalid MessageType:" & oAsyncConnectionInfo.sb.ToString, EventLogEntryType.Warning)
                        GoTo FLUSH_BAD_DATA
                End Select

                ' 2.1 get message identifier
                _messageIdentifier = oAsyncConnectionInfo.sb.ToString.Substring(_SOH + 1 + LENGTH_messageType, 1)

                ' 2.2 return message identifier to caller
                MessageIdentifier = _messageIdentifier

                ' 3. get the <encrypt> option
                Dim sEncryptionOption As Char = CChar(oAsyncConnectionInfo.sb.ToString.Substring(_SOH + 1 + LENGTH_messageType + LENGTH_messageIdentifier, 1))

                Select Case sEncryptionOption
                    Case DC1
                        _encryptionOption = ENCRYPTION_OPTION.None
                    Case DC2
                        _encryptionOption = ENCRYPTION_OPTION.Session
                    Case DC3
                        _encryptionOption = ENCRYPTION_OPTION.PublicKey
                    Case Else
                        ' invalid option
                        ' clear the message to this point and try again
                        GoTo CLEAR_RETRY
                End Select

                ' 4. get the <length> (must be followed by STX character within 8 characters)

                ' first look for STX character
                _STX = oAsyncConnectionInfo.sb.ToString.IndexOf(STX, 0)

                ' found one?
                If _STX < 0 Then
                    FailureCode = FAILURE_CODE.BadFormat
                    Return False
                Else
                    ' is the STX within 12 characters of the SOH?
                    If (_STX - _SOH) > 12 Then
                        GoTo CLEAR_RETRY
                    End If
                End If

                ' get <length>
                Try
                    _length = Convert.ToInt32(oAsyncConnectionInfo.sb.ToString.Substring(_SOH + 1 + LENGTH_messageType + LENGTH_messageIdentifier + 1, _STX - _SOH - LENGTH_messageType - LENGTH_messageIdentifier - 2), 16)
                    oAsyncConnectionInfo.MessageLength = _length
                Catch ex As Exception
                    EventLog.WriteEntry("CWTCPbase", "InspectMessage, invalid length bytes:" & oAsyncConnectionInfo.sb.ToString & " Err:" & ex.Message, EventLogEntryType.Warning)
                    GoTo FLUSH_BAD_DATA
                End Try

                ' 5. see if we have all the <data> or need to wait for the remainder
                Try
                    If oAsyncConnectionInfo.sb.ToString.Substring(_STX).Length < _length + 1 Then
                        ' wait for more data
                        GoTo WAIT_FOR_MORE
                    End If

                Catch ex As Exception
                    GoTo WAIT_FOR_MORE
                End Try

                ' 5.1 get <message>
                _message = oAsyncConnectionInfo.sb.ToString.Substring(_STX + 1, _length)

                ' 5.2 decrypt <message> if necessary
                Select Case _encryptionOption
                    Case ENCRYPTION_OPTION.None

                    Case ENCRYPTION_OPTION.PublicKey
                        ' decrypt message using private RSA key
                        If oRSAServices.DecryptWithProvider(_message, clearText) Then
                            _message = clearText
                        Else
                            EventLog.WriteEntry("CWTCPbase", "InspectMessage, invalid RSA Decrypt:" & oAsyncConnectionInfo.sb.ToString, EventLogEntryType.Warning)
                            GoTo FLUSH_BAD_DATA
                        End If

                    Case ENCRYPTION_OPTION.Session
                        ' decrypt message using the TCP connection's session keys
                        Try
                            ' convert from display to hex
                            'If Not oDESServices.ConvertDisplayToHex(_message, _message) Then
                            ' GoTo FLUSH_BAD_DATA
                            'End If

                            ' convert from Base64 Unicode to 8-bit bytes
                            'Dim bNoError As Boolean = False
                            'Dim sErrorMessage As String = String.Empty
                            '_message = Base64Decode(_message, bNoError, sErrorMessage)

                            'If Not bNoError Then
                            'GoTo FLUSH_BAD_DATA
                            'End If

                            ' decrypt the message (in Base64 format) using our session keys
                            If oDESServices.EncryptOrDecryptBase64String(CRYPTO_ACTION.Decrypt, DES_TYPE.TripleDES, System.Security.Cryptography.CipherMode.CBC, _message, clearText, oAsyncConnectionInfo.SessionKeys.Substring(0, 16), oAsyncConnectionInfo.SessionKeys.Substring(16, 16), oAsyncConnectionInfo.SessionKeys.Substring(32, 16), " ", OUTPUT_FORMAT.ASCIIDisplay) Then
                                _message = clearText
                            Else
                                EventLog.WriteEntry("CWTCPbase", "InspectMessage, invalid Session Key Decrypt:" & oAsyncConnectionInfo.sb.ToString, EventLogEntryType.Warning)
                                GoTo FLUSH_BAD_DATA
                            End If

                        Catch ex As Exception
                            EventLog.WriteEntry("CWTCPbase", "InspectMessage, invalid Session Key Decrypt exception:" & oAsyncConnectionInfo.sb.ToString & " Err:" & ex.Message, EventLogEntryType.Warning)
                            GoTo FLUSH_BAD_DATA
                        End Try

                End Select

                ' 6.0 return the complete unencrypted <data> to caller
                MessageData = _message.TrimEnd          ' remove any trailing spaces

                ' preserve any trailing data as it could be another message to be processed
                If oAsyncConnectionInfo.sb.Length > _STX + 1 + _length Then
                    ' only clear the data we have processed
                    oAsyncConnectionInfo.sb.Remove(0, _STX + 1 + _length)
                    oAsyncConnectionInfo.MessageLength = 0
                Else
                    ' clear the string builder
                    oAsyncConnectionInfo.sb = New StringBuilder
                    oAsyncConnectionInfo.MessageLength = 0
                End If

                FailureCode = FAILURE_CODE.None
                Return True

CLEAR_RETRY:
                ' clear the message to this point and try again
                oAsyncConnectionInfo.sb.Remove(0, _SOH + 1)
                oAsyncConnectionInfo.MessageLength = 0
                GoTo BEGIN_INSPECTION

WAIT_FOR_MORE:
                ' wait for more characters from TCP client
                FailureCode = FAILURE_CODE.WaitForMoreData
                Return False

FLUSH_BAD_DATA:
                oAsyncConnectionInfo.sb = New StringBuilder
                oAsyncConnectionInfo.MessageLength = 0

                ' reset length and flag
                'oAsyncConnectionInfo.MessageLength = 0
                'oAsyncConnectionInfo.WaitingForRemainder = False

                FailureCode = FAILURE_CODE.BadFormat
                Return False

            Catch ex As Exception
                Return False

            End Try
        End Function

        ''' <summary>
        ''' TCP messages have this BNF format
        ''' [SOH][type][identifier][encrypt][length][STX][message]
        '''
        ''' where:
        ''' [SOH]:= hex 01
        ''' [STX]:= hex 02
        ''' [DC1]:= hex 11    [message] NOT encrypted
        ''' [DC2]:= hex 12    [message] encrypted with session keys
        ''' [DC3]:= hex 13    [message] encrypted with RSA public key
        ''' [encrypt]:= [DC1]|[DC2]|[DC3]
        ''' [message]:= [data]
        ''' [type]:= CMC|STD|SVR
        ''' [identifier]:= 1 character identifier (0-9, A-Z) used to match responses for "STD" and "TCP" types
        '''                required, but not used for other types
        ''' "CMC" indicates a SuperCMC request from the enterprise server
        ''' "STD" indicates a standard request to/from the TCP client and Enterprise server
        ''' "SVR" indicates an unsolicited message from the Enterprise server for the TCP client
        ''' "TCP" indicates a message to/from the TCP client and TCP server
        '''
        ''' [length]:= length of [message] in hex
        ''' [data]:= one or more characters
        ''' </summary>
        ''' <param name="oAsyncConnectionInfo"></param>
        ''' <param name="MessageType"></param>
        ''' <param name="MessageIdentifier"></param>
        ''' <param name="Message"></param>
        ''' <param name="TypeofEncryption"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function SendMessage(ByRef oAsyncConnectionInfo As AsyncConnectionInfo, ByVal MessageType As MESSAGE_TYPE, ByVal MessageIdentifier As String, ByVal Message As String, Optional ByVal TypeofEncryption As ENCRYPTION_OPTION = ENCRYPTION_OPTION.UseDefault) As Boolean
            ' sends a message to a socket

            Try
                Dim _Length As String = String.Empty
                Dim _MessageType As String = String.Empty
                Dim _MessageIdentifier As String
                Dim _cipherText As String = String.Empty
                Dim _Encryption As Char
                ' 10/27/07 Dim oStopWatch As Stopwatch         ' 09/18/06
                ' 10/27/07 Dim oStopWatchCrypto As Stopwatch   ' 09/18/06

                ' 08/03/07 start of changes
                ' need to insure that the socket exists. It's possible it has gone away.
                ' if the socket is closed
                If IsNothing(oAsyncConnectionInfo.socket) Then
                    EventLog.WriteEntry("CWTCPbase", "SendMessage: socket disposed", EventLogEntryType.Warning)
                    Return False
                End If

                If Not oAsyncConnectionInfo.socket.Connected Then
                    EventLog.WriteEntry("CWTCPbase", "SendMessage: socket closed", EventLogEntryType.Warning)
                    Return False
                End If
                ' 08/03/07 end of changes

                ' special case where message starts with "[" and ends with "]"
                ' if so, then handle in raw format (used for Excrypt testing)
                If Left(Message, 1) = "[" Then
                    If Right(Message, 1) = "]" Then
                        Dim encoder2 As UTF8Encoding = New UTF8Encoding()
                        Dim encBytes As Byte() = encoder2.GetBytes(Message.ToString)
                        oAsyncConnectionInfo.socket.Send(encBytes, encBytes.Length, SocketFlags.None)
                        Return True
                    End If
                End If
                ' start timing for this method
                ' 10/27/07 oStopWatch = Stopwatch.StartNew()      ' 09/18/06

                ' get the message identifier, if not provided, use "1"
                If MessageIdentifier.Length = 1 Then
                    _MessageIdentifier = MessageIdentifier
                Else
                    _MessageIdentifier = "1"
                End If

                ' determine the type of message
                Select Case MessageType
                    Case MESSAGE_TYPE.CMC
                        _MessageType = "CMC"
                    Case MESSAGE_TYPE.Server
                        _MessageType = "SVR"
                    Case MESSAGE_TYPE.Standard
                        _MessageType = "STD"
                    Case MESSAGE_TYPE.TCP
                        _MessageType = "TCP"
                    Case Else
                        EventLog.WriteEntry("CWTCPbase", "SendMessage: invalid MessageType", EventLogEntryType.Warning)
                        Return False
                End Select
                ' 10/27/07 PM_SendMessage.MessageType = MessageType    ' 01/05/07

                ' if using "default encryption" flag it now
                If TypeofEncryption = ENCRYPTION_OPTION.UseDefault Then
                    TypeofEncryption = oAsyncConnectionInfo.DefaultEncryption
                End If

                ' encrypt message if necessary

                ' start timing for encryption
                ' 10/27/07 oStopWatchCrypto = Stopwatch.StartNew()      ' 09/18/06

                Select Case TypeofEncryption
                    Case ENCRYPTION_OPTION.None
                        _Encryption = DC1
                        ' 10/27/07 PM_SendMessage.EncryptionType = "none"

                    Case ENCRYPTION_OPTION.PublicKey
                        _Encryption = DC3
                        ' 10/27/07 PM_SendMessage.EncryptionType = "public key"

                        ' encrypt the data using the server's RSA public key
                        Dim oRSAservices As RSAServices = New RSAServices
                        If oRSAservices.EncryptWithPublicKey(Message, oAsyncConnectionInfo.PublicKey, _cipherText) Then
                            Message = _cipherText
                        Else
                            ' failed
                            EventLog.WriteEntry("CWTCPbase", "SendMessage: invalid RSA encryption", EventLogEntryType.Warning)
                            Return False
                        End If

                    Case ENCRYPTION_OPTION.Session
                        _Encryption = DC2
                        ' 10/27/07 PM_SendMessage.EncryptionType = "session"

                        ' encrypt using triple DES with CBC, output will be hex display characters
                        If oDESServices.EncryptOrDecryptString(CRYPTO_ACTION.Encrypt, DES_TYPE.TripleDES, System.Security.Cryptography.CipherMode.CBC, Message, _cipherText, oAsyncConnectionInfo.SessionKeys.Substring(0, 16), oAsyncConnectionInfo.SessionKeys.Substring(16, 16), oAsyncConnectionInfo.SessionKeys.Substring(32, 16), " ", OUTPUT_FORMAT.Base64) Then
                            Message = _cipherText
                        Else
                            ' failed
                            EventLog.WriteEntry("CWTCPbase", "SendMessage: invalid session key decryption", EventLogEntryType.Warning)
                            Return False
                        End If

                    Case Else
                        ' error
                        EventLog.WriteEntry("CWTCPbase", "SendMessage: invalid encyption type:" & TypeofEncryption.ToString, EventLogEntryType.Warning)
                        Return False

                End Select

                ' save the time it took to do encryption
                ' 10/27/07 oStopWatchCrypto.Stop()
                ' 10/27/07 PM_SendMessage.EncryptionTime = oStopWatchCrypto.Elapsed
                ' 10/27/07 oStopWatchCrypto = Nothing

BUILD_FORMAT:
                ' get the length of the message in hex <type><data>
                _Length = Hex(((Message.Length)).ToString)

                ' use a string builder for better performance for long messages
                Dim sb As New StringBuilder(Message.Length + _Length.Length)

                ' build the proper message to send
                sb.Append(SOH)          ' SOH

                ' <type>
                sb.Append(_MessageType)

                ' message identifier
                sb.Append(_MessageIdentifier)

                ' encryption option
                sb.Append(_Encryption)

                ' message length
                sb.Append(_Length)

                ' STX
                sb.Append(STX)          ' STX

                ' <message>
                sb.Append(Message)

                ' 10/27/07 begin changes
                Try
                    ' acquire a write lock on the resource
                    rwlSendMessage.AcquireWriterLock(5000)  ' 10/27/07

                    ' were we successful in getting the lock?
                    If rwlSendMessage.IsWriterLockHeld Then
                        Dim encoder As UTF8Encoding = New UTF8Encoding()
                        Dim encBytes As Byte() = encoder.GetBytes(sb.ToString)
                        oAsyncConnectionInfo.socket.Send(encBytes, encBytes.Length, SocketFlags.None)
                    Else
                        ' no,
                        Throw New System.Exception("unable to acquire writer lock after 5 seconds")
                    End If

                Finally
                    ' release write lock on the resource
                    rwlSendMessage.ReleaseWriterLock()
                End Try
                ' 10/27/07 end changes

                ' save the time it took to do this method
                ' 10/27/07 oStopWatch.Stop()
                ' 10/27/07 PM_SendMessage.SendMessageTime = oStopWatch.Elapsed
                ' 10/27/07 oStopWatch = Nothing

                ' save the length of the message sent
                ' 10/27/07 PM_SendMessage.MessageLength = Message.Length

                ' save the message identifier for the message
                ' 10/27/07 PM_SendMessage.MessageIdentifier = _MessageIdentifier

                Return True

            Catch ex As Exception
                EventLog.WriteEntry("CWTCPbase", "SendMessage: exception: " & ex.InnerException.ToString & ", " & ex.Message.ToString, EventLogEntryType.Warning)  ' 08/03/07
                Return False

            Finally
                ' 10/27/07 SendMessageBUSY = False

            End Try
        End Function

        Public Overridable Sub Dispose() Implements IDisposable.Dispose
            Try
                oRSAServices = Nothing
                oDESServices = Nothing

                ' indicate we are being disposed. thus when we loose the TCP
                ' connection with server, we will not think it was an error
                mObjectDisposed = True      ' 08/03/07

            Catch ex As Exception

            End Try
        End Sub

        Public Sub New()
            MyBase.New()

            Try
                ' create our objects for encryption services
                oRSAServices = New RSAServices
                oDESServices = New DESServices

                ' TEST ONLY
                ' convert from Base64 Unicode to 8-bit bytes
                'Dim bNoError As Boolean = False
                'Dim sErrorMessage As String = String.Empty
                'Dim _message As String = "now is the time for all good programmers"
                '_message = Base64Encode(_message)
                '_message = Base64Decode(_message, bNoError, sErrorMessage)
                '_message = String.Empty

                ' TEST 2
                ' encrypt some data into a Base64 string
                ' encrypt using triple DES with CBC, output will be hex display characters
                Dim message As String = "hello world, how are you?"
                Dim cipherText As String = String.Empty
                Dim clearText As String = String.Empty

                If oDESServices.EncryptOrDecryptString(CRYPTO_ACTION.Encrypt, DES_TYPE.TripleDES, System.Security.Cryptography.CipherMode.CBC, message, cipherText, "9DB46389D60E41BA", "96E2A133766513F1", "11C36B85EF985B07", " ", OUTPUT_FORMAT.Base64) Then
                    message = cipherText
                End If

                ' convert from Base64 Unicode to 8-bit bytes
                'Dim bNoError As Boolean = False
                'Dim sErrorMessage As String = String.Empty
                'message = Base64Decode(message, bNoError, sErrorMessage)

                ' decrypt the message using our session keys
                If oDESServices.EncryptOrDecryptBase64String(CRYPTO_ACTION.Decrypt, DES_TYPE.TripleDES, System.Security.Cryptography.CipherMode.CBC, message, clearText, "9DB46389D60E41BA", "96E2A133766513F1", "11C36B85EF985B07", " ", OUTPUT_FORMAT.ASCIIDisplay) Then
                    message = clearText
                End If

            Catch ex As Exception
                EventLog.WriteEntry("CWTCPbase", "New: " & ex.Message.ToString, EventLogEntryType.Error)

            End Try
        End Sub

    End Class

    Public Class ConnectionInfo
        Public socket As Socket
    End Class

    Public Class AsyncConnectionInfo
        ' this defines the object that will be created for each TCP connection
        Inherits ConnectionInfo
        Public Buffer As Byte()                         ' data received from TCP client
        Public ClientID As String = String.Empty        ' IP/Port number identifier for the connection
        Public SessionActive As Boolean = False         ' TRUE = indicates we are in session with CW server
        Public SessionID As String = String.Empty       ' CW session ID
        Public SessionKeys As String = String.Empty     ' session encryption keys 3DES with CBC
        Public PublicKey As String = String.Empty       ' server's public RSA key
        Public DefaultEncryption As TCPBase.ENCRYPTION_OPTION = TCPBase.ENCRYPTION_OPTION.None ' default encryption option (none, session)

        Public Const BufferSize As Integer = 1024 * 32  ' Size of receive buffer. 32K 
        Public sb As New StringBuilder                  ' Received data string
        Public MessageLength As Integer = 0             ' size used to monitor continued massage sizes                 
    End Class

End Namespace