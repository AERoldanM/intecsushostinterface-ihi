﻿Imports System.Xml
Imports System.IO
Imports System.Configuration
Imports Intecusus.OperarBD
Imports Intecsus.Entidades

''' <summary>
''' Clase encargada de convertir Respuestas de CW
''' </summary>
''' <remarks>AERm 02-02-2015</remarks>
Public Class Respuesta

    Dim operacionesBD As OperacionesBase = New OperacionesBase()
    Dim xpath As String
    Dim xn As XmlNode
    Dim m_Estados As List(Of EstadoCW) = Nothing
    Dim m_EstadosXImpresora As List(Of EstadoXImpresora) = Nothing

    ''' <summary>
    ''' Metodo encargado de procesar la repsuesta de una session
    ''' </summary>
    ''' <param name="respuesta">Respuesta obtenida</param>
    ''' <returns>Retorna la nueva session</returns>
    ''' <remarks>AERM 02-02-2015</remarks>
    Public Function ProcesarSession(ByRef respuesta As String) As String
        Try
            Dim m_doc As XmlDocument = Me.ProcessResponse(respuesta)
            xpath = "//SessionID"
            xn = m_doc.SelectSingleNode(xpath)
            'operacionesBD.GuardarLogXNombreConectionString(2, 0, 1, "Respuesta - ProcesarSession : " + xn.InnerText,
            '                                             ConfigurationManager.AppSettings("BD"))
            Return xn.InnerText
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(2, 0, 2, "Respuesta - ProcesarSession : " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
        End Try

        Return String.Empty
    End Function

    ''' <summary>
    ''' Metodo encargado de procesar la respuesta de una solicitud de esta card device
    ''' </summary>
    ''' <param name="respuesta">Respuesta obtenida</param>
    ''' <returns>Retorna la nueva session</returns>
    ''' <remarks>AERM 10-02-2015</remarks>
    Public Function ProcesarEstadoCardDevice(ByRef respuesta As String, idTransaccion As Int32) As RespuestaEstado
        Dim m_respuesta As RespuestaEstado = New RespuestaEstado()
        Try
            Dim m_doc As XmlDocument = Me.ProcessResponse(respuesta, idTransaccion)
            Dim m_ReturnCodigo As String
            Dim m_DeviceType As String
            Dim m_DeviceStatus As String
            Dim m_Estado As Integer
            Dim m_Evento As Integer = 1

            xpath = "//ReturnCode  "
            xn = m_doc.SelectSingleNode(xpath)
            m_ReturnCodigo = xn.InnerText

            xpath = "//DeviceStatus"
            xn = m_doc.SelectSingleNode(xpath)
            'm_Estado = CInt("0" & xn.InnerText)
            m_DeviceStatus = xn.InnerText

            xpath = "//DeviceType "
            xn = m_doc.SelectSingleNode(xpath)
            m_DeviceType = xn.InnerText

            Me.BuscarEstadoImpresoraXBD(idTransaccion)
            m_Estado = Me.BuscarEstadoImpresora(m_ReturnCodigo, m_DeviceType, m_DeviceStatus, idTransaccion)
            ''Antes 1 -4  y 0, 2, 3, 5 'Impresora desconectada, apagada, no lista (Not Ready)
            Select Case m_Estado
                Case 101
                    m_respuesta.estado = ESTADO_REALCE.ST_OK
                    m_respuesta.status = "OK"
                Case 102
                    m_respuesta.estado = ESTADO_REALCE.ST_OCUPADA
                    m_respuesta.status = "BUSY"
                    m_Evento = 2
                Case 103
                    m_respuesta.estado = ESTADO_REALCE.ST_ERROR
                    m_respuesta.status = "ERROR"
                    m_Evento = 2
            End Select

            xpath = "//DeviceDescription"
            xn = m_doc.SelectSingleNode(xpath)
            m_respuesta.mensaje = xn.InnerText

            xpath = "//DeviceLocation"
            xn = m_doc.SelectSingleNode(xpath)
            m_respuesta.mensaje += "  " + xn.InnerText + "@" + m_DeviceStatus + m_DeviceType

            ''operacionesBD.GuardarLogXNombreConectionString(8, 0, m_Evento, "Respuesta - ProcesarEstadoCardDevice : " + m_respuesta.status + " " + m_respuesta.mensaje,
            ''                                             ConfigurationManager.AppSettings("BD"))

        Catch ex As Exception
            m_respuesta.estado = ESTADO_REALCE.ST_ERROR
            m_respuesta.status = ex.Message
            operacionesBD.GuardarLogXNombreConectionString(8, idTransaccion, 2, "Respuesta - ProcesarEstadoCardDevice : " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
        End Try

        Return m_respuesta
    End Function

    ''' <summary>
    ''' Metodo encargado de procesar un mensaje enviado por el servidor CW
    ''' </summary>
    ''' <param name="respuesta">Respuesta enviado por el sevirdor</param>
    ''' <returns>Respuesta el objeto RespuestaDeviceStatus  </returns>
    ''' <remarks>AERM 16/03/2015</remarks>
    Public Function ProcesarMensage(respuesta As String) As RespuestaDeviceStatus
        Dim m_respuesta As RespuestaDeviceStatus = New RespuestaDeviceStatus()
        Dim m_CodigoError As String = String.Empty
        m_respuesta.Transaccion = 0
        Try
            Dim m_doc As XmlDocument = Me.ObtenerDOC(respuesta)
            ''Proceso nuevo
            m_respuesta.Estado = InterpretHostMessage(m_doc, m_respuesta.hostIdentifier, m_respuesta.Mensaje, m_respuesta.MessageType, m_respuesta.DataInteger)
            If (m_respuesta.MessageType = "0" Or m_respuesta.MessageType = "11") Then
                m_CodigoError = m_respuesta.DataInteger
            Else
                m_CodigoError = m_respuesta.MessageType + m_respuesta.DataInteger
            End If

            Me.ObtenerMensajeCliente(m_respuesta, m_CodigoError)

            'TODO cerar función que elimine el request y llamarla desde aquí si el estadoRealce = ST_ERROR_DELETE
            If m_respuesta.Estado = ESTADO_REALCE.ST_ERROR_DELETE Then
                xpath = "//RequestCounter"
                xn = m_doc.SelectSingleNode(xpath)
                m_respuesta.Transaccion = If(IsNumeric(xn.InnerText), CInt(xn.InnerText), 0)
            End If

            operacionesBD.GuardarLogXNombreConectionString(7, If(String.IsNullOrEmpty(m_respuesta.hostIdentifier), 0, m_respuesta.hostIdentifier), 1, "Respuesta - RespuestaDeviceStatus Msj del servidor: hostIdentifier: " + m_respuesta.hostIdentifier + " Request Counter " + m_respuesta.Transaccion +
                                                          " estado=" + m_respuesta.Estado.ToString + " - " + m_respuesta.Mensaje, ConfigurationManager.AppSettings("BD"))


        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(7, m_respuesta.Transaccion, 2, "Respuesta - ProcesarMensage : " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
        End Try

        Return m_respuesta
    End Function

    ''' <summary>
    ''' Metodo encargado de procesar la respuesta paravalidar q no tenga error.
    ''' </summary>
    ''' <param name="respuesta">Respuesta obtenida</param>
    ''' <returns>Retorna si se cerro bien</returns>
    ''' <remarks>AERM 02-02-2015</remarks>
    Public Function ProcesarRespuestaValida(ByRef respuesta As String, idTransaccion As Int32) As Boolean
        Try
            If Me.ProcessResponse(respuesta, idTransaccion) Is Nothing Then
                Return False
            End If
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(2, idTransaccion, 2, "Respuesta - ProcesarRespuestaValida : " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
            Return False
        End Try

        Return True
    End Function

    ''' <summary>
    ''' Processes the xml response from the server and displays the proper information to the output boxes.
    ''' </summary>
    ''' <param name="sResponse">The response to process</param>
    Public Function ProcessResponse(ByRef sResponse As String, Optional ByRef idTransaccion As Int32 = 0, Optional ByRef transaccionName As String = "") As XmlDocument
        ' process the server's response to our request

        Dim iReturnCode As Int32
        ' if no data, exit early
        If sResponse.Length = 0 Then Return Nothing
        operacionesBD.GuardarLogXNombreConectionString(4, idTransaccion, 1, "OperacionesBaseCW - Respuesta-ProcessResponse  : " + sResponse, ConfigurationManager.AppSettings("BD"))
        ' Create an Xml document instance and load XML data.
        Dim doc As XmlDocument = New XmlDocument

        ' convert the string (sResponse) to a memory stream
        Dim buffer As New MemoryStream(System.Text.Encoding.ASCII.GetBytes(sResponse))
        Dim mMethodName As String

        Try
            ' now load the XML document
            doc.Load(buffer)

            ' get the method name
            xpath = "//METHOD"
            xn = doc.SelectSingleNode(xpath)
            mMethodName = xn.Attributes("name").Value
            transaccionName = mMethodName
            ' get the return code element (but only if method name is not "EventMessage")
            If mMethodName = "EventMessage" Then
                iReturnCode = 0
            Else
                xpath = "//ReturnCode"
                xn = doc.SelectSingleNode(xpath)

                If TypeName(xn) = "Nothing" Then
                    ' return code element is missing
                    Throw New Exception("ERROR missing return code in response " + xn.InnerText)
                    Exit Function
                End If

                iReturnCode = CInt(xn.InnerText)
            End If

            Select Case iReturnCode
                Case 0
                    Return doc
                Case Else
                    Select Case mMethodName
                        Case "HostGetMessage"
                            Select Case iReturnCode
                                Case 14         ' no messages
                                    Return Nothing
                                Case Else
                                    Throw New Exception("Error returned from server: code=" & iReturnCode.ToString & ", for method=" & mMethodName & ".  See CWEServer.log for more information.")
                            End Select

                        Case Else
                            Throw New Exception("Error returned from server: code=" & iReturnCode.ToString & ", for method=" & mMethodName & ".  See CWEServer.log for more information.")

                    End Select
            End Select

        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(4, idTransaccion, 2, "OperacionesBaseCW - Respuesta- ProcessResponse : " + ex.Message,
                                                           ConfigurationManager.AppSettings("BD"))
            Throw ex
        End Try

        Return Nothing
    End Function

    ''' <summary>
    ''' Metodo encargado de obtener el codigo y mensaje para el cliente
    ''' </summary>
    ''' <param name="m_respuesta">Retorna respuesta del cliente</param>
    ''' <remarks>AERM 27/08/2015</remarks>
    Public Sub ObtenerMensajeCliente(ByRef m_respuesta As RespuestaDeviceStatus, ByVal m_Codigo As String)
        Try
            Dim m_Mensaje = m_respuesta.Mensaje
            Dim m_RespuestaString As List(Of String)
            m_RespuestaString = operacionesBD.ObtenerMensajeConversion(m_Mensaje, ConfigurationManager.AppSettings("BD"))
            If (m_RespuestaString.Count >= 2) Then
                m_respuesta.Mensaje = m_RespuestaString(0) + " - " + m_Mensaje
                m_respuesta.DataInteger = m_RespuestaString(1)
            Else
                m_RespuestaString = operacionesBD.ObtenerMensaje(m_Codigo, ConfigurationManager.AppSettings("BD"))
                If (m_RespuestaString.Count >= 2) Then
                    m_respuesta.Mensaje = m_RespuestaString(0) + " - " + m_Mensaje
                    m_respuesta.DataInteger = m_RespuestaString(1)
                Else
                    m_RespuestaString = operacionesBD.ObtenerMensaje("300", ConfigurationManager.AppSettings("BD"))
                    m_respuesta.Mensaje = m_RespuestaString(0) + " - " + m_Mensaje
                    m_respuesta.DataInteger = m_RespuestaString(1)
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#Region "Metodos Privados"

    ''' <summary>
    ''' Helper function for ProcessResponse().  This function decodes Host and Event messages.
    ''' </summary>
    ''' <param name="doc">The XmlDocument containing the message received from the server</param>
    ''' <returns>The readable string that describes the message</returns>
    Private Function InterpretHostMessage(ByRef doc As XmlDocument, ByRef hostIdentifier As String, ByRef mensaje As String, ByRef m_MessageType As String, ByRef m_DataInteger As String) As Integer

        Dim xpath As String
        Dim xn As XmlNode
        Dim sMessage As String = String.Empty
        Dim sHostIdentifier As String = String.Empty
        Dim iMessageType As MSG_TO_CLIENT
        Dim iDataInteger As Integer
        Dim iEstadoRealce As Integer = 0
        Dim sDataString As String
        'Dim sIdentifier As String
        'Dim iRequestCounter As Integer

        Try
            ' get "HostIdentifier"
            xpath = "//HostIdentifier"
            xn = doc.SelectSingleNode(xpath)
            If TypeName(xn) <> "Nothing" Then
                sHostIdentifier = xn.InnerText
            Else
                sHostIdentifier = "0"
            End If

            hostIdentifier = sHostIdentifier
            ' get "MessageType"
            xpath = "//MessageType"
            xn = doc.SelectSingleNode(xpath)
            iMessageType = CInt("0" & xn.InnerText)
            m_MessageType = xn.InnerText


            ' get "DataInteger"
            xpath = "//DataInteger"
            xn = doc.SelectSingleNode(xpath)
            iDataInteger = CInt("0" & xn.InnerText)
            m_DataInteger = iDataInteger

            ' get "DataString"
            xpath = "//DataString"
            xn = doc.SelectSingleNode(xpath)
            sDataString = GetNodeContents(xn)
            mensaje = sDataString.ToString
            iEstadoRealce = Me.EstadoCWXBD(iMessageType, iDataInteger, sHostIdentifier)

            Return iEstadoRealce
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(7, 0, 2, "respuesta - InterpretHostMessage : " + ex.Message,
                                                               ConfigurationManager.AppSettings("BD"))
            Return ESTADO_REALCE.ST_ERROR
        End Try

    End Function

    ''' <summary>
    ''' This function returns the contents of the xmlNode (InnerText).  If the contents are encrypted we will decrypt them.
    ''' </summary>
    ''' <param name="node">The node to get the contents of.</param>
    ''' <returns>The decrypted inner text.</returns>
    Private Function GetNodeContents(ByVal node As Xml.XmlNode) As String

        Dim decodingNoError As Boolean
        Dim decodingMsg As String = ""
        Dim retVal As String = ""

        ' make sure the node exists
        If Not node Is Nothing Then

            ' get the contents of the node
            retVal = node.InnerText

            ' see if we need to decrypt the contents
            If LCase(SafeSelectAttributeText(node, "encoding")) = "base64" Then
                retVal = Base64UnicodeDecode(retVal, decodingNoError, decodingMsg)
                If decodingNoError = False Then
                    Throw New System.Exception("Error decoding base64 node contents.  Error message: " & decodingMsg)
                End If
            ElseIf LCase(SafeSelectAttributeText(node, "encoding")) = "base64net" Then
                retVal = Base64ASCIIDecode(retVal, decodingNoError, decodingMsg)
                If decodingNoError = False Then
                    Throw New System.Exception("Error decoding base64NET node contents.  Error message: " & decodingMsg)
                End If
            End If

        End If

        Return retVal

    End Function

    ''' <summary>
    ''' Safely retrieves the text value of an attribute from the inpputed sourceNode.
    ''' </summary>
    ''' <param name="sourceNode">The node to get the attribute from</param>
    ''' <param name="attrName">The name of the attribute to look for</param>
    ''' <returns>The inner text of the attribute</returns>
    Private Function SafeSelectAttributeText(ByVal sourceNode As Xml.XmlNode, ByVal attrName As String) As String

        Dim attr As Xml.XmlAttribute
        Dim retVal As String = ""

        If Not sourceNode Is Nothing Then
            attr = sourceNode.Attributes(attrName)
            If Not attr Is Nothing Then
                retVal = attr.InnerText
            End If
        End If

        Return retVal

    End Function

    ''' <summary>
    ''' Does base64 decoding on the string "sInput" and returns the result
    ''' </summary>
    ''' <param name="sInput">The string to decode</param>
    ''' <param name="noError">If we run into an error this argument will be set to False</param>
    ''' <param name="errorMessage">If noError is returned False this argument will hold the error description</param>
    ''' <returns>The decoded string</returns>
    Private Function Base64UnicodeDecode(ByVal sInput As String, ByRef noError As Boolean, ByRef errorMessage As String) As String

        ' Convert the Base64 UUEncoded input into binary output.
        Dim binaryData() As Byte
        Dim sb As New System.Text.StringBuilder

        Try
            noError = True
            errorMessage = String.Empty

            ' convert the data
            binaryData = System.Convert.FromBase64String(sInput)

            ' return the decoded data to caller
            Return System.Text.Encoding.Unicode.GetString(binaryData)

        Catch exp As System.ArgumentNullException
            errorMessage = "Base 64 string is null."
            noError = False
            Return String.Empty

        Catch exp As System.FormatException
            errorMessage = "Base 64 length is not 4 or is not an even multiple of 4."
            noError = False
            Return String.Empty

        End Try

    End Function

    ''' <summary>
    ''' Does base64 decoding on the string "sInput" and returns the result
    ''' </summary>
    ''' <param name="sInput">The string to decode</param>
    ''' <param name="noError">If we run into an error this argument will be set to False</param>
    ''' <param name="errorMessage">If noError is returned False this argument will hold the error description</param>
    ''' <returns>The decoded string</returns>
    Private Function Base64ASCIIDecode(ByVal sInput As String, ByRef noError As Boolean, ByRef errorMessage As String) As String

        ' Convert the Base64 UUEncoded input into binary output.
        Dim binaryData() As Byte
        Dim sb As New System.Text.StringBuilder

        Try
            noError = True
            errorMessage = String.Empty

            ' convert the data
            binaryData = System.Convert.FromBase64String(sInput)

            ' return the decoded data to caller
            Return System.Text.Encoding.ASCII.GetString(binaryData)

        Catch exp As System.ArgumentNullException
            errorMessage = "Base 64 string is null."
            noError = False
            Return String.Empty

        Catch exp As System.FormatException
            errorMessage = "Base 64 length is not 4 or is not an even multiple of 4."
            noError = False
            Return String.Empty

        End Try

    End Function

    ''' <summary>
    ''' Metodo encargado de ejecutar el proceso de carga de estados de la BD 
    ''' </summary>
    ''' <param name="idTransaccion">Identificador de la transaccion</param>
    ''' <remarks>AERM 20/06/2016</remarks>
    Private Sub BuscarEstadoImpresoraXBD(idTransaccion As Int32)
        Try

            If (m_EstadosXImpresora Is Nothing) Then
                If (EstadosXImpresora.Instancia(Nothing).obtenerEstados Is Nothing) Then
                    m_EstadosXImpresora = operacionesBD.ObtenerEstadosXImrpesoraCW(ConfigurationManager.AppSettings("BD"))
                Else
                    m_EstadosXImpresora = EstadosXImpresora.Instancia(Nothing).obtenerEstados
                End If
            End If
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(7, idTransaccion, 2, "respuesta - BuscarEstadoImpresoraXBD : " + ex.Message,
                                                             ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Obtiene los estados cargados por BD si no estan y se encarga de retornar el estado FINAL CW segun su respuesta
    ''' </summary>
    ''' <param name="iMessageType">Tipo de mensaje retornado por CW en el XML con el mismo nombre  </param>
    ''' <param name="iDataInteger">COdigo especifico de CW en el XML con el mismo nombre</param>
    ''' <param name="sHostIdentifier">Id transaccion para CW</param>
    ''' <returns>Retorna el estado de la transaccion</returns>
    ''' <remarks>AERM 25/05/2016</remarks>
    Private Function EstadoCWXBD(iMessageType As Integer, iDataInteger As Integer, sHostIdentifier As String) As Integer
        Dim idTransaccion = 0
        Try

            If (String.IsNullOrEmpty(sHostIdentifier) = False) Then
                idTransaccion = Convert.ToInt32(sHostIdentifier)
            End If

            If (m_Estados Is Nothing) Then
                If (EstadosCW.Instancia(Nothing).obtenerEstados Is Nothing) Then
                    m_Estados = operacionesBD.ObtenerEstadosXCodigoCW(ConfigurationManager.AppSettings("BD"))
                Else
                    m_Estados = EstadosCW.Instancia(Nothing).obtenerEstados
                End If
            End If

            Return Me.ObtenerEstado(m_Estados, iMessageType.ToString(), iDataInteger.ToString(), False)
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(7, idTransaccion, 2, "respuesta - EstadoCWXBD : " + ex.Message,
                                                             ConfigurationManager.AppSettings("BD"))
            Return 3
        End Try
        Return 0
    End Function

    ''' <summary>
    ''' Obtiene el estado final de CW de una lista de estados
    ''' </summary>
    ''' <param name="m_Estados">Lista de estados</param>
    ''' <param name="iMessageType">Tipo de mensaje retornado por CW en el XML con el mismo nombre, si el valor de final busqueda es true este
    ''' es la union del  iMessageType e iDataInteger </param>
    ''' <param name="iDataInteger">COdigo especifico de CW en el XML con el mismo nombre, si el siguiente valor es true este se convierte en el 
    ''' iMessageType original</param>
    ''' <param name="finalBusqueda">Indica si ya se realizo la busqueda solo por iMessageType</param>
    ''' <returns>Retorna estado FInal para la respuesta CW</returns>
    ''' <remarks>AERM 25/05/2016</remarks>
    Private Function ObtenerEstado(m_Estados As List(Of EstadoCW), iMessageType As String, iDataInteger As String, finalBusqueda As Boolean) As Integer
        Try
            Dim m_EstadoFINAL As EstadoCW = (From es In m_Estados
                          Where es.IdMessageType = iMessageType And es.IdDataInteger = iDataInteger
                          Select es).FirstOrDefault()

            If (m_EstadoFINAL Is Nothing) Then
                If (finalBusqueda = False) Then
                    Return Me.ObtenerEstado(m_Estados, iMessageType, "*", True)
                Else
                    Return Me.ObtenerEstado(m_Estados, "*", "*", True)
                End If
            End If

            Dim m_EstadoRealce As ESTADO_REALCE = CType(m_EstadoFINAL.estadoFinalCW, ESTADO_REALCE)

            If (m_EstadoRealce = ESTADO_REALCE.ST_NOPROCESO) Then
                If (finalBusqueda = False) Then
                    Return Me.ObtenerEstado(m_Estados, iMessageType.ToString(), "*", True)
                Else
                    Throw New Exception("Para el siguiente valor " + iMessageType + " El estado es ST_NOPROCESO (Definir otro estado)")
                End If
            End If

            Return m_EstadoFINAL.estadoFinalCW
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' ENviar al metodo que realiza el proceso de busqueda de la respuesta segun los estados obtenidos
    ''' 101,'OK'
    ''' 102,'OCUAPDA O NO AUMENTAR REINTENTOS'
    ''' 103,'AGREGAR A REINTENTOS'
    ''' </summary>
    ''' <param name="m_ReturnCodigo">Parametro XML de la respuesta de CW</param>
    ''' <param name="m_DeviceType">Parametro XML de la respuesta de CW</param>
    ''' <param name="m_DeviceStatus">Parametro XML de la respuesta de CW</param>
    ''' <param name="idTransaccion">Identificador de la transaccion</param>
    ''' <returns>Estado de la transaccion</returns>
    ''' <remarks>AERM 20/06/2016</remarks>
    Private Function BuscarEstadoImpresora(m_ReturnCodigo As String, m_DeviceType As String, m_DeviceStatus As String, idTransaccion As Integer) As Integer
        Try

            Dim estado As EstadoXImpresora = Me.ObtenerEstadoXImpresora(m_EstadosXImpresora, m_ReturnCodigo, "", "")
            If (estado Is Nothing) Then
                estado = Me.ObtenerEstadoXImpresora(m_EstadosXImpresora, m_ReturnCodigo, m_DeviceType, "")
                If (estado Is Nothing) Then
                    estado = Me.ObtenerEstadoXImpresora(m_EstadosXImpresora, m_ReturnCodigo, m_DeviceType, m_DeviceStatus)
                    If (estado Is Nothing) Then
                        estado = Me.ObtenerEstadoXImpresora(m_EstadosXImpresora, "*", "*", "*") ''Generico
                    End If
                End If
            End If
            Return estado.estadoFinalCW
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(7, idTransaccion, 2, "respuesta - BuscarEstadoImpresora : " + ex.Message,
                                                            ConfigurationManager.AppSettings("BD"))
            Return 103
        End Try
    End Function

    ''' <summary>
    ''' OBtiene el estado de una impresora segun respuesta CW
    ''' </summary>
    ''' <param name="m_Estados">Lista de estados que puede tomar</param>
    ''' <param name="m_ReturnCodigo">Parametro XML de la respuesta de CW</param>
    ''' <param name="m_DeviceType">Parametro XML de la respuesta de CW</param>
    ''' <param name="m_DeviceStatus">Parametro XML de la respuesta de CW</param>
    ''' <returns>Retorna el estado segun codigo</returns>
    ''' <remarks>AERM 20/06/2016</remarks>
    Private Function ObtenerEstadoXImpresora(m_Estados As List(Of EstadoXImpresora), m_ReturnCodigo As String, m_DeviceType As String, m_DeviceStatus As String) As EstadoXImpresora
        Try
            Dim m_EstadoFINAL As EstadoXImpresora = (From es In m_Estados
                                                    Where es.ReturnCode = m_ReturnCodigo And es.DeviceType = m_DeviceType And es.DeviceStatus = m_DeviceStatus
                                                    Select es).FirstOrDefault()

            Return m_EstadoFINAL

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Metodo encargado convertir en XML una respuesta de CW
    ''' </summary>
    ''' <param name="sResponse">Respuesta de CW</param>
    ''' <returns>Retorna Respuesta de CW</returns>
    ''' <remarks>AERM 25/05/2016</remarks>
    Private Function ObtenerDOC(sResponse As String) As XmlDocument
        Try
            ' if no data, exit early
            If sResponse.Length = 0 Then Return Nothing
            operacionesBD.GuardarLogXNombreConectionString(4, 0, 1, "OperacionesBaseCW - Respuesta- ObtenerDOC : " + sResponse, ConfigurationManager.AppSettings("BD"))
            ' Create an Xml document instance and load XML data.
            Dim doc As XmlDocument = New XmlDocument

            ' convert the string (sResponse) to a memory stream
            Dim buffer As New MemoryStream(System.Text.Encoding.ASCII.GetBytes(sResponse))
            ' now load the XML document
            doc.Load(Buffer)
            Return doc
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(4, 0, 2, "OperacionesBaseCW - Respuesta- ObtenerDOC : " + ex.Message,
                                                           ConfigurationManager.AppSettings("BD"))
            Throw ex
        End Try

        Return Nothing
    End Function
#End Region

End Class
