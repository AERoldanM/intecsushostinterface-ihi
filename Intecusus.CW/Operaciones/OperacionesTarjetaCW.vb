﻿Imports Intecusus.OperarBD
Imports System.Configuration
Imports Intecsus.CW.TCP
Imports Intecsus.Entidades
Imports Intecsus.OperarXML
Imports System.Xml

Partial Public Class OperacionesBaseCW
    Implements IOperacion

    Public Function HacerTarjeta(ByVal servidor As [String], ByVal puerto As Int32, ByVal respuesta As String, ByVal usuario As String, ByVal session As String, ByVal idTransaccion As String,
                          ByVal interfaceUsar As String, ByVal informacion As DatosCardWizard) As Boolean Implements IOperacion.HacerTarjeta
        Try
            Dim m_peticion As String = String.Empty
            Dim m_parametros As List(Of ParametrosXML) = New List(Of ParametrosXML)
            m_Parametros.Add(Me.m_GenerarXML.GenerarParametro("SessionID", "string", session))
            m_parametros.Add(Me.m_GenerarXML.GenerarParametro("RequestorPCName", "string", informacion.PCName))
            m_parametros.Add(Me.m_GenerarXML.GenerarParametro("RequestorName", "string",
                                                                If(String.IsNullOrEmpty(informacion.Usuario) = True, usuario, informacion.Usuario)))
            ''Id transaccion 
            m_parametros.Add(Me.m_GenerarXML.GenerarParametro("HostIdentifier", "string", idTransaccion.ToString()))
            m_parametros.Add(Me.m_GenerarXML.GenerarParametro("CardFormatName", "string", informacion.CardWizardName))

            For Each m_param As ParametrosXML In informacion.Parametros
                m_parametros.Add(m_param)
            Next

            m_peticion = m_GenerarXML.CrearXML(interfaceUsar, "HostSubmitCardMake", m_parametros)
            operacionesBD.GuardarLogXNombreConectionString(5, idTransaccion, 1, "OperacionesBaseCW - HacerTarjeta : " + m_GenerarXML.CrearXML(interfaceUsar, "HostSubmitCardMake", m_parametros, True),
                                                          ConfigurationManager.AppSettings("BD"))
            If (Me.EnviarSolicitudTOServidor(servidor, puerto, m_peticion, idTransaccion, respuesta)) Then
                operacionesBD.CambiarEstado(idTransaccion, ConfigurationManager.AppSettings("BD"), ESTADO_REALCE.ST_EN_PROCESO.ToString())
                Return m_TransRespuesta.ProcesarRespuestaValida(respuesta, idTransaccion)
            End If
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(5, idTransaccion, 2, "OperacionesBaseCW - HacerTarjeta : " + ex.Message,
                                                           ConfigurationManager.AppSettings("BD"))
            operacionesBD.CambiarEstado(idTransaccion, ConfigurationManager.AppSettings("BD"), ESTADO_REALCE.ST_ERROR.ToString())
        End Try

        Return False
    End Function

    'Public Function Reintento(ByVal servidor As [String], ByVal puerto As Int32, ByVal idTransaccion As Integer, ByVal respuesta As String) As Boolean
    '    Try
    '        Dim m_peticion As String = operacionesBD.ObtenerProcesoXID(idTransaccion, ConfigurationManager.AppSettings("BD"))
    '        If (Me.EnviarSolicitudTOServidor(servidor, puerto, m_peticion, idTransaccion, respuesta)) Then

    '            Return m_TransRespuesta.ProcesarRespuestaValida(respuesta)
    '        End If
    '    Catch ex As Exception
    '        operacionesBD.GuardarLogXNombreConectionString(5, idTransaccion, 2, "OperacionesBaseCW - Reintento : " + ex.Message,
    '                                                       ConfigurationManager.AppSettings("BD"))
    '        operacionesBD.CambiarEstado(idTransaccion, ConfigurationManager.AppSettings("BD"), ESTADO_REALCE.ST_ERROR)
    '    End Try

    '    Return False
    'End Function

    Public Function EliminarTarjeta(servidor As String, puerto As Integer, respuesta As String, session As String, counter As String, idTransaccion As String,
                                    interfaceUsar As String) As Boolean Implements IOperacion.EliminarTarjeta
        Try
            Dim m_peticion As String = String.Empty
            Dim m_Parametros As List(Of ParametrosXML) = New List(Of ParametrosXML)()
           m_Parametros.Add(Me.m_GenerarXML.GenerarParametro("SessionID", "string", session))
            m_Parametros.Add(Me.m_GenerarXML.GenerarParametro("RequestCounter", "int", counter))

            m_peticion = m_GenerarXML.CrearXML(interfaceUsar, "HostCardRequestDelete", m_Parametros)
            operacionesBD.GuardarLogXNombreConectionString(6, idTransaccion, 1, "OperacionesBaseCW - EliminarTarjeta : " + m_peticion,
                                                          ConfigurationManager.AppSettings("BD"))
            If (Me.EnviarSolicitudTOServidor(servidor, puerto, m_peticion, idTransaccion, respuesta)) Then
                Return m_TransRespuesta.ProcesarRespuestaValida(respuesta, idTransaccion)
            End If

        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(6, idTransaccion, 2, "OperacionesBaseCW - EliminarTarjeta : " + ex.Message,
                                                           ConfigurationManager.AppSettings("BD"))
        End Try

        Return False
    End Function

    
    Function EstadoXPCName(ByVal servidor As [String], ByVal puerto As Int32, ByVal respuesta As String, ByVal session As String, ByVal pcName As String,
                           ByVal interfaceUsar As String, ByVal cardFormatName As String, ByVal idTransaccion As Integer, Optional ByVal bEncodeOnly As Boolean = False) As RespuestaEstado Implements IOperacion.EstadoXPCName
        Dim m_respuesta As RespuestaEstado = New RespuestaEstado()
        Try
            m_respuesta.estado = ESTADO_REALCE.ST_ERROR
            m_respuesta.status = "ERROR"
            Dim m_peticion As String = String.Empty
            Dim m_Parametros As List(Of ParametrosXML) = New List(Of ParametrosXML)()
            Dim m_EncodeOnly As String
            m_Parametros.Add(Me.m_GenerarXML.GenerarParametro("SessionID", "string", session))
            m_Parametros.Add(Me.m_GenerarXML.GenerarParametro("RequestorPCName", "string", pcName))
            m_Parametros.Add(Me.m_GenerarXML.GenerarParametro("CardFormatName", "string", cardFormatName))
            ' EncodeOnly
            If bEncodeOnly Then
                m_EncodeOnly = "Y"
            Else
                m_EncodeOnly = "N"
            End If

            m_Parametros.Add(Me.m_GenerarXML.GenerarParametro("EncodeOnly", "string", m_EncodeOnly))
            m_peticion = m_GenerarXML.CrearXML(interfaceUsar, "HostGetCardDeviceStatus", m_Parametros)
            operacionesBD.GuardarLogXNombreConectionString(8, idTransaccion, 1, "OperacionesBaseCW - EstadoXPCName : " + m_peticion,
                                                          ConfigurationManager.AppSettings("BD"))
            If (Me.EnviarSolicitudTOServidor(servidor, puerto, m_peticion, idTransaccion, respuesta)) Then
                Return m_TransRespuesta.ProcesarEstadoCardDevice(respuesta, idTransaccion)
            Else
                Throw New Exception("timed out waiting for response from server")
            End If
        Catch ex As Exception
            m_respuesta.estado = ESTADO_REALCE.ST_ERROR
            m_respuesta.status = "ERROR"
            m_respuesta.mensaje = ex.Message + "@" + "300"
            operacionesBD.GuardarLogXNombreConectionString(8, idTransaccion, 2, "OperacionesBaseCW - EstadoXPCName : " + ex.Message,
                                                           ConfigurationManager.AppSettings("BD"))
        End Try

        Return m_respuesta
    End Function

End Class
