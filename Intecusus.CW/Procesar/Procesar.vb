﻿Imports Intecsus.CW
Imports Intecsus.Entidades
Imports Intecusus.OperarBD
Imports System.Configuration
Imports Intecsus.MSMQ
Imports System.Messaging
Imports System.Threading
Imports Intecsus.OperarXML
Imports Intecsus.Seguridad

Public Class Procesar

#Region "Variables"
    Public puerto As Int32
    Public interfaceUsar As String
    Public servidor As String
    Private WithEvents operar As IOperacion = New OperacionesBaseCW()
    Private session As String
    Private operacionesBD As OperacionesBase = New OperacionesBase()
    Private cantidadReintentos As Integer
    Private usuarioCW As String
    Private passwordCW As String
    Private tiempoCerrarSession As Integer
    Private tiempoProceso As Integer
    Private tiempoReProceso As Integer
    Private fechaCreacionSession As DateTime
    Private colaRealce As String
    Private colaNotificacion As String
    Private operarCola As ICola = New InstanciaCola().IntanciarCola(ConfigurationManager.AppSettings("ColaInstancia"))
    Private impresoras As List(Of Impresoras) = New List(Of Impresoras)()
#End Region

#Region "Inicializar variables"

    ''' <summary>
    ''' Metodo encargado de inicializar as variables base para comenzar los procesos
    ''' </summary>
    ''' <remarks>AERM 16/03/2015</remarks>
    Public Sub InicilizarVariables()
        Try
            ''Datos CW Base
            servidor = operacionesBD.ObtenerValoresConfigurador("IpCW", ConfigurationManager.AppSettings("BD"))

            puerto = operacionesBD.ObtenerValoresConfigurador("PuertoCW", ConfigurationManager.AppSettings("BD"))
            session = String.Empty
            interfaceUsar = operacionesBD.ObtenerValoresConfigurador("InterfaceCW", ConfigurationManager.AppSettings("BD"))
            usuarioCW = operacionesBD.ObtenerValoresConfigurador("UsuarioCW", ConfigurationManager.AppSettings("BD"))
            passwordCW = Me.ObtenerContrasena()
            cantidadReintentos = Convert.ToInt32(operacionesBD.ObtenerValoresConfigurador("CantReintentos", ConfigurationManager.AppSettings("BD")))
            ''Variable tiempos
            tiempoCerrarSession = Convert.ToInt32(operacionesBD.ObtenerValoresConfigurador("CerrarSessionEnHoras", ConfigurationManager.AppSettings("BD")))
            tiempoProceso = Convert.ToInt32(operacionesBD.ObtenerValoresConfigurador("EjecucionProcesoMiliSegundos", ConfigurationManager.AppSettings("BD")))
            tiempoReProceso = Convert.ToInt32(operacionesBD.ObtenerValoresConfigurador("EjecucionReIntentosCWSegundos", ConfigurationManager.AppSettings("BD")))
            ''Colas
            colaRealce = operacionesBD.ObtenerValoresConfigurador("ColaRealce", ConfigurationManager.AppSettings("BD"))
            colaNotificacion = operacionesBD.ObtenerValoresConfigurador("ColaNotificacion", ConfigurationManager.AppSettings("BD"))
            'Carga estados X codigos CW
            operacionesBD.ObtenerEstadosXCodigoCW(ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(1, 0, 2, "Procesar.vb - InicilizarVariables : " + ex.Message,
                                                           ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de borrar las variables base para comenzar los procesos
    ''' </summary>
    ''' <remarks>AERM 25/05/2015</remarks>
    Public Sub BorrarVariables()
        Try

            If (String.IsNullOrEmpty(session) = False) Then
                operar.CerrarSession(servidor, Int32.Parse(puerto), String.Empty, session, "CW_XML_Interface")
            End If
            ''Datos CW Base
            servidor = String.Empty
            puerto = 0
            session = String.Empty
            interfaceUsar = String.Empty

            usuarioCW = String.Empty
            passwordCW = String.Empty
            cantidadReintentos = 0
            ''Variable tiempos
            tiempoCerrarSession = 0
            tiempoProceso = 0
            tiempoReProceso = 0
            ''Colas
            colaRealce = String.Empty
            colaNotificacion = String.Empty

        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(1, 0, 2, "Procesar.vb - BorrarVariables : " + ex.Message,
                                                           ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub
#End Region

#Region "Session"

    ''' <summary>
    ''' Metodo encargado de validar session 
    ''' </summary>
    ''' <remarks>AERM 17/03/2015</remarks>
    Public Sub CrearSession()
        Dim Estados As List(Of EstadoCW) = EstadosCW.Instancia(Nothing).obtenerEstados()
        Dim respuesta As String = String.Empty
        Dim fechaActual As DateTime = DateTime.Now
        Dim cambiarSession As Boolean = False
        Try
            If String.IsNullOrEmpty(session) Then
                cambiarSession = True
            Else
                Dim fechaFinal As Integer = DateDiff(DateInterval.Hour, fechaCreacionSession, fechaActual)
                If fechaFinal >= tiempoCerrarSession Then
                    cambiarSession = operar.CerrarSession(servidor, Int32.Parse(puerto), respuesta, session, interfaceUsar)
                End If
            End If

            If cambiarSession Then
                session = operar.CrearSession(servidor, Int32.Parse(puerto), respuesta, usuarioCW, passwordCW, "STD", interfaceUsar)
                fechaCreacionSession = DateTime.Now
            End If
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(2, 0, 2, "Procesar.vb - CrearSession : " + ex.Message,
                                                           ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

#End Region

#Region "Proceso"

    ''' <summary>
    ''' Metodo encargado de retornar el tiempo para comenzar los procesos
    ''' </summary>
    ''' <returns>Retorna Tiempo en segundos </returns>
    ''' <remarks>AERM 18/03/2015</remarks>
    Public Function RetornarTiempo() As Integer
        Return tiempoProceso
    End Function

    ''' <summary>
    ''' Metodo encargado de retornar el tiempo para comenzar los procesos
    ''' </summary>
    ''' <returns>Retorna Tiempo en segundos </returns>
    ''' <remarks>AERM 18/03/2015</remarks>
    Public Function RetornarTiempoTransaccion() As Integer
        Return Convert.ToInt32(operacionesBD.ObtenerValoresConfigurador("EntreTransaccionMiliSegundos", ConfigurationManager.AppSettings("BD")))
    End Function

    ''' <summary>
    ''' Metodo encargado de retonar la session
    ''' </summary>
    ''' <returns>Retorna la session creada</returns>
    ''' <remarks>AERM 09/04/2015</remarks>
    Public Function RetornarSession() As String
        Return session
    End Function

    ''' <summary>
    ''' Metodo usado para inicar el proceso de impresiones de tarjetas
    ''' </summary>
    ''' <remarks>AERM 18/03/2015</remarks>
    Public Sub ComenzarProceso()
        Dim m_IdTransaccion As Integer = 0
        Dim m_reencolar As List(Of DatosCardWizard) = New List(Of DatosCardWizard)
        Dim m_datosRealce As DatosCardWizard = New DatosCardWizard()
        Dim m_TiempoTransaccion As Boolean = False
        Try
            Dim m_respuestaEstado As RespuestaEstado
            Dim m_respuestaDoc As String = String.Empty
            While operarCola.RecibirObjetoCardWizard(colaRealce, m_datosRealce)
                ' operacionesBD.GuardarLogXNombreConectionString(5, 0, 1, "Procesar.vb - ComenzarProceso X ColaRealce " + m_datosRealce.ID_Transaccion.ToString(),ConfigurationManager.AppSettings("BD"))
                If Me.ProcesarCola(m_datosRealce) Then
                    m_IdTransaccion = m_datosRealce.ID_Transaccion
                    ''Si sucede alguna excepcion a cosultar la impresora vuelve a porner en la cola la petición
                    Try
                        m_respuestaEstado = operar.EstadoXPCName(servidor, Int32.Parse(puerto), m_respuestaDoc, session, m_datosRealce.PCName, interfaceUsar, m_datosRealce.CardWizardName, m_IdTransaccion)
                    Catch ex As Exception
                        m_reencolar.Add(m_datosRealce)
                        Throw ex
                    End Try

                    m_datosRealce.Mensaje = m_respuestaEstado.mensaje

                    If m_respuestaEstado.estado = ESTADO_REALCE.ST_OK Then
                        Me.RealizarImpresion(m_respuestaDoc, m_datosRealce)
                        Dim m_Impresora As Impresoras = New Impresoras()
                        m_Impresora.CardWizardName = m_datosRealce.CardWizardName
                        m_Impresora.pCName = m_datosRealce.PCName
                        m_Impresora.tiempo = DateTime.Now()
                        impresoras.Add(m_Impresora)

                    ElseIf m_respuestaEstado.estado = ESTADO_REALCE.ST_OCUPADA Then
                        Me.volverACola(m_datosRealce)
                    Else
                        m_reencolar.Add(m_datosRealce)
                    End If

                    operacionesBD.CambiarEstado(m_IdTransaccion, ConfigurationManager.AppSettings("BD"), "Impresion : " + m_respuestaEstado.mensaje + m_respuestaEstado.status)
                    ''operacionesBD.GuardarLogXNombreConectionString(5, m_IdTransaccion, 2, "Elimina error impresora : " + m_respuestaEstado.mensaje + m_respuestaEstado.status,
                    ''                                    ConfigurationManager.AppSettings("BD"))
                Else
                    Me.volverACola(m_datosRealce)
                End If

                m_IdTransaccion = 0
            End While
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(5, m_IdTransaccion, 2, "Procesar.vb - ComenzarProceso : " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
        Finally
            Me.AgregarReintentos(m_reencolar)
            Me.ProcesosEliminar()
            RaiseEvent TerminoProceso(Me, tiempoProceso)
        End Try
    End Sub
#End Region

#Region "Eventos"

    Event TerminoProceso As EventHandler(Of Integer)

    Event IntervaloProceso As EventHandler(Of Integer)
    ''' <summary>
    ''' Metodo sucede cuando se ejecuta el envento event message de tcpClient
    ''' </summary>
    ''' <param name="sender">Obejto del evento</param>
    ''' <param name="resp">Respuesta enviada por CW</param>
    ''' <remarks>AERM 18/03/2015</remarks>
    Private Sub Mensaje(ByVal sender As Object, ByVal resp As RespuestaDeviceStatus) Handles operar.RecibeMensaje
        Try
            Dim m_RespuestaDOC As String = String.Empty
            resp.EstadoF = 1
            Dim m_Transaccion As EstadoTransaccion = operacionesBD.ObtenerTransaccionXNotificar(ConfigurationManager.AppSettings("BD"), resp.hostIdentifier, False)
            operacionesBD.GuardarLogXNombreConectionString(10, resp.hostIdentifier, 1, "Procesar.vb - Mensaje : Transacción " + resp.TransaccionName + "- " + resp.Estado.ToString(),
                                                      ConfigurationManager.AppSettings("BD"))

            If m_Transaccion IsNot Nothing Then
                If resp.Estado = ESTADO_REALCE.ST_OK Then
                    resp.EstadoF = 0
                End If

                Me.GuardarRespuesta(resp, m_RespuestaDOC)
            End If
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(10, resp.hostIdentifier, 2, "Procesar.vb - Mensaje : " + ex.Message,
                                                         ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

#End Region

#Region "Metodos Privados"

    ''' <summary>
    ''' Metodo encargado de volver  aponer un registro en la cola
    ''' </summary>
    ''' <param name="m_datosRealce"> Objeto a encolaar </param>
    ''' <remarks>AERM 18/03/2015</remarks>
    Private Sub volverACola(m_datosRealce As DatosCardWizard)
        If operarCola.GuardarObjetoCardWizard(m_datosRealce, colaRealce) Then
            operacionesBD.CambiarEstado(m_datosRealce.ID_Transaccion, ConfigurationManager.AppSettings("BD"), "Se vuelve a en colar a la  impresora")
        Else
            operacionesBD.CambiarEstado(m_datosRealce.ID_Transaccion, ConfigurationManager.AppSettings("BD"), ESTADO_REALCE.ST_ERROR.ToString())
        End If
    End Sub

    ''' <summary>
    ''' Metodo encargado de realizar todo el proceso de enviar a imprimir
    ''' </summary>
    ''' <param name="m_respuestaDoc">String donde se almacena la respuesta de la peticion</param>
    ''' <param name="m_datosRealce">Obejto con datos de la impresora</param>
    ''' <remarks>AERM 18/03/2015</remarks>
    Private Sub RealizarImpresion(m_respuestaDoc As String, m_datosRealce As DatosCardWizard)
        Try
            Dim resultado = operar.HacerTarjeta(servidor, Int32.Parse(puerto), m_respuestaDoc, usuarioCW, session, m_datosRealce.ID_Transaccion,
                                 interfaceUsar, m_datosRealce)
            If resultado = False Then
                operacionesBD.CambiarEstado(m_datosRealce.ID_Transaccion, ConfigurationManager.AppSettings("BD"), ESTADO_REALCE.ST_ERROR.ToString())
                Dim m_respuesta As RespuestaDeviceStatus = Me.RespuestaMensaje(String.Empty, "300")
                m_respuesta.hostIdentifier = m_datosRealce.ID_Transaccion
                m_respuesta.EstadoF = 1
                m_respuesta.Estado = ESTADO_REALCE.ST_ERROR
                operarCola.GuardarObjetoNotificacion(m_respuesta, colaNotificacion)
            End If
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(5, m_datosRealce.ID_Transaccion, 2, "Procesar.vb - RealizarImpresion : " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de agregar a la cola los reintentos
    ''' </summary>
    ''' <param name="m_reencolar">Lista de datos a encolar</param>
    ''' <remarks>AERM 31/03/2015</remarks>
    Private Sub AgregarReintentos(m_reencolar As List(Of DatosCardWizard))
        Try
            For Each m_cola As DatosCardWizard In m_reencolar
                Try
                    If m_cola.Reintentos >= cantidadReintentos Then
                        operacionesBD.CambiarEstado(m_cola.ID_Transaccion, ConfigurationManager.AppSettings("BD"), "Supero cantidad de reintentos  " + m_cola.Reintentos.ToString())
                        Dim m_variables = m_cola.Mensaje.Split("@")
                        Dim m_respuesta As RespuestaDeviceStatus
                        If (m_variables.Count >= 2) Then
                            m_respuesta = Me.RespuestaMensaje(m_variables(0), m_variables(1))
                        Else
                            m_respuesta = Me.RespuestaMensaje(m_variables(0), "300")
                        End If

                        m_respuesta.hostIdentifier = m_cola.ID_Transaccion
                        m_respuesta.Estado = ESTADO_REALCE.ST_ERROR
                        m_respuesta.EstadoF = 1
                        operarCola.GuardarObjetoNotificacion(m_respuesta, colaNotificacion)
                        operacionesBD.GuardarLogXNombreConectionString(15, m_cola.ID_Transaccion, 1, "Procesar.vb - SuperoReintentos " + m_cola.Mensaje,
                                                              ConfigurationManager.AppSettings("BD"))
                    Else
                        m_cola.Reintentos += 1
                        Me.volverACola(m_cola)
                        operacionesBD.CambiarEstado(m_cola.ID_Transaccion, ConfigurationManager.AppSettings("BD"), "Se vuelve agregar a reintento  " + m_cola.Reintentos.ToString())
                        operacionesBD.GuardarLogXNombreConectionString(15, m_cola.ID_Transaccion, 1, "Procesar.vb - AgregarReintentos ",
                                                              ConfigurationManager.AppSettings("BD"))
                    End If

                Catch ex As Exception
                    operacionesBD.GuardarLogXNombreConectionString(15, m_cola.ID_Transaccion, 2, "Procesar.vb - AgregarReintentos : " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
                End Try
            Next
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(15, 0, 2, "Procesar.vb - AgregarReintentos : " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de identificar si el proceso se realiza
    ''' </summary>
    ''' <param name="m_datosRealce">Dato a realizar el proceso</param>
    ''' <returns>Retorna si la se realiza el proceso o no</returns>
    ''' <remarks>AERM 31/03/2015</remarks>
    Private Function ProcesarCola(m_datosRealce As DatosCardWizard) As Boolean
        Try
            Try
                Dim m_imp = (From i In impresoras
                             Where i.pCName = m_datosRealce.PCName And i.CardWizardName = m_datosRealce.CardWizardName
                             Select i).First()
                If m_imp IsNot Nothing Then
                    If (Me.ValidarTiempo(m_imp.tiempo, tiempoReProceso) = False) Then
                        Return False
                    Else
                        impresoras.Remove(m_imp)
                    End If
                End If
            Catch ex As Exception

            End Try

            Try
                If (Me.ValidarTiempo(m_datosRealce.TiempoPRoceso, tiempoReProceso)) Then
                    m_datosRealce.TiempoPRoceso = Date.Now
                    Return True
                Else
                    operacionesBD.CambiarEstado(m_datosRealce.ID_Transaccion, ConfigurationManager.AppSettings("BD"), "No ha superado el tiempo establecido")
                End If
            Catch ex As Exception
                m_datosRealce.TiempoPRoceso = Date.Now
                Return True
            End Try

        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(5, m_datosRealce.ID_Transaccion, 2, "Procesar.vb - ProcesarCola : " + ex.Message,
                                                         ConfigurationManager.AppSettings("BD"))
        End Try

        Return False
    End Function

    ''' <summary>
    ''' Metodo encargado de obtener contraseña
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ObtenerContrasena() As String
        Try
            Dim m_Contrasena = operacionesBD.ObtenerValoresConfigurador("PasswordCW", ConfigurationManager.AppSettings("BD"))
            Dim m_key As String = operacionesBD.ObtenerValoresConfigurador("KeyHash", ConfigurationManager.AppSettings("BD"))
            Dim m_Operar As HashEncriptar = New HashEncriptar(m_key)
            Return m_Operar.Descifrar(m_Contrasena)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Metodo encargado de guardar respuesta en el servidor para realizar notificacion
    ''' </summary>
    ''' <param name="resp"></param>
    ''' <param name="m_RespuestaDOC"></param>
    ''' <remarks>AERM 25/05/2015</remarks>
    Private Sub GuardarRespuesta(resp As RespuestaDeviceStatus, m_RespuestaDOC As String)
        Try
            Dim m_Notificacion As Boolean = False
            If resp.Estado = ESTADO_REALCE.ST_ERROR_DELETE Or resp.Estado = ESTADO_REALCE.ST_OK Or resp.Estado = ESTADO_REALCE.ST_ROLLBACK Then
                operacionesBD.CambiarEstadoNotificacion(resp.hostIdentifier, ConfigurationManager.AppSettings("BD"), True)
                operarCola.GuardarObjetoNotificacion(resp, colaNotificacion)
                m_Notificacion = True
            End If

            operacionesBD.CambiarEstado(resp.hostIdentifier, ConfigurationManager.AppSettings("BD"), resp.Estado.ToString() + resp.Mensaje, m_Notificacion)

            If resp.Estado = ESTADO_REALCE.ST_ERROR_DELETE Then
                Me.EliminarTarjeta(resp.Transaccion, resp.hostIdentifier, False)
            End If
        Catch ex As Exception
            resp.Mensaje = resp.Mensaje + " " + ex.Message
            Dim serializar As String = New GeneradorXML().Serializar(resp)
            operacionesBD.InsertarProceso(resp.hostIdentifier, ConfigurationManager.AppSettings("BD"), serializar, 10)
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de enviar una eliminación de tarjeta sobre CW
    ''' </summary>
    ''' <param name="counter">Identificador transaccion para CW</param>
    ''' <param name="idtransaccion">Id transaccion IHI</param>
    ''' <param name="aumentarIntento">Boolean que indica si es un intento o se debe agregar a la tabla de procesos</param>
    ''' <param name="idProceso">Opcional menciona el id en el proceso para reintento</param>
    ''' <remarks></remarks>
    Public Sub EliminarTarjeta(counter As String, idtransaccion As String, aumentarIntento As Boolean, Optional ByRef idProceso As Int32 = 0)
        Dim m_resultado As Boolean = False
        Try
            Dim m_respuesta As String = String.Empty
            m_resultado = operar.EliminarTarjeta(servidor, Int32.Parse(puerto), m_respuesta, session, counter, idtransaccion, interfaceUsar)
            If (m_resultado) Then
                If (aumentarIntento) Then
                    operacionesBD.EliminarProceso(idProceso, ConfigurationManager.AppSettings("BD"))
                End If
            End If
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(6, idtransaccion, 2, "Procesar.vb - EliminarTarjeta : " + ex.Message,
                                                         ConfigurationManager.AppSettings("BD"))
            m_resultado = False
        Finally
            If (m_resultado = False) Then ''Proceso de reintentos (aumenta o lo crea)
                If (aumentarIntento) Then
                    operacionesBD.AumentarIntentosXProceso(idProceso, ConfigurationManager.AppSettings("BD"))
                    operacionesBD.GuardarLogXNombreConectionString(6, idtransaccion, 1, "Procesar.vb - EliminarTarjeta : Aumenta Intento",
                                                         ConfigurationManager.AppSettings("BD"))
                Else
                    operacionesBD.InsertarProceso(idtransaccion, ConfigurationManager.AppSettings("BD"), counter, 6)
                    operacionesBD.GuardarLogXNombreConectionString(6, idtransaccion, 1, "Procesar.vb - EliminarTarjeta : Inserta para re-Intentos",
                                                        ConfigurationManager.AppSettings("BD"))
                End If
            End If
        End Try
    End Sub

    Private Function ValidarTiempo(tiempo As Date, tiempoReProceso As Integer) As Boolean
        Try
            Dim m_Tiempo As Integer = 0
            m_Tiempo = DateDiff(DateInterval.Second, tiempo, Date.Now)
            If m_Tiempo > tiempoReProceso Then
                Return True
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return False
    End Function

    Private Function RespuestaMensaje(m_mensaje As String, m_codigo As String) As RespuestaDeviceStatus
        Dim m_Respuesta As RespuestaDeviceStatus = New RespuestaDeviceStatus()
        Try
            Dim m_RespuestaClase As Respuesta = New Respuesta()
            m_Respuesta.Mensaje = m_mensaje
            m_RespuestaClase.ObtenerMensajeCliente(m_Respuesta, m_codigo)
        Catch ex As Exception
            Throw ex
        End Try
        Return m_Respuesta
    End Function

    ''' <summary>
    ''' Metodo encargado de obtener todos los procesos de eliminar tarjeta pendientes
    ''' </summary>
    ''' <remarks>AERM 20/06/2016</remarks>
    Private Sub ProcesosEliminar()
        Dim m_transacion As Int32 = 0
        Try
            Dim m_transacciones As List(Of Reintentos) = operacionesBD.ObtnerNotificacion(6, ConfigurationManager.AppSettings("BD"))
            For Each m_Transac As Reintentos In m_transacciones
                m_transacion = m_Transac.IDTransaccion
                Me.EliminarTarjeta(m_Transac.Informacion, m_Transac.IDTransaccion, True, m_Transac.IDProceso)
                m_transacion = 0
            Next
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(6, m_transacion, 2, "Procesar.vb - ProcesosEliminar (Pendientes): " + ex.Message,
                                                        ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

#End Region
End Class
