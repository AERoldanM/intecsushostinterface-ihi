﻿Imports Intecsus.Entidades
Imports Intecsus.Encripcion
Imports System.Xml
Imports System.IO
Imports System.Xml.Serialization
Imports System.Configuration
Imports System.Text
Imports Intecsus.Seguridad

Public Class GeneradorXML

#Region "Instancia Variable"
    Dim interfaz As IEncrypt = New FactoryMethod().DevolverClase(ConfigurationManager.AppSettings("TDES"))
    Dim m_Encoding As String = "utf-8"
#End Region

#Region "Metodos Publicos"

    ''' <summary>
    ''' Metodo encargado de cambiar el tipod e codificacion que este por defecto
    ''' </summary>
    ''' <param name="encoding"></param>
    ''' <remarks>AERM 29/07/2016</remarks>
    Public Sub CambiarEncoding(encoding As String)
        m_Encoding = encoding
    End Sub

    ''' <summary>
    ''' Metodo encargado de generar el XML para la ejecucion de metodos con el CW
    ''' </summary>
    ''' <param name="interfaceUsada">Interfaz del CW</param>
    ''' <param name="metodo">Metodo el cual se implementa</param>
    ''' <param name="parametros">Parametros enviados a CW</param>
    ''' <returns>Retorna un strig con lo creado</returns>
    ''' <remarks>AERM 02-02-2015</remarks>
    Public Function CrearXML(ByVal interfaceUsada As String, ByVal metodo As String, ByVal parametros As List(Of ParametrosXML), Optional enmascara As Boolean = False) As String
        Dim m_Xwriter As Xml.XmlWriter
        Dim resultado As String
        Try
            Using m_Stream As New MemoryStream()
                Dim m_Enmascara As Cifrar = New Cifrar()
                m_Xwriter = New XmlTextWriter(m_Stream, New UTF8Encoding(False))
                With m_Xwriter

                    'Write the beginning of the document including the 
                    'document declaration. Standalone is true. 
                    .WriteStartDocument(True)

                    'Write the beginning of the "data" element. This is 
                    'the opening tag to our data. 
                    .WriteStartElement(interfaceUsada)
                    .WriteAttributeString("direction", "Request")
                    .WriteAttributeString("sequence", "1")

                    ' METHOD
                    .WriteStartElement("METHOD")
                    .WriteAttributeString("name", metodo)
                    .WriteAttributeString("version", "1")

                    'Enviamos los parametros necesarios para la solicitud'
                    For Each m_parametro As ParametrosXML In parametros
                        ' Guarda cada elemnto
                        .WriteStartElement(m_parametro.Nombre)
                        If (String.IsNullOrEmpty(m_parametro.NameCard) = False) Then
                            .WriteAttributeString("name", m_parametro.NameCard)
                        End If

                        .WriteAttributeString("type", m_parametro.Typo)
                        .WriteAttributeString("encoding", "none")
                        Dim valor As String = Me.ObtenerValor(m_parametro, m_Enmascara, enmascara)

                        .WriteCData(valor)
                        .WriteEndElement()
                    Next

                    .WriteEndDocument() 'End the document

                    'Flush the XML document to the underlying stream and
                    'close the underlying stream. The data will not be written out 
                    'to the stream until either the Flush() method is called or 
                    'the Close() method is called.
                    .Close()

                End With
                resultado = System.Text.Encoding.UTF8.GetString(m_Stream.ToArray)
            End Using
        Catch ex As Exception
            Throw ex
        End Try

        ' convert the stream to a string
        Return resultado
    End Function

    Public Function ConfigurarArchivoXML(configurador As String) As String
        Try
            Dim m_XML As String
            m_XML = configurador
            m_XML = m_XML.Replace("&lt;", "<")
            m_XML = m_XML.Replace("&quot;", "'")
            m_XML = m_XML.Replace("&gt;", ">")
            Return m_XML
        Catch ex As Exception
            Throw ex
        End Try

        Return String.Empty
    End Function

    ''' <summary>
    ''' Calse encargada de generar un parametro generico
    ''' </summary>
    ''' <param name="nombre">Nombre parametro</param>
    ''' <param name="tipo">tipo del aprametros</param>
    ''' <param name="valor">Valor del parametro</param>
    ''' <returns>Retorna el parametro</returns>
    ''' <remarks>AERM 17/09/2015</remarks>
    Public Function GenerarParametro(nombre As String, tipo As String, valor As String) As ParametrosXML
        Dim m_Parametro As ParametrosXML = New ParametrosXML()
        Dim m_Valor As Valor = New Valor()
        m_Parametro.Nombre = nombre
        m_Parametro.Typo = tipo
        m_Valor.ValorS = valor
        m_Parametro.Valor = New List(Of Valor)
        m_Parametro.Valor.Add(m_Valor)
        Return m_Parametro
    End Function
#End Region

#Region "Serializar o deserilizar Objetos"

    ''' <summary>
    ''' Metodo engardado de serializar cualquier objeto y retornar un XML
    ''' </summary>
    ''' <typeparam name="T">Tipo de objeto </typeparam>
    ''' <param name="datoSerializar">Objeto a serializar</param>
    ''' <returns>Retorna objeto XML en un string</returns>
    ''' <remarks>AERM 26/03/2015</remarks>
    Public Function Serializar(Of T)(datoSerializar As T) As String
        Try
            Dim stringwriter = New System.IO.StringWriter()
            Dim serializer = New XmlSerializer(GetType(T))
            serializer.Serialize(stringwriter, datoSerializar)
            Return stringwriter.ToString()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Metodo encargado de desearilizar un OBjeto
    ''' </summary>
    ''' <typeparam name="T">Tipo de objeto a deserializar</typeparam>
    ''' <param name="xmlText">String con el xml del objeto</param>
    ''' <returns>Retorna objeto deserializado</returns>
    ''' <remarks>AERM 26/03/2015</remarks>
    Public Function Deserialize(Of T)(xmlText As String) As T
        Try
            Dim stringReader = New System.IO.StringReader(xmlText)
            Dim serializer = New XmlSerializer(GetType(T))
            Return DirectCast(serializer.Deserialize(stringReader), T)
        Catch
            Throw
        End Try
    End Function
#End Region

    Private Function ObtenerValor(m_parametro As ParametrosXML, m_Enmascara As Cifrar, enmascara As Boolean) As String
        Dim m_valorF As String
        Try
            Dim m_ValorFinal = New StringBuilder()
            For Each m_valor As Valor In m_parametro.Valor
                m_ValorFinal.Append(Me.ObtenerValor(m_valor, m_Enmascara))
            Next

            m_valorF = m_ValorFinal.ToString()
            m_valorF = If(enmascara, If(m_parametro.Codificar, m_Enmascara.EnMascarar(m_valorF, m_parametro.Izquierda, m_parametro.Derecha), m_valorF), m_valorF)
        Catch ex As Exception
            Throw New Exception("Obtener valor " + ex.Message)
        End Try

        Return m_valorF
    End Function

    Private Function ObtenerValor(m_valor As Valor, m_Enmascara As Cifrar) As String
        Dim m_ValorS As String = m_valor.ValorS
        Dim m_Data() As Byte
        If (m_valor.MetodoPCI) Then
            ''Debe desencriptar
            m_Data = m_Enmascara.FromHex(interfaz.DescifrarDato(m_valor.ValorS))
            Dim m_iso As Encoding = Encoding.GetEncoding(m_Encoding)
            'Encoding.GetEncoding("ISO-8859-1")
            m_ValorS = m_iso.GetString(m_Data)
            ' Encoding.ASCII.GetString(m_Data)
        End If

        If (m_valor.FechaAsNet) Then
            If (m_ValorS.Length >= 4) Then
                m_ValorS = m_ValorS.Substring(2, 2) + "/" + m_ValorS.Substring(0, 2)
            End If
        End If

        If (m_ValorS.Length >= m_valor.ValoresDerecha And m_valor.ValoresDerecha <> 0) Then
            m_ValorS = m_ValorS.Substring(m_ValorS.Length - m_valor.ValoresDerecha, m_valor.ValoresDerecha)
        End If

        If (m_ValorS.Length > m_valor.Tamamio And m_valor.Tamamio <> 0) Then
            Return m_ValorS.Substring(0, m_valor.Tamamio)
        End If

        Return m_ValorS
    End Function

End Class
