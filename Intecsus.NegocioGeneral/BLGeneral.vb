﻿Imports Intecsus.Entidades
Imports Intecsus.DatosGeneral
Imports System.Configuration
Imports Intecsus.OperarXML
Imports Intecsus.Seguridad

Public Class BLGeneral
    Implements IBLGeneral

    Public Function ValidarUsuario(user As Usuario) As Boolean Implements IBLGeneral.ValidarUsuario
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Try
            Dim m_key As String = Me.ObtenerValoresConfigurador("KeyHash")
            Dim m_Operar As HashEncriptar = New HashEncriptar(m_key)
            user.contrasena = m_Operar.EncryptData(user.contrasena)
            Dim m_resultado As List(Of Usuario) = m_operaciones.ValidarUsuario(user, ConfigurationManager.AppSettings("BD"))
            If (m_resultado.Count > 0) Then
                Return True
            End If
        Catch ex As Exception
            m_operaciones.GuardarLog(16, 0, 2, "IBLGeneral-ValidarUsuario " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
        Return False
    End Function

    Public Function ObtenerConfiguracion() As List(Of Configurar) Implements IBLGeneral.ObtenerConfiguracion
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Dim m_Resultado As List(Of Configurar) = New List(Of Configurar)()
        Try
            Return m_operaciones.ObtenerConfiguracion(ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            m_operaciones.GuardarLog(17, 0, 2, "IBLGeneral-ObtenerConfiguracion " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
        Return m_Resultado
    End Function

    ''' <summary>
    ''' Metodo encargado de actualizar los datos
    ''' </summary>
    ''' <param name="datoActualizar">Datos a actualizar</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Sub ActualizarConfiguracion(datoActualizar As Configurar) Implements IBLGeneral.ActualizarConfiguracion
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Try
            Dim m_XML As GeneradorXML = New GeneradorXML()
            datoActualizar.Valor = m_XML.ConfigurarArchivoXML(datoActualizar.Valor)
            m_operaciones.ActualizarConfiguracion(datoActualizar, ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            m_operaciones.GuardarLog(18, 0, 2, "IBLGeneral-ActualizarConfiguracion " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de insertar los datos
    ''' </summary>
    ''' <param name="datoActualizar">Datos a actualizar</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Sub IngresarConfiguracion(datoActualizar As Configurar) Implements IBLGeneral.IngresarConfiguracion
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Try
            Dim m_XML As GeneradorXML = New GeneradorXML()
            datoActualizar.Valor = m_XML.ConfigurarArchivoXML(datoActualizar.Valor)
            m_operaciones.IngresarConfiguracion(datoActualizar, ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            m_operaciones.GuardarLog(18, 0, 2, "IBLGeneral-ActualizarConfiguracion " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de reotrnar las impresoras creadas en el sistema
    ''' </summary>
    ''' <returns>Retorna Lista de impresoras creadas</returns>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Function ObtenerImpresoras() As List(Of Impresora) Implements IBLGeneral.ObtenerImpresoras
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Try
            Return m_operaciones.ObtenerImpresoras(ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            m_operaciones.GuardarLog(17, 0, 2, "IBLGeneral-ObtenerImpresoras " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
        Return New List(Of Impresora)
    End Function

    ''' <summary>
    ''' Metodo encargado de insertar una impresora
    ''' </summary>
    ''' <param name="impresora">Impresora a ingresar</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Sub InsertarImpresora(impresora As Impresora) Implements IBLGeneral.InsertarImpresora
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Try
            m_operaciones.InsertarImpresora(impresora, ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            m_operaciones.GuardarLog(18, 0, 2, "IBLGeneral-ObtenerImpresoras " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de actualizar una impresora
    ''' </summary>
    ''' <param name="impresora">Impresora q se debe actualizar</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Sub ActualizarImpresora(impresora As Impresora) Implements IBLGeneral.ActualizarImpresora
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Try
            m_operaciones.ActualizarImpresora(impresora, ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            m_operaciones.GuardarLog(18, 0, 2, "IBLGeneral-ActualizarImpresora " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de Eliminar una impresora
    ''' </summary>
    ''' <param name="impresora">Impresora q se debe Eliminar</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Sub EliminarImpresora(impresora As Impresora) Implements IBLGeneral.EliminarImpresora
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Try
            m_operaciones.EliminarImpresora(impresora, ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            m_operaciones.GuardarLog(18, 0, 2, "IBLGeneral-EliminarImpresora " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de traer la lista de prodcutos asociados alos cardNAme
    ''' </summary>
    ''' <returns>Retorna la lista de productos</returns>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Function ObtenerProductos() As List(Of ProductoXCW) Implements IBLGeneral.ObtenerProductos
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Try
            Return m_operaciones.ObtenerProductos(ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            m_operaciones.GuardarLog(17, 0, 2, "IBLGeneral-ObtenerImpresoras " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
        Return New List(Of ProductoXCW)
    End Function

    ''' <summary>
    ''' Metodo encargado de producto una impresora
    ''' </summary>
    ''' <param name="producto">producto a ingresar</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Sub InsertarProducto(producto As ProductoXCW) Implements IBLGeneral.InsertarProducto
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Try
            m_operaciones.InsertarProducto(producto, ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            m_operaciones.GuardarLog(18, 0, 2, "IBLGeneral-InsertarImpresora " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de actualizar un producto 
    ''' </summary>
    ''' <param name="producto">producto actualizar</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Sub ActualizarProducto(producto As ProductoXCW) Implements IBLGeneral.ActualizarProducto
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Try
            m_operaciones.ActualizarProducto(producto, ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            m_operaciones.GuardarLog(18, 0, 2, "IBLGeneral-ActualizarProducto " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de Eliminar un producto 
    ''' </summary>
    ''' <param name="producto">Eliminar actualizar</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Sub EliminarProducto(producto As ProductoXCW) Implements IBLGeneral.EliminarProducto
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Try
            m_operaciones.EliminarProducto(producto, ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            m_operaciones.GuardarLog(18, 0, 2, "IBLGeneral-EliminarProducto " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de traer la lista de procesosXAprobar
    ''' </summary>
    ''' <returns>Retorna lista de PorcesoxAP</returns>
    ''' <remarks>AERM 07/04/2015</remarks>
    Function ObtenerProcesosXAP() As List(Of ProcesoXAP) Implements IBLGeneral.ObtenerProcesosXAP
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Dim m_Resultado As List(Of ProcesoXAP) = New List(Of ProcesoXAP)()
        Try
            Return m_operaciones.ObtenerProcesosXAP(ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            m_operaciones.GuardarLog(17, 0, 2, "IBLGeneral-ObtenerProcesosXAP " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try

        Return m_Resultado
    End Function

    ''' <summary>
    ''' Metodo encargado de actualizar un procesosXAprobar
    ''' </summary>
    ''' <param name="procesoXAP">Proceso a actualizar</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Sub ActualizarProcesoXAP(procesoXAP As ProcesoXAP) Implements IBLGeneral.ActualizarProcesoXAP
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Try
            m_operaciones.ActualizarProcesoXAP(procesoXAP, ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            m_operaciones.GuardarLog(17, 0, 2, "IBLGeneral-EliminarProducto " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de obtener los logs de la aplicacion
    ''' </summary>
    ''' <returns>Lista de logs</returns>
    ''' <remarks>AERM 08/04/2015</remarks>
    Public Function ObtenerLogs() As List(Of LOG) Implements IBLGeneral.ObtenerLogs
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Dim m_Resultado As List(Of LOG) = New List(Of LOG)()
        Try
            Return m_operaciones.ObtenerLogs(ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            m_operaciones.GuardarLog(17, 0, 2, "IBLGeneral-ObtenerLogs " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
        Return m_Resultado
    End Function

    ''' <summary>
    ''' Metodo encargado de obtener los logs de la aplicacionXPaginacion
    ''' </summary>
    ''' <param name="pagina" >Pagina en la cual retorna los registros</param>
    ''' <param name="registro">Numero de registros por pagina</param>
    ''' <returns>Retorna Lista de logs</returns>
    ''' <remarks>AERM 20/04/2015</remarks>
    Public Function ObtenerLogs(registro As Integer, pagina As Integer) As List(Of LOG) Implements IBLGeneral.ObtenerLogs
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Dim m_Resultado As List(Of LOG) = New List(Of LOG)()
        Try
            Return m_operaciones.ObtenerLogs(registro, pagina, ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            m_operaciones.GuardarLog(17, 0, 2, "IBLGeneral-ObtenerLogs " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
        Return m_Resultado
    End Function

    ''' <summary>
    ''' Metodo encargado de obtener los logs de alguna transaccion
    ''' </summary>
    ''' <param name="transaccion">Identificador de la transaccion en el motor</param>
    ''' <param name="transaccionCliente">Identificador de la transaccion para el cliente</param>
    ''' <param name="fechaIni">Fecha inicial para realizar la bsuqueda</param>
    ''' <param name="fechaFin">Fecha final para realizar la busqueda</param>
    ''' <returns>retorna lista de logs</returns>
    ''' <remarks>AERM 08/04/2015</remarks>
    Public Function ObtenerLogXTran(transaccion As String, transaccionCliente As String, fechaIni As String, fechaFin As String) As List(Of LOG) Implements IBLGeneral.ObtenerLogXTran
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Dim m_Resultado As List(Of LOG) = New List(Of LOG)()
        Try
            Return m_operaciones.ObtenerLogXTran(CInt(transaccion), transaccionCliente, fechaIni, fechaFin, ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            m_operaciones.GuardarLog(17, 0, 2, "IBLGeneral-ObtenerLogXTran " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
        Return m_Resultado
    End Function



    ''' <summary>
    ''' Metodo encargado de obtener los EstadoTransaccion de la aplicacion
    ''' </summary>
    ''' <returns>Retorna Lista de EstadoTransaccion</returns>
    ''' <remarks>AERM 08/04/2015</remarks>
    Public Function ObtenerEstadoTransaccion() As List(Of EstadoTransaccion) Implements IBLGeneral.ObtenerEstadoTransaccion
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Dim m_Resultado As List(Of EstadoTransaccion) = New List(Of EstadoTransaccion)()
        Try
            Return m_operaciones.ObtenerEstadoTransaccion(ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            m_operaciones.GuardarLog(17, 0, 2, "IBLGeneral-ObtenerEstadoTransaccion " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
        Return m_Resultado
    End Function

    ''' <summary>
    ''' Metodo encargado de obtener los EstadoTransaccion de la aplicacion
    ''' </summary>
    ''' <param name="transaccion">Identificador de la transaccion en el motor</param>
    ''' <param name="transaccionCliente">Identificador de la transaccion para el cliente</param>
    ''' <returns>Retorna Lista de EstadoTransaccion</returns>
    ''' <remarks>AERM 08/04/2015</remarks>
    Public Function ObtenerEstadoTransaXTran(transaccion As String, transaccionCliente As String) As List(Of EstadoTransaccion) Implements IBLGeneral.ObtenerEstadoTransaXTran
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Dim m_Resultado As List(Of EstadoTransaccion) = New List(Of EstadoTransaccion)()
        Try
            Return m_operaciones.ObtenerEstadoTransaXTran(CInt(transaccion), transaccionCliente, ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            m_operaciones.GuardarLog(17, 0, 2, "IBLGeneral-ObtenerEstadoTransaXTran " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
        Return m_Resultado
    End Function

    ''' <summary>
    ''' Metodo encargado de cambair la contrasena
    ''' </summary>
    ''' <param name="usuario">Usuario a cambiar la contrasena</param>
    ''' <param name="contrasena">Contrasena nueva</param>
    ''' <returns>Retorna usuario con nuevas credenciales</returns>
    ''' <remarks>AERM 08/04/2015</remarks>
    Function CambiarContrasena(usuario As Usuario, contrasena As String) As Usuario Implements IBLGeneral.CambiarContrasena
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Try
            Dim m_key As String = Me.ObtenerValoresConfigurador("KeyHash")
            Dim m_Operar As HashEncriptar = New HashEncriptar(m_key)
            usuario.contrasena = m_Operar.EncryptData(contrasena)

            'Dim encripta As Encriptar = New Encriptar()
            'usuario.contrasena = encripta.Encriptar(contrasena)
            m_operaciones.CambiarContrasena(usuario, ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            m_operaciones.GuardarLog(18, 0, 2, "IBLGeneral-CambiarContrasena " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
        Return usuario
    End Function

    ''' <summary>
    ''' Metodo encargado de enviar los valores del configurador de CardWizard
    ''' </summary>
    ''' <param name="idValor">Identificador del valor</param>
    ''' <returns>retorna valor solicitado</returns>
    ''' <remarks>AERM 16/03/2015</remarks>
    Public Function ObtenerValoresConfigurador(idValor As String) As String Implements IBLGeneral.ObtenerValoresConfigurador
        Dim m_operaciones As IDALGeneral = New DALGeneral()
        Try
            Return m_operaciones.ObtenerValoresConfigurador(idValor, ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            m_operaciones.GuardarLog(17, 0, 2, "IBLGeneral-ObtenerValoresConfigurador " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try

        Return String.Empty
    End Function
End Class
