﻿Imports Intecsus.Entidades

''' <summary>
''' Interface que contiene las operaciones q se realiza el configurador
''' </summary>
''' <remarks>AERM 06/04/2015</remarks>
Public Interface IBLGeneral

    ''' <summary>
    ''' Metodo encargado de retornar las credenciales de un usuario
    ''' </summary>
    ''' <param name="user">Objeto usuario</param>
    ''' <returns>Retorna si el usuario es valido</returns>
    ''' <remarks>AERM 06/04/2015</remarks>
    Function ValidarUsuario(ByVal user As Usuario) As Boolean

    ''' <summary>
    ''' Metodo encargado de retornar la lista de los valores a configurar
    ''' </summary>
    ''' <returns>Retorna lista de configuracion</returns>
    ''' <remarks>AERM 07/04/2015</remarks>
    Function ObtenerConfiguracion() As List(Of Configurar)

    ''' <summary>
    ''' Metodo encargado de actualizar los datos
    ''' </summary>
    ''' <param name="datoActualizar">Datos a actualizar</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Sub ActualizarConfiguracion(datoActualizar As Configurar)

    ''' <summary>
    ''' Metodo encargado de insertar los datos
    ''' </summary>
    ''' <param name="datoActualizar">Datos a actualizar</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Sub IngresarConfiguracion(datoActualizar As Configurar)

    ''' <summary>
    ''' Metodo encargado de reotrnar las impresoras creadas en el sistema
    ''' </summary>
    ''' <returns>Retorna Lista de impresoras creadas</returns>
    ''' <remarks>AERM 07/04/2015</remarks>
    Function ObtenerImpresoras() As List(Of Impresora)

    ''' <summary>
    ''' Metodo encargado de insertar una impresora
    ''' </summary>
    ''' <param name="impresora">Impresora a ingresar</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Sub InsertarImpresora(impresora As Impresora)

    ''' <summary>
    ''' Metodo encargado de actualizar una impresora
    ''' </summary>
    ''' <param name="impresora">Impresora q se debe actualizar</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Sub ActualizarImpresora(impresora As Impresora)

    ''' <summary>
    ''' Metodo encargado de Eliminar una impresora
    ''' </summary>
    ''' <param name="impresora">Impresora q se debe Eliminar</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Sub EliminarImpresora(impresora As Impresora)

    ''' <summary>
    ''' Metodo encargado de traer la lista de prodcutos asociados alos cardNAme
    ''' </summary>
    ''' <returns>Retorna la lista de productos</returns>
    ''' <remarks>AERM 07/04/2015</remarks>
    Function ObtenerProductos() As List(Of ProductoXCW)

    ''' <summary>
    ''' Metodo encargado de ingresarun producto 
    ''' </summary>
    ''' <param name="producto">producto a ingresar</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Sub InsertarProducto(producto As ProductoXCW)

    ''' <summary>
    ''' Metodo encargado de actualizar un producto 
    ''' </summary>
    ''' <param name="producto">producto actualizar</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Sub ActualizarProducto(producto As ProductoXCW)

    ''' <summary>
    ''' Metodo encargado de Eliminar un producto 
    ''' </summary>
    ''' <param name="producto">Eliminar actualizar</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Sub EliminarProducto(producto As ProductoXCW)

    ''' <summary>
    ''' Metodo encargado de traer la lista de procesosXAprobar
    ''' </summary>
    ''' <returns>Retorna lista de PorcesoxAP</returns>
    ''' <remarks>AERM 07/04/2015</remarks>
    Function ObtenerProcesosXAP() As List(Of ProcesoXAP)

    ''' <summary>
    ''' Metodo encargado de actualizar un procesosXAprobar
    ''' </summary>
    ''' <param name="procesoXAP">Proceso a actualizar</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Sub ActualizarProcesoXAP(procesoXAP As ProcesoXAP)

    ''' <summary>
    ''' Metodo encargado de obtener los logs de la aplicacion
    ''' </summary>
    ''' <returns>Retorna Lista de logs</returns>
    ''' <remarks>AERM 08/04/2015</remarks>
    Function ObtenerLogs() As List(Of LOG)


    ''' <summary>
    ''' Metodo encargado de obtener los logs de la aplicacionXPaginacion
    ''' </summary>
    ''' <param name="pagina" >Pagina en la cual retorna los registros</param>
    ''' <param name="registro">Numero de registros por pagina</param>
    ''' <returns>Retorna Lista de logs</returns>
    ''' <remarks>AERM 20/04/2015</remarks>
    Function ObtenerLogs(registro As Integer, pagina As Integer) As List(Of LOG)


    ''' <summary>
    ''' Metodo encargado de obtener los logs de alguna transaccion
    ''' </summary>
    ''' <param name="transaccion">Identificador de la transaccion en el motor</param>
    ''' <param name="transaccionCliente">Identificador de la transaccion para el cliente</param>
    ''' <param name="fechaIni">Fecha inicial para realizar la bsuqueda</param>
    ''' <param name="fechaFin">Fecha final para realizar la busqueda</param>
    ''' <returns>retorna lista de logs</returns>
    ''' <remarks>AERM 08/04/2015</remarks>
    Function ObtenerLogXTran(transaccion As String, transaccionCliente As String, fechaIni As String, fechaFin As String) As List(Of LOG)


    ''' <summary>
    ''' Metodo encargado de obtener los EstadoTransaccion de la aplicacion
    ''' </summary>
    ''' <returns>Retorna Lista de EstadoTransaccion</returns>
    ''' <remarks>AERM 08/04/2015</remarks>
    Function ObtenerEstadoTransaccion() As List(Of EstadoTransaccion)

    ''' <summary>
    ''' Metodo encargado de obtener los EstadoTransaccion de la aplicacion
    ''' </summary>
    ''' <param name="transaccion">Identificador de la transaccion en el motor</param>
    ''' <param name="transaccionCliente">Identificador de la transaccion para el cliente</param>
    ''' <returns>Retorna Lista de EstadoTransaccion</returns>
    ''' <remarks>AERM 08/04/2015</remarks>
    Function ObtenerEstadoTransaXTran(transaccion As String, transaccionCliente As String) As List(Of EstadoTransaccion)

    ''' <summary>
    ''' Metodo encargado de cambair la contrasena
    ''' </summary>
    ''' <param name="usuario">Usuario a cambiar la contrasena</param>
    ''' <param name="contrasena">Contrasena nueva</param>
    ''' <returns>Retorna usuario con nuevas credenciales</returns>
    ''' <remarks>AERM 08/04/2015</remarks>
    Function CambiarContrasena(usuario As Usuario, contrasena As String) As Usuario

    ''' <summary>
    ''' Metodo encargado de enviar los valores del configurador de CardWizard
    ''' </summary>
    ''' <param name="idValor">Identificador del valor</param>
    ''' <returns>retorna valor solicitado</returns>
    ''' <remarks>AERM 16/03/2015</remarks>
    Function ObtenerValoresConfigurador(idValor As String) As String
End Interface
