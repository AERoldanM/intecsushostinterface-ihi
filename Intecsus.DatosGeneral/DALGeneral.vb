﻿Imports Intecsus.Entidades
Imports Intecusus.OperarBD
Imports System.Configuration

Public Class DALGeneral
    Implements IDALGeneral

    Private operarBD As OperacionesBase = New OperacionesBase()

    Public Sub GuardarLog(idProceso As Integer, idTransaccion As Integer, evento As Integer, descripcionEnvento As String, nombreConexion As String) Implements IDALGeneral.GuardarLog
        Try
            operarBD.GuardarLogXNombreConectionString(idProceso, idTransaccion, evento, descripcionEnvento, nombreConexion)
        Catch ex As Exception

        End Try
    End Sub

    Public Function ValidarUsuario(user As Usuario, conexion As String) As List(Of Usuario) Implements IDALGeneral.ValidarUsuario
        Dim m_Resp As List(Of Usuario) = New List(Of Usuario)()
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            Dim m_resultado As DataSet
            m_parametro.nombre = "@Contrasena"
            m_parametro.valorEnviar = user.contrasena
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = user.contrasena.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@UserID"
            m_parametro.valorEnviar = user.userID.ToString()
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = user.userID.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_resultado = m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_ObtenerUsuario", "Parametros", m_listParametros)
            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                Dim m_Usuario As Usuario = New Usuario()
                m_Usuario.userID = m_resultado.Tables(0).Rows(0)("UserID").ToString()
                m_Usuario.contrasena = m_resultado.Tables(0).Rows(0)("Contrasena").ToString()
                m_Resp.Add(m_Usuario)
            End If
        Catch ex As Exception
            operarBD.GuardarLogXNombreConectionString(16, 0, 2, "DALGeneral-ValidarUsuario " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try

        Return m_Resp
    End Function

    ''' <summary>
    ''' Metodo encargado de retornar la lista de los valores a configurar
    ''' </summary>
    ''' <returns>Retorna lista de configuracion</returns>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Function ObtenerConfiguracion(conexion As String) As List(Of Configurar) Implements IDALGeneral.ObtenerConfiguracion
        Dim m_Resultado As List(Of Configurar) = New List(Of Configurar)()
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_resul As DataSet

            m_resul = m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_ObtenerConfiguracion", "Parametros", m_listParametros)
            If m_resul.Tables.Count > 0 And m_resul.Tables(0).Rows.Count > 0 Then
                For Each m_row As DataRow In m_resul.Tables(0).Rows
                    Dim m_Configuracion As Configurar = New Configurar()
                    m_Configuracion.IDValor = m_row("IdValor").ToString()
                    m_Configuracion.Valor = m_row("Valor").ToString()
                    m_Configuracion.TipoDato = m_row("TipoDato").ToString()
                    m_Resultado.Add(m_Configuracion)
                Next
            End If
        Catch ex As Exception
            operarBD.GuardarLogXNombreConectionString(17, 0, 2, "DALGeneral-ObtenerConfiguracion " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try

        Return m_Resultado
    End Function

    ''' <summary>
    ''' Metodo encargado de actualizar los datos
    ''' </summary>
    ''' <param name="datoActualizar">Datos a actualizar</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Sub ActualizarConfiguracion(datoActualizar As Configurar, conexion As String) Implements IDALGeneral.ActualizarConfiguracion

        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdValor"
            m_parametro.valorEnviar = datoActualizar.IDValor
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = datoActualizar.IDValor.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Valor"
            m_parametro.valorEnviar = datoActualizar.Valor
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = datoActualizar.Valor.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@TipoDato"
            m_parametro.valorEnviar = datoActualizar.TipoDato
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = datoActualizar.TipoDato.Length()
            m_listParametros.Add(m_parametro)

            m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_UpdateValor", "Parametros", m_listParametros)
           
        Catch ex As Exception
            operarBD.GuardarLogXNombreConectionString(18, 0, 2, "DALGeneral-ActualizarConfiguracion " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub


    ''' <summary>
    ''' Metodo encargado de Insertar los datos
    ''' </summary>
    ''' <param name="datoActualizar">Datos a actualizar</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Sub IngresarConfiguracion(datoActualizar As Configurar, conexion As String) Implements IDALGeneral.IngresarConfiguracion

        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdValor"
            m_parametro.valorEnviar = datoActualizar.IDValor
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = datoActualizar.IDValor.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Valor"
            m_parametro.valorEnviar = datoActualizar.Valor
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = datoActualizar.Valor.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@TipoDato"
            m_parametro.valorEnviar = datoActualizar.TipoDato
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = datoActualizar.TipoDato.Length()
            m_listParametros.Add(m_parametro)

            m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_InsertValor", "Parametros", m_listParametros)

        Catch ex As Exception
            operarBD.GuardarLogXNombreConectionString(18, 0, 2, "DALGeneral-ActualizarConfiguracion " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de reotrnar las impresoras creadas en el sistema
    ''' </summary>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <returns>Retorna Lista de impresoras creadas</returns>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Function ObtenerImpresoras(conexion As String) As List(Of Impresora) Implements IDALGeneral.ObtenerImpresoras
        Dim m_Resultado As List(Of Impresora) = New List(Of Impresora)()
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_resul As DataSet

            m_resul = m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_ObtenerImpresora", "Parametros", m_listParametros)
            If m_resul.Tables.Count > 0 And m_resul.Tables(0).Rows.Count > 0 Then
                For Each m_row As DataRow In m_resul.Tables(0).Rows
                    Dim m_Impresora As Impresora = New Impresora()
                    m_Impresora.IDImpresora = m_row("IdImpresora").ToString()
                    m_Impresora.NombreRecibido = m_row("NombreEnviado").ToString()
                    m_Impresora.PCNAME = m_row("PCName").ToString()
                    m_Impresora.IP = m_row("Ip").ToString()
                    m_Resultado.Add(m_Impresora)
                Next
            End If
        Catch ex As Exception
            operarBD.GuardarLogXNombreConectionString(17, 0, 2, "DALGeneral-ObtenerImpresoras " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try

        Return m_Resultado
    End Function


    ''' <summary>
    ''' Metodo encargado de insertar una impresora
    ''' </summary>
    ''' <param name="impresora">Impresora a ingresar</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Sub InsertarImpresora(impresora As Impresora, conexion As String) Implements IDALGeneral.InsertarImpresora
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@NombreEnviado"
            m_parametro.valorEnviar = impresora.NombreRecibido
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = impresora.NombreRecibido.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@PCName"
            m_parametro.valorEnviar = impresora.PCNAME
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = impresora.PCNAME.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Ip"
            m_parametro.valorEnviar = impresora.IP
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = impresora.IP.Length()
            m_listParametros.Add(m_parametro)

            m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_InsertImpresora", "Parametros", m_listParametros)

        Catch ex As Exception
            operarBD.GuardarLogXNombreConectionString(18, 0, 2, "DALGeneral-InsertarImpresora " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub


    ''' <summary>
    ''' Metodo encargado de actualizar una impresora
    ''' </summary>
    ''' <param name="impresora">Impresora q se debe actualizar</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Sub ActualizarImpresora(impresora As Impresora, conexion As String) Implements IDALGeneral.ActualizarImpresora
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@NombreEnviado"
            m_parametro.valorEnviar = impresora.NombreRecibido
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = impresora.NombreRecibido.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@PCName"
            m_parametro.valorEnviar = impresora.PCNAME
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = impresora.PCNAME.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Ip"
            m_parametro.valorEnviar = impresora.IP
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = impresora.IP.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdImpresora"
            m_parametro.valorEnviar = impresora.IdImpresora
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = impresora.IdImpresora.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_UpdateImpresora", "Parametros", m_listParametros)

        Catch ex As Exception
            operarBD.GuardarLogXNombreConectionString(18, 0, 2, "DALGeneral-ActualizarImpresora " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de Eliminar una impresora
    ''' </summary>
    ''' <param name="impresora">Impresora q se debe Eliminar</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Sub EliminarImpresora(impresora As Impresora, conexion As String) Implements IDALGeneral.EliminarImpresora
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdImpresora"
            m_parametro.valorEnviar = impresora.IdImpresora
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = impresora.IdImpresora.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_DeleteImpresora", "Parametros", m_listParametros)

        Catch ex As Exception
            operarBD.GuardarLogXNombreConectionString(18, 0, 2, "DALGeneral-EliminarImpresora " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de traer la lista de prodcutos asociados alos cardNAme
    ''' </summary>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <returns>Retorna la lista de productos</returns>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Function ObtenerProductos(conexion As String) As List(Of ProductoXCW) Implements IDALGeneral.ObtenerProductos
        Dim m_Resultado As List(Of ProductoXCW) = New List(Of ProductoXCW)()
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_resul As DataSet

            m_resul = m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_ObtenerProductoXCW", "Parametros", m_listParametros)
            If m_resul.Tables.Count > 0 And m_resul.Tables(0).Rows.Count > 0 Then
                For Each m_row As DataRow In m_resul.Tables(0).Rows
                    Dim m_Producto As ProductoXCW = New ProductoXCW()
                    m_Producto.ID = m_row("IdProductoXCW").ToString()
                    m_Producto.Cliente = m_row("Cliente").ToString()
                    m_Producto.Producto = m_row("Producto").ToString()
                    m_Producto.CardName = m_row("CardName").ToString()
                    m_Resultado.Add(m_Producto)
                Next
            End If
        Catch ex As Exception
            operarBD.GuardarLogXNombreConectionString(17, 0, 2, "DALGeneral-ObtenerImpresoras " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try

        Return m_Resultado
    End Function

    ''' <summary>
    ''' Metodo encargado de producto una impresora
    ''' </summary>
    ''' <param name="producto">producto a ingresar</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Sub InsertarProducto(producto As ProductoXCW, conexion As String) Implements IDALGeneral.InsertarProducto
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Cliente"
            m_parametro.valorEnviar = producto.Cliente
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = producto.Cliente.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Producto"
            m_parametro.valorEnviar = producto.Producto
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = producto.Producto.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@CardName"
            m_parametro.valorEnviar = producto.CardName
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = producto.CardName.Length()
            m_listParametros.Add(m_parametro)


            m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_InsertProductoXCW", "Parametros", m_listParametros)

        Catch ex As Exception
            operarBD.GuardarLogXNombreConectionString(18, 0, 2, "DALGeneral-InsertarImpresora " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de actualizar un producto 
    ''' </summary>
    ''' <param name="producto">producto actualizar</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Sub ActualizarProducto(producto As ProductoXCW, conexion As String) Implements IDALGeneral.ActualizarProducto
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Cliente"
            m_parametro.valorEnviar = producto.Cliente
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = producto.Cliente.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Producto"
            m_parametro.valorEnviar = producto.Producto
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = producto.Producto.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@CardName"
            m_parametro.valorEnviar = producto.CardName
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = producto.CardName.Length()
            m_listParametros.Add(m_parametro)


            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdProductoXCW"
            m_parametro.valorEnviar = CInt(producto.ID)
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = producto.ID.Length()
            m_listParametros.Add(m_parametro)

            m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_UpdateProductoXCW", "Parametros", m_listParametros)

        Catch ex As Exception
            operarBD.GuardarLogXNombreConectionString(18, 0, 2, "DALGeneral-InsertarImpresora " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de Eliminar un producto 
    ''' </summary>
    ''' <param name="producto">Eliminar actualizar</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Sub EliminarProducto(producto As ProductoXCW, conexion As String) Implements IDALGeneral.EliminarProducto
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdProductoXCW"
            m_parametro.valorEnviar = CInt(producto.ID)
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = producto.ID.Length()
            m_listParametros.Add(m_parametro)

            m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_DeleteProductoXCW", "Parametros", m_listParametros)

        Catch ex As Exception
            operarBD.GuardarLogXNombreConectionString(18, 0, 2, "DALGeneral-EliminarProducto " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de traer la lista de procesosXAprobar
    ''' </summary>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <returns>Retorna lista de PorcesoxAP</returns>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Function ObtenerProcesosXAP(conexion As String) As List(Of ProcesoXAP) Implements IDALGeneral.ObtenerProcesosXAP
        Dim m_Resultado As List(Of ProcesoXAP) = New List(Of ProcesoXAP)()
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_resul As DataSet

            m_resul = m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_ObtenerProcesos", "Parametros", m_listParametros)
            If m_resul.Tables.Count > 0 And m_resul.Tables(0).Rows.Count > 0 Then
                For Each m_row As DataRow In m_resul.Tables(0).Rows
                    Dim m_Producto As ProcesoXAP = New ProcesoXAP()
                    m_Producto.ID = CInt(m_row("IdProcesoXRealizar").ToString())
                    m_Producto.IDProceso = CInt(m_row("IdProceso").ToString())
                    m_Producto.IDTransaccion = CInt(m_row("IdTransaccion").ToString())
                    m_Producto.Informacion = m_row("Informacion").ToString()
                    m_Producto.Fecha = m_row("Fecha").ToString()
                    m_Producto.Intentos = CInt(m_row("CantidadReIntentos").ToString())
                    m_Resultado.Add(m_Producto)
                Next
            End If
        Catch ex As Exception
            operarBD.GuardarLogXNombreConectionString(17, 0, 2, "DALGeneral-ObtenerProcesosXAP " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try

        Return m_Resultado
    End Function

    ''' <summary>
    ''' Metodo encargado de actualizar un procesosXAprobar
    ''' </summary>
    ''' <param name="procesoXAP">Proceso a actualizar</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Sub ActualizarProcesoXAP(procesoXAP As ProcesoXAP, conexion As String) Implements IDALGeneral.ActualizarProcesoXAP
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdProcesoXRealizar"
            m_parametro.valorEnviar = procesoXAP.ID
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = procesoXAP.ID.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Intentos"
            m_parametro.valorEnviar = procesoXAP.Intentos
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = procesoXAP.Intentos.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_UpdateCantidadIntentos", "Parametros", m_listParametros)

        Catch ex As Exception
            operarBD.GuardarLogXNombreConectionString(18, 0, 2, "DALGeneral-ActualizarProcesoXAP " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de obtener los logs de la aplicacion
    ''' </summary>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <returns>Lista de logs</returns>
    ''' <remarks>AERM 08/04/2015</remarks>
    Public Function ObtenerLogs(conexion As String) As List(Of LOG) Implements IDALGeneral.ObtenerLogs
        Dim m_Resultado As List(Of LOG) = New List(Of LOG)()
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_resul As DataSet

            m_resul = m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_ObtenerLog", "Parametros", m_listParametros)
            If m_resul.Tables.Count > 0 And m_resul.Tables(0).Rows.Count > 0 Then
                For Each m_row As DataRow In m_resul.Tables(0).Rows
                    Dim m_Producto As LOG = New LOG()
                    m_Producto.IdLog = CInt(m_row("IdLog").ToString())
                    m_Producto.IdProceso = CInt(m_row("IdProceso").ToString())
                    m_Producto.IDTransaccion = CInt(m_row("IdTransaccion").ToString())
                    m_Producto.DescripcionEvento = m_row("DescripcionEvento").ToString()
                    m_Producto.Fecha = m_row("Fecha").ToString()
                    m_Producto.Evento = CInt(m_row("Evento").ToString())
                    m_Resultado.Add(m_Producto)
                Next
            End If
        Catch ex As Exception
            operarBD.GuardarLogXNombreConectionString(17, 0, 2, "DALGeneral-ObtenerLogs " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try

        Return m_Resultado
    End Function

    ''' <summary>
    ''' Metodo encargado de obtener los logs de la aplicacionXPaginacion
    ''' </summary>
    ''' <param name="pagina" >Pagina en la cual retorna los registros</param>
    ''' <param name="registro">Numero de registros por pagina</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <returns>Retorna Lista de logs</returns>
    ''' <remarks>AERM 20/04/2015</remarks>
    Public Function ObtenerLogs(registro As Integer, pagina As Integer, conexion As String) As List(Of LOG) Implements IDALGeneral.ObtenerLogs
        Dim m_Resultado As List(Of LOG) = New List(Of LOG)()
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_resul As DataSet
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@CantidadRegistro"
            m_parametro.valorEnviar = registro
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = registro.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Pagina"
            m_parametro.valorEnviar = pagina
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = pagina.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_resul = m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_ObtenerDatosLog", "Parametros", m_listParametros)
            If m_resul.Tables.Count > 0 And m_resul.Tables(0).Rows.Count > 0 Then
                For Each m_row As DataRow In m_resul.Tables(0).Rows
                    Dim m_Producto As LOG = New LOG()
                    m_Producto.IdLog = CInt(m_row("IdLog").ToString())
                    m_Producto.IdProceso = CInt(m_row("IdProceso").ToString())
                    m_Producto.IDTransaccion = CInt(m_row("IdTransaccion").ToString())
                    m_Producto.DescripcionEvento = m_row("DescripcionEvento").ToString()
                    m_Producto.Fecha = m_row("Fecha").ToString()
                    m_Producto.Evento = CInt(m_row("Evento").ToString())
                    m_Resultado.Add(m_Producto)
                Next
            End If
        Catch ex As Exception
            operarBD.GuardarLogXNombreConectionString(17, 0, 2, "DALGeneral-ObtenerLogs " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try

        Return m_Resultado
    End Function


    ''' <summary>
    ''' Metodo encargado de obtener los logs de alguna transaccion
    ''' </summary>
    ''' <param name="transaccion">Identificador de la transaccion en el motor</param>
    ''' <param name="transaccionCliente">Identificador de la transaccion para el cliente</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <param name="fechaIni">Fecha inicial para realizar la bsuqueda</param>
    ''' <param name="fechaFin">Fecha final para realizar la busqueda</param>
    ''' <returns>retorna lista de logs</returns>
    ''' <remarks>AERM 08/04/2015</remarks>
    Function ObtenerLogXTran(transaccion As Integer, transaccionCliente As String, fechaIni As String, fechaFin As String, conexion As String) As List(Of LOG) Implements IDALGeneral.ObtenerLogXTran
        Dim m_Resultado As List(Of LOG) = New List(Of LOG)()
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_resul As DataSet

            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IDtransaccion"
            m_parametro.valorEnviar = transaccion
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = transaccion.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@TransaccionCLi"
            m_parametro.valorEnviar = transaccionCliente
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = transaccionCliente.Length()
            m_listParametros.Add(m_parametro)


            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@FechaInicio"
            m_parametro.valorEnviar = fechaIni
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = fechaIni.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@FechaFin"
            m_parametro.valorEnviar = fechaFin
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = fechaFin.Length()
            m_listParametros.Add(m_parametro)
            m_resul = m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_ObtenerLogXTransaccion", "Parametros", m_listParametros)
            If m_resul.Tables.Count > 0 And m_resul.Tables(0).Rows.Count > 0 Then
                For Each m_row As DataRow In m_resul.Tables(0).Rows
                    Dim m_Producto As LOG = New LOG()
                    m_Producto.IdLog = CInt(m_row("IdLog").ToString())
                    m_Producto.IdProceso = CInt(m_row("IdProceso").ToString())
                    m_Producto.IDTransaccion = CInt(m_row("IdTransaccion").ToString())
                    m_Producto.DescripcionEvento = m_row("DescripcionEvento").ToString()
                    m_Producto.Fecha = m_row("Fecha").ToString()
                    m_Producto.Evento = CInt(m_row("Evento").ToString())
                    m_Resultado.Add(m_Producto)
                Next
            End If
        Catch ex As Exception
            operarBD.GuardarLogXNombreConectionString(17, 0, 2, "DALGeneral-ObtenerLogXTran " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try

        Return m_Resultado
    End Function

    ''' <summary>
    ''' Metodo encargado de obtener los EstadoTransaccion de la aplicacion
    ''' </summary>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <returns>Retorna Lista de EstadoTransaccion</returns>
    ''' <remarks>AERM 08/04/2015</remarks>
    Public Function ObtenerEstadoTransaccion(conexion As String) As List(Of EstadoTransaccion) Implements IDALGeneral.ObtenerEstadoTransaccion
        Dim m_Resultado As List(Of EstadoTransaccion) = New List(Of EstadoTransaccion)()
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_resul As DataSet

            m_resul = m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_ObtenerEstadosTransacciones", "Parametros", m_listParametros)
            If m_resul.Tables.Count > 0 And m_resul.Tables(0).Rows.Count > 0 Then
                For Each m_row As DataRow In m_resul.Tables(0).Rows
                    Dim m_Producto As EstadoTransaccion = New EstadoTransaccion()
                    m_Producto.IDTransaccion = CInt(m_row("IdTransaccion").ToString())
                    m_Producto.Cliente = m_row("Cliente").ToString()
                    m_Producto.IdTransaccionCl = m_row("IdTransaccionC").ToString()
                    m_Producto.Estado = m_row("Estado").ToString()
                    m_Resultado.Add(m_Producto)
                Next
            End If
        Catch ex As Exception
            operarBD.GuardarLogXNombreConectionString(17, 0, 2, "DALGeneral-ObtenerEstadoTransaccion " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try

        Return m_Resultado
    End Function

    ''' <summary>
    ''' Metodo encargado de obtener los EstadoTransaccion de la aplicacion
    ''' </summary>
    ''' <param name="transaccion">Identificador de la transaccion en el motor</param>
    ''' <param name="transaccionCliente">Identificador de la transaccion para el cliente</param>
    ''' <returns>Retorna Lista de EstadoTransaccion</returns>
    ''' <remarks>AERM 08/04/2015</remarks>
    Public Function ObtenerEstadoTransaXTran(transaccion As Integer, transaccionCliente As String, conexion As String) As List(Of EstadoTransaccion) Implements IDALGeneral.ObtenerEstadoTransaXTran
        Dim m_Resultado As List(Of EstadoTransaccion) = New List(Of EstadoTransaccion)()
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_resul As DataSet
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IDtransaccion"
            m_parametro.valorEnviar = transaccion
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = transaccion.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@TransaccionCLi"
            m_parametro.valorEnviar = transaccionCliente
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = transaccionCliente.Length()
            m_listParametros.Add(m_parametro)
            m_resul = m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_ObtenerTranXID", "Parametros", m_listParametros)
            If m_resul.Tables.Count > 0 And m_resul.Tables(0).Rows.Count > 0 Then
                For Each m_row As DataRow In m_resul.Tables(0).Rows
                    Dim m_Producto As EstadoTransaccion = New EstadoTransaccion()
                    m_Producto.IDTransaccion = CInt(m_row("IdTransaccion").ToString())
                    m_Producto.Cliente = m_row("Cliente").ToString()
                    m_Producto.IdTransaccionCl = m_row("IdTransaccionC").ToString()
                    m_Producto.Estado = m_row("Estado").ToString()
                    m_Resultado.Add(m_Producto)
                Next
            End If
        Catch ex As Exception
            operarBD.GuardarLogXNombreConectionString(17, 0, 2, "DALGeneral-ObtenerEstadoTransaXTran " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try

        Return m_Resultado
    End Function

    ''' <summary>
    ''' Metodo encargado de cambair la contrasena
    ''' </summary>
    ''' <param name="usuario">Usuario a cambiar la contrasena</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 08/04/2015</remarks>
    Public Sub CambiarContrasena(usuario As Usuario, conexion As String) Implements IDALGeneral.CambiarContrasena
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@UserID"
            m_parametro.valorEnviar = usuario.userID
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = usuario.userID.Length()
            m_listParametros.Add(m_parametro)
            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Contrasena"
            m_parametro.valorEnviar = usuario.contrasena
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = usuario.contrasena.Length()
            m_listParametros.Add(m_parametro)
            m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_ActualizarUsuario", "Parametros", m_listParametros)

        Catch ex As Exception
            operarBD.GuardarLogXNombreConectionString(18, 0, 2, "DALGeneral-CambiarContrasena " + ex.Message, ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de enviar los valores del configurador de CardWizard
    ''' </summary>
    ''' <param name="idValor">Identificador del valor</param>
    ''' <param name="conexionBD">Conexion de la base de datos</param>
    ''' <returns>retorna valor solicitado</returns>
    ''' <remarks>AERM 16/03/2015</remarks>
    Public Function ObtenerValoresConfigurador(idValor As String, conexionBD As String) As String Implements IDALGeneral.ObtenerValoresConfigurador
        Try
            Dim operacionesBD As OperacionesBase = New OperacionesBase()
            Return operacionesBD.ObtenerValoresConfigurador(idValor, conexionBD)
        Catch ex As Exception
            Throw ex
        End Try
        Return String.Empty
    End Function

    ''' <summary>
    ''' Metodo encargado de devolver la cadena de conexion configurada en la base de datos
    ''' </summary>
    ''' <param name="idConeString">Nombre del conexion String</param>
    ''' <returns>Retorna cadena de conexion por el nombre</returns>
    ''' <remarks>AERM 29/01/2015</remarks>
    Private Function CadenaConexion(ByVal idConeString As String) As String
        Try
            Return ConfigurationManager.ConnectionStrings(idConeString).ConnectionString
        Catch ex As Exception
            Throw ex
        End Try
    End Function
   
End Class
