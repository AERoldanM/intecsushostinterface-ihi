﻿Imports Intecsus.Entidades

''' <summary>
''' Clase que represneta las operaciones a la BD donde se ejecutan operaciones del configurador
''' </summary>
''' <remarks>AERM 06/04/2015</remarks>
Public Interface IDALGeneral

    ''' <summary>
    ''' Metodo encargado de retornar las credenciales de un usuario
    ''' </summary>
    ''' <param name="user">Objeto usuario</param>
    ''' <returns>Retorna usuario con las credenciales</returns>
    ''' <remarks>AERM 06/04/2015</remarks>
    Function ValidarUsuario(ByVal user As Usuario, conexion As String) As List(Of Usuario)

    ''' <summary>
    ''' Metodo encargado de retornar la lista de los valores a configurar
    ''' </summary>
    ''' <returns>Retorna lista de configuracion</returns>
    ''' <remarks>AERM 07/04/2015</remarks>
    Function ObtenerConfiguracion(conexion As String) As List(Of Configurar)

    ''' <summary>
    ''' Metodo encargado de guardar alguna operacion en el log
    ''' </summary>
    ''' <param name="idProceso">Identificador del proceso en el cual s eesta trabajando</param>
    ''' <param name="idTransaccion">Identificador de la transaccion</param>
    ''' <param name="evento">Evento que esta pasando 1-Informativo 2- Error aplicacion</param>
    ''' <param name="descripcionEnvento">Descripcion de lo que esta sucediendo</param>
    ''' <param name="nombreConexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 06/04/2015</remarks>
    Sub GuardarLog(idProceso As Integer, idTransaccion As Integer, evento As Integer, descripcionEnvento As String, nombreConexion As String)

    ''' <summary>
    ''' Metodo encargado de actualizar los datos
    ''' </summary>
    ''' <param name="datoActualizar">Datos a actualizar</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Sub ActualizarConfiguracion(datoActualizar As Configurar, conexion As String)

    ''' <summary>
    ''' Metodo encargado de insertar los datos
    ''' </summary>
    ''' <param name="datoActualizar">Datos a actualizar</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Sub IngresarConfiguracion(datoActualizar As Configurar, conexion As String)

    ''' <summary>
    ''' Metodo encargado de reotrnar las impresoras creadas en el sistema
    ''' </summary>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <returns>Retorna Lista de impresoras creadas</returns>
    ''' <remarks>AERM 07/04/2015</remarks>
    Function ObtenerImpresoras(conexion As String) As List(Of Impresora)

    ''' <summary>
    ''' Metodo encargado de insertar una impresora
    ''' </summary>
    ''' <param name="impresora">Impresora a ingresar</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Sub InsertarImpresora(impresora As Impresora, conexion As String)

    ''' <summary>
    ''' Metodo encargado de actualizar una impresora
    ''' </summary>
    ''' <param name="impresora">Impresora q se debe actualizar</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Sub ActualizarImpresora(impresora As Impresora, conexion As String)

    ''' <summary>
    ''' Metodo encargado de Eliminar una impresora
    ''' </summary>
    ''' <param name="impresora">Impresora q se debe Eliminar</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Sub EliminarImpresora(impresora As Impresora, conexion As String)

    ''' <summary>
    ''' Metodo encargado de traer la lista de prodcutos asociados alos cardNAme
    ''' </summary>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <returns>Retorna la lista de productos</returns>
    ''' <remarks>AERM 07/04/2015</remarks>
    Function ObtenerProductos(conexion As String) As List(Of ProductoXCW)

    ''' <summary>
    ''' Metodo encargado de producto una impresora
    ''' </summary>
    ''' <param name="producto">producto a ingresar</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Sub InsertarProducto(producto As ProductoXCW, conexion As String)

    ''' <summary>
    ''' Metodo encargado de actualizar un producto 
    ''' </summary>
    ''' <param name="producto">producto actualizar</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Sub ActualizarProducto(producto As ProductoXCW, conexion As String)

    ''' <summary>
    ''' Metodo encargado de Eliminar un producto 
    ''' </summary>
    ''' <param name="producto">Eliminar actualizar</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Sub EliminarProducto(producto As ProductoXCW, conexion As String)

    ''' <summary>
    ''' Metodo encargado de traer la lista de procesosXAprobar
    ''' </summary>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <returns>Retorna lista de PorcesoxAP</returns>
    ''' <remarks>AERM 07/04/2015</remarks>
    Function ObtenerProcesosXAP(conexion As String) As List(Of ProcesoXAP)

    ''' <summary>
    ''' Metodo encargado de actualizar un procesosXAprobar
    ''' </summary>
    ''' <param name="procesoXAP">Proceso a actualizar</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Sub ActualizarProcesoXAP(procesoXAP As ProcesoXAP, conexion As String)

    ''' <summary>
    ''' Metodo encargado de obtener los logs de la aplicacion
    ''' </summary>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <returns>retorna Lista de logs</returns>
    ''' <remarks>AERM 08/04/2015</remarks>
    Function ObtenerLogs(conexion As String) As List(Of LOG)

    ''' <summary>
    ''' Metodo encargado de obtener los logs de la aplicacionXPaginacion
    ''' </summary>
    ''' <param name="pagina" >Pagina en la cual retorna los registros</param>
    ''' <param name="registro">Numero de registros por pagina</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <returns>Retorna Lista de logs</returns>
    ''' <remarks>AERM 20/04/2015</remarks>
    Function ObtenerLogs(registro As Integer, pagina As Integer, conexion As String) As List(Of LOG)


    ''' <summary>
    ''' Metodo encargado de obtener los logs de alguna transaccion
    ''' </summary>
    ''' <param name="transaccion">Identificador de la transaccion en el motor</param>
    ''' <param name="transaccionCliente">Identificador de la transaccion para el cliente</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <param name="fechaIni">Fecha inicial para realizar la bsuqueda</param>
    ''' <param name="fechaFin">Fecha final para realizar la busqueda</param>
    ''' <returns>retorna lista de logs</returns>
    ''' <remarks>AERM 08/04/2015</remarks>
    Function ObtenerLogXTran(transaccion As Integer, transaccionCliente As String, fechaIni As String, fechaFin As String, conexion As String) As List(Of LOG)


    ''' <summary>
    ''' Metodo encargado de obtener los EstadoTransaccion de la aplicacion
    ''' </summary>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <returns>Retorna Lista de EstadoTransaccion</returns>
    ''' <remarks>AERM 08/04/2015</remarks>
    Function ObtenerEstadoTransaccion(conexion As String) As List(Of EstadoTransaccion)

    ''' <summary>
    ''' Metodo encargado de obtener los EstadoTransaccion de la aplicacion
    ''' </summary>
    ''' <param name="transaccion">Identificador de la transaccion en el motor</param>
    ''' <param name="transaccionCliente">Identificador de la transaccion para el cliente</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <returns>Retorna Lista de EstadoTransaccion</returns>
    ''' <remarks>AERM 08/04/2015</remarks>
    Function ObtenerEstadoTransaXTran(transaccion As Integer, transaccionCliente As String, conexion As String) As List(Of EstadoTransaccion)

    ''' <summary>
    ''' Metodo encargado de cambair la contrasena
    ''' </summary>
    ''' <param name="usuario">Usuario a cambiar la contrasena</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 08/04/2015</remarks>
    Sub CambiarContrasena(usuario As Usuario, conexion As String)

    ''' <summary>
    ''' Metodo encargado de enviar los valores del configurador de CardWizard
    ''' </summary>
    ''' <param name="idValor">Identificador del valor</param>
    ''' <param name="conexionBD">Conexion de la base de datos</param>
    ''' <returns>retorna valor solicitado</returns>
    ''' <remarks>AERM 23/04/2015</remarks>
    Function ObtenerValoresConfigurador(idValor As String, conexionBD As String) As String


End Interface

