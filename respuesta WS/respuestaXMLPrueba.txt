<?xml version="1.0" encoding="utf-16"?>
<RespuestaRealceEmisionTarjetasDTO xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <ExtensionData />
  <codigoRespuestaField>00</codigoRespuestaField>
  <descripcionRespuestaField />
  <numeroAutorizacionField>1234567</numeroAutorizacionField>
  <codigoPuntoDistribucionField>001</codigoPuntoDistribucionField>
  <convenioMercadeoField>209</convenioMercadeoField>
  <nombreCompletoField>Juan Pablo Castel</nombreCompletoField>
  <numeroIdentificacionBField>000000981285</numeroIdentificacionBField>
  <numeroTarjetaAfiliacionField>8800039000009202</numeroTarjetaAfiliacionField>
  <numeroTarjetaField>8800039000009178</numeroTarjetaField>
  <tipoIdentificacionAField>2</tipoIdentificacionAField>
  <tipoIdentificacionBField>0</tipoIdentificacionBField>
  <trackIField>
    <ExtensionData />
    <beneficiarioField>981285       Juan Pablo Castel</beneficiarioField>
    <caracterAField>^</caracterAField>
    <caracterBField>^</caracterBField>
    <caracterCField>^</caracterCField>
    <caracterInicioField>%</caracterInicioField>
    <caracteresControlField>000000000000</caracteresControlField>
    <fechaVencimientoField>6012</fechaVencimientoField>
    <formatoField>B</formatoField>
    <panField>8800039000009202</panField>
    <separadorField>;</separadorField>
  </trackIField>
  <trackIIField>
    <ExtensionData />
    <codigoServicioField>501</codigoServicioField>
    <cvc1Field>045</cvc1Field>
    <fechaVencimientoField>6012</fechaVencimientoField>
    <impresionAnversoField>352</impresionAnversoField>
    <panField>8800039000009202</panField>
    <pinOffField>9999999999999999</pinOffField>
    <pvkiField>1</pvkiField>
    <pvvField>0000</pvvField>
    <separadorCampoField>=</separadorCampoField>
    <separadorField>?</separadorField>
  </trackIIField>
</RespuestaRealceEmisionTarjetasDTO>