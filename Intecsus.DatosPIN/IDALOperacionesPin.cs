﻿using Intecsus.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intecsus.DatosPIN
{
    public interface IDALOperacionesPin
    {
        /// <summary>
        /// Metodo encargado de realizar login
        /// </summary>
        /// <param name="id">Identificador del usuario</param>
        /// <param name="contrasena">Contraseña de usuario</param>
        /// <param name="conexion">Conexion con BD</param>
        /// <returns>Retorna objeto con respuesta y permisos del usuario</returns>
        /// <remarks> AERM 11/07/2016</remarks>
        UsuarioPIN Ingresar(string id, string contrasena, string conexion);

        /// <summary>
        /// Metodo encargado de obtener las operaciones que puede realizar el usuario
        /// </summary>
        /// <param name="userID">Identificador de usuario para obtener las operaciones q puede realizar</param>
        /// <param name="conexion">Conexion con BD</param>
        /// <returns>Retorna respuesta con los objetos de configuracion </returns>
        /// <remarks> AERM 12/07/2016</remarks>
        Operaciones  OperacionesXUsuario(string userID, string conexion);

        /// <summary>
        /// Metodo encargado de obtener parametros de configuracion
        /// </summary>
        /// <param name="conexion">Conexion con BD</param>
        /// <returns>Retorna respuesta con los objetos de configuracion </returns>
        /// <remarks> AERM 12/07/2016</remarks>
        List<Configuracion> Configuracion(string conexion);

        /// <summary>
        /// Metodo encargado de obtener los perfiles del sistema
        /// </summary>
        /// <param name="conexion">Conexion con BD</param>
        /// <returns>Retorna respuesta con los perfiles </returns>
        /// <remarks> AERM 12/07/2016</remarks>
        List<Perfil> Perfiles(string conexion);

        /// <summary>
        /// Metodo encargado de listar los usuarios del sistema
        /// </summary>
        /// <param name="conexion">Conexion con BD</param>
        /// <returns>Retorna lista de usuarios</returns>
        /// <remarks> AERM 11/07/2016</remarks>
        List<UsuarioPIN> ListarUsuario(string conexion);

        /// <summary>
        /// Metodo encargado de crear un usuario en el sistema
        /// </summary>
        /// <param name="user">Usuario a crear</param>
        /// <param name="conexion">Conexion con BD</param>
        /// <returns>Retorna si fue creado o no</returns>
        /// <remarks> AERM 11/07/2016</remarks>
        Boolean CrearUsuario(UsuarioPIN user, string conexion);

        /// <summary>
        /// Metodo encargado de eliminar un usuario 
        /// </summary>
        /// <param name="userIDDelete">Id usuario que  se desea eliminar </param>
        /// <param name="conexion">Conexion con BD</param>
        /// <returns>Retorna el resultado de la operacion</returns>
        /// <remarks> AERM 11/07/2016</remarks>
        Boolean EliminarUsuario(string userIDDelete,  string conexion);

        /// <summary>
        /// Metodo encargado de editar un usuario 
        /// </summary>
        /// <param name="userIDDelete">Id usuario que  se desea eliminar </param>
        /// <param name="conexion">Conexion con BD</param>
        /// <returns>Retorna el resultado de la operacion</returns>
        /// <remarks> AERM 11/07/2016</remarks>
        Boolean EditarUsuario(UsuarioPIN userEditar, string conexion);

        /// <summary>
        /// Metodo encargado de asignar contraseña
        /// </summary>
        /// <param name="contrasenaNueva">Contraseña nueva a almacenar</param>
        /// <param name="conexion">Conexion con BD</param>
        /// <returns>Retorna el resultado de la operacion</returns>
        /// <remarks> AERM 11/07/2016</remarks>
        Boolean AsignarContrasena(string contrasenaNueva,  string userID, string conexion);

        /// <summary>
        /// Metodo encargado de cambiar la contraseña del usuario
        /// </summary>
        /// <param name="userID">Identificador del usuario</param>
        /// <param name="contrasena">Contraseña a cmabiar</param>
        /// <param name="conexion">Conexion con BD</param>
        /// <returns>Retorna resultado de la operacion</returns>
        /// <remarks> AERM 11/07/2016</remarks>
        Boolean CambiarContrena(string userID, string contrasena, string conexion);

      
        /// <summary>
        /// Metodo encargado de almacenar en la tabla log
        /// </summary>
        /// <param name="evento">Tipo 1 o 2 segun si es error o no</param>
        /// <param name="descripcion">Desripcion del evento</param>
        /// <param name="conexion">Conexion con BD</param>
        /// <remarks> AERM 11/07/2016</remarks>
        void GuardarLOG(int evento, string descripcion, string conexion);

        /// <summary>
        /// Metodo encargado de encontrar un mensaje de error
        /// </summary>
        /// <param name="codigoMensaje">Codigo del mensaje</param>
        /// <param name="conexion">Conexion con BD</param>
        /// <returns>Retorna mensaje</returns>
        string ObtenerMensaje(string codigoMensaje, string conexion);

        /// <summary>
        /// Metodo encargado de encontrar una conciliacion previa realizada
        /// </summary>
        /// <param name="panSha">Pan en SHa 512</param>
        /// <param name="cliente"> Cliente que ejecuta la trnasaccion</param>
        /// <param name="conexion">Conexion con BD</param>
        /// <returns>Retorna conciliacion si existe</returns>
        Conciliacion ObtenerConciliacion(string panSha, string cliente ,string conexion);

        /// <summary>
        /// Metodo encargado d obtener el valor de configuracion especifico
        /// </summary>
        /// <param name="nombre">Nombre por el cual se busca</param>
        /// <param name="conexion">Conexion con la BD</param>
        /// <returns>Retorna valor especifico</returns>
        string ObtenerValorConfiguracion(string nombre, string conexion);

        /// <summary>
        /// Metodo encargado de modifcar  la conciliacion (Asigno PIN)
        /// </summary>
        /// <param name="idTransaccion">Identificador de la transaccion</param>
        /// <param name="conexion">Conexion con la BD</param>
        void ActializarEstadoAsignacionPIN(string idTransaccion, string conexion);

        /// <summary>
        /// Metodo encargado de Obtneer la contraseña de un usuario
        /// </summary>
        /// <param name="idUsuario">Identificador del usuario</param>
        /// <param name="conexion">Conexion con la BD</param>
        /// <returns>Retorna contraseña del usuario</returns>
        string ObtenerContrasenaXUsuario(string idUsuario, string conexion);

         /// <summary>
       /// Metodo encargado de guardar
       /// </summary>
       /// <param name="cliente">CLiente q solicita la peticion</param>
       /// <param name="idTransaccionCliente">Identificador de la transaccion (Para el cliente)</param>
       /// <param name="conexionBD">Identificador coenxion con la BD</param>
       /// <returns>Retorna idtransaccion del sistema</returns>
       /// <remarks>AERM 17/03/2015</remarks>
        int InsertarTransaccion(string cliente, string idTransaccionCliente, string conexionBD);
    }
}
