﻿using Intecusus.OperarBD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Intecsus.Entidades;
using System.Data;
using System.Configuration;

namespace Intecsus.DatosPIN
{
    public class DALOperacionesPin : IDALOperacionesPin
    {
        public UsuarioPIN Ingresar(string id, string contrasena, string conexion)
        {
            UsuarioPIN m_user = null;
            try 
            {
                IDALBaseDatos m_Conexion = OperacionesBD.Instancia.operarBD();
                List<Parametro<SqlDbType>> m_ListaParametros = new List<Parametro<SqlDbType>>();
                Parametro<SqlDbType> m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Identificador";
                m_Parametro.valorEnviar = id;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = id.Length;
                m_ListaParametros.Add(m_Parametro);

                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Contrasena";
                m_Parametro.valorEnviar = contrasena;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = contrasena.Length;
                m_ListaParametros.Add(m_Parametro);

                DataSet m_Resul = m_Conexion.EjecutarSP(this.CadenaConexion(conexion), "SP_IngresoUsuario", "Parametros", m_ListaParametros);
                if (m_Resul.Tables.Count > 0 && m_Resul.Tables[0].Rows.Count > 0) 
                {
                    m_user = new UsuarioPIN();
                    m_user.perfil = new Perfil();
                    m_user.Identificacion = id;
                    m_user.contrasena = contrasena;
                    m_user.IDUsuario = Convert.ToInt32(m_Resul.Tables[0].Rows[0]["IdUsuario"].ToString());
                    m_user.perfil.IDPerfil = Convert.ToInt32(m_Resul.Tables[0].Rows[0]["IdPerfil"].ToString());
                    m_user.Nombre = m_Resul.Tables[0].Rows[0]["Nombre"].ToString();
                    m_user.Apellido = m_Resul.Tables[0].Rows[0]["Apellido"].ToString();
                    m_user.Email = m_Resul.Tables[0].Rows[0]["Email"].ToString();
                    m_user.Telefono = m_Resul.Tables[0].Rows[0]["Telefono"].ToString();
                    m_user.Activo = Convert.ToBoolean(m_Resul.Tables[0].Rows[0]["Activo"].ToString());
                    m_user.ContrasenaInicial = Convert.ToBoolean(m_Resul.Tables[0].Rows[0]["ContrasenaInicial"].ToString());
                }
            }
            catch (Exception ex)
            { new Exception("DALOperacionesPin -" + ex.Message); }

            return m_user;
        }


        public Operaciones OperacionesXUsuario(string userID, string conexion)
        {
            Operaciones m_operacion = new Operaciones();
            try 
            {
                IDALBaseDatos m_Conexion = OperacionesBD.Instancia.operarBD();
                List<Parametro<SqlDbType>> m_ListaParametros = new List<Parametro<SqlDbType>>();
                Parametro<SqlDbType> m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Identificador";
                m_Parametro.valorEnviar = userID;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = userID.Length;
                m_ListaParametros.Add(m_Parametro);

                DataSet m_Resul = m_Conexion.EjecutarSP(this.CadenaConexion(conexion), "SP_OperacionesXUsuario", "Parametros", m_ListaParametros);
                if (m_Resul.Tables.Count > 0 && m_Resul.Tables[0].Rows.Count > 0)
                {
                    m_operacion.CrearUsuario = Convert.ToBoolean(m_Resul.Tables[0].Rows[0]["CrearUsuario"].ToString());
                    m_operacion.ModUsuario = Convert.ToBoolean(m_Resul.Tables[0].Rows[0]["ModificarUsuario"].ToString());
                    m_operacion.ListarUsuario = Convert.ToBoolean(m_Resul.Tables[0].Rows[0]["ListarUsuario"].ToString());
                    m_operacion.EliminarUsuario = Convert.ToBoolean(m_Resul.Tables[0].Rows[0]["EliminarUsario"].ToString());
                    m_operacion.CambiarContrasena = Convert.ToBoolean(m_Resul.Tables[0].Rows[0]["CambiarContrasena"].ToString());
                    m_operacion.AsignarPin = Convert.ToBoolean(m_Resul.Tables[0].Rows[0]["AsignarPin"].ToString());
                    m_operacion.ListarPerfiles = Convert.ToBoolean(m_Resul.Tables[0].Rows[0]["ListarPerfiles"].ToString());
                    m_operacion.ConsultarConfiguracion = Convert.ToBoolean(m_Resul.Tables[0].Rows[0]["ConsultarConfiguracion"].ToString());
                    m_operacion.AsignarContrasena = Convert.ToBoolean(m_Resul.Tables[0].Rows[0]["AsignarContrasena"].ToString());
                }
            }
            catch (Exception ex)
            { new Exception("DALOperacionesPin -" + ex.Message); }

            return m_operacion;
        }


        public List<Configuracion> Configuracion(string conexion)
        {
            List<Configuracion> m_Configuraciones = new List<Configuracion>();
            try 
            {
                IDALBaseDatos m_Conexion = OperacionesBD.Instancia.operarBD();
                List<Parametro<SqlDbType>> m_ListaParametros = new List<Parametro<SqlDbType>>();
                Parametro<SqlDbType> m_Parametro = new Parametro<SqlDbType>();

                DataSet m_Resul = m_Conexion.EjecutarSP(this.CadenaConexion(conexion), "SP_ObtenerConfiguracion", "Parametros", m_ListaParametros);
                if (m_Resul.Tables.Count > 0 && m_Resul.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow m_row in m_Resul.Tables[0].Rows) 
                    {
                        Configuracion m_Configuracion = new Configuracion();
                        m_Configuracion.Nombre = m_row["Nombre"].ToString();
                        m_Configuracion.Valor = m_row["Valor"].ToString(); 
                        m_Configuraciones.Add(m_Configuracion);
                    }
                }
            }
            catch (Exception ex)
            { new Exception("DALOperacionesPin -" + ex.Message); }

            return m_Configuraciones;
        }

        public List<Perfil> Perfiles(string conexion)
        {
            List<Perfil> m_Perfiles = new List<Perfil>();
            try
            {
                IDALBaseDatos m_Conexion = OperacionesBD.Instancia.operarBD();
                List<Parametro<SqlDbType>> m_ListaParametros = new List<Parametro<SqlDbType>>();
                Parametro<SqlDbType> m_Parametro = new Parametro<SqlDbType>();

                DataSet m_Resul = m_Conexion.EjecutarSP(this.CadenaConexion(conexion), "SP_ObtenerPerfiles", "Parametros", m_ListaParametros);
                if (m_Resul.Tables.Count > 0 && m_Resul.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow m_row in m_Resul.Tables[0].Rows)
                    {
                        Perfil m_Perfil = new Perfil();
                        m_Perfil.IDPerfil = Convert.ToInt32(m_row["IdPerfil"].ToString());
                        m_Perfil.NombrePerfil  = m_row["NombrePerfil"].ToString();
                        m_Perfiles.Add(m_Perfil);
                    }
                }
            }
            catch (Exception ex)
            { new Exception("DALOperacionesPin -" + ex.Message); }

            return m_Perfiles;
        }

        public List<UsuarioPIN> ListarUsuario(string conexion)
        {
            List<UsuarioPIN> m_Usuarios = new List<UsuarioPIN>();
            try
            {
                IDALBaseDatos m_Conexion = OperacionesBD.Instancia.operarBD();
                List<Parametro<SqlDbType>> m_ListaParametros = new List<Parametro<SqlDbType>>();
                Parametro<SqlDbType> m_Parametro = new Parametro<SqlDbType>();

                DataSet m_Resul = m_Conexion.EjecutarSP(this.CadenaConexion(conexion), "SP_ObtenerUsuarios", "Parametros", m_ListaParametros);
                if (m_Resul.Tables.Count > 0 && m_Resul.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow m_row in m_Resul.Tables[0].Rows)
                    {
                        UsuarioPIN m_user = new UsuarioPIN();
                        m_user.perfil = new Perfil();
                        m_user.IDUsuario = Convert.ToInt32(m_row["IdUsuario"].ToString());
                        m_user.Identificacion = m_row["Identificador"].ToString();
                        m_user.perfil.IDPerfil = Convert.ToInt32(m_row["IdPerfil"].ToString());
                        m_user.Nombre = m_row["Nombre"].ToString();
                        m_user.Apellido = m_row["Apellido"].ToString();
                        m_user.Email = m_row["Email"].ToString();
                        m_user.Telefono = m_row["Telefono"].ToString();
                        m_user.contrasena = m_row["Contrasena"].ToString();
                        m_user.Activo = Convert.ToBoolean(m_row["Activo"].ToString());
                        m_user.ContrasenaInicial = Convert.ToBoolean(m_row["ContrasenaInicial"].ToString());

                        m_Usuarios.Add(m_user);
                    }
                }
            }
            catch (Exception ex)
            { new Exception("DALOperacionesPin -" + ex.Message); }

            return m_Usuarios;
        }

        public Boolean CrearUsuario(UsuarioPIN user, string conexion)
        {
            try 
            {
                IDALBaseDatos m_Conexion = OperacionesBD.Instancia.operarBD();
                List<Parametro<SqlDbType>> m_ListaParametros = new List<Parametro<SqlDbType>>();
                Parametro<SqlDbType> m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Identificador";
                m_Parametro.valorEnviar = user.Identificacion;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = user.Identificacion.Length;
                m_ListaParametros.Add(m_Parametro);

                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@IdPerfil";
                m_Parametro.valorEnviar = user.perfil.IDPerfil.ToString();
                m_Parametro.tipoDato = SqlDbType.Int;
                m_Parametro.tamano = user.perfil.IDPerfil.ToString().Length;
                m_ListaParametros.Add(m_Parametro);

                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Nombre";
                m_Parametro.valorEnviar = user.Nombre;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = user.Nombre.Length;
                m_ListaParametros.Add(m_Parametro);

                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Apellido";
                m_Parametro.valorEnviar = user.Apellido;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = user.Apellido.Length;
                m_ListaParametros.Add(m_Parametro);

                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Email";
                m_Parametro.valorEnviar = user.Email;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = user.Email.Length;
                m_ListaParametros.Add(m_Parametro);

                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Telefono";
                m_Parametro.valorEnviar = user.Telefono;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = user.Telefono.Length;
                m_ListaParametros.Add(m_Parametro);

                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Contrasena";
                m_Parametro.valorEnviar = user.contrasena;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = user.contrasena.Length;
                m_ListaParametros.Add(m_Parametro);

                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Activo";
                m_Parametro.valorEnviar = user.Activo.ToString();
                m_Parametro.tipoDato = SqlDbType.Bit;
                m_Parametro.tamano = user.Activo.ToString().Length;
                m_ListaParametros.Add(m_Parametro);

                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@ContrasenaInicial";
                m_Parametro.valorEnviar = user.ContrasenaInicial.ToString();
                m_Parametro.tipoDato = SqlDbType.Bit;
                m_Parametro.tamano = user.ContrasenaInicial.ToString().Length;
                m_ListaParametros.Add(m_Parametro);

                m_Conexion.EjecutarSP(this.CadenaConexion(conexion), "SP_CrearUsuario", "Parametros", m_ListaParametros);
            }
            catch (Exception ex)
            { new Exception("DALOperacionesPin -" + ex.Message); }

            return true;
        }

        public Boolean EliminarUsuario( string userIDDelete, string conexion)
        {
            try
            {
                IDALBaseDatos m_Conexion = OperacionesBD.Instancia.operarBD();
                List<Parametro<SqlDbType>> m_ListaParametros = new List<Parametro<SqlDbType>>();
                Parametro<SqlDbType> m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Identificador";
                m_Parametro.valorEnviar = userIDDelete;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = userIDDelete.Length;
                m_ListaParametros.Add(m_Parametro);

                m_Conexion.EjecutarSP(this.CadenaConexion(conexion), "SP_EliminarUsuario", "Parametros", m_ListaParametros);
            }
            catch (Exception ex)
            { new Exception("DALOperacionesPin -" + ex.Message); }

            return true;
        }

        public Boolean EditarUsuario(Entidades.UsuarioPIN user, string conexion)
        {
            try
            {
                IDALBaseDatos m_Conexion = OperacionesBD.Instancia.operarBD();
                List<Parametro<SqlDbType>> m_ListaParametros = new List<Parametro<SqlDbType>>();
                Parametro<SqlDbType> m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Identificador";
                m_Parametro.valorEnviar = user.Identificacion;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = user.Identificacion.Length;
                m_ListaParametros.Add(m_Parametro);

                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@IdPerfil";
                m_Parametro.valorEnviar = user.perfil.IDPerfil.ToString();
                m_Parametro.tipoDato = SqlDbType.Int;
                m_Parametro.tamano = user.perfil.IDPerfil.ToString().Length;
                m_ListaParametros.Add(m_Parametro);

                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Nombre";
                m_Parametro.valorEnviar = user.Nombre;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = user.Nombre.Length;
                m_ListaParametros.Add(m_Parametro);

                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Apellido";
                m_Parametro.valorEnviar = user.Apellido;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = user.Apellido.Length;
                m_ListaParametros.Add(m_Parametro);

                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Email";
                m_Parametro.valorEnviar = user.Email;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = user.Email.Length;
                m_ListaParametros.Add(m_Parametro);

                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Telefono";
                m_Parametro.valorEnviar = user.Telefono;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = user.Telefono.Length;
                m_ListaParametros.Add(m_Parametro);

                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Contrasena";
                m_Parametro.valorEnviar = user.contrasena;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = user.contrasena.Length;
                m_ListaParametros.Add(m_Parametro);

                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Activo";
                m_Parametro.valorEnviar = user.Activo.ToString();
                m_Parametro.tipoDato = SqlDbType.Bit;
                m_Parametro.tamano = user.Activo.ToString().Length;
                m_ListaParametros.Add(m_Parametro);

                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@ContrasenaInicial";
                m_Parametro.valorEnviar = user.ContrasenaInicial.ToString();
                m_Parametro.tipoDato = SqlDbType.Bit;
                m_Parametro.tamano = user.ContrasenaInicial.ToString().Length;
                m_ListaParametros.Add(m_Parametro);

                m_Conexion.EjecutarSP(this.CadenaConexion(conexion), "SP_EditarUsuario", "Parametros", m_ListaParametros);
            }
            catch (Exception ex)
            { new Exception("DALOperacionesPin -" + ex.Message); }

            return true;
        }

        public Boolean AsignarContrasena( string contrasenaNueva, string userID,  string conexion)
        {
            try
            {
                IDALBaseDatos m_Conexion = OperacionesBD.Instancia.operarBD();
                List<Parametro<SqlDbType>> m_ListaParametros = new List<Parametro<SqlDbType>>();
                Parametro<SqlDbType> m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Identificador";
                m_Parametro.valorEnviar = userID;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = userID.Length;
                m_ListaParametros.Add(m_Parametro);
               
                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Contrasena";
                m_Parametro.valorEnviar = contrasenaNueva;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = contrasenaNueva.Length;
                m_ListaParametros.Add(m_Parametro);

                m_Conexion.EjecutarSP(this.CadenaConexion(conexion), "SP_AsignarContrasenaUsuario", "Parametros", m_ListaParametros);
            }
            catch (Exception ex)
            { new Exception("DALOperacionesPin -" + ex.Message); }

            return true;
        }

        public Boolean CambiarContrena(string userID, string contrasena,  string conexion)
        {
            try
            {
                IDALBaseDatos m_Conexion = OperacionesBD.Instancia.operarBD();
                List<Parametro<SqlDbType>> m_ListaParametros = new List<Parametro<SqlDbType>>();
                Parametro<SqlDbType> m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Identificador";
                m_Parametro.valorEnviar = userID;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = userID.Length;
                m_ListaParametros.Add(m_Parametro);

                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Contrasena";
                m_Parametro.valorEnviar = contrasena;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = contrasena.Length;
                m_ListaParametros.Add(m_Parametro);

                m_Conexion.EjecutarSP(this.CadenaConexion(conexion), "SP_CambiarContrasena", "Parametros", m_ListaParametros);
            }
            catch (Exception ex)
            { new Exception("DALOperacionesPin -" + ex.Message); }

            return true;
        }

       
        public void GuardarLOG(int evento, string descripcion, string conexion)
        {
            try
            {
                IDALBaseDatos m_Conexion = OperacionesBD.Instancia.operarBD();
                List<Parametro<SqlDbType>> m_ListaParametros = new List<Parametro<SqlDbType>>();
                Parametro<SqlDbType> m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Evento";
                m_Parametro.valorEnviar = evento.ToString();
                m_Parametro.tipoDato = SqlDbType.Int;
                m_Parametro.tamano = evento.ToString().Length;
                m_ListaParametros.Add(m_Parametro);

                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Descripcion";
                m_Parametro.valorEnviar = descripcion;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = descripcion.Length;
                m_ListaParametros.Add(m_Parametro);

                m_Conexion.EjecutarSP(this.CadenaConexion(conexion), "SP_GuardarLog", "Parametros", m_ListaParametros);
            }
            catch (Exception ex)
            {
                throw new Exception("DALOperacionesPin -" + ex.Message);
            }
        }


        public string ObtenerMensaje(string codigoMensaje, string conexion)
        {
            try
            {
                IDALBaseDatos m_Conexion = OperacionesBD.Instancia.operarBD();
                List<Parametro<SqlDbType>> m_ListaParametros = new List<Parametro<SqlDbType>>();
                Parametro<SqlDbType> m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@CODIGO";
                m_Parametro.valorEnviar = codigoMensaje;
                m_Parametro.tipoDato = SqlDbType.Int;
                m_Parametro.tamano = codigoMensaje.Length;
                m_ListaParametros.Add(m_Parametro);

                DataSet m_Resul = m_Conexion.EjecutarSP(this.CadenaConexion(conexion), "SP_ObtenerMensajeXCodigo", "Parametros", m_ListaParametros);
                if (m_Resul.Tables.Count > 0 && m_Resul.Tables[0].Rows.Count > 0)
                    return m_Resul.Tables[0].Rows[0]["Mensaje"].ToString();

            }
            catch (Exception ex)
            {
                throw new Exception("ObtenerMensaje -" + ex.Message);
            }

            return string.Empty;
        }

        public Conciliacion ObtenerConciliacion(string panSha, string cliente, string conexion) 
        {
            Conciliacion m_Respuesta = null;
            try
            {
                IDALBaseDatos m_Conexion = OperacionesBD.Instancia.operarBD();
                List<Parametro<SqlDbType>> m_ListaParametros = new List<Parametro<SqlDbType>>();
                Parametro<SqlDbType> m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@PANSHA";
                m_Parametro.valorEnviar = panSha;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = panSha.Length;
                m_ListaParametros.Add(m_Parametro);

                m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Cliente";
                m_Parametro.valorEnviar = cliente;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = cliente.Length;
                m_ListaParametros.Add(m_Parametro);

                DataSet m_Resul = m_Conexion.EjecutarSP(this.CadenaConexion(conexion), "SP_ObtenerConciliacionXSHA", "Parametros", m_ListaParametros);
                if (m_Resul.Tables.Count > 0 && m_Resul.Tables[0].Rows.Count > 0) 
                {
                    m_Respuesta = new Conciliacion();
                    m_Respuesta.Cliente = cliente;
                    m_Respuesta.IdTransaccion = Convert.ToInt32(m_Resul.Tables[0].Rows[0]["Idtransaccion"].ToString());
                    m_Respuesta.IDTransaccionCliente = m_Resul.Tables[0].Rows[0]["IDTransaccionCliente"].ToString();
                    m_Respuesta.PAN = m_Resul.Tables[0].Rows[0]["PAN"].ToString();
                    m_Respuesta.CODFUMO = m_Resul.Tables[0].Rows[0]["CODFUMO"].ToString();
                    m_Respuesta.Canal = m_Resul.Tables[0].Rows[0]["CANAL"].ToString();
                    m_Respuesta.INDES = m_Resul.Tables[0].Rows[0]["INDES"].ToString();
                    m_Respuesta.Usuario = m_Resul.Tables[0].Rows[0]["Usuario"].ToString();
                    m_Respuesta.Fecha = Convert.ToDateTime(m_Resul.Tables[0].Rows[0]["Fecha"].ToString());
                    m_Respuesta.CodMen = m_Resul.Tables[0].Rows[0]["CodMen"].ToString();
                    m_Respuesta.Descripcion1 = m_Resul.Tables[0].Rows[0]["Descripcion1"].ToString();
                    m_Respuesta.PIN = Convert.ToBoolean(m_Resul.Tables[0].Rows[0]["Pin"].ToString());
                }
            }
            catch (Exception ex)
            {
                throw new Exception("ObtenerConciliacion -" + ex.Message);
            }

            return m_Respuesta;
        }


        public string ObtenerValorConfiguracion(string nombre, string conexion)
        {
            try
            {
                IDALBaseDatos m_Conexion = OperacionesBD.Instancia.operarBD();
                List<Parametro<SqlDbType>> m_ListaParametros = new List<Parametro<SqlDbType>>();
                Parametro<SqlDbType> m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Nombre";
                m_Parametro.valorEnviar = nombre;
                m_Parametro.tipoDato = SqlDbType.VarChar;
                m_Parametro.tamano = nombre.Length;
                m_ListaParametros.Add(m_Parametro);

                DataSet m_Resul = m_Conexion.EjecutarSP(this.CadenaConexion(conexion), "SP_ConfigXNombre", "Parametros", m_ListaParametros);
                if (m_Resul.Tables.Count > 0 && m_Resul.Tables[0].Rows.Count > 0)
                    return m_Resul.Tables[0].Rows[0]["Valor"].ToString();

            }
            catch (Exception ex)
            {
                throw new Exception("ObtenerValorConfiguracion -" + ex.Message);
            }

            return string.Empty;
        }

        public void ActializarEstadoAsignacionPIN(string idTransaccion, string conexion) 
        {
            try
            {
                IDALBaseDatos m_Conexion = OperacionesBD.Instancia.operarBD();
                List<Parametro<SqlDbType>> m_ListaParametros = new List<Parametro<SqlDbType>>();
                Parametro<SqlDbType> m_Parametro = new Parametro<SqlDbType>();
                m_Parametro.nombre = "@Idtransaccion";
                m_Parametro.valorEnviar = idTransaccion;
                m_Parametro.tipoDato = SqlDbType.Int;
                m_Parametro.tamano = idTransaccion.Length;
                m_ListaParametros.Add(m_Parametro);
                m_Conexion.EjecutarSP(this.CadenaConexion(conexion), "SP_ChangeAsigPIN", "Parametros", m_ListaParametros);
            }
            catch (Exception ex)
            {
                throw new Exception("DALOperacionesPin -" + ex.Message);
            }
        }

        
       public string ObtenerContrasenaXUsuario(string idUsuario, string conexion) 
       {
           try
           {
               IDALBaseDatos m_Conexion = OperacionesBD.Instancia.operarBD();
               List<Parametro<SqlDbType>> m_ListaParametros = new List<Parametro<SqlDbType>>();
               Parametro<SqlDbType> m_Parametro = new Parametro<SqlDbType>();
               m_Parametro.nombre = "@Identificacion";
               m_Parametro.valorEnviar = idUsuario;
               m_Parametro.tipoDato = SqlDbType.VarChar;
               m_Parametro.tamano = idUsuario.Length;
               m_ListaParametros.Add(m_Parametro);
               DataSet m_Resul =  m_Conexion.EjecutarSP(this.CadenaConexion(conexion), "SP_ObtenerContrasenaXUsuario", "Parametros", m_ListaParametros);
               if (m_Resul.Tables.Count > 0 && m_Resul.Tables[0].Rows.Count > 0)
                   return m_Resul.Tables[0].Rows[0]["Contrasena"].ToString();
           }
           catch (Exception ex)
           {
               throw new Exception("DALOperacionesPin -" + ex.Message);
           }

           return string.Empty;
       }


       /// <summary>
       /// Metodo encargado de guardar
       /// </summary>
       /// <param name="cliente">CLiente q solicita la peticion</param>
       /// <param name="idTransaccionCliente">Identificador de la transaccion (Para el cliente)</param>
       /// <param name="conexionBD">Identificador coenxion con la BD</param>
       /// <returns>Retorna idtransaccion del sistema</returns>
       /// <remarks>AERM 17/03/2015</remarks>
       public int InsertarTransaccion(string cliente, string idTransaccionCliente, string conexionBD)
       {
           int m_IdTransaccion = 0;
           try
           {
               IDALBaseDatos m_conexion = OperacionesBD.Instancia.operarBD();
               List<Parametro<SqlDbType>> m_listParametros = new List<Parametro<SqlDbType>>();
               Parametro<SqlDbType> m_parametro = new Parametro<SqlDbType>();
               DataSet m_resultado = default(DataSet);
               m_parametro.nombre = "@Cliente";
               m_parametro.valorEnviar = cliente;
               m_parametro.tipoDato = SqlDbType.VarChar;
               m_parametro.tamano = cliente.ToString().Length;
               m_listParametros.Add(m_parametro);

               m_parametro = new Parametro<SqlDbType>();
               m_parametro.nombre = "@IdTransaccionC";
               m_parametro.valorEnviar = idTransaccionCliente.ToString();
               m_parametro.tipoDato = SqlDbType.VarChar;
               m_parametro.tamano = idTransaccionCliente.ToString().Length;
               m_listParametros.Add(m_parametro);

               m_parametro = new Parametro<SqlDbType>();
               m_parametro.nombre = "@Estado";
               m_parametro.valorEnviar = ESTADO_REALCE.ST_EN_PROCESO.ToString();
               m_parametro.tipoDato = SqlDbType.VarChar;
               m_parametro.tamano = ESTADO_REALCE.ST_EN_PROCESO.ToString().Length;
               m_listParametros.Add(m_parametro);
               m_resultado = m_conexion.EjecutarSP(this.CadenaConexion(conexionBD), "SP_InsertTransaccion", "Parametros", m_listParametros);
               if (m_resultado.Tables.Count > 0 & m_resultado.Tables[0].Rows.Count > 0)
               {
                   return Convert.ToInt32(m_resultado.Tables[0].Rows[0]["Identificador"].ToString());
               }
           }
           catch (Exception ex)
           {
               throw ex;
           }

           return m_IdTransaccion;
       }

        /// <summary>
        /// Metodo encargado de obtener la cadena de conexion
        /// </summary>
        /// <param name="conexion">De la BD</param>
        /// <returns>Retorna string con la cadena de conexion</returns>
        private string CadenaConexion(string conexion)
        {
            return ConfigurationManager.ConnectionStrings[conexion].ConnectionString;
        }


    }
}
