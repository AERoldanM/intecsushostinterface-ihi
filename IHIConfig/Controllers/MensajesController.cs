﻿using IHIConfig.Filtros;
using Intecsus.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IHIConfig.Controllers
{
    [Loging(Order = 1)]
    [LogErrors(Order = 2)]
    [HandleError(Order = 3)]
    public class MensajesController : Controller
    {
        // GET: Mensajes
         [OutputCache(Duration = 50)]
        public ActionResult Index()
        {
            List<Mensajes> m_Mensajes = new List<Mensajes>();
            Mensajes m_Mensaje = new Mensajes
            {
                CodigoCliente = "123",
                CodigoMensaje = "1234",
                MensajeEnviado = "Prueba mensajes"

            };
            m_Mensajes.Add(m_Mensaje);
            m_Mensajes.Add(m_Mensaje);
            m_Mensajes.Add(m_Mensaje);
            ViewBag.ListaMensajes = m_Mensajes;
            return View();
        }

         public PartialViewResult Details(Mensajes mensaje)
         {
             ViewBag.Mensajes = "Modificar";
             return PartialView("_Create", mensaje);
         }

         public PartialViewResult Create()
         {
             return PartialView("_Create");
         }

         [HttpPost]
         public PartialViewResult Crear(Mensajes mensaje)
         {
             //OPTIMIZAR  y validar si viene vacio para no ejecutar la operacion
             List<Mensajes> m_Mensajes = new List<Mensajes>();
             Mensajes m_Mensaje = new Mensajes
                {
                    CodigoCliente = "123",
                    CodigoMensaje = "1234",
                    MensajeEnviado = "Prueba mensajes Creado"
             
                };
             m_Mensajes.Add(m_Mensaje);
             m_Mensajes.Add(m_Mensaje);
             m_Mensajes.Add(m_Mensaje);
             ViewBag.ListaMensajes = m_Mensajes;
             ViewBag.MensajeSuccess = "Mensaje Creado";
             return PartialView("Index");
         }

         [HttpPost]
         public PartialViewResult Modificar(Mensajes mensaje)
         {
             //OPTIMIZAR
             List<Mensajes> m_Mensajes = new List<Mensajes>();
             Mensajes m_Mensaje = new Mensajes
             {
                 CodigoCliente = "123",
                 CodigoMensaje = "1234",
                 MensajeEnviado = "Prueba mensajes Editado"

             };
             m_Mensajes.Add(m_Mensaje);
             m_Mensajes.Add(m_Mensaje);
             m_Mensajes.Add(m_Mensaje);
             ViewBag.ListaMensajes = m_Mensajes;
             ViewBag.MensajeSuccess = "Mensaje Editado";
             return PartialView("Index");
         }

         [HttpPost]
         public PartialViewResult Delete(int codigoMensaje)
         {
             //OPTIMIZAR 
             List<Mensajes> m_Mensajes = new List<Mensajes>();
             Mensajes m_Mensaje = new Mensajes
             {
                 CodigoCliente = "123",
                 CodigoMensaje = "1234",
                 MensajeEnviado = "Prueba mensajes Elimidado"

             };
             m_Mensajes.Add(m_Mensaje);
             m_Mensajes.Add(m_Mensaje);
             m_Mensajes.Add(m_Mensaje);
             ViewBag.ListaMensajes = m_Mensajes;
             ViewBag.MensajeSuccess = "Mensaje Elimidado";
             return PartialView("Index");
            
         }
    }
}