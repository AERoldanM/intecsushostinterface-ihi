﻿using IHIConfig.Filtros;
using Intecsus.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IHIConfig.Controllers
{
    
    [LogErrors(Order = 2)]
    [HandleError(Order = 3)]
    public class UsuarioController : Controller
    {
        [HttpPost]
        public ActionResult Ingreso(UsuarioC user)
        {
            if (user.UserID == "1" && user.Contrasena == "12345")
            {
                user.Nombre = "Andres";
                user.Apellido = "Roldan";
                user.Identificacion = "123456";
                user.RolAsociado = new Rol();
                user.RolAsociado.IdRol = 1;
                user.RolAsociado.NombreRol = "Admin";
                user.RolAsociado.Configuracion = true;
               
                //Buscar los roles
                List<Rol> m_Roles = new List<Rol>();
                Rol m_Rol = new Rol {
                    IdRol = 1,
                    NombreRol = "Admin"
                };
                m_Roles.Add(m_Rol);
                m_Rol = new Rol
                {
                    IdRol = 2,
                    NombreRol = "Operario"
                };
                m_Roles.Add(m_Rol);
                m_Roles.Add(m_Rol);
                user.Roles = m_Roles;
                Session["Usuario"] = user;
                ViewBag.Useranme = user.Nombres;
                return RedirectToAction("Exito", "Home", new { mensajeError = "Ingreso Exitoso" });
            }

            return RedirectToAction("Error", "Home", new { mensajeError = "Usuario Incorrecto" });
        }


        // GET: Configuracion
        [Loging(Order = 1)]
        public ActionResult Index()
        {
            List<UsuarioC> m_ListUsuario = new List<UsuarioC>();
            //Capa negocio
            UsuarioC user = new UsuarioC();
            user.Nombre = "Andres";
            user.Apellido = "Roldan";
            user.Identificacion = "123456";
            user.RolAsociado = new Rol();
            user.RolAsociado.IdRol = 1;
            user.RolAsociado.NombreRol = "Admin";
            user.RolAsociado.Configuracion = true;
            m_ListUsuario.Add(user);
            m_ListUsuario.Add(user);
            m_ListUsuario.Add(user);
            m_ListUsuario.Add(user);
            m_ListUsuario.Add(user);
            m_ListUsuario.Add(user);
            ViewBag.ListUsuarios = m_ListUsuario;
            return View("Index");
        }

        // GET: Usuario/Details/5
        [Loging(Order = 1)]
        public PartialViewResult Details(Usuario user)
        {
            ViewBag.UsuarioEdit = "Modificar";
            return PartialView("_Create", user);
        }

        // GET: Usuario/Create
        [Loging(Order = 1)]
        public PartialViewResult Create()
        {
            return PartialView("_Create");
        }

        [Loging(Order = 1)]
        [HttpPost]
        public PartialViewResult Crear(Usuario usuario)
        {
            //OPTIMIZAR y validar si viene vacio para no ejecutar la operacion
            List<Usuario> m_Usuarios = new List<Usuario>();

            m_Usuarios.Add(usuario);
            m_Usuarios.Add(usuario);
            m_Usuarios.Add(usuario);
            ViewBag.ListUsuarios = m_Usuarios;
            ViewBag.MensajeSuccess = "Usuario Creado";
            return PartialView("Index");
        }

        [Loging(Order = 1)]
        [HttpPost]
        public PartialViewResult Modificar(Usuario usuario)
        {
            //OPTIMIZAR
            List<Usuario> m_Usuarios = new List<Usuario>();

            m_Usuarios.Add(usuario);
            m_Usuarios.Add(usuario);
            m_Usuarios.Add(usuario);
            ViewBag.ListUsuarios = m_Usuarios;
            ViewBag.MensajeSuccess = "Usuario Modificado";
            return PartialView("Index");
        }

        [Loging(Order = 1)]
        [HttpPost]
        public PartialViewResult Delete(int idUser)
        {
            //OPTIMIZAR
            List<UsuarioC> m_Usuarios = new List<UsuarioC>();
            UsuarioC m_Usuario = new UsuarioC
            {
                IdUsuario = 1,
                UserID = "PruebaEli",
                Contrasena = "123",
                Nombre = "Naruto",
                Apellido = "Uzumaki",
                Identificacion = "11234",
                RolAsociado = new Rol
                {
                    IdRol = 1
                }
            };
            m_Usuarios.Add(m_Usuario);
            m_Usuarios.Add(m_Usuario);
            m_Usuarios.Add(m_Usuario);
            ViewBag.ListUsuarios = m_Usuarios;
            ViewBag.MensajeSuccess = "Usuario  Eliminado";
            return PartialView("Index");
        }

        [Loging(Order = 1)]
        [HttpPost]
        public PartialViewResult ModificarContrasena(Usuario usuario)
        {
            //OPTIMIZAR
            List<Usuario> m_Usuarios = new List<Usuario>();

            m_Usuarios.Add(usuario);
            m_Usuarios.Add(usuario);
            m_Usuarios.Add(usuario);
            ViewBag.ListUsuarios = m_Usuarios;
            ViewBag.MensajeSuccess = "Contraseña Modificada";
            return PartialView("Index");
        }
    }
}
