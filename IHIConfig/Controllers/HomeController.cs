﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Intecsus.Entidades;
using IHIConfig.Filtros;

namespace IHIConfig.Controllers
{
    [LogErrors(Order = 2)]
    [HandleError(Order = 3)]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Error(string mensajeError)
        {
            ViewBag.MensajeError = mensajeError;
            return View("Index");
        }

        [Loging(Order = 1)]
        [OutputCache(Duration=60)]
        public ActionResult Exito(string mensajeError)
        {
            ViewBag.MensajeSuccess = mensajeError;
            ViewBag.Informativo = "Recuerda: Por temas de seguridad para configuración de conexiones de bases de datos, parametros base,  comunicación  con Servicios " +
                               "Web o bases de datos directamente en los archivos de configuración de las distintas aplicaciones del IHI.";
            ViewBag.TotalExitoso = 20;
            ViewBag.TotalError = 10;
            ViewBag.Total = 30;
            Usuario user = (Usuario)Session["Usuario"];
            //Enviamos 10 primeros estodos por transaccion
            List<EstadoTransaccion> m_Estados = new List<EstadoTransaccion>();
            EstadoTransaccion m_Estado = new EstadoTransaccion();
            m_Estado.IdTransaccion = "1";
            m_Estado.IdtransaCliente = "PRUEBA";
            m_Estado.Cliente = "PRUEBAS AERM";
            m_Estado.Fecha = DateTime.Now;
            m_Estado.Estado = "No se";
            m_Estados.Add(m_Estado);
            m_Estados.Add(m_Estado);
            m_Estados.Add(m_Estado);
            m_Estados.Add(m_Estado);
            m_Estados.Add(m_Estado);
            ViewData["Estados"] = m_Estados;
            return View(user);
        }

        [LogOn]
        public ActionResult LogOn()
        {
            ViewBag.MensajeSuccess = "Cierre Exitoso";
            return View("Index");
        }
    }
}