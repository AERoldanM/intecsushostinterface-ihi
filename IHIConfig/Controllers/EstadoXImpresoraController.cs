﻿using IHIConfig.Filtros;
using Intecsus.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IHIConfig.Controllers
{
    [Loging(Order = 1)]
    [LogErrors(Order = 2)]
    [HandleError(Order = 3)]
    public class EstadoXImpresoraController : Controller
    {
        // GET: EstadoXImpresora
        public ActionResult Index()
        {
            List<EstadoXImpresoraC> m_EstadosXImpresora = new List<EstadoXImpresoraC>();
            EstadoXImpresoraC m_EstadoxImpresora = new EstadoXImpresoraC
            {
                IdCodRespuesta = 1,
                DeviceStatus = "1",
                DeviceType = "1",
                ReturnCode = "1",
                Estado = new Estados
                {
                    IdEstado = 1,
                    NombreEstado = "PruebaInicial"
                }

            };


            m_EstadosXImpresora.Add(m_EstadoxImpresora);
            m_EstadosXImpresora.Add(m_EstadoxImpresora);
            m_EstadosXImpresora.Add(m_EstadoxImpresora);
            ViewBag.ListaEstadoXImpresora = m_EstadosXImpresora;
            return View();
        }


        // GET: Configuracion/Details
        public PartialViewResult Details(EstadoXImpresora estadoXImpresora)
        {
            ViewBag.EstadoXImpresora = "Modificar";
            return PartialView("_Create", estadoXImpresora);
        }

        // GET: Configuracion/Create
        public PartialViewResult Create()
        {
            EstadoXImpresoraC m_EstadoImpresora = new EstadoXImpresoraC();
            m_EstadoImpresora.EstadosSelec = (IEnumerable<Estados>)Session["Estados"];
            return PartialView("_Create", m_EstadoImpresora);
        }

        [HttpPost]
        public PartialViewResult Crear(EstadoXImpresora estadoXImpresora)
        {
            //OPTIMIZAR y validar si viene vacio para no ejecutar la operacion
            List<EstadoXImpresora> m_EstadosImpresora = new List<EstadoXImpresora>();

            m_EstadosImpresora.Add(estadoXImpresora);
            m_EstadosImpresora.Add(estadoXImpresora);
            m_EstadosImpresora.Add(estadoXImpresora);
            ViewBag.ListaEstadoXImpresora = m_EstadosImpresora;
            ViewBag.MensajeSuccess = "Parametro  Creado";
            return PartialView("Index");
        }

        [HttpPost]
        public PartialViewResult Modificar(EstadoXImpresora estadoXImpresora)
        {
            //OPTIMIZAR
            List<EstadoXImpresora> m_EstadosImpresora = new List<EstadoXImpresora>();
            m_EstadosImpresora.Add(estadoXImpresora);
            m_EstadosImpresora.Add(estadoXImpresora);
            m_EstadosImpresora.Add(estadoXImpresora);
            ViewBag.ListaEstadoXImpresora = m_EstadosImpresora;
            ViewBag.MensajeSuccess = "Parametro  Modificado";
            return PartialView("Index");
        }

        [HttpPost]
        public PartialViewResult Delete(int IdCodRespuesta)
        {
            //OPTIMIZAR
            List<EstadoXImpresoraC> m_EstadosXImpresion = new List<EstadoXImpresoraC>();
            EstadoXImpresoraC m_EstadoxImpresion = new EstadoXImpresoraC
            {
                IdCodRespuesta = 1,
                DeviceStatus = "1",
                DeviceType = "1",
                ReturnCode = "1",
                Estado = new Estados
                {
                    IdEstado = 1,
                    NombreEstado = "Elimiancion"
                }

            };


            m_EstadosXImpresion.Add(m_EstadoxImpresion);
            m_EstadosXImpresion.Add(m_EstadoxImpresion);
            m_EstadosXImpresion.Add(m_EstadoxImpresion);
            ViewBag.ListaEstadoXImpresora = m_EstadosXImpresion;
            ViewBag.MensajeSuccess = "Parametro  Eliminado";
            return PartialView("Index");
        }
    }
}