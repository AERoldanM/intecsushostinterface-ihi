﻿using IHIConfig.Filtros;
using Intecsus.Entidades;
using System.Collections.Generic;
using System.Web.Mvc;

namespace IHIConfig.Controllers
{
    [Loging(Order = 1)]
    [LogErrors(Order = 2)]
    [HandleError(Order = 3)]
    public class RolController : Controller
    {
        // GET: Rol
        public ActionResult Index()
        {
            UsuarioC m_User = (UsuarioC)Session["Usuario"];
            ViewBag.ListRoles = m_User.Roles;
            return View("Index");
        }

        // GET: Configuracion/Details
        public PartialViewResult Details(Rol rol)
        {
            ViewBag.Rol = "Modificar";
            return PartialView("_Create", rol);
        }

        // GET: Configuracion/Create
        public PartialViewResult Create()
        {
            return PartialView("_Create");
        }


        [HttpPost]
        public PartialViewResult Crear(Rol rol)
        {
            //OPTIMIZAR y validar si viene vacio para no ejecutar la operacion
            List<Rol> m_Roles = new List<Rol>();

            m_Roles.Add(rol);
            m_Roles.Add(rol);
            m_Roles.Add(rol);
            ViewBag.ListaRoles = m_Roles;
            ViewBag.MensajeSuccess = "Rol Creado";
            return PartialView("Index");
        }

        [HttpPost]
        public PartialViewResult Modificar(Rol rol)
        {
            //OPTIMIZAR
            List<Rol> m_Roles = new List<Rol>();

            m_Roles.Add(rol);
            m_Roles.Add(rol);
            m_Roles.Add(rol);
            ViewBag.ListaRoles = m_Roles;
            ViewBag.MensajeSuccess = "Rol Modificado";
            return PartialView("Index");
        }

        [HttpPost]
        public PartialViewResult Delete(int idRol)
        {
            //OPTIMIZAR
            List<Rol> m_Roles = new List<Rol>();
            Rol m_Rol = new Rol
            {
                IdRol = 1,
                NombreRol = "Rol ELiminado",
                Log = true,
                Configuracion = true
               
            };
            m_Roles.Add(m_Rol);
            m_Roles.Add(m_Rol);
            m_Roles.Add(m_Rol);
            ViewBag.ListaConfig = m_Roles; 
            ViewBag.MensajeSuccess = "Rol Eliminado";
            return PartialView("Index");
        }
    }
}