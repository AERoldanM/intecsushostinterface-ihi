﻿using IHIConfig.Filtros;
using Intecsus.Entidades;
using System.Collections.Generic;
using System.Web.Mvc;

namespace IHIConfig.Controllers
{
    [Loging(Order = 1)]
    [LogErrors(Order = 2)]
    [HandleError(Order = 3)]
    public class EstadosXImpresionController : Controller
    {
        // GET: EstadosXImpresion
        public ActionResult Index()
        {
            //
            List<EstadoXImpresion> m_Estados = new List<EstadoXImpresion>();
            EstadoXImpresion m_Estado = new EstadoXImpresion
            {
                IdCodRespuesta = 1,
                IdDataInteger = "1",
                IdMessageType = "1",
                Estado = new Estados
                {
                    IdEstado = 1,
                    NombreEstado = "Prueba"
                }

            };
            m_Estados.Add(m_Estado);
            m_Estados.Add(m_Estado);
            m_Estados.Add(m_Estado);
            ViewBag.ListEstadoXImpresion = m_Estados;
            return View();
        }

        public PartialViewResult Details(EstadoXImpresion estadoImpresion)
        {
            ViewBag.EstadoXImpresion = "Modificar";
            return PartialView("_Create", estadoImpresion);
        }

        public PartialViewResult Create()
        {
            EstadoXImpresion m_EstadoImpresion = new EstadoXImpresion();
            m_EstadoImpresion.EstadosSelec = (IEnumerable<Estados>)Session["Estados"];
            return PartialView("_Create", m_EstadoImpresion);
        }

        [HttpPost]
        public PartialViewResult Crear(EstadoXImpresion estadoImpresion)
        {
            //OPTIMIZAR y validar si viene vacio para no ejecutar la operacion
            List<EstadoXImpresion> m_EstadosImpresion = new List<EstadoXImpresion>();

            m_EstadosImpresion.Add(estadoImpresion);
            m_EstadosImpresion.Add(estadoImpresion);
            m_EstadosImpresion.Add(estadoImpresion);
            ViewBag.ListEstadoXImpresion = m_EstadosImpresion;
            ViewBag.MensajeSuccess = "Parametro Creado";
            return PartialView("Index");
        }

        [HttpPost]
        public PartialViewResult Modificar(EstadoXImpresion estadoImpresion)
        {
            //OPTIMIZAR
            List<EstadoXImpresion> m_EstadosImpresion = new List<EstadoXImpresion>();
            m_EstadosImpresion.Add(estadoImpresion);
            m_EstadosImpresion.Add(estadoImpresion);
            m_EstadosImpresion.Add(estadoImpresion);
            ViewBag.ListEstadoXImpresion = m_EstadosImpresion;
            ViewBag.MensajeSuccess = "Parametro  Modificado";
            return PartialView("Index");
        }

        [HttpPost]
        public PartialViewResult Delete(int IdCodRespuesta)
        {
            //OPTIMIZAR
            List<EstadoXImpresion> m_Estados = new List<EstadoXImpresion>();
            EstadoXImpresion m_Estado = new EstadoXImpresion
            {
                IdCodRespuesta = 1,
                IdDataInteger = "1",
                IdMessageType = "1",
                Estado = new Estados
                {
                    IdEstado = 1,
                    NombreEstado = "Elimiancion"
                }
                
            };
            m_Estados.Add(m_Estado);
            m_Estados.Add(m_Estado);
            m_Estados.Add(m_Estado);
            ViewBag.ListEstadoXImpresion = m_Estados;
            ViewBag.MensajeSuccess = "Parametro  Eliminado";
            return PartialView("Index");
        }
    }
}