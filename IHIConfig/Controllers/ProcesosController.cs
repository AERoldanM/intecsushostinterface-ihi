﻿using IHIConfig.Filtros;
using Intecsus.Entidades;
using System.Collections.Generic;
using System.Web.Mvc;

namespace IHIConfig.Controllers
{
    [Loging(Order = 1)]
    [LogErrors(Order = 2)]
    [HandleError(Order = 3)]
    public class ProcesosController : Controller
    {
        // GET: Procesos
        [OutputCache(Duration = 30)]
        public PartialViewResult Index()
        {
            List<ProcesosBD> m_procesos = new List<ProcesosBD>();
            ProcesosBD m_Proceso = new ProcesosBD();
            m_Proceso.IDProceso = 1;
            m_Proceso.NombreProceso = "Proceso Prueba";
            m_procesos.Add(m_Proceso);
            m_procesos.Add(m_Proceso);
            m_procesos.Add(m_Proceso);
            ViewBag.ListaProcesos = m_procesos;
            return PartialView("Index");
        }

        // GET: Procesos/Details/5
        public PartialViewResult Details(ProcesosBD proceso)
        {
            ViewBag.EditarProceso = "Modificar";
            return PartialView("_Create", proceso);
        }

        // GET: Procesos/Create
        public PartialViewResult Create()
        {
            return PartialView("_Create");
        }

        // POST: Procesos/Create
        [HttpPost]
        public PartialViewResult Crear(ProcesosBD proceso)
        {
            //OPTIMIZAR  y validar si viene vacio para no ejecutar la operacion
            List<ProcesosBD> m_procesos = new List<ProcesosBD>();
            ProcesosBD m_Proceso = new ProcesosBD();
            m_Proceso.IDProceso = 1;
            m_Proceso.NombreProceso = "Proceso CREAAAAA COntrol";
            m_procesos.Add(m_Proceso);
            m_procesos.Add(m_Proceso);
            m_procesos.Add(m_Proceso);
            ViewBag.ListaProcesos = m_procesos;
            ViewBag.MensajeSuccess = "Proceso Creado";
            return PartialView("Index");
            //try
            //{
            //    // TODO: Add insert logic here

            //    return RedirectToAction("Index");
            //}
            //catch
            //{
            //    return View();
            //}
        }

        
        [HttpPost]
        public PartialViewResult Modificar(ProcesosBD proceso)
        {
            //OPTIMIZAR
            List<ProcesosBD> m_procesos = new List<ProcesosBD>();
            ProcesosBD m_Proceso = new ProcesosBD();
            m_Proceso.IDProceso = 1;
            m_Proceso.NombreProceso = "Proceso Editado COntrol";
            m_procesos.Add(m_Proceso);
            m_procesos.Add(m_Proceso);
            ViewBag.ListaProcesos = m_procesos;
            ViewBag.MensajeSuccess = "Proceso Editado";
            return PartialView("Index");
        }
        // POST: Procesos/Delete/5
        [HttpPost]
        public PartialViewResult Delete(int idProceso)
        {
            //OPTIMIZAR 
            List<ProcesosBD> m_procesos = new List<ProcesosBD>();
            ProcesosBD m_Proceso = new ProcesosBD();
            m_Proceso.IDProceso = 1;
            m_Proceso.NombreProceso = "Proceso Elimina COntrol";
            m_procesos.Add(m_Proceso);
            m_procesos.Add(m_Proceso);
            ViewBag.ListaProcesos = m_procesos;
            ViewBag.MensajeSuccess = "Proceso Elimidado";
            return PartialView("Index");
            //try
            //{
            //    // TODO: Add delete logic here

            //    return RedirectToAction("Index");
            //}
            //catch
            //{
            //    return View();
            //}
        }
    }
}
