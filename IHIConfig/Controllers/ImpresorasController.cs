﻿using IHIConfig.Filtros;
using Intecsus.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IHIConfig.Controllers
{
    [Loging(Order = 1)]
    [LogErrors(Order = 2)]
    [HandleError(Order = 3)]
    public class ImpresorasController : Controller
    {
        // GET: Impresoras
         [OutputCache(Duration = 30)]
        public PartialViewResult Index()
        {
            List<Impresora> m_Impresoras = new List<Impresora>();
            Impresora m_Impresora = new Impresora
            {
                IdImpresora = 1,
                NombreRecibido = "PruebaEnvio",
                PCNAME = "Prueba",
                IP = "192.10.10.1"
            };
            m_Impresoras.Add(m_Impresora);
            m_Impresoras.Add(m_Impresora);
            m_Impresoras.Add(m_Impresora);
            ViewBag.ListaImpresoras = m_Impresoras;
            return PartialView("Index");
        }


         public PartialViewResult Details(Impresoras impresora)
         {
             ViewBag.EditarImpresora = "Modificar";
             return PartialView("_Create", impresora);
         }

         public PartialViewResult Create()
         {
             return PartialView("_Create");
         }

         [HttpPost]
         public PartialViewResult Crear(Impresoras impresora)
         {
             //OPTIMIZAR  y validar si viene vacio para no ejecutar la operacion
             List<Impresora> m_Impresoras = new List<Impresora>();
             Impresora m_Impresora = new Impresora
                                     {
                                         IdImpresora = 1,
                                         NombreRecibido = "ReciboCreacion",
                                         PCNAME = "Creado",
                                         IP = "192.10.10.1"
                                     };
             m_Impresoras.Add(m_Impresora);
             m_Impresoras.Add(m_Impresora);
             m_Impresoras.Add(m_Impresora);
             ViewBag.ListaImpresoras = m_Impresoras;
             ViewBag.MensajeSuccess = "Impresora Creada";
             return PartialView("Index");
         }

         [HttpPost]
         public PartialViewResult Modificar(Impresoras impresora)
         {
             //OPTIMIZAR
             List<Impresora> m_Impresoras = new List<Impresora>();
             Impresora m_Impresora = new Impresora
             {
                 IdImpresora = 1,
                 NombreRecibido = "ReciboModificacion",
                 PCNAME = "Modificado",
                 IP = "192.10.10.1"
             };
             m_Impresoras.Add(m_Impresora);
             m_Impresoras.Add(m_Impresora);
             m_Impresoras.Add(m_Impresora);
             ViewBag.ListaImpresoras = m_Impresoras;
             ViewBag.MensajeSuccess = "Impresora Modificada";
             return PartialView("Index");
         }

         [HttpPost]
         public PartialViewResult Delete(int idImpresora)
         {
             //OPTIMIZAR 
             List<Impresora> m_Impresoras = new List<Impresora>();
             Impresora m_Impresora = new Impresora
             {
                 IdImpresora = 1,
                 NombreRecibido = "ReciboEliminacion",
                 PCNAME = "Elimnado",
                 IP = "192.10.10.1"
             };
             m_Impresoras.Add(m_Impresora);
             m_Impresoras.Add(m_Impresora);
             m_Impresoras.Add(m_Impresora);
             ViewBag.ListaImpresoras = m_Impresoras;
             ViewBag.MensajeSuccess = "Impresora Eliminada";
             return PartialView("Index");
         }
    }
}