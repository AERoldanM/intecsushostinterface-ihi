﻿using IHIConfig.Filtros;
using Intecsus.Entidades;
using System.Collections.Generic;
using System.Web.Mvc;

namespace IHIConfig.Controllers
{
    [Loging(Order = 1)]
    [LogErrors(Order = 2)]
    [HandleError(Order = 3)]
    public class ConversionMController : Controller
    {
        // GET: ConversionM
        public ActionResult Index()
        {
            List<ConversionMensaje> m_Conversiones = new List<ConversionMensaje>();
            ConversionMensaje m_Conversion = new ConversionMensaje
            {
                CodigoCliente = "1234",
                IdConversion = 1,
                MensajeCW = "What is it??",
                Mensaje = "Prueba conversion"

            };

            m_Conversiones.Add(m_Conversion);
            m_Conversiones.Add(m_Conversion);
            m_Conversiones.Add(m_Conversion);
            ViewBag.ListaConversionM = m_Conversiones;
            return View();
        }

        public PartialViewResult Details(ConversionMensaje mensajesC)
        {
            ViewBag.ConversionM = "Modificar";
            return PartialView("_Create", mensajesC);
        }

        public PartialViewResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public PartialViewResult Crear(ConversionMensaje mensajesC)
        {
            //OPTIMIZAR  y validar si viene vacio para no ejecutar la operacion
            List<ConversionMensaje> m_Conversiones = new List<ConversionMensaje>();
            ConversionMensaje m_Conversion = new ConversionMensaje 
                                {
                                    CodigoCliente = "1234",
                                    IdConversion = 1,
                                    MensajeCW = "What is it??",
                                    Mensaje = "Prueba conversion creación"

                                };

            m_Conversiones.Add(m_Conversion);
            m_Conversiones.Add(m_Conversion);
            m_Conversiones.Add(m_Conversion);
            ViewBag.ListaConversionM = m_Conversiones;
            ViewBag.MensajeSuccess = "Conversión Creada";
            return PartialView("Index");
        }

        [HttpPost]
        public PartialViewResult Modificar(ConversionMensaje conversion)
        {
            //OPTIMIZAR
            List<ConversionMensaje> m_Conversiones = new List<ConversionMensaje>();
            ConversionMensaje m_Conversion = new ConversionMensaje
            {
                CodigoCliente = "1234",
                IdConversion = 1,
                MensajeCW = "What is it??",
                Mensaje = "Prueba conversion Editado"

            };

            m_Conversiones.Add(m_Conversion);
            m_Conversiones.Add(m_Conversion);
            m_Conversiones.Add(m_Conversion);
            ViewBag.ListaConversionM = m_Conversiones;
            ViewBag.MensajeSuccess = "Conversión Editado";
            return PartialView("Index");
        }

        [HttpPost]
        public PartialViewResult Delete(int idConversion)
        {
            //OPTIMIZAR 
            List<ConversionMensaje> m_Conversiones = new List<ConversionMensaje>();
            ConversionMensaje m_Conversion = new ConversionMensaje
            {
                CodigoCliente = "1234",
                IdConversion = 1,
                MensajeCW = "What is it??",
                Mensaje = "Prueba conversion Eliminado"

            };

            m_Conversiones.Add(m_Conversion);
            m_Conversiones.Add(m_Conversion);
            m_Conversiones.Add(m_Conversion);
            ViewBag.ListaConversionM = m_Conversiones;
            ViewBag.MensajeSuccess = "Conversión Eliminado";
            return PartialView("Index");
        }
    }
}