﻿using IHIConfig.Filtros;
using Intecsus.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IHIConfig.Controllers
{
    [Loging]
    public class EstadoTransaccionController : Controller
    {
        
        // GET: EstadoTransaccion/Details/5
        
        public ActionResult Details()
        {
            List<EstadoTransaccion> m_Estados = new List<EstadoTransaccion>();
            EstadoTransaccion m_Estado = new EstadoTransaccion();
            m_Estado.IdTransaccion = "1";
            m_Estado.IdtransaCliente = "PRUEBA";
            m_Estado.Cliente = "PRUEBAS AERM";
            m_Estado.Fecha = DateTime.Now;
            m_Estado.Estado = "No se";
            m_Estados.Add(m_Estado);
            m_Estados.Add(m_Estado);
            m_Estados.Add(m_Estado);
            m_Estados.Add(m_Estado);
            m_Estados.Add(m_Estado);
            ViewData["Estados"] = m_Estados;
            return PartialView("_Details");
        }
    }
}
