﻿using IHIConfig.Filtros;
using Intecsus.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IHIConfig.Controllers
{
    [Loging(Order = 1)]
    [LogErrors(Order = 2)]
    [HandleError(Order = 3)]
    public class PrincipalesController : Controller
    {
        [OutputCache(Duration = 40)]
        public ActionResult Configuracion()
        {
            List<Configurar> m_ListConfigurar = new List<Configurar>();
            //Lo debe traer la capa de negocio 
            //Config
            Configurar m_Config = new Configurar
            {
                IDValor = "1",
                Valor = "Pruebas",
                TipoDato = "Cadena"
            };
            m_ListConfigurar.Add(m_Config);
            m_ListConfigurar.Add(m_Config);
            m_ListConfigurar.Add(m_Config);
            m_ListConfigurar.Add(m_Config);
            m_ListConfigurar.Add(m_Config);
            m_ListConfigurar.Add(m_Config);
            ViewBag.ListaConfig = m_ListConfigurar;
            //OPTIMIZAR EstadosXimpresion
            List<ProcesosBD> m_procesos = new List<ProcesosBD>();
            ProcesosBD m_Proceso = new ProcesosBD();
            m_Proceso.IDProceso = 1;
            m_Proceso.NombreProceso = "Proceso Prueba";
            m_procesos.Add(m_Proceso);
            m_procesos.Add(m_Proceso);
            m_procesos.Add(m_Proceso);
            ViewBag.ListaProcesos = m_procesos;
            return View();
        }

        [OutputCache(Duration = 40)]
        public ActionResult IntegracionCW()
        {
            //Impresoras
            List<Impresora> m_Impresoras = new List<Impresora>();
            Impresora m_Impresora = new Impresora
            {
                IdImpresora = 1,
                NombreRecibido = "PruebaEnvio",
                PCNAME = "Prueba",
                IP = "192.10.10.1"
            };
            m_Impresoras.Add(m_Impresora);
            m_Impresoras.Add(m_Impresora);
            m_Impresoras.Add(m_Impresora);
            ViewBag.ListaImpresoras = m_Impresoras;
            //Productos
            //OPTIMIZAR
            List<Producto> m_Productos = new List<Producto>();
            Producto m_Producto = new Producto
            {
                IdProducto = 1,
                ProductoName = "Producto Prueba",
                CardName = "Pruebas",
                Cliente = "Prueba",
                XML = @"<Parametros>
                                  <Parametro nombre='@PAN@'  tipo='String' codifica='SI' derecha='5' izquierda='3' >
                                    <Valor inicio='63' tamano='16' ISPCI='TRUE' ValoresS='/RespuestaRealceEmisionTarjetasDTO/trackIField/panField'  ></Valor>
                                  </Parametro>
                                  <Parametro nombre='@CardHolderName@'  tipo='String' codifica='NO'>
                                    <Valor inicio='38' tamano='40'  ISPCI='TRUE'  ValoresS='/RespuestaRealceEmisionTarjetasDTO/nombreCompletoField'></Valor>
                                  </Parametro>
                                  <Parametro nombre='@TIPODOCUMENTO1@'  tipo='String' codifica='NO'>
                                    <Valor inicio='38' tamano='2' ValoresS='/RespuestaRealceEmisionTarjetasDTO/tipoIdentificacionAField'></Valor>
                                  </Parametro>
                                </Parametros>",
                Tamano = 200
            };
            m_Productos.Add(m_Producto);
            m_Productos.Add(m_Producto);
            m_Productos.Add(m_Producto);
            Session["ListaProductos"] = m_Productos;
            //Conversiones
            List<ConversionMensaje> m_Conversiones = new List<ConversionMensaje>();
            ConversionMensaje m_Conversion = new ConversionMensaje
            {
                CodigoCliente = "1234",
                IdConversion = 1,
                MensajeCW = "What is it??",
                Mensaje = "Prueba conversion"

            };

            m_Conversiones.Add(m_Conversion);
            m_Conversiones.Add(m_Conversion);
            m_Conversiones.Add(m_Conversion);
            ViewBag.ListaConversionM = m_Conversiones;
            return View();
        }

        [OutputCache(Duration = 40)]
        public ActionResult Mensajes()
        {
            //Mensajes
            List<Mensajes> m_Mensajes = new List<Mensajes>();
            Mensajes m_Mensaje = new Mensajes
            {
                CodigoCliente = "123",
                CodigoMensaje = "1234",
                MensajeEnviado = "Prueba mensajes"

            };
            m_Mensajes.Add(m_Mensaje);
            m_Mensajes.Add(m_Mensaje);
            m_Mensajes.Add(m_Mensaje);
            ViewBag.ListaMensajes = m_Mensajes;

            //Conversiones
            List<ConversionMensaje> m_Conversiones = new List<ConversionMensaje>();
            ConversionMensaje m_Conversion = new ConversionMensaje
            {
                CodigoCliente = "1234",
                IdConversion = 1,
                MensajeCW = "What is it??",
                Mensaje = "Prueba conversion"

            };

            m_Conversiones.Add(m_Conversion);
            m_Conversiones.Add(m_Conversion);
            m_Conversiones.Add(m_Conversion);
            ViewBag.ListaConversionM = m_Conversiones;
            return View();
        }

        [OutputCache(Duration = 40)]
        public ActionResult EstadosCW()
        {
            //Estados
            if (Session["Estados"] == null)
            {
                List<Estados> m_Estados = new List<Estados>();
                Estados m_Estado = new Estados
                {
                    IdEstado = 1,
                    NombreEstado = "Estado Inicio"
                };
                m_Estados.Add(m_Estado);
                 m_Estado = new Estados
                {
                    IdEstado = 2,
                    NombreEstado = "Estado Prueba"
                };
                m_Estados.Add(m_Estado);
                m_Estado = new Estados
                {
                    IdEstado = 2,
                    NombreEstado = "Estado Tercero"
                };
                m_Estados.Add(m_Estado);
                Session["Estados"] = m_Estados;
            }

            //COnversion Estado X Impresora

            List<EstadoXImpresoraC> m_EstadosXImpresora = new List<EstadoXImpresoraC>();
            EstadoXImpresoraC m_EstadoxImpresora = new EstadoXImpresoraC
            {
                IdCodRespuesta = 1,
                DeviceStatus = "1",
                DeviceType = "1",
                ReturnCode = "1",
                Estado = new Estados
                {
                    IdEstado = 1,
                    NombreEstado = "PruebaInicial"
                }

            };


            m_EstadosXImpresora.Add(m_EstadoxImpresora);
            m_EstadosXImpresora.Add(m_EstadoxImpresora);
            m_EstadosXImpresora.Add(m_EstadoxImpresora);
            ViewBag.ListaEstadoXImpresora = m_EstadosXImpresora;
            //Conversiones x Impresion
            List<EstadoXImpresion> m_EstadosXImpresion = new List<EstadoXImpresion>();
            EstadoXImpresion m_EstadoXImpresion = new EstadoXImpresion
            {
                IdCodRespuesta = 1,
                IdDataInteger = "1",
                IdMessageType = "1",
                Estado = new Estados
                {
                    IdEstado = 1,
                    NombreEstado = "Prueba"
                }

            };
            m_EstadosXImpresion.Add(m_EstadoXImpresion);
            m_EstadosXImpresion.Add(m_EstadoXImpresion);
            m_EstadosXImpresion.Add(m_EstadoXImpresion);
            ViewBag.ListEstadoXImpresion = m_EstadosXImpresion;
            return View();
        }

        [OutputCache(Duration = 40)]
        public ActionResult UsuariosCW()
        {
            UsuarioC m_User = (UsuarioC)Session["Usuario"];
            if (m_User.RolAsociado.Usuarios == true) 
            {
                List<UsuarioC> m_ListUsuario = new List<UsuarioC>();
                //Capa negocio
                UsuarioC user = new UsuarioC();
                user.Nombre = "Andres";
                user.Apellido = "Roldan";
                user.Identificacion = "123456";
                user.RolAsociado = new Rol();
                user.RolAsociado.IdRol = 1;
                user.RolAsociado.NombreRol = "Admin";
                user.RolAsociado.Configuracion = true;
                m_ListUsuario.Add(user);
                m_ListUsuario.Add(user);
                m_ListUsuario.Add(user);
                m_ListUsuario.Add(user);
                m_ListUsuario.Add(user);
                m_ListUsuario.Add(user);
                ViewBag.ListUsuarios = m_ListUsuario;
            }
            //Roles
           
            ViewBag.ListRoles = m_User.Roles;

            return View();
        }
    }
}