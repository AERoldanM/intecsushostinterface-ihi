﻿using IHIConfig.Filtros;
using Intecsus.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IHIConfig.Controllers
{
    [Loging(Order = 1)]
    [LogErrors(Order = 2)]
    [HandleError(Order = 3)]
    public class EstadosController : Controller
    {
        // GET: Estados
        public ActionResult Index()
        {
            //Optimizar
            if (Session["Estados"] == null)
            {
                List<Estados> m_Estados = new List<Estados>();
                Estados m_Estado = new Estados
                {
                    IdEstado = 1,
                    NombreEstado = "Estado Inicio"
                };
                m_Estados.Add(m_Estado);
                m_Estados.Add(m_Estado);
                m_Estados.Add(m_Estado);
                Session["Estados"] = m_Estados;
            }
            return View();
        }

        public PartialViewResult Details(Estados estados)
        {
            ViewBag.Estados = "Modificar";
            return PartialView("_Create", estados);
        }

        public PartialViewResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public PartialViewResult Crear(Estados estado)
        {
            //OPTIMIZAR y validar si viene vacio para no ejecutar la operacion
            List<Estados> m_Estados = new List<Estados>();
            estado.IdEstado = 1;
            m_Estados.Add(estado);
            m_Estados.Add(estado);
            m_Estados.Add(estado);
            Session["Estados"] = m_Estados;
            ViewBag.MensajeSuccess = "Estado Creado";
            return PartialView("Index");
        }

        [HttpPost]
        public PartialViewResult Modificar(Estados estado)
        {
            //OPTIMIZAR
            List<Estados> m_Estados = new List<Estados>();
            estado.IdEstado = 2;
            m_Estados.Add(estado);
            m_Estados.Add(estado);
            m_Estados.Add(estado);
            Session["Estados"] = m_Estados;
            ViewBag.MensajeSuccess = "Estado Modificado";
            return PartialView("Index");
        }

        [HttpPost]
        public PartialViewResult Delete(int idEstado)
        {
            //OPTIMIZAR
            List<Estados> m_Estados = new List<Estados>();
            Estados m_Estado = new Estados
            {
                IdEstado = 3,
                 NombreEstado = "Estado Eliminacion"
            };
            m_Estados.Add(m_Estado);
            m_Estados.Add(m_Estado);
            m_Estados.Add(m_Estado);
            Session["Estados"] = m_Estados;
            ViewBag.MensajeSuccess = "Estadon Eliminado";
            ViewBag.Informativo = "Se aconseja NO eliminar los Estados ya que afectara el funcionamiento del IHI";
            return PartialView("Index");
        }
    }
}