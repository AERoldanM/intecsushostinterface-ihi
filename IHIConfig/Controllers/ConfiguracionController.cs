﻿using IHIConfig.Filtros;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Intecsus.Entidades;

namespace IHIConfig.Controllers
{
    [Loging(Order = 1)]
    [LogErrors(Order = 2)]
    [HandleError(Order = 3)]
    public class ConfiguracionController : Controller
    {
        // GET: Configuracion
        public ActionResult Index()
        {
            List<Configurar> m_ListConfigurar = new List<Configurar>();
            //Capa negocio
            Configurar m_Config = new Configurar
            {
                IDValor = "1",
                Valor = "Pruebas",
                TipoDato = "Cadena"
            };
            m_ListConfigurar.Add(m_Config);
            m_ListConfigurar.Add(m_Config);
            m_ListConfigurar.Add(m_Config);
            m_ListConfigurar.Add(m_Config);
            m_ListConfigurar.Add(m_Config);
            m_ListConfigurar.Add(m_Config);
            ViewBag.ListaConfig = m_ListConfigurar;
            return View("Index");
        }

        // GET: Configuracion/Details
        public PartialViewResult Details(Configurar configurar)
        {
            ViewBag.ConfigIHI = "Modificar";
            return PartialView("_Create", configurar);
        }

        // GET: Configuracion/Create
        public PartialViewResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public PartialViewResult Crear(Configurar config)
        {
            //OPTIMIZAR y validar si viene vacio para no ejecutar la operacion
            List<Configurar> m_Configs = new List<Configurar>();
            Configurar m_Config = new Configurar
            {
                IDValor = "1",
                Valor = "Pruebas Creacion",
                TipoDato = "Cadena"
            };
            m_Configs.Add(m_Config);
            m_Configs.Add(m_Config);
            m_Configs.Add(m_Config);
            ViewBag.ListaConfig = m_Configs;
            ViewBag.MensajeSuccess = "Parametro de configuración Creado";
            return PartialView("Index");
        }

        [HttpPost]
        public PartialViewResult Modificar(Configurar config)
        {
            //OPTIMIZAR
            List<Configurar> m_Configs = new List<Configurar>();
            Configurar m_Config = new Configurar
            {
                IDValor = "1",
                Valor = "Pruebas Edición",
                TipoDato = "Cadena"
            };
            m_Configs.Add(m_Config);
            m_Configs.Add(m_Config);
            m_Configs.Add(m_Config);
            ViewBag.ListaConfig = m_Configs;
            ViewBag.MensajeSuccess = "Parametro de configuración Modificado";
            return PartialView("Index");
        }

        [HttpPost]
        public PartialViewResult Delete(int idConfig)
        {
            //OPTIMIZAR
            List<Configurar> m_Configs = new List<Configurar>();
            Configurar m_Config = new Configurar
            {
                IDValor = "1",
                Valor = "Pruebas Eliminacion",
                TipoDato = "Cadena"
            };
            m_Configs.Add(m_Config);
            m_Configs.Add(m_Config);
            m_Configs.Add(m_Config);
            ViewBag.ListaConfig = m_Configs;
            ViewBag.MensajeSuccess = "Parametro de configuración Eliminado";
            ViewBag.Informativo = "Se aconseja NO eliminar los parametros ya que afectara el funcionamiento del IHI";
            return PartialView("Index");
        }
    }
}
