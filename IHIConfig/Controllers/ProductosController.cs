﻿using IHIConfig.Filtros;
using Intecsus.Entidades;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace IHIConfig.Controllers
{
    [Loging(Order = 1)]
    [LogErrors(Order = 2)]
    [HandleError(Order = 3)]
    public class ProductosController : Controller
    {
        // GET: Productos
        public PartialViewResult Index()
        {
            //OPTIMIZAR
            List<Producto> m_Productos = new List<Producto>();
            Producto m_Producto = new Producto
            {
                IdProducto = 1,
                ProductoName = "Producto Prueba",
                CardName = "Pruebas",
                Cliente = "Prueba",
                XML = @"<Parametros>
                                  <Parametro nombre='@PAN@'  tipo='String' codifica='SI' derecha='5' izquierda='3' >
                                    <Valor inicio='63' tamano='16' ISPCI='TRUE' ValoresS='/RespuestaRealceEmisionTarjetasDTO/trackIField/panField'  ></Valor>
                                  </Parametro>
                                  <Parametro nombre='@CardHolderName@'  tipo='String' codifica='NO'>
                                    <Valor inicio='38' tamano='40'  ISPCI='TRUE'  ValoresS='/RespuestaRealceEmisionTarjetasDTO/nombreCompletoField'></Valor>
                                  </Parametro>
                                  <Parametro nombre='@TIPODOCUMENTO1@'  tipo='String' codifica='NO'>
                                    <Valor inicio='38' tamano='2' ValoresS='/RespuestaRealceEmisionTarjetasDTO/tipoIdentificacionAField'></Valor>
                                  </Parametro>
                                </Parametros>",
                Tamano = 200
            };
            m_Productos.Add(m_Producto);
            m_Productos.Add(m_Producto);
            m_Productos.Add(m_Producto);
            Session["ListaProductos"] = m_Productos;
            return PartialView("Index");
        }

        public PartialViewResult Details(Producto producto)
        {
            ViewBag.EditarProducto = "Modificar";
            //Obtener XML no se puede por temas de seguridad
            if (Session["ListaProductos"] != null) 
            {
                List<Producto> m_Productos = (List<Producto>)Session["ListaProductos"];
                //Un metodo en la capa de negocio
                producto.XML = (from p in m_Productos
                                where p.IdProducto == producto.IdProducto
                                select p.XML).First();

            }
            return PartialView("_Create", producto);
        }

        public PartialViewResult Create()
        {
            return PartialView("_Create");
        }

        /// <summary>
        /// Validate input false para q deje pasar los XML
        /// </summary>
        /// <param name="producto"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateInput(false)]
        public PartialViewResult Crear(Producto producto)
        {
            //OPTIMIZAR  y validar si viene vacio para no ejecutar la operacion
            List<Producto> m_Productos = new List<Producto>();
            Producto m_Producto = new Producto 
                    {
                        IdProducto = 1,
                        ProductoName = "Producto Creacion",
                        CardName = "Creado",
                        Cliente = "Prueba",
                        XML = @"<Parametros>
                                  <Parametro nombre='@PAN@'  tipo='String' codifica='SI' derecha='5' izquierda='3' >
                                    <Valor inicio='63' tamano='16' ISPCI='TRUE' ValoresS='/RespuestaRealceEmisionTarjetasDTO/trackIField/panField'  ></Valor>
                                  </Parametro>
                                  <Parametro nombre='@CardHolderName@'  tipo='String' codifica='NO'>
                                    <Valor inicio='38' tamano='40'  ISPCI='TRUE'  ValoresS='/RespuestaRealceEmisionTarjetasDTO/nombreCompletoField'></Valor>
                                  </Parametro>
                                  <Parametro nombre='@TIPODOCUMENTO1@'  tipo='String' codifica='NO'>
                                    <Valor inicio='38' tamano='2' ValoresS='/RespuestaRealceEmisionTarjetasDTO/tipoIdentificacionAField'></Valor>
                                  </Parametro>
                                </Parametros>",
                        Tamano = 200
                    };
            m_Productos.Add(m_Producto);
            m_Productos.Add(m_Producto);
            m_Productos.Add(m_Producto);
            Session["ListaProductos"] = m_Productos;
            ViewBag.MensajeSuccess = "Producto Creado";
            return PartialView("Index");
        }


        [HttpPost]
        [ValidateInput(false)]
        public PartialViewResult Modificar(Producto producto)
        {
            //OPTIMIZAR
            List<Producto> m_Productos = new List<Producto>();
            Producto m_Producto = new Producto
            {
                IdProducto = 1,
                ProductoName = "Producto Modificacion",
                CardName = "Modificado",
                Cliente = "Prueba",
                XML = @"<Parametros>
                                  <Parametro nombre='@PAN@'  tipo='String' codifica='SI' derecha='5' izquierda='3' >
                                    <Valor inicio='63' tamano='16' ISPCI='TRUE' ValoresS='/RespuestaRealceEmisionTarjetasDTO/trackIField/panField'  ></Valor>
                                  </Parametro>
                                  <Parametro nombre='@CardHolderName@'  tipo='String' codifica='NO'>
                                    <Valor inicio='38' tamano='40'  ISPCI='TRUE'  ValoresS='/RespuestaRealceEmisionTarjetasDTO/nombreCompletoField'></Valor>
                                  </Parametro>
                                  <Parametro nombre='@TIPODOCUMENTO1@'  tipo='String' codifica='NO'>
                                    <Valor inicio='38' tamano='2' ValoresS='/RespuestaRealceEmisionTarjetasDTO/tipoIdentificacionAField'></Valor>
                                  </Parametro>
                                </Parametros>",
                Tamano = 200
            };
            m_Productos.Add(m_Producto);
            m_Productos.Add(m_Producto);
            m_Productos.Add(m_Producto);
            Session["ListaProductos"] = m_Productos;
            ViewBag.MensajeSuccess = "Producto Modificado";
            return PartialView("Index");
        }

        [HttpPost]
        public PartialViewResult Delete(int idProducto)
        {
            //OPTIMIZAR 
            List<Producto> m_Productos = new List<Producto>();
            Producto m_Producto = new Producto
            {
                IdProducto = 1,
                ProductoName = "Producto Eliminacion",
                CardName = "Eliminado",
                Cliente = "Prueba",
                XML = @"<Parametros>
                                  <Parametro nombre='@PAN@'  tipo='String' codifica='SI' derecha='5' izquierda='3' >
                                    <Valor inicio='63' tamano='16' ISPCI='TRUE' ValoresS='/RespuestaRealceEmisionTarjetasDTO/trackIField/panField'  ></Valor>
                                  </Parametro>
                                  <Parametro nombre='@CardHolderName@'  tipo='String' codifica='NO'>
                                    <Valor inicio='38' tamano='40'  ISPCI='TRUE'  ValoresS='/RespuestaRealceEmisionTarjetasDTO/nombreCompletoField'></Valor>
                                  </Parametro>
                                  <Parametro nombre='@TIPODOCUMENTO1@'  tipo='String' codifica='NO'>
                                    <Valor inicio='38' tamano='2' ValoresS='/RespuestaRealceEmisionTarjetasDTO/tipoIdentificacionAField'></Valor>
                                  </Parametro>
                                </Parametros>",
                Tamano = 200
            };
            m_Productos.Add(m_Producto);
            m_Productos.Add(m_Producto);
            m_Productos.Add(m_Producto);
            Session["ListaProductos"] = m_Productos;
            ViewBag.MensajeSuccess = "Producto Eliminado";
            return PartialView("Index");
        }
    }
}