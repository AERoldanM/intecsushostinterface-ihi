﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Routing;

namespace IHIConfig.Filtros
{
    public class LogingAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //Se ejecuta ANTES de que empiece a ejecutarse el controlador
            var m_Session = filterContext.HttpContext.Session["Usuario"];
            if (m_Session == null)
            {
                filterContext.Result = new RedirectToRouteResult(
                        new RouteValueDictionary {{ "Controller", "Home" },
                                      { "Action", "Index" } });
                filterContext.Controller.ViewBag.MensajeError = "Session Expirada";

            }

            base.OnActionExecuting(filterContext);
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);
        }

        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            base.OnResultExecuting(filterContext);
        }
    }
}
