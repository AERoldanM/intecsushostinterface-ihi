﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace IHIConfig.Filtros
{
    /// <summary>
    /// Filtro para obtener error en caso que suceda en presentación
    /// </summary>
    public class LogErrorsAttribute : FilterAttribute, IExceptionFilter
    {
        #region IExceptionFilter Members

         void IExceptionFilter.OnException(ExceptionContext filterContext)
        {
            if (filterContext != null && filterContext.Exception != null)
            {
                string m_controller = filterContext.RouteData.Values["controller"].ToString();
                string m_action = filterContext.RouteData.Values["action"].ToString();
                string m_error = filterContext.Exception.Message;
                //ALMACENAMOS EL ERROR EN LA BASE DE DATOS......
            }

        }

        #endregion
    }
}
