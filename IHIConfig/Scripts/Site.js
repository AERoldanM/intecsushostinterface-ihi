﻿/// <reference path="jquery-1.10.2.min.js" />
/// <reference path="jquery.validate.unobtrusive.min.js" />
/// <reference path="jquery.validate.min.js" />
/// <reference path="jquery.tablesorter.min.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
$(document).ready(function () {
    Utilidades.ActivarTableSorter();
    var onSuccess = function (result) {
        // enable unobtrusive validation for the contents
        // that was injected into the <div id="result"></div> node
        return $.validator.unobtrusive.parse($(result));
    };
}
);

/// <summary>
/// Activa la funcionalidad de ordenar las columnas de una tabla
/// </summary>
var Utilidades = {
    ActivarTableSorter: function () {
        $(".tableSorter").tablesorter({
            widgets: ['zebra'],
            dedug: false,
            /*textExtractionCustom: {
            6: function (o) {
            return alert($('img', o).src);
            },
            1: function (o) {
            return $('input', o).val();
            },
            2: function (o) {
            return $('input', o).val();
            },
            3: function (o) {
            return $('select', o).val();
            },
            4: function (o) {
            return ($('input', o).is(':checked')) ? '0': '1';
            },
            5: function (o) {
            return $('input:checked', o).val();
            }
            },*/
            textExtraction: function (s) {
                var $el = $(s),
                $img = $el.find('img');
                return $img.length ? $img[0].src : $el.text();
            }
        })

        $(".tableSorter").each(function () {
            var $paginador = $(this).parent("div:first").siblings(".tablaHerramientas:first").find(".tablaPaginador:first");
            if ($paginador.size() == 1) {
                var filas = $paginador.attr("tableSorterSize");
                if (!filas || filas == '' || filas % 10 != 0 || filas < 10)
                    filas = 10;
                else if (filas > 50)
                    filas = 50;

                $(this).tablesorterPager({ container: $paginador, size: filas });

                $paginador.find('.pagesize [value="' + filas + '"]').attr('selected', true);
            }
            var $busqueda = $(this).parent("div:first").siblings(".tablaHerramientas:first").find(".tablaFiltro:first");
            if (!$.browser.mozilla) {
                if ($busqueda.size() == 1) {
                    $(this).tablesorterFilter({
                        filterContainer: $busqueda.find("input"),
                        filterClearContainer: $busqueda.find("img"),
                        filterCaseSensitive: false
                    });
                }
            }
            else {
                $busqueda.hide();
            }
        });
    }
}