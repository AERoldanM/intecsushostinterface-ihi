﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Principal.Master" AutoEventWireup="true" CodeBehind="Configurador.aspx.cs" Inherits="Intecsus.Configurador.Paginas.Configurador" %>

<%@ Register Src="../UserControls/ListaConfiguracion.ascx" TagName="ListaConfiguracion" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnConfiguracion" runat="server">
        <ContentTemplate>
            <div class="grilla960_12_4 flotanteIzq">
                <div class="caja cajaOculta">
                    <div class="cajaTitulo">
                        Edición de la Configuración
                    </div>
                    <div class="cajaContenido">
                        <asp:UpdatePanel ID="upnFormulario" runat="server">
                            <ContentTemplate>
                                <table width="100%">
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblIDValor" runat="server" Text="IdValor:"></asp:Label>
                                        </td>
                                        <td align="left" colspan="2">
                                            <asp:TextBox ID="txtIdValor" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="fvvIdValor" runat="server" ControlToValidate="txtIdValor"
                                                ErrorMessage="* Obligatorio" ValidationGroup="vgConfig"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblValor" runat="server" Text="Valor:"></asp:Label>
                                        </td>
                                        <td align="left" colspan="2">
                                            <asp:TextBox ID="txtValor" runat="server" TextMode="MultiLine" Width="95%" Height="150px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvValor" runat="server" ControlToValidate="txtValor"
                                                ErrorMessage="* Obligatorio" ValidationGroup="vgConfig"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td align="left">
                                            <asp:Label ID="lblValidador" runat="server" Text="Validación:"></asp:Label>
                                        </td>
                                        <td align="left" colspan="2">
                                            <asp:TextBox ID="txtValidacion" runat="server" ></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:Button ID="btnInsert" runat="server" Text="Insertar" ValidationGroup="vgConfig" OnClick="btnInsert_Click" />
                                        </td>
                                        <td align="center">
                                            <asp:Button ID="btnModificar" runat="server" Text="Modificar" ValidationGroup="vgConfig" OnClick="btnModificar_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>

                    </div>
                </div>
            </div>
            <asp:Panel ID="pnlGrillaTiendas" runat="server">
                <div class="grilla960_12_6 flotanteDer">
                    <div class="caja cajaOculta">
                        <div class="cajaTitulo">
                            Lista Configuraciones
                        </div>
                        <div class="cajaContenido">
                            <uc1:ListaConfiguracion ID="ListaConfiguracion1" runat="server" />
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
