﻿using Intecsus.Entidades;
using Intecsus.NegocioGeneral;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Intecsus.Configurador.Paginas
{
    public partial class EstadoTransacciones: System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                IBLGeneral m_operacion = new BLGeneral();
                this.CargarGrilla(m_operacion.ObtenerEstadoTransaccion());
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(this.txtTranCliente.Text) && string.IsNullOrEmpty(this.txtTransaccion.Text)) 
                {
                    (this.Master as VistaMaestraBase).MostrarNotificacion("Debes llenar al menos un filtro de busqueda", VistaMaestraBase.TipoNotificacion.Informacion);
                    return;
                }

                IBLGeneral m_operacion = new BLGeneral();
                this.CargarGrilla(m_operacion.ObtenerEstadoTransaXTran(this.txtTransaccion.Text, this.txtTranCliente.Text));
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }

        private void CargarGrilla(List<EstadoTransaccion> estados)
        {
            try
            {
                this.ListaTransacciones1.CargarConfiguraciones(estados);
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }
    }
}