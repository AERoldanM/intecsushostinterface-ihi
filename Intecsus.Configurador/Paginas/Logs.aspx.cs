﻿using Intecsus.Entidades;
using Intecsus.NegocioGeneral;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Intecsus.Configurador.Paginas
{
    public partial class Logs: System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                if (!IsPostBack)
                {
                    int m_Registros = Convert.ToInt32(ConfigurationManager.AppSettings["Resgistros"].ToString());
                    ViewState["Registro"] = m_Registros;
                    ViewState["Pagina"] = 1;
                    this.btnAnterior.Visible = false;
                }

                int m_Pagina = (int)ViewState["Pagina"];
                int m_Registro = Convert.ToInt32(ConfigurationManager.AppSettings["Resgistros"].ToString());
                IBLGeneral m_operacion = new BLGeneral();
                this.CargarGrilla(m_operacion.ObtenerLogs(m_Registro, m_Pagina));
                (this.Master as VistaMaestraBase).LanzarJavascript("Calendario();", "Calendario");
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }

        private void CargarGrilla(List<LOG> logs)
        {
            try
            {
                this.ListaLogs1.CargarConfiguraciones(logs);
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(this.txtTransaccion.Text) && string.IsNullOrEmpty(this.txtTranCliente.Text) && string.IsNullOrEmpty(this.txtFechaInicio.Text) && string.IsNullOrEmpty(this.txtFechaFin.Text)) 
                {
                    (this.Master as VistaMaestraBase).LanzarJavascript("alert('Señor usuario debe diligeciar algún campo');", "MensajeAdvertencia");
                    return;
                }

                this.btnAnterior.Visible = false;
                this.btnSiguiente.Visible = false;
                IBLGeneral m_operacion = new BLGeneral();
                this.CargarGrilla(m_operacion.ObtenerLogXTran(this.txtTransaccion.Text, this.txtTranCliente.Text, this.txtFechaInicio.Text, this.txtFechaFin.Text));
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }

        protected void btnAnterior_Click(object sender, EventArgs e)
        {
            try 
            {
                int m_Pagina = (int)ViewState["Pagina"] - 1;
                int m_Registro = Convert.ToInt32(ConfigurationManager.AppSettings["Resgistros"].ToString());
                IBLGeneral m_operacion = new BLGeneral();
                this.CargarGrilla(m_operacion.ObtenerLogs(m_Registro, m_Pagina));
                ViewState["Pagina"] = m_Pagina;
                if(m_Pagina <= 1)
                    this.btnAnterior.Visible = false;
            }
            catch (Exception ex) { (this.Master as VistaMaestraBase).MostrarNotificacion(ex); }

        }

        protected void btnSiguiente_Click(object sender, EventArgs e)
        {
            try {

                int m_Pagina = (int)ViewState["Pagina"] + 1;
                int m_Registro = Convert.ToInt32(ConfigurationManager.AppSettings["Resgistros"].ToString());
                IBLGeneral m_operacion = new BLGeneral();
                this.CargarGrilla(m_operacion.ObtenerLogs(m_Registro, m_Pagina));
                ViewState["Pagina"] = m_Pagina;
                this.btnAnterior.Visible = true;
            }
            catch (Exception ex) { (this.Master as VistaMaestraBase).MostrarNotificacion(ex); }
        }
    }
}