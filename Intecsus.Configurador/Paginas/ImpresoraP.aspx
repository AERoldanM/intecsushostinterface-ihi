﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Principal.Master" AutoEventWireup="true" CodeBehind="ImpresoraP.aspx.cs" Inherits="Intecsus.Configurador.Paginas.ImpresoraP" %>

<%@ Register Src="../UserControls/ListaImpresoras.ascx" TagName="ListaImpresoras" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnImpresora" runat="server">
        <ContentTemplate>
            <div class="grilla960_12_4 flotanteIzq">
                <div class="caja cajaOculta">
                    <div class="cajaTitulo">
                        Impresoras
                    </div>
                    <div class="cajaContenido">
                        <asp:UpdatePanel ID="upnFormulario" runat="server">
                            <ContentTemplate>
                                <table width="100%">
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblIDImpresora" runat="server" Text="Id Impresora:"></asp:Label>
                                        </td>
                                        <td align="left" colspan="2">
                                            <asp:TextBox ID="txtIDImpresora" runat="server" Enabled="false" Text="0"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="fvvIDImpresora" runat="server" ControlToValidate="txtIDImpresora"
                                                ErrorMessage="* Obligatorio" ValidationGroup="vgConfig"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblNombreRecibido" runat="server" Text="Nombre Recibido:"></asp:Label>
                                        </td>
                                        <td align="left" colspan="2">
                                            <asp:TextBox ID="txtNombrerecibido" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtNombrerecibido" runat="server" ControlToValidate="txtNombrerecibido"
                                                ErrorMessage="* Obligatorio" ValidationGroup="vgConfig"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblPCName" runat="server" Text="PCNAME:"></asp:Label>
                                        </td>
                                        <td align="left" colspan="2">
                                            <asp:TextBox ID="txtPCNAME" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvPCNAME" runat="server" ControlToValidate="txtPCNAME"
                                                ErrorMessage="* Obligatorio" ValidationGroup="vgConfig"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblIP" runat="server" Text="IP:"></asp:Label>
                                        </td>
                                        <td align="left" colspan="2">
                                            <asp:TextBox ID="txtIP" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvIP" runat="server" ControlToValidate="txtIP"
                                                ErrorMessage="* Obligatorio" ValidationGroup="vgConfig"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnInsertar" runat="server" Text="Insertar" ValidationGroup="vgConfig" OnClick="btnInsertar_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnModificar" runat="server" Text="Modificar" ValidationGroup="vgConfig" OnClick="btnModificar_Click" />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" ValidationGroup="vgConfig" OnClick="btnEliminar_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
            <asp:Panel ID="pnlGrillaTiendas" runat="server">
                <div class="grilla960_12_6 flotanteDer">
                    <div class="caja cajaOculta">
                        <div class="cajaTitulo">
                            Lista Impresoras
                        </div>
                        <div class="cajaContenido">
                            <uc1:ListaImpresoras ID="ListaImpresoras1" runat="server" />
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
