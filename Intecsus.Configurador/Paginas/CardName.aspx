﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Principal.Master" AutoEventWireup="true" CodeBehind="CardName.aspx.cs" Inherits="Intecsus.Configurador.Paginas.CardName" %>

<%@ Register src="../UserControls/ListaProductos.ascx" tagname="ListaProductos" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnCardFormat" runat="server">
        <ContentTemplate>
            <div class="grilla960_12_4 flotanteIzq">
                <div class="caja cajaOculta">
                    <div class="cajaTitulo">
                        Producto X CW
                    </div>
                    <div class="cajaContenido">
                        <asp:UpdatePanel ID="upnFormulario" runat="server">
                            <ContentTemplate>
                                <table width="100%">
                                    <tr>
                                        <td align="left" valign="top">
                                            <asp:Label ID="lblIDProducto" runat="server" Text="Id:"></asp:Label>
                                        </td>
                                        <td align="left" colspan="2" valign="top">
                                            <asp:TextBox ID="txtIDProducto" runat="server" Enabled="false" Text="0"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="fvvIDProducto" runat="server" ControlToValidate="txtIDProducto"
                                                ErrorMessage="* Obligatorio" ValidationGroup="vgConfig"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td align="left" valign="top">
                                            <asp:Label ID="lblCliente" runat="server" Text="Cliente:"></asp:Label>
                                        </td>
                                        <td align="left" colspan="2" valign="top">
                                            <asp:TextBox ID="txtCliente" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtCliente" runat="server" ControlToValidate="txtCliente"
                                                ErrorMessage="* Obligatorio" ValidationGroup="vgConfig"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td align="left" valign="top">
                                            <asp:Label ID="lblProducto" runat="server" Text="Producto:"></asp:Label>
                                        </td>
                                        <td align="left" colspan="2">
                                            <asp:TextBox ID="txtProducto" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtProducto" runat="server" ControlToValidate="txtProducto"
                                                ErrorMessage="* Obligatorio" ValidationGroup="vgConfig"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td align="left" valign="top">
                                            <asp:Label ID="lblCardName" runat="server" Text="Card Name:"></asp:Label>
                                        </td>
                                        <td align="left" colspan="2">
                                            <asp:TextBox ID="txtCardName" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtCardName" runat="server" ControlToValidate="txtCardName"
                                                ErrorMessage="* Obligatorio" ValidationGroup="vgConfig"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                     <tr>
                                        <td>
                                            <asp:Button ID="btnInsertar" runat="server" Text="Insertar" ValidationGroup="vgConfig" OnClick="btnInsertar_Click"  />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnModificar" runat="server" Text="Modificar" ValidationGroup="vgConfig" OnClick="btnModificar_Click"  />
                                        </td>
                                        <td>
                                            <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" ValidationGroup="vgConfig" OnClick="btnEliminar_Click" />
                                        </td>
                                    </tr>
                                    </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
            <asp:Panel ID="pnlGrillaTiendas" runat="server">
                <div class="grilla960_12_6 flotanteDer">
                    <div class="caja cajaOculta">
                        <div class="cajaTitulo">
                            Lista Productos
                        </div>
                        <div class="cajaContenido">
                            <uc1:ListaProductos ID="ListaProductos1" runat="server" />
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
