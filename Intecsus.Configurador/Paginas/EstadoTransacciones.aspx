﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Principal.Master" AutoEventWireup="true" CodeBehind="EstadoTransacciones.aspx.cs" Inherits="Intecsus.Configurador.Paginas.EstadoTransacciones" %>

<%@ Register src="../UserControls/ListaTransacciones.ascx" tagname="ListaTransacciones" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnContenido" runat="server">
        <ContentTemplate>
            <div class="grilla960_12_6 flotanteIzq">
                <div class="caja cajaOculta">
                    <div class="cajaTitulo">
                        Filtrar
                    </div>
                    <div class="cajaContenido">
                        <asp:UpdatePanel ID="upnFormulario" runat="server">
                            <ContentTemplate>
                                <table width="100%">
                                     <tr>
                                        <td align="left">
                                            <asp:Label ID="lblIDTransaccion" runat="server" Text="Id Transacción:"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTransaccion" runat="server" Text="0"></asp:TextBox>
                                          
                                              <asp:RegularExpressionValidator ID="revTelC" runat="server" ErrorMessage="! Solo Nùmeros"
                                                SetFocusOnError="true" ControlToValidate="txtTransaccion" Display="Dynamic" ValidationExpression="[0123456789]*"
                                                ValidationGroup="vgConfig"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblTransaccionCliente" runat="server" Text="Transacción Cliente:"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTranCliente" runat="server"></asp:TextBox>
                                           
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  align="center" colspan="2">
                                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
            <div class=" separacion limpiarFlotante "></div>
            <asp:Panel ID="pnlGrillaTiendas" runat="server">
                <div class="grilla960_12_9 flotanteIzq">
                    <div class="caja cajaOculta">
                        <div class="cajaTitulo">
                            Lista Transacciones
                        </div>
                        <div class="cajaContenido">
                            
                            <uc1:ListaTransacciones ID="ListaTransacciones1" runat="server" />
                            
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
