﻿using Intecsus.Entidades;
using Intecsus.NegocioGeneral;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Intecsus.Configurador.Paginas
{
    public partial class CambiarPassword: System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (Session["Usuario"] == null)
                    {
                        Response.Redirect("~/Paginas/Login.aspx");
                    }
                }
                catch (Exception exc)
                {
                    (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
                }
            }
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.txtNuevaContrasena.Text == this.txtPassword.Text)
                {
                    IBLGeneral m_operar = new BLGeneral();
                    Usuario m_user = (Usuario)Session["Usuario"];
                    m_user = m_operar.CambiarContrasena(m_user, this.txtNuevaContrasena.Text);
                    Session["Usuario"] = m_user;
                    (this.Master as VistaMaestraBase).MostrarNotificacion("La contraseña fue modificada", VistaMaestraBase.TipoNotificacion.Realizado);
                }
                else
                    (this.Master as VistaMaestraBase).MostrarNotificacion("La contraseña no es igual", VistaMaestraBase.TipoNotificacion.Error);

            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }
    }
}