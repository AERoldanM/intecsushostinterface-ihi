﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Principal.Master" AutoEventWireup="true" CodeBehind="Inicio.aspx.cs" Inherits="Intecsus.Configurador.Paginas.Inicio" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="grilla960_12_7 flotanteIzq">
        <div class="caja cajaOculta"> 
            <div class="cajaTitulo">
                Informativo...
            </div>
             <div class="cajaContenido">
                 <B>Recuerda</B> que la configuración de la cola cliente para el servicio WEB consumido se debe modificar directamente en el Web.config de la aplicación. 
                 <br />
                 <div class="separacion"></div>
                 De igual forma las <B>cadenas de conexión a la base de datos</B> debe ser configuradas directamente en los app.config de las aplicaciones instaladas (Incluyendo el probador en caso que se necesite).
                 </div>
        </div>
    </div>
</asp:Content>
