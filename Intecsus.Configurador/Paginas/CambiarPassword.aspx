﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Principal.Master" AutoEventWireup="true" CodeBehind="CambiarPassword.aspx.cs" Inherits="Intecsus.Configurador.Paginas.CambiarPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnLogin" runat="server">
        <ContentTemplate>
            <div align="center">
                <div class="grilla960_12_5">
                    <div class="caja cajaOculta">
                        <div class="cajaTitulo">
                            Cambiar Contraseña
                        </div>
                        <div class="cajaContenido">
                            <p>
                                Señor usuario: Recuerde que tamaño máximo de la contraseña es de 20 caracteres.
                            </p>
                            <table width="100%">
                                <tr>
                                    <td align="left" valign="top">
                                        <asp:Label ID="lblUsuario" runat="server" Text="Nueva Contraseña:"></asp:Label>
                                    </td>
                                    <td align="left" valign="top">
                                        <asp:TextBox ID="txtNuevaContrasena" runat="server" TextMode="Password" MaxLength="20"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvLogin1" runat="server" ControlToValidate="txtNuevaContrasena"
                                            ErrorMessage="* Obligatorio" ValidationGroup="vgLogin"></asp:RequiredFieldValidator>
                                        <%--  <asp:RegularExpressionValidator ID="revLogin" runat="server" ErrorMessage="! Solo Nùmeros"
                                            SetFocusOnError="true" ControlToValidate="txtUsuario" Display="Dynamic" ValidationExpression="[0123456789]*"
                                            ValidationGroup="vgLogin"></asp:RegularExpressionValidator>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="top">
                                        <asp:Label ID="lblPassword" runat="server" Text="Confirmar Contraseña:"></asp:Label>
                                    </td>
                                    <td align="left" valign="top">
                                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" MaxLength="20"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvLogin2" runat="server" ControlToValidate="txtPassword"
                                            ErrorMessage="* Obligatorio" ValidationGroup="vgLogin"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnModificar" runat="server" Text="Cambiar"
                                            ValidationGroup="vgLogin" OnClick="btnModificar_Click" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
