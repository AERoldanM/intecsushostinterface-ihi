﻿using Intecsus.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Intecsus.NegocioGeneral;

namespace Intecsus.Configurador.Paginas
{
    public partial class Login: System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Session["Usuario"] = null;
        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            try
            {
                IBLGeneral m_operar = new BLGeneral();
                Usuario m_user = new Usuario();
                m_user.contrasena =this.txtPassword.Text;
                m_user.userID = this.txtUsuario.Text;

                if (m_operar.ValidarUsuario(m_user))
                {
                    Session["Usuario"] = m_user;
                    Response.Redirect("~/Paginas/Inicio.aspx", false);
                }
                else{
                    (this.Master as VistaMaestraBase).MostrarNotificacion("Señor usuario: Verifique Usuario, contraseña y que su cuenta se encuentre activa", VistaMaestraBase.TipoNotificacion.Advertencia);
                }
                
                //Usuario user = new Usuario();
                //user.Cedula = int.Parse(this.txtUsuario.Text);
                //user.Contraseña = this.txtPassword.Text;
                //IBLUsuarios datosUsuarios = new BLUsuarios();
                //user = datosUsuarios.ObtenerPermisos(user);

                //if (user != null)
                //{
                //    Session["Usuario"] = user;
                //    datosUsuarios.CambioEstadoUsuario(user, true);
                //    Response.Redirect("~/Paginas/Inicio.aspx");
                //}
                //else
                //{
                //    (this.Master as VistaMaestraBase).MostrarNotificacion("Señor usuario: Verifique Usuario, contraseña y que su cuenta se encuentre activa", VistaMaestraBase.TipoNotificacion.Advertencia);
                //}
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }
    }
}