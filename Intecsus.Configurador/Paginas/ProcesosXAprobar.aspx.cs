﻿using Intecsus.Entidades;
using Intecsus.NegocioGeneral;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Intecsus.Configurador.Paginas
{
    public partial class ProcesosXAprobar: System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.ListaProcososXR1.SeSeleccionoProcesoXAP += new EventHandler<Generico<List<ProcesoXAP>>>(this.ListaProcososXR1_SeSeleccionoProcesoXAP);
                IBLGeneral m_operacion = new BLGeneral();
                this.CargarGrilla(m_operacion.ObtenerProcesosXAP());

            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }



        protected void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                ProcesoXAP m_dato = new ProcesoXAP();
                m_dato.ID = Convert.ToInt32(this.txtIDProcesXR.Text);
                m_dato.IDProceso = Convert.ToInt32(this.txtProceso.Text);
                m_dato.Intentos = Convert.ToInt32(this.txtIntentos.Text);
                m_dato.Fecha = this.txtFechaProceso.Text;
                m_dato.IDTransaccion = Convert.ToInt32(this.txtTransaccion.Text);
                m_dato.Informacion = this.txtInformacion.Text;
                IBLGeneral m_operacion = new BLGeneral();
                int m_CantidadReintento = int.Parse(m_operacion.ObtenerValoresConfigurador("CantReintentos"));
                if (m_dato.Intentos > m_CantidadReintento) 
                {
                    (this.Master as VistaMaestraBase).MostrarNotificacion("Cantidad de intentos mayor al permitido", VistaMaestraBase.TipoNotificacion.Error );
                    return;
                }
                m_operacion.ActualizarProcesoXAP(m_dato);
                this.CargarGrilla(m_operacion.ObtenerProcesosXAP());
                (this.Master as VistaMaestraBase).MostrarNotificacion("Se modifico correctamente", VistaMaestraBase.TipoNotificacion.Realizado);

            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }

        private void ListaProcososXR1_SeSeleccionoProcesoXAP(object sender, Generico<List<ProcesoXAP>> e)
        {
            try
            {
                foreach (ProcesoXAP m_dato in e.Argumento)
                {
                    this.txtIDProcesXR.Text = m_dato.ID.ToString();
                    this.txtProceso.Text = m_dato.IDProceso.ToString();
                    this.txtIntentos.Text = m_dato.Intentos.ToString();
                    this.txtFechaProceso.Text = m_dato.Fecha;
                    this.txtTransaccion.Text = m_dato.IDTransaccion.ToString();
                    this.txtInformacion.Text = m_dato.Informacion;
                    break;
                }
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }

        /// <summary>
        /// Carga grilla de configuraciones
        /// </summary>
        private void CargarGrilla(List<ProcesoXAP> procesos)
        {
            try
            {
                this.ListaProcososXR1.CargarConfiguraciones(procesos);
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }
    }
}