﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Principal.Master" AutoEventWireup="true" CodeBehind="ProcesosXAprobar.aspx.cs" Inherits="Intecsus.Configurador.Paginas.ProcesosXAprobar" %>

<%@ Register src="../UserControls/ListaProcososXR.ascx" tagname="ListaProcososXR" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="upnImpresora" runat="server">
        <ContentTemplate>
            <div class="grilla960_12_8 flotanteIzq">
                <div class="caja cajaOculta">
                    <div class="cajaTitulo">
                        ProcesosXAprobar
                    </div>
                    <div class="cajaContenido">
                        <asp:UpdatePanel ID="upnFormulario" runat="server">
                            <ContentTemplate>
                                <table width="100%">
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblIDProcesoXR" runat="server" Text="Id:"></asp:Label>
                                        </td>
                                        <td align="left" colspan="2">
                                            <asp:TextBox ID="txtIDProcesXR" runat="server" Enabled="false" Text="0"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblProceso" runat="server" Text="Proceso:"></asp:Label>
                                        </td>
                                        <td align="left" colspan="2">
                                            <asp:TextBox ID="txtProceso" runat="server" Enabled="false"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtProceso" runat="server" ControlToValidate="txtProceso"
                                                ErrorMessage="* Seleccione un proceso" ValidationGroup="vgConfig"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblFecha" runat="server" Text="Fecha:"></asp:Label>
                                        </td>
                                        <td align="left" colspan="2">
                                            <asp:TextBox ID="txtFechaProceso" runat="server" Enabled="false"></asp:TextBox>
                                         
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblInformacion" runat="server" Text="Informacion:"></asp:Label>
                                        </td>
                                        <td align="left" colspan="2">
                                             <asp:Label ID="txtInformacion"  runat="server" Text=""  Width="95%" Height="100px"></asp:Label>
                                        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblCantidadIntentos" runat="server" Text="Intentos:"></asp:Label>
                                        </td>
                                        <td align="left" colspan="2">
                                            <asp:TextBox ID="txtIntentos" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtIntentos" runat="server" ControlToValidate="txtIntentos"
                                                ErrorMessage="* Obligatorio" ValidationGroup="vgConfig"></asp:RequiredFieldValidator>
                                             <asp:RegularExpressionValidator ID="revTelC" runat="server" ErrorMessage="! Solo Nùmeros"
                                                SetFocusOnError="true" ControlToValidate="txtIntentos" Display="Dynamic" ValidationExpression="[0123456789]*"
                                                ValidationGroup="vgConfig"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblTrasaccion" runat="server" Text="ID Transaccion:"></asp:Label>
                                        </td>
                                        <td align="left" colspan="2">
                                            <asp:TextBox ID="txtTransaccion" runat="server" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="3">
                                            <asp:Button ID="btnModificar" runat="server" Text="Modificar" ValidationGroup="vgConfig" OnClick="btnModificar_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
            <div class=" separacion limpiarFlotante "></div>
            <asp:Panel ID="pnlGrillaTiendas" runat="server">
                <div class="grilla960_12_9 flotanteIzq">
                    <div class="caja cajaOculta">
                        <div class="cajaTitulo">
                            Lista ProcesoXAprobar
                        </div>
                        <div class="cajaContenido">
                            <uc1:ListaProcososXR ID="ListaProcososXR1" runat="server" />
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
