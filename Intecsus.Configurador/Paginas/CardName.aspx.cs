﻿using Intecsus.Entidades;
using Intecsus.NegocioGeneral;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Intecsus.Configurador.Paginas
{
    public partial class CardName: System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.ListaProductos1.SeSeleccionoProducto += new EventHandler<Generico<List<ProductoXCW>>>(this.ListaProductos1_SeSeleccionoProducto);
                IBLGeneral m_operacion = new Fabrica().InstanciaNegocio();
                this.CargarGrilla(m_operacion.ObtenerProductos());
                if (!IsPostBack)
                {
                    this.btnEliminar.Visible = false;
                    this.btnModificar.Visible = false;
                }
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }


        protected void btnInsertar_Click(object sender, EventArgs e)
        {
            try
            {
                ProductoXCW m_Producto = new ProductoXCW();
                m_Producto.Cliente = this.txtCliente.Text;
                m_Producto.Producto = this.txtProducto.Text;
                m_Producto.CardName = this.txtCardName.Text;
                IBLGeneral m_operacion = new BLGeneral();
                m_operacion.InsertarProducto(m_Producto);
                this.LimpiarCampos();
                (this.Master as VistaMaestraBase).MostrarNotificacion("Se inserto el producto correctamente", VistaMaestraBase.TipoNotificacion.Realizado);
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                ProductoXCW m_Producto = new ProductoXCW();
                m_Producto.ID = this.txtIDProducto.Text;
                m_Producto.Cliente = this.txtCliente.Text;
                m_Producto.Producto = this.txtProducto.Text;
                m_Producto.CardName = this.txtCardName.Text;
                IBLGeneral m_operacion = new BLGeneral();
                m_operacion.ActualizarProducto(m_Producto);
                this.CargarGrilla(m_operacion.ObtenerProductos());
                (this.Master as VistaMaestraBase).MostrarNotificacion("Se modificó el producto correctamente", VistaMaestraBase.TipoNotificacion.Realizado);
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                ProductoXCW m_Producto = new ProductoXCW();
                m_Producto.ID = this.txtIDProducto.Text;
                IBLGeneral m_operacion = new BLGeneral();
                m_operacion.EliminarProducto(m_Producto);
                this.LimpiarCampos();
                (this.Master as VistaMaestraBase).MostrarNotificacion("Se eliminó el producto correctamente", VistaMaestraBase.TipoNotificacion.Realizado);
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }


        private void ListaProductos1_SeSeleccionoProducto(object sender, Generico<List<ProductoXCW>> e)
        {
            try
            {
                foreach (ProductoXCW m_Producto in e.Argumento)
                {
                    this.txtIDProducto.Text = m_Producto.ID;
                    this.txtCliente.Text = m_Producto.Cliente;
                    this.txtProducto.Text = m_Producto.Producto;
                    this.txtCardName.Text = m_Producto.CardName;
                    this.btnEliminar.Visible = true;
                    this.btnModificar.Visible = true;
                    break;
                }
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }

        /// <summary>
        /// Carga grilla de configuraciones
        /// </summary>
        private void CargarGrilla(List<ProductoXCW> productos)
        {
            try
            {
                this.ListaProductos1.CargarConfiguraciones(productos);
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }

        private void LimpiarCampos()
        {
            IBLGeneral m_operacion = new BLGeneral();
            this.txtIDProducto.Text = "0";
            this.txtCliente.Text = String.Empty;
            this.txtProducto.Text = String.Empty;
            this.txtCardName.Text = String.Empty;
            this.btnEliminar.Visible = false;
            this.btnModificar.Visible = false;
            this.CargarGrilla(m_operacion.ObtenerProductos());
        }
    }
}