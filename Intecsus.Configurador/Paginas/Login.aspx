﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Master.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Intecsus.Configurador.Paginas.Login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <asp:UpdatePanel ID="upnLogin" runat="server">
        <ContentTemplate>
            <div align="center">
                <div class="grilla960_12_5">
                    <div class="caja cajaOculta">
                        <div class="cajaTitulo">
                            Ingreso al sistema
                        </div>
                        <div class="cajaContenido">
                            <table width="100%">
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblUsuario" runat="server" Text="Usuario:"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtUsuario" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvLogin1" runat="server" ControlToValidate="txtUsuario"
                                            ErrorMessage="* Obligatorio" ValidationGroup="vgLogin"></asp:RequiredFieldValidator>
                                      <%--  <asp:RegularExpressionValidator ID="revLogin" runat="server" ErrorMessage="! Solo Nùmeros"
                                            SetFocusOnError="true" ControlToValidate="txtUsuario" Display="Dynamic" ValidationExpression="[0123456789]*"
                                            ValidationGroup="vgLogin"></asp:RegularExpressionValidator>--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                        <asp:Label ID="lblPassword" runat="server" Text="Contraseña:"></asp:Label>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" MaxLength="20"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvLogin2" runat="server" ControlToValidate="txtPassword"
                                            ErrorMessage="* Obligatorio" ValidationGroup="vgLogin"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnIngresar" runat="server" Text="Iniciar" OnClick="btnIngresar_Click"
                                            ValidationGroup="vgLogin" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
