﻿using Intecsus.Entidades;
using Intecsus.NegocioGeneral;
using Intecsus.Seguridad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Intecsus.Configurador.Paginas
{
    public partial class Configurador: System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try {
                this.ListaConfiguracion1.SeSeleccionoConfiguracion += new EventHandler<Generico<List<Configurar>>>(this.ListaConfiguracion1_SeSeelecionoConfiguracion);
                IBLGeneral m_operacion = new BLGeneral();
                this.CargarGrilla(m_operacion.ObtenerConfiguracion());
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }



        protected void btnInsert_Click(object sender, EventArgs e)
        {
            try {
                IBLGeneral m_operacion = new BLGeneral();
                Configurar m_Dato = new Configurar();
                Validadores m_valida = new Validadores();
                string m_cadena = m_operacion.ObtenerValoresConfigurador(this.txtValidacion.Text);
                if (!m_valida.ValidadorExpresionesRegulares(this.txtValor.Text, m_cadena))
                {
                    throw new Exception("Error a procesar el dato por favor validar el tipo");
                }

                m_Dato.IDValor = this.txtIdValor.Text;
                m_Dato.Valor = this.txtValor.Text;
                m_Dato.TipoDato = this.txtValidacion.Text;
                m_operacion.IngresarConfiguracion(m_Dato);
                this.CargarGrilla(m_operacion.ObtenerConfiguracion());
                this.LimpiarDatos();
                (this.Master as VistaMaestraBase).MostrarNotificacion("Se ingreso correctamente", VistaMaestraBase.TipoNotificacion.Realizado);
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            try {
                
                IBLGeneral m_operacion = new BLGeneral();
                Configurar m_Dato = new Configurar();
                Validadores m_valida = new Validadores();
                string m_cadena =  m_operacion.ObtenerValoresConfigurador(this.txtValidacion.Text);
                if (!m_valida.ValidadorExpresionesRegulares(this.txtValor.Text, m_cadena))
                {
                    throw new Exception("Error a procesar el dato por favor validar el tipo");
                }
                m_Dato.IDValor = this.txtIdValor.Text;
                m_Dato.Valor = this.txtValor.Text;
                m_Dato.TipoDato = this.txtValidacion.Text;
                m_operacion.ActualizarConfiguracion(m_Dato);
                this.CargarGrilla(m_operacion.ObtenerConfiguracion());
                this.LimpiarDatos();
                (this.Master as VistaMaestraBase).MostrarNotificacion("Se modifico correctamente", VistaMaestraBase.TipoNotificacion.Realizado);
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }

        private void LimpiarDatos()
        {
            this.txtIdValor.Text = string.Empty;
            this.txtValor.Text = string.Empty;
            this.txtValidacion.Text = string.Empty;
            this.txtIdValor.Enabled = true;
            this.btnInsert.Visible = true;
        }

        protected void ListaConfiguracion1_SeSeelecionoConfiguracion(object sender, Generico<List<Configurar>> e) 
        {
            try
            {
                this.txtIdValor.Enabled = false;
                foreach (Configurar m_dato in e.Argumento) 
                {
                    this.txtIdValor.Text = m_dato.IDValor;
                    this.txtValor.Text = m_dato.Valor;
                    this.txtValidacion.Text = m_dato.TipoDato;
                    this.btnInsert.Visible = false;
                    break;
                }
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }

        /// <summary>
        /// Carga grilla de configuraciones
        /// </summary>
        private void CargarGrilla(List<Configurar> configurar)
        {
            try
            {
                this.ListaConfiguracion1.CargarConfiguraciones(configurar);
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }
    }
}