﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Principal.Master" AutoEventWireup="true" CodeBehind="Logs.aspx.cs" Inherits="Intecsus.Configurador.Paginas.Logs" %>

<%@ Register src="../UserControls/ListaLogs.ascx" tagname="ListaLogs" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <script>
          function Calendario () {
              $("#ContentPlaceHolder1_txtFechaInicio").datepicker();
              $("#ContentPlaceHolder1_txtFechaFin").datepicker();
          }
  </script>
    <asp:UpdatePanel ID="upnContenido" runat="server">
        <ContentTemplate>
            <div class="grilla960_12_6 flotanteIzq">
                <div class="caja cajaOculta">
                    <div class="cajaTitulo">
                        Filtrar
                    </div>
                    <div class="cajaContenido">
                        La información mostrada son de los últimos cien registros almacenados en la tabla de logs para tener más información se puede filtrar 
                        por la transacción especifica o dar click en siguiente.
                        <asp:UpdatePanel ID="upnFormulario" runat="server">
                            <ContentTemplate>
                                <table width="100%">
                                     <tr>
                                        <td align="left">
                                            <asp:Label ID="lblIDTransaccion" runat="server" Text="Id Transacción:"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTransaccion" runat="server" Text="0"></asp:TextBox>
                                              <asp:RegularExpressionValidator ID="revTelC" runat="server" ErrorMessage="! Solo Nùmeros"
                                                SetFocusOnError="true" ControlToValidate="txtTransaccion" Display="Dynamic" ValidationExpression="[0123456789]*"
                                                ValidationGroup="vgConfig"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <asp:Label ID="lblTransaccionCliente" runat="server" Text=" Cliente:"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtTranCliente" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvtxtTranCliente" runat="server" ControlToValidate="txtTranCliente"
                                                ErrorMessage="* Obligatorio" ValidationGroup="vgConfig"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                         <td align="left">
                                            <asp:Label ID="lblFechaIni" runat="server" Text="Fecha Inicio:"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtFechaInicio" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                         <td align="left">
                                            <asp:Label ID="lblFechaFin" runat="server" Text="Fecha Fin:"></asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="txtFechaFin" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  align="center" colspan="2">
                                            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" OnClientClick="return ValidarFechas('#ContentPlaceHolder1_txtFechaInicio','#ContentPlaceHolder1_txtFechaFin'); " />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  align="center">
                                            <asp:Button ID="btnAnterior" runat="server" Text="Anterior" OnClick="btnAnterior_Click" />
                                        </td>
                                        <td  align="center">
                                            <asp:Button ID="btnSiguiente" runat="server" Text="Siguiente" OnClick="btnSiguiente_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
            <div class=" separacion limpiarFlotante "></div>
            <asp:Panel ID="pnlGrillaTiendas" runat="server">
                <div class="grilla960_12_9 flotanteIzq">
                    <div class="caja cajaOculta">
                        <div class="cajaTitulo">
                            Lista LOG
                        </div>
                        <div class="cajaContenido">
                            <uc1:ListaLogs ID="ListaLogs1" runat="server" />
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
