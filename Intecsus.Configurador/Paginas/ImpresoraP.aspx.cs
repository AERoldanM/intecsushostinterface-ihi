﻿using Intecsus.Entidades;
using Intecsus.NegocioGeneral;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Intecsus.Configurador.Paginas
{
    public partial class ImpresoraP: System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.ListaImpresoras1.SeSeleccionoImpresora += new EventHandler<Generico<List<Impresora>>>(this.ListaConfiguracion1_SeSeleccionoImpresora);
                IBLGeneral m_operacion = new BLGeneral();
                this.CargarGrilla(m_operacion.ObtenerImpresoras());
                if (!IsPostBack)
                {
                    this.btnEliminar.Visible = false;
                    this.btnModificar.Visible = false;
                }
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }

        }


        protected void btnInsertar_Click(object sender, EventArgs e)
        {
            try
            {
                Impresora m_Impresora = new Impresora();
                m_Impresora.NombreRecibido = this.txtNombrerecibido.Text;
                m_Impresora.PCNAME = this.txtPCNAME.Text;
                m_Impresora.IP = this.txtIP.Text;
                IBLGeneral m_operacion = new BLGeneral();
                m_operacion.InsertarImpresora(m_Impresora);
                (this.Master as VistaMaestraBase).MostrarNotificacion("Se inserto la impresora correctamente", VistaMaestraBase.TipoNotificacion.Realizado);
                this.LimpiarCampos();
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }



        protected void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {


                Impresora m_Impresora = new Impresora();
                m_Impresora.IdImpresora = Convert.ToInt32(this.txtIDImpresora.Text);
                m_Impresora.NombreRecibido = this.txtNombrerecibido.Text;
                m_Impresora.PCNAME = this.txtPCNAME.Text;
                m_Impresora.IP = this.txtIP.Text;
                IBLGeneral m_operacion = new BLGeneral();
                m_operacion.ActualizarImpresora(m_Impresora);
                this.CargarGrilla(m_operacion.ObtenerImpresoras());
                (this.Master as VistaMaestraBase).MostrarNotificacion("Se actualizó la impresora correctamente", VistaMaestraBase.TipoNotificacion.Realizado);
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                Impresora m_Impresora = new Impresora();
                m_Impresora.IdImpresora = Convert.ToInt32(this.txtIDImpresora.Text);
                IBLGeneral m_operacion = new BLGeneral();
                m_operacion.EliminarImpresora(m_Impresora);
                this.LimpiarCampos();
                (this.Master as VistaMaestraBase).MostrarNotificacion("Se eliminó la impresora correctamente", VistaMaestraBase.TipoNotificacion.Realizado);
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }

        private void ListaConfiguracion1_SeSeleccionoImpresora(object sender, Generico<List<Impresora>> e)
        {
            try
            {
                foreach (Impresora m_dato in e.Argumento)
                {
                    this.txtIDImpresora.Text = m_dato.IdImpresora.ToString();
                    this.txtNombrerecibido.Text = m_dato.NombreRecibido;
                    this.txtPCNAME.Text = m_dato.PCNAME;
                    this.txtIP.Text = m_dato.IP;
                    this.btnEliminar.Visible = true;
                    this.btnModificar.Visible = true;
                    break;
                }
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }

        /// <summary>
        /// Carga grilla de configuraciones
        /// </summary>
        private void CargarGrilla(List<Impresora> impresora)
        {
            try
            {
                this.ListaImpresoras1.CargarConfiguraciones(impresora);
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }

        private void LimpiarCampos()
        {
            IBLGeneral m_operacion = new BLGeneral();
            this.txtIDImpresora.Text = "0";
            this.txtNombrerecibido.Text = String.Empty;
            this.txtPCNAME.Text = String.Empty;
            this.txtIP.Text = String.Empty;
            this.btnEliminar.Visible = false;
            this.btnModificar.Visible = false;
            this.CargarGrilla(m_operacion.ObtenerImpresoras());
        }
    }
}