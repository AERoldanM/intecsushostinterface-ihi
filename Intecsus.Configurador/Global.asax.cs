﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;

namespace Intecsus.Configurador
{
    public class Global: System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {

        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception ex = Server.GetLastError();
            if (ex is ThreadAbortException)
                return;
            //Server.Transfer("Inicio.aspx");
        }

        protected void Session_End(object sender, EventArgs e)
        {
            if (Session["Usuario"] != null)
            {

                Session.Abandon();
                Session.Clear();
                Session.RemoveAll();
            }
            Server.Transfer("Login.aspx");

        }

        protected void Application_End(object sender, EventArgs e)
        {

        }

    }
}