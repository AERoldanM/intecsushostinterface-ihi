﻿using Intecsus.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Intecsus.Configurador.UserControls
{
    public partial class ListaLogs: System.Web.UI.UserControl
    {
        /// <summary>
        /// Establece la página maestra para ser utilizada desde el control de Cliente
        /// </summary>
        public VistaMaestraBase Maestra { private get; set; }

        #region Metodo públicos

        /// <summary>
        /// Método que vuelve a cargar los logs en la grilla
        /// </summary>
        public void CargarConfiguraciones(List<LOG> logs)
        {
            this.CargarGrilla(logs);
        }

        #endregion

        #region Manejadores de evento

        /// <summary>
        /// Manejador de evento
        /// </summary>
        /// <param name="sender">Evento que lo lanzó</param>
        /// <param name="e">Argumentos del evento</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.HerramientasConfiguracion.GridView = this.GridView;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.HerramientasConfiguracion.MostrarSeleccionarCheck = false;
        }

        #endregion

        #region Metodos Privados

        /// <summary>
        /// Metodo encargado de volver a cargar la grilla
        /// </summary>
        private void CargarGrilla(List<LOG> logs)
        {
            this.GridView.DataSource = logs;
            this.GridView.DataBind();
        }

        #endregion
    }
}