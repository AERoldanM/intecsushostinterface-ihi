﻿using Intecsus.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Intecsus.Configurador.UserControls
{
    public partial class ListaImpresoras: System.Web.UI.UserControl
    {
        #region Eventos

        public event EventHandler<Generico<List<Impresora>>> SeSeleccionoImpresora;

        #endregion
        /// <summary>
        /// Establece la página maestra para ser utilizada desde el control de Cliente
        /// </summary>
        public VistaMaestraBase Maestra { private get; set; }

        #region Metodo públicos

        /// <summary>
        /// Método que vuelve a cargar los Clientes en la grilla
        /// </summary>
        public void CargarConfiguraciones(List<Impresora> Configuraciones)
        {
            this.CargarGrilla(Configuraciones);
        }

        #endregion

        #region Manejadores de evento

        /// <summary>
        /// Manejador de evento
        /// </summary>
        /// <param name="sender">Evento que lo lanzó</param>
        /// <param name="e">Argumentos del evento</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.HerramientasConfiguracion.GridView = this.GridView;
        }


        /// <summary>
        /// Manejador de evento
        /// </summary>
        /// <param name="sender">Evento que lo lanzó</param>
        /// <param name="e">Argumentos del evento</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.HerramientasConfiguracion.MostrarSeleccionarCheck = false;
        }

        /// Manejador de evento
        /// </summary>
        /// <param name="sender">Evento que lo lanzó</param>
        /// <param name="e">Argumentos del evento</param>
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.Cells.Count > 1)
            {

            }
        }

        /// <summary>
        /// Manejador de evento
        /// </summary>
        /// <param name="sender">Evento que lo lanzó</param>
        /// <param name="e">Argumentos del evento</param>
        protected void GridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<Impresora> m_Impresora = new List<Impresora>();
            m_Impresora.Add(this.ObtenerRedGridViewRow(this.GridView.SelectedRow));

            if (this.SeSeleccionoImpresora != null)
            {
                this.SeSeleccionoImpresora(this, new Generico<List<Impresora>> { Argumento = m_Impresora });
            }
        }

        #endregion

        #region Metodos Privados

        /// <summary>
        /// Metodo encargado de volver a cargar la grilla
        /// </summary>
        private void CargarGrilla(List<Impresora> Impresoras)
        {
            this.GridView.DataSource = Impresoras;
            this.GridView.DataBind();
        }

        /// <summary>
        /// Método que retorna un objeto Red partir de una fila en el Grid
        /// </summary>
        /// <param name="fila">Fila correspondiente a la Selector</param>
        /// <returns>Selector de la fila</returns>
        private Impresora ObtenerRedGridViewRow(GridViewRow fila)
        {
            Impresora m_Impresora = new Impresora();
            m_Impresora.IdImpresora = Convert.ToInt32(fila.Cells[1].Text);
            m_Impresora.NombreRecibido  = fila.Cells[2].Text;
            m_Impresora.PCNAME  = fila.Cells[3].Text;
            m_Impresora.IP = fila.Cells[4].Text;
            return m_Impresora;
        }

        #endregion
    }
}