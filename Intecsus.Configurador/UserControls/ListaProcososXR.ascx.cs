﻿using Intecsus.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Intecsus.Configurador.UserControls
{
    public partial class ListaProcososXR: System.Web.UI.UserControl
    {
        #region Eventos

        public event EventHandler<Generico<List<ProcesoXAP>>> SeSeleccionoProcesoXAP;

        #endregion
        /// <summary>
        /// Establece la página maestra para ser utilizada desde el control de Cliente
        /// </summary>
        public VistaMaestraBase Maestra { private get; set; }

        #region Metodo públicos

        /// <summary>
        /// Método que vuelve a cargar los datos en la grilla
        /// </summary>
        public void CargarConfiguraciones(List<ProcesoXAP> procesos)
        {
            this.CargarGrilla(procesos);
        }

        #endregion

        #region Manejadores de evento

        /// <summary>
        /// Manejador de evento
        /// </summary>
        /// <param name="sender">Evento que lo lanzó</param>
        /// <param name="e">Argumentos del evento</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.HerramientasConfiguracion.GridView = this.GridView;
        }

        /// <summary>
        /// Manejador de evento
        /// </summary>
        /// <param name="sender">Evento que lo lanzó</param>
        /// <param name="e">Argumentos del evento</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.HerramientasConfiguracion.MostrarSeleccionarCheck = false;
        }

        /// <summary>
        /// Manejador de evento
        /// </summary>
        /// <param name="sender">Evento que lo lanzó</param>
        /// <param name="e">Argumentos del evento</param>
        protected void GridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<ProcesoXAP> m_ProcesoXAprobar = new List<ProcesoXAP>();
            m_ProcesoXAprobar.Add(this.ObtenerRedGridViewRow(this.GridView.SelectedRow));
            //foreach (GridViewRow fila in this.GridView.Rows)
            //{
            //    CheckBox chk = (CheckBox)fila.FindControl("chkSeleccion");

            //    if (chk != null)
            //    {
            //        chk.Checked = fila == GridView.SelectedRow ? true: false;
            //    }
            //}

            if (this.SeSeleccionoProcesoXAP != null)
            {
                this.SeSeleccionoProcesoXAP(this, new Generico<List<ProcesoXAP>> { Argumento = m_ProcesoXAprobar });
            }
        }

        #endregion

        #region Metodos Privados

        /// <summary>
        /// Metodo encargado de volver a cargar la grilla
        /// </summary>
        private void CargarGrilla(List<ProcesoXAP> Configuraciones)
        {
            this.GridView.DataSource = Configuraciones;
            this.GridView.DataBind();
        }

        /// <summary>
        /// Método que retorna un objeto Red partir de una fila en el Grid
        /// </summary>
        /// <param name="fila">Fila correspondiente a la Selector</param>
        /// <returns>Selector de la fila</returns>
        private ProcesoXAP ObtenerRedGridViewRow(GridViewRow fila)
        {
            ProcesoXAP m_Proceso = new ProcesoXAP();
            m_Proceso.ID  = Convert.ToInt32(fila.Cells[1].Text);
            m_Proceso.IDProceso = Convert.ToInt32(fila.Cells[2].Text);
            m_Proceso.Fecha = fila.Cells[3].Text;
            m_Proceso.Informacion = fila.Cells[4].Text;
            m_Proceso.Intentos = Convert.ToInt32(fila.Cells[5].Text);
            m_Proceso.IDTransaccion  = Convert.ToInt32(fila.Cells[6].Text);
            return m_Proceso;
        }

        #endregion
    }
}