﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListaTransacciones.ascx.cs" Inherits="Intecsus.Configurador.UserControls.ListaTransacciones" %>
<%@ Register Src="Herramientas.ascx" TagName="Herramientas" TagPrefix="uc1" %>
<div>
    <uc1:Herramientas ID="HerramientasConfiguracion" runat="server" />
    <asp:GridView ID="GridView" CssClass="tabla1 tableSorter" runat="server" AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField DataField="IDTransaccion" HeaderText="Transacción" HeaderStyle-Width="10%" />
            <asp:BoundField DataField="Cliente" HeaderText="Cliente" HeaderStyle-Width="15%" />
            <asp:BoundField DataField="IdTransaccionCl" HeaderText="Transaccion Cliente" HeaderStyle-Width="15%" />
            <asp:BoundField DataField="Estado" HeaderText="Estado" HeaderStyle-Width="60%" />
        </Columns>
        <EmptyDataTemplate>
            <asp:Label ID="lblNoDatos" runat="server" Text="No hay datos"></asp:Label>
        </EmptyDataTemplate>
    </asp:GridView>
</div>