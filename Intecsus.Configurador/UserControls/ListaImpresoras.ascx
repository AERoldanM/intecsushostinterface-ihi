﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListaImpresoras.ascx.cs" Inherits="Intecsus.Configurador.UserControls.ListaImpresoras" %>
<%@ Register Src="Herramientas.ascx" TagName="Herramientas" TagPrefix="uc1" %>
<div>
    <uc1:Herramientas ID="HerramientasConfiguracion" runat="server" />
    <asp:GridView ID="GridView" CssClass="tabla1 tableSorter" runat="server" OnSelectedIndexChanged="GridView_SelectedIndexChanged"
        OnRowDataBound="GridView_RowDataBound" AutoGenerateColumns="False">
        <Columns>
            <asp:CommandField ShowSelectButton="True" ButtonType="Image" HeaderStyle-Width="5%"
                SelectImageUrl="~/img/iconoHecho15x15.png" />
            <asp:BoundField DataField="IDImpresora" HeaderText="Id " HeaderStyle-Width="10%" />
            <asp:BoundField DataField="NombreEnviado" HeaderText="Nombre" HeaderStyle-Width="20%" />
            <asp:BoundField DataField="PCNAME" HeaderText="PCNAME" HeaderStyle-Width="20%" />
            <asp:BoundField DataField="IP" HeaderText="IP" HeaderStyle-Width="20%" />
        </Columns>
        <EmptyDataTemplate>
            <asp:Label ID="lblNoDatos" runat="server" Text="No hay datos"></asp:Label>
        </EmptyDataTemplate>
    </asp:GridView>
</div>