﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListaConfiguracion.ascx.cs" Inherits="Intecsus.Configurador.UserControls.ListaConfiguracion" %>
<%@ Register Src="Herramientas.ascx" TagName="Herramientas" TagPrefix="uc1" %>
<div>
    <uc1:Herramientas ID="HerramientasConfiguracion" runat="server" />
    <asp:GridView ID="GridView" CssClass="tabla1 tableSorter" runat="server" OnSelectedIndexChanged="GridView_SelectedIndexChanged"
        OnRowDataBound="GridView_RowDataBound" AutoGenerateColumns="False">
        <Columns>
            <asp:CommandField ShowSelectButton="True" ButtonType="Image" HeaderStyle-Width="5%"
                SelectImageUrl="~/img/iconoHecho15x15.png" />
            <asp:BoundField DataField="IDValor" HeaderText="Id Valor" HeaderStyle-Width="30%" />
            <asp:BoundField DataField="Valor" HeaderText="Valor" HeaderStyle-Width="60%" />
             <asp:BoundField DataField="TipoDato" HeaderText="Validación" HeaderStyle-Width="60%" />
        </Columns>
        <EmptyDataTemplate>
            <asp:Label ID="lblNoDatos" runat="server" Text="No hay datos"></asp:Label>
        </EmptyDataTemplate>
    </asp:GridView>
</div>
