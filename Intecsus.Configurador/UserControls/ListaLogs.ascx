﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListaLogs.ascx.cs" Inherits="Intecsus.Configurador.UserControls.ListaLogs" %>
<%@ Register Src="Herramientas.ascx" TagName="Herramientas" TagPrefix="uc1" %>
<div>
    <uc1:Herramientas ID="HerramientasConfiguracion" runat="server" />
    <asp:GridView ID="GridView" CssClass="tabla1 tableSorter" runat="server" AutoGenerateColumns="False">
        <Columns>
            <asp:BoundField DataField="IdLog" HeaderText="Id " HeaderStyle-Width="5%" />
            <asp:BoundField DataField="IdProceso" HeaderText="IdProceso" HeaderStyle-Width="5%" />
            <asp:BoundField DataField="Fecha" HeaderText="Fecha" HeaderStyle-Width="20%" />
            <asp:BoundField DataField="IDTransaccion" HeaderText="IDTransaccion" HeaderStyle-Width="5%" />
            <asp:BoundField DataField="Evento" HeaderText="Evento" HeaderStyle-Width="5%" />
            <asp:BoundField DataField="DescripcionEvento" HeaderText="Descripción" HeaderStyle-Width="60%" />
        </Columns>
        <EmptyDataTemplate>
            <asp:Label ID="lblNoDatos" runat="server" Text="No hay datos"></asp:Label>
        </EmptyDataTemplate>
    </asp:GridView>
</div>