﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListaProcososXR.ascx.cs" Inherits="Intecsus.Configurador.UserControls.ListaProcososXR" %>
<%@ Register Src="Herramientas.ascx" TagName="Herramientas" TagPrefix="uc1" %>
<div>
    <uc1:Herramientas ID="HerramientasConfiguracion" runat="server" />
    <asp:GridView ID="GridView" CssClass="tabla1 tableSorter" runat="server" OnSelectedIndexChanged="GridView_SelectedIndexChanged"
        AutoGenerateColumns="False">
        <Columns>
            <asp:CommandField ShowSelectButton="True" ButtonType="Image" HeaderStyle-Width="5%"
                SelectImageUrl="~/img/iconoHecho15x15.png" />
            <asp:BoundField DataField="ID" HeaderText="Id" HeaderStyle-Width="7%" />
            <asp:BoundField DataField="IdProceso" HeaderText="IdProceso" HeaderStyle-Width="7%" />
            <asp:BoundField DataField="Fecha" HeaderText="Fecha" HeaderStyle-Width="12%" />
            <asp:BoundField DataField="Informacion" HeaderText="Información" HeaderStyle-Width="20%" />
            <asp:BoundField DataField="Intentos" HeaderText="Intentos" HeaderStyle-Width="8%" />
             <asp:BoundField DataField="IDTransaccion" HeaderText="Transacción" HeaderStyle-Width="5%" />
        </Columns>
        <EmptyDataTemplate>
            <asp:Label ID="lblNoDatos" runat="server" Text="No hay datos"></asp:Label>
        </EmptyDataTemplate>
    </asp:GridView>
</div>
