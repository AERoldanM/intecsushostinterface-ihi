﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ListaProductos.ascx.cs" Inherits="Intecsus.Configurador.UserControls.ListaProductos" %>
<%@ Register Src="Herramientas.ascx" TagName="Herramientas" TagPrefix="uc1" %>
<div>
    <uc1:Herramientas ID="HerramientasConfiguracion" runat="server" />
    <asp:GridView ID="GridView" CssClass="tabla1 tableSorter" runat="server" OnSelectedIndexChanged="GridView_SelectedIndexChanged"
         AutoGenerateColumns="False">
        <Columns>
            <asp:CommandField ShowSelectButton="True" ButtonType="Image" HeaderStyle-Width="5%"
                SelectImageUrl="~/img/iconoHecho15x15.png" />
            <asp:BoundField DataField="ID" HeaderText="Id " HeaderStyle-Width="10%" />
            <asp:BoundField DataField="Cliente" HeaderText="Cliente" HeaderStyle-Width="20%" />
            <asp:BoundField DataField="Producto" HeaderText="Producto" HeaderStyle-Width="20%" />
            <asp:BoundField DataField="CardName" HeaderText="Card Name" HeaderStyle-Width="20%" />
        </Columns>
        <EmptyDataTemplate>
            <asp:Label ID="lblNoDatos" runat="server" Text="No hay datos"></asp:Label>
        </EmptyDataTemplate>
    </asp:GridView>
</div>