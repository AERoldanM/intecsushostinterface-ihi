﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Herramientas.ascx.cs" Inherits="Intecsus.Configurador.UserControls.Herramientas" %>
<div class="tablaHerramientas">
    <div class="flotanteIzq tablaTotal" runat="server" id="divTotal">
        <asp:Label ID="lbltotalTitulo" runat="server" Text="Total" />:&nbsp;
        <asp:Label ID="lbltotalFilas" runat="server" Text="" />
    </div>
    <div class="tablaPaginador flotanteIzq" runat="server" id="divPaginador">
        <asp:Image ID="imgPrimero" ToolTip="Inicio" runat="server" ImageUrl="~/img/first.png"
            CssClass="first" />
        <asp:Image ID="imgAnterior" ToolTip="Anterior" runat="server" ImageUrl="~/img/prev.png"
            CssClass="prev" />
        <input type="text" class="pagedisplay" onkeypress="return event.keyCode!=13" />
        <asp:Image ID="imgSiguiente" ToolTip="Siguiente" runat="server" ImageUrl="~/img/next.png"
            CssClass="next" />
        <asp:Image ID="imgUltimo" ToolTip="Último" runat="server" ImageUrl="~/img/last.png"
            CssClass="last" />
        <select class="pagesize">
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="30">30</option>
            <option value="40">40</option>
            <option value="50">50</option>
        </select>
    </div>
    <div id="divBuscar" runat="server" class="tablaFiltro flotanteIzq">
        <asp:TextBox ID="txtTextoABuscar" runat="server" onkeypress="return event.keyCode!=13"></asp:TextBox>
        <asp:Image ID="imgLimpiarBusqueda" ToolTip="Limpiar búsqueda" runat="server" ImageUrl="~/img/borrar_15x15.png" />
    </div>
    <div class="tablaSeleccion flotanteIzq" runat="server" id="divSeleccionCheck">
        <a href="javascript:void(0);" onclick="Utilidades.Tablas.SeleccionarTodo(this)" title="Seleccionar todo">
        </a><a href="javascript:void(0);" onclick="javascript:Utilidades.Tablas.DesSeleccionarTodo(this);"
            title="Deseleccionar todo"></a><a href="javascript:void(0);" onclick="javascript:Utilidades.Tablas.InvertirSeleccion(this);"
                title="Invertir Selección"></a>
    </div>
    <div class="limpiarFlotante">
    </div>
</div>