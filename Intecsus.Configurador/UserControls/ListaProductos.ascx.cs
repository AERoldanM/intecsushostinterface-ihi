﻿using Intecsus.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Intecsus.Configurador.UserControls
{
    public partial class ListaProductos: System.Web.UI.UserControl
    {
        #region Eventos

        public event EventHandler<Generico<List<ProductoXCW>>> SeSeleccionoProducto;

        #endregion

        #region Metodo públicos

        /// <summary>
        /// Método que vuelve a cargar los Clientes en la grilla
        /// </summary>
        public void CargarConfiguraciones(List<ProductoXCW> productos)
        {
            this.CargarGrilla(productos);
        }

        #endregion

        /// <summary>
        /// Establece la página maestra para ser utilizada desde el control de Cliente
        /// </summary>
        public VistaMaestraBase Maestra { private get; set; }

        #region Manejadores de evento

        /// <summary>
        /// Manejador de evento
        /// </summary>
        /// <param name="sender">Evento que lo lanzó</param>
        /// <param name="e">Argumentos del evento</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.HerramientasConfiguracion.GridView = this.GridView;
        }


        /// <summary>
        /// Manejador de evento
        /// </summary>
        /// <param name="sender">Evento que lo lanzó</param>
        /// <param name="e">Argumentos del evento</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.HerramientasConfiguracion.MostrarSeleccionarCheck = false;
        }

        /// <summary>
        /// Manejador de evento
        /// </summary>
        /// <param name="sender">Evento que lo lanzó</param>
        /// <param name="e">Argumentos del evento</param>
        protected void GridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<ProductoXCW> m_producto = new List<ProductoXCW>();
            m_producto.Add(this.ObtenerRedGridViewRow(this.GridView.SelectedRow));

            if (this.SeSeleccionoProducto != null)
            {
                this.SeSeleccionoProducto(this, new Generico<List<ProductoXCW>> { Argumento = m_producto });
            }
        }

        #endregion

        #region Metodos Privados

        /// <summary>
        /// Metodo encargado de volver a cargar la grilla
        /// </summary>
        private void CargarGrilla(List<ProductoXCW> Productos)
        {
            this.GridView.DataSource = Productos;
            this.GridView.DataBind();
        }

        /// <summary>
        /// Método que retorna un objeto Red partir de una fila en el Grid
        /// </summary>
        /// <param name="fila">Fila correspondiente a la Selector</param>
        /// <returns>Selector de la fila</returns>
        private ProductoXCW ObtenerRedGridViewRow(GridViewRow fila)
        {
            ProductoXCW m_Producto = new ProductoXCW();
            m_Producto.ID  = fila.Cells[1].Text;
            m_Producto.Cliente = fila.Cells[2].Text;
            m_Producto.Producto = fila.Cells[3].Text;
            m_Producto.CardName = fila.Cells[4].Text;
            return m_Producto;
        }

        #endregion
    }
}