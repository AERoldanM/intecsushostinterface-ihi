﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Intecsus.Configurador.UserControls
{
    public partial class Herramientas: System.Web.UI.UserControl
    {
        /// <summary>
        /// Obtiene o establece si se muestra la caja de filtrar por texto
        /// </summary>
        public bool MostrarFiltro
        {
            get
            {
                return this.divBuscar.Visible;
            }

            set
            {
                this.divBuscar.Visible = value;
            }
        }

        /// <summary>
        /// Obtiene o establece si se muestra la caja de paginador
        /// </summary>
        public bool MostrarPaginador
        {
            get
            {
                return this.divPaginador.Visible;
            }

            set
            {
                this.divPaginador.Visible = value;
            }
        }

        /// <summary>
        /// Obtiene o establece si se muestra la caja con las utilidades de selección de checkBoxes de la grilla
        /// </summary>
        public bool MostrarSeleccionarCheck
        {
            get
            {
                return this.divSeleccionCheck.Visible;
            }

            set
            {
                this.divSeleccionCheck.Visible = value;
            }
        }

        /// <summary>
        /// Establece el número de filas a mostrar por defecto
        /// </summary>
        public int NumeroFilas
        {
            set
            {
                this.divPaginador.Attributes.Add("tableSorterSize", value.ToString());
            }
        }

        /// <summary>
        /// Obtiene o establece la grilla que hará uso de las herramientas
        /// </summary>
        public GridView GridView { get; set; }

        /// <summary>
        /// Manejador del evento de carga del control de usuario
        /// </summary>
        /// <param name="sender">Objeto que lo lanzó</param>
        /// <param name="e">Argumentos del evento</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.GridView != null)
            {
                this.GridView.Load += new EventHandler(this.GridView_Event);
                this.GridView.DataBound += new EventHandler(this.GridView_Event);
            }
        }

        /// <summary>
        /// Manejador del evento
        /// </summary>
        /// <param name="sender">Objeto que lo lanzó</param>
        /// <param name="e">Argumentos del evento</param>
        protected void GridView_Event(object sender, EventArgs e)
        {
            if (this.GridView.Rows.Count > 0)
            {
                this.Visible = true;

                this.GridView.UseAccessibleHeader = true;
                this.GridView.HeaderRow.TableSection = TableRowSection.TableHeader;
                this.GridView.FooterRow.TableSection = TableRowSection.TableFooter;

                this.lbltotalFilas.Text = this.GridView.Rows.Count.ToString();
            }
            else
            {
                this.Visible = false;
            }
        }
    }
}