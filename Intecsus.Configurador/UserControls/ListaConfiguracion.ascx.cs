﻿using Intecsus.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Intecsus.Configurador.UserControls
{
    public partial class ListaConfiguracion: System.Web.UI.UserControl
    {
        #region Eventos

        public event EventHandler<Generico<List<Configurar>>> SeSeleccionoConfiguracion;

        #endregion

        /// <summary>
        /// Establece la página maestra para ser utilizada desde el control de Cliente
        /// </summary>
        public VistaMaestraBase Maestra { private get; set; }

        #region Metodo públicos

        /// <summary>
        /// Método que vuelve a cargar los Clientes en la grilla
        /// </summary>
        public void CargarConfiguraciones(List<Configurar> Configuraciones)
        {
            this.CargarGrilla(Configuraciones);
        }

        #endregion

        #region Manejadores de evento

        /// <summary>
        /// Manejador de evento
        /// </summary>
        /// <param name="sender">Evento que lo lanzó</param>
        /// <param name="e">Argumentos del evento</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this.HerramientasConfiguracion.GridView = this.GridView;
        }

        /// <summary>
        /// Manejador de evento
        /// </summary>
        /// <param name="sender">Evento que lo lanzó</param>
        /// <param name="e">Argumentos del evento</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.HerramientasConfiguracion.MostrarSeleccionarCheck = false;
        }

        /// Manejador de evento
        /// </summary>
        /// <param name="sender">Evento que lo lanzó</param>
        /// <param name="e">Argumentos del evento</param>
        protected void GridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.Cells.Count > 1)
            {
                
            }
        }

        /// <summary>
        /// Manejador de evento
        /// </summary>
        /// <param name="sender">Evento que lo lanzó</param>
        /// <param name="e">Argumentos del evento</param>
        protected void GridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<Configurar> m_Configuracion = new List<Configurar>();
            m_Configuracion.Add(this.ObtenerRedGridViewRow(this.GridView.SelectedRow));
            //foreach (GridViewRow fila in this.GridView.Rows)
            //{
            //    CheckBox chk = (CheckBox)fila.FindControl("chkSeleccion");

            //    if (chk != null)
            //    {
            //        chk.Checked = fila == GridView.SelectedRow ? true: false;
            //    }
            //}

            if (this.SeSeleccionoConfiguracion != null)
            {
                this.SeSeleccionoConfiguracion(this, new Generico<List<Configurar>> { Argumento = m_Configuracion });
            }
        }

        #endregion

        #region Metodos Privados

        /// <summary>
        /// Metodo encargado de volver a cargar la grilla
        /// </summary>
        private void CargarGrilla(List<Configurar> Configuraciones)
        {
            this.GridView.DataSource = Configuraciones;
            this.GridView.DataBind();
        }

        /// <summary>
        /// Método que retorna un objeto Red partir de una fila en el Grid
        /// </summary>
        /// <param name="fila">Fila correspondiente a la Selector</param>
        /// <returns>Selector de la fila</returns>
        private Configurar ObtenerRedGridViewRow(GridViewRow fila)
        {
            Configurar m_Configuracion = new Configurar();
            m_Configuracion.IDValor  = fila.Cells[1].Text;
            m_Configuracion.Valor = fila.Cells[2].Text.Contains("&amp;") ?  fila.Cells[2].Text.Replace("&amp;", "&") : fila.Cells[2].Text;
            m_Configuracion.TipoDato = fila.Cells[3].Text == "&nbsp;" ? string.Empty : fila.Cells[3].Text;
            return m_Configuracion;
        }

        #endregion
    }
}