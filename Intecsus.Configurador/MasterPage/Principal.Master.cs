﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Intecsus.Configurador.MasterPage
{
    public partial class Principal: VistaMaestraBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["Usuario"] == null)
                {
                    string mensaje = "alert('Se ha vencido la sessión');";
                    string host = HttpContext.Current.Request.Url.Scheme + @"://" + HttpContext.Current.Request.Url.Authority;
                    this.LanzarJavascript(mensaje + "location.href='" + host + "/Paginas/Login.aspx';", "Redireccion");
                }
            }
            catch (Exception exc)
            {
                (this.Master as VistaMaestraBase).MostrarNotificacion(exc);
            }
        }

        /// <summary>       
        /// Manejador del evento
        /// </summary>
        /// <param name="sender">Objeto que lo solicitó</param>
        /// <param name="e">Argumentos del evento</param>
        protected void ScriptManagerMasterPage_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
        {
            this.MostrarNotificacion(e.Exception);
        }

        protected void MenuPrincipal_MenuItemClick(object sender, MenuEventArgs e)
        {

        }
    }
}