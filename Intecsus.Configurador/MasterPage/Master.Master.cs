﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Intecsus.Configurador.MasterPage
{
    public partial class Master: VistaMaestraBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>       
        /// Manejador del evento
        /// </summary>
        /// <param name="sender">Objeto que lo solicitó</param>
        /// <param name="e">Argumentos del evento</param>
        protected void ScriptManagerMasterPage_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
        {
            this.MostrarNotificacion(e.Exception);
        }
    }
}