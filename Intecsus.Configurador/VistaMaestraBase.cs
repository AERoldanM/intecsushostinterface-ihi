﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Intecsus.Configurador
{
    /// <summary>
    /// Las funciones basicas de las paginas maestras
    /// </summary>
    public class VistaMaestraBase: System.Web.UI.MasterPage
    {
        /// <summary>
        /// Tipo de notificación a mostrar
        /// </summary>
        public enum TipoNotificacion
        {
            /// <summary>
            /// Notificación de tipo error 
            /// </summary>
            Error = 3,

            /// <summary>
            /// Notificación de tipo información
            /// </summary>
            Informacion = 0,

            /// <summary>
            /// Notificación de tipo realizado
            /// </summary>
            Realizado = 2,

            /// <summary>
            /// Notificación de tipo advertencia
            /// </summary>
            Advertencia = 1
        }

        #region Notificaciones

        /// <summary>
        /// Método que muestra una notificación de excepción en la página al lado del cliente
        /// </summary>
        /// <param name="exc">Excepción lanzada</param>
        public void MostrarNotificacion(Exception exc)
        {
            this.MostrarNotificacion(exc, null, true);
        }

        /// <summary>
        /// Método que muestra una notificación de excepción en la página al lado del cliente
        /// </summary>
        /// <param name="exc">Excepción lanzada</param>
        /// <param name="mensajePrevio">Mensaje a mostrar previo al detalle de la excepción</param>
        public void MostrarNotificacion(Exception exc, string mensajePrevio)
        {
            this.MostrarNotificacion(exc, mensajePrevio, true);
        }

        /// <summary>
        /// Método que muestra una notificación de excepción en la página al lado del cliente
        /// </summary>
        /// <param name="exc">Excepción lanzada</param>
        /// <param name="mostrarInformacionExtra">Define si se muestra información detallada del error</param>
        public void MostrarNotificacion(Exception exc, bool mostrarInformacionExtra)
        {
            this.MostrarNotificacion(exc, null, mostrarInformacionExtra);
        }

        /// <summary>
        /// Método que muestra una notificación de excepción en la página al lado del cliente
        /// </summary>
        /// <param name="exc">Excepción lanzada</param>
        /// <param name="mensajePrevio">Mensaje a mostrar previo al detalle de la excepción</param>
        /// <param name="mostrarInformacionExtra">Define si se muestra información detallada del error</param>
        public void MostrarNotificacion(Exception exc, string mensajePrevio, bool mostrarInformacionExtra)
        {
            this.MostrarNotificacion(
                string.Format(
                "{3}<b>{0}:</b> {1}{2}",
                exc.GetType(),
                exc.Message,
                mostrarInformacionExtra ? "<hr />" + exc.StackTrace: string.Empty,
                string.IsNullOrEmpty(mensajePrevio) ? string.Empty: mensajePrevio + "<br />"),
             TipoNotificacion.Error,
             false,
             0);
        }

        /// <summary>
        /// Método que muestra una notificación en la página al lado del cliente
        /// </summary>
        /// <param name="mensaje">Mensaje a mostrar</param>
        public void MostrarNotificacion(string mensaje)
        {
            this.MostrarNotificacion(mensaje, TipoNotificacion.Informacion);
        }

        /// <summary>
        /// Método que muestra una notificación en la página al lado del cliente
        /// </summary>
        /// <param name="mensaje">Mensaje a mostrar</param>
        /// <param name="tipoNotificacion">Tipo de notificación</param>
        public void MostrarNotificacion(string mensaje, TipoNotificacion tipoNotificacion)
        {
            this.MostrarNotificacion(mensaje, tipoNotificacion, true, 10);
        }

        /// <summary>
        /// Método que muestra una notificación en la página al lado del cliente
        /// </summary>
        /// <param name="mensaje">Mensaje a mostrar</param>
        /// <param name="tipoNotificacion">Tipo de notificación</param>
        /// <param name="autoOcultarMensaje">Define si quiere que el mensaje se oculte automáticamente después de 10 segundos</param>
        public void MostrarNotificacion(string mensaje, TipoNotificacion tipoNotificacion, bool autoOcultarMensaje)
        {
            this.MostrarNotificacion(mensaje, tipoNotificacion, autoOcultarMensaje, 10);
        }

        /// <summary>
        /// Método que muestra una notificación en la página al lado del cliente
        /// </summary>
        /// <param name="mensaje">Mensaje a mostrar</param>
        /// <param name="tipoNotificacion">Tipo de notificación</param>
        /// <param name="tiempoAMostrar">Tiempo en segundos para ocultar el mensaje</param>
        public void MostrarNotificacion(string mensaje, TipoNotificacion tipoNotificacion, int tiempoAMostrar)
        {
            this.MostrarNotificacion(mensaje, tipoNotificacion, true, tiempoAMostrar);
        }

        /// <summary>
        /// Método que muestra una notificación en la página al lado del cliente
        /// </summary>
        /// <param name="mensaje">Mensaje a mostrar</param>
        /// <param name="tipoNotificacion">Tipo de notificación</param>
        /// <param name="autoOcultarMensaje">Define si quiere que el mensaje se oculte automáticamente después de 10 segundos</param>
        /// <param name="tiempoAMostrar">Tiempo en segundos para ocultar el mensaje, solo aplica si autoOcultarMensaje es true</param>
        private void MostrarNotificacion(string mensaje, TipoNotificacion tipoNotificacion, bool autoOcultarMensaje, int tiempoAMostrar)
        {
            mensaje = mensaje.Replace("\n", "<br />").Replace("\r", string.Empty).Replace("'", "\\'");

            string script = string.Format(
                "Notificacion.Mostrar('{0}','{1}',{2},{3});",
                mensaje,
                tipoNotificacion.ToString(),
                autoOcultarMensaje.ToString().ToLower(),
                tiempoAMostrar);
            this.LanzarJavascript(script, "notificacion");
        }
     

        #endregion

        #region Modales

        /// <summary>
        /// Método que muestra una ventana modal al que se le adjunta un control
        /// </summary>
        /// <param name="titulo">Título de la ventana</param>
        /// <param name="controlAMostrar">Control a mostrar dentro de la ventana</param>
        public void MostrarVentanaModal(string titulo, Control controlAMostrar)
        {
            this.MostrarVentanaModal(titulo, controlAMostrar, false);
        }

        /// <summary>
        /// Método que muestra una ventana modal al que se le adjunta un control
        /// </summary>
        /// <param name="titulo">Título de la ventana</param>
        /// <param name="mostrarBotonCerrar">Indica si se muestra el botón de cerrado en la ventana modal</param>
        /// <param name="controlAMostrar">Control a mostrar dentro de la ventana</param>
        public void MostrarVentanaModal(string titulo, bool mostrarBotonCerrar, Control controlAMostrar)
        {
            this.MostrarVentanaModal(titulo, controlAMostrar, false, 0, mostrarBotonCerrar);
        }

        /// <summary>
        /// Método que muestra una ventana modal al que se le adjunta un control
        /// </summary>
        /// <param name="titulo">Título de la ventana</param>
        /// <param name="controlAMostrar">Control a mostrar dentro de la ventana</param>
        /// <param name="esPostBack">Indica si se trata de un postback para que no se tenga que posicionar y cambiar tamaño a la modal</param>
        public void MostrarVentanaModal(string titulo, Control controlAMostrar, bool esPostBack)
        {
            this.MostrarVentanaModal(titulo, controlAMostrar, esPostBack, 200, true);
        }

        /// <summary>
        /// Método que muestra una ventana modal al que se le adjunta un control
        /// </summary>
        /// <param name="titulo">Título de la ventana</param>
        /// <param name="controlAMostrar">Control a mostrar dentro de la ventana</param>
        /// <param name="ancho">Ancho en pixeles del contenedor de la ventana modal</param>
        public void MostrarVentanaModal(string titulo, Control controlAMostrar, int ancho)
        {
            this.MostrarVentanaModal(titulo, controlAMostrar, false, ancho, true);
        }

        /// <summary>
        /// Método que muestra una ventana modal al que se le adjunta un control
        /// </summary>
        /// <param name="titulo">Título de la ventana</param>
        /// <param name="controlAMostrar">Control a mostrar dentro de la ventana</param>
        /// <param name="esPostBack">Indica si se trata de un postback para que no se tenga que posicionar y cambiar tamaño a la modal</param>
        /// <param name="ancho">Ancho en pixeles del contenedor de la ventana modal</param>
        public void MostrarVentanaModal(string titulo, Control controlAMostrar, bool esPostBack, int ancho)
        {
            this.MostrarVentanaModal(titulo, controlAMostrar, esPostBack, ancho, true);
        }

        /// <summary>
        /// Método que muestra una ventana modal al que se le adjunta un control
        /// </summary>
        /// <param name="titulo">Título de la ventana</param>
        /// <param name="controlAMostrar">Control a mostrar dentro de la ventana</param>
        /// <param name="esPostBack">Indica si se trata de un postback para que no se tenga que posicionar y cambiar tamaño a la modal</param>
        /// <param name="ancho">Ancho en pixeles del contenedor de la ventana modal</param>        
        /// <param name="mostrarBotonCerrar">Indica si se muestra el botón de cerrado en la ventana modal</param>
        public void MostrarVentanaModal(string titulo, Control controlAMostrar, bool esPostBack, int ancho, bool mostrarBotonCerrar)
        {
            string script = string.Format(
                                       "VentanaModal.Mostrar('{0}','{1}',{2},{3},false,{4});",
                                       titulo,
                                       controlAMostrar.ClientID,
                                       esPostBack.ToString().ToLower(),
                                       ancho,
                                       mostrarBotonCerrar.ToString().ToLower());

            controlAMostrar.Visible = true;

            this.LanzarJavascript(script, "ventanaModal");
        }

        /// <summary>
        /// Método que oculta la ventana modal
        /// </summary>
        public void OcultarVentanaModal()
        {
            this.LanzarJavascript("VentanaModal.Ocultar();", "ocultarVentanaModal");
        }

        #endregion

        /// <summary>
        /// Método que lanza un javascript en la página
        /// </summary>
        /// <param name="script">Script a lanzar</param>
        /// <param name="nombre">Nombre llave del script</param>
        public void LanzarJavascript(string script, string nombre)
        {
            ScriptManager sm = ScriptManager.GetCurrent(this.Page);

            // Se valida que se esté utilizando ajax
            if (sm != null)
            {
                // Se valida que sea un postback de ajax
                if (sm.IsInAsyncPostBack)
                {
                    ScriptManager.RegisterClientScriptBlock(
                        this.Page,
                        this.GetType(),
                        nombre + new Random().Next(100),
                        script,
                        true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(
                             this.Page,
                             this.GetType(),
                             "ventanaModal" + new Random().Next(100),
                             "$(document).ready(function () {" + script + "});",
                             true);
                }
            }
            else
            {
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), nombre, "$(document).ready(function () {" + script + "});");
            }
        }
    }
}