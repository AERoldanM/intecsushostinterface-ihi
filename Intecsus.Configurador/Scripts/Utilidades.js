﻿
function ValidarFechas(fechaIni, fechaFin) {
    if ($(fechaIni).val().length == 0 && $(fechaFin).val().length == 0)
    {
        return true;
    }

    var fechaI = $(fechaIni).val().split("/");
    var day = fechaI[1];
    var month = fechaI[0];
    var year = fechaI[2];
    var dateIni = new Date(year, month, day);

    var fechaf = $(fechaFin).val().split("/");
    day = fechaf[1];
    month = fechaf[0];
    year = fechaf[2];
    var dateFin = new Date(year, month, day);
    if (dateFin >= dateIni) {
        return true;
    }

    alert("Fecha inicial mayor que fecha final");
    return false;
}


$(document).ready(function () {
    Utilidades.ActivarUtilidadesCaja();
    Ajax.Iniciar();

    // Se controla evento de cambio de tamaño de la ventana
    $(window).resize(function () {

        $("#divNotificacion").width("960px");

        VentanaModal.AnimarPosicion();
    });

    // $("#menuDesplegable li").hide();
    // $("#divContenidoPrincipal").css("padding-top", $(".encabezadoFlotante").height() + 10 + "px");
    // $("#menuDesplegable li").show();

    Utilidades.Tablas.ActivarTableSorter();
    //Utilidades.OcultarLista();
    //ColorPicker.SetColorPicker();

    $("#menuAyudaMaster .cajaContenido").hide();
});


/// <summary>
/// Array de objetos generales de la aplicación
/// </summary>
var Aplicacion = {
    efectosVisualesHabilitados: true,
    //Iniciar app configuration
    //visualEffectsEnabled: Jquery animation in page elements
    Iniciar: function () {
    },

    EstablecerIconosDeEfectos: function () {
        //Getting effect image
        img = $(".configButtoms #appIconVisualEffects");
        if (Aplicacion.efectosVisualesHabilitados) {
            img.attr("src", "images/effect.gif");
            img.attr("title", "Deshabilitar efectos de pantalla");
        }
        else {
            img.attr("src", "images/effectOff.gif");
            img.attr("title", "Habilitar afectos de pantalla");
        }
    },

    SettingIconsEnabled: function (element) {
        var item = $(element).attr("id").substring(7);
        switch (item) {
            case "VisualEffects":
                Aplicacion.efectosVisualesHabilitados = !Aplicacion.efectosVisualesHabilitados;
                break;
        }
        Aplicacion.EstablecerIconosDeEfectos();
    }
}

/// <summary>
/// Array de objetos para el manejo de las notificaciones
/// </summary>
var Notificacion = {
    relojParaOcultarMensaje: null,
    divDeNotificacion: null,

    /// <summary>
    /// Método que muestra la notificación en pantalla
    /// </summary>
    /// <param name="mensaje">mensaje a mostrar</param>
    /// <param name="tipoNotificacion">Tipo notificación ['Error'|'Advertencia'|'Hecho'|'Informacion']</param>
    /// <param name="autoOcultar">Indica si se auto aculta la notificación (bool)</param>
    /// <param name="tiempoAOcultar">Tiempo en milisegundos a ocultar el mensaje</param>
    Mostrar: function (mensaje, tipoNotificacion, autoOcultar, tiempoAOcultar) {
        var className = "notificacion" + tipoNotificacion;
        Notificacion.divDeNotificacion = $("#divNotificacion");
        $("#divNotificacion").width("960px");
        Notificacion.divDeNotificacion.width("960px");
        var contenedorControl = Notificacion.divDeNotificacion.children(".notificacionControles");
        var divMensajes = Notificacion.divDeNotificacion.children(".notificacionContenido:first");
        var nuevoMensaje = $('<div class="' + className + '" style="display:none;">' + mensaje + '</div>');
        $(".notificacion").css("z-index", "1002");
        $(".notificacion").css("width", "960px");

        $(divMensajes).html("");

        $(nuevoMensaje).prependTo(divMensajes);

        if (Aplicacion.efectosVisualesHabilitados) {
            Notificacion.divDeNotificacion.show();
            $(Notificacion.divDeNotificacion).css("opacity", "0.95");
            $(nuevoMensaje).slideDown();
        }
        else {
            Notificacion.divDeNotificacion.show();
            $(Notificacion.divDeNotificacion).css("opacity", "1.0");
            $(nuevoMensaje).show();
        }

        contenedorControl.children("span.notificacionAnclar").hide();

        if (autoOcultar) {
            if (Notificacion.relojParaOcultarMensaje)
                clearTimeout(Notificacion.relojParaOcultarMensaje);
            tiempoAOcultar = tiempoAOcultar ? tiempoAOcultar * 1000: 10000
            tiempoAOcultar = tiempoAOcultar < 1 ? 1000: tiempoAOcultar;
            Notificacion.relojParaOcultarMensaje = setTimeout('Notificacion.Ocultar("#divNotificacion");', tiempoAOcultar);
            contenedorControl.children("span.notificacionAnclar").show();
        }
        else {
            Notificacion.DetenerAutoOcultado();
        }

        contenedorControl.children("span.notificacionCerrar").click(function () {
            Notificacion.Ocultar("#divNotificacion");
        });

        contenedorControl.children("span.notificacionAnclar").click(function () {
            Notificacion.DetenerAutoOcultado();
            if (Aplicacion.efectosVisualesHabilitados)
                $(this).fadeOut();
            else
                $(this).hide();
        });
    },

    /// <summary>
    /// Método que detiene el el timer de ocultar la notificación
    /// </summary>
    DetenerAutoOcultado: function () {
        clearTimeout(Notificacion.relojParaOcultarMensaje);
    },

    /// <summary>
    /// Método que oculta la notificación
    /// </summary>
    Ocultar: function (valor) {
        Notificacion.DetenerAutoOcultado();
        if (Aplicacion.efectosVisualesHabilitados)
            Notificacion.divDeNotificacion.slideUp(function () {
                $(valor).children(".notificacionContenido").html("");
            });
        else {
            Notificacion.divDeNotificacion.hide();
            $(valor).children(".notificacionContenido").html("");
        }
    }
}

/// <summary>
/// Array de objetos para el manejo de la ventana modal
/// </summary>
var VentanaModal = {
    ControlAMostrar: null,

    /// <summary>
    /// Método que Muestra la ventana modal en pantalla
    /// </summary>
    /// <param name="titulo">Título a mostrar</param>
    /// <param name="idControl">Identificador del control a mover dentro de la ventana modal</param>
    /// <param name="esPostBack">Indica si es un postback para que no se anime la ventana</param>
    /// <param name="ancho">Ancho de la ventana</param>
    Mostrar: function (titulo, idControl, esPostBack, ancho, modoClonacion, mostrarBtnCerrar) {
        VentanaModal.ControlAMostrar = $("#" + idControl);
        $(".ventanaModal .ventanaModalContenido").css("z-index", "1002");
        var contenedor = $(".ventanaModal .ventanaModalContenido");
        var contenedorControl = $(".ventanaModal .ventanaModalContenidoDinamico");
        $(".ventanaModalEnvoltura").css({ 'width': '' });
        contenedorControl.html('');

        if (mostrarBtnCerrar)
            $(".ventanaModalTituloHerramientas a:last").show();
        else
            $(".ventanaModalTituloHerramientas a:last").hide();

        if (modoClonacion)
            VentanaModal.ControlAMostrar.clone().appendTo(contenedorControl);
        else
            VentanaModal.ControlAMostrar.appendTo(contenedorControl);
        $(contenedor).append("<div class='limpiarFlotante'></div>");
        if (!esPostBack) {
            if (ancho > 0) {
                $(".ventanaModalEnvoltura").css({ 'width': ancho });
                $(".ventanaModalEnvoltura").css("margin-left", (ancho / 2) * -1 + "px");
            }
            $(".ventanaModalEnvoltura").css({ 'height': 'auto' });
            $(".ventanaModalEnvoltura").css("left", "50%");
            $(".ventanaModalEnvoltura").css({ top: '' });
            $('.ventanaModalTitulo span').text(titulo);
            if (Aplicacion.efectosVisualesHabilitados) {
                //$(".ventanaModalFondo").css({ height: "0%", left: 0, top: 0 });
                //$(".ventanaModalFondo").animate({ height: "100%" }, 500, function () { });				
                $("#divVentanaModal").show();
                $(contenedor).show();
                ancho = $(".ventanaModalEnvoltura").width();
                $(".ventanaModalEnvoltura").css({ "marginLeft": (ancho / 2) * -1 });
                $(".ventanaModalEnvoltura").css({ 'width': ancho });
                VentanaModal.AnimarPosicion();
                $(contenedor).hide().fadeIn(500);
            }
            else {
                $("#divVentanaModal").show();
                $(contenedor).show();
                ancho = $(".ventanaModalEnvoltura").width();
                $(".ventanaModalEnvoltura").css({ 'width': ancho }); $(".ventanaModalEnvoltura").css("margin-left", (ancho / 2) * -1 + "px");
                VentanaModal.AnimarPosicion();
            }
        }
        else {
            $("#divVentanaModal").show();
            $(contenedor).show();
        }
    },

    /// <summary>
    /// Método que anima la posición de la ventana modal
    /// </summary>
    /// <param name="centrarCompleto">Indica si se centra la ventana completamente</param>
    AnimarPosicion: function (centrarCompleto) {
        var altoContenido = $(window).height();
        var anchoContenido = $(window).width();
        var tamanoPosicionAnimarVer = $(".ventanaModalEnvoltura").height() / 2;
        var tamanoPosicionAnimarHor = $(".ventanaModalEnvoltura").width() / 2;

        if (tamanoPosicionAnimarVer > altoContenido / 2) {
            tamanoPosicionAnimarVer = altoContenido / 2;
        }

        if (Aplicacion.efectosVisualesHabilitados) {

            if (centrarCompleto && $(".ventanaModalEnvoltura").css("top") != "50%") {
                $(".ventanaModalEnvoltura").css({
                    "top": altoContenido / 2
                });

                $(".ventanaModalEnvoltura").css({
                    "left": anchoContenido / 2
                });
            }

            $(".ventanaModalEnvoltura").css({
                "marginTop": tamanoPosicionAnimarVer * -1
            });
            $(".ventanaModalEnvoltura").css({
                "marginLeft": tamanoPosicionAnimarHor * -1
            });

            $(".ventanaModalContenido").css("max-height", $(window).height() - ($(".ventanaModalEnvoltura").height() - $(".ventanaModalContenido").height()) - 50);
        }
        else {
            $(".ventanaModalEnvoltura").css("margin-top", tamanoPosicionAnimarVer * -1 + "px");
            $(".ventanaModalContenido").css("max-height", $(window).height() - ($(".ventanaModalEnvoltura").height() - $(".ventanaModalContenido").height()));
        }
    },


    /// <summary>
    /// Método que oculta la ventana modal
    /// </summary>
    Ocultar: function () {
        var contenedor = $(".ventanaModal .ventanaModalContenido");
        var contenedorControl = $(".ventanaModal .ventanaModalContenidoDinamico");
        if (Aplicacion.efectosVisualesHabilitados) {
            $(contenedor).slideUp(250, function () {
                $(".ventanaModalFondo").css({ left: null, top: null, bottom: 0, right: 0 });
                //$(".ventanaModalFondo").animate({ height: "0%" }, 700, function () { });
                $('#divVentanaModal').fadeOut(250, function () {
                    contenedorControl.html('');
                });
            });
        }
        else {
            $('#divVentanaModal').hide();
            contenedorControl.html('');
        }
    },

    /// <summary>
    /// Método que Alterna la visibilidad del contenedor de contenido de la ventana modal
    /// </summary>
    /// <param name="objA">Referencia al botón</param>
    AlternarContenedor: function (objA) {
        if ($(".ventanaModalContenido").is(":visible")) {
            $(objA).html("^");
            $(objA).attr("title", "Mostrar");
            if (Aplicacion.efectosVisualesHabilitados) {
                $(".ventanaModalContenido").slideUp();
            }
            else {
                $(".ventanaModalContenido").hide();
            }

        }
        else {
            $(objA).html("_");
            $(objA).attr("title", "Ocultar");
            if (Aplicacion.efectosVisualesHabilitados) {
                $(".ventanaModalContenido").slideDown();
            }
            else {
                $(".ventanaModalContenido").show();
            }

        }
    }
};

/// <summary>
/// Array de objetos de utilidades
/// </summary>
var Utilidades = {

    /// <summary>
    /// Activa la posibilidad de ocultar el contenido de una caja
    /// </summary>
    ActivarMostrarUOcultarCaja: function () {
        var isIe6 = false;
        jQuery.each(jQuery.browser, function (i, val) {
            if ($.browser.msie && jQuery.browser.version.substr(0, 1) == '6')
                isIe6 = true;
        });
        //        if (isIe6) {
        //            $(".cajaOculta .cajaTitulo").siblings(".cajaContenido").show();
        //        }
        //        else {
        $(".cajaOculta .cajaTitulo").css("cursor", "pointer").attr("title", "mostrar/ocultar");
        $(".cajaOculta .cajaTitulo").unbind('click');
        $(".cajaOculta .cajaTitulo").click(function () {
            if (Aplicacion.efectosVisualesHabilitados)
                $(this).siblings(".cajaContenido").slideToggle();
            else
                $(this).siblings(".cajaContenido").toggle();
        });
        //        }
    },

    Confirmacion: function (mensaje) {
        return confirm(mensaje);
    },

    /// <summary>
    /// Oculta un elemento a partir de su id
    /// </summary>
    OcultarElemento: function (idElemento) {
        if (Aplicacion.efectosVisualesHabilitados)
            $("#" + idElemento).slideUp();
        else
            $("#" + idElemento).hide();
    },

    /// <summary>
    /// Muestra un elemento a partir de su id
    /// </summary>
    MostrarElemento: function (idElemento) {
        if (Aplicacion.efectosVisualesHabilitados)
            $("#" + idElemento).slideDown();
        else
            $("#" + idElemento).show();
    },

    /// <summary>
    /// Activa las utilidades de caja
    /// </summary>
    ActivarUtilidadesCaja: function () {
        Utilidades.ActivarMostrarUOcultarCaja();
        $(".cajaMover").draggable("destroy");
        if (Aplicacion.efectosVisualesHabilitados)
            $(".cajaMover").draggable({ opacity: 0.5, handle: ".manejador:first" });
        else
            $(".cajaMover").draggable({ handle: ".manejador:first" });

        $(".cajaCambiaTamano").each(function (i, e) {
            var ancho = $(e).width();
            $(e).resizable({
                minWidth: ancho
            });
        });

        $(".cajaCambiaTamanoX").each(function (i, e) {
            var ancho = $(e).width();
            $(e).resizable({
                resize: function (event, ui) {
                    ui.size.height = ui.originalSize.height;
                },
                minWidth: ancho
            });
        });
    },

    MostrarPopUp: function (URL, width, height, resizable) {
        newWin = window.open(URL, '_blank', 'toolbar=0,scrollbars=1,location=1,left=100,top=30,statusbar=1,menubar=0,resizable=' + resizable + ',width=' + width + ',height=' + height);
    },

    /// <summary>
    /// Activa la posibilidad de ocultar mostrar/ocultar los items de una lista (UL, OL)
    /// </summary>
    OcultarLista: function () {
        $(".divOcultarElementos").siblings("div").hide();
        $(".divOcultarElementos").css("cursor", "pointer").attr("title", "mostrar/ocultar");
        $(".divOcultarElementos").unbind('click');

        $(".divOcultarElementos").click(function () {
            $(this).text($(this).text().trim() == '+' ? '-': '+');
            if (Aplicacion.efectosVisualesHabilitados) {
                $(this).siblings("div").slideToggle();
            }
            else {
                $(this).siblings("div").toggle();
            }
        });
    },

    /// <summary>
    /// Cierra la ventana actual
    /// </summary>
    CerrarVentana: function () {
        window.close();
    },

    /// <summary>
    /// Array de objetos de utilidades de cajas
    /// </summary>
    Tablas: {
        /// <summary>
        /// Selecciona todos los checkBox de una tabla
        /// </summary>
        /// <param name="obj">Botón de función</param>
        SeleccionarTodo: function (obj) {
            var $contenedorTabla = $(obj).parents(".tablaHerramientas:first").parents("div:first");
            $contenedorTabla.find("input[type='checkbox']").each(function (i, e) {
                $(e).attr("checked", "checked");
            });
        },
        /// <summary>
        /// Deselecciona todos los checkBox de una tabla
        /// </summary>
        /// <param name="obj">Botón de función</param>
        DesSeleccionarTodo: function (obj) {
            var $contenedorTabla = $(obj).parents(".tablaHerramientas:first").parents("div:first");
            $contenedorTabla.find("table input[type='checkbox']").each(function (i, e) {
                $(e).removeAttr("checked");
            });
        },
        /// <summary>
        /// Invierte la selección de los checkBox de una tabla
        /// </summary>
        /// <param name="obj">Botón de función</param>
        InvertirSeleccion: function (obj) {
            var $contenedorTabla = $(obj).parents(".tablaHerramientas:first").parents("div:first");
            $contenedorTabla.find("table input[type='checkbox']").each(function (i, e) {
                if ($(e).is(':checked'))
                    $(e).removeAttr("checked");
                else
                    $(e).attr("checked", "checked");
            });
        },

        /// <summary>
        /// Activa la funcionalidad de cargar un archivo
        /// </summary>
        uploadError: function (sender, args) {
            var divmsg = $get("<%= msgClientSide.ClientID %>");
            divmsg.className = "error";
            divmsg.style.display = 'block';
            divmsg.innerHTML = 'Archivo: ' + args.get_fileName() +
            '<br/>' + 'Error: '
            + args.get_errorMessage();

        },

        /// <summary>
        /// Activa la funcionalidad de cargar un archivo
        /// </summary>
        uploadComplete: function (sender, args) {
            var divmsg = $get("<%= msgClientSide.ClientID %>");
            divmsg.className = "success";
            divmsg.style.display = 'block';
            divmsg.innerHTML = 'Archivo: ' + args.get_fileName() + '<br/>' +
             'Tamaño: ' + args.get_length() + 'bytes';

        },

        /// <summary>
        /// Activa la funcionalidad de ordenar las columnas de una tabla
        /// </summary>
        ActivarTableSorter: function () {
            $(".tableSorter").tablesorter({
                widgets: ['zebra'],
                dedug: false,
                /*textExtractionCustom: {
                6: function (o) {
                return alert($('img', o).src);
                },
                1: function (o) {
                return $('input', o).val();
                },
                2: function (o) {
                return $('input', o).val();
                },
                3: function (o) {
                return $('select', o).val();
                },
                4: function (o) {
                return ($('input', o).is(':checked')) ? '0': '1';
                },
                5: function (o) {
                return $('input:checked', o).val();
                }
                },*/
                textExtraction: function (s) {
                    var $el = $(s),
                    $img = $el.find('img');
                    return $img.length ? $img[0].src: $el.text();
                }
            })

            $(".tableSorter").each(function () {
                var $paginador = $(this).parent("div:first").siblings(".tablaHerramientas:first").find(".tablaPaginador:first");
                if ($paginador.size() == 1) {
                    var filas = $paginador.attr("tableSorterSize");
                    if (!filas || filas == '' || filas % 10 != 0 || filas < 10)
                        filas = 10;
                    else if (filas > 50)
                        filas = 50;

                    $(this).tablesorterPager({ container: $paginador, size: filas });

                    $paginador.find('.pagesize [value="' + filas + '"]').attr('selected', true);
                }
                var $busqueda = $(this).parent("div:first").siblings(".tablaHerramientas:first").find(".tablaFiltro:first");
                if (!$.browser.mozilla) {
                    if ($busqueda.size() == 1) {
                        $(this).tablesorterFilter({ filterContainer: $busqueda.find("input"),
                            filterClearContainer: $busqueda.find("img"),
                            filterCaseSensitive: false
                        });
                    }
                }
                else {
                    $busqueda.hide();
                }
            });
        }
    }
};

/// <summary>
/// Array de objetos para el control de las peticiones asp.net Ajax
/// </summary>
var Ajax = {
    //Getting .Net Ajax intance
    _instance: null,
    //Iniciar Class
    Iniciar: function () {
        if (Sys) {
            Ajax._instance = Sys.WebForms.PageRequestManager.getInstance();
            if (Ajax._instance) {
                //Adding postback handlers
                Ajax._instance.add_beginRequest(Ajax.BeginRequestHandler);
                Ajax._instance.add_endRequest(Ajax.EndRequestHandler);
            }
        }
        else {
            $("#aLoadingCancelar").hide();
        }
    },

    BeginRequestHandler: function (sender, args) {
        if (Aplicacion.efectosVisualesHabilitados) {
            $("#cargando").hide();
            $("#cargando").fadeIn();
        }
    },

    EndRequestHandler: function (sender, args) {
        if (Aplicacion.efectosVisualesHabilitados) {
            $("#cargando").show();
            $("#cargando").fadeOut();
        }
        if (args.get_error() != undefined) {
            //Get error from ajax
            if (args.get_error().httpStatusCode == '500') {
                var errorMessage = args.get_error().message;
                // errorMessage = errorMessage.replace("'", "\'");
                args.set_errorHandled(true);
                //Showing the error
                Notificacion.Mostrar("[error: 500] " + errorMessage, 'Error', false);
            }

            //Error 12029: Error establishing connection with server
            if (args.get_response().get_statusCode() == '12007' || args.get_response().get_statusCode() == '12029' || args.get_response().get_statusCode() == '0') {
                args.set_errorHandled(true);
                Notificacion.Mostrar("[Estado: " + args.get_response().get_statusCode() + "] No se pudo establecer conexión con el servidor", 'Error', false);
            }
        }
        Utilidades.ActivarUtilidadesCaja();
        Utilidades.Tablas.ActivarTableSorter();
        Utilidades.OcultarLista();
    },

    //Cancel request
    CancelarPeticion: function () {
        if (Ajax._instance) {
            if (Ajax._instance.get_isInAsyncPostBack()) {
                Ajax._instance.abortPostBack();
                Notificacion.Mostrar("La solicitud al servidor ha sido cancelada", "Advertencia", true);
            }
            if (System._ajaxRequest) {
                System._ajaxRequest.abort();
                Notificacion.Mostrar("La solicitud al servidor ha sido cancelada", "Advertencia", true);
            }
        }
    }
};
