﻿Imports System.Configuration

Public Class OperacionesBD
    ''' <summary>
    ''' Crea el objeto de la instancia
    ''' </summary>
    ''' <remarks>AERM 20/01/2015</remarks>
    Private Shared _instancia As OperacionesBD

    Private conexion As IDALBaseDatos = Nothing

    ''' <summary>
    ''' No causar problemas si es multihilo (Si tiene varias peticiones a  la vez)
    ''' </summary>
    ''' <remarks>AERM 20/01/2015</remarks>
    Private Shared padlock As New Object()

    ''' <summary>
    ''' Método que instancia solo una vez la clase de conexion atravez de una fachada
    ''' </summary>
    ''' <remarks>AERM 20/01/2015</remarks>
    Private Sub New()
        'Aqui podrian ir un facade con la Conexión
        conexion = Me.operarBD(ConfigurationManager.AppSettings("BD").ToString())
    End Sub

    ''' <summary>
    ''' Método que realiza la instancia mediante una propiedad
    ''' </summary>
    Public Shared ReadOnly Property Instancia() As OperacionesBD
        Get
            'Hasta que no termine la petición no recibe la otra (Caso multihilo)
            SyncLock padlock
                ' valida que no exista una instancia si ya existe, no la realiza
                If (_instancia Is Nothing) Then
                    _instancia = New OperacionesBD()
                End If
            End SyncLock

            Return _instancia
        End Get
    End Property

    ''' <summary>
    ''' Devuelve la instacia ya creada con el singleton
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>AERM 20/01/2015</remarks>
    Public Function operarBD() As IDALBaseDatos
        Return conexion
    End Function

    ''' <summary>
    ''' Crea la instacion segun el tipo de bd
    ''' </summary>
    ''' <param name="baseDatos"></param>
    ''' <returns></returns>
    ''' <remarks>AERM 20/01/2015</remarks>
    Private Function operarBD(baseDatos As String) As IDALBaseDatos
        Try
            Return Me.TipoBaseDeDatosConsulta(baseDatos)
        Catch ex As Exception
            Dim mensaje As String = ex.Message
            Throw
        End Try
    End Function

    ''' <summary>
    ''' Consulta el tipo de base de datos al cual se esta conectando segun la fabrica creada por el proveedor
    ''' </summary>
    ''' <param name="db">Bas e de datos de la conexión</param>
    ''' <returns>Tipo de Base de datos</returns>
    ''' <remarks>AERM 20/01/2015</remarks>
    Private Function TipoBaseDeDatosConsulta(db As String) As IDALBaseDatos
        Try
            Select Case db
                Case "SqlServer"
                    Return New DALBaseDatosSQL()
                Case Else
                    Throw New Exception("No está disponible la base de datos. (OperacionesBD - Datos)")
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class

