﻿Imports Intecsus.Entidades
Imports System.Configuration

Partial Public Class OperacionesBase

    ''' <summary>
    ''' Metodo encargado de guardar alguna operacion en el log
    ''' </summary>
    ''' <param name="idProceso">Identificador del proceso en el cual s eesta trabajando</param>
    ''' <param name="idTransaccion">Identificador de la transaccion</param>
    ''' <param name="evento">Evento que esta pasando 1-Informativo 2- Error aplicacion</param>
    ''' <param name="descripcionEnvento">Descripcion de lo que esta sucediendo</param>
    ''' <param name="conexionBD">Cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 27/01/2015</remarks>
    Public Sub GuardarLogXCadena(idProceso As Integer, idTransaccion As Integer, evento As Integer, descripcionEnvento As String, conexionBD As String)
        Try
            Dim _conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim _listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim _parametro As New Parametro(Of SqlDbType)
            _parametro.nombre = "@IdProceso"
            _parametro.valorEnviar = idProceso
            _parametro.tipoDato = SqlDbType.Int
            _parametro.tamano = idProceso.ToString().Length
            _listParametros.Add(_parametro)

            _parametro = New Parametro(Of SqlDbType)
            _parametro.nombre = "@IdTransaccion"
            _parametro.valorEnviar = idTransaccion
            _parametro.tipoDato = SqlDbType.Int
            _parametro.tamano = idTransaccion.ToString().Length
            _listParametros.Add(_parametro)

            _parametro = New Parametro(Of SqlDbType)
            _parametro.nombre = "@Evento"
            _parametro.valorEnviar = evento
            _parametro.tipoDato = SqlDbType.Int
            _parametro.tamano = evento.ToString().Length
            _listParametros.Add(_parametro)

            _parametro = New Parametro(Of SqlDbType)
            _parametro.nombre = "@DescripcionEvento"
            _parametro.valorEnviar = descripcionEnvento
            _parametro.tipoDato = SqlDbType.VarChar
            _parametro.tamano = descripcionEnvento.Length
            _listParametros.Add(_parametro)

            _conexion.EjecutarSP(conexionBD, "SP_RegistrarLog", "Nothing", _listParametros)
        Catch ex As Exception
            visorEventos.GuardarLog("Application", "Intecsus.BD", "GuardarLogXCadena " + ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de guardar alguna operacion en el log
    ''' </summary>
    ''' <param name="idProceso">Identificador del proceso en el cual s eesta trabajando</param>
    ''' <param name="idTransaccion">Identificador de la transaccion</param>
    ''' <param name="evento">Evento que esta pasando 1-Informativo 2- Error aplicacion</param>
    ''' <param name="descripcionEnvento">Descripcion de lo que esta sucediendo</param>
    ''' <param name="nombreConexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 27/01/2015</remarks>
    Public Sub GuardarLogXNombreConectionString(idProceso As Integer, idTransaccion As Integer, evento As Integer, descripcionEnvento As String, nombreConexion As String)
        Try
            Dim _conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim _listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim _parametro As New Parametro(Of SqlDbType)
            _parametro.nombre = "@IdProceso"
            _parametro.valorEnviar = idProceso
            _parametro.tipoDato = SqlDbType.Int
            _parametro.tamano = idProceso.ToString().Length
            _listParametros.Add(_parametro)

            _parametro = New Parametro(Of SqlDbType)
            _parametro.nombre = "@IdTransaccion"
            _parametro.valorEnviar = idTransaccion
            _parametro.tipoDato = SqlDbType.Int
            _parametro.tamano = idTransaccion.ToString().Length
            _listParametros.Add(_parametro)

            _parametro = New Parametro(Of SqlDbType)
            _parametro.nombre = "@Evento"
            _parametro.valorEnviar = evento
            _parametro.tipoDato = SqlDbType.Int
            _parametro.tamano = evento.ToString().Length
            _listParametros.Add(_parametro)

            _parametro = New Parametro(Of SqlDbType)
            _parametro.nombre = "@DescripcionEvento"
            _parametro.valorEnviar = descripcionEnvento
            _parametro.tipoDato = SqlDbType.VarChar
            _parametro.tamano = descripcionEnvento.Length
            _listParametros.Add(_parametro)

            _conexion.EjecutarSP(Me.CadenaConexion(nombreConexion), "SP_RegistrarLog", "Nothing", _listParametros)
        Catch ex As Exception
            visorEventos.GuardarLog("Application", "Intecsus.BD", "GuardarLogXNombreConectionString " + ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de guardar alguna operacion en el log
    ''' </summary>
    ''' <param name="idProceso">Identificador del proceso en el cual s eesta trabajando</param>
    ''' <param name="idTransaccion">Identificador de la transaccion</param>
    ''' <param name="evento">Evento que esta pasando 1-Informativo 2- Error aplicacion</param>
    ''' <param name="descripcionEnvento">Descripcion de lo que esta sucediendo</param>
    ''' <param name="nombreConexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 27/01/2015</remarks>
    Public Sub GuardarLogXNombreConectionString(idProceso As Integer, idTransaccion As Integer, evento As Integer, descripcionEnvento As String, nombreConexion As String, cliente As String)
        Try
            Dim _conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim _listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim _parametro As New Parametro(Of SqlDbType)
            _parametro.nombre = "@IdProceso"
            _parametro.valorEnviar = idProceso
            _parametro.tipoDato = SqlDbType.Int
            _parametro.tamano = idProceso.ToString().Length
            _listParametros.Add(_parametro)

            _parametro = New Parametro(Of SqlDbType)
            _parametro.nombre = "@IdTransaccion"
            _parametro.valorEnviar = idTransaccion
            _parametro.tipoDato = SqlDbType.Int
            _parametro.tamano = idTransaccion.ToString().Length
            _listParametros.Add(_parametro)

            _parametro = New Parametro(Of SqlDbType)
            _parametro.nombre = "@Evento"
            _parametro.valorEnviar = evento
            _parametro.tipoDato = SqlDbType.Int
            _parametro.tamano = evento.ToString().Length
            _listParametros.Add(_parametro)

            _parametro = New Parametro(Of SqlDbType)
            _parametro.nombre = "@DescripcionEvento"
            _parametro.valorEnviar = descripcionEnvento
            _parametro.tipoDato = SqlDbType.VarChar
            _parametro.tamano = descripcionEnvento.Length
            _listParametros.Add(_parametro)

            _parametro = New Parametro(Of SqlDbType)
            _parametro.nombre = "@Cliente"
            _parametro.valorEnviar = cliente
            _parametro.tipoDato = SqlDbType.VarChar
            _parametro.tamano = cliente.Length
            _listParametros.Add(_parametro)

            _conexion.EjecutarSP(Me.CadenaConexion(nombreConexion), "SP_RegistrarLogCliente", "Nothing", _listParametros)
        Catch ex As Exception
            visorEventos.GuardarLog("Application", "Intecsus.BD", "GuardarLogXNombreConectionString " + ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de devolver la cadena de conexion configurada en la base de datos
    ''' </summary>
    ''' <param name="idConeString">Nombre del conexion String</param>
    ''' <returns>Retorna cadena de conexion por el nombre</returns>
    ''' <remarks>AERM 29/01/2015</remarks>
    Public Function CadenaConexion(ByVal idConeString As String) As String
        Try
            Return ConfigurationManager.ConnectionStrings(idConeString).ConnectionString
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Metodo encargado de enviar los valores del configurador de CardWizard
    ''' </summary>
    ''' <param name="idValor">Identificador del valor</param>
    ''' <param name="conexionBD">Conexion de la base de datos</param>
    ''' <returns>retorna valor solicitado</returns>
    ''' <remarks>AERM 16/03/2015</remarks>
    Public Function ObtenerValoresConfigurador(idValor As String, conexionBD As String) As String
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            Dim m_resultado As DataSet
            m_parametro.nombre = "@IdValor"
            m_parametro.valorEnviar = idValor
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = idValor.Length
            m_listParametros.Add(m_parametro)

            m_resultado = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_ObtenerValor", "Parametros", m_listParametros)
            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                Return m_resultado.Tables(0).Rows(0)("Valor").ToString()
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return String.Empty
    End Function

    ''' <summary>
    ''' Metodo encargado de enviar los valores del configuracion segura
    ''' </summary>
    ''' <param name="idValor">Identificador del valor</param>
    ''' <param name="conexionBD">Conexion de la base de datos</param>
    ''' <returns>retorna valor solicitado</returns>
    ''' <remarks>AERM 30/03/2016</remarks>
    Public Function ObtenerValoresConfiguracionSegura(idValor As String, conexionBD As String) As String
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            Dim m_resultado As DataSet
            m_parametro.nombre = "@IdValor"
            m_parametro.valorEnviar = idValor
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = idValor.Length
            m_listParametros.Add(m_parametro)

            m_resultado = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_ObtenerValorS", "Parametros", m_listParametros)
            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                Return m_resultado.Tables(0).Rows(0)("Valor").ToString()
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return String.Empty
    End Function
    Public Function ObtenerMensaje(codigo As String, conexionBD As String) As List(Of String)
        Dim m_Respuesta As List(Of String) = New List(Of String)
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            Dim m_resultado As DataSet
            m_parametro.nombre = "@CodigoMensaje"
            m_parametro.valorEnviar = codigo
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = codigo.Length
            m_listParametros.Add(m_parametro)

            m_resultado = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_ObtenerMensajeRespuesta", "Parametros", m_listParametros)
            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                Dim m_valor As String = m_resultado.Tables(0).Rows(0)("Mensaje").ToString()
                m_Respuesta.Add(m_valor)
                m_valor = m_resultado.Tables(0).Rows(0)("CodigoCliente").ToString()
                m_Respuesta.Add(m_valor)
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return m_Respuesta
    End Function

    Public Function ObtenerMensajeConversion(mensaje As String, conexionBD As String) As List(Of String)
        Dim m_Respuesta As List(Of String) = New List(Of String)
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            Dim m_resultado As DataSet
            m_parametro.nombre = "@CodigoMensaje"
            m_parametro.valorEnviar = mensaje
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = mensaje.Length
            m_listParametros.Add(m_parametro)

            m_resultado = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_ObtenerConvMensajeRespuesta", "Parametros", m_listParametros)
            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                Dim m_valor As String = m_resultado.Tables(0).Rows(0)("Mensaje").ToString()
                m_Respuesta.Add(m_valor)
                m_valor = m_resultado.Tables(0).Rows(0)("CodigoCliente").ToString()
                m_Respuesta.Add(m_valor)
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return m_Respuesta
    End Function

    Public Function ObtenerSecuencia(conexionBD As String) As Int32
        Dim m_Respuesta As Int32
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_resultado As DataSet
            m_resultado = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_RetornarSecuencia", "Parametros", m_listParametros)
            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                m_Respuesta = Convert.ToInt32(m_resultado.Tables(0).Rows(0)("Numero").ToString())
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return m_Respuesta
    End Function


    ''' <summary>
    ''' Metodo encargado de traer el estado de una respuesta de CW
    ''' </summary>
    ''' <param name="idCodigoCW"></param>
    ''' <param name="conexionBD"></param>
    ''' <returns>Retorna un estado X codigo</returns>
    ''' <remarks>AERM 24/05/2016</remarks>
    Public Function ObtenerEstadoXCodigoCW(idCodigoCW As String, conexionBD As String) As String
        Dim m_Respuesta As String = String.Empty
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            Dim m_resultado As DataSet
            m_parametro.nombre = "@IdCodigo"
            m_parametro.valorEnviar = idCodigoCW
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = idCodigoCW.Length
            m_listParametros.Add(m_parametro)

            m_resultado = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_EstadoXCodigoRespuestaCW", "Parametros", m_listParametros)
            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                Return m_resultado.Tables(0).Rows(0)("IdEstado").ToString()
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return m_Respuesta
    End Function

    ''' <summary>
    ''' Metodo encargado de traer todos estados de una respuesta  Impresion de CW
    ''' </summary>
    ''' <remarks>AERM 24/05/2016</remarks>
    Public Function ObtenerEstadosXCodigoCW(conexionBD As String) As List(Of EstadoCW)
        Dim m_respuesta As List(Of EstadoCW) = New List(Of EstadoCW)()
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_resultado As DataSet

            m_resultado = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_EstadosRespuestaCW", "Parametros", m_listParametros)
            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                For Each m_row As DataRow In m_resultado.Tables(0).Rows
                    Dim m_estado As EstadoCW = New EstadoCW()
                    m_estado.IdMessageType = m_row("IdMessageType").ToString()
                    m_estado.IdDataInteger = m_row("IdDataInteger").ToString()
                    m_estado.estadoFinalCW = CInt(m_row("IdEstado").ToString())
                    m_respuesta.Add(m_estado)
                Next
                Dim m_Estados = EstadosCW.Instancia(m_respuesta)
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return m_respuesta
    End Function

    ''' <summary>
    ''' Metodo encargado de traer todos estados de una respuesta Estado impresora de CW
    ''' </summary>
    ''' <remarks>AERM 24/05/2016</remarks>
    Public Function ObtenerEstadosXImrpesoraCW(conexionBD As String) As List(Of EstadoXImpresora)
        Dim m_respuesta As List(Of EstadoXImpresora) = New List(Of EstadoXImpresora)()
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_resultado As DataSet

            m_resultado = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_EstadosPCNAMEXCW", "Parametros", m_listParametros)
            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                For Each m_row As DataRow In m_resultado.Tables(0).Rows
                    Dim m_estado As EstadoXImpresora = New EstadoXImpresora()
                    m_estado.ReturnCode = m_row("ReturnCode").ToString()
                    m_estado.DeviceType = m_row("DeviceType").ToString()
                    m_estado.DeviceStatus = m_row("DeviceStatus").ToString()
                    m_estado.estadoFinalCW = CInt(m_row("IdEstado").ToString())
                    m_respuesta.Add(m_estado)
                Next
                Dim m_Estados = EstadosXImpresora.Instancia(m_respuesta)
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return m_respuesta
    End Function

    ''' <summary>
    ''' Metodo encargado de actualizar los datos
    ''' </summary>
    ''' <param name="datoActualizar">Datos a actualizar</param>
    ''' <param name="conexion">nombre para buscar la cadena de conexion de la base de datos</param>
    ''' <remarks>AERM 07/04/2015</remarks>
    Public Sub ActualizarConfiguracion(datoActualizar As Configurar, conexion As String)

        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdValor"
            m_parametro.valorEnviar = datoActualizar.IDValor
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = datoActualizar.IDValor.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Valor"
            m_parametro.valorEnviar = datoActualizar.Valor
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = datoActualizar.Valor.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@TipoDato"
            m_parametro.valorEnviar = datoActualizar.TipoDato
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = datoActualizar.TipoDato.Length()
            m_listParametros.Add(m_parametro)

            m_conexion.EjecutarSP(Me.CadenaConexion(conexion), "SP_UpdateValor", "Parametros", m_listParametros)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
