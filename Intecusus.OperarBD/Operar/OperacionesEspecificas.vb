﻿Imports Intecsus.Entidades
Imports System.Configuration
Imports Intecsus.Utilidades

''' <summary>
''' Clase encargada de operaciones especificas
''' </summary>
''' <remarks>AERM 17/03/2015</remarks>
Partial Public Class OperacionesBase

#Region "Instanciar Clases"
    Dim visorEventos As VisorEventos = New VisorEventos()
#End Region
#Region "Estados Transaccion"

    ''' <summary>
    ''' Metodo encargado de guardar
    ''' </summary>
    ''' <param name="cliente">CLiente q solicita la peticion</param>
    ''' <param name="idTransaccionCliente">Identificador de la transaccion (Para el cliente)</param>
    ''' <param name="conexionBD">Identificador coenxion con la BD</param>
    ''' <returns>Retorna idtransaccion del sistema</returns>
    ''' <remarks>AERM 17/03/2015</remarks>
    Public Function InsertarTransaccion(ByVal cliente As String, ByVal idTransaccionCliente As String, ByVal conexionBD As String) As Integer
        Dim m_IdTransaccion As Integer = 0
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            Dim m_resultado As DataSet
            m_parametro.nombre = "@Cliente"
            m_parametro.valorEnviar = cliente
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = cliente.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdTransaccionC"
            m_parametro.valorEnviar = idTransaccionCliente.ToString()
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = idTransaccionCliente.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Estado"
            m_parametro.valorEnviar = ESTADO_REALCE.ST_EN_PROCESO.ToString()
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = ESTADO_REALCE.ST_EN_PROCESO.ToString().Length()
            m_listParametros.Add(m_parametro)
            m_resultado = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_InsertTransaccion", "Parametros", m_listParametros)
            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                Return m_resultado.Tables(0).Rows(0)("Identificador").ToString()
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return m_IdTransaccion
    End Function

    ''' <summary>
    ''' Metodo encargado de cambiar el estado a una transaccion
    ''' </summary>
    ''' <param name="idTransaccion"> Identificador de la transaccion</param>
    ''' <param name="conexionBD">Base de datos conexion</param>
    ''' <param name="estado">Estado a guardar a la BD</param>
    ''' <remarks>AERM 17/03/2015</remarks>
    Public Sub CambiarEstado(ByVal idTransaccion As Integer, ByVal conexionBD As String, ByVal estado As String, Optional ByVal finalNotificacion As Boolean = False)
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdTransaccion"
            m_parametro.valorEnviar = idTransaccion
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = idTransaccion.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Estado"
            m_parametro.valorEnviar = estado
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = estado.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@FinalNotificacion"
            m_parametro.valorEnviar = finalNotificacion
            m_parametro.tipoDato = SqlDbType.Bit
            m_parametro.tamano = finalNotificacion.ToString().Length()
            m_listParametros.Add(m_parametro)
            m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_UpdateTransaccion", "Parametros", m_listParametros)
        Catch ex As Exception
            visorEventos.GuardarLog("Application", "Intecsus.BD", "CambiarEstado " + ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de cambiar el estado de la notificacion a una transaccion
    ''' </summary>
    ''' <param name="idTransaccion"> Identificador de la transaccion</param>
    ''' <param name="conexionBD">Base de datos conexion</param>
    ''' <param name="finalNotificacion">SI al notificacion se envia a realizar</param>
    ''' <remarks>AERM 04/04/2016</remarks>
    Public Sub CambiarEstadoNotificacion(ByVal idTransaccion As Integer, ByVal conexionBD As String, ByVal finalNotificacion As Boolean)
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdTransaccion"
            m_parametro.valorEnviar = idTransaccion
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = idTransaccion.ToString().Length()
            m_listParametros.Add(m_parametro)


            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@MensajeNotificar"
            m_parametro.valorEnviar = finalNotificacion
            m_parametro.tipoDato = SqlDbType.Bit
            m_parametro.tamano = finalNotificacion.ToString().Length()
            m_listParametros.Add(m_parametro)
            m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_UpdateNotificacion", "Parametros", m_listParametros)
        Catch ex As Exception
            visorEventos.GuardarLog("Application", "Intecsus.BD", "CambiarEstado " + ex.Message, EventLogEntryType.Error)
        End Try
    End Sub

    Public Function ObtenerTransaccionXNotificar(ByVal conexionBD As String, ByVal idTransaccion As Integer, ByVal estado As Boolean) As EstadoTransaccion
        Dim transaccion As EstadoTransaccion = Nothing

        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            Dim m_resultado As DataSet
            m_parametro.nombre = "@IdTransaccion"
            m_parametro.valorEnviar = idTransaccion
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = idTransaccion.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@FinalNotificacion"
            m_parametro.valorEnviar = estado
            m_parametro.tipoDato = SqlDbType.Bit
            m_parametro.tamano = estado.ToString().Length()
            m_listParametros.Add(m_parametro)
            m_resultado = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_ObtTransXNotificaicon", "Parametros", m_listParametros)
            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                transaccion = New EstadoTransaccion()
                transaccion.IDTransaccion = idTransaccion
                transaccion.Cliente = m_resultado.Tables(0).Rows(0)("Cliente").ToString()
                transaccion.IdTransaccionCl = m_resultado.Tables(0).Rows(0)("IdTransaccionC").ToString()
                transaccion.Estado = m_resultado.Tables(0).Rows(0)("Estado").ToString()
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return transaccion
    End Function


    ''' <summary>
    ''' Metodo encargado de actualizar valor del Sha512
    ''' </summary>
    ''' <param name="idTransaccion"> Identificador de la transaccion</param>
    ''' <param name="conexionBD">Base de datos conexion</param>
    ''' <param name="Sha512">Valor en SHA512</param>
    ''' <remarks>AERM 23/09/2015</remarks>
    Public Sub IngresarValorSha(ByVal idTransaccion As Integer, ByVal conexionBD As String, ByVal Sha512 As String)
        Dim transaccion As EstadoTransaccion = Nothing
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdTransaccion"
            m_parametro.valorEnviar = idTransaccion
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = idTransaccion.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@InfoSha"
            m_parametro.valorEnviar = Sha512
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = Sha512.Length()
            m_listParametros.Add(m_parametro)
            m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_ActualizarSHA", "Parametros", m_listParametros)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de obtner los datos de la transaccion
    ''' </summary>
    ''' <param name="conexionBD">Base de datos conexion</param>
    ''' <param name="Sha512">Valor en SHA512</param>
    ''' <param name="ChipContacto">Iddentificador del chip de contacto</param>
    ''' <remarks>AERM 23/09/2015</remarks>
    Public Function ObtenerNoticiXSha(ByVal Sha512 As String, ByVal ChipContacto As String, ByVal cliente As String, ByVal conexionBD As String) As EstadoTransaccion
        Dim transaccion As EstadoTransaccion = Nothing
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            Dim m_resultado As DataSet

            m_parametro.nombre = "@IDChip"
            m_parametro.valorEnviar = ChipContacto
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = ChipContacto.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@InfoSha"
            m_parametro.valorEnviar = Sha512
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = Sha512.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Cliente"
            m_parametro.valorEnviar = cliente
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = cliente.Length()
            m_listParametros.Add(m_parametro)

            m_resultado = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_IngresarIDChip", "Parametros", m_listParametros)
            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                transaccion = New EstadoTransaccion()
                transaccion.IDTransaccion = m_resultado.Tables(0).Rows(0)("IdTransaccion").ToString()
                transaccion.Cliente = m_resultado.Tables(0).Rows(0)("Cliente").ToString()
                transaccion.IdTransaccionCl = m_resultado.Tables(0).Rows(0)("IdTransaccionC").ToString()
                transaccion.Estado = m_resultado.Tables(0).Rows(0)("Estado").ToString()
                transaccion.Final = Convert.ToBoolean(m_resultado.Tables(0).Rows(0)("FinalNotificacion").ToString())
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return transaccion
    End Function

#End Region

#Region "Operaciones reintentos"

    ''' <summary>
    ''' Metodod encargado de insertar un proceso para realizar sus reintentos respectivos
    ''' </summary>
    ''' <param name="idTransaccion">Identificador de la transaccion</param>
    ''' <param name="conexionBD">Base de datos conexion</param>
    ''' <param name="informacion">Informacion del proceso</param>
    ''' <remarks>AERM 17/03/2015</remarks>
    Public Sub InsertarProceso(ByVal idTransaccion As Integer, ByVal conexionBD As String, ByVal informacion As String, ByVal idProceso As Integer)
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            Dim m_resultado As DataSet
            m_parametro.nombre = "@IdProceso"
            m_parametro.valorEnviar = idProceso
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = idProceso.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdTransaccion"
            m_parametro.valorEnviar = idTransaccion
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = idTransaccion.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Informacion"
            m_parametro.valorEnviar = informacion
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = informacion.Length()
            m_listParametros.Add(m_parametro)
            m_resultado = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_InsertProceso", "Parametros", m_listParametros)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de obtener un proceso pendiente por ID
    ''' </summary>
    ''' <param name="idTransaccion">Indentificador de la transaacion</param>
    ''' <param name="conexionBD">Base de datos conexion</param>
    ''' <returns>Retorna proceso asociado</returns>
    ''' <remarks>AERM 17/03/2015</remarks>
    Public Function ObtenerProcesoXID(ByVal idTransaccion As Integer, ByVal conexionBD As String) As String
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            Dim m_resultado As DataSet
            m_parametro.nombre = "@IdTransaccion"
            m_parametro.valorEnviar = idTransaccion
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = idTransaccion.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_resultado = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_ObtenerProcesosXIDTransaccion", "Parametros", m_listParametros)
            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                Me.CambiarEstado(idTransaccion, conexionBD, ESTADO_REALCE.ST_EN_PROCESO.ToString())
                Return m_resultado.Tables(0).Rows(0)("Informacion").ToString()
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return String.Empty
    End Function

    ''' <summary>
    ''' Metodo encargado de obtener todas los reintentos pendientes 
    ''' </summary>
    ''' <param name="idProces">Identificador del proceso</param>
    ''' <param name="conexionBD">Base de datos conexion</param>
    ''' <returns>Retorna lista con los procesos pendientes</returns>
    ''' <remarks>AERM 27/03/2014</remarks>
    Public Function ObtnerNotificacion(idProces As Integer, conexionBD As String) As List(Of Reintentos)
        Dim m_Respuesta As List(Of Reintentos) = New List(Of Reintentos)
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            Dim m_resultado As DataSet
            m_parametro.nombre = "@IdProceso"
            m_parametro.valorEnviar = idProces
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = idProces.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_resultado = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_ObtenerProcesosXIDProceso", "Parametros", m_listParametros)
            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                For Each m_row As DataRow In m_resultado.Tables(0).Rows
                    Dim m_Notificacion As Reintentos = New Reintentos()
                    m_Notificacion.IDProceso = CInt(m_row("IdProcesoXRealizar").ToString())
                    m_Notificacion.Informacion = m_row("Informacion").ToString()
                    m_Notificacion.IDTransaccion = CInt(m_row("IdTransaccion").ToString())
                    m_Respuesta.Add(m_Notificacion)
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return m_Respuesta
    End Function

    ''' <summary>
    ''' Metodo encargado de elimianr un proceso ya realizado
    ''' </summary>
    ''' <param name="idProceso">Identificador del proceso</param>
    ''' <param name="conexionBD">Conexion con la base de datos</param>
    ''' <remarks>AERM 27/03/2015</remarks>
    Public Sub EliminarProceso(idProceso As Integer, conexionBD As String)
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdProcesoXRealizar"
            m_parametro.valorEnviar = idProceso
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = idProceso.ToString().Length()
            m_listParametros.Add(m_parametro)
            m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_DeleteProceso", "Parametros", m_listParametros)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de aumentar el intento de un proceso realizado
    ''' </summary>
    ''' <param name="idProceso">Identificador del proceso</param>
    ''' <param name="conexionBD">Conexion con la base de datos</param>
    ''' <remarks>AERM 27/03/2015</remarks>
    Public Sub AumentarIntentosXProceso(idProceso As Integer, conexionBD As String)
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdProcesoXRealizar"
            m_parametro.valorEnviar = idProceso
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = idProceso.ToString().Length()
            m_listParametros.Add(m_parametro)
            m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_AumentarIntentos", "Parametros", m_listParametros)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Valores Cliente X CardWizard"

    ''' <summary>
    ''' Metodo encargado de Obtener el cardFormatName spor tipo producto y cliente
    ''' </summary>
    ''' <param name="cliente">Clietne que solicita la impresion</param>
    ''' <param name="tipoProducto">Tipo de producto que el cliente desea imprimir</param>
    ''' <param name="conexionBD">Conexion de la base datos con al informacion</param>
    ''' <returns> Retorna CardFormatName con la impresion </returns>
    ''' <remarks>AERM 25/03/2015</remarks>
    Public Function ObtenerCardFormatName(cliente As String, tipoProducto As String, conexionBD As String) As List(Of String)
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            Dim m_resultado As DataSet
            m_parametro.nombre = "@Cliente"
            m_parametro.valorEnviar = cliente
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = cliente.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Producto"
            m_parametro.valorEnviar = tipoProducto
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = tipoProducto.Length()
            m_listParametros.Add(m_parametro)

            m_resultado = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_ObtProducXClienXProd", "Parametros", m_listParametros)
            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                Dim m_Retorno As List(Of String) = New List(Of String)
                m_Retorno.Add(m_resultado.Tables(0).Rows(0)("CardName").ToString())
                m_Retorno.Add(m_resultado.Tables(0).Rows(0)("XmlPeticion").ToString())
                Return m_Retorno
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return New List(Of String)
    End Function

    ''' <summary>
    ''' Metodo encargado de obtener el PCNAME segun el nombre enviado por el cliente
    ''' </summary>
    ''' <param name="nombreImpresora">Nombre dado por el cliente</param>
    ''' <param name="conexionBD">Conexion Base de datos</param>
    ''' <returns>Retorna string con el PCNAME</returns>
    ''' <remarks>AERM 25/02/2015</remarks>
    Public Function ObtenerPCNAME(nombreImpresora As String, conexionBD As String) As String
        Dim m_PCNAME As String = String.Empty
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            Dim m_resultado As DataSet
            m_parametro.nombre = "@NombreEnviado"
            m_parametro.valorEnviar = nombreImpresora
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = nombreImpresora.Length()
            m_listParametros.Add(m_parametro)

            m_resultado = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_ObtImprXNombre", "Parametros", m_listParametros)
            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                Return m_resultado.Tables(0).Rows(0)("PCName").ToString()
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return m_PCNAME
    End Function

    ''' <summary>
    ''' Metodo encargado de obtener los valores de cliente y transaccion del cliente
    ''' </summary>
    ''' <param name="idTransaccion">Identificador de la transaccion Idenpla</param>
    ''' <param name="conexionBD"></param>
    ''' <returns>Retorna lsita de string con los valores asociados</returns>
    ''' <remarks>AERM 25/03/2015</remarks>
    Public Function ObtenerDatosCliente(idTransaccion As Integer, conexionBD As String) As List(Of String)
        Dim m_Respuesta As List(Of String) = New List(Of String)()
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            Dim m_resultado As DataSet
            m_parametro.nombre = "@IdTransaccion"
            m_parametro.valorEnviar = idTransaccion
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = idTransaccion.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_resultado = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_ObtenerTransaccionXID", "Parametros", m_listParametros)
            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                m_Respuesta.Add(m_resultado.Tables(0).Rows(0)("Cliente").ToString())
                m_Respuesta.Add(m_resultado.Tables(0).Rows(0)("IdTransaccionC").ToString())
                m_Respuesta.Add(m_resultado.Tables(0).Rows(0)("Estado").ToString())
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return m_Respuesta
    End Function

    Public Function ObtenerRespuesta(cliente As String, idTransaccionC As String, conexionBD As String) As RespuestaWSASNET
        Dim m_Respuesta As RespuestaWSASNET = Nothing
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            Dim m_resultado As DataSet
            m_parametro.nombre = "@Cliente"
            m_parametro.valorEnviar = cliente
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = cliente.Length
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdTransaccionC"
            m_parametro.valorEnviar = idTransaccionC
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = idTransaccionC.Length
            m_listParametros.Add(m_parametro)

            m_resultado = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_SelectTrasaccion", "Parametros", m_listParametros)
            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                m_Respuesta = New RespuestaWSASNET()
                m_Respuesta.Peticion = m_resultado.Tables(0).Rows(0)("Peticion").ToString()
                m_Respuesta.Respuestas = m_resultado.Tables(0).Rows(0)("Respuesta").ToString()
                m_Respuesta.Notifica = m_resultado.Tables(0).Rows(0)("NumeroNotificacion").ToString()
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return m_Respuesta
    End Function

    Public Sub GuardarRespuestaWS(peticion As Peticion, m_Peticion As String, m_RespuestaFinal As String, m_Respuesta As RespuestaTuya, conexionBD As String)
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Cliente"
            m_parametro.valorEnviar = peticion.cliente
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = peticion.cliente.Length()
            m_listParametros.Add(m_parametro)
            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdTransaccionC"
            m_parametro.valorEnviar = peticion.idTransaccion
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = peticion.idTransaccion.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Peticion"
            m_parametro.valorEnviar = m_Peticion
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = m_Peticion.Length()
            m_listParametros.Add(m_parametro)
            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Respuesta"
            m_parametro.valorEnviar = m_RespuestaFinal
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = m_RespuestaFinal.Length()
            m_listParametros.Add(m_parametro)
            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@NumeroNotificacion"
            m_parametro.valorEnviar = m_Respuesta.numeroAutorizacion
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = m_Respuesta.numeroAutorizacion.Length()
            m_listParametros.Add(m_parametro)

            m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_InsertTrasaccion", "Parametros", m_listParametros)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub GuardarRespuestaPeticion(idTransaccion As Int32, m_Peticion As String, numeroNotificacion As String, conexionBD As String)
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdTransaccion"
            m_parametro.valorEnviar = idTransaccion
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = idTransaccion.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Peticion"
            m_parametro.valorEnviar = m_Peticion
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = m_Peticion.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@NumeroNotificacion"
            m_parametro.valorEnviar = numeroNotificacion
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = numeroNotificacion.Length()
            m_listParametros.Add(m_parametro)

            m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_InsertRespuestaTransaccion", "Parametros", m_listParametros)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function ObtenerRespuestaRollBack(idTransaccion As Int32, conexionBD As String) As RespuestaWSASNET
        Dim m_Respuesta As RespuestaWSASNET = Nothing
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            Dim m_resultado As DataSet
            m_parametro.nombre = "@IdTransaccion"
            m_parametro.valorEnviar = idTransaccion
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = idTransaccion.ToString().Length
            m_listParametros.Add(m_parametro)

            m_resultado = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_ObtenerRespuestaTransaccion", "Parametros", m_listParametros)
            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                m_Respuesta = New RespuestaWSASNET()
                m_Respuesta.Peticion = m_resultado.Tables(0).Rows(0)("Peticion").ToString()
                m_Respuesta.Notifica = m_resultado.Tables(0).Rows(0)("NumeroNotificacion").ToString()
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return m_Respuesta
    End Function
#End Region

#Region "Conciliacion"

    Public Sub GuardarConciliacion(conciliacion As Conciliacion, conexionBD As String)
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdTransaccion"
            m_parametro.valorEnviar = conciliacion.IdTransaccion
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = conciliacion.IdTransaccion.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Cliente"
            m_parametro.valorEnviar = conciliacion.Cliente
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = conciliacion.Cliente.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IDTransaccionCliente"
            m_parametro.valorEnviar = conciliacion.IDTransaccionCliente
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = conciliacion.IDTransaccionCliente.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@TipoRegistro"
            m_parametro.valorEnviar = conciliacion.TipoRegistro
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = conciliacion.TipoRegistro.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@CodigoEntidad"
            m_parametro.valorEnviar = conciliacion.CodigoEntidad
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = conciliacion.CodigoEntidad.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@PAN"
            m_parametro.valorEnviar = conciliacion.PAN
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = conciliacion.PAN.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@CODFUMO"
            m_parametro.valorEnviar = conciliacion.CODFUMO
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = conciliacion.CODFUMO.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Usuario"
            m_parametro.valorEnviar = conciliacion.Usuario
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = conciliacion.Usuario.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Canal"
            m_parametro.valorEnviar = conciliacion.Canal
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = conciliacion.Canal.Length()
            m_listParametros.Add(m_parametro)

            m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_InsertConciliacion", "Parametros", m_listParametros)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub ActualizarConciliacion(codigoMensaje As String, mensaje As String, INDES As String, transaccion As Integer, conexionBD As String)
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdTransaccion"
            m_parametro.valorEnviar = transaccion
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = transaccion.ToString().Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@INDES"
            m_parametro.valorEnviar = INDES
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = INDES.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@CodMen"
            m_parametro.valorEnviar = codigoMensaje
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = codigoMensaje.Length()
            m_listParametros.Add(m_parametro)

            m_parametro = New Parametro(Of SqlDbType)
            m_parametro.nombre = "@Descripcion1"
            m_parametro.valorEnviar = mensaje
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = mensaje.Length()
            m_listParametros.Add(m_parametro)

            m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_UpdateConciliacion", "Parametros", m_listParametros)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function ObtenerConciliacion(transaccion As Integer, conexionBD As String) As Conciliacion
        Dim m_conciliacion As Conciliacion = Nothing
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@IdTransaccion"
            m_parametro.valorEnviar = transaccion
            m_parametro.tipoDato = SqlDbType.Int
            m_parametro.tamano = transaccion.ToString().Length()
            m_listParametros.Add(m_parametro)
            Dim m_resultado As DataSet = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_ObtenerConciliacionXID", "Parametros", m_listParametros)
            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                m_conciliacion = New Conciliacion()
                m_conciliacion.IdTransaccion = transaccion
                m_conciliacion.Cliente = m_resultado.Tables(0).Rows(0)("Cliente").ToString()
                m_conciliacion.IDTransaccionCliente = m_resultado.Tables(0).Rows(0)("IDTransaccionCliente").ToString()
                m_conciliacion.TipoRegistro = m_resultado.Tables(0).Rows(0)("TipoRegistro").ToString()
                m_conciliacion.CodigoEntidad = m_resultado.Tables(0).Rows(0)("CodigoEntidad").ToString()
                m_conciliacion.PAN = m_resultado.Tables(0).Rows(0)("PAN").ToString()
                m_conciliacion.CODFUMO = m_resultado.Tables(0).Rows(0)("CODFUMO").ToString()
                m_conciliacion.Canal = m_resultado.Tables(0).Rows(0)("CANAL").ToString()
                m_conciliacion.INDES = m_resultado.Tables(0).Rows(0)("INDES").ToString()
                m_conciliacion.Usuario = m_resultado.Tables(0).Rows(0)("Usuario").ToString()
                m_conciliacion.Fecha = Convert.ToDateTime(m_resultado.Tables(0).Rows(0)("Fecha").ToString())
                m_conciliacion.CodMen = m_resultado.Tables(0).Rows(0)("CodMen").ToString()
                m_conciliacion.Descripcion1 = m_resultado.Tables(0).Rows(0)("Descripcion1").ToString()
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return m_conciliacion
    End Function

    Public Function ObtenerListaConciliacionesXDIA(conexionBD As String) As List(Of Conciliacion)
        Dim m_conciliaciones As List(Of Conciliacion) = New List(Of Conciliacion)()
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_resultado As DataSet = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_ObtenerConciliacionXDIA", "Parametros", m_listParametros)

            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                For Each m_row As DataRow In m_resultado.Tables(0).Rows
                    Dim m_conciliacion As Conciliacion = New Conciliacion()
                    m_conciliacion.IdConciliacion = m_row("IDConciliacion").ToString()
                    m_conciliacion.IdTransaccion = m_row("IdTransaccion").ToString()
                    m_conciliacion.Cliente = m_row("Cliente").ToString()
                    m_conciliacion.IDTransaccionCliente = m_row("IDTransaccionCliente").ToString()
                    m_conciliacion.TipoRegistro = m_row("TipoRegistro").ToString()
                    m_conciliacion.CodigoEntidad = m_row("CodigoEntidad").ToString()
                    m_conciliacion.PAN = m_row("PAN").ToString()
                    m_conciliacion.CODFUMO = m_row("CODFUMO").ToString()
                    m_conciliacion.Canal = m_row("CANAL").ToString()
                    m_conciliacion.INDES = m_row("INDES").ToString()
                    m_conciliacion.Usuario = m_row("Usuario").ToString()
                    m_conciliacion.Fecha = Convert.ToDateTime(m_row("Fecha").ToString())
                    m_conciliacion.CodMen = m_row("CodMen").ToString()
                    m_conciliacion.Descripcion1 = m_row("Descripcion1").ToString()
                    m_conciliacion.Descripcion2 = m_row("Descripcion2").ToString()
                    m_conciliaciones.Add(m_conciliacion)
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return m_conciliaciones
    End Function

    Public Function ObtenerProductos(conexionBD As String) As List(Of Productos)
        Dim m_Productos As List(Of Productos) = New List(Of Productos)()
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_resultado As DataSet = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_ObtenerTamanoProductos", "Parametros", m_listParametros)

            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                For Each m_row As DataRow In m_resultado.Tables(0).Rows
                    Dim m_prodcuto As Productos = New Productos()
                    m_prodcuto.Cliente = m_row("Cliente").ToString()
                    m_prodcuto.Producto = m_row("Producto").ToString()
                    m_prodcuto.Tamano = Convert.ToInt32(m_row("Tamano").ToString())
                    m_Productos.Add(m_prodcuto)
                Next
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return m_Productos
    End Function

    ''' <summary>
    ''' Metodo encargado de obtener el usuario a enviar en CW
    ''' </summary>
    ''' <param name="conexionBD">Indica la cadena de conexion con respecto a la BD</param>
    ''' <param name="usuarioRecibido">Usuario enviado por el cliente</param>
    ''' <returns>Retonar el usuario que se debe enviar segun lo retornado en BD</returns>
    ''' <remarks>AERM 30/111/2016</remarks>
    Public Function Obtener_Usuario(usuarioRecibido As String, conexionBD As String) As String
        Try
            Dim m_conexion As IDALBaseDatos = OperacionesBD.Instancia.operarBD
            Dim m_listParametros As New List(Of Parametro(Of SqlDbType))()
            Dim m_parametro As New Parametro(Of SqlDbType)
            m_parametro.nombre = "@UsuarioRecibido"
            m_parametro.valorEnviar = usuarioRecibido
            m_parametro.tipoDato = SqlDbType.VarChar
            m_parametro.tamano = usuarioRecibido.Length()
            m_listParametros.Add(m_parametro)
            Dim m_resultado As DataSet = m_conexion.EjecutarSP(Me.CadenaConexion(conexionBD), "SP_ObtenerUsuarioConversion", "Parametros", m_listParametros)

            If m_resultado.Tables.Count > 0 And m_resultado.Tables(0).Rows.Count > 0 Then
                Return m_resultado.Tables(0).Rows(0)("UsuarioEviar").ToString
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return String.Empty
    End Function


#End Region
End Class
