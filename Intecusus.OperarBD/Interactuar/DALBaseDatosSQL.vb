﻿Imports Intecsus.Entidades
Imports System.Configuration
Imports Intecsus.Utilidades

Public Class DALBaseDatosSQL
    Implements IDALBaseDatos

#Region "Instanciar Clases"
    Dim visorEventos As VisorEventos = New VisorEventos()
#End Region

#Region "Implementacion Interface"

    Public Function EjecutarSentencia(conexionBD As String, nombreTablaReturn As String, sentencia As String, tablaDestino As String) As DataSet Implements IDALBaseDatos.EjecutarSentencia
        Dim topics = New DataSet()
        Dim adapter = New System.Data.SqlClient.SqlDataAdapter()
        Try
            Dim objCommand As New System.Data.SqlClient.SqlCommand()
            objCommand.CommandType = CommandType.Text
            objCommand.CommandText = sentencia
            objCommand.Connection = New System.Data.SqlClient.SqlConnection(conexionBD)
            Try
                objCommand.Connection.Open()
                adapter = New System.Data.SqlClient.SqlDataAdapter(objCommand)
                adapter.Fill(topics, nombreTablaReturn)
                objCommand.Connection.Close()
                objCommand.Parameters.Clear()
            Finally
                If adapter IsNot Nothing Then
                    adapter.Dispose()
                End If
            End Try
            Return topics
        Catch ex As Exception
            Throw ex
        Finally
            topics.Dispose()
        End Try
    End Function

    Public Function EjecutarSP(conexionBD As String, nombreSP As String, nombreTablaReturn As String, parametros As List(Of Intecsus.Entidades.Parametro(Of SqlDbType))) As DataSet Implements IDALBaseDatos.EjecutarSP
        Dim topics = New DataSet()
        Dim adapter = New System.Data.SqlClient.SqlDataAdapter()
        Try
            Dim objCommand As System.Data.SqlClient.SqlCommand = Me.ObtenerParametros(nombreSP, parametros)

            objCommand.Connection = New System.Data.SqlClient.SqlConnection(conexionBD)
            objCommand.CommandTimeout = 60
            objCommand.Connection.Open()
            Try
                adapter = New System.Data.SqlClient.SqlDataAdapter(objCommand)
                adapter.Fill(topics, nombreTablaReturn)
                objCommand.Connection.Close()
                objCommand.Parameters.Clear()
            Finally
                If adapter IsNot Nothing Then
                    adapter.Dispose()
                End If
            End Try
            Return topics
        Catch ex As Exception
            visorEventos.GuardarLog("Application", "Intecsus.BD", "EjecutarSP " + ex.Message, EventLogEntryType.Error)
            Return topics
        Finally
            topics.Dispose()
        End Try
    End Function

    Public Function EjecutarSPSinTamano(conexionBD As String, nombreSP As String, nombreTablaReturn As String, parametros As List(Of Intecsus.Entidades.Parametro(Of SqlDbType))) As DataSet Implements IDALBaseDatos.EjecutarSPSinTamano
        Dim topics = New DataSet()
        Dim adapter = New System.Data.SqlClient.SqlDataAdapter()
        Try
            Dim objCommand As System.Data.SqlClient.SqlCommand = Me.ObtenerParametrosSinTamano(nombreSP, parametros)

            objCommand.Connection = New System.Data.SqlClient.SqlConnection(conexionBD)
            objCommand.Connection.Open()
            Try
                adapter = New System.Data.SqlClient.SqlDataAdapter(objCommand)
                adapter.Fill(topics, nombreTablaReturn)
                objCommand.Connection.Close()
                objCommand.Parameters.Clear()
            Finally
                If adapter IsNot Nothing Then
                    adapter.Dispose()
                End If
            End Try
            Return topics
        Catch ex As Exception
            Throw ex
        Finally
            topics.Dispose()
        End Try
    End Function

    Public Function SubirDataTable(dtOrigen As DataTable, tablaDestino As String, connectionString As String) As String Implements IDALBaseDatos.SubirDataTable
        Try
            Using bulkCopy As New System.Data.SqlClient.SqlBulkCopy(connectionString, System.Data.SqlClient.SqlBulkCopyOptions.KeepNulls)
                bulkCopy.DestinationTableName = tablaDestino
                bulkCopy.BulkCopyTimeout = 0
                Try
                    bulkCopy.WriteToServer(dtOrigen)
                    Return "OK"
                Catch ex As Exception
                    Return "NOK:" + ex.ToString()
                End Try
            End Using
        Catch ex As Exception
            Return "NOK:" + ex.ToString()
        End Try
    End Function
#End Region

#Region "Metodos Privados"

    ''' <summary>
    ''' Metodo encargado de enviar los parametros
    ''' </summary>
    ''' <param name="nombreSP">Nombre del storeProcedure</param>
    ''' <param name="parametros">Parametros a enviar</param>
    ''' <returns>retorna objeto con el comando a ejecutar</returns>
    ''' <remarks>AERM 20150119</remarks>
    Private Function ObtenerParametros(nombreSP As String, parametros As List(Of Parametro(Of SqlDbType))) As System.Data.SqlClient.SqlCommand
        Try

            Dim objCommand As New System.Data.SqlClient.SqlCommand(nombreSP)
            objCommand.CommandType = CommandType.StoredProcedure
            For Each param In parametros
                objCommand.Parameters.Add(param.nombre, param.tipoDato, param.tamano)
                objCommand.Parameters(param.nombre).Value = param.valorEnviar
            Next

            Return objCommand
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Metodo encargado de enviar los parametros
    ''' </summary>
    ''' <param name="nombreSP">Nombre del storeProcedure</param>
    ''' <param name="parametros">Parametros a enviar</param>
    ''' <returns>retorna objeto con el comando a ejecutar</returns>
    ''' <remarks>AERM 20150119</remarks>
    Private Function ObtenerParametrosSinTamano(nombreSP As String, parametros As List(Of Parametro(Of SqlDbType))) As System.Data.SqlClient.SqlCommand
        Try

            Dim objCommand As New System.Data.SqlClient.SqlCommand(nombreSP)
            objCommand.CommandType = CommandType.StoredProcedure
            For Each param In parametros
                objCommand.Parameters.Add(param.nombre, param.tipoDato)
                objCommand.Parameters(param.nombre).Value = param.valorEnviar
            Next

            Return objCommand
        Catch ex As Exception
            Throw ex
        End Try
    End Function
#End Region
End Class
