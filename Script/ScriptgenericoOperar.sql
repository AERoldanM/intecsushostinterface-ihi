/****** Script for SelectTopNRows command from SSMS  ******/
declare @IDENTIFY as varchar(10)
set @IDENTIFY = '89-'
SELECT  [IdLog]
      ,[IdProceso]
      ,[Fecha]
      ,[IdTransaccion]
      ,[Evento]
      ,[DescripcionEvento]
  FROM [dbo].[LogAplicacion]
 --WHERE DescripcionEvento LIKE '%' + @IDENTIFY + '%'
 where [IdProceso] = 19
 -- where [IdTransaccion] = 4749
  --where [DescripcionEvento] like '%Peticion -89-%'
  order by [Fecha] desc

  SELECT  [IdTransaccion]
      ,[Cliente]
      ,[IdTransaccionC]
      ,[Estado]
      ,[FinalNotificacion]
      ,[Peticion]
      ,[NumeroNotificacion]
      ,[InfoSha]
      ,[IDChip]
  FROM [dbo].[EstadoTransaccion]

  SELECT  [IdTransaccionExitosaWS]
      ,[Cliente]
      ,[IdTransaccionC]
      ,[Peticion]
      ,[Respuesta]
      ,[NumeroNotificacion]
  FROM [dbo].[TransaccionExitosaWS]

  exec SP_ObtenerTransaccionXID 1053

  delete [dbo].[LogAplicacion]
  delete [dbo].[EstadoTransaccion]
  delete  [dbo].[TransaccionExitosaWS]