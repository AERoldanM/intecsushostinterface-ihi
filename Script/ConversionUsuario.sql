CREATE TABLE ConvercionUsuario(
IdConversionUser [int]  identity(1,1) NOT NULL,
UsuarioRecibido [nvarchar](100) NOT NULL,
UsuarioEviar [nvarchar](100) NOT NULL
CONSTRAINT [PK_ConvercionUsuario] PRIMARY KEY  
(
	IdConversionUser ASC
))
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 29-11-2016
-- Description:	Obtener un el usuario a enviar a CW
-- exec SP_ObtenerUsuarioConversion 'Prueba'
-- =============================================
CREATE PROCEDURE SP_ObtenerUsuarioConversion
	@UsuarioRecibido [nvarchar](100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT UsuarioEviar
	FROM ConvercionUsuario
	WHERE UsuarioRecibido  = @UsuarioRecibido
END
GO

INSERT ConvercionUsuario (UsuarioRecibido, UsuarioEviar) VALUES ( N'Prueba', N'PruebaCW')

--ROLLBACK
--DROP TABLE ConvercionUsuario;
--DROP PROCEDURE SP_ObtenerUsuarioConversion;
--GO