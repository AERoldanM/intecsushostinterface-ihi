

---- =============================================
---- Author:		Andrés Roldan
---- Create date: 26-01-2015
---- Description:	IngresarUnaExcepcion
---- exec SP_InsertExcepcion 'Prueba', 'Prueba.SP' , 'Notihing'
---- =============================================
--CREATE PROCEDURE SP_InsertExcepcion
--	@MensajeErro nvarchar(MAX),
--	@Transaccion nvarchar(40)  ,
--	@MensajeConcreto nvarchar(MAX)
--AS
--BEGIN
--	-- SET NOCOUNT ON added to prevent extra result sets from
--	-- interfering with SELECT statements.
--	SET NOCOUNT ON;

--   INSERT INTO [dbo].ErrorAplicacion (Fecha  ,MensajeErro ,Transaccion  ,MensajeConcreto)
--     VALUES (GETDATE()   , @MensajeErro ,@Transaccion  , @MensajeConcreto);
--END
--GO

-- =============================================
-- Author:		Andrés Roldan
-- Create date: 26-01-2015
-- Description:	Ingresar un proceso no realizado
-- exec SP_InsertProceso 10 , 'Notihing', '3594'
-- =============================================
ALTER PROCEDURE [dbo].[SP_InsertProceso]
	@IdProceso int,
	@Informacion nvarchar(MAX),
	@IdTransaccion int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Proceso int
	set @Proceso = 0
	SELECT @Proceso = IdTransaccion
	FROM ProcesosXAprobar
	WHERE IdProceso = @IdProceso AND IdTransaccion = @IdTransaccion
	IF(@Proceso <= 0)
	BEGIN
		 INSERT INTO [dbo].ProcesosXAprobar (IdProceso  ,Fecha ,Informacion  ,CantidadReIntentos, IdTransaccion, Estado)
		VALUES (@IdProceso, GETDATE()   , @Informacion ,0, @IdTransaccion, 0);
	END
END