
-- =============================================
-- Author:		Andrés Roldan
-- Create date: 31-03-2016
-- Description:	elimina registros de las tablas [LogAplicacion] y EstadoTransaccion
--  adicionalmente inicia secuencias autonumericas de ID y reorganiza indices
-- Modificacion: 07/10/2016
-- exec SP_ADMINMantenimiento '2015-11-30 14:22:41.667'
-- =============================================
CREATE PROCEDURE [dbo].[SP_ADMINMantenimiento]
	@Fecha DATETIME
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	/*********************************************
	--------- PROCESO PARA LogAplicacion
	---- DELETE
	*/
		Delete FROM [dbo].[LogAplicacion]
		WHERE FECHA < @FECHA


	---- RESTAURAR ID AUTONUMERICO
		DECLARE @REGISTROS INT = 0

		SELECT TOP 1 @REGISTROS = IdLog
		FROM LogAplicacion
		ORDER BY IdLog DESC

		SET @REGISTROS = @REGISTROS
		DBCC CHECKIDENT (LogAplicacion, RESEED,@REGISTROS);

	/*********************************************
	--------- PROCESO PARA Conciliacion
	---- DELETE
	*/
		Delete FROM [dbo].[Conciliacion]
		WHERE FECHA < @FECHA
	---- RESTAURAR ID AUTONUMERICO
		SET @REGISTROS = 0  

		SELECT TOP 1 @REGISTROS = IdConciliacion
		FROM Conciliacion
		ORDER BY IdConciliacion DESC

		SET @REGISTROS = @REGISTROS
		DBCC CHECKIDENT (Conciliacion, RESEED,@REGISTROS);
	/*********************************************
	--------- No VALIDE constrain
	*/
	ALTER TABLE Conciliacion NOCHECK CONSTRAINT FK_ConciliacionXEstadoTransaccion
	/*********************************************
	--------- PROCESO PARA EstadoTransaccion
	---- DELETE
	*/
		DELETE FROM  [dbo].EstadoTransaccion
		WHERE FECHA < @FECHA

		SET @REGISTROS = 0  
		SELECT TOP 1 @REGISTROS = IdTransaccion
		FROM EstadoTransaccion
		ORDER BY IdTransaccion DESC

		DBCC CHECKIDENT (EstadoTransaccion, RESEED,@REGISTROS);
	/*********************************************
	---------  VALIDE constrain
	*/
	ALTER TABLE Conciliacion CHECK CONSTRAINT FK_ConciliacionXEstadoTransaccion
	/*********************************************
	--------- PROCESO PARA INDICES
	*/
		ALTER INDEX  PK_IdLog  on  dbo.LogAplicacion REORGANIZE
		ALTER INDEX  PK_Conciliacion  on  dbo.Conciliacion REORGANIZE
		ALTER INDEX  PK_EstadoTransaccion  on  dbo.EstadoTransaccion REORGANIZE
END
