ALTER TABLE EstadoTransaccion ADD MensajeNotificar BIT null default 0

  -- =============================================
-- Author:		Andr�s Roldan
-- Create date: 22-05-2015
-- Description:	Busca si existe un registro con notificacion ya realizada
-- Modificacion 04-04-2016
-- Se agrega columna de validaci�n
-- exec SP_ObtTransXNotificaicon '1','0'
-- =============================================
ALTER PROCEDURE [dbo].[SP_ObtTransXNotificaicon]
	@IdTransaccion int,
	@FinalNotificacion bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT IdTransaccion, Cliente, IdTransaccionC, Estado
	FROM  EstadoTransaccion
	WHERE @IdTransaccion = IdTransaccion  AND MensajeNotificar = @FinalNotificacion
END


-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 04-04-2016
-- Description:	actualiza el estado de una transaccion para la notificacion
-- exec SP_UpdateTransaccion 1,'Prueba2'
-- =============================================
CREATE PROCEDURE [dbo].[SP_UpdateNotificacion]
	@IdTransaccion int,
	@MensajeNotificar bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Update  EstadoTransaccion 
	SET  MensajeNotificar = @MensajeNotificar
	WHERE IdTransaccion = @IdTransaccion
END

