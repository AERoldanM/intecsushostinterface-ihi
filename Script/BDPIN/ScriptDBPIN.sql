CREATE TABLE [dbo].Perfil(
IdPerfil [int] identity(1,1) NOT NULL,
NombrePerfil [nvarchar](100) NOT NULL
CONSTRAINT [PK_Perfil] PRIMARY KEY  
(
	IdPerfil ASC
))
GO

CREATE TABLE [dbo].Usuario(
IdUsuario [int] identity(1,1) NOT NULL,
Identificador [nvarchar](100) NOT NULL,
IdPerfil [int]  NOT NULL,
Nombre [nvarchar](100) NOT NULL,
Apellido [nvarchar](100) NOT NULL,
Email [nvarchar](max) NOT NULL,
Telefono [nvarchar](20) NOT NULL,
Contrasena [nvarchar](max)  NULL,
Activo bit not null,
ContrasenaInicial bit not null,
CONSTRAINT [PK_Usuario] PRIMARY KEY  
(
	IdUsuario ASC
))
GO

ALTER TABLE [dbo].Usuario  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Perfil] FOREIGN KEY(IdPerfil)
REFERENCES [dbo].Perfil (IdPerfil)
GO

CREATE TABLE [dbo].OperacionesXPerfil(
IdOperacion [int] identity(1,1) NOT NULL,
IdPerfil [int]  NOT NULL,
CrearUsuario bit NOT NULL,
ModificarUsuario bit NOT NULL,
ListarUsuario bit NOT NULL,
EliminarUsario bit NOT NULL,
CambiarContrasena bit NOT NULL,
AsignarPin bit NOT NULL,
ListarPerfiles bit NOT NULL,
ConsultarConfiguracion bit NOT NULL,
AsignarContrasena bit NOT NULL,
CONSTRAINT [PK_OperacionesXPerfil] PRIMARY KEY  
(
	IdOperacion ASC
))
GO

ALTER TABLE [dbo].OperacionesXPerfil  WITH CHECK ADD  CONSTRAINT [FK_OperacionesX_Perfil] FOREIGN KEY(IdPerfil)
REFERENCES [dbo].Perfil (IdPerfil)
GO



CREATE TABLE [dbo].LogApp(
IdLog [int] identity(1,1) NOT NULL,
Evento int NOT NULL,
Descripcion varchar(max)  NOT NULL,
Fecha DateTime not null,
CONSTRAINT [PK_LogApp] PRIMARY KEY  
(
	IdLog ASC
))
GO

CREATE TABLE [dbo].Configuracion(
IdConfig [int] identity(1,1) NOT NULL,
Nombre varchar(max)  NOT NULL,
Valor varchar(max)  NOT NULL,
CONSTRAINT [PK_Configuracion] PRIMARY KEY  
(
	IdConfig ASC
))
GO


CREATE TABLE [dbo].MensajesError(
IdMensaje [int] identity(1,1) NOT NULL,
Codigo int  NOT NULL,
Mensaje varchar(max)  NOT NULL,
CONSTRAINT [PK_MensajesError] PRIMARY KEY  
(
	IdMensaje ASC
))
GO
-- PERFILES
INSERT INTO [DBO].PERFIL (NombrePerfil) VALUES ('Administrador');
INSERT INTO [DBO].PERFIL (NombrePerfil) VALUES ('Operador');

-- OPERACIONES POR PERFIL
-- ADMINISTRADOR
INSERT INTO [DBO].OPERACIONESXPERFIL (IdPerfil, CrearUsuario, ModificarUsuario, ListarUsuario, EliminarUsario, CambiarContrasena, AsignarPin, ListarPerfiles, ConsultarConfiguracion, AsignarContrasena) VALUES (1, 1, 1, 1, 1, 1, 1, 1, 1, 1);
-- OPERADOR
INSERT INTO [DBO].OPERACIONESXPERFIL (IdPerfil, CrearUsuario, ModificarUsuario, ListarUsuario, EliminarUsario, CambiarContrasena, AsignarPin, ListarPerfiles, ConsultarConfiguracion, AsignarContrasena) VALUES (2, 0, 0, 0, 0, 1, 1, 1, 1, 0);

-- CONFIGURACI�N DE LA APP
INSERT INTO [DBO].CONFIGURACION (NOMBRE, VALOR) VALUES ('TimeOutLecturaBanda',			'30');
INSERT INTO [DBO].CONFIGURACION (NOMBRE, VALOR) VALUES ('TimeOutLecturaChip',			'30');
INSERT INTO [DBO].CONFIGURACION (NOMBRE, VALOR) VALUES ('TimeOutLecturaPIN',			'30');
INSERT INTO [DBO].CONFIGURACION (NOMBRE, VALOR) VALUES ('TimeOutAsignacionPIN',			'30');
INSERT INTO [DBO].CONFIGURACION (NOMBRE, VALOR) VALUES ('TimeOutCrearUsuario',			'30');
INSERT INTO [DBO].CONFIGURACION (NOMBRE, VALOR) VALUES ('TimeOutModificarUsuario',		'30');
INSERT INTO [DBO].CONFIGURACION (NOMBRE, VALOR) VALUES ('TimeOutEliminarUsuario',		'30');
INSERT INTO [DBO].CONFIGURACION (NOMBRE, VALOR) VALUES ('TimeOutListarUsuario',			'30');
INSERT INTO [DBO].CONFIGURACION (NOMBRE, VALOR) VALUES ('TimeOutCambiarPw',				'30');
INSERT INTO [DBO].CONFIGURACION (NOMBRE, VALOR) VALUES ('TimeOutAsignarPw',				'30');
INSERT INTO [DBO].CONFIGURACION (NOMBRE, VALOR) VALUES ('ReintentosAsignacionPIN',		'3');
INSERT INTO [DBO].CONFIGURACION (NOMBRE, VALOR) VALUES ('ReintentosCrearUsuario',		'3');
INSERT INTO [DBO].CONFIGURACION (NOMBRE, VALOR) VALUES ('ReintentosModificarUsuario',	'30');
INSERT INTO [DBO].CONFIGURACION (NOMBRE, VALOR) VALUES ('ReintentosEliminarUsuario', 	'3');
INSERT INTO [DBO].CONFIGURACION (NOMBRE, VALOR) VALUES ('ReintentosListarUsuario',		'3');
INSERT INTO [DBO].CONFIGURACION (NOMBRE, VALOR) VALUES ('ReintentosCambiarPw',			'3');
INSERT INTO [DBO].CONFIGURACION (NOMBRE, VALOR) VALUES ('ReintentosAsignarPw',			'3');

-- C�DIGOS DE ERROR
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (0,	'ValidadorDUKPT: �xito en validaciones');
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (1001, 'Validador DUKPT: Error al iniciar los recursos PKCS#11');
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (1002, 'Validador DUKPT: Error al iniciar sesi�n PKCS#11');
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (1003, 'Validador DUKPT: Error en proceso login PKCS#11');
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (1004, 'Validador DUKPT: Llave no cargada en el HSM');
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (1005, 'Validador DUKPT: Error al cifrar datos');
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (1006, 'Validador DUKPT: Error al descifrar datos');
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (1007, 'Validador DUKPT: Error en proceso de logout');
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (1008, 'Validador DUKPT: Error al cerrar sesi�n PKCS#11');
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (2001, 'Validador DUKPT: PIN de usuario no puede contener secuencias, Ej: 1234, 9876');
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (2002, 'Validador DUKPT: PIN no puede ser el mismo d�gito repetido. Ej: 1111, 9999');
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (2003, 'Validador DUKPT: La longitud del PIN tiene que ser 4');
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (2004, 'Validador DUKPT: PIN nuevo y verificaci�n de PIN no coinciden');
--Errores del sistema WS negocio y datos
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (1, 'Error a realizar la acci�n revisar log');
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (2, 'Usuario o contrase�a erronea');
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (3, 'Error a validar la seguridad');
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (4, 'No tiene privilegios para ');
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (5, 'Contrase�as no son iguales ');
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (6, 'Supero cantidad de intentos el WS Externo ');
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (7, 'Contrase�a antigua no coincide ');
INSERT INTO [DBO].MENSAJESERROR (CODIGO, MENSAJE) VALUES (8, 'Usuario ya existe en la BD ');
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 11-07-2016
-- Description:	Metodo encargado de almacenar una linea en el log
-- exec SP_GuardarLog 0, 'pRUEBA'
-- =============================================
CREATE PROCEDURE SP_GuardarLog
@Evento int,
@Descripcion varchar(max)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO LogApp (Evento, Descripcion, Fecha)
	VALUES(@Evento, @Descripcion, GETDATE())
END
GO



-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 11-07-2016
-- Description:	Metodo encargado de obtener el mensaje por codigo
-- exec SP_ObtenerMensajeXCodigo 0
-- =============================================
CREATE PROCEDURE SP_ObtenerMensajeXCodigo
@CODIGO int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT Mensaje 
	FROM MENSAJESERROR
	WHERE Codigo = @CODIGO
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 11-07-2016
-- Description:	Metodo encargado de validar los datos de un usuario
-- exec SP_IngreosUsuario 1,2132132
-- =============================================
CREATE PROCEDURE SP_IngresoUsuario
@Identificador nvarchar(100),
@Contrasena nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT  IdUsuario, IdPerfil, Nombre, Apellido, Email, Telefono, Activo, ContrasenaInicial
	FROM Usuario 
	WHERE Contrasena  = @Contrasena and Identificador = @Identificador
END
GO


-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 12-07-2016
-- Description:	Metodo encargado de OBTENER LAS OPERACIONES DEL USUARIO
-- exec SP_OperacionesXUsuario 1
-- =============================================
CREATE PROCEDURE SP_OperacionesXUsuario
@Identificador nvarchar(100)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT O.CrearUsuario, O.ModificarUsuario, O.ListarUsuario, O.EliminarUsario, O.CambiarContrasena, O.AsignarPin, O.ListarPerfiles, 
		   O.ConsultarConfiguracion, O.AsignarContrasena
	FROM OperacionesXPerfil As O 
	INNER JOIN Usuario AS U on U.IdPerfil = O.IdPerfil 
	WHERE U.Identificador = @Identificador
END
GO


-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 12-07-2016
-- Description:	Metodo encargado de OBTENER LA configuracion de la app
-- exec SP_ObtenerConfiguracion 
-- =============================================
CREATE PROCEDURE SP_ObtenerConfiguracion
AS
BEGIN
	SET NOCOUNT ON;
	SELECT Nombre ,Valor
	FROM Configuracion
END
GO


-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 12-07-2016
-- Description:	Metodo encargado de OBTENER Los perfiles de la BD
-- exec SP_ObtenerPerfiles 
-- =============================================
CREATE PROCEDURE SP_ObtenerPerfiles
AS
BEGIN
	SET NOCOUNT ON;
	SELECT IdPerfil, NombrePerfil
	FROM Perfil
END
GO


-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 12-07-2016
-- Description:	Metodo encargado de OBTENER Los Uusarios del sistema
-- exec SP_ObtenerUsuarios 
-- =============================================
CREATE PROCEDURE SP_ObtenerUsuarios
AS
BEGIN
	SET NOCOUNT ON;
	SELECT IdUsuario, Identificador, IdPerfil, Nombre, Apellido, Email, Telefono, Contrasena, Activo, ContrasenaInicial
	FROM Usuario
END
GO


-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 12-07-2016
-- Description:	Metodo encargado de crear un usuario en  del sistema
-- exec SP_CrearUsuario '1', 1, 'asa', 'asas','asas', 'asas', 'asas', 0 ,1 
-- =============================================
CREATE PROCEDURE SP_CrearUsuario
@Identificador  nvarchar(100),
@IdPerfil int,
@Nombre nvarchar(100),
@Apellido nvarchar(100),
@Email nvarchar(max),
@Telefono nvarchar(20),
@Contrasena nvarchar(max),
@Activo bit,
@ContrasenaInicial bit
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO Usuario ([Identificador] ,[IdPerfil] ,[Nombre] ,[Apellido] ,[Email] ,[Telefono] ,[Contrasena]  ,[Activo] ,[ContrasenaInicial])
     VALUES  (@Identificador, @IdPerfil, @Nombre, @Apellido, @Email, @Telefono, @Contrasena, @Activo, @ContrasenaInicial)
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 12-07-2016
-- Description:	Metodo encargado de crear un usuario en  del sistema
-- exec SP_EliminarUsuario '1'
-- =============================================
CREATE PROCEDURE SP_EliminarUsuario
@Identificador  nvarchar(100)
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM Usuario 
	WHERE Identificador = @Identificador 
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 12-07-2016
-- Description:	Metodo encargado de crear un usuario en  del sistema
-- exec SP_EditarUsuario '1', 1, 'Estiarmas', 'asas','asas', 'asas', 'asas', 0 ,1 
-- =============================================
CREATE PROCEDURE SP_EditarUsuario
@Identificador nvarchar(100),
@IdPerfil int,
@Nombre nvarchar(100),
@Apellido nvarchar(100),
@Email nvarchar(max),
@Telefono nvarchar(20),
@Contrasena nvarchar(max),
@Activo bit,
@ContrasenaInicial bit
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE Usuario 
	SET 
	   IdPerfil = @IdPerfil
      ,Nombre = @Nombre
      ,Apellido = @Apellido
      ,Email = @Email
      ,Telefono = @Telefono
      ,Contrasena = @Contrasena
      ,Activo = @Activo
      ,ContrasenaInicial = @ContrasenaInicial
 WHERE Identificador = @Identificador
END
GO


-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 12-07-2016
-- Description:	Metodo encargado de asignar una contrase�a por primera vez
-- exec SP_AsignarContrasenaUsuario '1', 'Estiarmas'
-- =============================================
CREATE PROCEDURE SP_AsignarContrasenaUsuario
@Identificador nvarchar(100),
@Contrasena nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE Usuario 
	SET 
      Contrasena = @Contrasena
      ,ContrasenaInicial  = 1
	WHERE Identificador = @Identificador
END
GO


-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 12-07-2016
-- Description:	Metodo encargado cambiar la contrase�a de un usuario
-- exec SP_CambiarContrasena '1', 'Estiarmas'
-- =============================================
CREATE PROCEDURE SP_CambiarContrasena
@Identificador nvarchar(100),
@Contrasena nvarchar(max)
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE Usuario 
	SET 
      Contrasena = @Contrasena
      ,ContrasenaInicial  = 0
 WHERE Identificador = @Identificador
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 14-07-2016
-- Description:	Metodo encargado de obtener la configuraciond e un dato exacto
-- exec SP_CambiarContrasena '1', 'Estiarmas'
-- =============================================
CREATE PROCEDURE SP_ConfigXNombre
@Nombre nvarchar(MAX)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT Valor
	FROM Configuracion
	WHERE Nombre = @Nombre 
END
GO


-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 14-07-2016
-- Description:	Metodo encargado de obtener contrase�a de un usuario X IDUusario
-- exec SP_ObtenerContrasenaXUsuario '1'
-- =============================================
CREATE PROCEDURE SP_ObtenerContrasenaXUsuario
@Identificacion nvarchar(100)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT Contrasena 
	FROM Usuario
	WHERE Identificador = @Identificacion 
END
GO



/*RollBAck*/
/*Procedimientos*/
--DROP PROCEDURE SP_ObtenerContrasenaXUsuario
--DROP PROCEDURE SP_ConfigXNombre
--DROP PROCEDURE SP_CambiarContrasena
--DROP PROCEDURE SP_AsignarContrasenaUsuario
--DROP PROCEDURE SP_EditarUsuario
--DROP PROCEDURE SP_EliminarUsuario
--DROP PROCEDURE SP_CrearUsuario
--DROP PROCEDURE SP_ObtenerUsuarios
--DROP PROCEDURE SP_ObtenerPerfiles
--DROP PROCEDURE SP_ObtenerConfiguracion
--DROP PROCEDURE SP_OperacionesXUsuario
--DROP PROCEDURE SP_IngresoUsuario
--DROP PROCEDURE SP_ObtenerMensajeXCodigo
--DROP PROCEDURE SP_GuardarLog
/*Tablas*/
--Drop table LogApp
--Drop table OperacionesXPerfil
--Drop table Usuario
--Drop table Perfil
--Drop table Configuracion
