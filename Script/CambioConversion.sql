CREATE TABLE [dbo].ConversionMensaje(
IdConversion [int] IDENTITY(1,1) NOT NULL,
MensajeCW [varchar](max) NOT NULL,
Mensaje [varchar](255) NOT NULL,
CodigoCliente [varchar](4) NOT NULL,
CONSTRAINT [PK_ConversionMensaje] PRIMARY KEY  
(
	IdConversion ASC
))
GO

INSERT INTO ConversionMensaje(MensajeCW ,Mensaje,CodigoCliente) VALUES  ('example','Ejemplo','0001')
GO


-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 16-12-2015
-- Description:	Obtiene la conversionde los mensajes de CW
-- Y del cliente
-- exec SP_ObtenerConvMensajeRespuesta 'Example'
-- =============================================
CREATE PROCEDURE SP_ObtenerConvMensajeRespuesta
	@CodigoMensaje varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT Mensaje, CodigoCliente
	FROM ConversionMensaje
	WHERE MensajeCW = @CodigoMensaje
END
GO