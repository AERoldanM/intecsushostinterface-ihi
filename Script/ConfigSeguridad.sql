

CREATE TABLE [dbo].ConfiguracionSegura(
IdValorS nvarchar(50) NOT NULL,
Valor nvarchar(max) NOT NULL
CONSTRAINT [PK_IdValors] PRIMARY KEY  
(
	IdValorS ASC
))
GO


	INSERT  ConfiguracionSegura(IdValorS , Valor)
	Values( 'ResBPOTUYA', '0')
	GO
	INSERT  ConfiguracionSegura(IdValorS , Valor)
	Values( 'ResBPOCOLSUBSIDIO', '1')
	GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 30-03-2016
-- Description:	Inserta algun valor de la configuracion Segura
-- exec SP_InsertValorS 'ResBPO', '1'
-- =============================================
CREATE PROCEDURE SP_InsertValorS
	@IdValor nvarchar(50),
	@Valor nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT  ConfiguracionSegura(IdValorS , Valor)
	Values( @IdValor, @Valor)
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 30-03-2016
-- Description:	Obtiene algun valor de la configuracion SEGURA
-- exec SP_ObtenerValorS 'ResBPO'
-- =============================================
CREATE PROCEDURE SP_ObtenerValorS
	@IdValor nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Select Valor
	From  ConfiguracionSegura
	WHERE IdValorS = @IdValor
END
GO