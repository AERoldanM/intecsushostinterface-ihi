USE [IHIBSPR]
GO
/****** Object:  StoredProcedure [dbo].[SP_ReportImpresiones]    Script Date: 28/09/2016 13:54:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:          Andr�s Roldan
-- Create date: 17-11-2015
-- Description:     genera valores para reporte del cliente
-- Modificacion: 05-09-2016 AERM
-- Se modifica el procedimiento segun estandares de COOMEVA y validando INFO
---Modificacion 28/09/2016 AERM
---- Se aumenta parametro del ID CLIENTE y mensaje de error sea tomado de la tabla conciliacion
-- exec SP_ReportImpresiones 'Coomeva', '2016-08-29', '2016-09-02'
-- =============================================
ALTER PROCEDURE [dbo].[SP_ReportImpresiones]
       @Cliente varchar(max),
       @fechaini DATETIME,
       @fechaFin DATETIME
AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;
       DECLARE @Contador INT           -- Variable contador
       DECLARE @CantidadRegistros INT  -- Cantidad de registros 
       DECLARE @TableVar TABLE (IdTransaccion int, nombreCLiente varchar(20), impresora varchar(20), idTransaccionC varchar(max), pan varchar(MAX),
                                               estadoFInal varchar(2), fecha varchar(10), hora TIME, mensajeNotifica varchar(max))  
        --Tabla que almacena los registros inicialmente
       DECLARE @TableRegistros TABLE (id  int IDENTITY(1,1), IdTransaccion int,  nombreCLiente varchar(20),
                                               idTransaccionC varchar(max), Estado varchar(100),
                                               FinalNotificacion bit, fecha DATETIME, MensajeNotifica varchar(MAX))
		--Variable de las tablas
       DECLARE @nombreCLiente varchar(20)
       DECLARE @impresora varchar(20)
       DECLARE @idTransaccion varchar(MAX)
       DECLARE @idTransaccionF INT
       DECLARE @estado varchar(20)
       DECLARE @estadoFInal varchar(MAX)
       DECLARE @fecha DATETIME
       DECLARE @fechaF varchar(10)
       DECLARE @hora TIME 
       DECLARE @totalExito INT
       DECLARE @totalFallos INT
       DECLARE @totalPend INT
       DECLARE @finalNoti VARCHAR(MAX)
       DECLARE @xml XML
       DECLARE @PAN VARCHAR(MAX)
	   DECLARE @SummitCard varchar(MAX)
	   DECLARE @MensajeNotifica varchar(MAX)
       --inicializacion de variables
       SET @totalExito  = 0
       SET @totalFallos  = 0 
       SET @totalPend  = 0 
       SET @Contador = 1  

	  INSERT INTO @TableRegistros
       SELECT  ET.IdTransaccion,  ET.[Cliente], ET.[IdTransaccionC] , ET.[Estado]  , ET.[FinalNotificacion],  MAX(lo.Fecha) AS Fecha, 
				C.Descripcion1 
         FROM [dbo].[EstadoTransaccion] AS ET
         INNER JOIN [dbo].LogAplicacion AS LO on ET.IdTransaccion = LO.IdTransaccion
		 INNER JOIN [dbo].Conciliacion AS C on ET.IdTransaccion = C.IdTransaccion
       WHERE LO.FECHA BETWEEN @fechaini AND @fechaFin  AND ET.Cliente = @Cliente
         GROUP BY  ET.[IdTransaccion], ET.[Cliente] , ET.[IdTransaccionC] , ET.[Estado] , ET.[FinalNotificacion] , ET.[Peticion]
               , ET.[NumeroNotificacion] , ET.[InfoSha]  , ET.[IDChip], C.Descripcion1, C.Descripcion2
	 --Realizamos el count de la transaccion
       SELECT @CantidadRegistros =  COUNT(IdTransaccion)
       FROM @TableRegistros

	 --Recorremos los registros --select * from @TableRegistros
       WHILE @Contador <= @CantidadRegistros
       BEGIN
		   SET @xml = '';
		   SET @impresora = '';
		   SET @PAN = '';
		   SET @SummitCard = '';
		   SET @finalNoti = '';
		   set @estadoFInal = '';
		   set @estadoFInal = '';
		   set @MensajeNotifica = ''
		  -- set @noveda = '';
             SELECT @idTransaccionF = R.[IdTransaccion], @nombreCLiente = R.[nombreCLiente], @idTransaccion =  R.[IdTransaccionC], @estado =  R.[Estado],
                       @finalNoti = R.[FinalNotificacion] ,  @fecha =  R.fecha , @MensajeNotifica = R.MensajeNotifica
             FROM @TableRegistros AS R
             WHERE R.id = @Contador 
		 --Obtenemos el XML de Hacertajeta y lo formateamos para generar su respectivo XML
			SELECT @SummitCard = REPLACE(REPLACE(L.DescripcionEvento,'OperacionesBaseCW - HacerTarjeta :',''),'?','') 
			FROM LogAplicacion AS L
			WHERE L.IdTransaccion = @idTransaccionF AND L.IdProceso = 5
			SET  @SummitCard = REPLACE(REPLACE(@SummitCard,'<xml version="1.0" encoding="utf-8" standalone="yes"><CW_XML_Interface direction="Request" sequence="1">',''), '</CW_XML_Interface>', '')
		--Formatea a XML y obtiene la impresora y el PAN
			SELECT @xml = CONVERT(xml,@SummitCard)
			SELECT  @impresora = N.x.value('(RequestorPCName/text())[1]', 'varchar(max)') ,  @PAN = N.x.value('(DATAITEM/text())[1]', 'varchar(max)')
			FROM @xml.nodes('METHOD') AS N(x)
		--Se  Obtiene cuantas fueron correctas y cuantas mal
			IF( @estado = 'OK')
			BEGIN
				SET @totalExito  = @totalExito + 1
                SET @estado = 'OK'
			END
			ELSE
			BEGIN
				SET @totalFallos  = @totalFallos + 1 
                SET @estado = 'ER' 
			END
			SELECT @fechaF =  CONVERT(varchar(10),@fecha, 103), @hora =  CONVERT(time, @fecha , 118)
			INSERT INTO @TableVar VALUES (@idTransaccionF, @nombreCLiente, @impresora, @idTransaccion, @PAN, @estado,  @fechaF, @hora,  @MensajeNotifica )
			SET @Contador = @Contador + 1
       END

	   SELECT * FROM @TableVar ORDER BY fecha desc, hora desc
       SELECT @totalExito as TOTALEXITOSO, @totalFallos as TOTALFALLOS, (@totalExito + @totalFallos) as TOTALGENERAL
END

