CREATE FUNCTION [dbo].[ufnObtenerPAN](@texto varchar(max))
RETURNS varchar(max) 
AS 
-- Returns the stock level for the product.
BEGIN
       DECLARE @index as int
       DECLARE @cadena as varchar(max)
	  if(@texto = '')
	  begin
	  SET @cadena = '****************'
	 END
	 ELSE
	 BEGIN
	   select @index = PATINDEX('%@PAN@%', @texto)
       select @cadena = SUBSTRING(@texto, @index, 100)
       select @index = PATINDEX('%CDATA%',@cadena) + 6
       select @cadena = SUBSTRING(@cadena, @index, 15)
	 END

    RETURN @cadena;
END;

-- =============================================
-- Author:          Andr�s Roldan
-- Create date: 17-11-2015
-- Description:     genera valores para reporte del cliente
-- exec SP_ReportImpresiones 'TUYA', '2015-09-22', '2016-09-24'
--exec SP_ReportImpresiones 'COLSUBSIDIO', '2015-12-12 00:00:00.000', '2015-12-17 23:59:59'
-- =============================================
CREATE PROCEDURE [dbo].[SP_ReportImpresiones]
       @Cliente varchar(max),
       @fechaini DATETIME,
       @fechaFin DATETIME
AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;
       DECLARE @Contador INT           -- Variable contador
       DECLARE @CantidadRegistros INT  -- Cantidad de registros 
       DECLARE @TableVar TABLE (nombreCLiente varchar(20), impresora varchar(20), idTransaccion varchar(20), pan varchar(MAX),
                                               estadoFInal varchar(2), fecha varchar(10), hora TIME)  
       --Tabla que almacena los registros inicialmente
       DECLARE @TableRegistros TABLE (id  int IDENTITY(1,1), IdTransaccion int,  nombreCLiente varchar(20),
                                               idTransaccionC varchar(20), Estado varchar(100),
                                               FinalNotificacion bit,Peticion varchar(max), NumeroNotificacion varchar(max),
                                               IDChip varchar(max), InfoSha varchar(max), fecha DATETIME) 
       --Variable de las tablas
       DECLARE @nombreCLiente varchar(20)
       DECLARE @impresora varchar(20)
       DECLARE @idTransaccion varchar(20)
       DECLARE @idTransaccionF INT
       DECLARE @estado varchar(20)
       DECLARE @estadoFInal varchar(2)
       DECLARE @fecha DATETIME
       DECLARE @fechaF varchar(10)
       DECLARE @hora TIME 
       DECLARE @totalExito INT
       DECLARE @totalFallos INT
       DECLARE @totalPend INT
       DECLARE @finalNoti BIT
       DECLARE @xml XML
       DECLARE @numNotificacion VARCHAR(MAX)
       DECLARE @noveda INT
       DECLARE @PAN VARCHAR(MAX)
       DECLARE @shaPAN VARCHAR(MAX)
       --inicializacion de variables
       SET @totalExito  = 0
       SET @totalFallos  = 0 
       SET @totalPend  = 0 
       SET @Contador = 1  
       --Almacenamos los registros en la tabla dinamica
       INSERT INTO @TableRegistros
       SELECT  ET.IdTransaccion,  ET.[Cliente], ET.[IdTransaccionC] , ET.[Estado]  , ET.[FinalNotificacion], 
                    ET.[Peticion], ET.[NumeroNotificacion], ET.[IDChip] ,ET.[InfoSha], MAX(lo.Fecha) AS Fecha
               --,CONVERT(varchar(10),  Max(lo.Fecha), 101) as Fecha
             -- ,CONVERT(varchar(8),  Max(lo.Fecha), 114) as HORA
         FROM [dbo].[EstadoTransaccion] AS ET
         INNER JOIN [dbo].LogAplicacion AS LO on ET.IdTransaccion = LO.IdTransaccion
       WHERE LO.FECHA BETWEEN @fechaini AND @fechaFin AND ET.Cliente = @Cliente and DescripcionEvento like '%<codigoNovedad>03</codigoNovedad>%'
         GROUP BY  ET.[IdTransaccion], ET.[Cliente] , ET.[IdTransaccionC] , ET.[Estado] , ET.[FinalNotificacion] , ET.[Peticion]
               , ET.[NumeroNotificacion] , ET.[InfoSha]  , ET.[IDChip]
       --Realizamos el count de la transaccion
       SELECT @CantidadRegistros =  COUNT(IdTransaccion)
       FROM @TableRegistros
 
       --Recorremos los registros --select * from @TableRegistros
       WHILE @Contador <= @CantidadRegistros
       BEGIN
		   set @xml = '';
		   set @impresora = '';
		  -- set @noveda = '';
             SELECT @idTransaccionF = R.[IdTransaccion], @nombreCLiente = R.[nombreCLiente], @idTransaccion =  R.[IdTransaccionC], @estado =  R.[Estado],
                       @finalNoti = R.[FinalNotificacion],  @xml = convert(xml, REPLACE(R.[Peticion],'utf-16','utf-8') ), @numNotificacion = R.[NumeroNotificacion],
                       @shaPAN =  R.[InfoSha],  @fecha =  R.fecha 
             FROM @TableRegistros AS R
             WHERE R.id = @Contador
 
             SELECT @noveda = N.x.value('(codigoNovedad/text())[1]', 'int') 
             FROM @xml.nodes('Peticion') AS N(x)
             IF(@noveda = 3)
             BEGIN
                    SELECT @impresora = N.x.value('(printName/text())[1]', 'varchar(max)') 
                    FROM @xml.nodes('Peticion') AS N(x)
       
                    IF((@finalNoti = 1 AND @estado = 'Notificacion-OK') OR (@numNotificacion != 'No existe') AND @estado != 'Notificacion-ERR' AND @estado != 'ST_ERRORNotificacion')
                    BEGIN
                    SET @totalExito  = @totalExito + 1
                    SET @estado = 'OK'
                    END
                    ELSE
                    BEGIN
                           SET @totalFallos  = @totalFallos + 1 
                           SET @estado = 'ER'
                    END
					set @PAN = ''
                    SELECT @fechaF =  CONVERT(varchar(10),@fecha, 103), @hora =  CONVERT(time, @fecha , 118)
                    SELECT @PAN = DescripcionEvento
                    FROM  [dbo].[LogAplicacion]
                    WHERE [IdTransaccion] = @idTransaccionF AND IdProceso = 5
                    INSERT INTO @TableVar VALUES (@nombreCLiente, @impresora, @idTransaccion, dbo.ufnObtenerPAN(@PAN), @estado,  @fechaF, @hora )
                    --SELECT @nombreCLiente, @finalNoti, @fecha, @fechaF,
             END
    
       --SELECT @nombreCLiente, @idTransaccion, @finalNoti
       SET @Contador = @Contador + 1
       END
       SELECT * FROM @TableVar ORDER BY fecha desc, hora desc
       SELECT @totalExito as TOTALEXITOSO, @totalFallos as TOTALFALLOS, (@totalExito + @totalFallos) as TOTALGENERAL
END
GO
CREATE FUNCTION [dbo].[ufnObtenerTagXML]
(@textoCOMPLETO varchar(max), @tag varchar(max), @TamIni int)
RETURNS varchar(max) 
AS 
-- Returns the stock level for the product.
BEGIN
	IF(@textoCOMPLETO <> '')
	BEGIN
	   DECLARE @index as int
	   DECLARE @indexFinal as int
       DECLARE @cadena as varchar(max)
	  
	  SELECT @index = PATINDEX(@tag, @textoCOMPLETO)
	  SELECT @cadena = SUBSTRING(@textoCOMPLETO, @index , @TamIni)
	  SELECT @index = PATINDEX('%>%', @cadena)
	  SELECT @indexFinal = PATINDEX('%</%', @cadena)
	  SELECT @cadena = SUBSTRING(@cadena, @index + 1, (@indexFinal - @index) -1)
	 END
      
	
    RETURN @cadena;
END;
GO
-- =============================================
-- Author:          Andr�s Roldan
-- Create date: 17-11-2015
-- Description:     genera valores para reporte del cliente
-- exec SP_ReportSoporte 'TUYA', '2015-09-22', '2016-09-24'
--exec SP_ReportSoporte 'COLSUBSIDIO', '2015-12-01 11:24:44.023', '2015-12-01 23:59:59'
-- =============================================
CREATE PROCEDURE [dbo].[SP_ReportSoporte]
       @Cliente varchar(max),
       @fechaini DATETIME,
       @fechaFin DATETIME
AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;
       DECLARE @Contador INT           -- Variable contador
       DECLARE @CantidadRegistros INT  -- Cantidad de registros 
       DECLARE @TableVar TABLE (idTransaccion int, nombreCLiente varchar(20), impresora varchar(20), idTransaccionC varchar(20), pan varchar(MAX),
                                               estadoFInal varchar(2), fecha varchar(10), hora TIME, NotificacionCliente varchar(MAX), 
											   RespuestaBPO varchar(MAX))  
       --Tabla que almacena los registros inicialmente
       DECLARE @TableRegistros TABLE (id  int IDENTITY(1,1), IdTransaccion int,  nombreCLiente varchar(20),
                                               idTransaccionC varchar(20), Estado varchar(100),
                                               FinalNotificacion bit,Peticion varchar(max), NumeroNotificacion varchar(max),
                                               IDChip varchar(max), InfoSha varchar(max), fecha DATETIME) 
       --Variable de las tablas
       DECLARE @nombreCLiente varchar(20)
       DECLARE @impresora varchar(20)
       DECLARE @idTransaccion varchar(20)
       DECLARE @idTransaccionF INT
       DECLARE @estado varchar(20)
       DECLARE @estadoFInal varchar(2)
       DECLARE @fecha DATETIME
       DECLARE @fechaF varchar(10)
       DECLARE @hora TIME 
       DECLARE @totalExito INT
       DECLARE @totalFallos INT
       DECLARE @totalPend INT
       DECLARE @finalNoti BIT
       DECLARE @xml XML
       DECLARE @numNotificacion VARCHAR(MAX)
       DECLARE @noveda INT
       DECLARE @PAN VARCHAR(MAX)
       DECLARE @shaPAN VARCHAR(MAX)
	   DECLARE @respuestaNoti as varchar(max)
	   DECLARE @respuestaBPO as varchar(max)
       --inicializacion de variables
       SET @totalExito  = 0
       SET @totalFallos  = 0 
       SET @totalPend  = 0 
       SET @Contador = 1  
       --Almacenamos los registros en la tabla dinamica
       INSERT INTO @TableRegistros
       SELECT  ET.IdTransaccion,  ET.[Cliente], ET.[IdTransaccionC] , ET.[Estado]  , ET.[FinalNotificacion], 
                    ET.[Peticion], ET.[NumeroNotificacion], ET.[IDChip] ,ET.[InfoSha], MAX(lo.Fecha) AS Fecha
               --,CONVERT(varchar(10),  Max(lo.Fecha), 101) as Fecha
             -- ,CONVERT(varchar(8),  Max(lo.Fecha), 114) as HORA
         FROM [dbo].[EstadoTransaccion] AS ET
         INNER JOIN [dbo].LogAplicacion AS LO on ET.IdTransaccion = LO.IdTransaccion
       WHERE LO.FECHA BETWEEN @fechaini AND @fechaFin AND ET.Cliente = @Cliente and DescripcionEvento like '%<codigoNovedad>03</codigoNovedad>%'
         GROUP BY  ET.[IdTransaccion], ET.[Cliente] , ET.[IdTransaccionC] , ET.[Estado] , ET.[FinalNotificacion] , ET.[Peticion]
               , ET.[NumeroNotificacion] , ET.[InfoSha]  , ET.[IDChip]
       --Realizamos el count de la transaccion
       SELECT @CantidadRegistros =  COUNT(IdTransaccion)
       FROM @TableRegistros
 
       --Recorremos los registros --select * from @TableRegistros
       WHILE @Contador <= @CantidadRegistros
       BEGIN
		   set @xml = '';
		   set @impresora = '';
		  -- set @noveda = '';
             SELECT @idTransaccionF = R.[IdTransaccion], @nombreCLiente = R.[nombreCLiente], @idTransaccion =  R.[IdTransaccionC], @estado =  R.[Estado],
                       @finalNoti = R.[FinalNotificacion],  @xml = convert(xml, REPLACE(R.[Peticion],'utf-16','utf-8') ), @numNotificacion = R.[NumeroNotificacion],
                       @shaPAN =  R.[InfoSha],  @fecha =  R.fecha 
             FROM @TableRegistros AS R
             WHERE R.id = @Contador
 
             SELECT @noveda = N.x.value('(codigoNovedad/text())[1]', 'int') 
             FROM @xml.nodes('Peticion') AS N(x)
             IF(@noveda = 3)
             BEGIN
                    SELECT @impresora = N.x.value('(printName/text())[1]', 'varchar(max)') 
                    FROM @xml.nodes('Peticion') AS N(x)
       
                    IF((@finalNoti = 1 AND @estado = 'Notificacion-OK') OR (@numNotificacion != 'No existe') AND @estado != 'Notificacion-ERR' AND @estado != 'ST_ERRORNotificacion')
                    BEGIN
                    SET @totalExito  = @totalExito + 1
                    SET @estado = 'OK'
                    END
                    ELSE
                    BEGIN
                           SET @totalFallos  = @totalFallos + 1 
                           SET @estado = 'ER'
                    END
					set @PAN = ''
					set @respuestaNoti = ''
					set @respuestaBPO = ''
						SELECT TOP(1) @respuestaNoti = DescripcionEvento 
						FROM [dbo].[LogAplicacion]
						WHERE IdProceso = 21 and IdTransaccion = @idTransaccionF
						ORDER BY fecha
						SELECT TOP(1) @respuestaBPO = DescripcionEvento 
						FROM [dbo].[LogAplicacion]
						WHERE IdProceso = 13 and IdTransaccion = @idTransaccionF
						ORDER BY fecha
						 
                    SELECT @fechaF =  CONVERT(varchar(10),@fecha, 103), @hora =  CONVERT(time, @fecha , 118)
                    SELECT @PAN = DescripcionEvento
                    FROM  [dbo].[LogAplicacion]
                    WHERE [IdTransaccion] = @idTransaccionF AND IdProceso = 5
                    INSERT INTO @TableVar VALUES (@idTransaccionF,@nombreCLiente, @impresora, @idTransaccion, dbo.ufnObtenerPAN(@PAN), @estado,  @fechaF, @hora, 
													--@respuestaNoti, @respuestaBPO)
													dbo.ufnObtenerTagXML(@respuestaNoti, '%Mensaje%', 500), dbo.ufnObtenerTagXML(@respuestaBPO, '%<descripcion%', 500) )
                    --SELECT @nombreCLiente, @finalNoti, @fecha, @fechaF,
             END
    
       --SELECT @nombreCLiente, @idTransaccion, @finalNoti
       SET @Contador = @Contador + 1
       END
       SELECT * FROM @TableVar ORDER BY fecha desc, hora desc
       SELECT @totalExito as TOTALEXITOSO, @totalFallos as TOTALFALLOS, (@totalExito + @totalFallos) as TOTALGENERAL
END