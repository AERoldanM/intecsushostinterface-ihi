-- =================================================================================
-- Autor:		Andr�s Rold�n
-- Fecha Creaci�n:	26-02-2016
-- Nombre Proyecto:	IHI-Cliente
-- Descripci�n:	Insert Basicos
-- =================================================================================

--Configuracion
INSERT  Configuracion(IdValor , Valor, TipoDato) Values( 'CantReintentos', '3', 'INT')
GO
INSERT  Configuracion(IdValor , Valor, TipoDato) Values( 'UsuarioCW', 'w', 'ALFA')
GO
INSERT  Configuracion(IdValor , Valor) Values( 'PasswordCW', 'JpRcm07jhw6J8PUe11DhzkcH0kVyCr7R')
GO
INSERT  Configuracion(IdValor , Valor) Values( 'IpCW', '10.2.5.50')
GO
INSERT  Configuracion(IdValor , Valor, TipoDato) Values( 'PuertoCW', '8890', 'INT')
GO
INSERT  Configuracion(IdValor , Valor, TipoDato) Values( 'EjecucionProcesoMiliSegundos', '5000','INTO')
GO
INSERT  Configuracion(IdValor , Valor, TipoDato) Values( 'EjecucionProcesoMiliSegundosI', '1000','INTO')
GO
INSERT  Configuracion(IdValor , Valor, TipoDato) Values( 'EntreTransaccionMiliSegundos', '500','INTO')
GO
INSERT  Configuracion(IdValor , Valor, TipoDato) Values( 'EjecucionReIntentosCWSegundos', '90', 'INTO')
GO
INSERT  Configuracion(IdValor , Valor, TipoDato) Values( 'InterfaceCW', 'CW_XML_Interface','ALFA')
GO
INSERT  Configuracion(IdValor , Valor, TipoDato) Values( 'CerrarSessionEnHoras', '12', 'INTO')
GO
INSERT  Configuracion(IdValor , Valor) Values( 'ColaRealce', 'IDBO-IN09\RegistroRealce')
GO
INSERT  Configuracion(IdValor , Valor) Values( 'ColaNotificacion', 'IDBO-IN09\Notificacion')
GO
INSERT  Configuracion(IdValor , Valor) Values( 'ColaCliente', 'IDBO-IN09\private$\ServicioIntegracion')
GO
INSERT  Configuracion(IdValor , Valor) Values( 'BuscarImpresoraSantander', '0')
GO
INSERT  Configuracion(IdValor , Valor) Values( 'ArchivoPlano', 
	'<Parametros> 
		<Producto inicio="300" tamano="5"></Producto>
		<Impresora inicio="300" tamano="5"></Impresora>
	</Parametros>')

GO 
INSERT  Configuracion(IdValor , Valor) Values( 'Tiempo', '90')
GO

INSERT  Configuracion(IdValor , Valor) Values( 'RutaArchivo', 'C:\Archivo')
GO
INSERT  Configuracion(IdValor , Valor, TipoDato) Values( 'FormatoArchivo', 'TXT', 'ALFA')
GO
INSERT  Configuracion(IdValor , Valor, TipoDato) Values( 'ProcesarArchivo', 'ACTIVO', 'ALFA')
GO
INSERT  Configuracion(IdValor , Valor) Values( 'CardWizardNameXArchivo', '1')
GO
INSERT  Configuracion(IdValor , Valor) Values( 'PCNAMEXArchivo', 'WIN-4H1I0FLTUGA')
GO
INSERT  Configuracion(IdValor , Valor) Values( 'RutaArchivoNotifica', 'C:\Archivo\Notifica')
GO
INSERT  Configuracion(IdValor , Valor) Values( 'RutaArchivoProcesado', 'C:\Archivo\Procesado')
GO
INSERT  Configuracion(IdValor , Valor,TipoDato) Values( 'KeyHash', 'Intecsus', 'ALFA')
GO
-- Validadores
INSERT  Configuracion(IdValor , Valor) Values( 'ALFA', '^[A-z 0-9]*$')
GO
INSERT  Configuracion(IdValor , Valor) Values( 'ALFAAM', '^[A-z& 0-9]*$')
GO
INSERT  Configuracion(IdValor , Valor) Values( 'INT', '^[0-9]*$')
GO
INSERT  Configuracion(IdValor , Valor) Values( 'INTO', '^[1-9][0-9]*$ ')
GO
INSERT  Configuracion(IdValor , Valor) Values( 'CORREO', '^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$')
GO
INSERT  Configuracion(IdValor , Valor) Values( 'URL', '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \?=.-]*)*\/?$/')
GO
INSERT  Configuracion(IdValor , Valor) Values( 'TmeOutServicioMiliSegundos', '5000')
GO
INSERT  Configuracion(IdValor , Valor) Values( 'BuscarRespuestaMiliSegundos', '500')
GO
INSERT  Configuracion(IdValor , Valor, TipoDato) Values( 'TimeOutServicios', '2', 'INT')
GO
INSERT  Configuracion(IdValor , Valor, TipoDato) Values( 'MaximoEnvioPaquetes', '20000000', 'INT')
GO

--Colsubsidio
	--INSERT  Configuracion(IdValor , Valor) Values( 'BuscarImpresoraCOLSUBSIDIO', '1')
	--GO
	--TUYA
	--INSERT  Configuracion(IdValor , Valor) Values( 'BuscarImpresoraTUYA', '0')
	--GO


-- Notificacion por correo
	/*INSERT  Configuracion(IdValor , Valor) Values( 'CorreoNotificacion', 'aroldan@idenpla.com.co')
	GO
	INSERT  Configuracion(IdValor , Valor) Values( 'CorreoFrom', 'aquevedo@idenpla.com.co')
	GO
	INSERT  Configuracion(IdValor , Valor) Values( 'ContrasenaCorreo', '1234')
	GO
	INSERT  Configuracion(IdValor , Valor) Values( 'PuertoCorreo', '7701')
	GO*/
--

--Servicios Configuracion BPOASNET
	--INSERT  Configuracion(IdValor , Valor) Values( 'UrlServiceAsNet', 'http://10.52.1.165:8009/ASBroadcastCardsWS/services/EmisionTarjetasImplPort?WSDL')
	--GO
	--
	--INSERT  Configuracion(IdValor , Valor) Values( 'UserNameAsNet', 'pruebascol')
	--GO
	--INSERT  Configuracion(IdValor , Valor) Values( 'PasswordAsNet', 'asnet2016/*')
	--GO
--Colsubsidio
	--INSERT  Configuracion(IdValor , Valor) Values( 'UserColsubsidio', 'DANTE')
	--GO
	--INSERT  Configuracion(IdValor , Valor) Values( 'PasswordColsubsidio', 'Nasugo')
	--GO
--TUYA
	--INSERT  Configuracion(IdValor , Valor) Values( 'UserTUYA', 'FFRNANDOVIDALOLMOS')
	--GO
	--INSERT  Configuracion(IdValor , Valor) Values( 'PassTUYA', 'c47a980ebefb1fef7a469ba4ececdfbe3459247eb5b4fbb88ec2737f720dafbc19cc64b0e00bfba0b872eaf723e7f83ce1e4eb9796dc4742a81b7a4978057af0')
	--GO

--Configuracion segura
--Colsubsidio
	--INSERT  ConfiguracionSegura(IdValorS , Valor)
	--Values( 'ResBPOTUYA', '0')
	--GO
--TUYA
	--INSERT  ConfiguracionSegura(IdValorS , Valor)
	--Values( 'ResBPOCOLSUBSIDIO', '1')
	--GO
--CMS ASNET
	--INSERT  Configuracion(IdValor , Valor)Values( 'EncodingASNET', 'ISO-8859-1')
	--GO
-- Santander Puerto rico
	INSERT  Configuracion(IdValor , Valor) Values( 'FechaConciliacionSantander', '')
	GO
	INSERT  Configuracion(IdValor , Valor) Values( 'HoraConciliacionSantander', '19')--HORA MILITAR
	GO
	INSERT  Configuracion(IdValor , Valor) Values( 'MinutoConciliacionSantander', '50')
	GO
	INSERT  Configuracion(IdValor , Valor)Values( 'RutaArchivoConciliacion', 'C:\Archivo\Concilia')
	go
	INSERT  Configuracion(IdValor , Valor)Values( 'ValoresCabecera', '0004-1-9007')--Entidad-tiporeg-canal
	go
	INSERT  Configuracion(IdValor , Valor)Values( 'ValoresSumario', '0004-3')--Entidad-tiporeg
	go
	INSERT  ConfiguracionSegura(IdValorS , Valor)Values( 'ValoresPredefinidos', '0')
	go
	INSERT  ConfiguracionSegura(IdValorS , Valor)
	Values( 'ParametrosBaseSANTANDER', '<Parametros>
								 <EntidadBancaria inicio="0" final="0" tamano="0" valorDefinido="0004"></EntidadBancaria> 
								 <Producto inicio="0" final="5" tamano="6" valorDefinido=""></Producto>
								 <Usuario inicio="74" final="82" tamano="8" valorDefinido=""/>
								 <CANAL inicio="0" final="0" tamano="0" valorDefinido="9007"></CANAL>
								 <TIPOREG inicio="0" final="0" tamano="0" valorDefinido="1"/>
								 <CODOFIUMO  inicio="6" final="9" tamano="4" valorDefinido=""></CODOFIUMO>
								 <CENALTA  inicio="59" final="62" tamano="4" valorDefinido=""></CENALTA>
								 <PAN  inicio="41" final="58" tamano="19" valorDefinido=""></PAN></Parametros>')
	GO
	INSERT  Configuracion(IdValor , Valor)Values( 'EncodingSANTANDER', 'IBM037')
	GO
	INSERT  Configuracion(IdValor , Valor) Values( 'MostrarTrama', '0')
	GO
-- Delete from Configuracion
--Select * from Proceso
-- Select * from EstadoTransaccion order by IdTransaccion desc
--select * from LogAplicacion order by Fecha desc
--Procesos
INSERT INTO Proceso (NombreProceso) VALUES ('Enviar request')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('Crear Session')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('Cerrar Session')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('Analizar Respuesta')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('Hacer tarjeta')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('Eliminar impresi�n tarjeta')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('Mensajes de Evento')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('Consultar Estado de CardDevice')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('WinSockServer Proceso')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('Notificar Proceso')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('Tomar clientes')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('Convertir entidades a WS externo')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('Convertir  WS externo a entidades')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('Operacion Ws externo')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('Reintento volver a poner  colar')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('Ingreso Configurador')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('Obtener valores configurador')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('Actualizar valores configurador')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('WS Expuesto')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('WS ASNET')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('WS Notificacion')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('WS Obtener Chip')
GO
INSERT INTO Proceso (NombreProceso) VALUES ('Conciliar diario')
GO
--INSERT INTO Proceso (NombreProceso) VALUES ('Conciliar Transacciones')--24 Borrar a futuro
--GO

-- select * from proceso
-- Valores para pruebas
INSERT ProductoXCW(Cliente, Producto, CardName) Values('PruebaIntecsus', '1', 'Credit Card Smart')
GO
INSERT ProductoXCW(Cliente, Producto, CardName) Values('ARCHIVO', '1', 'Credit Card Smart')
GO
INSERT  Impresora(NombreEnviado , PCName, Ip) Values( 'WIN-4H1I0FLTUGA', 'WIN-4H1I0FLTUGA', '192.3.2.1')
GO

-- Mensajes
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'0', N'Impresora no lista', N'1561')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'100', N'Inserte tarjeta de exce', N'501')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'101', N'Inserte tarjeta leer', N'502')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'102', N'Inserte tarjeta', N'503')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'103', N'Inserte tarjeta rPIN', N'504')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'104', N'Imposible re-PIN', N'505')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'105', N'PIN incorrecto', N'506')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'106', N'Fallo calculo PIN', N'507')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'107', N'Tarjeta pos banda', N'508')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'108', N'Insertar tarjeta rtop', N'509')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'109', N'Error codificaci�n', N'510')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'110', N'Error del apilador.', N'511')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'111', N'Imposible rePIN', N'512')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'112', N'Tarjeta pos chip', N'513')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'113', N'Imposible rePIN', N'514')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'114', N'Imposible codificar', N'515')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'141', N'Error stepper', N'1526')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'1410', N'Interlock abierto.', N'1535')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'1411', N'Topper atorado', N'1536')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'1412', N'Error de apilador.', N'1537')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'1413', N'Limite embo excedidos.', N'1538')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'1414', N'Falla comunicaci�n.', N'1539')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'1415', N'Falla comunicaci�n.', N'1540')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'1416', N'Falla comunicaci�n.', N'1541')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'1417', N'Error lectura precod', N'1542')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'1418', N'Error emparej datos', N'1543')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'1419', N'Bandeja sin tarjetas', N'1544')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'142', N'Error rueda estepero', N'1527')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'1420', N'Estado desconocido', N'1545')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'1421', N'Error de sistema.', N'1546')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'1422', N'Error cinta indent', N'1547')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'1423', N'Maquina  desconectada', N'1548')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'1424', N'Se agot� el tiempo.', N'1549')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'1425', N'Error lectura tarjet', N'1550')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'1426', N'Error verif tarjeta', N'1551')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'1427', N'Operaci�n cancelada', N'1552')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'1428', N'Error gen�rico', N'1553')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'143', N'L�mina topper baja.', N'1528')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'144', N'Cinta OCR baja.', N'1529')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'145', N'Error alimentaci�n.', N'1530')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'146', N'Error de banda mag', N'1531')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'147', N'Apila rechazo lleno.', N'1532')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'148', N'Apila salida lleno.', N'1533')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'149', N'Error de memoria.', N'1534')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'2', N'Impresora no lista', N'1561')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'205', N'No se encuentra producto configurado', N'1561')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'23', N'No usar cola produc', N'516')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'24', N'Sesi�n clnte caducado', N'517')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'25', N'Sin cnxion servidor', N'518')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'300', N'Error gen�rico.', N'519')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'5', N'Impresora no lista', N'1561')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'7001', N'No se puede Obtener valores de configuracion', N'1553')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'7002', N'No Datos Requeridos', N'ERR02')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'7003', N'Error obtener Peticion WS Cliente', N'1553')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'7005', N'Error almacenar peticion en la cola', N'1553')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'7006', N'Error obtener informacion HSM ', N'1553')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'7007', N'Error enviar notificacion WS externo ', N'1553')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'7008', N'Long Reg Invalida', N'ERR01')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'7009', N'Formato Inval Datos', N'ERR03')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'870', N'SuperoReintentos', N'600')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'90', N'Completo sin errores', N'000')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'91', N'Errores en rePIN', N'520')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'92', N'Solicitud eliminada', N'521')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'93', N'Solicitud eliminada', N'522')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'94', N'Solicitud encolada', N'523')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'95', N'Solicitud con error', N'524')
INSERT [MensajesRespuesta] ([CodigoMensaje], [Mensaje], [CodigoCliente]) VALUES (N'96', N'Solicitud con error', N'525')
GO


SET IDENTITY_INSERT [ConversionMensaje] ON 
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (1, N'Smart card chip programming error ', N'Error prog de chip.', N'1554')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (2, N'Error communicating with printer.', N'Error de conexi�n', N'1555')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (3, N'Printer errors: Duplex failed: Code: 102 - Card did not reach the required position. ', N'Error pos Duplex', N'1556')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (4, N'Printer errors: Submit magstripe data failed: Code: 102 - Card did not reach the required position. Submit magstripe data failed: Code: 102 - Card did not reach the required position. ', N'Error pos banda', N'1557')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (5, N'Printer errors: Pick card failed: Code: 112 - Input hopper is empty.', N'Bandeja sin tarjetas', N'1558')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (6, N'Printer errors: Pick card failed: Code: 111 - Card has failed to pick.', N'No pudo tomar tarjet', N'1559')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (7, N'Printer errors: Submit monochrome data failed: Code: 109 - Print ribbon has a problem.', N'Error cinta impresio', N'1560')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (8, N'Printer errors: Pick card failed: Code: 102 - Card did not reach the required position. ', N'Tarj no alc pos req.', N'1556')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (9, N'Impresora desconectada, apagada, no lista (Not Ready)', N'Impresora no lista', N'1561')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (10, N'Printer not ready to print. Printer status: (Idle Suspended Locked)', N'Impresora no lista', N'1562')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (11, N'Printer errors: Submit magstripe data failed: Code: 105 - MagStripeEncode action had a problem reading the card.', N'Error grabar banda', N'1563')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (12, N'Card Request Stranded in Active Queue Exception (Unknown Card Stock Usage) Exception Message: Card request stranded in active queue error with CMC Device=3', N'Error srvc impresi�n', N'1564')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (13, N'Printer errors: Start job failed: Code: 9 - Job is being inhibited by external events.  failed: Code: 0 - Job finished successfully.  failed: Code: 0 - Job finished successfully.', N'Error en el job', N'1565')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (14, N'Printer errors: Move to smart card failed: Code: 102 - Card did not reach the required position. ', N'Error pos en chip', N'1566')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (15, N'Error returned when sending card to smartcard', N'Error m�dulo chip', N'1567')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (16, N'Printer errors: Start job failed: Code: 102 - Card did not reach the required position.', N'Error pos al inicio', N'1568')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (17, N'Printer not ready to print. Printer status: (Busy Online Locked)', N'Impresora no lista', N'1569')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (18, N'Printer is not locked, requested action can not start.', N'Impresora no cerrada', N'1570')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (19, N'Printer errors: Submit monochrome data failed: Code: 110 - Print ribbon is missing or out of material.', N'Sin cinta de impres', N'1571')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (20, N'Printer errors: Pick card failed: Code: 109 - Print ribbon has a problem.', N'Error cinta impresio', N'1572')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (21, N'Printer errors: Move to smart card failed: Code: 114 - Cover opened during job. Move to smart card failed: Code: 114 - Cover opened during job.', N'Cubierta abierta', N'1573')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (22, N'Error communicating with printer.. See log for more information.', N'Error de conexi�n', N'1574')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (23, N'Card Request Stranded in Active Queue Exception (Unknown Card Stock Usage) Exception Message: Card request stranded in active queue error with CMC Device=2002', N'Soliciud varada col', N'1575')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (24, N'Printer errors: End job failed: Code: 109 - Print ribbon has a problem. End job failed: Code: 100 - Request sent relates to an existing job and is not supported by the printer.', N'Error cinta impresio', N'1576')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (25, N'Printer errors: Start job failed: Code: 109 - Print ribbon has a problem.', N'Error cinta impresio', N'1577')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (26, N'Printer errors: Submit monochrome data failed: Code: 102 - Card did not reach the required position. ', N'Error pos tarjeta', N'1578')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (27, N'Printer errors: Submit Topcoat data failed: Code: 100 - Request sent relates to an existing job and is not supported by the printer. Move to smart card failed: Code: 102 - Card did not reach the required position.', N'Error pos en chip', N'1579')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (28, N'Printer errors: Duplex failed: Code: 100 - Request sent relates to an existing job and is not supported by the printer. Move to smart card failed: Code: 102 - Card did not reach the required position.', N'Error pos Duplex', N'1580')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (29, N'Job completion error - job status: Failed', N'Sin completar impr', N'1581')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (30, N'Printer errors: Duplex failed: Code: 100 - Request sent relates to an existing job and is not supported by the printer. Duplex failed: Code: 102 - Card did not reach the required position.', N'Error pos Duplex', N'1582')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (31, N'Printer errors: Submit Topcoat data failed: Code: 109 - Print ribbon has a problem.', N'Error lamin superior', N'1583')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (32, N'Printer errors: Submit monochrome data failed: Code: 114 - Cover opened during job. ', N'Impresora abierta', N'1584')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (33, N'Card Request Stranded in Active Queue Exception (Unknown Card Stock Usage) Exception Message: Card request stranded in active queue error with CMC Device=2006', N'Impresora varada col', N'1585')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (34, N'Printer errors: Move to smart card failed: Code: 114 - Cover opened during job.', N'Impresora abierta', N'1586')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (35, N'Printer errors: Submit magstripe data failed: Code: 106 - MagStripeEncode action found no data on the card.', N'Error grabar banda', N'1587')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (36, N'Card Request Stranded in Active Queue Exception (Unknown Card Stock Usage) Exception Message: Card request stranded in active queue error with CMC Device=2019', N'Impresi�n varada col', N'1588')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (37, N'Printer errors: Submit magstripe data failed: Code: 102 - Card did not reach the required position. ', N'Tarj no posicion req', N'1589')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (38, N'Printer errors: Submit magstripe data failed: Code: 122 - MagStripeEncode action has data that does not match the configuration.', N'MagStriEnc datos inv', N'1590')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (39, N'Printer does not support magnetic stripe.', N'Banda mag no soport', N'1591')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (42, N'Printer errors: Pause card or cancel failed: Code: 114 - Cover opened during job. ', N'Cubierta abier job', N'1592')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (44, N'Printer errors: Submit emboss/indent data failed: Code: 136 - Emboss/indent error: ribbon is broken. ', N'Emb/ind cinta rota', N'1593')
INSERT [ConversionMensaje] ([IdConversion], [MensajeCW], [Mensaje], [CodigoCliente]) VALUES (45, N'Printer errors: Start job failed: Code: 132 - The embosser output hopper is full. ', N'Hopper esta lleno', N'1594')
SET IDENTITY_INSERT [ConversionMensaje] OFF
GO
-- Secuencias
CREATE SEQUENCE dbo.SecuenciaID
    START WITH 1
    INCREMENT BY 1 ;
GO