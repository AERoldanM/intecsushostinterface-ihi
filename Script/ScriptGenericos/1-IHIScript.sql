-- =================================================================================
-- Autor:		Andr�s Rold�n
-- Fecha Creaci�n:	26-02-2016
-- Nombre Proyecto:	IHI
-- Descripci�n:	Creaci�n Objetos, SP e inserciones base del IHI
-- =================================================================================
CREATE TABLE Proceso(
IdProceso [int] IDENTITY(1,1) NOT NULL,
NombreProceso [nvarchar](100) NOT NULL
CONSTRAINT [PK_Proceso] PRIMARY KEY  
(
	IdProceso ASC
))
GO


CREATE TABLE LogAplicacion(
IdLog [int] IDENTITY(1,1) NOT NULL,
IdProceso [int] NOT NULL,
Fecha DateTime Not Null,
IdTransaccion [int] NOT NULL,
Evento [int] NOT NULL,
DescripcionEvento varchar(MAX) not null,
Cliente VarChar(80)  null,
CONSTRAINT [PK_IdLog] PRIMARY KEY  
(
	IdLog ASC
))
GO

ALTER TABLE LogAplicacion  WITH CHECK ADD  CONSTRAINT [FK_Log_Proceso] FOREIGN KEY(IdProceso)
REFERENCES Proceso (IdProceso)
GO

--select * from LogAplicacion order by Fecha desc
CREATE TABLE Configuracion(
IdValor nvarchar(50) NOT NULL,
Valor nvarchar(max) NOT NULL,
TipoDato nvarchar(50) NULL
CONSTRAINT [PK_IdValor] PRIMARY KEY  
(
	IdValor ASC
))
GO


CREATE TABLE ConfiguracionSegura(
IdValorS nvarchar(50) NOT NULL,
Valor nvarchar(max) NOT NULL
CONSTRAINT [PK_IdValors] PRIMARY KEY  
(
	IdValorS ASC
))
GO




CREATE TABLE ProcesosXAprobar(
IdProcesoXRealizar  [int] IDENTITY(1,1) NOT NULL,
IdProceso [int] NOT NULL,
Fecha DateTime Not Null,
Informacion nvarchar(MAX) Not Null,
CantidadReIntentos int  NOT Null,
IdTransaccion int null,
Estado bit NOT NULL,
CONSTRAINT [PK_IdProcesoXRealizar] PRIMARY KEY  
(
	IdProcesoXRealizar ASC
))
GO

ALTER TABLE ProcesosXAprobar  WITH CHECK ADD  CONSTRAINT [FK_ProcesosXAprobar_Proceso] FOREIGN KEY(IdProceso)
REFERENCES Proceso (IdProceso)
GO

CREATE TABLE Impresora(
IdImpresora  [int] IDENTITY(1,1) NOT NULL,
NombreEnviado nvarchar(80) Not Null,
PCName nvarchar(80) Not Null,
Ip nvarchar(30) Not Null,
CONSTRAINT [PK_IdImpresora] PRIMARY KEY  
(
	IdImpresora ASC
))
GO

CREATE TABLE ProductoXCW(
IdProductoXCW  [int] IDENTITY(1,1) NOT NULL,
Cliente nvarchar(80) Not Null,
Producto nvarchar(80) Not Null,
CardName nvarchar(80) Not Null,
XmlPeticion nvarchar(max)  Null,
Tamano int not null default 0,
CONSTRAINT [PK_ProductoXCW] PRIMARY KEY  
(
	IdProductoXCW ASC
))
GO

CREATE TABLE EstadoTransaccion(
IdTransaccion [int] IDENTITY(1,1) NOT NULL,
Cliente varchar(80) NOT NULL,
IdTransaccionC varchar(80) Not Null,
Estado varchar(100) NOT NULL,
FinalNotificacion bit  null,
Peticion varchar(max)  NULL,
NumeroNotificacion varchar(max) null,
InfoSha varchar(max) null,
IDChip varchar(max) null,
Fecha datetime null,
MensajeNotificar bit default 0,
CONSTRAINT [PK_EstadoTransaccion] PRIMARY KEY  
(
	IdTransaccion ASC
))
GO

CREATE TABLE Conciliacion(
IDConciliacion int IDENTITY(1,1) NOT NULL,
IdTransaccion [int]  NOT NULL,
Cliente varchar(80) NOT NULL,
IDTransaccionCliente varchar(80) Not Null,--Clav OPeracion
TipoRegistro varchar(100) NULL,
CodigoEntidad varchar(100) NULL,
PAN varchar(max)  NULL,
CODFUMO varchar(max) null,
Canal varchar(max) null,
INDES varchar(max) null,--OK o NOK
Usuario varchar(max) null,
Fecha datetime null,
CodMen varchar(max) null,
Descripcion1 varchar(max) null,
Descripcion2 varchar(max) null,
Pin bit not null default 0,
CONSTRAINT [PK_Conciliacion] PRIMARY KEY  
(
	IDConciliacion ASC
))
GO

ALTER TABLE Conciliacion  WITH CHECK ADD  CONSTRAINT [FK_ConciliacionXEstadoTransaccion] FOREIGN KEY(IdTransaccion)
REFERENCES EstadoTransaccion (IdTransaccion)
GO

CREATE TABLE TransaccionExitosaWS(
IdTransaccionExitosaWS [int] IDENTITY(1,1) NOT NULL,
Cliente varchar(80) NOT NULL,
IdTransaccionC varchar(80) Not Null,
Peticion varchar(max) NOT NULL,
Respuesta varchar(max) not  null,
NumeroNotificacion varchar(max) null,
CONSTRAINT [PK_TransaccionExitosaWS] PRIMARY KEY  
(
	IdTransaccionExitosaWS ASC
))
GO

CREATE TABLE MensajesRespuesta(
CodigoMensaje [varchar](4) NOT NULL,
Mensaje [varchar](255) NOT NULL,
CodigoCliente [varchar](10) NOT NULL,
CONSTRAINT [PK_MensajesRespuesta] PRIMARY KEY  
(
	CodigoMensaje ASC
))
GO

CREATE TABLE ConversionMensaje(
IdConversion [int] IDENTITY(1,1) NOT NULL,
MensajeCW [varchar](max) NOT NULL,
Mensaje [varchar](255) NOT NULL,
CodigoCliente [varchar](10) NOT NULL,
CONSTRAINT [PK_ConversionMensaje] PRIMARY KEY  
(
	IdConversion ASC
))
GO

-- Select * from EstadoTransaccion



-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 26-11-2015
-- Description:	Actualiza la Info Sha del estado de la transaccion
-- exec SP_RetornarSecuencia
-- =============================================
CREATE PROCEDURE SP_RetornarSecuencia
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT NEXT VALUE FOR dbo.SecuenciaID AS Numero
END
GO

--select * from Proceso
-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 27-01-2015
-- Description:	Crea una impresora en el sistema
-- exec SP_InsertImpresora 'Impresora1', 'PCPrueba', '102.123.456.789'
-- =============================================
CREATE PROCEDURE SP_InsertImpresora
	@NombreEnviado nvarchar(80),
	@PCName nvarchar(80),
	@Ip nvarchar(30)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT  Impresora(NombreEnviado , PCName, Ip)
	Values( @NombreEnviado, @PCName, @Ip)
END
GO


-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 27-01-2015
-- Description:	Actualiza una impresora en el sistema
-- exec SP_UpdateImpresora '1', 'Impresora1', 'PCPrueba', '102.123.456.789'
-- =============================================
CREATE PROCEDURE SP_UpdateImpresora
	@IdImpresora int,
	@NombreEnviado nvarchar(80),
	@PCName nvarchar(80),
	@Ip nvarchar(30)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Update  Impresora
	SET NombreEnviado = @NombreEnviado , PCName = @PCName, Ip = @Ip
	WHERE IdImpresora = @IdImpresora
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 27-01-2015
-- Description:	OBTIENE TODAS LAS Impresoras creadas en el sistema
-- exec SP_ObtenerImpresora 
-- =============================================
CREATE PROCEDURE SP_ObtenerImpresora
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT IdImpresora, NombreEnviado, PCName, Ip
	FROM  Impresora
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 27-01-2015
-- Description:	Obtiene la impresoraXId 
-- exec SP_ObtenerImpresoraXID 1
-- =============================================
CREATE PROCEDURE SP_ObtenerImpresoraXID
	@IdImpresora int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT IdImpresora, NombreEnviado, PCName, Ip
	FROM  Impresora
	WHERE IdImpresora = @IdImpresora
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 25-03-2015
-- Description:	Obtiene la impresoraXNombreEnviado 
-- exec SP_ObtImprXNombre 1
-- =============================================
CREATE PROCEDURE SP_ObtImprXNombre
	@NombreEnviado Varchar(80)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT IdImpresora, PCName, Ip
	FROM  Impresora
	WHERE NombreEnviado = @NombreEnviado
END
GO
-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 27-01-2015
-- Description:	Elimina una  impresora creada 
-- exec SP_DeleteImpresora 1
-- =============================================
CREATE PROCEDURE SP_DeleteImpresora
	@IdImpresora int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Delete  Impresora
	WHERE IdImpresora = @IdImpresora
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 26-01-2015
-- Description:	Inserta algun valor de la configuracion
-- exec SP_InsertValor 'CantReintentos', '3'
-- =============================================
CREATE PROCEDURE SP_InsertValor
	@IdValor nvarchar(50),
	@Valor nvarchar(max),
	@TipoDato nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT  Configuracion(IdValor , Valor, TipoDato)
	Values( @IdValor, @Valor, @TipoDato)
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 26-01-2015
-- Description:	Obtiene algun valor de la configuracion
-- exec SP_ObtenerValor 'CantReintentos'
-- =============================================
CREATE PROCEDURE SP_ObtenerValor
	@IdValor nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Select Valor
	From  Configuracion
	WHERE IdValor = @IdValor
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 07-04-2015
-- Description:	Obtener todos los valores de la configuracion
-- exec SP_ObtenerConfiguracion 
-- =============================================
CREATE PROCEDURE SP_ObtenerConfiguracion
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Select IdValor, Valor, TipoDato
	From  Configuracion
	ORDER BY IdValor DESC
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 26-01-2015
-- Description:	Modifica algun valor de la configuracion
-- exec SP_UpdateValor 'CantReintentos', '3'
-- =============================================
CREATE PROCEDURE SP_UpdateValor
	@IdValor nvarchar(50),
	@Valor nvarchar(max),
	@TipoDato nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE Configuracion
	SET Valor = @Valor, TipoDato = @TipoDato
	WHERE IdValor = @IdValor
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 26-01-2015
-- Description:	Elimina algun valor de la configuracion
-- exec SP_DeleteConfiguracion 'CantReintentos'
-- =============================================
CREATE PROCEDURE SP_DeleteConfiguracion
	@IdValor nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DELETE Configuracion
	WHERE IdValor = @IdValor
END
GO


---- =============================================
---- Author:		Andr�s Roldan
---- Create date: 26-01-2015
---- Description:	IngresarUnaExcepcion
---- exec SP_InsertExcepcion 'Prueba', 'Prueba.SP' , 'Notihing'
---- =============================================
--CREATE PROCEDURE SP_InsertExcepcion
--	@MensajeErro nvarchar(MAX),
--	@Transaccion nvarchar(40)  ,
--	@MensajeConcreto nvarchar(MAX)
--AS
--BEGIN
--	-- SET NOCOUNT ON added to prevent extra result sets from
--	-- interfering with SELECT statements.
--	SET NOCOUNT ON;

--   INSERT INTO ErrorAplicacion (Fecha  ,MensajeErro ,Transaccion  ,MensajeConcreto)
--     VALUES (GETDATE()   , @MensajeErro ,@Transaccion  , @MensajeConcreto);
--END
--GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 26-01-2015
-- Description:	Ingresar un proceso no realizado
-- exec SP_InsertProceso 1 , 'Notihing', '222'
-- =============================================
CREATE PROCEDURE SP_InsertProceso
	@IdProceso int,
	@Informacion nvarchar(MAX),
	@IdTransaccion int
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Proceso int
	set @Proceso = 0
	SELECT @Proceso = IdTransaccion
	FROM ProcesosXAprobar
	WHERE IdProceso = @IdProceso AND IdTransaccion = @IdTransaccion
	IF(@Proceso <= 0)
	BEGIN
		 INSERT INTO ProcesosXAprobar (IdProceso  ,Fecha ,Informacion  ,CantidadReIntentos, IdTransaccion, Estado)
		VALUES (@IdProceso, GETDATE()   , @Informacion ,0, @IdTransaccion, 0);
	END
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 26-01-2015
-- Description:	Aumentar el numero de intentos realizados
-- exec SP_AumentarIntentos 1
-- =============================================
CREATE PROCEDURE SP_AumentarIntentos
	@IdProcesoXRealizar int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Intentos int
	set @Intentos = (SELECT CantidadReIntentos 
					FROM ProcesosXAprobar
					WHERE IdProcesoXRealizar = @IdProcesoXRealizar)
	
	UPDATE ProcesosXAprobar
	SET CantidadReIntentos = @Intentos + 1
	WHERE IdProcesoXRealizar = @IdProcesoXRealizar
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 26-01-2015
-- Description:	Eliminar proceso por realizar
-- exec SP_DeleteProceso 1
-- =============================================
CREATE PROCEDURE SP_DeleteProceso
	@IdProcesoXRealizar int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DELETE ProcesosXAprobar
	WHERE IdProcesoXRealizar = @IdProcesoXRealizar
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 26-01-2015
-- Description:	Obtener procesos por realizar
-- exec SP_ObtenerProcesosXProcesar 
-- =============================================
CREATE PROCEDURE SP_ObtenerProcesosXProcesar
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Intentos int
	set @Intentos = (SELECT  CONVERT(int, Valor)  
					FROM Configuracion
					WHERE IdValor = 'CantReintentos')
	
	SELECT IdProcesoXRealizar, IdProceso, Informacion, Fecha, IdTransaccion,Estado
	FROM ProcesosXAprobar
	WHERE CantidadReIntentos < @Intentos 
	Order By Fecha Desc
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 26-01-2015
-- Description:	Obtener procesos por realizar por proceso
-- exec SP_ObtenerProcesosXIDProceso 1 
-- =============================================
CREATE PROCEDURE SP_ObtenerProcesosXIDProceso
	@IdProceso int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @Intentos int
	set @Intentos = (SELECT  CONVERT(int, Valor)  
					FROM Configuracion
					WHERE IdValor = 'CantReintentos')
	
	SELECT IdProcesoXRealizar, Informacion, Fecha, IdTransaccion, Estado
	FROM ProcesosXAprobar
	WHERE CantidadReIntentos < @Intentos and IdProceso = @IdProceso
	Order By Fecha Desc

END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 17-03-2015
-- Description:	Obtener procesos por realizar por idTransaccion
-- exec SP_ObtenerProcesosXIDProceso 1 
-- =============================================
CREATE PROCEDURE SP_ObtenerProcesosXIDTransaccion
	@IdTransaccion int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT IdProcesoXRealizar, Informacion, Fecha, IdTransaccion
	FROM ProcesosXAprobar
	WHERE IdTransaccion = @IdTransaccion
	Order By Fecha Desc
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 17-03-2015
-- Description:	Obtener procesos por realizar 
-- exec SP_ObtenerProcesos 
-- =============================================
CREATE PROCEDURE SP_ObtenerProcesos
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT IdProcesoXRealizar,IdProceso, IdTransaccion, Informacion, Fecha, CantidadReIntentos
	FROM ProcesosXAprobar
	Order By Fecha Desc
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 07-04-2015
-- Description:	actualizar intentos  por idProcesoXrealizar
-- exec SP_UpdateCantidadIntentos 1 , 1
-- =============================================
CREATE PROCEDURE SP_UpdateCantidadIntentos
	@IdProcesoXRealizar int,
	@Intentos int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE ProcesosXAprobar
	SET  CantidadReIntentos  = @Intentos 
	FROM ProcesosXAprobar
	WHERE  IdProcesoXRealizar  = @IdProcesoXRealizar
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 17-03-2015
-- Description:	actualizar estado  por idTransaccion
-- exec SP_UpdateEstadoXIDTransaccion 1 , 1
-- =============================================
CREATE PROCEDURE SP_UpdateEstadoXIDTransaccion
	@IdTransaccion int,
	@Estado bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE ProcesosXAprobar
	SET  Estado = @Estado 
	FROM ProcesosXAprobar
	WHERE  IdTransaccion  = @IdTransaccion
END
GO
-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 26-01-2015
-- Description:	Actualizar procesos por realizar
-- exec SP_UpdateProceso 1,1, 'nothing' 
-- =============================================
CREATE PROCEDURE SP_UpdateProceso
	@IdProcesoXRealizar int, 
	@IdProceso int, 
	@Informacion nvarchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE ProcesosXAprobar
	SET  IdProceso = @IdProceso , Informacion = @Informacion
	FROM ProcesosXAprobar
	WHERE IdProcesoXRealizar = @IdProcesoXRealizar
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 08-04-2015
-- Description:	Obtiene los ultimos cien registros del log
-- exec SP_ObtenerLog 
-- =============================================
CREATE PROCEDURE SP_ObtenerLog
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT TOP 100 IdLog,IdProceso, Fecha, IdTransaccion, Evento, DescripcionEvento
	FROM LogAplicacion
	ORDER BY Fecha DESC
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 08-04-2015
-- Description:	Obtiene los registros de una transaccion en el log
-- exec SP_ObtenerLogXTransaccion 0, '211prueba.txt','',''
-- =============================================
CREATE PROCEDURE SP_ObtenerLogXTransaccion
 @IDtransaccion int,
 @TransaccionCLi varchar(80),
 @FechaInicio DateTime,
 @FechaFin DateTime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	If (@TransaccionCLi IS NOT NULL)
		SELECT *
		FROM LogAplicacion LA
		INNER JOIN EstadoTransaccion ET ON ET.IdTransaccion = LA.IdTransaccion
		WHERE ET.IdTransaccionC = @TransaccionCLi OR (ET.Fecha BETWEEN @FechaInicio AND  DATEADD(day, 1, @FechaFin))
	ELSE 
		SELECT LA.IdLog,LA.IdProceso, LA.Fecha, LA.IdTransaccion, LA.Evento, LA.DescripcionEvento
		FROM LogAplicacion LA
		LEFT JOIN EstadoTransaccion ET ON ET.IdTransaccion = LA.IdTransaccion
		WHERE LA.IdTransaccion = @IDtransaccion OR ET.IdTransaccionC = @TransaccionCLi OR (ET.Fecha BETWEEN @FechaInicio AND DATEADD(day, 1, @FechaFin))
		ORDER BY Fecha DESC


END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 27-01-2015
-- Description:	Regista una operacion de la aplicacion en la tabla
-- exec SP_RegistrarLogCliente 1, 1, 1,'Prueba', 'colsubsidio'
-- =============================================
CREATE PROCEDURE SP_RegistrarLogCliente
	@IdProceso int,
	@IdTransaccion [int],
	@Evento [int],
	@DescripcionEvento varchar(MAX),
	@Cliente varchar(80)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT LogAplicacion(IdProceso, Fecha, IdTransaccion, Evento, DescripcionEvento, cliente)
	Values(@IdProceso, GETDATE(), @IdTransaccion,  @Evento, @DescripcionEvento, @Cliente)
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 27-01-2015
-- Description:	Regista una operacion de la aplicacion en la tabla
-- exec SP_RegistrarLog 1, 1, 1,'Prueba'
-- =============================================
CREATE PROCEDURE SP_RegistrarLog
	@IdProceso int,
	@IdTransaccion [int],
	@Evento [int],
	@DescripcionEvento varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT LogAplicacion(IdProceso, Fecha, IdTransaccion, Evento, DescripcionEvento)
	Values(@IdProceso, GETDATE(), @IdTransaccion,  @Evento, @DescripcionEvento)
END
GO


-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 30-01-2015
-- Description:	Regista un producto del cliente con respecto a CW
-- exec SP_InsertProductoXCW 'Prieba','MasterGold','Prueba'
-- =============================================
CREATE PROCEDURE SP_InsertProductoXCW
	@Cliente nvarchar(80),
	@Producto nvarchar(80),
	@CardName nvarchar(80)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT ProductoXCW(Cliente, Producto, CardName)
	Values(@Cliente, @Producto, @CardName)
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 30-01-2015
-- Description:	Obtiene todos los productos regitrados
-- exec SP_ObtenerProductoXCW
-- =============================================
CREATE PROCEDURE SP_ObtenerProductoXCW
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT IdProductoXCW, Cliente, Producto, CardName
	FROM ProductoXCW
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 30-01-2015
-- Description:	Obtiene  producto regitrados por cliente y producto
-- exec SP_ObtProducXClienXProd
-- Modificacion 20/07/2015
-- AERM -> devuelve peticion XML
-- =============================================
CREATE PROCEDURE SP_ObtProducXClienXProd
	@Cliente nvarchar(80),
	@Producto nvarchar(80)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT IdProductoXCW, Cliente, Producto, CardName, XmlPeticion
	FROM ProductoXCW
	where Cliente = @Cliente and Producto = @Producto
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 30-01-2015
-- Description:	Actualiza  producto 
-- exec SP_UpdateProductoXCW 1, 'Prueba', 'MasterCard gold' , 'Prueba'
-- =============================================
CREATE PROCEDURE SP_UpdateProductoXCW
	@IdProductoXCW int,
	@Cliente nvarchar(80),
	@Producto nvarchar(80),
	@CardName nvarchar(80)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE ProductoXCW
	SET Cliente = @Cliente , Producto = @Producto, CardName = @CardName
	where IdProductoXCW = @IdProductoXCW 
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 30-01-2015
-- Description:	Elimina  producto un producto
-- exec SP_DeleteProductoXCW 1
-- =============================================
CREATE PROCEDURE SP_DeleteProductoXCW
	@IdProductoXCW int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	DELETE ProductoXCW
	where IdProductoXCW = @IdProductoXCW 
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 30-01-2015
-- Description:	Regista una transaccion  del cliente
-- exec SP_InsertTransaccion 'Prieba','1212323','0'
-- =============================================
CREATE PROCEDURE SP_InsertTransaccion
	@Cliente varchar(80),
	@IdTransaccionC varchar(80),
	@Estado varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT EstadoTransaccion(Cliente, IdTransaccionC, Estado, FinalNotificacion, Fecha)
	Values(@Cliente, @IdTransaccionC, @Estado, 0, GETDATE())
	SELECT @@IDENTITY As Identificador;
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 30-01-2015
-- Description:	actualiza el estado de una transaccion 
-- exec SP_UpdateTransaccion 1,'Prueba2'
-- =============================================
CREATE PROCEDURE SP_UpdateTransaccion
	@IdTransaccion int,
	@Estado varchar(100),
	@FinalNotificacion bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Update  EstadoTransaccion 
	SET Estado = @Estado, FinalNotificacion = @FinalNotificacion
	WHERE IdTransaccion = @IdTransaccion
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 30-01-2015
-- Description:	Obtiene todas las transacciones 
-- exec SP_ObtenerTransaccion
-- =============================================
CREATE PROCEDURE SP_ObtenerTransaccion
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Select IdTransaccion, Cliente, IdTransaccionC, Estado
	FROM  EstadoTransaccion f
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 25-03-2015
-- Description:	Obtiene estado de la transaccion XIDdela transaccion
-- exec SP_ObtenerTransaccionXID 1
-- =============================================
CREATE PROCEDURE SP_ObtenerTransaccionXID
	@IdTransaccion int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Select Cliente, IdTransaccionC, Estado
	FROM  EstadoTransaccion 
	WHERE IdTransaccion = @IdTransaccion
END
GO



-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 08-04-2015
-- Description:	Obtiene los estados de las transacciones
-- exec SP_ObtenerEstadosTransacciones
-- =============================================
CREATE PROCEDURE SP_ObtenerEstadosTransacciones
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT TOP 300 IdTransaccion, Cliente,IdTransaccionC,Estado
	FROM EstadoTransaccion 
	ORDER BY IdTransaccion DESC
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 08-04-2015
-- Description:	Obtiene los estados de las transacciones POR iDTRANSACCION
-- O TRANSACCIONcLIENTE
-- exec SP_ObtenerTranXID
-- =============================================
CREATE PROCEDURE SP_ObtenerTranXID
 @IDtransaccion int,
 @TransaccionCLi varchar(80)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT IdTransaccion, Cliente,IdTransaccionC,Estado
	FROM EstadoTransaccion
	where IdTransaccion = @IDtransaccion OR IdTransaccionC = @TransaccionCLi 
END
GO


-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 08-04-2015
-- Description:	Actualiza la contrase�a de un usuario 
-- exec SP_ActualizarUsuario 'Admin', 'QQBkAG0AaQBuAA=='
-- =============================================
CREATE PROCEDURE SP_ActualizarUsuario
	@UserID nvarchar(100),
	@Contrasena nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE Usuario
	SET Contrasena =  @Contrasena
	WHERE UserID = @UserID
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 20-04-2015
-- Description:	Obtiene los datos del log por paginacion
-- exec SP_ObtenerDatosLog 10, 1
-- =============================================
CREATE PROCEDURE SP_ObtenerDatosLog
	@CantidadRegistro int,
	@Pagina int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	  SELECT IdLog, IdProceso, Fecha, IdTransaccion, Evento, DEscripcionEvento 
	  FROM (
	  SELECT IdLog, IdProceso, Fecha, IdTransaccion, Evento, DEscripcionEvento, 
	  ROW_NUMBER() OVER (ORDER BY Fecha desc, IdLog) AS RowNumber 
	  FROM  LogAplicacion) AS Tabla
	  WHERE RowNumber BETWEEN @Pagina - 1 * @CantidadRegistro + 1  AND @CantidadRegistro * (@Pagina)
	  Order By Fecha desc
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 22-05-2015
-- Description:	Busca si existe un registro con notificacion ya realizada
-- exec SP_ObtTransXNotificaicon '1','0'
-- =============================================
CREATE PROCEDURE SP_ObtTransXNotificaicon
	@IdTransaccion int,
	@FinalNotificacion bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT IdTransaccion, Cliente, IdTransaccionC, Estado
	FROM  EstadoTransaccion
	WHERE @IdTransaccion = IdTransaccion AND MensajeNotificar = @FinalNotificacion
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 13-08-2015
-- Description:	Crea una respuesta WS 
-- exec SP_InsertImpresora 'Impresora1', 'PCPrueba', '102.123.456.789'
-- =============================================
CREATE PROCEDURE SP_InsertTrasaccion
	@Cliente varchar(80),
	@IdTransaccionC varchar(80),
	@Peticion varchar(max),
	@Respuesta varchar(max) ,
	@NumeroNotificacion varchar(max) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT  TransaccionExitosaWS(Cliente , IdTransaccionC, Peticion, Respuesta, NumeroNotificacion)
	Values( @Cliente, @IdTransaccionC, @Peticion, @Respuesta, @NumeroNotificacion)
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 13-08-2015
-- Description:	Consulta una respuesta WS 
-- exec SP_SelectTrasaccion 'COLSUBSIDIO', 'Prueba02'
-- =============================================
CREATE PROCEDURE SP_SelectTrasaccion
	@Cliente varchar(80),
	@IdTransaccionC varchar(80)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT Peticion, Respuesta, NumeroNotificacion
	FROM   TransaccionExitosaWS
	WHERE Cliente = @Cliente AND  IdTransaccionC = @IdTransaccionC
	IF @@ROWCOUNT >= 1
	BEGIN
	exec SP_DeleteTrasaccion @Cliente, @IdTransaccionC
	END
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 13-08-2015
-- Description:	Consulta una respuesta WS 
-- exec SP_DeleteTrasaccion 'Impresora1', 'PCPrueba'
-- =============================================
CREATE PROCEDURE SP_DeleteTrasaccion
	@Cliente varchar(80),
	@IdTransaccionC varchar(80)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Delete   TransaccionExitosaWS
	WHERE Cliente = @Cliente AND  IdTransaccionC = @IdTransaccionC
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 24-08-2015
-- Description:	Almacena valor de la peticion y numero notificacion
-- exec SP_InsertRespuestaTransaccion '1', 'PCPrueba', '102.123.456.789'
-- =============================================
CREATE PROCEDURE SP_InsertRespuestaTransaccion
	@IdTransaccion int,
	@Peticion varchar(max),
	@NumeroNotificacion varchar(max) 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE EstadoTransaccion
	SET Peticion = @Peticion, NumeroNotificacion = @NumeroNotificacion
	WHERE IdTransaccion = @IdTransaccion
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 24-08-2015
-- Description:	oBTENER valor de la peticion y numero notificacion
-- exec SP_ObtenerRespuestaTransaccion '1'
-- =============================================
CREATE PROCEDURE SP_ObtenerRespuestaTransaccion
	@IdTransaccion int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT Peticion, NumeroNotificacion
	FROM EstadoTransaccion
	WHERE IdTransaccion = @IdTransaccion
END
GO
-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 27-08-2015
-- Description:	Obtiene los mensajes respuesta
-- exec SP_ObtenerMensajeRespuesta '1'
-- =============================================
CREATE PROCEDURE SP_ObtenerMensajeRespuesta
	@CodigoMensaje varchar(4)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT Mensaje, CodigoCliente
	FROM MensajesRespuesta
	WHERE CodigoMensaje = @CodigoMensaje
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 23-09-2015
-- Description:	Obtiene la informacion de la transacci�n y adicionalmente almacena un IDShip
-- exec SP_IngresarIDChip 'ABC1','CHIPPRUEBA'
-- =============================================
CREATE PROCEDURE SP_IngresarIDChip
	@InfoSha varchar(max),
	@IDChip varchar(max),
	@Cliente varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE  EstadoTransaccion 
	SET IDChip  = @IDChip
	WHERE InfoSha = @InfoSha

	Select IdTransaccion, Cliente, IdTransaccionC, Estado, FinalNotificacion
	FROM  EstadoTransaccion
	WHERE InfoSha = @InfoSha and Cliente = @Cliente
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 23-09-2015
-- Description:	Actualiza la Info Sha del estado de la transaccion
-- exec SP_ActualizarSHA 1, 'ABC1'
-- =============================================
CREATE PROCEDURE SP_ActualizarSHA
	@IdTransaccion int,
	@InfoSha varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	UPDATE  EstadoTransaccion 
	SET InfoSha = @InfoSha
	WHERE IdTransaccion = @IdTransaccion

END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 16-12-2015
-- Description:	Obtiene la conversionde los mensajes de CW
-- Y del cliente
-- exec SP_ObtenerConvMensajeRespuesta 'Example'
-- =============================================
CREATE PROCEDURE SP_ObtenerConvMensajeRespuesta
	@CodigoMensaje varchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT Mensaje, CodigoCliente
	FROM ConversionMensaje
	WHERE MensajeCW = @CodigoMensaje
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 30-03-2016
-- Description:	Inserta algun valor de la configuracion Segura
-- exec SP_InsertValorS 'ResBPO', '1'
-- =============================================
CREATE PROCEDURE SP_InsertValorS
	@IdValor nvarchar(50),
	@Valor nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT  ConfiguracionSegura(IdValorS , Valor)
	Values( @IdValor, @Valor)
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 30-03-2016
-- Description:	Obtiene algun valor de la configuracion SEGURA
-- exec SP_ObtenerValorS 'ResBPO'
-- =============================================
CREATE PROCEDURE SP_ObtenerValorS
	@IdValor nvarchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Select Valor
	From  ConfiguracionSegura
	WHERE IdValorS = @IdValor
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 04-04-2016
-- Description:	actualiza el estado de una transaccion para la notificacion
-- exec SP_UpdateTransaccion 1,'Prueba2'
-- =============================================
CREATE PROCEDURE [SP_UpdateNotificacion]
	@IdTransaccion int,
	@MensajeNotificar bit
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	Update  EstadoTransaccion 
	SET  MensajeNotificar = @MensajeNotificar
	WHERE IdTransaccion = @IdTransaccion
END
GO
-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 27-06-2016
-- Description:	Inserta valor iniciar en la tabla conciliacion
-- exec SP_InsertConciliacion 'ResBPO', '1'
-- =============================================
CREATE PROCEDURE SP_InsertConciliacion
	@IdTransaccion int,
	@Cliente varchar(80),
	@IDTransaccionCliente varchar(80),
	@TipoRegistro varchar(100), 
	@CodigoEntidad varchar(100),
	@PAN varchar(MAX), 
	@CODFUMO varchar(MAX), 
	@Usuario varchar(MAX), 
	@Canal varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	INSERT INTO Conciliacion(IdTransaccion, Cliente, IDTransaccionCliente, TipoRegistro, CodigoEntidad, PAN, CODFUMO, Usuario, Canal, Fecha)
     VALUES ( @IdTransaccion, @Cliente, @IDTransaccionCliente, @TipoRegistro, @CodigoEntidad, @PAN, @CODFUMO, @Usuario, @Canal, GETDATE())
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 28-06-2016
-- Description:	actualiza valores  en la tabla conciliacion para una transaccion
-- exec SP_UpdateConciliacion 4065, 'OK', 'ASAS', 'pRUIEBA'
-- =============================================
CREATE PROCEDURE SP_UpdateConciliacion
	@IdTransaccion int,
	@INDES varchar(MAX),
	@CodMen varchar(MAX),
	@Descripcion1 varchar(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE Conciliacion
	SET INDES = @INDES, CodMen = @CodMen, Descripcion1 = @Descripcion1
	WHERE IdTransaccion = @IdTransaccion
	
END
GO


-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 14-07-2016
-- Description:	Obtiene una conciliacion por PAN
-- EXEC SP_ObtenerConciliacionXSHA 7891, 'SANTANDER'
-- =============================================
CREATE PROCEDURE SP_ObtenerConciliacionXSHA
@PANSHA varchar(max),
@Cliente varchar(max)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT TOP 1 E.Idtransaccion, C.IDTransaccionCliente, C.PAN, C.CODFUMO, C.CANAL, C.INDES, C.Usuario, C.Fecha, C.CodMen, C.Descripcion1, C.Pin
	FROM Conciliacion AS C
	INNER JOIN EstadoTransaccion AS E on C.IdTransaccion = E.IdTransaccion 
	WHERE E.InfoSha  = @PANSHA AND INDES = 'OK'--AND C.Pin = 0 AND INDES = 'OK'
	ORDER BY C.Fecha DESC
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 05-07-2016
-- Description:	Obtiene una conciliacion por idtransaccion del IHI
-- exec SP_ObtenerConciliacionXID 4065
-- =============================================
CREATE PROCEDURE SP_ObtenerConciliacionXID
@IdTransaccion int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT Cliente, IDTransaccionCliente, TipoRegistro, CodigoEntidad, PAN, CODFUMO, CANAL, INDES, Usuario, Fecha, CodMen, Descripcion1
	FROM Conciliacion
	WHERE IdTransaccion = @IdTransaccion 
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 06-07-2016
-- Description:	Obtiene las conciliaciones almacenadas por un d�a
-- exec SP_ObtenerConciliacionXDIA 
-- =============================================
CREATE PROCEDURE SP_ObtenerConciliacionXDIA
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @FechaIni as DateTime
	DECLARE @FechaFin as DateTime
	Set @FechaIni = DATEADD(dd,DATEDIFF(dd,0,GETDATE()),0)
	Set  @FechaFin = DATEADD(ms,-3,DATEADD(dd,DATEDIFF(dd,0,GETDATE()),1))
	SELECT IDConciliacion, IdTransaccion, Cliente, IDTransaccionCliente, TipoRegistro, CodigoEntidad, PAN, CODFUMO, 
		 CANAL, INDES, Usuario, Fecha, CodMen, Descripcion1, Descripcion2
	FROM Conciliacion
	WHERE INDES Is NOT NULL and (fecha >= @FechaIni and fecha <= @FechaFin)
END
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 08-07-2016
-- Description:	Obtiene los tamanos de todos los productos y clientes
-- exec SP_ObtenerTamanoProductos 
-- =============================================
CREATE PROCEDURE SP_ObtenerTamanoProductos
AS
BEGIN
	SET NOCOUNT ON;
	SELECT Cliente, Producto, Tamano
	FROM ProductoXCW
END
GO





-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 14-07-2016
-- Description:	Metodo encargado actualizar la asignacion de pin en conciliacion
-- exec SP_ChangeAsigPIN 12
-- =============================================
CREATE PROCEDURE SP_ChangeAsigPIN
@Idtransaccion int
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE Conciliacion
	SET PIN = 1
	WHERE IdTransaccion  = @Idtransaccion 
END
GO