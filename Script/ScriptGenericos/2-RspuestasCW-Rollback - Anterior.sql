CREATE TABLE [dbo].Estados(
IdEstado [int] NOT NULL,
NombreEstado [nvarchar](100) NOT NULL
CONSTRAINT [PK_Estadp] PRIMARY KEY  
(
	IdEstado ASC
))
GO

CREATE TABLE [dbo].CodigosRespuestaXEstado(
IdCodRespesta [int] identity(1,1) NOT NULL,
IdMessageType VARCHAR(max)  NOT NULL,
IdDataInteger VARCHAR(max)  NULL,
IdEstado [int] NOT NULL
CONSTRAINT [PK_CodigosRespuestaXEstado] PRIMARY KEY  
(
	IdCodRespesta ASC
))
GO
ALTER TABLE [dbo].CodigosRespuestaXEstado  WITH CHECK ADD  CONSTRAINT [FK_Estado_CodRespuesta] FOREIGN KEY(IdEstado)
REFERENCES [dbo].Estados (IdEstado)
GO

CREATE TABLE [dbo].EstadoImpresoraXCW(
IdCodRespesta [int] identity(1,1) NOT NULL,
ReturnCode VARCHAR(max)  NOT NULL,
DeviceType VARCHAR(max)  NULL,
DeviceStatus  VARCHAR(max)  NULL,
IdEstado [int] NOT NULL
CONSTRAINT [PK_CodigosRespuestaXEstadoImpresoraXCW] PRIMARY KEY  
(
	IdCodRespesta ASC
))
GO
ALTER TABLE [dbo].EstadoImpresoraXCW  WITH CHECK ADD  CONSTRAINT [FK_EstadoImpresora_CodRespuesta] FOREIGN KEY(IdEstado)
REFERENCES [dbo].Estados (IdEstado)
GO

/* INSERTAMOS LOS ESTADOS EXACTAMENTE IGUAL A COMO ESTA EN EL CODIGO*/
INSERT Estados(IdEstado, NombreEstado) VALUES (0,'ST_OK');
INSERT Estados(IdEstado, NombreEstado) VALUES (1,'ST_EN_PROCESO');
INSERT Estados(IdEstado, NombreEstado) VALUES (2,'ST_OCUPADA');
INSERT Estados(IdEstado, NombreEstado) VALUES (3,'ST_ERROR');
INSERT Estados(IdEstado, NombreEstado) VALUES (4,'ST_ERROR_DELETE');
--NUEVOS ESTADOS PARA IMPRIMIR TARJETA
INSERT Estados(IdEstado, NombreEstado) VALUES (5,'ST_NOPROCESO');
INSERT Estados(IdEstado, NombreEstado) VALUES (6,'ST_ROLLBACK');
--ESTADOS PARA ESTADOXPCNAME
INSERT Estados(IdEstado, NombreEstado) VALUES (101,'OK');
INSERT Estados(IdEstado, NombreEstado) VALUES (102,'OCUAPDA O NO AUMENTAR REINTENTOS');
INSERT Estados(IdEstado, NombreEstado) VALUES (103,'AGREGAR A REINTENTOS');
GO


/*CODIGOS Y SU RESPUESTA SEGUN EL ESTADO
---- PARTIENDO DEL MESSAGETYPE  -----------
	CPS_CHANGE	' 0 card production request status change
	CMS_CHANGE	' 1 CMC Device/card machine status change
	MESSAGE	' 2 user Message
	TRACK_DATA	' 3 Mag Stripe track data
	PIN_OFFSET	' 4 "DataString" contains the PIN offset for the new or repinned card
	CMC_CHANGE	' 5 CMC Controller device status change (only for Enterprise Server)
	CMCQ_CHANGE	' 6 CMC Device queue count change (only for Enterprise Server)
	PQ_CHANGE	' 7 Production queue change (only for Enterprise Server)
	CMCD_CHANGE	' 8 CMC Device status change (only for Enterprise. used by Admin component only.)
	CPR_FINAL_STATUS	' 9 Final Status for card request (only for Enterprise)
	CUSTOM_MESSAGE	'10 message from CWcustom component
	OP_ASSISTANCE	'11 operator assistance message
	CARD_NUMBER	'12 card number (PAN) (if auto assigned by server)
	SUPER_CMC_CHANGE	'13 Super CMC Controller device status change (only for Enterprise Server)
	PRODUCTION_ERROR	'14 Production error (used for HOST sessions)
*/
/*
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('0','',5);--CPS_CHANGE
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('1','',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('2','',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('3','',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('4','',1);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('5','',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('6','',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('7','',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('8','',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('9','',5);--FINALSTATUS
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('10','',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('11','',5);--OP_ASSISTANCE
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('12','',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('13','',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('14','',4);*/

/*CPS_CHANGE  + DATAINTEGER VALOR */
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('0', '0', 0); -- Para el estado final cuando se elimina impresion
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('0', '100', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('0', '101', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('0', '102', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('0', '103', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('0', '104', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('0', '105', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('0', '106', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('0', '107', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('0', '108', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('0', '109', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('0', '110', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('0', '111', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('0', '112', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('0', '113', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('0', '114', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('0', '*', 3);--Valor defecto

/*OP_ASSISTANCE + DATAINTEGER VALOR */
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('11', '100', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('11', '101', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('11', '102', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('11', '103', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('11', '104', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('11', '105', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('11', '106', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('11', '107', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('11', '108', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('11', '109', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('11', '110', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('11', '111', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('11', '112', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('11', '113', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('11', '114', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('11', '*', 3);--Valor defecto

/*FINALSTATUS + DATAINTEGER VALOR */
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('9', '0', 0);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('9', '1', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('9', '2', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('9', '3', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('9', '4', 1);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('9', '5', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('9', '6', 3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('9', '*', 3);--VALOR DEFECTO

/*Otros estados valor defecto */
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('1','*',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('2','*',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('3','*',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('4','*',1);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('5','*',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('6','*',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('7','*',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('8','*',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('10','*',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('12','*',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('13','*',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('14','*',4);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('*', '*', 3);--VALOR DEFECTO GENERAL-- es necesario este estado
GO

/* EstadosXPC NAME RESPUESTAS
-- 101,'OK'
--102,'OCUAPDA O NO AUMENTAR REINTENTOS'
--103,'AGREGAR A REINTENTOS'
*/
INSERT EstadoImpresoraXCW(ReturnCode, DeviceType, DeviceStatus, IdEstado) VALUES ('0', '16', '1', 101);
INSERT EstadoImpresoraXCW(ReturnCode, DeviceType, DeviceStatus, IdEstado) VALUES ('0', '16', '0', 103);
INSERT EstadoImpresoraXCW(ReturnCode, DeviceType, DeviceStatus, IdEstado) VALUES ('0', '16', '2', 103);
INSERT EstadoImpresoraXCW(ReturnCode, DeviceType, DeviceStatus, IdEstado) VALUES ('0', '16', '3', 103);
INSERT EstadoImpresoraXCW(ReturnCode, DeviceType, DeviceStatus, IdEstado) VALUES ('0', '16', '4', 102);
INSERT EstadoImpresoraXCW(ReturnCode, DeviceType, DeviceStatus, IdEstado) VALUES ('0', '16', '5', 103);
INSERT EstadoImpresoraXCW(ReturnCode, DeviceType, DeviceStatus, IdEstado) VALUES ('1', '', '', 103);
INSERT EstadoImpresoraXCW(ReturnCode, DeviceType, DeviceStatus, IdEstado) VALUES ('*', '*', '*', 102);--Defecto si no encuentra -- es necesarioooo
GO

-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 24-05-2016
-- Description:	Obtiene el estado de una respuesta dada por CW recibida por el IHI en el EVENTMESSAGE
-- exec SP_EstadoXCodigoRespuestaCW '0', '11'
-- =============================================
CREATE PROCEDURE SP_EstadoXCodigoRespuestaCW
	@IdMessageType nvarchar(max),
	@IdDataInteger nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT IdEstado
	FROM CodigosRespuestaXEstado
	WHERE IdMessageType  = @IdMessageType and IdDataInteger = @IdDataInteger
END
GO


-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 24-05-2016
-- Description:	Obtiene todos los estados de las respuestas dadas por CW
-- exec SP_EstadosRespuestaCW 
-- =============================================
CREATE PROCEDURE SP_EstadosRespuestaCW
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT IdMessageType, IdDataInteger, IdEstado
	FROM CodigosRespuestaXEstado
END
GO


-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 09-06-2016
-- Description:	Obtiene el estado de una respuesta dada por CW recibida por el IHI en el ESTADOXPCNAME
-- exec SP_EstadoXPCNAME '0', '11'
-- =============================================
CREATE PROCEDURE SP_EstadoXPCNAME
	@ReturnCode nvarchar(max),
	@DeviceType nvarchar(max),
	@DeviceStatus nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT IdEstado
	FROM EstadoImpresoraXCW
	WHERE ReturnCode  = @ReturnCode and DeviceType  = @DeviceType and DeviceStatus = @DeviceStatus
END
GO


-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 09-06-2016
-- Description:	Obtiene todos los estados de las respuestas dadas por CW para ESTADOXPCNAME
-- exec SP_EstadosPCNAMEXCW 
-- =============================================
CREATE PROCEDURE SP_EstadosPCNAMEXCW
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT ReturnCode, DeviceType, DeviceStatus, IdEstado
	FROM EstadoImpresoraXCW
END
GO
/*
------------ RollBack
*/
--Drop table CodigosRespuestaXEstado
--GO
--Drop table EstadoImpresoraXCW
--GO
--Drop table Estados
--GO
--DROP PROCEDURE SP_EstadosRespuestaCW
--GO
--DROP PROCEDURE SP_EstadoXCodigoRespuestaCW
--GO
--DROP PROCEDURE SP_EstadosPCNAMEXCW
--GO
--DROP PROCEDURE SP_EstadoXPCNAME
--GO