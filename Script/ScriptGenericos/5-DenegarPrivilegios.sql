use Integrador;
-- Tablas de De parametros CW
-- pARA EL USUARIO DE SOPORTE
DENY SELECT ON OBJECT::[dbo].[ProductoXCW] TO soporteconsulta;
GO
DENY UPDATE ON OBJECT::[dbo].[ProductoXCW] TO soporteconsulta;
GO
DENY DELETE ON OBJECT::[dbo].[ProductoXCW] TO soporteconsulta;
GO
DENY INSERT ON OBJECT::[dbo].[ProductoXCW] TO soporteconsulta;
GO
--pARA EL USUARIO DEL IHI
DENY UPDATE ON OBJECT::[dbo].[ProductoXCW] TO IHI;
GO
DENY DELETE ON OBJECT::[dbo].[ProductoXCW] TO IHI;
GO
DENY INSERT ON OBJECT::[dbo].[ProductoXCW] TO IHI;
GO

-- Tabla configuracion Segura
-- pARA EL USUARIO DE SOPORTE
DENY SELECT ON OBJECT::[dbo].[ConfiguracionSegura] TO soporteconsulta;
GO
DENY UPDATE ON OBJECT::[dbo].[ConfiguracionSegura] TO soporteconsulta;
GO
DENY DELETE ON OBJECT::[dbo].[ConfiguracionSegura] TO soporteconsulta;
GO
DENY INSERT ON OBJECT::[dbo].[ConfiguracionSegura] TO soporteconsulta;
GO
--pARA EL USUARIO DEL IHI
DENY UPDATE ON OBJECT::[dbo].[ConfiguracionSegura] TO IHI;
GO
DENY DELETE ON OBJECT::[dbo].[ConfiguracionSegura] TO IHI;
GO
DENY INSERT ON OBJECT::[dbo].[ConfiguracionSegura] TO IHI;
GO

/* ROLLBACK ----
-- pARA EL USUARIO DE SOPORTE
GRANT SELECT ON OBJECT::[dbo].[ProductoXCW] TO soporteconsulta;
GO
GRANT UPDATE ON OBJECT::[dbo].[ProductoXCW] TO soporteconsulta;
GO
GRANT DELETE ON OBJECT::[dbo].[ProductoXCW] TO soporteconsulta;
GO
GRANT INSERT ON OBJECT::[dbo].[ProductoXCW] TO soporteconsulta;
GO
--pARA EL USUARIO DEL IHI
GRANT UPDATE ON OBJECT::[dbo].[ProductoXCW] TO IHI;
GO
GRANT DELETE ON OBJECT::[dbo].[ProductoXCW] TO IHI;
GO
GRANT INSERT ON OBJECT::[dbo].[ProductoXCW] TO IHI;
GO
-- Tabla configuracion Segura
-- pARA EL USUARIO DE SOPORTE
GRANT SELECT ON OBJECT::[dbo].[ConfiguracionSegura] TO soporteconsulta;
GO
GRANT UPDATE ON OBJECT::[dbo].[ConfiguracionSegura] TO soporteconsulta;
GO
GRANT DELETE ON OBJECT::[dbo].[ConfiguracionSegura] TO soporteconsulta;
GO
GRANT INSERT ON OBJECT::[dbo].[ConfiguracionSegura] TO soporteconsulta;
GO
--pARA EL USUARIO DEL IHI
GRANT UPDATE ON OBJECT::[dbo].[ConfiguracionSegura] TO IHI;
GO
GRANT DELETE ON OBJECT::[dbo].[ConfiguracionSegura] TO IHI;
GO
GRANT INSERT ON OBJECT::[dbo].[ConfiguracionSegura] TO IHI;
GO
*/