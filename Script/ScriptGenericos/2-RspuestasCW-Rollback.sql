CREATE TABLE Estados(
IdEstado [int] NOT NULL,
NombreEstado [nvarchar](100) NOT NULL
CONSTRAINT [PK_Estadp] PRIMARY KEY  
(
	IdEstado ASC
))
GO

CREATE TABLE CodigosRespuestaXEstado(
IdCodRespesta [int] identity(1,1) NOT NULL,
IdMessageType VARCHAR(max)  NOT NULL,
IdDataInteger VARCHAR(max)  NULL,
IdEstado [int] NOT NULL
CONSTRAINT [PK_CodigosRespuestaXEstado] PRIMARY KEY  
(
	IdCodRespesta ASC
))
GO
ALTER TABLE CodigosRespuestaXEstado  WITH CHECK ADD  CONSTRAINT [FK_Estado_CodRespuesta] FOREIGN KEY(IdEstado)
REFERENCES Estados (IdEstado)
GO

CREATE TABLE EstadoImpresoraXCW(
IdCodRespesta [int] identity(1,1) NOT NULL,
ReturnCode VARCHAR(max)  NOT NULL,
DeviceType VARCHAR(max)  NULL,
DeviceStatus  VARCHAR(max)  NULL,
IdEstado [int] NOT NULL
CONSTRAINT [PK_CodigosRespuestaXEstadoImpresoraXCW] PRIMARY KEY  
(
	IdCodRespesta ASC
))
GO
ALTER TABLE EstadoImpresoraXCW  WITH CHECK ADD  CONSTRAINT [FK_EstadoImpresora_CodRespuesta] FOREIGN KEY(IdEstado)
REFERENCES Estados (IdEstado)
GO

/* INSERTAMOS LOS ESTADOS EXACTAMENTE IGUAL A COMO ESTA EN EL CODIGO*/
INSERT Estados(IdEstado, NombreEstado) VALUES (0,'ST_OK');
INSERT Estados(IdEstado, NombreEstado) VALUES (1,'ST_EN_PROCESO');
INSERT Estados(IdEstado, NombreEstado) VALUES (2,'ST_OCUPADA');
INSERT Estados(IdEstado, NombreEstado) VALUES (3,'ST_ERROR');
INSERT Estados(IdEstado, NombreEstado) VALUES (4,'ST_ERROR_DELETE');
--NUEVOS ESTADOS PARA IMPRIMIR TARJETA
INSERT Estados(IdEstado, NombreEstado) VALUES (5,'ST_NOPROCESO');
INSERT Estados(IdEstado, NombreEstado) VALUES (6,'ST_ROLLBACK');
--ESTADOS PARA ESTADOXPCNAME
INSERT Estados(IdEstado, NombreEstado) VALUES (101,'OK');
INSERT Estados(IdEstado, NombreEstado) VALUES (102,'OCUAPDA O NO AUMENTAR REINTENTOS');
INSERT Estados(IdEstado, NombreEstado) VALUES (103,'AGREGAR A REINTENTOS');
GO


/*CODIGOS Y SU RESPUESTA SEGUN EL ESTADO
---- PARTIENDO DEL MESSAGETYPE  -----------
	CPS_CHANGE	' 0 card production request status change
	CMS_CHANGE	' 1 CMC Device/card machine status change
	MESSAGE	' 2 user Message
	TRACK_DATA	' 3 Mag Stripe track data
	PIN_OFFSET	' 4 "DataString" contains the PIN offset for the new or repinned card
	CMC_CHANGE	' 5 CMC Controller device status change (only for Enterprise Server)
	CMCQ_CHANGE	' 6 CMC Device queue count change (only for Enterprise Server)
	PQ_CHANGE	' 7 Production queue change (only for Enterprise Server)
	CMCD_CHANGE	' 8 CMC Device status change (only for Enterprise. used by Admin component only.)
	CPR_FINAL_STATUS	' 9 Final Status for card request (only for Enterprise)
	CUSTOM_MESSAGE	'10 message from CWcustom component
	OP_ASSISTANCE	'11 operator assistance message
	CARD_NUMBER	'12 card number (PAN) (if auto assigned by server)
	SUPER_CMC_CHANGE	'13 Super CMC Controller device status change (only for Enterprise Server)
	PRODUCTION_ERROR	'14 Production error (used for HOST sessions)
*/
/*
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('0','',5);--CPS_CHANGE
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('1','',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('2','',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('3','',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('4','',1);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('5','',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('6','',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('7','',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('8','',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('9','',5);--FINALSTATUS
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('10','',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('11','',5);--OP_ASSISTANCE
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('12','',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('13','',3);
INSERT CodigosRespuestaXEstado(IdMessageType, IdDataInteger, IdEstado) VALUES ('14','',4);*/

/*CPS_CHANGE  + DATAINTEGER VALOR */
SET IDENTITY_INSERT [CodigosRespuestaXEstado] ON 

INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (1, N'0', N'0', 0)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (2, N'0', N'100', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (3, N'0', N'101', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (4, N'0', N'102', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (5, N'0', N'103', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (6, N'0', N'104', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (7, N'0', N'105', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (8, N'0', N'106', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (9, N'0', N'107', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (10, N'0', N'108', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (11, N'0', N'109', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (12, N'0', N'110', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (13, N'0', N'111', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (14, N'0', N'112', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (15, N'0', N'113', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (16, N'0', N'114', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (17, N'0', N'*', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (18, N'11', N'100', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (19, N'11', N'101', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (20, N'11', N'102', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (21, N'11', N'103', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (22, N'11', N'104', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (23, N'11', N'105', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (24, N'11', N'106', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (25, N'11', N'107', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (26, N'11', N'108', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (27, N'11', N'109', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (28, N'11', N'110', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (29, N'11', N'111', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (30, N'11', N'112', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (31, N'11', N'113', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (32, N'11', N'114', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (33, N'11', N'*', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (34, N'9', N'0', 0)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (35, N'9', N'1', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (36, N'9', N'2', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (37, N'9', N'3', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (38, N'9', N'4', 1)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (39, N'9', N'5', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (40, N'9', N'6', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (41, N'9', N'*', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (42, N'1', N'*', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (43, N'2', N'*', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (44, N'3', N'*', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (45, N'4', N'*', 1)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (46, N'5', N'*', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (47, N'6', N'*', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (48, N'7', N'*', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (49, N'8', N'*', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (50, N'10', N'*', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (51, N'12', N'*', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (52, N'13', N'*', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (53, N'14', N'*', 4)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (54, N'*', N'*', 3)
INSERT [CodigosRespuestaXEstado] ([IdCodRespesta], [IdMessageType], [IdDataInteger], [IdEstado]) VALUES (55, N'14', N'28', 4)
SET IDENTITY_INSERT [CodigosRespuestaXEstado] OFF
GO
/* EstadosXPC NAME RESPUESTAS
-- 101,'OK'
--102,'OCUAPDA O NO AUMENTAR REINTENTOS'
--103,'AGREGAR A REINTENTOS'
*/
SET IDENTITY_INSERT [EstadoImpresoraXCW] ON 

INSERT [EstadoImpresoraXCW] ([IdCodRespesta], [ReturnCode], [DeviceType], [DeviceStatus], [IdEstado]) VALUES (1, N'0', N'16', N'1', 101)
INSERT [EstadoImpresoraXCW] ([IdCodRespesta], [ReturnCode], [DeviceType], [DeviceStatus], [IdEstado]) VALUES (2, N'0', N'16', N'0', 103)
INSERT [EstadoImpresoraXCW] ([IdCodRespesta], [ReturnCode], [DeviceType], [DeviceStatus], [IdEstado]) VALUES (3, N'0', N'16', N'2', 103)
INSERT [EstadoImpresoraXCW] ([IdCodRespesta], [ReturnCode], [DeviceType], [DeviceStatus], [IdEstado]) VALUES (4, N'0', N'16', N'3', 103)
INSERT [EstadoImpresoraXCW] ([IdCodRespesta], [ReturnCode], [DeviceType], [DeviceStatus], [IdEstado]) VALUES (5, N'0', N'16', N'4', 102)
INSERT [EstadoImpresoraXCW] ([IdCodRespesta], [ReturnCode], [DeviceType], [DeviceStatus], [IdEstado]) VALUES (6, N'0', N'16', N'5', 103)
INSERT [EstadoImpresoraXCW] ([IdCodRespesta], [ReturnCode], [DeviceType], [DeviceStatus], [IdEstado]) VALUES (7, N'1', N'', N'', 103)
INSERT [EstadoImpresoraXCW] ([IdCodRespesta], [ReturnCode], [DeviceType], [DeviceStatus], [IdEstado]) VALUES (8, N'*', N'*', N'*', 102)
INSERT [EstadoImpresoraXCW] ([IdCodRespesta], [ReturnCode], [DeviceType], [DeviceStatus], [IdEstado]) VALUES (9, N'0', N'17', N'1', 101)
INSERT [EstadoImpresoraXCW] ([IdCodRespesta], [ReturnCode], [DeviceType], [DeviceStatus], [IdEstado]) VALUES (10, N'0', N'17', N'0', 103)
INSERT [EstadoImpresoraXCW] ([IdCodRespesta], [ReturnCode], [DeviceType], [DeviceStatus], [IdEstado]) VALUES (11, N'0', N'17', N'2', 103)
INSERT [EstadoImpresoraXCW] ([IdCodRespesta], [ReturnCode], [DeviceType], [DeviceStatus], [IdEstado]) VALUES (12, N'0', N'17', N'3', 103)
INSERT [EstadoImpresoraXCW] ([IdCodRespesta], [ReturnCode], [DeviceType], [DeviceStatus], [IdEstado]) VALUES (13, N'0', N'17', N'4', 102)
INSERT [EstadoImpresoraXCW] ([IdCodRespesta], [ReturnCode], [DeviceType], [DeviceStatus], [IdEstado]) VALUES (14, N'0', N'17', N'5', 102)
SET IDENTITY_INSERT [EstadoImpresoraXCW] OFF
GO
-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 24-05-2016
-- Description:	Obtiene el estado de una respuesta dada por CW recibida por el IHI en el EVENTMESSAGE
-- exec SP_EstadoXCodigoRespuestaCW '0', '11'
-- =============================================
CREATE PROCEDURE SP_EstadoXCodigoRespuestaCW
	@IdMessageType nvarchar(max),
	@IdDataInteger nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT IdEstado
	FROM CodigosRespuestaXEstado
	WHERE IdMessageType  = @IdMessageType and IdDataInteger = @IdDataInteger
END
GO


-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 24-05-2016
-- Description:	Obtiene todos los estados de las respuestas dadas por CW
-- exec SP_EstadosRespuestaCW 
-- =============================================
CREATE PROCEDURE SP_EstadosRespuestaCW
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT IdMessageType, IdDataInteger, IdEstado
	FROM CodigosRespuestaXEstado
END
GO


-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 09-06-2016
-- Description:	Obtiene el estado de una respuesta dada por CW recibida por el IHI en el ESTADOXPCNAME
-- exec SP_EstadoXPCNAME '0', '11'
-- =============================================
CREATE PROCEDURE SP_EstadoXPCNAME
	@ReturnCode nvarchar(max),
	@DeviceType nvarchar(max),
	@DeviceStatus nvarchar(max)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT IdEstado
	FROM EstadoImpresoraXCW
	WHERE ReturnCode  = @ReturnCode and DeviceType  = @DeviceType and DeviceStatus = @DeviceStatus
END
GO


-- =============================================
-- Author:		Andr�s Roldan
-- Create date: 09-06-2016
-- Description:	Obtiene todos los estados de las respuestas dadas por CW para ESTADOXPCNAME
-- exec SP_EstadosPCNAMEXCW 
-- =============================================
CREATE PROCEDURE SP_EstadosPCNAMEXCW
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT ReturnCode, DeviceType, DeviceStatus, IdEstado
	FROM EstadoImpresoraXCW
END
GO
/*
------------ RollBack
*/
--Drop table CodigosRespuestaXEstado
--GO
--Drop table EstadoImpresoraXCW
--GO
--Drop table Estados
--GO
--DROP PROCEDURE SP_EstadosRespuestaCW
--GO
--DROP PROCEDURE SP_EstadoXCodigoRespuestaCW
--GO
--DROP PROCEDURE SP_EstadosPCNAMEXCW
--GO
--DROP PROCEDURE SP_EstadoXPCNAME
--GO