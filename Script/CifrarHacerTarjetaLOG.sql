DECLARE @Contador INT           -- Variable contador
DECLARE @CantidadRegistros INT  -- Cantidad de registros 
DECLARE @variables AS VARCHAR(max) 
DECLARE @IDTRANSACCION AS INT 
DECLARE @xml XML
DECLARE @PAN AS VARCHAR(MAX)
DECLARE @CVC2 AS VARCHAR(MAX)
DECLARE @TRACK1 AS VARCHAR(MAX)
DECLARE @TRACK2 AS VARCHAR(MAX)
DECLARE @RECORDDATA AS VARCHAR(MAX)
DECLARE @BARCODE AS VARCHAR(MAX)
DECLARE @CODIGOBARRAS1 AS VARCHAR(MAX)
DECLARE @NAME AS VARCHAR(MAX)
--TABLA DONDE SE ALMACENAN TODOS LOS REGISTROS
DECLARE @TableRegistros TABLE (id  int IDENTITY(1,1), IdTransaccion int,   DescripcionEvento varchar(max))
INSERT INTO @TableRegistros
SELECT L.IdTransaccion, L.DescripcionEvento
FROM LogAplicacion AS L
WHERE L.IdProceso = 5  
--SELECT * FROM  @TableRegistros
--RECORRER LOS REGISTROS
 SELECT @CantidadRegistros =  COUNT(IdTransaccion)
 FROM @TableRegistros

 SET @Contador = 1 
 WHILE @Contador <= @CantidadRegistros
 BEGIN
	SET @IDTRANSACCION = ''
	SET @variables = ''
	SET @xml = ''
	SET @PAN = ''
	SET @CVC2 = ''
	SET @TRACK1 = ''
	SET @TRACK2 = ''
	SET @RECORDDATA = ''
	SET @BARCODE = ''
	SET @CODIGOBARRAS1 = ''
	SET @NAME = ''
	SELECT @IDTRANSACCION = R.IdTransaccion, @variables = R.DescripcionEvento
    FROM @TableRegistros AS R
    WHERE R.id = @Contador 

	SET @variables =   REPLACE(REPLACE(@variables,'OperacionesBaseCW - HacerTarjeta :',''),'?','') 
	SET  @variables = REPLACE(REPLACE(@variables,'<xml version="1.0" encoding="utf-8" standalone="yes">',''), '</CW_XML_Interface>', '')
	SET @variables = REPLACE(@variables,'<CW_XML_Interface direction="Request" sequence="1">','')

	SELECT @xml = CONVERT(xml,@variables)
	SELECT  @PAN = N.x.value('(DATAITEM/text())[1]', 'varchar(max)') , @NAME = N.x.value('(DATAITEM/text())[2]','varchar(max)'),
			 @CVC2 = N.x.value('(DATAITEM/text())[5]', 'varchar(max)'),
			@TRACK1 =   N.x.value('(DATAITEM/text())[6]', 'varchar(max)'), @TRACK2 =   N.x.value('(DATAITEM/text())[7]', 'varchar(max)'),
			@RECORDDATA =   N.x.value('(DATAITEM/text())[8]', 'varchar(max)'),  @BARCODE =   N.x.value('(DATAITEM/text())[9]', 'varchar(max)') ,
			@CODIGOBARRAS1 =   N.x.value('(DATAITEM/text())[10]', 'varchar(max)')
	FROM @xml.nodes('METHOD') AS N(x)

	--SELECT @TRACK1, @TRACK2, @CODIGOBARRAS1, @BARCODE, @PAN, @CVC2, @RECORDDATA
	SET @variables  = REPLACE(@variables, @TRACK1,        '***************************************************************************')
	SET @variables  = REPLACE(@variables, @TRACK2,        '********************************')
	SET @variables  = REPLACE(@variables, @CODIGOBARRAS1, '********************************')
	SET @variables  = REPLACE(@variables, @NAME,		  '********************************')
	SET @variables  = REPLACE(@variables, @BARCODE,       '*******************')
	SET @variables  = REPLACE(@variables, @PAN,           '************' + SUBSTRING(@PAN, LEN(@PAN) - 3, 4))
	SET @variables  = REPLACE(@variables, @CVC2,          '***')
	SET @variables  = REPLACE(@variables, @RECORDDATA,    '***')
	--SELECT  @variables
	UPDATE LogAplicacion
	SET DescripcionEvento = 'OperacionesBaseCW - HacerTarjeta :' + @variables
	WHERE IdTransaccion = @IDTRANSACCION AND IdProceso = 5

	
	--SELECT TOP 1 L.IdTransaccion,  L.DescripcionEvento
	--FROM LogAplicacion AS L
	--WHERE L.IdProceso = 5 AND L.IdTransaccion = 1045

	SET @Contador = @Contador + 1
END
SELECT @CantidadRegistros AS PETICIONESHACERTARJETA

