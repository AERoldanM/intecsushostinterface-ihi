DECLARE @TableVar TABLE (id  int IDENTITY(1,1), ident varchar(20), fechaINI  datetime) 
DECLARE @tablaResul table(id varchar(20), tiempoDIFF varchar(max), fechaini datetime, fechafin datetime)
DECLARE @Contador INT
DECLARE @IDENTIFY VARCHAR(10) 
DECLARE @tIEMPOiNI dATETIME 
DECLARE @tIEMPOFIN dATETIME 
DECLARE @CantidadRegistros INT
set @Contador = 1
insert @TableVar
SELECT distinct substring(DescripcionEvento, charindex('-', DescripcionEvento) + 1, convert(bigint, charindex('-', right(DescripcionEvento, len(DescripcionEvento) - charindex('-', DescripcionEvento))) -1))  as ID , fecha
from LogAplicacion
where IdProceso = 19  and descripcionEvento LIKE '%Peticion -%' and (Fecha >= '2015-12-02 08:05:00.000' and fecha <= '2015-12-02 15:15:00.000')


--select *
--from @TableVar
SELECT @CantidadRegistros =  COUNT(id)
	FROM @TableVar

WHILE @Contador <= @CantidadRegistros
BEGIN

SELECT @IDENTIFY = ident, @tIEMPOiNI = fechaINI 
FROM @TableVar
WHERE ID = @Contador

SELECT  top 1 @tIEMPOFIN =  Fecha
FROM LogAplicacion
where IdProceso = 19 and descripcionEvento LIKE '%-' + @IDENTIFY + '-%' and (Fecha >= '2015-12-02 08:05:00.000' and fecha <= '2015-12-02 15:15:00.000')
order by Fecha desc

insert @tablaResul
values  (@IDENTIFY, DATEDIFF(MILLISECOND,@tIEMPOiNI, @tIEMPOFIN),@tIEMPOiNI, @tIEMPOFIN)
--insert @tablaResul 
--SELECT top 1 SUBSTRING(L.DescripcionEvento, 11, 3), DATEDIFF(day,l.fecha,@tIEMPOiNI), l.fecha, @tIEMPOiNI
--FROM LogAplicacion AS l
--where  IdProceso = 19 and DescripcionEvento LIKE '%-' + @IDENTIFY + '%'  
--order by Fecha desc

SET @Contador = @Contador + 1
END
select *, DATEDIFF(ss,fechaini, fechafin) as segundo,  DATEDIFF(mm,fechaini, fechafin) as minuto
from @tablaResul 
order by fechaini desc

SELECT  [IdLog]
      ,[IdProceso]
      ,[Fecha]
      ,[IdTransaccion]
      ,[Evento]
      ,[DescripcionEvento]
  FROM [dbo].[LogAplicacion]
 --WHERE DescripcionEvento LIKE '%' + @IDENTIFY + '%'
 where [IdProceso] = 19


 -- =============================================
-- Author:		Andr�s Roldan
-- Create date: 02-12-2015
-- Description:	genera valores diferencia
-- exec sp_procesoMilISegundo '2015-12-02 08:05:00.000', '2015-12-02 15:15:00.000'
-- =============================================
create procedure sp_procesoMilISegundo 
	@fechaini DATETIME,
	@fechaFin DATETIME
AS
BEGIN
	DECLARE @TableVar TABLE (id  int IDENTITY(1,1), ident varchar(20), fechaINI  datetime) 
	DECLARE @tablaResul table(id varchar(20), tiempoDIFF varchar(max), fechaini datetime, fechafin datetime)
	DECLARE @Contador INT
	DECLARE @IDENTIFY VARCHAR(10) 
	DECLARE @tIEMPOiNI dATETIME 
	DECLARE @tIEMPOFIN dATETIME 
	DECLARE @CantidadRegistros INT
	set @Contador = 1
	insert @TableVar
	SELECT distinct substring(DescripcionEvento, charindex('-', DescripcionEvento) + 1, convert(bigint, charindex('-', right(DescripcionEvento, len(DescripcionEvento) - charindex('-', DescripcionEvento))) -1))  as ID , fecha
	from LogAplicacion
	where IdProceso = 19  and descripcionEvento LIKE '%Peticion -%' and (Fecha >= @fechaini and fecha <= @fechaFin)


	--select *
	--from @TableVar
	SELECT @CantidadRegistros =  COUNT(id)
		FROM @TableVar

	WHILE @Contador <= @CantidadRegistros
	BEGIN
		SELECT @IDENTIFY = ident, @tIEMPOiNI = fechaINI 
		FROM @TableVar
		WHERE ID = @Contador

		SELECT  top 1 @tIEMPOFIN =  Fecha
		FROM LogAplicacion
		where IdProceso = 19 and descripcionEvento LIKE '%-' + @IDENTIFY + '-%' and (Fecha >= @fechaini and fecha <= @fechaFin)
		order by Fecha desc

		insert @tablaResul
		values  (@IDENTIFY, DATEDIFF(MILLISECOND,@tIEMPOiNI, @tIEMPOFIN),@tIEMPOiNI, @tIEMPOFIN)

	SET @Contador = @Contador + 1
	END
	SELECT *, DATEDIFF(ss,fechaini, fechafin) as segundo,  DATEDIFF(mm,fechaini, fechafin) as minuto
	from @tablaResul 
END

SELECT DATEDIFF(MILLISECOND , '2015-11-28 09:23:21.050','2015-11-28 09:23:31.093');
SELECT DATEDIFF(ss , '2015-11-28 09:23:21.050', '2015-11-28 09:23:31.093'  );