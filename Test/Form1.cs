﻿using Intecsus.CW;
using Intecsus.Encripcion;
using Intecsus.Entidades;
using Intecsus.OperarCMS;
using Intecusus.OperarBD;
using Intecsus.Conciliacion;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Intecsus.Seguridad;

namespace Test
{
  

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
           // string fdecha = DateTime.Now.ToString("yyyy-MM-dd");
            //CadenasTexto cadena = new CadenasTexto();
            //string resul = cadena.UltimosCadena("35456413454asadfsf2", 4);
            //IBLConciliacion conciliar = new BLConciliacion();
            //conciliar.InicializarVariables();
            //conciliar.ProcesoConciliar();
            //DateTime tiempo = DateTime.Now;
            //tiempo.Hour.ToString();
            ////Procesar proceso = new Procesar();
            ////int idpor = 6;
            ////proceso.EliminarTarjeta("1", "4065", true, ref idpor);
            ////AdaptadorSANTANDER adap = new AdaptadorSANTANDER();
            ////Peticion pet = new Peticion();
            ////pet.cliente = "Pueba";
            ////pet.idTransaccion = "PRUEBA CONCILIACION";
            ////pet.nombreLargo = "ANDRESPRUEBA una transaccion de mas caracteres de para probar una conciliancionasdsfrefgdscvdfscvfdsgvfgfgh";
            ////adap.ObtenerValoresConfiguracion(ref pet, 4065);
            // OperacionesBase operacionesBD = new OperacionesBase();
            // operacionesBD.ObtenerEstadosXCodigoCW(ConfigurationManager.AppSettings["BD"]);
          
        }

        private void btnCrifrar_Click(object sender, EventArgs e)
        {
            OperacionesBase operacionesBD = new OperacionesBase();
            try 
            {
                Intecsus.CW.Respuesta resp = new Intecsus.CW.Respuesta();

                var respuesta = resp.ProcesarMensage(operacionesBD.ObtenerValoresConfigurador("ArchivoPlano", ConfigurationManager.AppSettings["BD"]));
                IEncrypt interfaz = new FactoryMethod().DevolverClase(ConfigurationManager.AppSettings["TDES"]);
                Cifrar des = new Cifrar();
              string hexa = this.ToHexString(this.txtCifrar.Text);
              this.txtDescifrar.Text = interfaz.CifrarDato(hexa);
              MessageBox.Show(this.txtDescifrar.Text);
              operacionesBD.GuardarLogXNombreConectionString(14, 0, 1, "TEST btnCrifrar_Click - Operacion exitosa  recibe: " + this.txtCifrar.Text + " resultado " + this.txtDescifrar.Text,
                                                             ConfigurationManager.AppSettings["BD"]);
            }
            catch (Exception ex) 
            {
                operacionesBD.GuardarLogXNombreConectionString(14, 0, 2, "TEST btnCrifrar_Click - Operacion ERRONEA  " + ex.Message,
                                                             ConfigurationManager.AppSettings["BD"]);
            }
        }

        private void btnDesCifrar_Click(object sender, EventArgs e)
        {
            OperacionesBase operacionesBD = new OperacionesBase();
            try
            {
                IEncrypt interfaz = new FactoryMethod().DevolverClase(ConfigurationManager.AppSettings["TDES"]);
                Cifrar desEncript = new Cifrar();
                byte[] m_Data = desEncript.FromHex(interfaz.DescifrarDato(this.txtDescifrar.Text));
                Encoding m_iso = Encoding.GetEncoding(ConfigurationManager.AppSettings["Encodig"]);
                this.txtCifrar.Text = m_iso.GetString(m_Data);
                MessageBox.Show(m_iso.GetString(m_Data));

                operacionesBD.GuardarLogXNombreConectionString(14, 0, 1, "TEST btnDesCifrar_Click - Operacion exitosa  recibe: " + this.txtDescifrar.Text + " resultado " + this.txtCifrar.Text,
                                                                 ConfigurationManager.AppSettings["BD"]);
            }
            catch (Exception ex)
            {
                operacionesBD.GuardarLogXNombreConectionString(14, 0, 2, "TEST btnDesCifrar_Click - Operacion ERRONEA  " + ex.Message,
                                                             ConfigurationManager.AppSettings["BD"]);
            }
        }


        private string ToHexString(string str, int len = -1, bool spaces = false)
        {
            string hexOutput = string.Empty;
            //byte[] bytes =  System.Text.Encoding.UTF8.GetBytes(str);
            //byte[] bytes = Encoding.ASCII.GetBytes(str);
            Encoding m_iso = Encoding.GetEncoding(ConfigurationManager.AppSettings["Encodig"]);
            byte[] bytes = m_iso.GetBytes(str);
            Int32 len2 = bytes.Length;
            Int32 i;
            String s = string.Empty;
            for (i = 0; i <= len2 - 1; i++)
            {
                s += bytes[i].ToString("x2");
            }
            return s;
        }
    }
}
