﻿namespace Test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCifrar = new System.Windows.Forms.TextBox();
            this.txtDescifrar = new System.Windows.Forms.TextBox();
            this.btnCrifrar = new System.Windows.Forms.Button();
            this.btnDesCifrar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtCifrar
            // 
            this.txtCifrar.Location = new System.Drawing.Point(13, 27);
            this.txtCifrar.Name = "txtCifrar";
            this.txtCifrar.Size = new System.Drawing.Size(100, 20);
            this.txtCifrar.TabIndex = 0;
            // 
            // txtDescifrar
            // 
            this.txtDescifrar.Location = new System.Drawing.Point(13, 89);
            this.txtDescifrar.Name = "txtDescifrar";
            this.txtDescifrar.Size = new System.Drawing.Size(100, 20);
            this.txtDescifrar.TabIndex = 1;
            // 
            // btnCrifrar
            // 
            this.btnCrifrar.Location = new System.Drawing.Point(149, 23);
            this.btnCrifrar.Name = "btnCrifrar";
            this.btnCrifrar.Size = new System.Drawing.Size(75, 23);
            this.btnCrifrar.TabIndex = 2;
            this.btnCrifrar.Text = "Cifrar";
            this.btnCrifrar.UseVisualStyleBackColor = true;
            this.btnCrifrar.Click += new System.EventHandler(this.btnCrifrar_Click);
            // 
            // btnDesCifrar
            // 
            this.btnDesCifrar.Location = new System.Drawing.Point(149, 85);
            this.btnDesCifrar.Name = "btnDesCifrar";
            this.btnDesCifrar.Size = new System.Drawing.Size(75, 23);
            this.btnDesCifrar.TabIndex = 3;
            this.btnDesCifrar.Text = "Des-cifrar";
            this.btnDesCifrar.UseVisualStyleBackColor = true;
            this.btnDesCifrar.Click += new System.EventHandler(this.btnDesCifrar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.btnDesCifrar);
            this.Controls.Add(this.btnCrifrar);
            this.Controls.Add(this.txtDescifrar);
            this.Controls.Add(this.txtCifrar);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtCifrar;
        private System.Windows.Forms.TextBox txtDescifrar;
        private System.Windows.Forms.Button btnCrifrar;
        private System.Windows.Forms.Button btnDesCifrar;
    }
}

