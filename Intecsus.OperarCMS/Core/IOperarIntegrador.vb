﻿Imports Intecsus.GeneradorValoresCore
Imports Intecsus.Entidades
''' <summary>
''' Interface con las operaciones del integrador especificas
''' </summary>
Public Interface IOperacionIntegrador
    ''' <summary>
    ''' Metodo encargado de instaciar la clase una vez se ha instanciado el objeto
    ''' </summary>
    ''' <remarks>AERM 28/07/2015</remarks>
    Sub ValoresInicio(configurador As ConfInicio)

    ''' <summary>
    ''' Metodo encargado del proceso de inicial de obtencion del realce
    ''' </summary>
    ''' <param name="m_Cliente">Cliente con los parametros de la peticion</param>
    ''' <param name="m_IdTransaccion">Identificador de la transaccion</param>
    ''' <remarks>AERM 31/03/2015</remarks>
    Sub IniciarProceso(m_Cliente As Peticion, m_IdTransaccion As Integer)

End Interface
