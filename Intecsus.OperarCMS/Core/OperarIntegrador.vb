﻿Imports Intecsus.Entidades
Imports System.Configuration
Imports Intecsus.GeneradorValoresCore
Imports Intecsus.Clientes
Imports Intecsus.Seguridad

Public Class OperarIntegrador
    Implements IOperacionIntegrador

#Region "Variables"
    Private varInicio As ConfInicio
    Private validadores As Validadores = New Validadores()
#End Region

#Region "Metodo Interface"


    Public Sub ValoresInicio(configurador As ConfInicio) Implements IOperacionIntegrador.ValoresInicio
        varInicio = configurador
    End Sub


    Public Sub IniciarProceso(m_Cliente As Peticion, m_IdTransaccion As Integer) Implements IOperacionIntegrador.IniciarProceso
        Dim m_respuesta As RespuestaWS = New RespuestaWS()
        Dim m_procesar As IOperacionesCliente
        Try
            m_procesar = New FactoryClient().RetornarCliente(m_Cliente.cliente)
            m_respuesta = m_procesar.ObtenerRealce(m_Cliente, m_IdTransaccion, varInicio)
            Me.AnalizarRespuesta(m_respuesta, m_Cliente, m_IdTransaccion)
        Catch ex As Exception
            varInicio.operacionesBD.CambiarEstado(m_IdTransaccion, ConfigurationManager.AppSettings("BD"), ex.Message)
            'Throw ex
        End Try
    End Sub


#End Region

#Region "Operaciones de proceso"


    ''' <summary>
    ''' Metodo encargado de analizar la respuesta de la peticion enviada al CMS
    ''' </summary>
    ''' <param name="respuesta">Objeto respuesta retornada por el adaptador</param>
    ''' <param name="m_Cliente">Cliente que realia la peticion</param>
    ''' <param name="idTransaccion">Identificador unico de la transccion</param>
    ''' <remarks>AERM 25/03/2015</remarks>
    Private Sub AnalizarRespuesta(respuesta As RespuestaWS, m_Cliente As Peticion, idTransaccion As Integer)
        Try
            Dim m_codigo = validadores.ValidarEntero(respuesta.codigo, 1)
            ''Validamos si la respuesta es correcta
            If m_codigo = 0 Then
                Select Case m_Cliente.codigoNovedad
                    ''Realce
                    Case "03"
                        Me.EnviarImpresion(respuesta, m_Cliente, idTransaccion)
                    Case Else
                        varInicio.operacionesBD.CambiarEstado(idTransaccion, ConfigurationManager.AppSettings("BD"), "Finalizado", True)
                End Select
            Else
                Throw New Exception("Respuesta incorrecta por el CMS Externo -Procesos.vb-AnalizarRespuesta " + respuesta.descripcion)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de almacenar notificaciones al cliente
    ''' </summary>
    ''' <param name="respuesta">Respuesta de la accion realizada en el CMS</param>
    ''' <param name="m_Cliente">Cliente q realiza la peticion</param>
    ''' <param name="idTransaccion">Identificador unico de la transaccion</param>
    ''' <remarks>AERM 25/03/2015</remarks>
    Private Sub NotificacionCliente(respuesta As RespuestaWS, m_Cliente As Peticion, idTransaccion As Integer)
        Try
            Dim m_RespuestaNotificar As RespuestaDeviceStatus = New RespuestaDeviceStatus()
            m_RespuestaNotificar.Transaccion = idTransaccion
            m_RespuestaNotificar.hostIdentifier = idTransaccion
            m_RespuestaNotificar.TransaccionCliente = m_Cliente.idTransaccion
            m_RespuestaNotificar.Cliente = m_Cliente.cliente
            m_RespuestaNotificar.Mensaje = respuesta.descripcion
            varInicio.operarCola.GuardarObjetoNotificacion(m_RespuestaNotificar, varInicio.colaNotificacion)
            varInicio.operacionesBD.CambiarEstado(idTransaccion, ConfigurationManager.AppSettings("BD"), m_RespuestaNotificar.Mensaje)
        Catch ex As Exception
            varInicio.operacionesBD.CambiarEstado(idTransaccion, ConfigurationManager.AppSettings("BD"), "Procesos.vb- NotificacionCliente Error ingresar notificacion Cliente")
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de almacenar informacion para imprimir
    ''' </summary>
    ''' <param name="respuesta">Respuesta a imprimir</param>
    ''' <param name="m_Cliente">CLietne q realiza la peticion</param>
    ''' <param name="idTransaccion">Identificador Unico de la transaccion</param>
    ''' <remarks>AERM 25/03/2015</remarks>
    Private Sub EnviarImpresion(respuesta As RespuestaWS, m_Cliente As Peticion, idTransaccion As Integer)
        Try
            Dim m_datosRealce As DatosCardWizard = New DatosCardWizard()
            m_datosRealce.ID_Transaccion = idTransaccion
            m_datosRealce.Parametros = respuesta.parametros
            ''Obtenemos el cardformatname deacuerdo a configuracion base de datos tabla ProductoXCW
            m_datosRealce.CardWizardName = respuesta.cardWizardName
            ''Obtenemos el PCNAME deacuerdo a configuracion base de datos tabla Impresoras
            m_datosRealce.PCName = respuesta.pCName
            ''Asociamos el usuario
            m_datosRealce.Usuario = respuesta.usuario
            varInicio.operarCola.GuardarObjetoCardWizard(m_datosRealce, varInicio.colaRealce)
            varInicio.operacionesBD.CambiarEstado(idTransaccion, ConfigurationManager.AppSettings("BD"), "Proximo a Imprimir")
        Catch ex As Exception
            Throw New Exception("Procesos.vb- EnviarImpresion Error ingresar la impresion cola " + ex.Message)
        End Try
    End Sub



#End Region
End Class
