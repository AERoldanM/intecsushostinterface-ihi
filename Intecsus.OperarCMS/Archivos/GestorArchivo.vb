﻿Imports System.Configuration
Imports System.IO
Imports Intecsus.Entidades
Imports Intecsus.GeneradorValoresCore
Imports Intecsus.OperarArchivos

Public Class GestorArchivo

#Region "Metodos Prublicos"
    ''' <summary>
    ''' Metodo encargado de comenzar el proceso de carga de archivos
    ''' </summary>
    ''' <remarks>AERM 29/07/2015</remarks>
    Public Sub IniciarArchivo(varInicio As ConfInicio)
        Dim m_IdTransaccion As Integer = 0
        Try
            varInicio.operacionesBD.GuardarLogXNombreConectionString(11, m_IdTransaccion, 1, "OperarCMS.Procesos.vb - ComenzarProcesoXArchivo ",
                                                          ConfigurationManager.AppSettings("BD"))
            Dim m_Validar As RetornarArchivo = New RetornarArchivo()
            Dim m_Archivos As List(Of FileInfo) = m_Validar.RetornarArchivos(varInicio.ubicacionArchivo)
            Me.LecturaArchivos(m_Archivos, varInicio)
        Catch ex As Exception
            varInicio.operacionesBD.GuardarLogXNombreConectionString(11, m_IdTransaccion, 2, "ProcesoBase.Procesos.vb - XArchivo : " + ex.Message,
                                  ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub
#End Region

#Region "Proceso Archivos"

    ''' <summary>
    ''' Metodo encargado de la lectura de archivos a procesar
    ''' </summary>
    ''' <param name="m_Archivos">Archivo en formato FielInfo a procesar</param>
    ''' <remarks>AERM 20/03/2015</remarks>
    Private Sub LecturaArchivos(m_Archivos As List(Of FileInfo), varInicio As ConfInicio)
        Dim m_operar As ICArchivos = New RetornarArchivo().IntanciarArchivo(varInicio.extensionArchivo)
        For Each m_Archivo In m_Archivos
            Try
                Dim m_lineasArchivo As List(Of String) = m_operar.ObtenerInformacioArchivo(varInicio.ubicacionArchivo, m_Archivo.Name)
                Dim m_NombreArchivo As String = Date.Now.ToString()
                m_NombreArchivo = m_NombreArchivo.Replace("/", "-")
                m_NombreArchivo = m_NombreArchivo.Replace(":", "-")
                m_operar.CopiarArchivo(varInicio.ubicacionArchivo, m_Archivo.Name, varInicio.archivoProcesado, m_NombreArchivo + "procesado")
                m_operar.EliminarArchivo(varInicio.ubicacionArchivo + "\" + m_Archivo.Name)
                Me.ProcesoXLineasArchivo(m_lineasArchivo, 0, varInicio)
            Catch ex As Exception
                varInicio.operacionesBD.GuardarLogXNombreConectionString(11, 0, 2, "OperarCMS.Procesos.vb - LecturaArchivos : " + ex.Message + " " + m_Archivo.Name,
                                                         ConfigurationManager.AppSettings("BD"))
            End Try
        Next
    End Sub

    ''' <summary>
    ''' Metodo encargado de procesar las lineas del archivo plano
    ''' </summary>
    ''' <param name="m_lineasArchivo">Lineas de archivo a procesar</param>
    ''' <param name="m_IdTransaccion">Identificador de la transaccion</param>
    ''' <remarks>AERM 20/03/2015</remarks>
    Private Sub ProcesoXLineasArchivo(m_lineasArchivo As List(Of String), m_IdTransaccion As Integer, varInicio As ConfInicio)
        Try
            For Each linea As String In m_lineasArchivo
                Dim rnd As New Random()
                Dim m_Cliente As Peticion = New Peticion()
                m_Cliente.cliente = "ARCHIVO"
                m_Cliente.nombreLargo = linea.Trim()
                m_Cliente.codigoNovedad = "03"
                ' m_Cliente.idTransaccion = rnd.[Next](0, 1000).ToString() + "Prueba"
                m_Cliente.tipoProducto = varInicio.cardWizardNameXArchivo
                m_Cliente.printName = varInicio.operacionesBD.ObtenerValoresConfigurador("PCNAMEXArchivo", ConfigurationManager.AppSettings("BD"))
                Try
                    m_Cliente.idTransaccion = linea.Substring(0, 4)
                    m_IdTransaccion = varInicio.operacionesBD.InsertarTransaccion("ARCHIVO", m_Cliente.idTransaccion, ConfigurationManager.AppSettings("BD"))
                    Dim integrador As IOperacionIntegrador = New OperarIntegrador()

                    integrador.IniciarProceso(m_Cliente, m_IdTransaccion)
                    ' Me.txtRespuesta.Text += String.Format("{0} {1}", linea, System.Environment.NewLine)
                Catch ex As Exception
                    'Reintentos
                    ' Dim serializar As String = operarXML.Serializar(m_Cliente)
                    ' operacionesBD.InsertarProceso(m_IdTransaccion, ConfigurationManager.AppSettings("BD"), serializar, 11)
                    varInicio.operacionesBD.GuardarLogXNombreConectionString(11, m_IdTransaccion, 2, "OperarCMS.Procesos.vb - ProcesoXLineasArchivo - LeerLinea : " + ex.Message + " " + linea,
                                                          ConfigurationManager.AppSettings("BD"))
                End Try
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region
End Class
