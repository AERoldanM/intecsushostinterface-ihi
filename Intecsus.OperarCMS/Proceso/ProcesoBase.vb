﻿Imports Intecsus.MSMQ
Imports Intecusus.OperarBD
Imports Intecsus.OperarXML
Imports System.Configuration
Imports Intecsus.Entidades
Imports System.IO
Imports Intecsus.OperarArchivos
Imports System.Threading
Imports Intecsus.GeneradorValoresCore
Imports Intecsus.Clientes
Imports Intecsus.Seguridad

Public Class ProcesoBase

#Region "Variables"
    Private varInicio As ConfInicio
    Private integrador As IOperacionIntegrador
    Private notificacion As GestorNotificaciones
    Private archivos As GestorArchivo
#End Region

#Region "Eventos"

    Event TerminoProceso As EventHandler(Of Integer)
#End Region

#Region "Inicializar variables"
    ''' <summary>
    ''' Metodo encargado de inicializar as variables base para comenzar los procesos
    ''' </summary>
    ''' <remarks>AERM 20/03/2015</remarks>
    Public Sub InicilizarVariables()
        Try
            varInicio = New ConfInicio()
            integrador = New OperarIntegrador()
            notificacion = New GestorNotificaciones()
            archivos = New GestorArchivo()
            integrador.ValoresInicio(varInicio)
        Catch ex As Exception
            Throw New Exception("Procesar.vb - InicilizarVariables" + ex.Message)
        End Try
    End Sub
#End Region

#Region "Proceso"

    ''' <summary>
    ''' Metodo encargado de retornar el tiempo para comenzar los procesos
    ''' </summary>
    ''' <returns>Retorna Tiempo en segundos </returns>
    ''' <remarks>AERM 20/03/2015</remarks>
    Public Function RetornarTiempo() As Integer
        Return varInicio.tiempoProceso
    End Function

    ''' <summary>
    ''' Metodo usado para inicar el proceso de traer clientes
    ''' </summary>
    ''' <remarks>AERM 20/03/2015</remarks>
    Public Sub ComenzarProceso()
        Try
            Dim m_delegado As ThreadStart = New ThreadStart(Sub() notificacion.EnviarNotificaciones(varInicio))
            Dim m_hilo As Thread = New Thread(m_delegado)
            m_hilo.Start()
            Me.IniciaCargaCola()
            If (varInicio.procesaArchivo) Then
                archivos.IniciarArchivo(varInicio)
            End If
            Me.ReintentosTomaClientes()
        Catch ex As Exception
            varInicio.operacionesBD.GuardarLogXNombreConectionString(11, 0, 2, "ProcesoBase.Procesos.vb - ComenzarProceso : " + ex.Message,
                                  ConfigurationManager.AppSettings("BD"))
        Finally
            RaiseEvent TerminoProceso(Me, varInicio.tiempoProceso)
        End Try
    End Sub


#End Region

#Region "Gestion HilosX Cola"

    ''' <summary>
    ''' Metodo encargado del proceso X cola
    ''' </summary>
    ''' <remarks>AERM 29/07/2015</remarks>
    Private Sub IniciaCargaCola()
        Dim m_IdTransaccion As Integer = 0
        Try
            Dim m_ClienteI As Peticion = New Peticion()
            While varInicio.operarCola.RecibirPeticionCliente(varInicio.colaClientes, m_ClienteI)
                Dim m_Cantidad As Int32 = Process.GetCurrentProcess().Threads.Count
                'Creamos el delegado 
                Dim m_delegado As ThreadStart = New ThreadStart(Sub() Me.GestorProceso(m_ClienteI))
                'Creamos la instancia del hilo 
                Dim m_hilo As Thread = New Thread(m_delegado)
                'm_hilo.Name = "Integrador"
                'Dim myThread As Int32 = (From entry In Process.GetCurrentProcess().Threads
                '                                Where entry.name = "Integrador").Count()
                'Iniciamos el hilo 
                m_hilo.Start()
            End While
        Catch ex As Exception
            varInicio.operacionesBD.GuardarLogXNombreConectionString(11, m_IdTransaccion, 2, "ProcesoBase.Procesos.vb - XCOLA : " + ex.Message,
                                  ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Proceso encargado de gestionar todo el proceso de una cola
    ''' </summary>
    ''' <param name="m_ClienteI">Cliente con la peticion realizada</param>
    ''' <remarks>AERM 27/11/2015</remarks>
    Private Sub GestorProceso(m_ClienteI As Peticion)
        Dim m_Cliente As Peticion = m_ClienteI
        Dim m_IdTransaccion As Integer = 0
        Try
            varInicio.operacionesBD.GuardarLogXNombreConectionString(11, 0, 1, "OperarCMS.Procesos.vb - ComenzarProcesoXCOLA " + m_Cliente.idTransaccion,
                                       ConfigurationManager.AppSettings("BD"))
            If (Me.ValidarCola(m_Cliente)) Then
                m_IdTransaccion = varInicio.operacionesBD.InsertarTransaccion(m_Cliente.cliente, m_Cliente.idTransaccion, ConfigurationManager.AppSettings("BD"))
                Try
                    integrador.IniciarProceso(m_Cliente, m_IdTransaccion)
                Catch ex As Exception
                    'reintentos Proceso 11- Tomar clientes
                    Dim serializar As String = varInicio.operarXML.Serializar(m_Cliente)
                    varInicio.operacionesBD.InsertarProceso(m_IdTransaccion, ConfigurationManager.AppSettings("BD"), serializar, 11)
                    ' Throw ex
                End Try
            End If
        Catch ex As Exception
            varInicio.operacionesBD.GuardarLogXNombreConectionString(11, m_IdTransaccion, 2, "ProcesoBase.GestorProceso.vb - XCOLA : " + ex.Message,
                                  ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

#End Region

#Region "Reintentos"

    ''' <summary>
    ''' Metodo encargado de tomar los reintentos almacenados en B.D
    ''' </summary>
    ''' <remarks>AERM 27/03/2015</remarks>
    Private Sub ReintentosTomaClientes()
        Try
            Dim m_procesos As List(Of Reintentos) = varInicio.operacionesBD.ObtnerNotificacion(11, ConfigurationManager.AppSettings("BD"))
            For Each m_proceso In m_procesos
                Try
                    varInicio.operacionesBD.AumentarIntentosXProceso(m_proceso.IDProceso, ConfigurationManager.AppSettings("BD"))
                    Dim m_Cliente As Peticion = varInicio.operarXML.Deserialize(Of Peticion)(m_proceso.Informacion)
                    m_Cliente.IDproceso = m_proceso.IDProceso
                    integrador.IniciarProceso(m_Cliente, m_proceso.IDTransaccion)
                    'varInicio.operacionesBD.EliminarProceso(m_proceso.IDProceso, ConfigurationManager.AppSettings("BD"))
                Catch ex As Exception
                    ''Aumentar el reintento

                End Try
            Next
        Catch ex As Exception
            varInicio.operacionesBD.GuardarLogXNombreConectionString(11, 0, 2, "OperarCMS.Procesos.vb - ReintentosTomaClientes : " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub



    Private Function ValidarCola(m_respuesta As Peticion) As Boolean
        Try
            Dim m_RespuestaWS As RespuestaWSASNET = varInicio.operacionesBD.ObtenerRespuesta(m_respuesta.cliente, m_respuesta.idTransaccion, ConfigurationManager.AppSettings("BD"))
            If (IsNothing(m_RespuestaWS) = True) Then
                Return True
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return False
    End Function
#End Region

End Class
