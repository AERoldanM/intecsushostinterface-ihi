﻿Imports System.Configuration
Imports Intecsus.Clientes
Imports Intecsus.Entidades
Imports Intecsus.GeneradorValoresCore

''' <summary>
''' Clase encargada de la gestion de notificaciones
''' </summary>
Public Class GestorNotificaciones

#Region "Metodos Prublicos"
    ''' <summary>
    ''' Meetodo encargado de comenzar el proceso de envio de notificaciones
    ''' </summary>
    ''' <remarks>AERM 25/03/2015</remarks>
    Public Sub EnviarNotificaciones(varInicio As ConfInicio)
        Dim m_IdTransaccion As Integer = 0
        Try
            Dim m_respuesta As RespuestaDeviceStatus = New RespuestaDeviceStatus()
            Dim m_procesar As IOperacionesCliente
            While varInicio.operarCola.RecibirObjetoNotificar(varInicio.colaNotificacion, m_respuesta)
                Try
                    m_IdTransaccion = If(IsNumeric(m_respuesta.hostIdentifier), CInt(m_respuesta.hostIdentifier), 0)
                    Dim m_valores As List(Of String) = varInicio.operacionesBD.ObtenerDatosCliente(m_respuesta.hostIdentifier, ConfigurationManager.AppSettings("BD"))
                    If (m_valores.Count >= 2 And Me.ValidarNotificacion(m_valores)) Then
                        m_respuesta.Cliente = m_valores(0)
                        m_respuesta.TransaccionCliente = m_valores(1)

                        m_procesar = New FactoryClient().RetornarCliente(m_respuesta.Cliente)
                        varInicio.operacionesBD.CambiarEstado(m_respuesta.hostIdentifier, ConfigurationManager.AppSettings("BD"), "Por enviar a Notificacion")
                        m_procesar.RollBack(m_respuesta, m_respuesta.TransaccionCliente, varInicio)
                        m_procesar.NotificarCliente(m_respuesta, m_respuesta.TransaccionCliente, varInicio)
                        'varInicio.operacionesBD.CambiarEstado(m_respuesta.hostIdentifier, ConfigurationManager.AppSettings("BD"), ESTADO_REALCE.ST_OK.ToString() + "Notificacion", True)
                        varInicio.operacionesBD.GuardarLogXNombreConectionString(10, m_IdTransaccion, 1, "OperarCMS.Procesos.vb - EnviarNotificaciones  ",
                                                           ConfigurationManager.AppSettings("BD"))
                    End If
                    m_IdTransaccion = 0
                Catch ex As Exception
                    'reintentos Proceso 10- Notificar Proces
                    varInicio.operacionesBD.CambiarEstado(m_respuesta.hostIdentifier, ConfigurationManager.AppSettings("BD"), ESTADO_REALCE.ST_ERROR.ToString() + "Notificacion", True)
                    Dim serializar As String = varInicio.operarXML.Serializar(m_respuesta)
                    varInicio.operacionesBD.InsertarProceso(m_respuesta.hostIdentifier, ConfigurationManager.AppSettings("BD"), serializar, 10)
                    Throw ex
                End Try
            End While

        Catch ex As Exception
            varInicio.operacionesBD.GuardarLogXNombreConectionString(10, m_IdTransaccion, 2, "OperarCMS.Procesos.vb - EnviarNotificaciones : " + ex.Message,
                                                         ConfigurationManager.AppSettings("BD"))
        Finally
            Me.ReintentosNotificacion(varInicio)
        End Try
    End Sub
#End Region

#Region "Metodos Privados"

    ''' <summary>
    ''' Valida si la notificacion es valida
    ''' </summary>
    ''' <param name="m_valores">Valores a validad</param>
    ''' <returns>AERM 17/04/2015</returns>
    Private Function ValidarNotificacion(m_valores As List(Of String)) As Boolean
        If (m_valores.Count >= 3) Then
            If (m_valores(2).Contains("Notificaci")) Then
                Return False
            End If
        End If
        Return True
    End Function


    ''' <summary>
    ''' Metodo encargado de reintentar enviar la notificacion
    ''' </summary>
    ''' <remarks>AERM 27/03/2015</remarks>
    Private Sub ReintentosNotificacion(varInicio As ConfInicio)
        Try
            Dim m_procesos As List(Of Reintentos) = varInicio.operacionesBD.ObtnerNotificacion(10, ConfigurationManager.AppSettings("BD"))
            For Each m_proceso In m_procesos
                Dim m_respuesta As RespuestaDeviceStatus = varInicio.operarXML.Deserialize(Of RespuestaDeviceStatus)(m_proceso.Informacion)
                ''SI la cola notificacion no existe almacena valores sin cliente
                If (String.IsNullOrEmpty(m_respuesta.Cliente)) Then
                    Dim m_valores As List(Of String) = varInicio.operacionesBD.ObtenerDatosCliente(m_respuesta.hostIdentifier, ConfigurationManager.AppSettings("BD"))
                    If (m_valores.Count >= 2 And Me.ValidarNotificacion(m_valores)) Then
                        m_respuesta.Cliente = m_valores(0)
                        m_respuesta.TransaccionCliente = m_valores(1)
                    Else
                        varInicio.operacionesBD.EliminarProceso(m_proceso.IDProceso, ConfigurationManager.AppSettings("BD"))
                        Throw New Exception("NO se encuentra valores base para reintentos Notificacion")
                    End If
                End If

                Dim m_procesar As IOperacionesCliente
                m_procesar = New FactoryClient().RetornarCliente(m_respuesta.Cliente)
                ''Aumentar el reintento
                varInicio.operacionesBD.AumentarIntentosXProceso(m_proceso.IDProceso, ConfigurationManager.AppSettings("BD"))
                m_procesar.NotificarCliente(m_respuesta, m_respuesta.TransaccionCliente, varInicio)
                ''varInicio.operacionesBD.CambiarEstado(m_respuesta.hostIdentifier, ConfigurationManager.AppSettings("BD"), ESTADO_REALCE.ST_OK.ToString() + "Notificacion", True)
                varInicio.operacionesBD.EliminarProceso(m_proceso.IDProceso, ConfigurationManager.AppSettings("BD"))
            Next
        Catch ex As Exception
            varInicio.operacionesBD.GuardarLogXNombreConectionString(10, 0, 2, "OperarCMS.Procesos.vb - ReintentosNotificacion : " + ex.Message,
                                                        ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

#End Region
End Class



