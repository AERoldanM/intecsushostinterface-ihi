﻿Imports Intecusus.OperarBD
Imports System.Configuration
Imports Intecsus.OperarCMS.TCP
Imports Intecsus.Entidades
Imports Intecsus.OperarXML
Imports System.Xml

''' <summary>
''' Operaciones base realizadas sobre CW para
''' impresion de tiquetes
''' </summary>
''' <remarks>AERM 29-01-2015</remarks>
Partial Public Class OperacionesBaseCW



    Implements IOperacion

    Private m_GenerarXML As GeneradorXML = New GeneradorXML()
    ' new way to utilize TCP
    Private WithEvents mNewTcpClient As TCP.TCPClient

    Private m_TransRespuesta As Respuesta = New Respuesta()

    Private operacionesBD As OperacionesBase = New OperacionesBase()

#Region "Operaciones Con Session"

    ''' <summary>
    ''' Metodo encargado de crear session en CW
    ''' </summary>
    ''' <param name="servidor">Nombre del servidor de CW </param>
    ''' <param name="puerto">Puerto de comunicación con CW</param>
    ''' <param name="respuesta">Respuesta obtenida</param>
    ''' <param name="usuario">Usuario de CW</param>
    ''' <param name="password">Password de CW</param>
    ''' <param name="callBack">Metodo enviado por ahora debe ir SDT</param>
    ''' <param name="interfaceUsar">Interface q se implementa por ahora CWHostInterfaceExample</param>
    ''' <returns>Retorna respuesta de Session</returns>
    ''' <remarks>AERM 02-02-2015</remarks>
    Public Function CrearSession(ByVal servidor As [String], ByVal puerto As Int32, ByVal respuesta As String, ByVal usuario As String, ByVal password As String,
                                  ByVal callBack As String, ByVal interfaceUsar As String) As String Implements IOperacion.CrearSession
        Try
            Dim m_peticion As String = String.Empty
            Dim m_Parametros As List(Of ParametrosXML) = New List(Of ParametrosXML)()
            Dim m_Parametro As ParametrosXML = New ParametrosXML()
            m_Parametro.Nombre = "UserName"
            m_Parametro.Typo = "string"
            m_Parametro.Valor = usuario
            m_Parametros.Add(m_Parametro)

            m_Parametro = New ParametrosXML()
            m_Parametro.Nombre = "UserPassword"
            m_Parametro.Typo = "string"
            m_Parametro.Valor = password
            m_Parametros.Add(m_Parametro)

            m_Parametro = New ParametrosXML()
            m_Parametro.Nombre = "CallbackMethod"
            m_Parametro.Typo = "string"
            m_Parametro.Valor = callBack
            m_Parametros.Add(m_Parametro)
            m_peticion = m_GenerarXML.CrearXML(interfaceUsar, "HostBeginSession", m_Parametros)
            operacionesBD.GuardarLogXNombreConectionString(2, 0, 1, "OperacionesBaseCW - CrearSession : " + m_peticion,
                                                          ConfigurationManager.AppSettings("BD"))
            If Me.EnviarSolicitudTOServidor(servidor, puerto, m_peticion, 0, respuesta) Then
                Return m_TransRespuesta.ProcesarSession(respuesta)
            End If
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(2, 0, 2, "OperacionesBaseCW - CrearSession : " + ex.Message,
                                                           ConfigurationManager.AppSettings("BD"))
        End Try

        Return String.Empty
    End Function

    Public Function CerrarSession(ByVal servidor As [String], ByVal puerto As Int32, ByVal respuesta As String, ByVal session As String,
                                   ByVal interfaceUsar As String) As Boolean Implements IOperacion.CerrarSession
        Try
            Dim m_peticion As String = String.Empty
            Dim m_Parametros As List(Of ParametrosXML) = New List(Of ParametrosXML)()
            Dim m_Parametro As ParametrosXML = New ParametrosXML()
            m_Parametro.Nombre = "SessionID"
            m_Parametro.Typo = "string"
            m_Parametro.Valor = session
            m_Parametros.Add(m_Parametro)


            m_peticion = m_GenerarXML.CrearXML(interfaceUsar, "HostEndSession", m_Parametros)
            operacionesBD.GuardarLogXNombreConectionString(3, 0, 1, "OperacionesBaseCW - CerrarSession : " + m_peticion,
                                                          ConfigurationManager.AppSettings("BD"))
            If (Me.EnviarSolicitudTOServidor(servidor, puerto, m_peticion, 0, respuesta)) Then
                Return m_TransRespuesta.ProcesarRespuestaValida(respuesta)
            End If
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(3, 0, 2, "OperacionesBaseCW - CerrarSession : " + ex.Message,
                                                           ConfigurationManager.AppSettings("BD"))
        End Try

        Return False
    End Function

#End Region

#Region "Obtener Mensajes"

    Public Function MensajesEventos(ByVal servidor As [String], ByVal puerto As Int32, ByVal respuesta As String, ByVal session As String,
                             ByVal interfaceUsar As String) As XmlDocument Implements IOperacion.MensajesEventos

        Try
            Dim m_peticion As String = String.Empty
            Dim m_Parametros As List(Of ParametrosXML) = New List(Of ParametrosXML)()
            Dim m_Parametro As ParametrosXML = New ParametrosXML()
            Dim m_Doc As XmlDocument = New XmlDocument()
            m_Parametro.Nombre = "SessionID"
            m_Parametro.Typo = "string"
            m_Parametro.Valor = session
            m_Parametros.Add(m_Parametro)

            m_peticion = m_GenerarXML.CrearXML(interfaceUsar, "HostGetMessage", m_Parametros)
            operacionesBD.GuardarLogXNombreConectionString(7, 0, 1, "OperacionesBaseCW - MensajesEventos : " + m_peticion,
                                                          ConfigurationManager.AppSettings("BD"))
            If (Me.EnviarSolicitudTOServidor(servidor, puerto, m_peticion, 0, respuesta)) Then
                m_Doc = m_TransRespuesta.ProcessResponse(respuesta)
                Return m_Doc
            End If
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(7, 0, 2, "OperacionesBaseCW - MensajesEventos : " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
        End Try
        Return Nothing
    End Function
#End Region

#Region "EnviarSolicitudes"

    ''' <summary>
    ''' Metodo encargado de enviar las solicitudes al servidor
    ''' </summary>
    ''' <param name="servidor">Servidor al que se le realiza la peticion</param>
    ''' <param name="puerto">Puerto de comunicacion</param>
    ''' <param name="request">Informacion de envio</param>
    ''' <param name="idTransaccion">Identificador de la transaccion puede ir en 0 si es abrir session</param>
    ''' <param name="respuesta">Respuesta obtenida</param>
    ''' <returns>Retorna si se puedo enviar la solicitud</returns>
    ''' <remarks>AERM 29-01-2015</remarks>
    Private Function EnviarSolicitudTOServidor(ByVal servidor As [String], ByVal puerto As Int32, ByVal request As [String], ByVal idTransaccion As Int32,
                                               ByRef respuesta As String) As Boolean Implements IOperacion.EnviarSolicitudTOServidor
        Dim sErrorMessage As String = String.Empty
        Try
            'Para este cliente para trabajar es necesario tener un tcpserver
            'Conectado a la misma dirección que especifica el servidor, puerto
            'Combinación.n.

            If TypeName(mNewTcpClient) = "Nothing" Then
                mNewTcpClient = New TCPClient("CWHostInterfaceExample")

                ' Establecer el nombre y el puerto
                mNewTcpClient.ServerName = servidor
                mNewTcpClient.PortNumber = puerto

                'Abrir la conexion
                If Not mNewTcpClient.Connect Then
                    ' Si no Genera excepcion
                    Throw New System.Exception("No se pudo conectar con el servidor.")
                End If
            End If

            If Not mNewTcpClient.SendMessageGetResponse(TCP.TCPBase.MESSAGE_TYPE.Standard, request, respuesta, sErrorMessage) Then
                ' error
                Throw New System.Exception(sErrorMessage)
            End If

            Return True
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(1, idTransaccion, 2, "OperacionesBaseCW - EnviarSolicitudTOServidor : " + ex.Message,
                                                           ConfigurationManager.AppSettings("BD"))
            Return False
        End Try
    End Function

#End Region

#Region "Eventos TCPClient"
    ''' <summary>
    ''' Handler that gets called when the TCP client gets disconnected
    ''' </summary>
    Private Sub oTCPclient_ClientDisconnected() Handles mNewTcpClient.ClientDisconnected
        ' Call DisplayEvent("TCP disconnected") ' Note that this is commented out because it was getting called during frmMain_Closing
    End Sub

    ''' <summary>
    ''' Handler that gets called when the TCP client runs into an error
    ''' </summary>
    ''' <param name="sErrorMessage"></param>
    ''' <remarks></remarks>
    Private Sub oTCPclient_ErrorEncountered(ByVal sErrorMessage As String) Handles mNewTcpClient.ErrorEncountered
        Try
            operacionesBD.GuardarLogXNombreConectionString(8, 0, 2, "OperacionesBaseCW - oTCPclient_ErrorEncountered : " + sErrorMessage,
                                                                ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(8, 0, 2, "OperacionesBaseCW - oTCPclient_ErrorEncountered : " + ex.Message,
                                                               ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Handler that gets called when a message is recieved from the server
    ''' </summary>
    ''' <param name="sMessage">The message sent to us</param>
    Private Sub oTCPclient_MessageFromServer(ByVal sMessage As String) Handles mNewTcpClient.MessageFromServer
        'DisplayEvent("Unsolicited Server Message = " & sMessage)

        Dim m_respuesta As RespuestaDeviceStatus = m_TransRespuesta.ProcesarMensage(sMessage)
        
        RaiseEvent RecibeMensaje(Me, m_respuesta)
    End Sub

    Public Event RecibeMensaje As EventHandler(Of RespuestaDeviceStatus) Implements IOperacion.RecibeMensaje

#End Region

End Class
