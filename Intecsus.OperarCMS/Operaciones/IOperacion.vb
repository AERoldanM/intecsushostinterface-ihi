﻿Imports Intecsus.Entidades
Imports System.Xml

Public Interface IOperacion

    Event RecibeMensaje As EventHandler(Of RespuestaDeviceStatus)

    ''' <summary>
    ''' Metodo encargado de crear session en CW
    ''' </summary>
    ''' <param name="servidor">Nombre del servidor de CW </param>
    ''' <param name="puerto">Puerto de comunicación con CW</param>
    ''' <param name="respuesta">Respuesta obtenida</param>
    ''' <param name="usuario">Usuario de CW</param>
    ''' <param name="password">Password de CW</param>
    ''' <param name="callBack">Metodo enviado por ahora debe ir SDT</param>
    ''' <param name="interfaceUsar">Interface q se implementa por ahora CWHostInterfaceExample</param>
    ''' <returns>Retorna respuesta de Session</returns>
    ''' <remarks>AERM 02-02-2015</remarks>
    Function CrearSession(ByVal servidor As [String], ByVal puerto As Int32, ByVal respuesta As String, ByVal usuario As String, ByVal password As String,
                                  ByVal callBack As String, ByVal interfaceUsar As String) As String

    ''' <summary>
    ''' Metodo encargado de cerrar uan session creada
    ''' </summary>
    ''' <param name="servidor">Nombre del servidor de CW </param>
    ''' <param name="puerto">Puerto de comunicación con CW</param>
    ''' <param name="respuesta">Respuesta obtenida</param>
    ''' <param name="session">Session a cerrar</param>
    ''' <param name="interfaceUsar">Nombre de la interfaz a usar </param>
    ''' <returns>Retorna si se obtuvo la operacion fue realizada con exito</returns>
    ''' <remarks>AERM 09-02-2014</remarks>
    Function CerrarSession(ByVal servidor As [String], ByVal puerto As Int32, ByVal respuesta As String, ByVal session As String,
                                   ByVal interfaceUsar As String) As Boolean

    ''' <summary>
    ''' Metodo encargado de enviar las peticiones al CW
    ''' </summary>
    ''' <param name="servidor">Servidor al que se le realiza la peticion</param>
    ''' <param name="puerto">Puerto de comunicacion</param>
    ''' <param name="request">Informacion de envio</param>
    ''' <param name="idTransaccion">Identificador de la transaccion puede ir en 0 si es abrir session</param>
    ''' <param name="respuesta">Respuesta obtenida</param>
    ''' <returns>Retorna si se puedo enviar la solicitud</returns>
    ''' <remarks>AERM 09-02-2014</remarks>
    Function EnviarSolicitudTOServidor(ByVal servidor As [String], ByVal puerto As Int32, ByVal request As [String], ByVal idTransaccion As Int32,
                                               ByRef respuesta As String) As Boolean

    ''' <summary>
    ''' Metodo encargado de manejar los mensajes de CW
    ''' </summary>
    ''' <param name="servidor">Servidor al que se le realiza la peticion</param>
    ''' <param name="puerto">Puerto de comunicacion</param>
    ''' <param name="respuesta">Respuesta obtenida</param>
    ''' <param name="session">Session a cerrar</param>
    ''' <param name="interfaceUsar">Nombre de la interfaz a usar </param>
    ''' <returns>Retorna XML con la respuesta para ser tratada</returns>
    ''' <remarks>AERM 10-02-2014</remarks>
    Function MensajesEventos(ByVal servidor As [String], ByVal puerto As Int32, ByVal respuesta As String, ByVal session As String,
                             ByVal interfaceUsar As String) As XmlDocument



    ''' <summary>
    ''' Metodo encargado de enviar la impresion de tarjeta al CW
    ''' </summary>
    ''' <param name="session">Session para realizar la peticion</param>
    ''' <param name="servidor">Servidor al que se le realiza la peticion</param>
    ''' <param name="puerto">Puerto de comunicacion</param>
    ''' <param name="idTransaccion">Identificador de la transaccion puede ir en 0 si es abrir session</param>
    ''' <param name="usuario">Usuario que realiza la peticion RequestorName</param>
    ''' <param name="respuesta">Respuesta obtenida</param>
    ''' <param name="interfaceUsar">Interface de comunicacion con CW</param>
    ''' <param name="informacion">Informacion q se debe enviar a CW</param>
    ''' <returns>Retorna si se logro enviar la solicitud</returns>
    ''' <remarks>AERM 09-02-2014</remarks>
    Function HacerTarjeta(ByVal servidor As [String], ByVal puerto As Int32, ByVal respuesta As String, ByVal usuario As String, ByVal session As String, ByVal idTransaccion As String,
                          ByVal interfaceUsar As String, ByVal informacion As DatosCardWizard) As Boolean

    ''' <summary>
    ''' Metodo encargado de eliminar la impresion de una tarjeta
    ''' </summary>
    ''' <param name="servidor">Servidor al que se le realiza la peticion</param>
    ''' <param name="puerto">Puerto de comunicacion</param>
    ''' <param name="idTransaccion">Identificador de la transaccion puede ir en 0 si es abrir session</param>
    ''' <param name="respuesta">Respuesta obtenida</param>
    ''' <param name="interfaceUsar">Interface de comunicacion con CW</param>
    ''' <param name="session">Session para realizar la peticion</param>
    ''' <returns>Retorna si el resultado es el esperado</returns>
    ''' <remarks>AERM 09-02-2014</remarks>
    Function EliminarTarjeta(ByVal servidor As [String], ByVal puerto As Int32, ByVal respuesta As String, ByVal session As String, ByVal idTransaccion As String,
                          ByVal interfaceUsar As String) As Boolean

    ''' <summary>
    ''' Metodo encardo de realizar la peticion para saber el estado de una tarjeta  especifica
    ''' </summary>
    ''' <param name="servidor">Servidor al que se le realiza la peticion</param>
    ''' <param name="puerto">Puerto de comunicacion</param>
    ''' <param name="respuesta">Respuesta obtenida</param>
    ''' <param name="session">Session a cerrar</param>
    ''' <param name="interfaceUsar">Nombre de la interfaz a usar </param>
    ''' <param name="pcName">Nombre del equipo asociado</param>
    ''' <param name="cardFormatName">Nombre del card format asociada al card device</param>
    ''' <param name="bEncodeOnly">Si es True obtener el estado del dispositivo designado para la codificación (re-pin) 
    ''' Si Falso obtener el estado del dispositivo designado para repujado</param>
    ''' <returns>Retorna XML con la respuesta para ser tratada</returns>
    ''' <remarks>AERM 10-02-2014</remarks>
    Function EstadoXPCName(ByVal servidor As [String], ByVal puerto As Int32, ByVal respuesta As String, ByVal session As String, ByVal pcName As String,
                           ByVal interfaceUsar As String, ByVal cardFormatName As String, Optional ByVal bEncodeOnly As Boolean = False) As RespuestaEstado

End Interface
