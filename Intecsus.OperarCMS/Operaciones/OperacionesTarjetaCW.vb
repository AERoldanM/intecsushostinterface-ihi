﻿Imports Intecusus.OperarBD
Imports System.Configuration
Imports Intecsus.OperarCMS.TCP
Imports Intecsus.Entidades
Imports Intecsus.OperarXML
Imports System.Xml

Partial Public Class OperacionesBaseCW
    Implements IOperacion

    Public Function HacerTarjeta(ByVal servidor As [String], ByVal puerto As Int32, ByVal respuesta As String, ByVal usuario As String, ByVal session As String, ByVal idTransaccion As String,
                          ByVal interfaceUsar As String, ByVal informacion As DatosCardWizard) As Boolean Implements IOperacion.HacerTarjeta
        Try
            Dim m_peticion As String = String.Empty
            Dim m_parametros As List(Of ParametrosXML) = New List(Of ParametrosXML)
            Dim m_Parametro As ParametrosXML = New ParametrosXML()
            m_Parametro.Nombre = "SessionID"
            m_Parametro.Typo = "string"
            m_Parametro.Valor = session
            m_parametros.Add(m_Parametro)

            m_Parametro = New ParametrosXML()
            m_Parametro.Nombre = "RequestorPCName"
            m_Parametro.Typo = "string"
            m_Parametro.Valor = informacion.PCName
            m_parametros.Add(m_Parametro)

            m_Parametro = New ParametrosXML()
            m_Parametro.Nombre = "RequestorName"
            m_Parametro.Typo = "string"
            m_Parametro.Valor = usuario
            m_parametros.Add(m_Parametro)


            ''Id transaccion 
            m_Parametro = New ParametrosXML()
            m_Parametro.Nombre = "HostIdentifier"
            m_Parametro.Typo = "string"
            m_Parametro.Valor = idTransaccion.ToString()
            m_parametros.Add(m_Parametro)

            m_Parametro = New ParametrosXML()
            m_Parametro.Nombre = "CardFormatName"
            m_Parametro.Typo = "string"
            m_Parametro.Valor = informacion.CardWizardName
            m_parametros.Add(m_Parametro)

            For Each m_param As ParametrosXML In informacion.Parametros
                m_parametros.Add(m_param)
            Next

            m_peticion = m_GenerarXML.CrearXML(interfaceUsar, "HostSubmitCardMake", m_parametros)
            operacionesBD.GuardarLogXNombreConectionString(5, idTransaccion, 1, "OperacionesBaseCW - HacerTarjeta : " + m_peticion,
                                                          ConfigurationManager.AppSettings("BD"))
            If (Me.EnviarSolicitudTOServidor(servidor, puerto, m_peticion, idTransaccion, respuesta)) Then
                operacionesBD.CambiarEstado(idTransaccion, ConfigurationManager.AppSettings("BD"), ESTADO_REALCE.ST_EN_PROCESO)
                Return m_TransRespuesta.ProcesarRespuestaValida(respuesta)
            End If
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(5, idTransaccion, 2, "OperacionesBaseCW - HacerTarjeta : " + ex.Message,
                                                           ConfigurationManager.AppSettings("BD"))
            operacionesBD.CambiarEstado(idTransaccion, ConfigurationManager.AppSettings("BD"), ESTADO_REALCE.ST_ERROR)
        End Try

        Return False
    End Function

    'Public Function Reintento(ByVal servidor As [String], ByVal puerto As Int32, ByVal idTransaccion As Integer, ByVal respuesta As String) As Boolean
    '    Try
    '        Dim m_peticion As String = operacionesBD.ObtenerProcesoXID(idTransaccion, ConfigurationManager.AppSettings("BD"))
    '        If (Me.EnviarSolicitudTOServidor(servidor, puerto, m_peticion, idTransaccion, respuesta)) Then

    '            Return m_TransRespuesta.ProcesarRespuestaValida(respuesta)
    '        End If
    '    Catch ex As Exception
    '        operacionesBD.GuardarLogXNombreConectionString(5, idTransaccion, 2, "OperacionesBaseCW - Reintento : " + ex.Message,
    '                                                       ConfigurationManager.AppSettings("BD"))
    '        operacionesBD.CambiarEstado(idTransaccion, ConfigurationManager.AppSettings("BD"), ESTADO_REALCE.ST_ERROR)
    '    End Try

    '    Return False
    'End Function

    Public Function EliminarTarjeta(servidor As String, puerto As Integer, respuesta As String, session As String, idTransaccion As String,
                                    interfaceUsar As String) As Boolean Implements IOperacion.EliminarTarjeta
        Try
            Dim m_peticion As String = String.Empty
            Dim m_Parametros As List(Of ParametrosXML) = New List(Of ParametrosXML)()
            Dim m_Parametro As ParametrosXML = New ParametrosXML()
            m_Parametro.Nombre = "SessionID"
            m_Parametro.Typo = "string"
            m_Parametro.Valor = session
            m_Parametros.Add(m_Parametro)

            m_Parametro.Nombre = "RequestCounter"
            m_Parametro.Typo = "int"
            m_Parametro.Valor = idTransaccion
            m_Parametros.Add(m_Parametro)

            m_peticion = m_GenerarXML.CrearXML(interfaceUsar, "HostCardRequestDelete", m_Parametros)
            operacionesBD.GuardarLogXNombreConectionString(6, idTransaccion, 1, "OperacionesBaseCW - EliminarTarjeta : " + m_peticion,
                                                          ConfigurationManager.AppSettings("BD"))
            If (Me.EnviarSolicitudTOServidor(servidor, puerto, m_peticion, idTransaccion, respuesta)) Then
                Return m_TransRespuesta.ProcesarRespuestaValida(respuesta)
            End If

        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(6, idTransaccion, 2, "OperacionesBaseCW - EliminarTarjeta : " + ex.Message,
                                                           ConfigurationManager.AppSettings("BD"))
        End Try

        Return False
    End Function

    ''' <summary>
    ''' Metodo encardo de realizar la peticion para saber el estado de una tarjeta  especifica
    ''' </summary>
    ''' <param name="servidor">Servidor al que se le realiza la peticion</param>
    ''' <param name="puerto">Puerto de comunicacion</param>
    ''' <param name="respuesta">Respuesta obtenida</param>
    ''' <param name="session">Session a cerrar</param>
    ''' <param name="interfaceUsar">Nombre de la interfaz a usar </param>
    ''' <param name="pcName">Nombre del equipo asociado</param>
    ''' <param name="cardFormatName">Nombre del card format asociada al card device</param>
    ''' <param name="bEncodeOnly">Si es True obtener el estado del dispositivo designado para la codificación (re-pin) 
    ''' Si Falso obtener el estado del dispositivo designado para repujado</param>
    ''' <returns>Retorna XML con la respuesta para ser tratada</returns>
    ''' <remarks>AERM 10-02-2014</remarks>
    Function EstadoXPCName(ByVal servidor As [String], ByVal puerto As Int32, ByVal respuesta As String, ByVal session As String, ByVal pcName As String,
                           ByVal interfaceUsar As String, ByVal cardFormatName As String, Optional ByVal bEncodeOnly As Boolean = False) As RespuestaEstado Implements IOperacion.EstadoXPCName
        Try
            Dim m_peticion As String = String.Empty
            Dim m_Parametros As List(Of ParametrosXML) = New List(Of ParametrosXML)()
            Dim m_Parametro As ParametrosXML = New ParametrosXML()
            Dim m_EncodeOnly As String
            m_Parametro.Nombre = "SessionID"
            m_Parametro.Typo = "string"
            m_Parametro.Valor = session
            m_Parametros.Add(m_Parametro)

            m_Parametro = New ParametrosXML()
            m_Parametro.Nombre = "RequestorPCName"
            m_Parametro.Typo = "string"
            m_Parametro.Valor = pcName
            m_Parametros.Add(m_Parametro)

            m_Parametro = New ParametrosXML()
            m_Parametro.Nombre = "CardFormatName"
            m_Parametro.Typo = "string"
            m_Parametro.Valor = cardFormatName
            m_Parametros.Add(m_Parametro)

            ' EncodeOnly
            If bEncodeOnly Then
                m_EncodeOnly = "Y"
            Else
                m_EncodeOnly = "N"
            End If

            m_Parametro = New ParametrosXML()
            m_Parametro.Nombre = "EncodeOnly"
            m_Parametro.Typo = "string"
            m_Parametro.Valor = m_EncodeOnly
            m_Parametros.Add(m_Parametro)
            m_peticion = m_GenerarXML.CrearXML(interfaceUsar, "HostGetCardDeviceStatus", m_Parametros)
            operacionesBD.GuardarLogXNombreConectionString(8, 0, 1, "OperacionesBaseCW - EstadoXPCName : " + m_peticion,
                                                          ConfigurationManager.AppSettings("BD"))
            If (Me.EnviarSolicitudTOServidor(servidor, puerto, m_peticion, 0, respuesta)) Then
                Return m_TransRespuesta.ProcesarEstadoCardDevice(respuesta)
            End If
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(8, 0, 2, "OperacionesBaseCW - EstadoXPCName : " + ex.Message,
                                                           ConfigurationManager.AppSettings("BD"))
        End Try

        Return Nothing
    End Function
End Class
