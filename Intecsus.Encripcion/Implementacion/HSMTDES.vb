﻿Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Configuration
Imports Intecusus.OperarBD

Public Class HSMTDES
    Implements IEncrypt

    Private Shared _instancia As HSMTDES = Nothing
    Private Shared LockThis As New Object
    Private Shared INITIALIZED As Boolean = False
    Private hSession As StringBuilder
    Private hKey As StringBuilder

    <DllImport("SafeNetCryotoki.dll", _
        BestFitMapping:=True, _
        PreserveSig:=True, _
        EntryPoint:="TDES", SetLastError:=False, _
        CharSet:=CharSet.Ansi, ExactSpelling:=False, _
        CallingConvention:=CallingConvention.Cdecl)> _
    Private Shared Function TDES(ByVal in_hSession As StringBuilder, ByVal in_hKey As StringBuilder, ByVal data_in As StringBuilder, ByVal data_out As StringBuilder, ByVal mode As StringBuilder, ByVal IV As StringBuilder) As Integer
    End Function

    <DllImport("SafeNetCryotoki.dll", _
        BestFitMapping:=True, _
        PreserveSig:=True, _
        EntryPoint:="INICIAR_PKCS11", SetLastError:=False, _
        CharSet:=CharSet.Ansi, ExactSpelling:=False, _
        CallingConvention:=CallingConvention.Cdecl)> _
    Private Shared Function INICIAR_PKCS11() As Integer
    End Function

    <DllImport("SafeNetCryotoki.dll", _
        BestFitMapping:=True, _
        PreserveSig:=True, _
        EntryPoint:="INICIAR_RECURSOS", SetLastError:=False, _
        CharSet:=CharSet.Ansi, ExactSpelling:=False, _
        CallingConvention:=CallingConvention.Cdecl)> _
    Private Shared Function INICIAR_RECURSOS(ByVal token_name As StringBuilder, ByVal passphrase As StringBuilder, ByVal key_label As StringBuilder, ByVal out_hSession As StringBuilder, ByVal out_hKey As StringBuilder) As Integer
    End Function

    <DllImport("SafeNetCryotoki.dll", _
            BestFitMapping:=True, _
            PreserveSig:=True, _
            EntryPoint:="FINALIZAR_RECURSOS", SetLastError:=False, _
            CharSet:=CharSet.Ansi, ExactSpelling:=False, _
            CallingConvention:=CallingConvention.Cdecl)> _
    Private Shared Function FINALIZAR_RECURSOS(ByVal in_hSession As StringBuilder) As Integer
    End Function

    Public Shared ReadOnly Property Instancia() As HSMTDES
        Get
            'Hasta que no termine la petición no recibe la otra (Caso multihilo)
            SyncLock LockThis
                ' valida que no exista una instancia si ya existe, no la realiza
                If (_instancia Is Nothing) Then
                    _instancia = New HSMTDES()
                End If
            End SyncLock

            Return _instancia
        End Get
    End Property

    Private Sub New()
        Try
            SyncLock LockThis
                Dim rv As Integer
                If Not INITIALIZED Then
                    ' Carga las funciones PKSC#11 directamente de la DLL cryptoki e inicializa las funciones PKCS#11
                    ' Solo se hace esta operacion una vez por proceso
                    rv = INICIAR_PKCS11()
                    If rv <> 0 Then
                        Throw New Exception("HSMTDES Constructor - error inciando PKCS#11: " & rv.ToString())
                    End If
                    INITIALIZED = True
                End If

                ' Inicializamos los handler de sesión y de llave
                Me.hSession = New StringBuilder("", 32)
                Me.hKey = New StringBuilder("", 32)

                ' Se inicializan los recursos para esta instancia de HSMTDES (sesion, login y busqueda de llave)
                rv = INICIAR_RECURSOS( _
                    New StringBuilder(ConfigurationManager.AppSettings("TokenName")), _
                    New StringBuilder(ConfigurationManager.AppSettings("TokenPassphrase")), _
                    New StringBuilder(ConfigurationManager.AppSettings("NameKeyHSM")), _
                    hSession, _
                    hKey)
                If rv <> 0 Then
                    Throw New Exception("HSMTDES Constructor - error inciando recursos: " & rv.ToString())
                End If
            End SyncLock
        Catch ex As Exception
            Dim operarBD As OperacionesBase = New OperacionesBase()
            operarBD.GuardarLogXNombreConectionString(7, 0, 2, "HSMTDES - TokenName : " + ConfigurationManager.AppSettings("TokenName") +
                                                                " TokenPassphrase " + ConfigurationManager.AppSettings("TokenPassphrase") +
                                                               " NameKeyHSM " + ConfigurationManager.AppSettings("NameKeyHSM") + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    Public Function CifrarDato(ByVal cadena As String) As String Implements IEncrypt.CifrarDato
        SyncLock LockThis
            ' Variables locales
            cadena = Me.ValidarDato(cadena, "00000000000000000000000000000000")
            Dim IV As StringBuilder = New StringBuilder(ConfigurationManager.AppSettings("VI"))
            Dim mode As StringBuilder = New StringBuilder("E")
            Dim data_in As StringBuilder = New StringBuilder(cadena)
            Dim data_out As StringBuilder = New StringBuilder("", cadena.Length + 1)
            Dim rv As Integer = 0

            ' Se llama a la función de la DLL
            rv = TDES(Me.hSession, Me.hKey, data_in, data_out, mode, IV)

            If rv <> 0 Then
                Throw New Exception("HSMTDES: funcion: CifrarDato, err: " & rv.ToString())
            End If
            Return data_out.ToString()
        End SyncLock
    End Function

    Public Function DescifrarDato(ByVal cadenaCifrada As String) As String Implements IEncrypt.DescifrarDato
        SyncLock LockThis
            ' Variables locales
            If cadenaCifrada.Length / 2 Mod 8 <> 0 Then
                Throw New Exception("HSMTDES - DescifrarDato: Tamaño de la cadena no es multiplo de  8")
            End If
            Dim IV As StringBuilder = New StringBuilder(ConfigurationManager.AppSettings("VI"))
            Dim mode As StringBuilder = New StringBuilder("D")
            Dim data_in As StringBuilder = New StringBuilder(cadenaCifrada)
            Dim data_out As StringBuilder = New StringBuilder("", cadenaCifrada.Length + 1)
            Dim rv As Integer = 0

            ' Se llama a la función de la DLL
            rv = TDES(Me.hSession, Me.hKey, data_in, data_out, mode, IV)

            If rv <> 0 Then
                Throw New Exception("HSMTDES: funcion: DescifrarDato, err: " & rv.ToString())
            End If
            Return data_out.ToString()
        End SyncLock
    End Function

    Public Function DigitoChequeo(ByVal tamano As Integer) As String Implements IEncrypt.DigitoChequeo
        SyncLock LockThis
            ' Variables locales
            Dim IV As StringBuilder = New StringBuilder(ConfigurationManager.AppSettings("VI"))
            Dim mode As StringBuilder = New StringBuilder("E")
            Dim data_in As StringBuilder = New StringBuilder("00000000000000000000000000000000")
            Dim data_out As StringBuilder = New StringBuilder("", 32 + 1)
            Dim rv As Integer = 0

            ' Se llama a la función de la DLL
            rv = TDES(Me.hSession, Me.hKey, data_in, data_out, mode, IV)

            If rv <> 0 Then
                Throw New Exception("HSMTDES: funcion: DigitoChequeo, err: " & rv.ToString())
            End If

            Return data_out.ToString().Substring(0, tamano)
        End SyncLock
    End Function

    ''' <summary>
    ''' Metodo encargado de validar las llaves
    ''' </summary>
    ''' <param name="cadenaCifrada">Cadena en hexadecimal</param>
    ''' <param name="key">Llave a validar</param>
    ''' <remarks>AERM 15/09/2015 </remarks>
    Private Function ValidarDato(ByVal cadenaCifrada As String, ByVal key As String) As String
        If (cadenaCifrada.Length / 2) Mod 8 <> 0 Then cadenaCifrada = Me.RellenarCadena(cadenaCifrada, key)
        If key.Length <> (16 * 2) Then Throw New Exception("El tamaño  del DES3 key no es de 16")

        Return cadenaCifrada
    End Function

    ''' <summary>
    ''' Metodo encargado de rellenar con 0 los valores faltantes del octeto
    ''' </summary>
    ''' <param name="cadena">Cadena a rellenar</param>
    ''' <param name="p2">llave </param>
    ''' <returns>Retorna la cadena con los valores resultantes</returns>
    ''' <remarks>AERM 15/09/2015</remarks>
    Private Function RellenarCadena(ByVal cadena As String, ByVal p2 As String) As String
        Try
            While (Me.ValidarTamanoDato(cadena, 8))
                cadena += "0"
            End While
        Catch ex As Exception
            Throw New Exception("Tamaño de la cadena no es multiplo de  8")
        End Try

        Return cadena
    End Function

    ''' <summary>
    ''' valida que el tamaño del dato sea múltiplo de 8
    ''' </summary>
    ''' <param name="cadena"></param>
    ''' <param name="valorValidar"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function ValidarTamanoDato(ByVal cadena As String, ByVal valorValidar As Integer) As Boolean
        If (cadena.Length / 2) Mod valorValidar <> 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Protected Overrides Sub Finalize()
        SyncLock LockThis
            FINALIZAR_RECURSOS(Me.hSession)
        End SyncLock
    End Sub
End Class
