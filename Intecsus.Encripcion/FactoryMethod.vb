﻿''' <summary>
''' Clase encargada de instanciar la forma de cifra o des-cifrar
''' </summary>
''' <remarks>AERM 14/09/2015</remarks>
Public Class FactoryMethod


    ''' <summary>
    ''' Metodo que se encarga de instaciar el metodo
    ''' </summary>
    ''' <param name="programEncript">Clase que debe instanciar</param>
    ''' <returns>Retorna implementación de la interface</returns>
    ''' <remarks>AERM 14/09/2015</remarks>
    Public Function DevolverClase(programEncript As String) As IEncrypt
        Try
            Select Case programEncript
                'Case "HSMTDES"
                '    Return HSMTDES.Instancia()
                Case "HSMShield"
                    Return HSMShield.Instancia()
                Case Else
                    Return New TDES()
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
