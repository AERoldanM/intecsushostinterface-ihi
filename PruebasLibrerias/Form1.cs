﻿using Intecsus.Entidades;
using Intecsus.MSMQ;
using Intecsus.OperarArchivos;
using Intecsus.CW;
using Intecusus.OperarBD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Intecsus.CW.TCP;
using System.Messaging;
using System.Xml.Serialization;
using Intecsus.OperarXML;
 using System.Web.Mail;
using Intecsus.OperarCMS;
using System.Threading;
using System.Diagnostics;
using Intecsus.Utilidades;

namespace PruebasLibrerias
{
    public partial class Form1 : Form
    {
        public WinSockServer WinSockServer1 = new WinSockServer();
        public TCPClient prueba2;
        IOperacion operar = new OperacionesBaseCW();

        List<ParametrosXML> parametros = new List<ParametrosXML>();
        public Form1()
        {
            InitializeComponent();
            String texto = "HOLAMUNDO";
            Intecsus.OperarWS.Implementaciones.ImplementacionSantander m_Santander = new Intecsus.OperarWS.Implementaciones.ImplementacionSantander();
            m_Santander.RespuestaRealce("0004", "9007", "00000001579893742016-09-19-10.40.45.223994", "0001", "OK", "0", string.Empty, string.Empty, 0);
            //GeneradorXML xml = new  GeneradorXML();
            //RespuestaWS m_RespuestaWS = new RespuestaWS();
            //m_RespuestaWS.codigo = "000";
            //m_RespuestaWS.descripcion = "ERRE";
            // string objetoXML = xml.Serializar(m_RespuestaWS);
            //string valor = "JUAN PABLO CASTEL";
            //valor = "Por enviar a Notificacion";
            //if (valor.Contains("Notificaci"))
            //{
            //    valor = "Notificacion-OK";
            //    if (valor.Contains("Notificaci"))
            //    {
            //        valor = "OperarCMS.Procesos.vb - EnviarNotificaciones : ";
            //        if (valor.Contains("Notificaci"))
            //        {
            //            valor = " ST_ERRORNotificacion";
            //            if (valor.Contains("Notificaci"))
            //            { MessageBox.Show("SI"); }
            //        }
            //       // MessageBox.Show(prueba.StringToBarcode("1111"));
            //    }

            //}
            //MessageBox.Show(valor.Substring(valor.Length - 4, 4));
            //WinSockServer1.PuertoDeEscucha = this.txtPuerto.Text;
            //WinSockServer1.DatosRecibidos += WinSockServer_DatosRecibidos;
            //WinSockServer1.NuevaConexion += WinSockServer_NuevaConexion;
            //WinSockServer1.ConexionTerminada += WinSockServer_ConexionTerminada;
            ////Comienzo la escucha
            //WinSockServer1.Escuchar();

            //PruebaColsubsidio.ColsubsidioServiceClient prueba = new PruebaColsubsidio.ColsubsidioServiceClient();
            //PruebaColsubsidio.MyHeader encabezado = new PruebaColsubsidio.MyHeader();
            //encabezado.UserName = "prueba";
            //encabezado.Password = "123";
            ////encabezado.MustUnderstand  = false;
            ////encabezado.Relay  = false;
            ////encabezado.DidUnderstand = false;
            ////encabezado.EncodedMustUnderstand = "0";
            //PruebaColsubsidio.Tarjeta tarejta = new PruebaColsubsidio.Tarjeta();
            //PruebaColsubsidio.Mensaje resp = prueba.ActualizacionEstadoTarjeta(ref encabezado, tarejta);
            //MessageBox.Show(resp.cod_respuesta + " " + resp.respuesta);
            //Peticion peti = new Peticion();
            //peti.numeroProducto = "Miprueba";
            // GeneradorXML generador = new GeneradorXML();
            // String xml = generador.Serializar<Peticion>(peti);
            // XmlDocument xm = new XmlDocument();
            // xm.LoadXml(xml);
            // var variable = xm.SelectSingleNode("/Peticion/numeroProducto");

            //MessageBox.Show(variable.InnerText);
            operar.RecibeMensaje += operar_RecibeMensaje;
            //ConfInicio ini = new ConfInicio();
        }

        public void operar_RecibeMensaje(object sender, RespuestaDeviceStatus e)
        {
            MessageBox.Show(e.Mensaje);
        }

        #region COlas
        private void btnCrearCola_Click(object sender, EventArgs e)
        {
            try
            {
                ICola cola = new InstanciaCola().IntanciarCola("MSMQ");
                bool crearCola = cola.CrearCola(this.txtNameCola.Text);
                MessageBox.Show(crearCola.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnMensajeCola_Click(object sender, EventArgs e)
        {
            try
            {
                ICola cola = new InstanciaCola().IntanciarCola("MSMQ");
                bool mensajeCola = cola.EnviarMensajeCola(this.txtNameCola.Text, this.txtMensajeCola.Text);
                MessageBox.Show(mensajeCola.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnResCola_Click(object sender, EventArgs e)
        {
            try
            {
                ICola cola = new InstanciaCola().IntanciarCola("MSMQ");
                string mensajeCola = cola.RecibirMensaje(this.txtNameCola.Text);
                this.txtRespuesta.Text = mensajeCola;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEliminarCola_Click(object sender, EventArgs e)
        {
            try
            {
                Intecsus.Integrador.Service1 servicio = new Intecsus.Integrador.Service1();
                servicio.TestStartupAndStop(null);
                OperacionesBase operacionesBD = new OperacionesBase();
                EstadoTransaccion key = operacionesBD.ObtenerNoticiXSha("ABC1", "CHIPPruebaWindows", "COLSUBSIDIO", ConfigurationManager.AppSettings["BD"]);
                operacionesBD.IngresarValorSha( 1, ConfigurationManager.AppSettings["BD"], "SHADesde windows");
                ICola cola = new InstanciaCola().IntanciarCola("MSMQ");
                bool mensajeCola = cola.EliminarCola(this.txtNameCola.Text);
                MessageBox.Show(mensajeCola.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEnviarObjeto_Click(object sender, EventArgs e)
        {
            try
            {
                ICola cola = new InstanciaCola().IntanciarCola("MSMQ");
                bool crearCola = cola.CrearCola(this.txtNameCola.Text);
                MessageBox.Show("Creacion cola " + crearCola.ToString());
                DatosCardWizard _objetoCW = new DatosCardWizard();
                _objetoCW.PCName = this.txtPCNAME.Text;
                bool mensajeCola = cola.GuardarObjetoCardWizard(_objetoCW, this.txtNameCola.Text);
                MessageBox.Show("Guarda mensaje " + mensajeCola.ToString());
                _objetoCW = new DatosCardWizard();
                if( cola.RecibirObjetoCardWizard(this.txtNameCola.Text, ref _objetoCW))
                    MessageBox.Show("Obtiene mensaje : " + _objetoCW.PCName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion

        #region Archivos

        private void btnLeer_Click(object sender, EventArgs e)
        {
            try
            {
                ServicioColsubsidio.ActualizacionEstadoTarjeta_OutClient cliente = new ServicioColsubsidio.ActualizacionEstadoTarjeta_OutClient("HTTP_Port");
                ServicioColsubsidio.RequestEstadoTarjetaOut resp = new ServicioColsubsidio.RequestEstadoTarjetaOut();
                cliente.Actualizacion_Out(resp);
              
                ICArchivos operar = new RetornarArchivo().IntanciarArchivo("TXT");
                operar.AgregarLineaArchivo(@"C:\Archivo", "notificacion.txt", "hola");
                List<string> lineasArchivo = operar.ObtenerInformacioArchivo(this.txtRutaArchivo.Text, this.txtLeerArchivo.Text);
                this.txtRespuesta.Text = string.Empty;
                foreach (string linea in lineasArchivo)
                {
                    this.txtRespuesta.Text += string.Format("{0} {1}", linea, System.Environment.NewLine);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }



        private void txtCopiarArchivo_Click(object sender, EventArgs e)
        {
            try
            {
                ICArchivos operar = new RetornarArchivo().IntanciarArchivo("TXT");
                bool resultado = operar.CopiarArchivo(this.txtRutaArchivo.Text, this.txtLeerArchivo.Text, this.txtRutaCopia.Text, this.txtCopiaArchi.Text);
                MessageBox.Show(resultado.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                ICArchivos operar = new RetornarArchivo().IntanciarArchivo("TXT");
                bool resultado = operar.EliminarArchivo(this.txtRutaCopia.Text + @"\" + this.txtCopiaArchi.Text);
                MessageBox.Show(resultado.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        #endregion

        private void btnLog_Click(object sender, EventArgs e)
        {
            BarcodeConverter128 prueba = new BarcodeConverter128();

            MessageBox.Show(prueba.StringToBarcode("1111"));
            //MailMessage msg = new MailMessage();
            //msg.To = "devjoker@djk.com";
            //msg.From = "aroldan@idenpla.com.co";
            //msg.Subject = "El asunto del mail";
            //msg.Body = "Este es el contenido del email";
            //msg.Priority = MailPriority.High;
            //msg.BodyFormat = MailFormat.Text; //o MailFormat.Html 
            //SmtpMail.SmtpServer = "WIN02"; // El servidor de correo

            //try
            //{

            //    SmtpMail.Send(msg);

            //}

            //catch (Exception ex)
            //{

            //    MessageBox.Show(ex.Message);

            //} 

            try
            {
                OperacionesBase _operar = new OperacionesBase();
                _operar.GuardarLogXCadena(1, 1, 1, this.txtErrorLog.Text, ConfigurationManager.ConnectionStrings["SQLServer"].ConnectionString);
                MessageBox.Show("Se agrego log");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCrearSession_Click(object sender, EventArgs e)
        {
            try
            {
                
                string respuesta = string.Empty;
                this.txtRespuesta.Text = operar.CrearSession(this.txtServidor.Text, Int32.Parse(this.txtPuerto.Text), respuesta,
                                                    this.txtUserName.Text, this.txtPassword.Text, "STD", "CW_XML_Interface");
                this.txtSessionID.Text = this.txtRespuesta.Text;
               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnCerrarSession_Click(object sender, EventArgs e)
        {
            try
            {
                
                string respuesta = string.Empty;
                bool cierre = operar.CerrarSession(this.txtServidor.Text, Int32.Parse(this.txtPuerto.Text), respuesta, this.txtSessionID.Text,
                                                             "CW_XML_Interface");
                if (cierre)
                {
                    this.txtRespuesta.Text = string.Empty;
                    MessageBox.Show("Se cerro correctamente");
                }
                else
                {
                    MessageBox.Show("No se cerro correctamente");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
               
                DatosCardWizard tarjeta = new DatosCardWizard();
                tarjeta.CardWizardName = this.txtCardFormat.Text;
                tarjeta.PCName = this.txtPCNameC.Text;
                tarjeta.Tipo_Producto = int.Parse(this.txtTipoC.Text);
                if (parametros.Count == 0)
                {
                    //ParametrosXML parametro = new ParametrosXML();
                    //parametro.Nombre = "DATAITEM";
                    //parametro.NameCard = "@FIRST_NAME@";
                    //parametro.Typo = "string";
                    //parametro.Valor = new List<Valor>(new Valor( "ANDRES";
                    //parametros.Add(parametro);
                    //parametro = new ParametrosXML();
                    //parametro.Nombre = "DATAITEM";
                    //parametro.NameCard = "@PAN@";
                    //parametro.Typo = "string";
                    //parametro.Valor = "4247771234567893";
                    //parametros.Add(parametro);
                    //parametro = new ParametrosXML();
                    //parametro.Nombre = "DATAITEM";
                    //parametro.NameCard = "@ExpirationDate@";
                    //parametro.Typo = "string";
                    //parametro.Valor = "03/10";
                    //parametros.Add(parametro);
                    //parametro = new ParametrosXML();
                    //parametro.Nombre = "DATAITEM";
                    //parametro.NameCard = "@PIN@";
                    //parametro.Typo = "string";
                    //parametro.Valor = "1234";
                    //parametros.Add(parametro);
                }

                List<ParametrosXML> parametros2 = new List<ParametrosXML>();
                ParametrosXML parametro2 = new ParametrosXML();
                //parametro2.Nombre = "DATAITEM";
                //parametro2.NameCard = "@FIRST_NAME@";
                //parametro2.Typo = "string";
                //parametro2.Valor = "Felipe";
                //parametros2.Add(parametro2);

                parametros2.AddRange(parametros);
                
                tarjeta.Parametros = parametros;

                
                var stringwriter = new System.IO.StringWriter();
                var serializer = new XmlSerializer(typeof(ParametrosXML));
                serializer.Serialize(stringwriter, parametro2);
                string respuesta = this.Serialize<ParametrosXML>(parametro2);
                GeneradorXML generador = new GeneradorXML();
                respuesta = generador.Serializar<ParametrosXML>(parametro2);
                    //stringwriter.ToString();

                var stringReader = new System.IO.StringReader(respuesta);
                var serializer2 = new XmlSerializer(typeof(ParametrosXML));
                var parametroCOnver = this.Deserialize<ParametrosXML>(respuesta);
                parametroCOnver = generador.Deserialize<ParametrosXML>(respuesta);
                    //(ParametrosXML)serializer2.Deserialize(stringReader);
                MessageBox.Show(parametroCOnver.Nombre);

                //Validamos estado de la impresora antes
                RespuestaEstado respuestaEstado = operar.EstadoXPCName(this.txtServidor.Text, Int32.Parse(this.txtPuerto.Text), respuesta, this.txtSessionID.Text,
                                                                        this.txtPCNameC.Text, "CW_XML_Interface", this.txtCardFormat.Text, 1);

                switch (respuestaEstado.estado)
                {
                    case ESTADO_REALCE.ST_OK:
                        bool cierre = operar.HacerTarjeta(this.txtServidor.Text, Int32.Parse(this.txtPuerto.Text), respuesta, this.txtUserName.Text,
                                              this.txtSessionID.Text, "1", "CW_XML_Interface", tarjeta);
                        parametros = new List<ParametrosXML>();
                        if (cierre) { MessageBox.Show("Se envio a imprimir"); } else { MessageBox.Show("No se envio a imprimir"); }
                        //Establezco el puerto donde escuchar
                       
                         break;
                    default:
                        MessageBox.Show(respuestaEstado.status + "  " + respuestaEstado.mensaje);
                        break;
                }



            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnAnadParam_Click(object sender, EventArgs e)
        {
            ParametrosXML parametro = new ParametrosXML();
            parametro.Nombre = this.txtNameP.Text;
            parametro.NameCard = this.txtNameCardP.Text;
            parametro.Typo = this.txtTipoP.Text;
            //parametro.Valor = this.txtValorP.Text;
            parametros.Add(parametro);
        }

        private void btnMensajeCard_Click(object sender, EventArgs e)
        {
            try {
              
                string respuesta = string.Empty;
                XmlDocument respuestaEstado = operar.MensajesEventos(this.txtServidor.Text, Int32.Parse(this.txtPuerto.Text), respuesta, this.txtSessionID.Text,
                                                                    "CW_XML_Interface");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnConfigurador_Click(object sender, EventArgs e)
        {
            
            OperacionesBase bases = new OperacionesBase();
            String valores = string.Empty;
             List<String> m_List = bases.ObtenerCardFormatName("COLSUBSIDIO", "CE870", "SqlServer");
            
            if(m_List.Count >= 2)
            {
                string peticion =  m_List[1];

                foreach (XmlNode m_xnode in ObtenerXMList("/Parametros/Parametro", null, true, peticion, new XmlDocument())) 
                 {
                     valores += " Parametro N ";
                     //XmlNodeList m_xnList2 = m_xnode.SelectNodes("Valor");
                     foreach (XmlNode m_xnode2 in ObtenerXMList("Valor", m_xnode) )
                     {
                         valores += "- " + m_xnode2.Attributes["valor"].Value;
                     }
                 }
            }

            MessageBox.Show(valores);

           
            DateTime fechaCreada = new DateTime(2015, 03,02);
            DateTime fechaactual = DateTime.Now;
            TimeSpan fechaHoras = fechaactual.Subtract(fechaCreada);
            int horas = fechaHoras.Hours;
            horas.ToString();
            MessageBox.Show("hola" + horas.ToString());
            MessageBox.Show(bases.ObtenerValoresConfigurador("CantReintentos", "SqlServer"));
        }

        private XmlNodeList ObtenerXMList(string nodo,XmlNode xmlNode = null, bool obtenerRespuesta = false, String xmlPeticion = null, XmlDocument m_xml = null)
        {
            if ((obtenerRespuesta))
            {
                m_xml = new XmlDocument();
                m_xml.LoadXml(xmlPeticion);
                return m_xml.SelectNodes(nodo);
            }
            else { return xmlNode.SelectNodes(nodo); }
            
        }

        public string Serialize<T>(T dataToSerialize)
        {
            try
            {
                var stringwriter = new System.IO.StringWriter();
                var serializer = new XmlSerializer(typeof(T));
                serializer.Serialize(stringwriter, dataToSerialize);
                return stringwriter.ToString();
            }
            catch
            {
                throw;
            }
        }

        public  T Deserialize<T>(string xmlText)
        {
            try
            {
                var stringReader = new System.IO.StringReader(xmlText);
                var serializer = new XmlSerializer(typeof(T));
                return (T)serializer.Deserialize(stringReader);
            }
            catch
            {
                throw;
            }
        }

        private void btnRedireccionar_Click(object sender, EventArgs e)
        {
            ClientesyNotificaciones frm = new ClientesyNotificaciones();
            frm.Show();
        }

        private void btnHilos_Click(object sender, EventArgs e)
        {
            VisorEventos visorEventos = new VisorEventos();
            visorEventos.GuardarLog("Application", "Intecsus.BD", "EjecutarSP Prueba error", EventLogEntryType.Error);
            for(int i =0; i <= 100; i ++)
            {
                ThreadStart m_Delegado = new ThreadStart(() => this.GuardarOperacionBD(i));
                Thread m_Hilo = new Thread(m_Delegado);
                m_Hilo.Start();
            }
            MessageBox.Show("Termino Proceso secuencias hilos");
        }

        private void GuardarOperacionBD(int valor) 
        {
            Random ra = new Random();
            int v = ra.Next(0,1000);
            string secuencias = v + "Num" + valor;
            OperacionesBase operacionesBD = new OperacionesBase();
            operacionesBD.GuardarLogXNombreConectionString(11, v, 1, secuencias,
                                  ConfigurationManager.AppSettings["BD"]);
            int secuencia = operacionesBD.ObtenerSecuencia(ConfigurationManager.AppSettings["BD"]);
            operacionesBD.GuardarLogXNombreConectionString(11, v, 2, secuencias,
                                ConfigurationManager.AppSettings["BD"]);
        } 
    }
}
