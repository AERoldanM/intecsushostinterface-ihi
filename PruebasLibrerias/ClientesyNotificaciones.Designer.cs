﻿namespace PruebasLibrerias
{
    partial class ClientesyNotificaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNombreLargo = new System.Windows.Forms.TextBox();
            this.lblPAN = new System.Windows.Forms.Label();
            this.txtPAN = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtExpiracion = new System.Windows.Forms.TextBox();
            this.lblPin = new System.Windows.Forms.Label();
            this.txtPIN = new System.Windows.Forms.TextBox();
            this.btnCliente = new System.Windows.Forms.Button();
            this.lblTransaccion = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnImpresion = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.btnNotificaciones = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTipoProduct = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPCNAME = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.lblArchivo = new System.Windows.Forms.Label();
            this.btnArchivo = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.txtCadenaCifrada = new System.Windows.Forms.TextBox();
            this.txtResultadoEncripcion = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtDesencripcion = new System.Windows.Forms.TextBox();
            this.btnEncriptar = new System.Windows.Forms.Button();
            this.btnIntegrador = new System.Windows.Forms.Button();
            this.btnNotificar = new System.Windows.Forms.Button();
            this.btnInvocar = new System.Windows.Forms.Button();
            this.txtCifrar = new System.Windows.Forms.TextBox();
            this.txtxDesC = new System.Windows.Forms.TextBox();
            this.txtHexa = new System.Windows.Forms.TextBox();
            this.txtResul1 = new System.Windows.Forms.TextBox();
            this.txtResult2 = new System.Windows.Forms.TextBox();
            this.btnCIfrar = new System.Windows.Forms.Button();
            this.btnDesc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(29, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Guardar Cliente en la cola";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombre largo: ";
            // 
            // txtNombreLargo
            // 
            this.txtNombreLargo.Location = new System.Drawing.Point(95, 39);
            this.txtNombreLargo.Name = "txtNombreLargo";
            this.txtNombreLargo.Size = new System.Drawing.Size(160, 20);
            this.txtNombreLargo.TabIndex = 2;
            this.txtNombreLargo.Text = "Prueba Nasugo";
            // 
            // lblPAN
            // 
            this.lblPAN.AutoSize = true;
            this.lblPAN.Location = new System.Drawing.Point(261, 42);
            this.lblPAN.Name = "lblPAN";
            this.lblPAN.Size = new System.Drawing.Size(29, 13);
            this.lblPAN.TabIndex = 3;
            this.lblPAN.Text = "PAN";
            // 
            // txtPAN
            // 
            this.txtPAN.Location = new System.Drawing.Point(296, 39);
            this.txtPAN.Name = "txtPAN";
            this.txtPAN.Size = new System.Drawing.Size(150, 20);
            this.txtPAN.TabIndex = 4;
            this.txtPAN.Text = "4247771234567893";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(452, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Fecha";
            // 
            // txtExpiracion
            // 
            this.txtExpiracion.Location = new System.Drawing.Point(496, 38);
            this.txtExpiracion.Name = "txtExpiracion";
            this.txtExpiracion.Size = new System.Drawing.Size(37, 20);
            this.txtExpiracion.TabIndex = 6;
            this.txtExpiracion.Text = "03/10";
            // 
            // lblPin
            // 
            this.lblPin.AutoSize = true;
            this.lblPin.Location = new System.Drawing.Point(539, 42);
            this.lblPin.Name = "lblPin";
            this.lblPin.Size = new System.Drawing.Size(25, 13);
            this.lblPin.TabIndex = 7;
            this.lblPin.Text = "PIN";
            // 
            // txtPIN
            // 
            this.txtPIN.Location = new System.Drawing.Point(570, 38);
            this.txtPIN.Name = "txtPIN";
            this.txtPIN.Size = new System.Drawing.Size(32, 20);
            this.txtPIN.TabIndex = 8;
            this.txtPIN.Text = "1234";
            // 
            // btnCliente
            // 
            this.btnCliente.Location = new System.Drawing.Point(689, 39);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(143, 23);
            this.btnCliente.TabIndex = 9;
            this.btnCliente.Text = "Agregar Cliente";
            this.btnCliente.UseVisualStyleBackColor = true;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // lblTransaccion
            // 
            this.lblTransaccion.AutoSize = true;
            this.lblTransaccion.Location = new System.Drawing.Point(602, 22);
            this.lblTransaccion.Name = "lblTransaccion";
            this.lblTransaccion.Size = new System.Drawing.Size(101, 13);
            this.lblTransaccion.TabIndex = 10;
            this.lblTransaccion.Text = "Transaccion Cliente";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 196);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(107, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "---Enviar a imprimir-----";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 245);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(336, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Recuerda la configuracion previamente que debe existir en el servidor";
            // 
            // btnImpresion
            // 
            this.btnImpresion.Location = new System.Drawing.Point(374, 245);
            this.btnImpresion.Name = "btnImpresion";
            this.btnImpresion.Size = new System.Drawing.Size(134, 23);
            this.btnImpresion.TabIndex = 13;
            this.btnImpresion.Text = "Proceso Impresion";
            this.btnImpresion.UseVisualStyleBackColor = true;
            this.btnImpresion.Click += new System.EventHandler(this.btnImpresion_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 91);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(263, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "---Recibir  clientes creados y notificaciones procesos---";
            // 
            // btnNotificaciones
            // 
            this.btnNotificaciones.Location = new System.Drawing.Point(18, 107);
            this.btnNotificaciones.Name = "btnNotificaciones";
            this.btnNotificaciones.Size = new System.Drawing.Size(171, 23);
            this.btnNotificaciones.TabIndex = 15;
            this.btnNotificaciones.Text = "Obtner CLientes y Notificaciones";
            this.btnNotificaciones.UseVisualStyleBackColor = true;
            this.btnNotificaciones.Click += new System.EventHandler(this.btnNotificaciones_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 75);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "TipoProduct";
            // 
            // txtTipoProduct
            // 
            this.txtTipoProduct.Location = new System.Drawing.Point(95, 68);
            this.txtTipoProduct.Name = "txtTipoProduct";
            this.txtTipoProduct.Size = new System.Drawing.Size(29, 20);
            this.txtTipoProduct.TabIndex = 17;
            this.txtTipoProduct.Text = "1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(156, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(50, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "PCNAme";
            // 
            // txtPCNAME
            // 
            this.txtPCNAME.Location = new System.Drawing.Point(222, 67);
            this.txtPCNAME.Name = "txtPCNAME";
            this.txtPCNAME.Size = new System.Drawing.Size(143, 20);
            this.txtPCNAME.TabIndex = 19;
            this.txtPCNAME.Text = "WIN-4H1I0FLTUGA";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 146);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(223, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "-- Obtener ClientesX Archivo --- Ruta Archivo: ";
            // 
            // lblArchivo
            // 
            this.lblArchivo.AutoSize = true;
            this.lblArchivo.Location = new System.Drawing.Point(264, 146);
            this.lblArchivo.Name = "lblArchivo";
            this.lblArchivo.Size = new System.Drawing.Size(0, 13);
            this.lblArchivo.TabIndex = 21;
            // 
            // btnArchivo
            // 
            this.btnArchivo.Location = new System.Drawing.Point(32, 163);
            this.btnArchivo.Name = "btnArchivo";
            this.btnArchivo.Size = new System.Drawing.Size(133, 23);
            this.btnArchivo.TabIndex = 22;
            this.btnArchivo.Text = "Obtener Datos Archivo";
            this.btnArchivo.UseVisualStyleBackColor = true;
            this.btnArchivo.Click += new System.EventHandler(this.btnArchivo_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(18, 300);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(151, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Obtener dato Encriptado Hash";
            // 
            // txtCadenaCifrada
            // 
            this.txtCadenaCifrada.Location = new System.Drawing.Point(21, 317);
            this.txtCadenaCifrada.Name = "txtCadenaCifrada";
            this.txtCadenaCifrada.Size = new System.Drawing.Size(185, 20);
            this.txtCadenaCifrada.TabIndex = 24;
            // 
            // txtResultadoEncripcion
            // 
            this.txtResultadoEncripcion.Location = new System.Drawing.Point(296, 310);
            this.txtResultadoEncripcion.Name = "txtResultadoEncripcion";
            this.txtResultadoEncripcion.Size = new System.Drawing.Size(193, 20);
            this.txtResultadoEncripcion.TabIndex = 25;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(222, 317);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 13);
            this.label11.TabIndex = 26;
            this.label11.Text = "Resultado";
            // 
            // txtDesencripcion
            // 
            this.txtDesencripcion.Location = new System.Drawing.Point(528, 310);
            this.txtDesencripcion.Name = "txtDesencripcion";
            this.txtDesencripcion.Size = new System.Drawing.Size(165, 20);
            this.txtDesencripcion.TabIndex = 27;
            // 
            // btnEncriptar
            // 
            this.btnEncriptar.Location = new System.Drawing.Point(725, 310);
            this.btnEncriptar.Name = "btnEncriptar";
            this.btnEncriptar.Size = new System.Drawing.Size(75, 23);
            this.btnEncriptar.TabIndex = 28;
            this.btnEncriptar.Text = "Encriptar";
            this.btnEncriptar.UseVisualStyleBackColor = true;
            this.btnEncriptar.Click += new System.EventHandler(this.btnEncriptar_Click);
            // 
            // btnIntegrador
            // 
            this.btnIntegrador.Location = new System.Drawing.Point(32, 354);
            this.btnIntegrador.Name = "btnIntegrador";
            this.btnIntegrador.Size = new System.Drawing.Size(137, 23);
            this.btnIntegrador.TabIndex = 29;
            this.btnIntegrador.Text = "inicializa el integrador";
            this.btnIntegrador.UseVisualStyleBackColor = true;
            this.btnIntegrador.Click += new System.EventHandler(this.btnIntegrador_Click);
            // 
            // btnNotificar
            // 
            this.btnNotificar.Location = new System.Drawing.Point(222, 354);
            this.btnNotificar.Name = "btnNotificar";
            this.btnNotificar.Size = new System.Drawing.Size(98, 23);
            this.btnNotificar.TabIndex = 30;
            this.btnNotificar.Text = "AgregarNotificacion";
            this.btnNotificar.UseVisualStyleBackColor = true;
            this.btnNotificar.Click += new System.EventHandler(this.btnNotificar_Click);
            // 
            // btnInvocar
            // 
            this.btnInvocar.Location = new System.Drawing.Point(351, 354);
            this.btnInvocar.Name = "btnInvocar";
            this.btnInvocar.Size = new System.Drawing.Size(120, 23);
            this.btnInvocar.TabIndex = 31;
            this.btnInvocar.Text = "invocar servicio";
            this.btnInvocar.UseVisualStyleBackColor = true;
            this.btnInvocar.Click += new System.EventHandler(this.btnInvocar_Click);
            // 
            // txtCifrar
            // 
            this.txtCifrar.Location = new System.Drawing.Point(32, 411);
            this.txtCifrar.Name = "txtCifrar";
            this.txtCifrar.Size = new System.Drawing.Size(174, 20);
            this.txtCifrar.TabIndex = 32;
            // 
            // txtxDesC
            // 
            this.txtxDesC.Location = new System.Drawing.Point(287, 411);
            this.txtxDesC.Name = "txtxDesC";
            this.txtxDesC.Size = new System.Drawing.Size(212, 20);
            this.txtxDesC.TabIndex = 33;
            // 
            // txtHexa
            // 
            this.txtHexa.Location = new System.Drawing.Point(32, 462);
            this.txtHexa.Name = "txtHexa";
            this.txtHexa.Size = new System.Drawing.Size(174, 20);
            this.txtHexa.TabIndex = 34;
            // 
            // txtResul1
            // 
            this.txtResul1.Location = new System.Drawing.Point(286, 462);
            this.txtResul1.Name = "txtResul1";
            this.txtResul1.Size = new System.Drawing.Size(203, 20);
            this.txtResul1.TabIndex = 35;
            // 
            // txtResult2
            // 
            this.txtResult2.Location = new System.Drawing.Point(528, 461);
            this.txtResult2.Name = "txtResult2";
            this.txtResult2.Size = new System.Drawing.Size(272, 20);
            this.txtResult2.TabIndex = 36;
            // 
            // btnCIfrar
            // 
            this.btnCIfrar.Location = new System.Drawing.Point(528, 411);
            this.btnCIfrar.Name = "btnCIfrar";
            this.btnCIfrar.Size = new System.Drawing.Size(75, 23);
            this.btnCIfrar.TabIndex = 37;
            this.btnCIfrar.Text = "cifrar";
            this.btnCIfrar.UseVisualStyleBackColor = true;
            this.btnCIfrar.Click += new System.EventHandler(this.btnCIfrar_Click);
            // 
            // btnDesc
            // 
            this.btnDesc.Location = new System.Drawing.Point(635, 410);
            this.btnDesc.Name = "btnDesc";
            this.btnDesc.Size = new System.Drawing.Size(75, 23);
            this.btnDesc.TabIndex = 38;
            this.btnDesc.Text = "des";
            this.btnDesc.UseVisualStyleBackColor = true;
            this.btnDesc.Click += new System.EventHandler(this.btnDesc_Click);
            // 
            // ClientesyNotificaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(874, 523);
            this.Controls.Add(this.btnDesc);
            this.Controls.Add(this.btnCIfrar);
            this.Controls.Add(this.txtResult2);
            this.Controls.Add(this.txtResul1);
            this.Controls.Add(this.txtHexa);
            this.Controls.Add(this.txtxDesC);
            this.Controls.Add(this.txtCifrar);
            this.Controls.Add(this.btnInvocar);
            this.Controls.Add(this.btnNotificar);
            this.Controls.Add(this.btnIntegrador);
            this.Controls.Add(this.btnEncriptar);
            this.Controls.Add(this.txtDesencripcion);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtResultadoEncripcion);
            this.Controls.Add(this.txtCadenaCifrada);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.btnArchivo);
            this.Controls.Add(this.lblArchivo);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtPCNAME);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtTipoProduct);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnNotificaciones);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.btnImpresion);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblTransaccion);
            this.Controls.Add(this.btnCliente);
            this.Controls.Add(this.txtPIN);
            this.Controls.Add(this.lblPin);
            this.Controls.Add(this.txtExpiracion);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtPAN);
            this.Controls.Add(this.lblPAN);
            this.Controls.Add(this.txtNombreLargo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "ClientesyNotificaciones";
            this.Text = "ClientesyNotificaciones";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNombreLargo;
        private System.Windows.Forms.Label lblPAN;
        private System.Windows.Forms.TextBox txtPAN;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtExpiracion;
        private System.Windows.Forms.Label lblPin;
        private System.Windows.Forms.TextBox txtPIN;
        private System.Windows.Forms.Button btnCliente;
        private System.Windows.Forms.Label lblTransaccion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnImpresion;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnNotificaciones;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtTipoProduct;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPCNAME;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblArchivo;
        private System.Windows.Forms.Button btnArchivo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtCadenaCifrada;
        private System.Windows.Forms.TextBox txtResultadoEncripcion;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtDesencripcion;
        private System.Windows.Forms.Button btnEncriptar;
        private System.Windows.Forms.Button btnIntegrador;
        private System.Windows.Forms.Button btnNotificar;
        private System.Windows.Forms.Button btnInvocar;
        private System.Windows.Forms.TextBox txtCifrar;
        private System.Windows.Forms.TextBox txtxDesC;
        private System.Windows.Forms.TextBox txtHexa;
        private System.Windows.Forms.TextBox txtResul1;
        private System.Windows.Forms.TextBox txtResult2;
        private System.Windows.Forms.Button btnCIfrar;
        private System.Windows.Forms.Button btnDesc;

    }
}