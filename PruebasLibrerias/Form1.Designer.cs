﻿namespace PruebasLibrerias
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnCrearCola = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNameCola = new System.Windows.Forms.TextBox();
            this.txtMensajeCola = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnMensajeCola = new System.Windows.Forms.Button();
            this.txtRespuesta = new System.Windows.Forms.TextBox();
            this.btnResCola = new System.Windows.Forms.Button();
            this.txtRutaArchivo = new System.Windows.Forms.TextBox();
            this.txtLeerArchivo = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRutaCopia = new System.Windows.Forms.TextBox();
            this.txtCopiaArchi = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnLeer = new System.Windows.Forms.Button();
            this.txtCopiarArchivo = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnEliminarCola = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtErrorLog = new System.Windows.Forms.TextBox();
            this.btnLog = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPCNAME = new System.Windows.Forms.TextBox();
            this.btnEnviarObjeto = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txtServidor = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtPuerto = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.btnCrearSession = new System.Windows.Forms.Button();
            this.btnCerrarSession = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.txtPCNameC = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtCardFormat = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtTipoC = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtNameP = new System.Windows.Forms.TextBox();
            this.txtNameCardP = new System.Windows.Forms.TextBox();
            this.txtTipoP = new System.Windows.Forms.TextBox();
            this.txtValorP = new System.Windows.Forms.TextBox();
            this.btnAnadParam = new System.Windows.Forms.Button();
            this.label21 = new System.Windows.Forms.Label();
            this.txtSessionID = new System.Windows.Forms.TextBox();
            this.btnMensajeCard = new System.Windows.Forms.Button();
            this.btnConfigurador = new System.Windows.Forms.Button();
            this.btnRedireccionar = new System.Windows.Forms.Button();
            this.btnHilos = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnCrearCola
            // 
            this.btnCrearCola.Location = new System.Drawing.Point(422, 15);
            this.btnCrearCola.Name = "btnCrearCola";
            this.btnCrearCola.Size = new System.Drawing.Size(75, 23);
            this.btnCrearCola.TabIndex = 0;
            this.btnCrearCola.Text = "Crear";
            this.btnCrearCola.UseVisualStyleBackColor = true;
            this.btnCrearCola.Click += new System.EventHandler(this.btnCrearCola_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nombre Cola";
            // 
            // txtNameCola
            // 
            this.txtNameCola.Location = new System.Drawing.Point(78, 18);
            this.txtNameCola.Name = "txtNameCola";
            this.txtNameCola.Size = new System.Drawing.Size(325, 20);
            this.txtNameCola.TabIndex = 2;
            this.txtNameCola.Text = "IDBO-IN09\\MyColaPrueba";
            // 
            // txtMensajeCola
            // 
            this.txtMensajeCola.Location = new System.Drawing.Point(78, 45);
            this.txtMensajeCola.Name = "txtMensajeCola";
            this.txtMensajeCola.Size = new System.Drawing.Size(325, 20);
            this.txtMensajeCola.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(4, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Mensaje";
            // 
            // btnMensajeCola
            // 
            this.btnMensajeCola.Location = new System.Drawing.Point(422, 41);
            this.btnMensajeCola.Name = "btnMensajeCola";
            this.btnMensajeCola.Size = new System.Drawing.Size(85, 23);
            this.btnMensajeCola.TabIndex = 5;
            this.btnMensajeCola.Text = "MensajeCola";
            this.btnMensajeCola.UseVisualStyleBackColor = true;
            this.btnMensajeCola.Click += new System.EventHandler(this.btnMensajeCola_Click);
            // 
            // txtRespuesta
            // 
            this.txtRespuesta.Location = new System.Drawing.Point(78, 156);
            this.txtRespuesta.Multiline = true;
            this.txtRespuesta.Name = "txtRespuesta";
            this.txtRespuesta.ReadOnly = true;
            this.txtRespuesta.Size = new System.Drawing.Size(360, 80);
            this.txtRespuesta.TabIndex = 6;
            // 
            // btnResCola
            // 
            this.btnResCola.Location = new System.Drawing.Point(513, 42);
            this.btnResCola.Name = "btnResCola";
            this.btnResCola.Size = new System.Drawing.Size(85, 23);
            this.btnResCola.TabIndex = 7;
            this.btnResCola.Text = "leerCola";
            this.btnResCola.UseVisualStyleBackColor = true;
            this.btnResCola.Click += new System.EventHandler(this.btnResCola_Click);
            // 
            // txtRutaArchivo
            // 
            this.txtRutaArchivo.Location = new System.Drawing.Point(82, 104);
            this.txtRutaArchivo.Name = "txtRutaArchivo";
            this.txtRutaArchivo.Size = new System.Drawing.Size(168, 20);
            this.txtRutaArchivo.TabIndex = 8;
            this.txtRutaArchivo.Text = "C:\\Archivo";
            // 
            // txtLeerArchivo
            // 
            this.txtLeerArchivo.Location = new System.Drawing.Point(378, 111);
            this.txtLeerArchivo.Name = "txtLeerArchivo";
            this.txtLeerArchivo.Size = new System.Drawing.Size(85, 20);
            this.txtLeerArchivo.TabIndex = 9;
            this.txtLeerArchivo.Text = "Prueba.txt";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Ruta Archivo";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(253, 111);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Nombre archivo a leer";
            // 
            // txtRutaCopia
            // 
            this.txtRutaCopia.Location = new System.Drawing.Point(82, 130);
            this.txtRutaCopia.Name = "txtRutaCopia";
            this.txtRutaCopia.Size = new System.Drawing.Size(168, 20);
            this.txtRutaCopia.TabIndex = 12;
            this.txtRutaCopia.Text = "C:\\Archivo";
            // 
            // txtCopiaArchi
            // 
            this.txtCopiaArchi.Location = new System.Drawing.Point(378, 129);
            this.txtCopiaArchi.Name = "txtCopiaArchi";
            this.txtCopiaArchi.Size = new System.Drawing.Size(85, 20);
            this.txtCopiaArchi.TabIndex = 13;
            this.txtCopiaArchi.Text = "PruebaCopia.txt";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(256, 136);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Nombre archivo copia";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(4, 132);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Ruta Archivo C";
            // 
            // btnLeer
            // 
            this.btnLeer.Location = new System.Drawing.Point(469, 102);
            this.btnLeer.Name = "btnLeer";
            this.btnLeer.Size = new System.Drawing.Size(75, 23);
            this.btnLeer.TabIndex = 16;
            this.btnLeer.Text = "Leer";
            this.btnLeer.UseVisualStyleBackColor = true;
            this.btnLeer.Click += new System.EventHandler(this.btnLeer_Click);
            // 
            // txtCopiarArchivo
            // 
            this.txtCopiarArchivo.Location = new System.Drawing.Point(469, 131);
            this.txtCopiarArchivo.Name = "txtCopiarArchivo";
            this.txtCopiarArchivo.Size = new System.Drawing.Size(75, 23);
            this.txtCopiarArchivo.TabIndex = 17;
            this.txtCopiarArchivo.Text = "Copiar";
            this.txtCopiarArchivo.UseVisualStyleBackColor = true;
            this.txtCopiarArchivo.Click += new System.EventHandler(this.txtCopiarArchivo_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Location = new System.Drawing.Point(551, 126);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(75, 23);
            this.btnEliminar.TabIndex = 18;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnEliminarCola
            // 
            this.btnEliminarCola.Location = new System.Drawing.Point(513, 15);
            this.btnEliminarCola.Name = "btnEliminarCola";
            this.btnEliminarCola.Size = new System.Drawing.Size(85, 23);
            this.btnEliminarCola.TabIndex = 19;
            this.btnEliminarCola.Text = "Eliminar";
            this.btnEliminarCola.UseVisualStyleBackColor = true;
            this.btnEliminarCola.Click += new System.EventHandler(this.btnEliminarCola_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(7, 244);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Descripcion error";
            // 
            // txtErrorLog
            // 
            this.txtErrorLog.Location = new System.Drawing.Point(100, 242);
            this.txtErrorLog.Name = "txtErrorLog";
            this.txtErrorLog.Size = new System.Drawing.Size(426, 20);
            this.txtErrorLog.TabIndex = 21;
            // 
            // btnLog
            // 
            this.btnLog.Location = new System.Drawing.Point(541, 240);
            this.btnLog.Name = "btnLog";
            this.btnLog.Size = new System.Drawing.Size(75, 23);
            this.btnLog.TabIndex = 22;
            this.btnLog.Text = "guardar Log";
            this.btnLog.UseVisualStyleBackColor = true;
            this.btnLog.Click += new System.EventHandler(this.btnLog_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 23;
            this.label8.Text = "PCNAME";
            // 
            // txtPCNAME
            // 
            this.txtPCNAME.Location = new System.Drawing.Point(78, 77);
            this.txtPCNAME.Name = "txtPCNAME";
            this.txtPCNAME.Size = new System.Drawing.Size(325, 20);
            this.txtPCNAME.TabIndex = 24;
            // 
            // btnEnviarObjeto
            // 
            this.btnEnviarObjeto.Location = new System.Drawing.Point(422, 73);
            this.btnEnviarObjeto.Name = "btnEnviarObjeto";
            this.btnEnviarObjeto.Size = new System.Drawing.Size(122, 23);
            this.btnEnviarObjeto.TabIndex = 25;
            this.btnEnviarObjeto.Text = "Objecto a Cola";
            this.btnEnviarObjeto.UseVisualStyleBackColor = true;
            this.btnEnviarObjeto.Click += new System.EventHandler(this.btnEnviarObjeto_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 282);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 13);
            this.label9.TabIndex = 26;
            this.label9.Text = "servidor";
            // 
            // txtServidor
            // 
            this.txtServidor.Location = new System.Drawing.Point(60, 279);
            this.txtServidor.Name = "txtServidor";
            this.txtServidor.Size = new System.Drawing.Size(109, 20);
            this.txtServidor.TabIndex = 27;
            this.txtServidor.Text = "10.2.1.69";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(175, 286);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 13);
            this.label10.TabIndex = 28;
            this.label10.Text = "Puerto";
            // 
            // txtPuerto
            // 
            this.txtPuerto.Location = new System.Drawing.Point(219, 279);
            this.txtPuerto.Name = "txtPuerto";
            this.txtPuerto.Size = new System.Drawing.Size(34, 20);
            this.txtPuerto.TabIndex = 29;
            this.txtPuerto.Text = "8890";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(259, 285);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(55, 13);
            this.label11.TabIndex = 30;
            this.label11.Text = "Username";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(320, 278);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(100, 20);
            this.txtUserName.TabIndex = 31;
            this.txtUserName.Text = "manager";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(426, 286);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(53, 13);
            this.label12.TabIndex = 32;
            this.label12.Text = "Password";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(485, 279);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(131, 20);
            this.txtPassword.TabIndex = 33;
            this.txtPassword.Text = "manager";
            this.txtPassword.UseSystemPasswordChar = true;
            // 
            // btnCrearSession
            // 
            this.btnCrearSession.Location = new System.Drawing.Point(638, 257);
            this.btnCrearSession.Name = "btnCrearSession";
            this.btnCrearSession.Size = new System.Drawing.Size(120, 23);
            this.btnCrearSession.TabIndex = 34;
            this.btnCrearSession.Text = "Crear Session";
            this.btnCrearSession.UseVisualStyleBackColor = true;
            this.btnCrearSession.Click += new System.EventHandler(this.btnCrearSession_Click);
            // 
            // btnCerrarSession
            // 
            this.btnCerrarSession.Location = new System.Drawing.Point(638, 286);
            this.btnCerrarSession.Name = "btnCerrarSession";
            this.btnCerrarSession.Size = new System.Drawing.Size(120, 23);
            this.btnCerrarSession.TabIndex = 35;
            this.btnCerrarSession.Text = "cerrar session";
            this.btnCerrarSession.UseVisualStyleBackColor = true;
            this.btnCerrarSession.Click += new System.EventHandler(this.btnCerrarSession_Click);
            // 
            // btnImprimir
            // 
            this.btnImprimir.Location = new System.Drawing.Point(607, 352);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(120, 39);
            this.btnImprimir.TabIndex = 36;
            this.btnImprimir.Text = "Imprimir Tarjeta";
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            // 
            // txtPCNameC
            // 
            this.txtPCNameC.Location = new System.Drawing.Point(60, 312);
            this.txtPCNameC.Name = "txtPCNameC";
            this.txtPCNameC.Size = new System.Drawing.Size(112, 20);
            this.txtPCNameC.TabIndex = 37;
            this.txtPCNameC.Text = "WIN-4H1I0FLTUGA";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(10, 315);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(48, 13);
            this.label13.TabIndex = 38;
            this.label13.Text = "PcName";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(189, 315);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 13);
            this.label14.TabIndex = 39;
            this.label14.Text = "CardFormat";
            // 
            // txtCardFormat
            // 
            this.txtCardFormat.Location = new System.Drawing.Point(262, 312);
            this.txtCardFormat.Name = "txtCardFormat";
            this.txtCardFormat.Size = new System.Drawing.Size(120, 20);
            this.txtCardFormat.TabIndex = 40;
            this.txtCardFormat.Text = "Credit Card Smart";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(397, 315);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(74, 13);
            this.label15.TabIndex = 41;
            this.label15.Text = "Tipo Producto";
            // 
            // txtTipoC
            // 
            this.txtTipoC.Location = new System.Drawing.Point(485, 315);
            this.txtTipoC.Name = "txtTipoC";
            this.txtTipoC.Size = new System.Drawing.Size(100, 20);
            this.txtTipoC.TabIndex = 42;
            this.txtTipoC.Text = "1";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(10, 332);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(153, 13);
            this.label16.TabIndex = 43;
            this.label16.Text = "Añadir parametros para imprimir";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 349);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(35, 13);
            this.label17.TabIndex = 44;
            this.label17.Text = "Name";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(350, 352);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(28, 13);
            this.label18.TabIndex = 45;
            this.label18.Text = "Tipo";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(178, 352);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(60, 13);
            this.label19.TabIndex = 46;
            this.label19.Text = "Name Card";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(453, 352);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(30, 13);
            this.label20.TabIndex = 47;
            this.label20.Text = "valor";
            // 
            // txtNameP
            // 
            this.txtNameP.Location = new System.Drawing.Point(54, 349);
            this.txtNameP.Name = "txtNameP";
            this.txtNameP.Size = new System.Drawing.Size(100, 20);
            this.txtNameP.TabIndex = 48;
            this.txtNameP.Text = "DATAITEM";
            this.txtNameP.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txtNameCardP
            // 
            this.txtNameCardP.Location = new System.Drawing.Point(244, 349);
            this.txtNameCardP.Name = "txtNameCardP";
            this.txtNameCardP.Size = new System.Drawing.Size(100, 20);
            this.txtNameCardP.TabIndex = 49;
            this.txtNameCardP.Text = "@ExpirationDate@";
            // 
            // txtTipoP
            // 
            this.txtTipoP.Location = new System.Drawing.Point(379, 352);
            this.txtTipoP.Name = "txtTipoP";
            this.txtTipoP.Size = new System.Drawing.Size(68, 20);
            this.txtTipoP.TabIndex = 50;
            this.txtTipoP.Text = "string";
            // 
            // txtValorP
            // 
            this.txtValorP.Location = new System.Drawing.Point(485, 352);
            this.txtValorP.Name = "txtValorP";
            this.txtValorP.Size = new System.Drawing.Size(100, 20);
            this.txtValorP.TabIndex = 51;
            this.txtValorP.Text = "03/10";
            // 
            // btnAnadParam
            // 
            this.btnAnadParam.Location = new System.Drawing.Point(485, 368);
            this.btnAnadParam.Name = "btnAnadParam";
            this.btnAnadParam.Size = new System.Drawing.Size(75, 23);
            this.btnAnadParam.TabIndex = 52;
            this.btnAnadParam.Text = "Añadir Parametro";
            this.btnAnadParam.UseVisualStyleBackColor = true;
            this.btnAnadParam.Click += new System.EventHandler(this.btnAnadParam_Click);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(584, 321);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(55, 13);
            this.label21.TabIndex = 53;
            this.label21.Text = "SessionID";
            // 
            // txtSessionID
            // 
            this.txtSessionID.Location = new System.Drawing.Point(646, 314);
            this.txtSessionID.Name = "txtSessionID";
            this.txtSessionID.Size = new System.Drawing.Size(100, 20);
            this.txtSessionID.TabIndex = 54;
            // 
            // btnMensajeCard
            // 
            this.btnMensajeCard.Location = new System.Drawing.Point(638, 228);
            this.btnMensajeCard.Name = "btnMensajeCard";
            this.btnMensajeCard.Size = new System.Drawing.Size(120, 23);
            this.btnMensajeCard.TabIndex = 55;
            this.btnMensajeCard.Text = "Mensajes Card";
            this.btnMensajeCard.UseVisualStyleBackColor = true;
            this.btnMensajeCard.Click += new System.EventHandler(this.btnMensajeCard_Click);
            // 
            // btnConfigurador
            // 
            this.btnConfigurador.Location = new System.Drawing.Point(607, 42);
            this.btnConfigurador.Name = "btnConfigurador";
            this.btnConfigurador.Size = new System.Drawing.Size(75, 23);
            this.btnConfigurador.TabIndex = 56;
            this.btnConfigurador.Text = "COnfigurador";
            this.btnConfigurador.UseVisualStyleBackColor = true;
            this.btnConfigurador.Click += new System.EventHandler(this.btnConfigurador_Click);
            // 
            // btnRedireccionar
            // 
            this.btnRedireccionar.Location = new System.Drawing.Point(10, 396);
            this.btnRedireccionar.Name = "btnRedireccionar";
            this.btnRedireccionar.Size = new System.Drawing.Size(228, 23);
            this.btnRedireccionar.TabIndex = 57;
            this.btnRedireccionar.Text = "Abrir Form Clientes y notificaciones";
            this.btnRedireccionar.UseVisualStyleBackColor = true;
            this.btnRedireccionar.Click += new System.EventHandler(this.btnRedireccionar_Click);
            // 
            // btnHilos
            // 
            this.btnHilos.Location = new System.Drawing.Point(550, 77);
            this.btnHilos.Name = "btnHilos";
            this.btnHilos.Size = new System.Drawing.Size(75, 23);
            this.btnHilos.TabIndex = 58;
            this.btnHilos.Text = "Hilos";
            this.btnHilos.UseVisualStyleBackColor = true;
            this.btnHilos.Click += new System.EventHandler(this.btnHilos_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(770, 455);
            this.Controls.Add(this.btnHilos);
            this.Controls.Add(this.btnRedireccionar);
            this.Controls.Add(this.btnConfigurador);
            this.Controls.Add(this.btnMensajeCard);
            this.Controls.Add(this.txtSessionID);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.btnAnadParam);
            this.Controls.Add(this.txtValorP);
            this.Controls.Add(this.txtTipoP);
            this.Controls.Add(this.txtNameCardP);
            this.Controls.Add(this.txtNameP);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtTipoC);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtCardFormat);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtPCNameC);
            this.Controls.Add(this.btnImprimir);
            this.Controls.Add(this.btnCerrarSession);
            this.Controls.Add(this.btnCrearSession);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtPuerto);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtServidor);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btnEnviarObjeto);
            this.Controls.Add(this.txtPCNAME);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnLog);
            this.Controls.Add(this.txtErrorLog);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnEliminarCola);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.txtCopiarArchivo);
            this.Controls.Add(this.btnLeer);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtCopiaArchi);
            this.Controls.Add(this.txtRutaCopia);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtLeerArchivo);
            this.Controls.Add(this.txtRutaArchivo);
            this.Controls.Add(this.btnResCola);
            this.Controls.Add(this.txtRespuesta);
            this.Controls.Add(this.btnMensajeCola);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtMensajeCola);
            this.Controls.Add(this.txtNameCola);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnCrearCola);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnCrearCola;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtNameCola;
        private System.Windows.Forms.TextBox txtMensajeCola;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnMensajeCola;
        private System.Windows.Forms.TextBox txtRespuesta;
        private System.Windows.Forms.Button btnResCola;
        private System.Windows.Forms.TextBox txtRutaArchivo;
        private System.Windows.Forms.TextBox txtLeerArchivo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtRutaCopia;
        private System.Windows.Forms.TextBox txtCopiaArchi;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnLeer;
        private System.Windows.Forms.Button txtCopiarArchivo;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.Button btnEliminarCola;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtErrorLog;
        private System.Windows.Forms.Button btnLog;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPCNAME;
        private System.Windows.Forms.Button btnEnviarObjeto;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtServidor;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtPuerto;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Button btnCrearSession;
        private System.Windows.Forms.Button btnCerrarSession;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.TextBox txtPCNameC;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtCardFormat;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtTipoC;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtNameP;
        private System.Windows.Forms.TextBox txtNameCardP;
        private System.Windows.Forms.TextBox txtTipoP;
        private System.Windows.Forms.TextBox txtValorP;
        private System.Windows.Forms.Button btnAnadParam;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtSessionID;
        private System.Windows.Forms.Button btnMensajeCard;
        private System.Windows.Forms.Button btnConfigurador;
        private System.Windows.Forms.Button btnRedireccionar;
        private System.Windows.Forms.Button btnHilos;
    }
}

