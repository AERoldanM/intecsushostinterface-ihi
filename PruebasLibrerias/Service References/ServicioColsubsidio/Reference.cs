﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PruebasLibrerias.ServicioColsubsidio {
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="urn:tcajas.adepcon.com:tarjeta:ActualizacionEstadoTarjeta", ConfigurationName="ServicioColsubsidio.ActualizacionEstadoTarjeta_Out")]
    public interface ActualizacionEstadoTarjeta_Out {
        
        // CODEGEN: Generating message contract since the operation Actualizacion_Out is neither RPC nor document wrapped.
        [System.ServiceModel.OperationContractAttribute(Action="http://sap.com/xi/WebService/soap1.1", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        PruebasLibrerias.ServicioColsubsidio.Actualizacion_OutResponse Actualizacion_Out(PruebasLibrerias.ServicioColsubsidio.Actualizacion_OutRequest request);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://sap.com/xi/WebService/soap1.1", ReplyAction="*")]
        System.Threading.Tasks.Task<PruebasLibrerias.ServicioColsubsidio.Actualizacion_OutResponse> Actualizacion_OutAsync(PruebasLibrerias.ServicioColsubsidio.Actualizacion_OutRequest request);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="urn:tcajas.adepcon.com:tarjeta:ActualizacionEstadoTarjeta")]
    public partial class RequestEstadoTarjetaOut : object, System.ComponentModel.INotifyPropertyChanged {
        
        private RequestEstadoTarjetaOutTarjeta tarjetaField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public RequestEstadoTarjetaOutTarjeta Tarjeta {
            get {
                return this.tarjetaField;
            }
            set {
                this.tarjetaField = value;
                this.RaisePropertyChanged("Tarjeta");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="urn:tcajas.adepcon.com:tarjeta:ActualizacionEstadoTarjeta")]
    public partial class RequestEstadoTarjetaOutTarjeta : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string numero_tarjetaField;
        
        private string estado_tarjetaField;
        
        private string mensajeField;
        
        private System.DateTime fecha_entregaField;
        
        private string id_chip_contactoField;
        
        private string id_chip_proximidadField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string numero_tarjeta {
            get {
                return this.numero_tarjetaField;
            }
            set {
                this.numero_tarjetaField = value;
                this.RaisePropertyChanged("numero_tarjeta");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string Estado_tarjeta {
            get {
                return this.estado_tarjetaField;
            }
            set {
                this.estado_tarjetaField = value;
                this.RaisePropertyChanged("Estado_tarjeta");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=2)]
        public string Mensaje {
            get {
                return this.mensajeField;
            }
            set {
                this.mensajeField = value;
                this.RaisePropertyChanged("Mensaje");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, DataType="date", Order=3)]
        public System.DateTime fecha_entrega {
            get {
                return this.fecha_entregaField;
            }
            set {
                this.fecha_entregaField = value;
                this.RaisePropertyChanged("fecha_entrega");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=4)]
        public string id_chip_contacto {
            get {
                return this.id_chip_contactoField;
            }
            set {
                this.id_chip_contactoField = value;
                this.RaisePropertyChanged("id_chip_contacto");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=5)]
        public string id_chip_proximidad {
            get {
                return this.id_chip_proximidadField;
            }
            set {
                this.id_chip_proximidadField = value;
                this.RaisePropertyChanged("id_chip_proximidad");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="urn:tcajas.adepcon.com:tarjeta:ActualizacionEstadoTarjeta")]
    public partial class ResponseEstadoTarjetaOut : object, System.ComponentModel.INotifyPropertyChanged {
        
        private ResponseEstadoTarjetaOutMensaje mensajeField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public ResponseEstadoTarjetaOutMensaje Mensaje {
            get {
                return this.mensajeField;
            }
            set {
                this.mensajeField = value;
                this.RaisePropertyChanged("Mensaje");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true, Namespace="urn:tcajas.adepcon.com:tarjeta:ActualizacionEstadoTarjeta")]
    public partial class ResponseEstadoTarjetaOutMensaje : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string cod_respuestaField;
        
        private string respuestaField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=0)]
        public string cod_respuesta {
            get {
                return this.cod_respuestaField;
            }
            set {
                this.cod_respuestaField = value;
                this.RaisePropertyChanged("cod_respuesta");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form=System.Xml.Schema.XmlSchemaForm.Unqualified, Order=1)]
        public string respuesta {
            get {
                return this.respuestaField;
            }
            set {
                this.respuestaField = value;
                this.RaisePropertyChanged("respuesta");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Actualizacion_OutRequest {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="urn:tcajas.adepcon.com:tarjeta:ActualizacionEstadoTarjeta", Order=0)]
        public PruebasLibrerias.ServicioColsubsidio.RequestEstadoTarjetaOut RequestEstadoTarjetaOut;
        
        public Actualizacion_OutRequest() {
        }
        
        public Actualizacion_OutRequest(PruebasLibrerias.ServicioColsubsidio.RequestEstadoTarjetaOut RequestEstadoTarjetaOut) {
            this.RequestEstadoTarjetaOut = RequestEstadoTarjetaOut;
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
    [System.ServiceModel.MessageContractAttribute(IsWrapped=false)]
    public partial class Actualizacion_OutResponse {
        
        [System.ServiceModel.MessageBodyMemberAttribute(Namespace="urn:tcajas.adepcon.com:tarjeta:ActualizacionEstadoTarjeta", Order=0)]
        public PruebasLibrerias.ServicioColsubsidio.ResponseEstadoTarjetaOut ResponseEstadoTarjetaOut;
        
        public Actualizacion_OutResponse() {
        }
        
        public Actualizacion_OutResponse(PruebasLibrerias.ServicioColsubsidio.ResponseEstadoTarjetaOut ResponseEstadoTarjetaOut) {
            this.ResponseEstadoTarjetaOut = ResponseEstadoTarjetaOut;
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ActualizacionEstadoTarjeta_OutChannel : PruebasLibrerias.ServicioColsubsidio.ActualizacionEstadoTarjeta_Out, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ActualizacionEstadoTarjeta_OutClient : System.ServiceModel.ClientBase<PruebasLibrerias.ServicioColsubsidio.ActualizacionEstadoTarjeta_Out>, PruebasLibrerias.ServicioColsubsidio.ActualizacionEstadoTarjeta_Out {
        
        public ActualizacionEstadoTarjeta_OutClient() {
        }
        
        public ActualizacionEstadoTarjeta_OutClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ActualizacionEstadoTarjeta_OutClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ActualizacionEstadoTarjeta_OutClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ActualizacionEstadoTarjeta_OutClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        PruebasLibrerias.ServicioColsubsidio.Actualizacion_OutResponse PruebasLibrerias.ServicioColsubsidio.ActualizacionEstadoTarjeta_Out.Actualizacion_Out(PruebasLibrerias.ServicioColsubsidio.Actualizacion_OutRequest request) {
            return base.Channel.Actualizacion_Out(request);
        }
        
        public PruebasLibrerias.ServicioColsubsidio.ResponseEstadoTarjetaOut Actualizacion_Out(PruebasLibrerias.ServicioColsubsidio.RequestEstadoTarjetaOut RequestEstadoTarjetaOut) {
            PruebasLibrerias.ServicioColsubsidio.Actualizacion_OutRequest inValue = new PruebasLibrerias.ServicioColsubsidio.Actualizacion_OutRequest();
            inValue.RequestEstadoTarjetaOut = RequestEstadoTarjetaOut;
            PruebasLibrerias.ServicioColsubsidio.Actualizacion_OutResponse retVal = ((PruebasLibrerias.ServicioColsubsidio.ActualizacionEstadoTarjeta_Out)(this)).Actualizacion_Out(inValue);
            return retVal.ResponseEstadoTarjetaOut;
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        System.Threading.Tasks.Task<PruebasLibrerias.ServicioColsubsidio.Actualizacion_OutResponse> PruebasLibrerias.ServicioColsubsidio.ActualizacionEstadoTarjeta_Out.Actualizacion_OutAsync(PruebasLibrerias.ServicioColsubsidio.Actualizacion_OutRequest request) {
            return base.Channel.Actualizacion_OutAsync(request);
        }
        
        public System.Threading.Tasks.Task<PruebasLibrerias.ServicioColsubsidio.Actualizacion_OutResponse> Actualizacion_OutAsync(PruebasLibrerias.ServicioColsubsidio.RequestEstadoTarjetaOut RequestEstadoTarjetaOut) {
            PruebasLibrerias.ServicioColsubsidio.Actualizacion_OutRequest inValue = new PruebasLibrerias.ServicioColsubsidio.Actualizacion_OutRequest();
            inValue.RequestEstadoTarjetaOut = RequestEstadoTarjetaOut;
            return ((PruebasLibrerias.ServicioColsubsidio.ActualizacionEstadoTarjeta_Out)(this)).Actualizacion_OutAsync(inValue);
        }
    }
}
