﻿using Intecsus.CW;
using Intecsus.Entidades;
using Intecsus.MSMQ;
using Intecsus.OperarCMS;
using Intecusus.OperarBD;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Intecsus.CW.TCP;
using System.IO;
using Intecsus.Encripcion;
using Intecsus.Seguridad;
using Intecsus.OperarArchivos;

namespace PruebasLibrerias
{
    public partial class ClientesyNotificaciones : Form
    {
        Procesar proceso = new Procesar();
        ProcesoBase procesoPre = new ProcesoBase();
        TCPClient mNewTcpClient = new TCPClient("CWHostInterfaceExample", 5);


        public ClientesyNotificaciones()
        {
            InitializeComponent();
            mNewTcpClient.PortNumber = 8890;
            mNewTcpClient.ServerName = "10.2.1.69";
            proceso.TerminoProceso += proceso_TerminoProceso;
            procesoPre.TerminoProceso += procesoPre_TerminoProceso;
            OperacionesBase operacionesBD = new OperacionesBase();
            mNewTcpClient.MessageFromServer += mNewTcpClient_MessageFromServer;
            this.lblArchivo.Text = operacionesBD.ObtenerValoresConfigurador("RutaArchivo", ConfigurationManager.AppSettings["BD"]) + @"\"
                + operacionesBD.ObtenerValoresConfigurador("NombreArchivo", ConfigurationManager.AppSettings["BD"]);
        }

        void mNewTcpClient_MessageFromServer(string sMessage)
        {
            MessageBox.Show(sMessage);
        }



        private void btnCliente_Click(object sender, EventArgs e)
        {
            try
            {
                Peticion peticion = new Peticion();
                peticion.nombreLargo = this.txtNombreLargo.Text;
                peticion.nombreRealce = this.txtPAN.Text;
                peticion.direccionResidencia1 = this.txtExpiracion.Text;
                peticion.BIN = this.txtPIN.Text;
                peticion.cliente = "PruebaIntecsus";
                peticion.printName = this.txtPCNAME.Text;
                peticion.tipoProducto = this.txtTipoProduct.Text;
                Random rnd = new Random();
                peticion.idTransaccion = rnd.Next(0, 110).ToString() + "AERM";
                peticion.codigoNovedad = "03";
                ICola m_OperarCOla = new InstanciaCola().IntanciarCola(ConfigurationManager.AppSettings["ColaInstancia"]);
                m_OperarCOla.GuardarObjetoPeticionCliente(peticion, ConfigurationManager.AppSettings["colaCliente"]);
                //m_OperarCOla.RecibirPeticionCliente(ConfigurationManager.AppSettings["colaCliente"], ref peticion);
                MessageBox.Show("Se agrega cliente");

                Prueba.ServicioIntegracionClient prueba = new Prueba.ServicioIntegracionClient();
                var respuesta = prueba.IntegradorComfenalco(this.txtPAN.Text, "", 0, 0, "", string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 0, string.Empty,
                                             0, 0, string.Empty, string.Empty, 0, 0, string.Empty, 0, string.Empty, 0, string.Empty, string.Empty, string.Empty, 0, 0, 0, string.Empty, 0, 0, string.Empty, 0, string.Empty, string.Empty, string.Empty, 0, 0,
                                             string.Empty, string.Empty, string.Empty, "Comfenalco");

                MessageBox.Show(respuesta.mensaje);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnImpresion_Click(object sender, EventArgs e)
        {
            try
            {
                Inetcsus.Motor.Service1 servicio = new Inetcsus.Motor.Service1();
                servicio.TestStartupAndStop(null);
                proceso.InicilizarVariables();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void proceso_TerminoProceso(object sender, int e)
        {
            MessageBox.Show("Termino el proceso Impresion solciitado : " + e.ToString());
        }

        private void btnNotificaciones_Click(object sender, EventArgs e)
        {
            try
            {
                Intecsus.Integrador.Service1 servicio = new Intecsus.Integrador.Service1();
                servicio.TestStartupAndStop(null);
                procesoPre.InicilizarVariables();
                procesoPre.ComenzarProceso();
                //notificacion.EnviarNotificaciones(varInicio);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void procesoPre_TerminoProceso(object sender, int e)
        {
            MessageBox.Show("Termino el proceso Cliente y notiifcacion solciitado : ");
        }

        private void btnArchivo_Click(object sender, EventArgs e)
        {
            try
            {
                RetornarArchivo vali = new RetornarArchivo();
                OperacionesBase operacionesBD = new OperacionesBase();
                List<FileInfo> archivos = vali.RetornarArchivos(operacionesBD.ObtenerValoresConfigurador("RutaArchivo", ConfigurationManager.AppSettings["BD"]));
                procesoPre.InicilizarVariables();
                // procesoPre.ComenzarLecturaArchivo();
                //procesoPre.EnviarNotificaciones();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnEncriptar_Click(object sender, EventArgs e)
        {
            try
            {

                OperacionesBase operacionesBD = new OperacionesBase();
                String key = operacionesBD.ObtenerValoresConfigurador("KeyHash", ConfigurationManager.AppSettings["BD"]);
                HashEncriptar operar = new HashEncriptar(key);

                String cadena = this.txtCadenaCifrada.Text;
                this.txtDesencripcion.Text = operar.SHA512(cadena);
                //MessageBox.Show(operar.SHA512(cadena));
                // validar que encripte
                //string PAN = "01234567891234567";
                ////Validar datos
                //Encriptar encript = new Encriptar();
                //string mascara2 = encript.EnMascarar(PAN, 6, 4);
                //MessageBox.Show(mascara2);
                //mascara2 = encript.EnMascarar(PAN, 0, 0);
                //MessageBox.Show(mascara2);
                //string caracter = "andres roldan";
                //Validadores vali = new Validadores();
                //string expresionR = operacionesBD.ObtenerValoresConfigurador("ALFA", ConfigurationManager.AppSettings["BD"]);
                //MessageBox.Show("ALFA" + vali.ValidadorExpresionesRegulares(caracter, expresionR).ToString());
                //caracter = "0305834068456";
                // expresionR = operacionesBD.ObtenerValoresConfigurador("INT", ConfigurationManager.AppSettings["BD"]);
                // MessageBox.Show("INT" + vali.ValidadorExpresionesRegulares(caracter, expresionR).ToString());
                // caracter = "asas&sdssdsd";
                // expresionR = operacionesBD.ObtenerValoresConfigurador("ALFAAM", ConfigurationManager.AppSettings["BD"]);
                // MessageBox.Show("ALFAAM" + vali.ValidadorExpresionesRegulares(caracter, expresionR).ToString());
                //Prueba mascara
                Cifrar encrip = new Cifrar();
                string mascara = encrip.EnMascarar("4247771234567893");
                //MessageBox.Show(mascara);


                this.txtResultadoEncripcion.Text = operar.EncryptData(cadena);
                operar = new HashEncriptar(key);
                this.txtDesencripcion.Text = operar.Descifrar(this.txtResultadoEncripcion.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnIntegrador_Click(object sender, EventArgs e)
        {
            //Intecsus.Integrador.Service1 servicio = new Intecsus.Integrador.Service1();
            //servicio.TestStartupAndStop(null);
            //Console.ReadLine();

            Inetcsus.Motor.Service1 servicio = new Inetcsus.Motor.Service1();
            servicio.TestStartupAndStop(null);
        }

        private void btnNotificar_Click(object sender, EventArgs e)
        {
            RespuestaDeviceStatus res = new RespuestaDeviceStatus();
            res.Cliente = "Archivo";
            res.Mensaje = "Pruebas...";
            res.Transaccion = "234";
            res.Estado = ESTADO_REALCE.ST_OK;
            ICola m_OperarCOla = new InstanciaCola().IntanciarCola(ConfigurationManager.AppSettings["ColaInstancia"]);
            m_OperarCOla.GuardarObjetoNotificacion(res, @"IDBO-IN09\Notificacion");
        }

        private void btnInvocar_Click(object sender, EventArgs e)
        {
            //Intecsus.OperarFront.IServicioIntegracion 
            //IServicioIntegracion inte = new ServicioIntegracion();
            //inte.ServicioIntegracionClient prueba = new PruebaTest.ServicioIntegracionClient();
            //var respuesta = prueba.Integrador(string.Empty, "2774B41D4AD5368E6A61EAE1B06C4A7B", "N", "2", //4
            //                   "233435436", "RIOS", "LUGO", "JUAN", "DIEGO", "F4F0FFE8540ACA0174AFC1062C65AA8F3E649DDBF94EA564",//6
            //                   string.Empty, "20090514", "M", "2", "CRA 96 66A 23", string.Empty, "02", "00005", string.Empty, string.Empty,//10
            //                   string.Empty, string.Empty, "00001", "3164688849", string.Empty, string.Empty, string.Empty,//7
            //                   "20150227", string.Empty, string.Empty, "001", string.Empty, string.Empty, string.Empty, "123",//8
            //                   "123", "01", string.Empty, string.Empty, string.Empty, "12345", string.Empty, "JDRIOS@ASNETLA.COM",//8
            //                   string.Empty, "1", string.Empty, "S", "999", "8888888888888888888", "99", string.Empty, "20150303", string.Empty,
            //                   "000880003", "8904800237", "001", "0057", "03", string.Empty, "Prueba01", "Prueba_01", "Tarjeta1", "Unico");//8
            //MessageBox.Show(respuesta.codigo + " " + respuesta.mensaje);
        }

        private void btnCIfrar_Click(object sender, EventArgs e)
        {
            try
            {
                IEncrypt interfaz = new FactoryMethod().DevolverClase(ConfigurationManager.AppSettings["TDES"]);
                Cifrar des = new Cifrar();
                this.txtHexa.Text = this.ToHexString(this.txtCifrar.Text);
                this.txtxDesC.Text = interfaz.CifrarDato(this.txtHexa.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDesc_Click(object sender, EventArgs e)
        {
            try
            {
                IEncrypt interfaz = new FactoryMethod().DevolverClase(ConfigurationManager.AppSettings["TDES"]);
                Cifrar des = new Cifrar();
                var m_Data = des.FromHex(interfaz.DescifrarDato(this.txtxDesC.Text));
                // Encoding m_iso = Encoding.GetEncoding("ISO-8859-1");
                Encoding m_iso = Encoding.GetEncoding("IBM037");
                this.txtResul1.Text = m_iso.GetString(m_Data);
                // string formato = m_iso.(m_Data);
                Encoding m_iso2 = new UTF8Encoding();
                this.txtResult2.Text = m_iso2.GetString(m_Data);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public string ToHexString(string str, int len = -1, bool spaces = false)
        {
            string hexOutput = string.Empty;
            //byte[] bytes =  System.Text.Encoding.UTF8.GetBytes(str);
            //byte[] bytes = Encoding.ASCII.GetBytes(str);
            Encoding m_iso = Encoding.GetEncoding("IBM037");
            byte[] bytes = m_iso.GetBytes(str);
            Int32 len2 = bytes.Length;
            Int32 i;
            String s = string.Empty;
            for (i = 0; i <= len2 - 1; i++)
            {
                s += bytes[i].ToString("x2");
            }
            return s;

            //StringBuilder hex = new StringBuilder();
            //char[] values = str.ToCharArray();
            //foreach (char letter in values)
            //{
            //    // Get the integral value of the character.
            //    int value = Convert.ToInt32(letter);
            //    // Convert the decimal value to a hexadecimal value in string form.
            //    hex.AppendFormat(String.Format("{0:X}", value));
            //    // Console.WriteLine("Hexadecimal value of {0} is {1}", letter, hexOutput);
            //}

            //
            //foreach (byte b in bytes)
            //    hex.AppendFormat("{0:x2}", b);



            // return hex.ToString();



            //string input = "Hello World!";
            //char[] values = input.ToCharArray();
            //foreach (char letter in values)
            //{
            //    // Get the integral value of the character.
            //    int value = Convert.ToInt32(letter);
            //    // Convert the decimal value to a hexadecimal value in string form.
            //   String.Format("{0:X}", value);
            //   hexOutput += String.Format("{0:X}", value);
            //    Console.WriteLine("Hexadecimal value of {0} is {1}", letter, hexOutput);
            //}

            return hexOutput;
            //var sb = new StringBuilder();

            //var bytes = Encoding.Unicode.GetBytes(str);
            //foreach (var t in bytes)
            //{
            //    sb.Append(t.ToString("X2"));
            //}

            //return sb.ToString(); // returns: "48656C6C6F20776F726C64" for "Hello world"
        }
    }
}
