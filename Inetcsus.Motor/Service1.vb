﻿Imports System.Timers
Imports Intecsus.CW
Imports Intecusus.OperarBD
Imports System.Configuration

Public Class Service1

    Private WithEvents proceso As Procesar = New Procesar()
    Private timer As Timer = New Timer()
    ' Private timerTransaccion As Timer = New Timer()
    ' new way to utilize TCP
    Public WithEvents mNewTcpClient As TCP.TCPClient

    Public Sub TestStartupAndStop(args As String())
        Me.OnStart(args)
        ''Me.OnStop()IntervaloProceso
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.

        Dim operacionesBD As OperacionesBase = New OperacionesBase()
        Try
            proceso.InicilizarVariables()
            mNewTcpClient = New TCP.TCPClient(proceso.interfaceUsar, operacionesBD.ObtenerValoresConfigurador("Tiempo", ConfigurationManager.AppSettings("BD")))
            mNewTcpClient.PortNumber = proceso.puerto
            mNewTcpClient.ServerName = proceso.servidor
            timer.Interval = proceso.RetornarTiempo()
            AddHandler timer.Elapsed, AddressOf timer_Elapsed
            timer.Start()
            operacionesBD.GuardarLogXNombreConectionString(1, 0, 1, "Service1.vb Motor - OnStart  ",
                                                         ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(1, 0, 2, "Service1.vb Motor - OnStart : " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
        End Try

    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        proceso.BorrarVariables()
        Dim operacionesBD As OperacionesBase = New OperacionesBase()

        timer.Stop()
        operacionesBD.GuardarLogXNombreConectionString(1, 0, 1, "Service1.vb Motor- OnSTOP : ",
                                                      ConfigurationManager.AppSettings("BD"))
    End Sub



#Region "Eventos"

    Private Sub timer_Elapsed(sender As Object, e As ElapsedEventArgs)
        timer.Stop()
        proceso.CrearSession()
        If String.IsNullOrEmpty(proceso.RetornarSession()) Then
            Me.Inicializar()
        Else
            proceso.ComenzarProceso()
        End If
    End Sub

    Private Sub InicializarTimer(ByVal sender As Object, ByVal tiempo As Integer) Handles proceso.TerminoProceso
        Me.Inicializar()
    End Sub


#End Region

    Private Sub Inicializar()
        timer = New Timer()
        timer.Interval = proceso.RetornarTiempo()
        AddHandler timer.Elapsed, AddressOf timer_Elapsed
        timer.Start()
    End Sub
End Class
