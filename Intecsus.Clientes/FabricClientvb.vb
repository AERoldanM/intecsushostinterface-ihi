﻿Public Class FactoryClient
    Public Function RetornarCliente(ByVal cliente As String) As IOperacionesCliente

        Try
            Select Case cliente.ToUpper
                Case "PRUEBA"
                    Return New ClientePruebas()
                    'Case "UNICO"
                    '    Return New ClienteComfenalco()
                    'Case "PRUEBAINTECSUS"
                    '    Return New ClientePrueba()
                    'Case "ARCHIVO"
                    '    Return New ClienteLineaArchivo()
                    'Case "COLSUBSIDIO"
                    '    Return New ClienteColsubsidio()
                    'Case "TUYA"
                    '    Return New ClienteTUYA()
                Case Else
                    Throw New Exception("No está disponible para este cliente. (Fabrica - RetornarCliente)")
            End Select
        Catch ex As Exception
            Throw New Exception("RetornarCliente " + ex.Message)
        End Try
    End Function

    Public Function RetornarClienteArchivo(ByVal cliente As String) As IProcesoArchivos
        Try
            Select Case cliente.ToUpper
                'Case "PRUEBA"
                   ' Return New ClientePruebas()
                    'Case "UNICO"
                    '    Return New ClienteComfenalco()
                    'Case "PRUEBAINTECSUS"
                    '    Return New ClientePrueba()
                Case "ARCHIVO"
                    Return New ClienteLineaArchivo()
                    'Case "COLSUBSIDIO"
                    '    Return New ClienteColsubsidio()
                    'Case "TUYA"
                    '    Return New ClienteTUYA()
                Case Else
                    Throw New Exception("No está disponible para este cliente. (Fabrica - RetornarClienteArchivo)")
            End Select
        Catch ex As Exception
            Throw New Exception("RetornarCliente " + ex.Message)
        End Try
    End Function
End Class
