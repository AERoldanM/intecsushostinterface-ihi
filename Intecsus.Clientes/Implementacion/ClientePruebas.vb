﻿Imports Intecsus.Entidades
Imports Intecusus.OperarBD
Imports System.Configuration
Imports System.Net.Mail
Imports Intecsus.GeneradorValoresCore
Imports Intecsus.Clientes

Public Class ClientePruebas
    Implements IOperacionesCliente

#Region "Variables"
    Private operacionesBD As OperacionesBase = New OperacionesBase()
#End Region

    Public Sub NotificarCliente(notificacion As RespuestaDeviceStatus, idTransaccion As String, varInicio As ConfInicio) Implements IOperacionesCliente.NotificarCliente
        Try
            Dim correo As New MailMessage
            Dim smtp As New SmtpClient()
            Dim correRemitente As String = operacionesBD.ObtenerValoresConfigurador("CorreoFrom", ConfigurationManager.AppSettings("BD"))
            correo.From = New MailAddress(correRemitente, "Intecsus SAS", System.Text.Encoding.UTF8)
            correo.To.Add(operacionesBD.ObtenerValoresConfigurador("CorreoNotificacion", ConfigurationManager.AppSettings("BD")))
            correo.SubjectEncoding = System.Text.Encoding.UTF8
            correo.Subject = "Notificacion: " + idTransaccion + " Intecus" + notificacion.TransaccionCliente + "TransaccionCLietne"
            correo.Body = notificacion.Mensaje
            correo.BodyEncoding = System.Text.Encoding.UTF8
            correo.IsBodyHtml = True '' (formato tipo web o normal: true = web)
            correo.Priority = MailPriority.High ''prioridad

            smtp.Credentials = New System.Net.NetworkCredential(correRemitente, operacionesBD.ObtenerValoresConfigurador("ContrasenaCorreo", ConfigurationManager.AppSettings("BD")))
            smtp.Port = operacionesBD.ObtenerValoresConfigurador("PuertoCorreo", ConfigurationManager.AppSettings("BD"))
            smtp.Host = operacionesBD.ObtenerValoresConfigurador("HOSTCorreo", ConfigurationManager.AppSettings("BD"))
            smtp.EnableSsl = True

            smtp.Send(correo)
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(10, idTransaccion, 2, "ClientePrueba - NotificarCliente " + ex.Message,
                                                           ConfigurationManager.AppSettings("BD"))
            Throw ex
        End Try
    End Sub

    Public Sub RollBack(notificacion As RespuestaDeviceStatus, idTransaccion As String, varInicio As ConfInicio) Implements IOperacionesCliente.RollBack
        Throw New NotImplementedException()
    End Sub

    Public Function ObtenerRealce(peticion As Peticion, transaccion As Integer, varInicio As ConfInicio) As RespuestaWS Implements IOperacionesCliente.ObtenerRealce
        Dim m_RespuestaWS As RespuestaWS = New RespuestaWS()
        Try
            m_RespuestaWS.codigo = "00"
            m_RespuestaWS.descripcion = ""
            m_RespuestaWS.numAutorizacion = ""
            m_RespuestaWS.parametros = New List(Of ParametrosXML)()
            'Dim m_Parametro As ParametrosXML = New ParametrosXML()
            'm_Parametro.Nombre = "DATAITEM"
            'm_Parametro.NameCard = "@FIRST_NAME@"
            'm_Parametro.Typo = "string"
            'm_Parametro.Valor = peticion.nombreLargo
            'm_RespuestaWS.parametros.Add(m_Parametro)
            'm_Parametro = New ParametrosXML()
            'm_Parametro.Nombre = "DATAITEM"
            'm_Parametro.NameCard = "@PAN@"
            'm_Parametro.Typo = "string"
            'm_Parametro.Valor = peticion.nombreRealce
            'm_RespuestaWS.parametros.Add(m_Parametro)
            'm_Parametro = New ParametrosXML()
            'm_Parametro.Nombre = "DATAITEM"
            'm_Parametro.NameCard = "@ExpirationDate@"
            'm_Parametro.Typo = "string"
            'm_Parametro.Valor = peticion.direccionResidencia1
            'm_RespuestaWS.parametros.Add(m_Parametro)
            'm_Parametro = New ParametrosXML()
            'm_Parametro.Nombre = "DATAITEM"
            'm_Parametro.NameCard = "@PIN@"
            'm_Parametro.Typo = "string"
            'm_Parametro.Valor = peticion.BIN
            'm_RespuestaWS.parametros.Add(m_Parametro)
        Catch ex As Exception
            m_RespuestaWS.codigo = "01"
            m_RespuestaWS.descripcion = ex.Message
            m_RespuestaWS.numAutorizacion = ""
            operacionesBD.GuardarLogXNombreConectionString(14, transaccion, 2, "ClientePrueba - EjecutarOperacion " + ex.Message,
                                                            ConfigurationManager.AppSettings("BD"))
        End Try
        Return m_RespuestaWS
    End Function
End Class
