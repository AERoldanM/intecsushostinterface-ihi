﻿Imports Intecsus.Entidades
Imports System.Configuration
Imports Intecsus.GeneradorValoresCore
Imports Intecusus.OperarBD
Imports Intecsus.OperarArchivos
Imports System.Xml

Public Class ClienteLineaArchivo
    Implements IOperacionesCliente, IProcesoArchivos


#Region "Variables"
    Private operacionesBD As OperacionesBase = New OperacionesBase()
    Private gestorParametros As GeneradorParametros = New GeneradorParametros()
    Private xmlArchivo As String = operacionesBD.ObtenerValoresConfigurador("ArchivoPlano", ConfigurationManager.AppSettings("BD"))
#End Region

#Region "ImplementarInterfaz"

    Public Sub NotificarCliente(notificacion As Entidades.RespuestaDeviceStatus, idTransaccion As String, varInicio As ConfInicio) Implements IOperacionesCliente.NotificarCliente
        Try
            Dim m_operar As ICArchivos = New RetornarArchivo().IntanciarArchivo("TXT")
            Dim m_rutaArchivo = operacionesBD.ObtenerValoresConfigurador("RutaArchivoNotifica", ConfigurationManager.AppSettings("BD"))
            Dim m_mensaje As String = "Transacion : " + idTransaccion + " Mensaje " + notificacion.Estado.ToString() + "  " + notificacion.Mensaje
            m_operar.AgregarLineaArchivo(m_rutaArchivo, "Notificaciones.txt", m_mensaje)
            operacionesBD.GuardarLogXNombreConectionString(10, notificacion.hostIdentifier, 1, "OperarCMS.ClienteLineaArchivo.vb - NotificarCliente  ",
                                                        ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception

            operacionesBD.GuardarLogXNombreConectionString(10, notificacion.hostIdentifier, 2, "OperarCMS.ClienteLineaArchivo.vb - NotificarCliente : " + ex.Message,
                                                        ConfigurationManager.AppSettings("BD"))
            Throw ex
        End Try
    End Sub

    Public Sub RollBack(ByVal notificacion As RespuestaDeviceStatus, ByVal idTransaccion As String, varInicio As ConfInicio) Implements IOperacionesCliente.RollBack

    End Sub

    Public Function ObtenerRealce(peticion As Entidades.Peticion, transaccion As Integer, confIni As ConfInicio) As Entidades.RespuestaWS Implements IOperacionesCliente.ObtenerRealce
        Dim m_respuesta As RespuestaWS = New RespuestaWS()
        Try
            m_respuesta.codigo = "00"
            m_respuesta.descripcion = ""
            m_respuesta.numAutorizacion = ""
            m_respuesta.parametros = New List(Of ParametrosXML)()
            Dim m_xml As XmlDocument = New XmlDocument()
            m_xml.LoadXml(xmlArchivo)
            peticion = Me.ParametrosCliente(peticion, m_xml, peticion.nombreLargo)
            m_respuesta = gestorParametros.RetornarParametrosCliente(peticion, confIni, transaccion, m_respuesta)
            m_respuesta.parametros = gestorParametros.ObtenerParametros(m_respuesta, peticion.nombreLargo)
        Catch ex As Exception
            m_respuesta.codigo = "01"
            m_respuesta.descripcion = ex.Message
            m_respuesta.numAutorizacion = ""
            operacionesBD.GuardarLogXNombreConectionString(14, transaccion, 2, "ClienteLineaArchivo - ObtenerRealce " + ex.Message,
                                                           ConfigurationManager.AppSettings("BD"))
        End Try

        Return m_respuesta
    End Function
#End Region

#Region "Metodos Privados"

    ''' <summary>
    ''' Optimizar
    ''' </summary>
    ''' <param name="cliente"></param>
    ''' <param name="xml"></param>
    ''' <param name="linea"></param>
    ''' <returns></returns>
    ''' <remarks>AERM 2/3/2015</remarks>
    Private Function ParametrosCliente(cliente As Peticion, xml As XmlDocument, linea As String) As Peticion
        Try
            Dim nodo As XmlNode = xml.SelectSingleNode("Parametros/Producto")
            If nodo IsNot Nothing Then
                Dim m_Inicio As Integer = CInt(nodo.Attributes("inicio").Value)
                Dim m_Tamano As Integer = CInt(nodo.Attributes("tamano").Value)
                If (linea.Length >= m_Inicio + m_Tamano) Then
                    cliente.tipoProducto = linea.Substring(m_Inicio, m_Tamano).Trim()
                End If
            End If

            nodo = xml.SelectSingleNode("Parametros/Impresora")
            If nodo IsNot Nothing Then
                Dim m_Inicio As Integer = CInt(nodo.Attributes("inicio").Value)
                Dim m_Tamano As Integer = CInt(nodo.Attributes("tamano").Value)
                If (linea.Length >= m_Inicio + m_Tamano) Then
                    cliente.printName = linea.Substring(m_Inicio, m_Tamano).Trim()
                End If
            End If

            nodo = xml.SelectSingleNode("Parametro/Cliente")
            If nodo IsNot Nothing Then
                Dim m_Inicio As Integer = CInt(nodo.Attributes("inicio").Value)
                Dim m_Tamano As Integer = CInt(nodo.Attributes("tamano").Value)
                If (linea.Length >= m_Inicio + m_Tamano) Then
                    cliente.cliente = linea.Substring(m_Inicio, m_Tamano).Trim()
                End If
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message + " ParametrosCliente")
        End Try

        Return cliente
    End Function

#End Region

End Class
