﻿Imports System.ServiceModel.Dispatcher
Imports System.ServiceModel.Description
Imports System.Configuration
Imports Intecusus.OperarBD

''' <summary>
''' Clase encargada de obtener la respuesta de un servicioantes q lo reciba el metodo q lo consume
''' </summary>
''' <remarks>AERM 28-10-2014</remarks>
Public Class RespuestaServicio
    Implements IClientMessageInspector

#Region "Variables"
    Private operacionesBD As OperacionesBase = New OperacionesBase()
#End Region

    ''' <summary>
    ''' Metodo que se eejecuta despues de la operacion
    ''' </summary>
    ''' <param name="reply"></param>
    ''' <param name="correlationState"></param>
    ''' <remarks>AERM 28-10-2014</remarks>
    Public Sub AfterReceiveReply(ByRef reply As ServiceModel.Channels.Message, correlationState As Object) Implements IClientMessageInspector.AfterReceiveReply
        Try
            operacionesBD.GuardarLogXNombreConectionString(21, 0, 1, "-NotificarCliente Resp: " + reply.ToString(),
                                                       ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(21, 0, 2, "AfterReceiveReply-NotificarCliente Resp: " + ex.Message,
                                                           ConfigurationManager.AppSettings("BD"))
        End Try
        Console.WriteLine("Message: {0}", reply.ToString())
    End Sub

    ''' <summary>
    ''' Metodo q se ejecuta antes de enviar operacion
    ''' </summary>
    ''' <param name="request"></param>
    ''' <param name="channel"></param>
    ''' <returns></returns>
    ''' <remarks>AERM 28-10-2014</remarks>
    Public Function BeforeSendRequest(ByRef request As ServiceModel.Channels.Message, channel As ServiceModel.IClientChannel) As Object Implements IClientMessageInspector.BeforeSendRequest
        Return Nothing

    End Function
End Class

Public Class InspectorBehavior
    Implements IEndpointBehavior

    Public Sub ApplyClientBehavior(endpoint As ServiceEndpoint, clientRuntime As ClientRuntime) Implements IEndpointBehavior.ApplyClientBehavior
        clientRuntime.MessageInspectors.Add(New RespuestaServicio())
    End Sub

    Public Sub AddBindingParameters(endpoint As ServiceEndpoint, bindingParameters As ServiceModel.Channels.BindingParameterCollection) Implements IEndpointBehavior.AddBindingParameters

    End Sub



    Public Sub ApplyDispatchBehavior(endpoint As ServiceEndpoint, endpointDispatcher As EndpointDispatcher) Implements IEndpointBehavior.ApplyDispatchBehavior

    End Sub

    Public Sub Validate(endpoint As ServiceEndpoint) Implements IEndpointBehavior.Validate

    End Sub
End Class
