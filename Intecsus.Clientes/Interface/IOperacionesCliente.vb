﻿Imports Intecsus.Entidades
Imports Intecsus.GeneradorValoresCore

''' <summary>
''' Interfaz que representa las operaciones q puede realizar un cliente
''' </summary>
''' <remarks>AERM 19/03/2015</remarks>
Public Interface IOperacionesCliente
    ''' <summary>
    ''' Metodo encargado de realizar la operacion de realce o para ASNET todas
    ''' </summary>
    ''' <param name="peticion">Peticion que se envia al cliente</param>
    ''' <param name="transaccion">Identificador de la transaccion</param>
    ''' <returns>Retorna obejto con la respuesta de la peticion</returns>
    ''' <remarks>AERM 24/03/2015</remarks>
    Function ObtenerRealce(ByVal peticion As Peticion, transaccion As Integer, varInicio As ConfInicio) As RespuestaWS

    ''' <summary>
    ''' Metodo encargado de realizar la operacion de notificar al cliente
    ''' </summary>
    ''' <param name="notificacion">Objeto q contiene la informacion a notificar</param>
    ''' <param name="idTransaccion">Identificador de la transaccion por parte del cliente</param>
    ''' <remarks>AERM 24/03/2015</remarks>
    Sub NotificarCliente(ByVal notificacion As RespuestaDeviceStatus, ByVal idTransaccion As String, varInicio As ConfInicio)

    ''' <summary>
    ''' Metodo encargado de realizar la operacion de rollback para un cliente
    ''' </summary>
    ''' <param name="notificacion">Objeto q contiene la informacion a notificar</param>
    ''' <param name="idTransaccion">Identificador de la transaccion por parte del cliente</param>
    ''' <remarks>AERM 24/03/2015</remarks>
    Sub RollBack(ByVal notificacion As RespuestaDeviceStatus, ByVal idTransaccion As String, varInicio As ConfInicio)
End Interface
