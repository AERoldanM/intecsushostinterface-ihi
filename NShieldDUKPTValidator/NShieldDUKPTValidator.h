#ifdef __cplusplus
extern "C" {
#endif

// NShieldCryptoki.h : main header file for the NShieldCryptoki DLL

#include <windows.h>

// ERROR CODES
#define INICIA_ERROR		1001
#define SESION_ERROR		1002
#define LOGIN_ERROR			1003
#define NOKEY_ERROR			1004
#define ENCRYPT_ERROR		1005
#define DECRYPT_ERROR		1006
#define LOGOUT_ERROR		1007
#define SESION_CLS_ERROR	1008

// PIN ERROR CODES
#define PIN_SECUENCE		2001
#define PIN_SAME_DIGIT		2002
#define PIN_LEN				2003
#define PINS_NO_EQUALS		2004

#define STACK_CALL __cdecl

#ifdef NSHIELDCRYPTOKI_EXPORTS
   #define DLLDIR  __declspec(dllexport)   // export DLL information
#else
   #define DLLDIR  __declspec(dllimport)   // import DLL information
#endif 

/**
* TDES:		funci�n que se encarga de encriptar/desencriptar datos haciendo llamadas a funciones
*			PKCS#11 del SafeNet. Siempre se emplea el modo CBC
* in_hSession:		handler de la sesion abierta por el hilo
* in_hKey:			handler de la llave encontrada por el hilo
* data_in:			data de entrada a operar (hex-string)
* data_out:			data de salida (hex-string)
* modo:				"E": encriptar, "D": desencriptar
* IV:				Verctor de inicializaci�n (hex-string)
*/
DLLDIR int STACK_CALL TDES(char *in_hSession, char *in_hKey, char *data_in, char *data_out, char *mode, char *IV);

/**
 * INICIALIZA_PKCS11: carga las funciones PKCS11 directamente de la cryptoki.dll
 * llama a la funci�n para inicilizar las estructura en memoria de PKCS11
 * Esta funci�n solo debe ser llamada una vez por proceso
 */
DLLDIR int STACK_CALL INICIAR_PKCS11();

/**
 * INICIAR_RECURSOS: Inicializa los recursos tales como sesi�n pkcs11, login, y busqueda de llave
 * token_name:		nombre del token
 * passphrase:		passphrase del token
 * bdk_label:		etiqueta de la bdk
 * panek_label:		etiqueta de la panek (llave para encriptado de PAN)
 * out_hSession:	manejador de la sesion abierta por esta funcion
 * out_hBdk:		manejador de la llave bdk
 * out_hPanek:		manejador de la llave panek
 */
DLLDIR int STACK_CALL INICIAR_RECURSOS(char *token_name, char *passphrase, char *BDK_label, char *PANEK_label, char *out_hSession, char *out_hBDK, char *out_hPANEK);

/**
 * FINALIZAR_RECURSOS: Finaliza los recursos reservados por el hilo, tales como la sesi�n pkcs11, logout, llave abierta
 * in_hSession: el handler de la sesi�n abierta por el hilo
 */
DLLDIR int STACK_CALL FINALIZAR_RECURSOS(char *in_hSession);

/**
* VALIDAR_DUKPT:	funci�n que se encarga de hacer validaciones sobre dos PINBlocks DUKPT
*					Validaci�n 1: Que sea de longitud 4
*					Validaci�n 2: Que no sean secuencia: 0123, 1234, 2345, ..., 6789, 9876, ..., 3210.
*					Validaci�n 3: Que no sean valores repetidos: 1111, 2222, ..., 9999
* in_hSession:		handler de la sesion abierta por el hilo
* in_hBDK:			handler de la llave BDK (Base Derivation Key)
* in_hPANEK:		handler de la llave de encripci�n de PAN
* EPAN				PAN encriptado (hex-string)
* PINBlock1:		PINBlock1 (hex-string)
* KSN1:				KSN1 (hex-string)
* PINBlock2:		PINBlock2 (hex-string)
* KSN2:				KSN2 (hex-string)
*/
DLLDIR int STACK_CALL VALIDAR_DUKPT(char *in_hSession, char *in_hBDK, char *in_hPANEK, char *EPAN, char *PINBlock1, char *KSN1, char *PINBlock2, char *KSN2);

#ifdef __cplusplus
}
#endif
