// NShieldCryotoki.cpp : Defines the initialization routines for the DLL.

#include "stdafx.h"
#include <stdio.h>
#include <process.h>
#include <stdlib.h>
#include <time.h>
#include "NShieldDUKPTValidator.h"
#include "cryptoki.h"

#define NUM(a) (sizeof(a) / sizeof((a)[0]))
#define LOG_PATH "C:\\WINDOWS\\Temp\\NShieldDUKPTValidator"

static bool initialized = false;
//Masks
static char KEY_MASK[16] = {0xC0, 0xC0, 0xC0, 0xC0, 0x00, 0x00, 0x00, 0x00, 0xC0, 0xC0, 0xC0, 0xC0, 0x00, 0x00, 0x00, 0x00};
static char KSN_MASK[10] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xE0, 0x00, 0x00};
static char KSN_MASK2[8] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xE0, 0x00, 0x00};
static char PEK_MASK[16] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF};
static char DEK_MASK[16] = {0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00};

// funciones privadas
static int hex2str(char *to, char *from, int len);
static int str2hex(char *to, char *from, int len);
static CK_RV load_3des_key(CK_SESSION_HANDLE hSession, CK_BYTE_PTR clearKey, CK_OBJECT_HANDLE_PTR phKey);
static CK_RV get_key(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hKey, CK_BYTE_PTR out_clearKey);
static CK_RV create_IPEK(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hBDK, char *KSN, char *IPEK);
static CK_RV create_SK(CK_SESSION_HANDLE hSession, char *IPEK, char *KSN, char *SK);
static CK_RV derive_key(CK_SESSION_HANDLE hSession, char *IPEK, char *KSN, char *DK);
static CK_RV generate_key(CK_SESSION_HANDLE hSession, char *cur_key_cpy,char * ksn_reg, char *cur_key);
static CK_RV init_serial(unsigned int *ksn_serial, char *KSN);
static CK_RV encrypt_register(CK_SESSION_HANDLE hSession, char *key, char *ksn_reg, char *key_out);
static int print_componet(FILE *fout, CK_BYTE *component, CK_LONG len);
static CK_RV update_mask(char *tmp_mask, unsigned int ksn_reg, int len);
static CK_RV or_ksn_reg(char *ksn_reg, int shift_reg);
static CK_RV get_clear_pan(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hPANEK, char *PANE, char *PANC);
static CK_RV get_clear_pin(CK_SESSION_HANDLE hSession, char *PB, char *SK, char *PANC, char *PIN);
static int get_pin(char *PIN, char *PANC, char *clear_PB);
static CK_RV do_validations(char *PIN);
static CK_RV validate_pin_len(char *PIN);
static CK_RV validate_pin_sec(char *PIN);
static CK_RV validate_pin_rep(char *PIN);

static FILE *FLOG = 0;

static int open_log()
{
	char LOG_PATH_C[128];
	if(FLOG == 0) {
		sprintf(LOG_PATH_C, "%s-%d.log", LOG_PATH, getpid());
		FLOG = fopen(LOG_PATH_C, "a");
		if(FLOG) setvbuf(FLOG, NULL, _IONBF, 0);
	}
	return 0;
}

static int close_log()
{
	if(FLOG != 0) {
		fclose(FLOG);
		FLOG = 0;
	}
	return 0;
}

static int print_componet(FILE *fout, CK_BYTE *component, CK_LONG len) {
	CK_LONG i;
	for (i = 0; i < len; i++) {
		fprintf(fout, "%02x", component[i]);
	}
	fprintf(fout, "\n");
	return 0;
}

static int hex2str(char *to, char *from, int len)
{
	int i;
	char str[32];

	//printf(">>hex2str()>>\n");
	to[0] = '\0';
	for(i = 0; i < len; i++) {
		sprintf(str, "%02x", 0x00FF&from[i]);
		strcat(to, str);
	}

	//printf("<<hex2str()<<\n");
	return 0;
}

static int str2hex(char *to, char *from, int len)
{
	int i;
	char str[32];

	//printf(">>str2hex()>>\n");
	for(i = 0; i < len/2; i++) {
		str[0] = from[2*i];
		str[1] = from[2*i + 1];
		str[2] = '\0';
		
		to[i] = (char)strtol(str, 0, 16);
	}
	//printf("<<str2hex()<<\n");
	return 0;
}

static CK_RV find_key(CK_SESSION_HANDLE hSession, char *key_label, CK_OBJECT_HANDLE_PTR out_hKey) {
	CK_RV rv = CKR_OK;
	CK_OBJECT_HANDLE hObject;
	CK_ULONG ulObjectCount;
	CK_ULONG key_found = 0;
	char pObjectLabel[1024];
	CK_ATTRIBUTE get_label_template[] = { { CKA_LABEL, NULL_PTR, 0 } };

	//list token objects
	rv = C_FindObjectsInit(hSession, NULL_PTR, 0);
	if (rv != CKR_OK) {
		rv = NOKEY_ERROR;
		goto err;
	}

	while (1) {
		rv = C_FindObjects(hSession, &hObject, 1, &ulObjectCount);
		if (rv != CKR_OK || ulObjectCount == 0) break;

		//print object label attribute
		get_label_template[0].pValue = NULL_PTR;
		get_label_template[0].ulValueLen = 0;
		C_GetAttributeValue(hSession, hObject, get_label_template, NUM(get_label_template));

		if (rv != CKR_OK || ulObjectCount == 0) break;

		memset(pObjectLabel, 0x00, sizeof(pObjectLabel));
		get_label_template[0].pValue = (CK_VOID_PTR)pObjectLabel;
		C_GetAttributeValue(hSession, hObject, get_label_template, NUM(get_label_template));

		if (rv != CKR_OK || ulObjectCount == 0) break;

		pObjectLabel[get_label_template[0].ulValueLen] = '\0';

		if (strcmp(pObjectLabel, key_label) == 0) {
			*out_hKey = hObject;
			key_found = 1;
			break;
		}
	}

	rv = C_FindObjectsFinal(hSession);

	if (key_found <= 0) {
		rv = NOKEY_ERROR;
		goto err;
	}

err:
	return rv;
}

static int xor_bytes(char *result, char *p1, char *p2, int len) {
	int i;
	for (i = 0; i < len; i++) {
		result[i] = p1[i] ^ p2[i];
	}
	return 0;
}

static int and_bytes(char *result, char *p1, char *p2, int len) {
	int i;
	for (i = 0; i < len; i++) {
		result[i] = p1[i] & p2[i];
	}
	return 0;
}

static CK_RV get_key(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hKey, CK_BYTE_PTR out_clearKey) {
	CK_RV rv = CKR_OK;
	CK_ATTRIBUTE get_key_template[] = { { CKA_VALUE, out_clearKey, sizeof(CK_BYTE) * 16 } };

	rv = C_GetAttributeValue(
		hSession,
		hKey,
		get_key_template,
		NUM(get_key_template));

	return rv;
}

static CK_RV load_3des_key(CK_SESSION_HANDLE hSession, CK_BYTE_PTR clearKey, CK_OBJECT_HANDLE_PTR phKey) {
	CK_RV rv = CKR_OK;
	
	//Valores
	CK_BBOOL TRUE_VAL = 1;
	CK_BBOOL FALSE_VAL = 0;
	CK_OBJECT_CLASS CLASS_SECRET = CKO_SECRET_KEY;
	CK_ULONG COMPONENT_SIZE = 16;
	CK_BYTE C1[16], C2[16];
	CK_ULONG IV_SIZE = 8;
	CK_BYTE IV[8 + 1];
	CK_MECHANISM key_mechanism = { CKM_CAC_TK_DERIVATION, NULL_PTR, 0 };
	CK_KEY_TYPE KEY_TYPE_DES2 = CKK_DES2;
	CK_BYTE KEY_LABEL[] = "TMP_3DES_KEY";	
	CK_ATTRIBUTE key_template[] = {
		{ CKA_CLASS, 		&CLASS_SECRET, sizeof(CLASS_SECRET) },
		//{ CKA_TOKEN, 		&TRUE_VAL, sizeof(CK_BBOOL) },
		{ CKA_TOKEN, 		&FALSE_VAL, sizeof(CK_BBOOL) },
		{ CKA_PRIVATE, 		&TRUE_VAL, sizeof(CK_BBOOL) },
		{ CKA_MODIFIABLE, 	&TRUE_VAL, sizeof(CK_BBOOL) },
		{ CKA_KEY_TYPE, 	&KEY_TYPE_DES2, sizeof(KEY_TYPE_DES2) },
		{ CKA_LABEL,		NULL, 0 },
		{ CKA_ENCRYPT,		&TRUE_VAL, sizeof(CK_BBOOL) },
		{ CKA_DECRYPT,		&TRUE_VAL, sizeof(CK_BBOOL) },
		{ CKA_WRAP,			&TRUE_VAL, sizeof(CK_BBOOL) },
		{ CKA_UNWRAP,		&TRUE_VAL, sizeof(CK_BBOOL) },
		{ CKA_SIGN,			&TRUE_VAL, sizeof(CK_BBOOL) },
		{ CKA_VERIFY,		&TRUE_VAL, sizeof(CK_BBOOL) },
		{ CKA_DERIVE,		&TRUE_VAL, sizeof(CK_BBOOL) },
		{ CKA_SENSITIVE,	&FALSE_VAL, sizeof(CK_BBOOL) },
		{ CKA_EXTRACTABLE,	&TRUE_VAL, sizeof(CK_BBOOL) },
		{ CKA_TKC1, 		NULL, 0 },
		{ CKA_TKC2, 		NULL, 0 },
		{ CKA_TKC3, 		NULL, 0 }
	};

	//Los dos primeros componentes nulos
	memset(C1, 0x00, sizeof(C1));
	memset(C2, 0x00, sizeof(C2));

	//Inicializacion
	key_template[5].pValue = (void *)KEY_LABEL;
	key_template[5].ulValueLen = (CK_ULONG)strlen((const char *)KEY_LABEL);

	key_template[15].pValue = (void *)C1;
	key_template[15].ulValueLen = (CK_ULONG)COMPONENT_SIZE;

	key_template[16].pValue = (void *)C2;
	key_template[16].ulValueLen = (CK_ULONG)COMPONENT_SIZE;

	key_template[17].pValue = (void *)clearKey;
	key_template[17].ulValueLen = (CK_ULONG)COMPONENT_SIZE;

	//se genera el IV random
	rv = C_GenerateRandom(hSession, IV, IV_SIZE);
	if (rv != CKR_OK) {
		goto err;
	}

	key_mechanism.pParameter = IV;
	key_mechanism.ulParameterLen = IV_SIZE;

	rv = C_GenerateKey(
		hSession,
		&key_mechanism,
		key_template,
		NUM(key_template),
		phKey);
	
	if (rv != CKR_OK) {
		goto err;
	}

err:
	return rv;
}

static CK_RV create_IPEK(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hBDK, char *KSN, char *IPEK) {
	CK_RV rv = CKR_OK;
	CK_MECHANISM mechanismTDES_ECB = { CKM_DES3_ECB };
	CK_ULONG clear_data_len;
	CK_ULONG encrypt_data_len;
	char hex_ksn[10];
	char hex_ksn_prima[10];
	char IPEK_part1[8], IPEK_part2[8];
	CK_BYTE hex_bdk[16];
	CK_BYTE hex_bdk_prima[16];
	CK_OBJECT_HANDLE hBDKP = 0x00;

	//fprintf(FLOG, "\t\t>>>create_IPEK()>>>\n");

	//Inicializacion
	memset(IPEK_part1, 0x00, 8);
	memset(IPEK_part2, 0x00, 8);
	memset(hex_bdk, 0x00, 16);

	str2hex(hex_ksn, KSN, 20);
	//print_componet(FLOG, (CK_BYTE*)hex_ksn, 10);

	and_bytes((char *)hex_ksn_prima, hex_ksn, KSN_MASK, 10);
	//print_componet(FLOG, (CK_BYTE*)hex_ksn_prima, 10);
	
	//print_componet(FLOG, (CK_BYTE*)KSN_MASK, 10);
	//print_componet(FLOG, (CK_BYTE*)hex_ksn_prima, 8);

	rv = C_EncryptInit(hSession, &mechanismTDES_ECB, hBDK);
	if (rv != CKR_OK) {
		rv = ENCRYPT_ERROR;
		goto err;
	}

	clear_data_len = 8;
	rv = C_Encrypt(
		hSession,
		(CK_BYTE_PTR)hex_ksn_prima,
		(CK_ULONG)clear_data_len,
		(CK_BYTE_PTR)IPEK_part1,
		(CK_ULONG_PTR)&encrypt_data_len);

	//fprintf(FLOG, "rv: %ul\n", rv);
	//fprintf(FLOG, "PART1: ");
	//print_componet(FLOG, (CK_BYTE *)hex_ksn_prima, 8);
	//print_componet(FLOG, (CK_BYTE *)IPEK_part1, 8);

	rv = get_key(hSession, hBDK, hex_bdk);
	if (rv != CKR_OK) {
		goto err;
	}

	//print_componet(FLOG, hex_bdk, 16);
	//print_componet(FLOG, (CK_BYTE*)KEY_MASK, 16);
	xor_bytes((char *)hex_bdk_prima, (char *)hex_bdk, KEY_MASK, 16);

	rv = load_3des_key(hSession, hex_bdk_prima, &hBDKP);
	if (rv != CKR_OK) {
		goto err;
	}

	rv = C_EncryptInit(hSession, &mechanismTDES_ECB, hBDKP);
	if (rv != CKR_OK) {
		rv = ENCRYPT_ERROR;
		goto err;
	}

	clear_data_len = 8;
	rv = C_Encrypt(
		hSession,
		(CK_BYTE_PTR)hex_ksn_prima,
		(CK_ULONG)clear_data_len,
		(CK_BYTE_PTR)IPEK_part2,
		(CK_ULONG_PTR)&encrypt_data_len);

	//fprintf(FLOG, "PART2: ");
	//print_componet(FLOG, (CK_BYTE *)IPEK_part2, 8);
	if (rv != CKR_OK) {
		goto err;
	}

	//Se copia IPEK
	memcpy(IPEK + 0, IPEK_part1, 8);
	memcpy(IPEK + 8, IPEK_part2, 8);
	
err:

	//fprintf(FLOG, "\t\t<<<create_IPEK()<<<\n");
	if (hBDKP != 0x00) {
		C_DestroyObject(hSession, hBDKP);
	}

	return rv;
}

static CK_RV create_SK(CK_SESSION_HANDLE hSession, char *IPEK, char *KSN, char *SK) {
	CK_RV rv = CKR_OK;
	char DK[16];

	memset(DK, 0x00, sizeof(DK));
	rv = derive_key(hSession, IPEK, KSN, DK);
	xor_bytes(SK, DK, PEK_MASK, 16);
	//fprintf(FLOG, "Derive Key():\n");
	//print_componet(FLOG, (CK_BYTE*)DK, 16);
	return rv;
}

static CK_RV derive_key(CK_SESSION_HANDLE hSession, char *IPEK, char *KSN, char *DK) {
	CK_RV rv = CKR_OK;
	char ksn_reg[8];
	char cur_key[16];
	char cur_key_cpy[16];
	unsigned int ksn_serial = 0;
	char hex_ksn[10];

	//Inicializacion
	memset(ksn_reg, 0x00, sizeof(ksn_reg));
	memset(cur_key, 0x00, sizeof(cur_key));
	memset(hex_ksn, 0x00, sizeof(hex_ksn));

	str2hex(hex_ksn, KSN, 20);
	//fprintf(FLOG, "hex_ksn:\n");
	//print_componet(FLOG, (CK_BYTE*)hex_ksn, 10);

	init_serial(&ksn_serial, hex_ksn);

	and_bytes(ksn_reg, hex_ksn + 2, KSN_MASK2, 8);

	memcpy(cur_key, IPEK, 16);
	//fprintf(FLOG, "generate_key(): Current Key (I):\n");
	//print_componet(FLOG, (CK_BYTE*)cur_key, 16);

	//fprintf(FLOG, "ksn_serial: %x\n", ksn_serial);

	for (int shift_reg = 0x100000; shift_reg > 0; shift_reg >>= 1) {

		//fprintf(FLOG, "ksn_reg:\n");
		//print_componet(FLOG, (CK_BYTE*)ksn_reg, 8);

		if ((shift_reg & ksn_serial & 0x1FFFFF) > 0) {
			
			memcpy(cur_key_cpy, cur_key, 16);
			
			//fprintf(FLOG, "ksn serial:\n");
			//print_componet(FLOG, (CK_BYTE*)&ksn_serial, 4);
			
			or_ksn_reg(ksn_reg, shift_reg);
			
			generate_key(hSession, cur_key_cpy, ksn_reg, cur_key);
			
			//fprintf(FLOG, "generate_key(): Current Key:\n");
			//print_componet(FLOG, (CK_BYTE*)cur_key, 16);
		}
	}

	memcpy(DK, cur_key, 16);

	return rv;
}

static CK_RV generate_key(CK_SESSION_HANDLE hSession, char *key, char *ksn_reg, char *key_out) {
	CK_RV rv = CKR_OK;
	char key_part1[8];
	char key_part2[8];
	char key_prima[16];

	//Inicializacion
	memset(key_part1, 0x00, sizeof(key_part1));
	memset(key_part2, 0x00, sizeof(key_part2));
	memset(key_prima, 0x00, sizeof(key_prima));
	
	xor_bytes(key_prima, key, KEY_MASK, 16);
	//fprintf(FLOG, "key:\n");
	//print_componet(FLOG, (CK_BYTE*)key, 16);
	//fprintf(FLOG, "key prima:\n");
	//print_componet(FLOG, (CK_BYTE*)key_prima, 16);

	encrypt_register(hSession, key_prima,	ksn_reg, key_part1);
	encrypt_register(hSession, key,			ksn_reg, key_part2);
	
	//fprintf(FLOG, "parte1:\n");
	//print_componet(FLOG, (CK_BYTE*)key_part1, 8);
	//fprintf(FLOG, "parte2:\n");
	//print_componet(FLOG, (CK_BYTE*)key_part2, 8);

	memcpy(key_out + 0, key_part1, 8);
	memcpy(key_out + 8, key_part2, 8);

	return rv;
}

static CK_RV or_ksn_reg(char *ksn_reg, int shift_reg) {
	CK_RV rv = CKR_OK;
	ksn_reg[7] |= shift_reg & 0xFF;
	ksn_reg[6] |= (shift_reg & 0xFF00) >> 8;
	ksn_reg[5] |= (shift_reg & 0xFF0000) >> 8 * 2;
	ksn_reg[4] |= (shift_reg & 0xFF000000) >> 8 * 3;
	return rv;
}

static CK_RV init_serial(unsigned int *ksn_serial, char *KSN) {
	CK_RV rv = CKR_OK;

	*ksn_serial = 0;
	for (int i = 9; i >= 7; i--) {
		*ksn_serial |= KSN[i] << 8 * (9 - i);
	}

	*ksn_serial &= 0x00FFFFFF;

	return rv;
}

static CK_RV encrypt_register(CK_SESSION_HANDLE hSession, char *key, char *ksn_reg, char *key_out) {
	CK_RV rv = CKR_OK;
	CK_MECHANISM mechanismDES3_ECB = { CKM_DES3_ECB };
	CK_OBJECT_HANDLE hKey = 0x00;
	CK_BYTE clear_data[8];
	CK_ULONG clear_data_len;
	CK_BYTE ecrypted_data[8];
	CK_ULONG ecrypted_data_len;
	char key_tmp[16];

	//Inicializacion
	memcpy(key_tmp + 0, key, 8);
	memcpy(key_tmp + 8, key, 8);

	rv = load_3des_key(hSession, (CK_BYTE_PTR)key_tmp, &hKey);
	if (rv != CKR_OK) {
		goto err;
	}

	rv = C_EncryptInit(hSession, &mechanismDES3_ECB, hKey);
	if (rv != CKR_OK) {
		rv = ENCRYPT_ERROR;
		goto err;
	}

	clear_data_len = 8;
	xor_bytes((char*)clear_data, key + 8, ksn_reg, 8);
	//fprintf(FLOG, "clear_data:\n");
	//print_componet(FLOG, (CK_BYTE*)clear_data, 8);
	
	rv = C_Encrypt(
		hSession,
		(CK_BYTE_PTR)clear_data,
		(CK_ULONG)clear_data_len,
		(CK_BYTE_PTR)ecrypted_data,
		(CK_ULONG_PTR)&ecrypted_data_len);

	xor_bytes(key_out, key + 8, (char*)ecrypted_data, 8);

err:
	if (hKey != 0x00) {
		C_DestroyObject(hSession, hKey);
	}
	return rv;
}

static CK_RV update_mask(char *tmp_mask, unsigned int ksn_reg, int len) {
	CK_RV rv = CKR_OK;
	int reg_len = 4;
	for (int i = len - 1; i > len - reg_len - 1; i--) {
		tmp_mask[reg_len + (len - i)] = (ksn_reg >> 8*(len - i -1)) & 0x00FF;
	}

	return rv;
}

static CK_RV get_clear_pan(CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE hPANEK, char *PANE, char *PANC) {

	CK_RV rv = CKR_OK;
	CK_MECHANISM mechanismTDES_ECB = { CKM_DES3_ECB };
	CK_BYTE encrypt_data[256];
	CK_ULONG encrypt_data_len;
	CK_BYTE clear_data[256];
	CK_BYTE clear_data_str[256];
	CK_ULONG clear_data_len;

	int i;

	//fprintf(FLOG, "\t\t>>>XXX()>>>\n");

	//Inicializacion
	memset(encrypt_data, 0x00, sizeof(encrypt_data));
	memset(clear_data, 0x00, sizeof(clear_data));
	memset(clear_data_str, 0x00, sizeof(clear_data_str));

	encrypt_data_len = strlen(PANE) / 2;
	str2hex((char*)encrypt_data, PANE, encrypt_data_len * 2);

	rv = C_DecryptInit(hSession, &mechanismTDES_ECB, hPANEK);
	if (rv != CKR_OK) {
		rv = ENCRYPT_ERROR;
		goto err;
	}

	rv = C_Decrypt(
		hSession,
		(CK_BYTE_PTR)encrypt_data,
		(CK_ULONG)encrypt_data_len,
		(CK_BYTE_PTR)clear_data,
		(CK_ULONG_PTR)&clear_data_len);

	hex2str((char*)clear_data_str, (char*)clear_data, clear_data_len * 2);

	i = 0;
	while (clear_data_str[i] != 'f') i++;
	//strncpy(PANC, (char *)clear_data_str + i, clear_data_len * 2);
	strncpy(PANC, (char *)clear_data_str, i);
	PANC[i] = '\0';

err:

	return rv;
}

static CK_RV get_clear_pin(CK_SESSION_HANDLE hSession, char *PB, char *SK, char *PANC, char *PIN) {
	CK_RV rv = CKR_OK;

	CK_BYTE encrypt_PB[64];
	CK_ULONG encrypt_PB_len;
	CK_BYTE clear_PB[64];
	CK_ULONG clear_PB_len;
	
	CK_OBJECT_HANDLE hSK = 0x00;
	CK_MECHANISM mechanismTDES_ECB = { CKM_DES3_ECB };

	//Inicializacion
	memset(encrypt_PB, 0x00, sizeof(encrypt_PB));
	memset(clear_PB, 0x00, sizeof(clear_PB));

	load_3des_key(hSession, (CK_BYTE_PTR)SK, &hSK);

	rv = C_DecryptInit(hSession, &mechanismTDES_ECB, hSK);
	if (rv != CKR_OK) {
		rv = ENCRYPT_ERROR;
		goto err;
	}

	encrypt_PB_len = strlen(PB);
	str2hex((char*)encrypt_PB, PB, encrypt_PB_len);

	rv = C_Decrypt(
		hSession,
		(CK_BYTE_PTR)encrypt_PB,
		(CK_ULONG)encrypt_PB_len,
		(CK_BYTE_PTR)clear_PB,
		(CK_ULONG_PTR)&clear_PB_len);

	get_pin(PIN, PANC, (char*)clear_PB);

err:
	if (hSK != 0x00) {
		C_DestroyObject(hSession, hSK);
	}
	return rv;
}

static int get_pin(char *PIN, char *PANC, char *clear_PB) {
	char datagram[8];
	char datagram_str[16+1];
	char clear_pin[8];
	char clear_pin_str[16 + 1];

	//Inicializacion
	memset(datagram, 0x00, sizeof(datagram));
	memset(datagram_str, 0x00, sizeof(datagram_str));
	memset(clear_pin, 0x00, sizeof(clear_pin));
	memset(clear_pin_str, 0x00, sizeof(clear_pin_str));

	//Proceso
	memset(datagram_str, 0x30, 4);
	memcpy(datagram_str + 4, PANC + (strlen(PANC) - 12 - 1), 12);
	datagram_str[16] = '\0';

	str2hex(datagram, datagram_str, 16);
	xor_bytes(clear_pin, datagram, clear_PB, 8);

	hex2str(clear_pin_str, clear_pin, 8);

	//fprintf(FLOG, "datagram: %s\n", datagram_str);
	//fprintf(FLOG, "clear_pin_str: %s\n", clear_pin_str);
	
	//clear_pin_str[0] == 0 - ISO-0
	memcpy(PIN, clear_pin_str + 2, clear_pin_str[1] - '0');
	PIN[clear_pin_str[1]] = '\0';

	return 0;
}

static CK_RV do_validations(char *PIN) {
	CK_RV rv = CKR_OK;

	rv = validate_pin_len(PIN);
	if (rv != CKR_OK) {
		rv = PIN_LEN;
		goto err;
	}

	rv = validate_pin_sec(PIN);
	if (rv != CKR_OK) {
		rv = PIN_SECUENCE;
		goto err;
	}

	rv = validate_pin_rep(PIN);
	if (rv != CKR_OK) {
		rv = PIN_SAME_DIGIT;
		goto err;
	}

err:
	return rv;
}

static CK_RV validate_pin_len(char *PIN) {
	return 4 - strlen(PIN);
}
static CK_RV validate_pin_sec(char *PIN) {
	CK_RV rv = CKR_OK;
	int len = strlen(PIN);
	int i;
	
	for (i = 1; i < len; i++) {
		if (PIN[i] != PIN[i - 1] + 1) {
			break;
		}
	}

	if (i == len) {
		rv = 1;
		goto err;
	}

	for (i = 1; i < len; i++) {
		if (PIN[i] != PIN[i - 1] - 1) {
			break;
		}
	}

	if (i == len) {
		rv = -1;
		goto err;
	}

err:
	return rv;
}

static CK_RV validate_pin_rep(char *PIN) {
	CK_RV rv = CKR_OK;
	int i;
	int len = strlen(PIN);
	char val = PIN[0];
	
	for (i = 1; i < len; i++) {
		if (PIN[i] != val) {
			break;
		}
	}
	if (i == len) {
		rv = 1;
		goto err;
	}
err:
	return rv;
}

DLLDIR int STACK_CALL TDES(char *in_hSession, char *in_hKey, char *data_in, char *data_out, char *mode, char *IV)
{
	// variables locales
	CK_RV rv = CKR_OK;
	CK_SESSION_HANDLE hSession;
	CK_OBJECT_HANDLE hObject;
	CK_BYTE IV_BUFFER[8];
	char encrypt_data[1024];
	char clear_data[1024];
	CK_ULONG encrypt_data_len;
	CK_ULONG clear_data_len;

	time_t t = time(NULL);
	struct tm * p = localtime(&t);
	char time_str[1024];
	strftime(time_str, sizeof(time_str), "[%Y-%m-%d %H:%M:%S] - ", p);

	open_log();

	//fprintf(FLOG, "%s >>>TDES()>>>\n", time_str);
	if(!initialized) {
		rv = INICIA_ERROR;
		goto err;
	}

	str2hex((char*)IV_BUFFER, IV, strlen(IV));

	CK_MECHANISM mechanismTDES_ECB = {CKM_DES3_ECB};
	CK_MECHANISM mechanismTDES_CBC = {CKM_DES3_CBC, IV_BUFFER, sizeof(IV_BUFFER)};

	hSession = (CK_SESSION_HANDLE)atol(in_hSession);
	hObject = (CK_OBJECT_HANDLE)atol(in_hKey);

	memset(clear_data,		0x00, sizeof(clear_data));
	memset(encrypt_data,	0x00, sizeof(encrypt_data));
	
	if(strcmp(mode, "E") == 0) {
		str2hex(clear_data, data_in, strlen(data_in));
		rv = C_EncryptInit(hSession, &mechanismTDES_CBC, hObject);
		
		if (rv != CKR_OK) {
			fprintf(FLOG, "%s Error __DLL_C_EncryptInit 0x%08x\n", time_str, rv);
			rv = ENCRYPT_ERROR;
			goto err;
		}
		
		clear_data_len = strlen(data_in)/2;
		encrypt_data_len = clear_data_len;
		
		rv = C_Encrypt(
			hSession,
			(CK_BYTE_PTR)clear_data,
			(CK_ULONG)clear_data_len,
			(CK_BYTE_PTR)encrypt_data,
			(CK_ULONG_PTR)&encrypt_data_len);
		
		if (rv != CKR_OK) {
			fprintf(FLOG, "%s Error C_Encrypt 0x%08x\n", time_str, rv);
			rv = ENCRYPT_ERROR;
			goto err;
		}
		hex2str(data_out, encrypt_data, encrypt_data_len);
	} else {
		str2hex(encrypt_data, data_in, strlen(data_in));
		
		rv = C_DecryptInit(hSession, &mechanismTDES_CBC, hObject);
		if (rv != CKR_OK) {
			fprintf(FLOG, "%s Error C_DecryptInit 0x%08x\n", time_str, rv);
			rv = DECRYPT_ERROR;
			goto err;
		}
		
		encrypt_data_len = strlen(data_in)/2;
		clear_data_len = sizeof(clear_data);

		rv = C_Decrypt(
			hSession,
			(CK_BYTE_PTR)encrypt_data,
			(CK_ULONG)encrypt_data_len,
			(CK_BYTE_PTR)clear_data,
			(CK_ULONG_PTR)&clear_data_len);
			
		if (rv != CKR_OK) {
			fprintf(FLOG, "%s Error C_Decrypt 0x%08x\n", time_str, rv);
			rv = DECRYPT_ERROR;
			goto err;
		}

		hex2str(data_out, clear_data, clear_data_len);
	}

err:
	//fprintf(FLOG, "%s <<<TDES()<<<\n", time_str);
	close_log();

	return rv;
}

DLLDIR int STACK_CALL INICIAR_PKCS11()
{
	//Variables locales
	CK_RV rv = CKR_OK;
	CK_C_INITIALIZE_ARGS init_args = {
		NULL_PTR,
		NULL_PTR,
		NULL_PTR,
		NULL_PTR,
		CKF_OS_LOCKING_OK,
		NULL_PTR};
	
	time_t t = time(NULL);
	struct tm * p = localtime(&t);
	char time_str[1024];
	strftime(time_str, sizeof(time_str), "[%Y-%m-%d %H:%M:%S] - ", p);

	open_log();

	fprintf(FLOG, "%s >>>INICIALIZA()>>>\n", time_str);

	if(!initialized) {
		//rv = functions_init();
		rv = CKR_OK;
		if (rv != CKR_OK) {
			fprintf(FLOG, "%s Error functions_init %d\n", time_str, rv);
			rv = INICIA_ERROR;
			goto err;
		}
		initialized = true;
	}

	rv = C_Initialize((CK_VOID_PTR)&init_args);
	if (rv != CKR_OK) {
		fprintf(FLOG, "%s Error C_Initialize 0x%08x\n", time_str, rv);
		rv = INICIA_ERROR;
		goto err;
	}

err:
	fprintf(FLOG, "%s <<<INICIALIZA()<<<\n", time_str);
	close_log();

	return rv;
}

DLLDIR int STACK_CALL INICIAR_RECURSOS(char *token_name, char *passphrase, char *BDK_label, char *PANEK_label, char *out_hSession, char *out_hBDK, char *out_hPANEK)
{
	//variables locales
	CK_RV rv = CKR_OK;
	CK_ULONG i, nslots;
	CK_SLOT_ID_PTR pslots = 0;
	CK_SLOT_ID hSlot;
	CK_SLOT_INFO sinfo;
	CK_TOKEN_INFO tinfo;
	CK_SESSION_HANDLE hSession;
	CK_OBJECT_HANDLE hObject;
	//CK_ULONG ulObjectCount;
	CK_ULONG login_success = 0;
	CK_ULONG key_found = 0;
	char *local_token_name[32 + 1];
	//char pObjectLabel[1024];
	//CK_ATTRIBUTE get_label_template[] = { { CKA_LABEL, NULL_PTR, 0 } };

	time_t t = time(NULL);
	struct tm * p = localtime(&t);
	char time_str[1024];
	strftime(time_str, sizeof(time_str), "[%Y-%m-%d %H:%M:%S] - ", p);

	open_log();

	fprintf(FLOG, "%s >>>INICIAR_RECURSOS()>>>\n", time_str);
	rv = C_GetSlotList(0, NULL_PTR, &nslots);
	if (rv != CKR_OK) {
		fprintf(FLOG, "%s Error C_GetSlotList 0x%08x\n", time_str, rv);
		rv = INICIA_ERROR;
		goto err;
	}

	pslots = (CK_SLOT_ID_PTR)malloc(sizeof(CK_SLOT_ID) * nslots);
	rv = C_GetSlotList(1, pslots, &nslots);
	if (rv != CKR_OK) {
		fprintf(FLOG, "%s Error C_GetSlotList 0x%08x\n", time_str, rv);
		rv = INICIA_ERROR;
		goto err;
	}

	for (i = 0; !login_success && i < nslots; i++) {
		hSlot = pslots[i];

		rv = C_GetSlotInfo(hSlot, &sinfo);
		if (rv != CKR_OK) {
			fprintf(FLOG, "%s Error C_GetSlotInfo 0x%08x\n", time_str, rv);
			goto err;
		}

		rv = C_GetTokenInfo(hSlot, &tinfo);
		if (rv != CKR_OK) {
			fprintf(FLOG, "%s Error C_GetTokenInfo 0x%08x\n", time_str, rv);
			goto err;
		}

		/* Skip write protected slots */
		if (tinfo.flags & CKF_WRITE_PROTECTED) {
			continue;
		}

		/* If we have a PIN, skip slots that don't accept it */
		if (passphrase && !(tinfo.flags & CKF_USER_PIN_INITIALIZED)) {
			continue;
		}

		/* The token label must match */
		memset(local_token_name, 0x00, sizeof(local_token_name));
		memcpy(local_token_name, tinfo.label, 32);
		local_token_name[32] = '\0';

		fprintf(FLOG, "%s token_name='%s', local_token_name='%s'\n", time_str, token_name, local_token_name);
		if (strncmp((char*)token_name, (char*)local_token_name, strlen(token_name)) != 0) {
			continue;
		}

		//CKF_RW_SESSION: in carwizard session can be read only
		rv = C_OpenSession(hSlot,
			CKF_RW_SESSION | CKF_SERIAL_SESSION,
			0, 0,
			&hSession);
		if (rv != CKR_OK) {
			fprintf(FLOG, "%s Error C_OpenSession 0x%08x\n", time_str, rv);
			rv = SESION_ERROR;
			goto err;
		}

		rv = C_Login(hSession, CKU_USER,
			(CK_CHAR_PTR)passphrase, (CK_ULONG)strlen((char *)passphrase));
		if (rv != CKR_OK && rv != CKR_USER_ALREADY_LOGGED_IN) {
			fprintf(FLOG, "%s Error C_Login 0x%08x\n", time_str, rv);
			rv = LOGIN_ERROR;
			goto err;
		}
		else {
			login_success = 1;
		}

		if (!login_success) {
			//continue to next slot
			continue;
		}
	}

	if (login_success == 0) {
		rv = LOGIN_ERROR;
		goto err;
	}

	rv = find_key(hSession, BDK_label, &hObject);
	if (rv != CKR_OK) {
		fprintf(FLOG, "%s Error No se encuentra BDK 0x%08x\n", time_str, rv);
		rv = NOKEY_ERROR;
		goto err;
	}
	sprintf(out_hBDK, "%lu", hObject);
	fprintf(FLOG, "%s Handle BDK: '%lu'\n", time_str, hObject);

	rv = find_key(hSession, PANEK_label, &hObject);
	if (rv != CKR_OK) {
		fprintf(FLOG, "%s Error No se encuentra PANEK 0x%08x\n", time_str, rv);
		rv = NOKEY_ERROR;
		goto err;
	}
	sprintf(out_hPANEK, "%lu", hObject);
	fprintf(FLOG, "%s Handle PANEK: '%lu'\n", time_str, hObject);

	fprintf(FLOG, "%s Handle Session: '%lu'\n", time_str, hSession);
	sprintf(out_hSession, "%lu", hSession);


err:
	fprintf(FLOG, "%s ret: %d\n", time_str, rv);
	fprintf(FLOG, "%s <<<INICIAR_RECURSOS()<<<\n", time_str);
	close_log();
	if (pslots) {
		free(pslots);
	}

	return rv;
}


DLLDIR int STACK_CALL FINALIZAR_RECURSOS(char *in_hSession)
{
	//variables locales
	CK_RV rv = CKR_OK;
	CK_SESSION_HANDLE hSession;

	time_t t = time(NULL);
	struct tm * p = localtime(&t);
	char time_str[1024];
	strftime(time_str, sizeof(time_str), "[%Y-%m-%d %H:%M:%S] - ", p);

	open_log();

	fprintf(FLOG, "%s >>>FINALIZAR_RECURSOS()>>>\n", time_str);
	hSession = (CK_SESSION_HANDLE)atol(in_hSession);

	//rv = __DLL_C_Logout(hSession);
	//if(rv != CKR_OK) {
	//	fprintf(stderr, "Error __DLL_C_Logout 0x%08x\n", rv);
	//	rv = LOGOUT_ERROR;
	//	goto err;
	//}

	rv = C_CloseSession(hSession);
	if(rv != CKR_OK) {
		fprintf(FLOG, "%s Error C_CloseSession 0x%08x\n", time_str, rv);
		rv = SESION_CLS_ERROR;
		goto err;
	}

	fprintf(FLOG, "%s Close session: '%lu'\n", time_str, hSession);

err:
	fprintf(FLOG, "%s <<<FINALIZAR_RECURSOS()<<<\n", time_str);
	close_log();
	return rv;
}

DLLDIR int STACK_CALL VALIDAR_DUKPT(char *in_hSession, char *in_hBDK, char *in_hPANEK, char *PAN, char *PB1, char *KSN1, char *PB2, char *KSN2)
{
	//variables locales
	CK_RV rv = CKR_OK;
	CK_SESSION_HANDLE hSession;
	CK_OBJECT_HANDLE hBDK;
	CK_OBJECT_HANDLE hPANEK;
	char IPEK1[16], IPEK2[16];
	char SK1[16], SK2[16];
	char hex_ksn1[10], hex_ksn2[10];
	char PANC[64];
	char PIN[64];
	char PIN1[64];
	char PIN2[64];

	time_t t = time(NULL);
	struct tm * p = localtime(&t);
	char time_str[1024];
	strftime(time_str, sizeof(time_str), "[%Y-%m-%d %H:%M:%S] - ", p);

	open_log();

	fprintf(FLOG, "%s >>>VALIDAR_DUKPT()>>>\n", time_str);
	hSession = (CK_SESSION_HANDLE)atol(in_hSession);
	hBDK = (CK_SESSION_HANDLE)atol(in_hBDK);
	hPANEK = (CK_SESSION_HANDLE)atol(in_hPANEK);

	//Inicializacion
	memset(IPEK1, 0x00, sizeof(IPEK1));
	memset(IPEK2, 0x00, sizeof(IPEK2));
	memset(SK1, 0x00, sizeof(SK1));
	memset(SK2, 0x00, sizeof(SK2));
	memset(hex_ksn1, 0x00, sizeof(hex_ksn1));
	memset(hex_ksn2, 0x00, sizeof(hex_ksn2));
	memset(PANC, 0x00, sizeof(PANC));
	memset(PIN, 0x00, sizeof(PIN));
	memset(PIN1, 0x00, sizeof(PIN1));
	memset(PIN2, 0x00, sizeof(PIN2));
	
	str2hex(hex_ksn1, KSN1, 20);
	str2hex(hex_ksn2, KSN2, 20);

	//print_componet(FLOG, (CK_BYTE*)hex_ksn1, 10);
	//print_componet(FLOG, (CK_BYTE*)hex_ksn1, 10);

	rv = create_IPEK(hSession, hBDK, KSN1, IPEK1);
	//fprintf(FLOG, "%s create_IPEK1()=%d\n", time_str, rv);
	//print_componet(FLOG, (CK_BYTE*)IPEK1, 16);
	rv = create_SK(hSession, IPEK1, KSN1, SK1);
	//fprintf(FLOG, "%s create_SK1()=%d\n", time_str, rv);
	//print_componet(FLOG, (CK_BYTE*)SK1, 16);
	rv = get_clear_pan(hSession, hPANEK, PAN, PANC);
	fprintf(FLOG, "PAN: %s\n", PANC);
	//fprintf(FLOG, "%s f1()=%d\n", time_str, rv);
	rv = get_clear_pin(hSession, PB1, SK1, PANC, PIN);
	if (rv != 0) {
		fprintf(FLOG, "get_clear_pin(): %d\n", rv);
		goto err;
	}
	//fprintf(FLOG, "PIN: %s\n", PIN);
	//fprintf(FLOG, "%s f2()=%d\n", time_str, rv);
	strcpy(PIN1, PIN);
	rv = do_validations(PIN);
	if (rv != 0) {
		fprintf(FLOG, "Validacion fallo: %d\n", rv);
		goto err;
	}

	
	rv = create_IPEK(hSession, hBDK, KSN2, IPEK2);
	//fprintf(FLOG, "%s create_IPEK2()=%d\n", time_str, rv);
	//print_componet(FLOG, (CK_BYTE*)IPEK2, 16);
	rv = create_SK(hSession, IPEK2, KSN2, SK2);
	//fprintf(FLOG, "%s create_SK2()=%d\n", time_str, rv);
	//print_componet(FLOG, (CK_BYTE*)SK2, 16);
	rv = get_clear_pan(hSession, hPANEK, PAN, PANC);
	fprintf(FLOG, "PAN: %s\n", PANC);
	//fprintf(FLOG, "%s f1()=%d\n", time_str, rv);
	rv = get_clear_pin(hSession, PB2, SK2, PANC, PIN);
	if (rv != 0) {
		fprintf(FLOG, "get_clear_pin(): %d\n", rv);
		goto err;
	}
	//fprintf(FLOG, "PIN: %s\n", PIN);
	//fprintf(FLOG, "%s f2()=%d\n", time_str, rv);
	strcpy(PIN2, PIN);
	rv = do_validations(PIN);
	if (rv != 0) {
		fprintf(FLOG, "Validacion fallo: %d\n", rv);
		goto err;
	}

	if(strcmp(PIN1, PIN2) != 0) {
		fprintf(FLOG, "PINs distintos\n");
		rv = PINS_NO_EQUALS;
		goto err;
	}

err:
	fprintf(FLOG, "%s <<<VALIDAR_DUKPT()<<<\n", time_str);
	close_log();
	return rv;
}
