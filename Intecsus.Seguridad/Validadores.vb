﻿Imports System.Text.RegularExpressions

''' <summary>
''' Clase encargada de validar los tipos de datos 
''' </summary>
''' <remarks>AERM 23/04/2015</remarks>
Public Class Validadores
    Public Function ValidadorExpresionesRegulares(cadena As String, expresion As String) As Boolean
        Try
            If String.IsNullOrEmpty(expresion) = False Then
                Dim m_regex As Regex = New Regex(expresion)
                If m_regex.IsMatch(cadena) Then
                    Return True
                End If
            Else
                Return True
            End If
        Catch ex As Exception
            Throw ex
        End Try
        Return False
    End Function



    Public Function ValidarEntero(expresionValidar As String, defecto As Integer)
        Dim m_Codigo As Integer = 1
        If (Integer.TryParse(expresionValidar, m_Codigo)) Then
            Return m_Codigo
        Else
            Return defecto
        End If

    End Function
End Class
