﻿Imports System.Text

''' <summary>
''' Clase encargada de encriptar y/o desincriptar valores
''' </summary>
''' <remarks>AERM 06/04/2015</remarks>
Public Class Cifrar

    ''' <summary>
    ''' Metodo encargado de dejar el password en base24
    ''' </summary>
    ''' <param name="cadenaEncriptar">Texto aplicar la encripcion</param>
    ''' <returns>Retorna string con el base 24</returns>
    ''' <remarks>AERM 06/04/2015</remarks>
    Public Function Encriptar(ByVal cadenaEncriptar As String) As String
        Dim m_resultado = String.Empty
        Try
            Dim m_encryted As Byte() = System.Text.Encoding.Unicode.GetBytes(cadenaEncriptar)
            m_resultado = Convert.ToBase64String(m_encryted)
        Catch ex As Exception
            Throw ex
        End Try

        Return m_resultado
    End Function

    ''' <summary>
    ''' Metodo encargado de desencriptar la cadena base 24
    ''' </summary>
    ''' <param name="m_cadenaDesEncriptar">Texto a desencriptar</param>
    ''' <returns>Retorna cadena des-encriptada</returns>
    ''' <remarks>AERM 06/04/2015</remarks>
    Public Function DesEncriptar(ByVal m_cadenaDesEncriptar As String) As String
        Dim m_resultado = String.Empty
        Try
            Dim m_descryted As Byte() = Convert.FromBase64String(m_cadenaDesEncriptar)
            m_resultado = System.Text.Encoding.Unicode.GetString(m_descryted)
        Catch ex As Exception
            Throw ex
        End Try

        Return m_resultado
    End Function

    Public Function EnMascarar(m_Dato As String, inicio As Integer, fin As Integer) As String
        Try
            Dim m_Mascara As StringBuilder = New StringBuilder()
            If (m_Dato <> "" AndAlso m_Dato.Length > fin) Then
                For tamano As Integer = 0 To m_Dato.Length - fin
                    If tamano > inicio Then
                        m_Mascara.Append("*")
                    End If
                Next
                m_Mascara.Append(m_Dato.Substring(m_Dato.Length - fin, fin))
                Return m_Dato.Substring(0, inicio) + m_Mascara.ToString()
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return String.Empty
    End Function

    Public Function EnMascarar(m_Dato As String) As String
        Try
            Dim m_Mascara As StringBuilder = New StringBuilder()
            If (m_Dato <> "" AndAlso m_Dato.Length > 4) Then
                For tamano As Integer = 0 To m_Dato.Length - 4
                    m_Mascara.Append("*")
                Next
                m_Mascara.Append(m_Dato.Substring(m_Dato.Length - 4, 4))
                Return m_Mascara.ToString()
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return m_Dato
    End Function

    ''' <summary>
    ''' Metodo encargado de convertir de hexadecimal a string
    ''' </summary>
    ''' <param name="hex">Cadena en hexa a convertir</param>
    ''' <returns>Retorna cadena convertida</returns>
    ''' <remarks>AERM 15/09/2015</remarks>
    Public Function FromHex(hex As String) As Byte()
        hex = hex.Replace("-", "")
        Dim raw As Byte() = New Byte(hex.Length / 2 - 1) {}
        For i As Integer = 0 To raw.Length - 1
            raw(i) = Convert.ToByte(hex.Substring(i * 2, 2), 16)
        Next
        Return raw
    End Function

End Class

