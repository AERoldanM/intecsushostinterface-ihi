﻿Imports System.Security.Cryptography
Imports System.Text

''' <summary>
''' Clase q implementa codificacion hsh
''' </summary>
''' <remarks>AERM 09/04/2015</remarks>
Public Class HashEncriptar
    Private TripleDes As New TripleDESCryptoServiceProvider

    ''' <summary>
    ''' Metodo encargado de incializar el proveedor de servicios criptográficos 3DES
    ''' </summary>
    ''' <param name="key">controla los métodos EncryptData y DecryptData</param>
    ''' <remarks>AERM 09/04/2015</remarks>
    Sub New(ByVal key As String)
        ' Initialize el provider cryptografico.
        TripleDes.Key = TruncateHash(key, TripleDes.KeySize \ 8)
        TripleDes.IV = TruncateHash("", TripleDes.BlockSize \ 8)
    End Sub


    ''' <summary>
    ''' Metodo encargado de cifrar una cadena
    ''' </summary>
    ''' <param name="dato">Cadena a cifrar</param>
    ''' <returns>Retorna texto cifrado</returns>
    ''' <remarks>AERM 09/04/2015</remarks>
    Public Function EncryptData(ByVal dato As String) As String

        ' Convierte el dato en un array de bytes.
        Dim m_DatoBytes() As Byte = System.Text.Encoding.Unicode.GetBytes(dato)

        Dim m_MS As New System.IO.MemoryStream

        Dim m_encStream As New CryptoStream(m_MS, TripleDes.CreateEncryptor(), System.Security.Cryptography.CryptoStreamMode.Write)

        m_encStream.Write(m_DatoBytes, 0, m_DatoBytes.Length)
        m_encStream.FlushFinalBlock()

        ' Convierte lo encryitado en un string de base 24 
        Return Convert.ToBase64String(m_MS.ToArray)
    End Function

    ''' <summary>
    ''' Metodo encargado de descifrar una cadena
    ''' </summary>
    ''' <param name="dato">dato crifrado</param>
    ''' <returns>retorna el dato descifrado</returns>
    ''' <remarks>AERm 09/04/2015</remarks>
    Public Function Descifrar(ByVal dato As String) As String

        Dim m_DatoBytes() As Byte = Convert.FromBase64String(dato)

        Dim m_MS As New System.IO.MemoryStream
        Dim m_dencStream As New CryptoStream(m_MS, TripleDes.CreateDecryptor(), System.Security.Cryptography.CryptoStreamMode.Write)

        m_dencStream.Write(m_DatoBytes, 0, m_DatoBytes.Length)
        m_dencStream.FlushFinalBlock()

        Return System.Text.Encoding.Unicode.GetString(m_MS.ToArray)
    End Function

    ''' <summary>
    ''' Metodo encargado de  cifrar una cadena en SHA512
    ''' </summary>
    ''' <param name="cadena">Cadena de texto a cifrar</param>
    ''' <returns>Retorna Hexadecimal del SHA512</returns>
    ''' <remarks>AERM 22/09/2015</remarks>
    Public Function SHA512(cadena As String) As String
        Dim m_Sha As StringBuilder = New StringBuilder()
        Using m_ShaM As SHA512 = New SHA512Managed()
            Dim m_Result As Byte() = m_ShaM.ComputeHash(Encoding.UTF8.GetBytes(cadena))
            m_Sha = New StringBuilder(m_Result.Length * 2)
            For Each m_Byte As Byte In m_Result
                m_Sha.AppendFormat("{0:x2}", m_Byte)
            Next
        End Using

        Return m_Sha.ToString()
    End Function

    ''' <summary>
    ''' método privado que crea una matriz de bytes de una longitud especificada a partir del hash de la clave especificada
    ''' </summary>
    ''' <param name="llave">llaave del hash</param>
    ''' <param name="tamano">longitud de la clave</param>
    ''' <returns>Retona en byets el nuevo hash</returns>
    ''' <remarks>AERM 09/04/2015</remarks>
    Private Function TruncateHash(ByVal llave As String, ByVal tamano As Integer) As Byte()
        Dim sha1 As New SHA1CryptoServiceProvider
        ' La llave hash.
        Dim keyBytes() As Byte =
            System.Text.Encoding.Unicode.GetBytes(llave)
        Dim hash() As Byte = sha1.ComputeHash(keyBytes)

        ' Trunca hash.
        ReDim Preserve hash(tamano - 1)
        Return hash
    End Function
End Class
