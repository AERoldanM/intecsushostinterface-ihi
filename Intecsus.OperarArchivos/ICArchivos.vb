﻿Imports System.IO

''' <summary>
''' Interface con los metodos de manejo de archivos
''' </summary>
''' <remarks>AERM 20/01/2015</remarks>
Public Interface ICArchivos

    ''' <summary>
    ''' Metodo encargado de retornar informacion de un archivo
    ''' </summary>
    ''' <param name="rutaArchivo">Ruta  del archivo</param>
    ''' <param name="nombreArchivo">NOmbre del archivo</param>
    ''' <returns>Retorna archivo con todo la informacion</returns>
    ''' <remarks>AERM 20/01/2015</remarks>
    Function RetornarArchivo(rutaArchivo As String, nombreArchivo As String) As FileInfo

    ''' <summary>
    ''' Metodo encargado de devolver una lista de string con la informacion del archivo
    ''' </summary>
    ''' <param name="rutaArchivo">Ruta y nombre del archivo a tomar</param>
    ''' <param name="nombreArchivo">NOmbre del archivo</param>
    ''' <returns>Lista de strings con la informacion del archivo</returns>
    ''' <remarks>AERM 20/01/2015</remarks>
    Function ObtenerInformacioArchivo(rutaArchivo As String, nombreArchivo As String) As List(Of String)

    ''' <summary>
    ''' Metodo encargado de copiar un archivo en otra direccion
    ''' </summary>
    ''' <param name="rutaArchivoOrigen">Ruta con nombre del archivo desde cual se realiza la copia</param>
    ''' <param name="direccionArchivoDestino">Direccion archivo donde debe quedar la copia</param>
    ''' <param name="nombreArchivoDestino">Nombre del archivo dque debe tomar la copia</param>
    ''' <returns>Retorna si el arhivo fue copiado</returns>
    ''' <remarks>AERM 20/01/2015</remarks>
    Function CopiarArchivo(rutaArchivoOrigen As String, nombreArchivoOrigen As String, direccionArchivoDestino As String, nombreArchivoDestino As String) As Boolean

    ''' <summary>
    ''' Metodo encargado de Eliminar un archivo
    ''' </summary>
    ''' <param name="rutaArchivo">Ruta con nombre del archivo el cual s eElimina</param>
    ''' <returns>Retorna true si se elimino correctamente</returns>
    Function EliminarArchivo(rutaArchivo As String) As Boolean

    ''' <summary>
    ''' Metodo engargado de agregar liena a una rchivo
    ''' </summary>
    ''' <param name="rutaArhivo">Ruta del archivo con el nombre y extencion</param>
    ''' <param name="mensaje">Mensaje agregar</param>
    ''' <param name="nombreArchivo"> Nombre del archivo</param>
    ''' <remarks>AERM 09/04/2015</remarks>
    Sub AgregarLineaArchivo(rutaArhivo As String, nombreArchivo As String, mensaje As String)

End Interface
