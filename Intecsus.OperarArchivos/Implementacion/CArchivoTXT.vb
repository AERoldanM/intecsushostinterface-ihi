﻿Imports System.IO

Public Class CArchivoTXT
    Implements ICArchivos

#Region "Implementacioninterfaz"

    Public Function CopiarArchivo(rutaArchivoOrigen As String, nombreArchivoOrigen As String, direccionArchivoDestino As String, nombreArchivoDestino As String) As Boolean Implements ICArchivos.CopiarArchivo
        Try
            If Me.ValidacionesBase(rutaArchivoOrigen, nombreArchivoOrigen) Then
                ' Verifica que la ruta indicada exista si no crea directorio
                Me.ValidarRutaArchivo(direccionArchivoDestino)

                System.IO.File.Copy(rutaArchivoOrigen + "\" + nombreArchivoOrigen, direccionArchivoDestino + "\" + nombreArchivoDestino, True)

                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Public Function EliminarArchivo(rutaArchivo As String) As Boolean Implements ICArchivos.EliminarArchivo
        Try
            If System.IO.File.Exists(rutaArchivo) Then

                System.IO.File.Delete(rutaArchivo)
                Return True
            Else
                Return False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ObtenerInformacioArchivo(rutaArchivo As String, nombreArchivo As String) As List(Of String) Implements ICArchivos.ObtenerInformacioArchivo
        Try
            Dim lineas As New List(Of String)()
            If Me.ValidacionesBase(rutaArchivo, nombreArchivo) Then
                Dim line As String = String.Empty
                'Obtenemos el archivo y lo recorremos
                Dim file As New StreamReader(rutaArchivo + "\" + nombreArchivo)
                Do
                    line = file.ReadLine()
                    If Not String.IsNullOrEmpty(line) Then
                        If Not String.IsNullOrEmpty(line.Trim) Then
                            lineas.Add(line)
                        End If
                    End If
                Loop Until line Is Nothing

                file.Close()
                
                Return lineas
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function RetornarArchivo(rutaArchivo As String, nombreArchivo As String) As IO.FileInfo Implements ICArchivos.RetornarArchivo
        Try
            If Me.ValidacionesBase(rutaArchivo, nombreArchivo) Then
                Dim info As New FileInfo(rutaArchivo + "\" + nombreArchivo)
                Return info
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub AgregarLineaArchivo(rutaArhivo As String, nombreArchivo As String, mensaje As String) Implements ICArchivos.AgregarLineaArchivo
        Try
            Me.ValidarRutaArchivo(rutaArhivo)
            Me.CrearArchivo(rutaArhivo, nombreArchivo)
            ''Abrimos el archivo
            Dim m_Archivo As StreamWriter
            m_Archivo = File.AppendText(rutaArhivo + "\" + nombreArchivo) 'Abrir para añadir
            m_Archivo.WriteLine(mensaje) 'Añadir
            m_Archivo.Flush() 'Acabar de escribir
            m_Archivo.Close() 'Cerra

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Metodos Privados"

    ''' <summary>
    ''' Metodo encargado de validar que la ruta exista y que el archivo exista
    ''' </summary>
    ''' <param name="rutaArchivo">Ruta donde se encuentra el archivo</param>
    ''' <param name="nombreArchivo">Nombre del archivo</param>
    ''' <returns>Retorna true si cumple con los requerimientos</returns>
    ''' <remarks>AERM  21/01/2015</remarks>
    Private Function ValidacionesBase(rutaArchivo As String, nombreArchivo As String) As Boolean
        Try
            ' Verifica si la ruta existe
            If System.IO.Directory.Exists(rutaArchivo) Then
                'Verifica si el archivo existe
                If File.Exists(Convert.ToString(rutaArchivo & Convert.ToString("\")) & nombreArchivo) Then
                    Return True
                Else
                    Throw New Exception(Convert.ToString((Convert.ToString("El archivo ") & nombreArchivo) + " No existe en esa ruta : ") & rutaArchivo)
                End If
            Else
                Throw New Exception(Convert.ToString("La ruta del archivo no existe: ") & rutaArchivo)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub ValidarRutaArchivo(rutaArchivo As String)
        If Not System.IO.Directory.Exists(rutaArchivo) Then
            System.IO.Directory.CreateDirectory(rutaArchivo)
        End If
    End Sub

    Private Sub CrearArchivo(rutaArchivo As String, nombreArchivo As String)
        If File.Exists(Convert.ToString(rutaArchivo & Convert.ToString("\")) & nombreArchivo) = False Then
            Using (System.IO.File.Create(rutaArchivo + "\" + nombreArchivo))
            End Using
        End If
    End Sub
#End Region
End Class
