﻿Imports System.IO
''' <summary>
''' Clase que gestiona los tipos de archivos
''' </summary>
''' <remarks>AERM 20/01/2015</remarks>
Public Class RetornarArchivo
    ''' <summary>
    ''' Metodo que devuelve el archivo a enviar
    ''' </summary>
    ''' <param name="extencion">Extencion del archivo</param>
    ''' <returns>Retorna objeto con operaciones sobre ese tipo de archivo</returns>
    ''' <remarks>AERM 20/01/2015</remarks>
    Public Function IntanciarArchivo(extencion As String) As ICArchivos
        Try
            Select Case extencion.ToUpper()
                Case "TXT"
                    Return New CArchivoTXT()
                Case Else
                    Throw New Exception("No está disponible para ese archivo. (Gestorarchivo - utilidades)")
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    ''' <summary>
    ''' Metodo encargado de retornar informacion de los archivos
    ''' </summary>
    ''' <param name="rutaArchivo">Ruta  del archivo</param>
    ''' <returns>Retorna archivos con todo la informacion</returns>
    ''' <remarks>AERM 27/04/2015</remarks>
    Public Function RetornarArchivos(rutaArchivo As String) As List(Of FileInfo)
        Dim m_archivos As List(Of FileInfo) = New List(Of FileInfo)()
        Try
            ' Verifica si la ruta existe
            If System.IO.Directory.Exists(rutaArchivo) Then
                If System.IO.Directory.GetFiles(rutaArchivo).Length > 0 Then
                    For Each m_Archivo In System.IO.Directory.GetFiles(rutaArchivo)
                        Dim m_archivoF As FileInfo = New FileInfo(m_Archivo)
                        m_archivos.Add(m_archivoF)
                    Next
                End If
            Else
                Throw New Exception("NO existe la ruta enviada " + rutaArchivo)
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return m_archivos
    End Function
End Class
