﻿using Intecsus.DatosPIN;
using Intecsus.Entidades;
using Intecsus.OperarXML;
using Intecsus.Utilidades;
using Intecusus.OperarBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intecsus.OperarWS.Implementaciones
{

    /// <summary>
    /// Clase q implementa los metodos expuestos para el WS ServicioSantander
    /// </summary>
    public class ImplementacionSantander
    {
        #region "Variables"

        public OperacionesBase operacionesBD = new OperacionesBase();
        CadenasTexto m_CadenasTexto = new CadenasTexto();

        public string m_CODENT = ConfigurationManager.AppSettings["CODENT"];
        public string m_Canal = ConfigurationManager.AppSettings["CANAL"];
        public string m_CLAVEOP = ConfigurationManager.AppSettings["CLAVEOP"];
        public string m_CENTALTA = ConfigurationManager.AppSettings["CENTALTA"];
        #endregion

        #region "Operaciones WS"

        /// <summary>
        /// Metodo encargado de enviar la primera peticion del cliente (realce)
        /// </summary>
        /// <param name="CODENT">Codigo de la entidad</param>
        /// <param name="CANAL">Canal</param>
        /// <param name="CLAVEOP">Identificador de la transaccion CLiente</param>
        /// <param name="CENTALTA">Centro de alta</param>
        /// <param name="ESTATUS">Estado OK/NOK</param>
        /// <param name="CODERROR">Codigo de la transaccion (</param>
        /// <param name="DESERROR1">Descripcion del error 1</param>
        /// <param name="DESERROR2">Descripcion del error 2</param>
        /// <returns>AERm 28/06/2016</returns>
        public bool RespuestaRealce(string CODENT, string CANAL, string CLAVEOP, string CENTALTA, string ESTATUS, string CODERROR, string DESERROR1, string DESERROR2, int idTransaccion)
        {
            try
            {
                CODENT = String.IsNullOrEmpty(CODENT) == true ? m_CODENT : CODENT;
                CANAL = String.IsNullOrEmpty(CANAL) == true ? m_Canal : CANAL;
                CLAVEOP = String.IsNullOrEmpty(CLAVEOP) == true ? m_CLAVEOP : CLAVEOP;
                CENTALTA = String.IsNullOrEmpty(CENTALTA) == true ? m_CENTALTA : CENTALTA;
                var servicio = new ServicioSantader.JWSPREmbozadoraClient();
                int m_tamanoMensaje = DESERROR1.Length;
                string m_Descripcion = DESERROR1;
                string m_Descripcion2 = string.Empty;
                if (m_tamanoMensaje > 20)
                {
                    m_Descripcion = DESERROR1.Substring(0, 20);
                    m_Descripcion2 = DESERROR1.Substring(19, m_tamanoMensaje - 20);
                }

                CODERROR = CODERROR == "0" ? string.Empty : CODERROR;
                CODERROR = m_CadenasTexto.RellenarCadenaDerecha(CODERROR, 5, " ");
                ESTATUS = m_CadenasTexto.RellenarCadenaDerecha(ESTATUS, 3, " ");
                operacionesBD.GuardarLogXNombreConectionString(14, idTransaccion, 1, "ImplementacionSantander- RespuestaRealce Envia " + CODENT + " - " + CANAL + " - " + CLAVEOP + " - " + CENTALTA + " - " + ESTATUS + " - " + CODERROR + " - " +
                                                        DESERROR1 + " - " + DESERROR2, ConfigurationManager.AppSettings["BD"]);
                ServicioSantader.respuestaDTO m_respuesta = servicio.jwsPRPrimeraRespuesta(CODENT, CANAL, CLAVEOP, CENTALTA, ESTATUS, CODERROR, m_Descripcion, m_Descripcion2);
                this.GuardarRespuesta(m_respuesta, idTransaccion);
                if (m_respuesta.codigo == "MPA0166")
                    return true;
               
            }
            catch (Exception ex)
            {
                throw new Exception(" ImplementacionSantander- RespuestaRealce ERROR " + ex.Message);
            }

            return false;
        }

       

        /// <summary>
        /// Metodo encargado de notificar al cliente el estado del estado final de la impresion de tarjeta
        /// </summary>
        /// <param name="CODENT">Entidad Bancaria</param>
        /// <param name="CANAL">Canal segun especificacion 390</param>
        /// <param name="CLAVEOP">Clave operacion del cliente</param>
        /// <param name="CENTALTA">centro de alta </param>
        /// <param name="idTransaccion">Identificacion de la transaccion</param>
        /// <returns>Retorna si la peticion fue enviada correctamente</returns>
        public bool EnviarNotificacion(string CODENT, string CANAL, string CLAVEOP, string CENTALTA, int idTransaccion)
        {
            try
            {
                var servicio = new ServicioSantader.JWSPREmbozadoraClient();
                operacionesBD.GuardarLogXNombreConectionString(14, idTransaccion, 1, "ImplementacionSantander- EnviarNotificacion Enviado "  +CODENT + " " + CANAL + 
                    " " + CLAVEOP + " " + CENTALTA , ConfigurationManager.AppSettings["BD"]);
                ServicioSantader.respuestaDTO m_respuesta = servicio.jwsPRActivacionTarjeta(CODENT, CANAL, CLAVEOP, CENTALTA);
                this.GuardarRespuesta(m_respuesta, idTransaccion);
                return true;
            }
            catch (Exception ex)
            {
                operacionesBD.GuardarLogXNombreConectionString(14, idTransaccion, 2, "ImplementacionSantander- EnviarNotificacion ERROR " + ex.Message, 
                    ConfigurationManager.AppSettings["BD"]);
               // return this.ValidarExcepcion(ex.Message);
            }

            return false;
        }

        /// <summary>
        /// Metodo que consume el WS cliente para asignar un pin
        /// </summary>
        /// <param name="CODENT">Entidad Bancaria</param>
        /// <param name="CANAL">Canal segun especificacion 390</param>
        /// <param name="CLAVEOP">Clave operacion Cliente</param>
        /// <param name="CLAVEOPA">Clave Operacion CW (id nuestro)</param>
        /// <param name="USUARIO">Numero de usuario</param>
        /// <param name="DESNOMUSR">Nombre usuario</param>
        /// <param name="EPINBLOCK">PINBLOCK con encripcion</param>
        /// <param name="KSN">Serial de la transaccion (consecutivo) del pin</param>
        /// <param name="idTransaccion">Identificador de la transaccion</param>
        /// <returns>Retorna si la peticion fue enviada correctamente</returns>
        public Resultado AsignarPIN(string CODENT, string CANAL, string CLAVEOP, string CLAVEOPA, string USUARIO, string DESNOMUSR, string EPINBLOCK, string KSN, int idTransaccion)
        {
            IDALOperacionesPin m_Operaciones = new DALOperacionesPin();
            Resultado m_Resultado = new Resultado();
            try
            {
                
                var servicio = new ServicioSantader.JWSPREmbozadoraClient();
                CLAVEOP = m_CadenasTexto.RellenarCadenaDerecha(CLAVEOP, 46, " ");
                CLAVEOPA = m_CadenasTexto.RellenarCadenaDerecha(CLAVEOPA, 42, " ");
                m_Operaciones.GuardarLOG ( 1, "ImplementacionSantander- AsignarPIN Enviado " + CODENT + " " + CANAL +
                    " " + CLAVEOP + " " + CLAVEOPA + " " + USUARIO + " " + DESNOMUSR + " " + EPINBLOCK + " " + KSN, ConfigurationManager.AppSettings["BDPIN"]);
                ServicioSantader.respuestaDTO m_respuesta  = servicio.jwsPRAsignacionPIN(CODENT, CANAL, CLAVEOP, CLAVEOPA, USUARIO, DESNOMUSR, EPINBLOCK, KSN);
                m_Resultado.CodRespuesta = m_respuesta.codigo;
                m_Resultado.Mensaje = m_respuesta.mensaje;
                this.GuardarRespuestaPIN(m_respuesta, idTransaccion);
            }
            catch (Exception ex)
            {
                throw new Exception(" ImplementacionSantander- AsignarPIN ERROR " + ex.Message);
               // return this.ValidarExcepcion(ex.Message);
            }

            return m_Resultado;
        }

      

        //private bool ValidarExcepcion(string mensaje)
        //{
        //    try
        //    {
        //        if (mensaje.Contains("MPA0166"))
        //        {
        //            return true;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        return false;
        //    }

        //    return false;
        //}

        /// <summary>
        /// Metodo encargado de guardar la respuesta del servicio santander
        /// </summary>
        /// <param name="respuesta">Objeto de respuesta</param>
        /// <param name="idTransaccion">Identificador de la transaccion</param>
        /// <remarks>AERM 13/07/2013</remarks>
        private void GuardarRespuesta(ServicioSantader.respuestaDTO respuesta, int idTransaccion)
        {
            try
            {
                GeneradorXML m_OperarXML = new GeneradorXML();
                string m_respuestaFinal = m_OperarXML.Serializar<ServicioSantader.respuestaDTO>(respuesta);
                operacionesBD.GuardarLogXNombreConectionString(14, idTransaccion, 1, "ImplementacionSantander-  RespuestaWSDL Santander " + m_respuestaFinal,
                   ConfigurationManager.AppSettings["BD"]);
            }
            catch (Exception ex)
            {
                operacionesBD.GuardarLogXNombreConectionString(14, idTransaccion, 2, "ImplementacionSantander- GuardarRespuesta WSDL " + ex.Message, ConfigurationManager.AppSettings["BD"]);
            }
        }

        /// <summary>
        /// Metodo encargado de guardar la respuesta del servicio santander en la BD PIN
        /// </summary>
        /// <param name="respuesta">Objeto de respuesta</param>
        /// <param name="idTransaccion">Identificador de la transaccion</param>
        /// <remarks>AERM 13/07/2013</remarks>
        private void GuardarRespuestaPIN(ServicioSantader.respuestaDTO m_respuesta, int idTransaccion)
        {
            IDALOperacionesPin m_Operaciones = new DALOperacionesPin();
            try
            {
                GeneradorXML m_OperarXML = new GeneradorXML();
                string m_respuestaFinal = m_OperarXML.Serializar<ServicioSantader.respuestaDTO>(m_respuesta);
                m_Operaciones.GuardarLOG(1, "ImplementacionSantander-  RespuestaWSDL Santander " + m_respuestaFinal,
                   ConfigurationManager.AppSettings["BDPIN"]);
            }
            catch (Exception ex)
            {
                m_Operaciones.GuardarLOG(2, "ImplementacionSantander- GuardarRespuesta WSDL " + ex.Message, ConfigurationManager.AppSettings["BDPIN"]);
            }
        }

        #endregion
    }
}
