﻿using Intecsus.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace FrontWSSE.TUYA
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IIntegracionTUYA" in both code and config file together.
    [ServiceContract]
    public interface IIntegracionTUYA
    {
        [OperationContract]
        RespuestaTuya Integrador(string numeroProducto, string numeroTarjeta, string tipoPersona, string tipoIdentificacion, string numeroIdentificacion, string primerApellido, string segundoApellido, string primerNombre, string segundoNombre, string nombreRealce,
        string nombreLargo, string fechaNacimiento, string sexo, string estadoCivil, string direccionResidencia1, string direccionResidencia2, string codigoDepartResidencia, string codigoCiudadResisdencia, string zonaPostal, string direccionCorresondencia1,
        string direccionCorresondencia2, string codigDepartDirCor, string codigoCiudadDirCor, string zonaPostalCorres, string oficinaRadicacion, string telResidencia, string telOficina, string afinidadTarjeta, string cupoAsignado, string fechaSolicitud,
        string tipoSolicitud, string manejoCuotas, string tipoTarjeta, string codigoVendedor, string estadoRegistroRecibido, string numeroSolicitud, string tarjetaAnterior, string codigoPuntoDistribucion, string codigoMercadeo, string tipoCuenta,
        string valorCuotaFija, string tipoIdAmparador, string numeroIDAmparador, string oficinaDistribucion, string indicativo, string correoElectronico, string actividadEconomica, string origenIngresos, string codigoBarras, string idenDebitoAut,
        string codBancoDebito, string numCuentaDebito, string tipoCuentaDebito, string indicadorCesacion, string fechaCesacion, string cicloFacturacion, string BIN, string NIT, string subtipo, string codigoCompensacion,
        string codigoNovedad, string codigoProceso, string filler1, string filler2, string filler3, string filler4, string printName, string idTransaccion, string tipoProducto);
    }
}
