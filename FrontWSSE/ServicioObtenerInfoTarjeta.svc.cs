﻿using Intecsus.Entidades;
using Intecsus.MSMQ;
using Intecusus.OperarBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace FrontWSSE
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ServicioObtenerInfoTarjeta" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ServicioObtenerInfoTarjeta.svc or ServicioObtenerInfoTarjeta.svc.cs at the Solution Explorer and start debugging.
    public class ServicioObtenerInfoTarjeta : IServicioObtenerInfoTarjeta
    {
        #region "variables"

        #endregion
        private ICola operarCola = new InstanciaCola().IntanciarCola(ConfigurationManager.AppSettings["ColaInstancia"]);


        public RespuestaInfo ObtenerInformacionChip(InformacionTarjeta datos)
        {
            RespuestaInfo m_Respuesta = new RespuestaInfo();
            OperacionesBase operacionesBD = new OperacionesBase();
            try
            {
                EstadoTransaccion m_notificar = operacionesBD.ObtenerNoticiXSha(datos.Identificador, datos.IdChipContacto, "COLSUBSIDIO", ConfigurationManager.AppSettings["BD"]);
                this.ValidarNotificacion(m_notificar, datos.Identificador);
                RespuestaDeviceStatus m_notificacion = new RespuestaDeviceStatus();
                m_notificacion.IdChip = datos.IdChipContacto;
                m_notificacion.hostIdentifier = m_notificar.IdTransaccion;
                m_notificacion.DataInteger = "000";
                m_notificacion.EstadoF = "0";
                m_notificacion.Mensaje = "Completado sin errores";
                m_notificacion.TransaccionCliente = m_notificar.IdTransaccionCl;
                m_notificacion.Cliente = m_notificar.Cliente;
                string colaNotificacion = operacionesBD.ObtenerValoresConfigurador("ColaNotificacion", ConfigurationManager.AppSettings["BD"]);
                operarCola.GuardarObjetoNotificacion(m_notificacion, colaNotificacion);
                operacionesBD.GuardarLogXNombreConectionString(22, Convert.ToInt32(m_notificar.IdTransaccion), 1, "ServicioObtenerInfoTarjeta- ObtenerInformacionChip : Se almacena notificacion" + datos.IdChipContacto, ConfigurationManager.AppSettings["BD"]);
                m_Respuesta.Codigo = "00";
                m_Respuesta.Mensaje = "Transacción reportada correctamente";
            }
            catch (Exception ex)
            {
                m_Respuesta.Codigo = "02";
                m_Respuesta.Mensaje = ex.Message;
                operacionesBD.GuardarLogXNombreConectionString(22, 0, 2, "ServicioObtenerInfoTarjeta- ObtenerInformacionChip : " + ex.Message, ConfigurationManager.AppSettings["BD"]);
            }

            return m_Respuesta;
        }

        private void ValidarNotificacion(EstadoTransaccion m_notificar, string identificador)
        {
            if ((m_notificar == null))
            {
                throw new Exception("No existe transacción reportada para " + identificador);
            }

            if ((m_notificar.Final))
            {
                throw new Exception("Notificacion ya fue previamente realizada " + identificador);
            }

        }
    }
}
