﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FrontWSSE.WSSEClases
{
    [DataContract]
    public class ServiceHeader
    {
        [DataMember]
        public string UsernameToken { get; set; }

        [DataMember]
        public string Username { get; set; }

        [DataMember]
        public string Password { get; set; }

        [DataMember]
        public string Nonce { get; set; }
    }
}