﻿using Intecsus.Entidades;
using Intecsus.MSMQ;
using Intecsus.OperarXML;
using Intecsus.Seguridad;
using Intecusus.OperarBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FrontWSSE.WSSEClases
{
    /// <summary>
    /// Clase que se encarga de las operaciones compartidas por los servicios
    /// </summary>
    public class OperacionesServicio
    {

        Validadores validar = new Validadores();
        OperacionesBase operacionesBD = new OperacionesBase();
        GeneradorXML obtenerObjeto = new GeneradorXML();

        /// <summary>
        /// Metood encargado de adjuntar una peticion a la cola
        /// </summary>
        /// <param name="peticion">Objeto de peticion que se agrega a la cola</param>
        /// <remarks>AERM 19/03/2015</remarks>
        public RespuestaTuya AdjuntarAColaT(Peticion peticion)
        {
            try
            {
                ICola m_OperarCOla = new InstanciaCola().IntanciarCola(ConfigurationManager.AppSettings["ColaInstancia"]);
                m_OperarCOla.GuardarObjetoPeticionCliente(peticion, ConfigurationManager.AppSettings["colaCliente"]);
                return this.EsperarRespuesta(peticion);
            }
            catch (Exception ex)
            {
                throw new Exception("OperacionesServicio- AdjuntarACola : " + ex.Message);
            }
        }

        /// <summary>
        /// Metood encargado de adjuntar una peticion a la cola
        /// </summary>
        /// <param name="peticion">Objeto de peticion que se agrega a la cola</param>
        /// <remarks>AERM 19/03/2015</remarks>
        public Respuesta AdjuntarACola(Peticion peticion)
        {
            try
            {
                ICola m_OperarCOla = new InstanciaCola().IntanciarCola(ConfigurationManager.AppSettings["ColaInstancia"]);
                m_OperarCOla.GuardarObjetoPeticionCliente(peticion, ConfigurationManager.AppSettings["colaCliente"]);
                return this.ConvertTORespuesta(this.EsperarRespuesta(peticion));
            }
            catch (Exception ex)
            {
                throw new Exception("OperacionesServicio- AdjuntarACola : " + ex.Message);
            }
        }

        public string ValidarDatoInteger(string dato, string nombre)
        {
            try
            {
                if ((string.IsNullOrEmpty(dato)))
                {
                    return string.Empty;
                }

                bool valdiacion = validar.ValidadorExpresionesRegulares(dato, "^[0-9]*$");
                if (valdiacion)
                {
                    return dato;
                }

                throw new Exception();
            }
            catch (Exception ex)
            {
                throw new Exception("Error a convertir el atributo " + nombre);
            }

            //return string.Empty;
        }

        public string validarDATO(string dato)
        {
            if ((string.IsNullOrEmpty(dato)))
            {
                return string.Empty;
            }

            return dato;
        }

        public RespuestaTuya EsperarRespuesta(Peticion peticion)
        {
            RespuestaTuya m_Respuesta = new RespuestaTuya();
            try
            {
                bool m_Peticion = true;
                Int32 m_TiempoMAX = Convert.ToInt32(operacionesBD.ObtenerValoresConfigurador("TmeOutServicioMiliSegundos", ConfigurationManager.AppSettings["BD"]));
                Int32 m_TimpoEspera = 0;
                Int32 m_TiempoRespuesta = Convert.ToInt32(operacionesBD.ObtenerValoresConfigurador("BuscarRespuestaMiliSegundos", ConfigurationManager.AppSettings["BD"]));
                while ((m_Peticion))
                {
                    Thread.Sleep(m_TiempoRespuesta);
                    m_TimpoEspera += m_TiempoRespuesta;
                    RespuestaWSASNET m_RespuestaWS = operacionesBD.ObtenerRespuesta(peticion.cliente, peticion.idTransaccion, ConfigurationManager.AppSettings["BD"]);
                    if (((m_RespuestaWS == null) == false))
                    {
                        operacionesBD.GuardarLogXNombreConectionString(19, 0, 1, "Respuesta -" + peticion.idTransaccion + "  " + m_RespuestaWS.Respuestas,
                                                               ConfigurationManager.AppSettings["BD"], peticion.cliente);
                        m_Respuesta = obtenerObjeto.Deserialize<RespuestaTuya>(m_RespuestaWS.Respuestas);
                        m_Peticion = false;
                    }

                    if ((m_TimpoEspera >= m_TiempoMAX & m_Peticion))
                    {
                        m_Respuesta.numeroAutorizacion = "ERROR";
                        string m_peticionR = obtenerObjeto.Serializar<Peticion>(peticion);
                        operacionesBD.GuardarRespuestaWS(peticion, m_peticionR, "Time Out -" + peticion.idTransaccion, m_Respuesta, ConfigurationManager.AppSettings["BD"]);
                        throw new Exception("Supero el tiempo de espera");
                    }
                }

            }
            catch (Exception ex)
            {
                m_Respuesta.codigo = "01";
                m_Respuesta.mensaje = ex.Message;
                operacionesBD.GuardarLogXNombreConectionString(19, 0, 2, "OperacionesServicio- EsperarRespuesta  : - " + peticion.idTransaccion + "  " + ex.Message, ConfigurationManager.AppSettings["BD"]);
            }

            return m_Respuesta;
        }


        /// <summary>
        /// Metodo encargado de cargar los datos de inicializacion de PCNAME, CardName y XML
        /// </summary>
        /// <param name="peticion">Peticion inicial enviada por el cliente</param>
        /// <returns>Vlaida si existe pcname o producto</returns>
        /// <remarks>AERM 29/07/2015</remarks>
        public bool ValidarCliente(Peticion peticion)
        {
            try
            {
                string m_StringObjeto = obtenerObjeto.Serializar<Peticion>(peticion);
                operacionesBD.GuardarLogXNombreConectionString(19, 0, 1, "Peticion -" + peticion.idTransaccion + "  " + m_StringObjeto, ConfigurationManager.AppSettings["BD"], peticion.cliente);
                if ((peticion.codigoNovedad != "03"))
                {
                    return true;
                }

                RespuestaWS m_respuestaWS = new RespuestaWS();
                List<string> m_List = operacionesBD.ObtenerCardFormatName(peticion.cliente, peticion.tipoProducto, ConfigurationManager.AppSettings["BD"]);
                if ((m_List.Count >= 2))
                {
                    m_respuestaWS.cardWizardName = m_List[0];
                    m_respuestaWS.xmlPeticion = m_List[1];
                }

                string m_BuscarImpresora = operacionesBD.ObtenerValoresConfigurador("BuscarImpresora" + peticion.cliente, ConfigurationManager.AppSettings["BD"]);
                //'Obtenemos el PCNAME deacuerdo a configuracion base de datos tabla Impresoras si lo tiene habilitado
                if ((Convert.ToBoolean(Convert.ToInt32(m_BuscarImpresora)) == true))
                {
                    m_respuestaWS.pCName = operacionesBD.ObtenerPCNAME(peticion.printName, ConfigurationManager.AppSettings["BD"]);
                }
                else
                {
                    m_respuestaWS.pCName = peticion.printName;
                }

                if ((string.IsNullOrEmpty(m_respuestaWS.cardWizardName) | string.IsNullOrEmpty(m_respuestaWS.pCName)))
                {
                    throw new Exception("No se encuentra PCName y/ó CardFormat para la transacción ");
                }

                return true;
            }
            catch (Exception ex)
            {
                operacionesBD.GuardarLogXNombreConectionString(19, 0, 2, "ServicioIntegracion: " + peticion.idTransaccion + "  " + ex.Message, ConfigurationManager.AppSettings["BD"]);
                throw new Exception("Error PCName y/ó CardFormat para la transacción ");
            }
        }

        /// <summary>
        /// Metodo encargado de retornar un error en la respuesta
        /// </summary>
        /// <param name="mensaje">Mensaje a enviar</param>
        /// <returns>Respuesta al cliente</returns>
        /// <remarks>AERM 11/09/2015</remarks>
        public Respuesta RespuestaErronea(string codigoError, string mensaje)
        {
            dynamic Respuesta = new Respuesta();
            Respuesta.codigo = codigoError;
            Respuesta.mensaje = "Solicitud Errada-  " + mensaje;
            Respuesta.numeroAutorizacion = string.Empty;
            return Respuesta;
        }

        public string ValidarTransaccion(Peticion peticion)
        {
            String m_StringObjeto = obtenerObjeto.Serializar<Peticion>(peticion);
            try
            {
                Int32 m_Secuencia = operacionesBD.ObtenerSecuencia(ConfigurationManager.AppSettings["BD"]);
                if(string.IsNullOrEmpty(peticion.idTransaccion))
                {
                    if (peticion.codigoNovedad != "03")
                        peticion.idTransaccion = string.Empty;
                    else
                        throw new Exception("No existe ID Transaccion");
                }
                peticion.idTransaccion = m_Secuencia.ToString() + "-" + peticion.idTransaccion;
                return peticion.idTransaccion;
            }
            catch (Exception ex)
            {
                operacionesBD.GuardarLogXNombreConectionString(19, 0, 2, "Peticion- " + peticion.idTransaccion + "  " + m_StringObjeto,
                                                           ConfigurationManager.AppSettings["BD"], peticion.cliente);
                operacionesBD.GuardarLogXNombreConectionString(19, 0, 2, "OperacionesServicio- IDTransacción : " + ex.Message, ConfigurationManager.AppSettings["BD"]);
                throw ex;
            }
        }

        public Respuesta ConvertTORespuesta(RespuestaTuya respuesta)
        {
            Respuesta m_resp = new Respuesta();
            m_resp.codigo = respuesta.codigo;
            m_resp.mensaje = respuesta.mensaje;
            m_resp.numeroAutorizacion = respuesta.numeroAutorizacion;

            return m_resp;
        }
    }
}
