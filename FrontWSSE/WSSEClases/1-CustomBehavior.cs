﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace FrontWSSE.WSSEClases
{
    /// <summary>
    /// Esta clase de comportamiento personalizado se utiliza para agregar inspectores en  ambos lados cliente y servidor 
    /// Los puntos finales WCF correspondientes.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class CustomBehavior : Attribute, IServiceBehavior, IEndpointBehavior
    {
        #region "IEndpointBehavior Miembros"

        //método para pasar los datos personalizados en tiempo de ejecución para permitir las consolidaciones para apoyar comportamiento personalizado.
        void IEndpointBehavior.AddBindingParameters(ServiceEndpoint endpoint,
        System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
        {
        }

        //método para modificar, examinar, o insertar extensiones a un punto final en una aplicación cliente.
        void IEndpointBehavior.ApplyClientBehavior(ServiceEndpoint endpoint,
                 System.ServiceModel.Dispatcher.ClientRuntime clientRuntime)
        {
            CustomMessageInspector inspector = new CustomMessageInspector();
            clientRuntime.MessageInspectors.Add(inspector);
        }

        //método para modificar, examinar, o insertar extensiones a la ejecución de todo el punto final en una aplicación de servicio.
        void IEndpointBehavior.ApplyDispatchBehavior(ServiceEndpoint endpoint,
                 System.ServiceModel.Dispatcher.EndpointDispatcher endpointDispatcher)
        {
            ChannelDispatcher channelDispatcher = endpointDispatcher.ChannelDispatcher;
            if (channelDispatcher != null)
            {
                foreach (EndpointDispatcher ed in channelDispatcher.Endpoints)
                {
                    CustomMessageInspector inspector = new CustomMessageInspector();
                    ed.DispatchRuntime.MessageInspectors.Add(inspector);
                }
            }
        }

        // Confirma que un ServceEnpoit cumple con los requerimientos especificos.
        //Esto puede ser usado para asegurar que un punto final tiene un valor de configuración cierta habilitado, soporta una característica particular y otros requisitos.
        void IEndpointBehavior.Validate(ServiceEndpoint endpoint) { }
        #endregion

        #region "IServiceBehavior Miembros"

        //método para pasar a un elemento de unión de la información personalizada para el servicio de manera que pueda soportar el servicio correctamente.
        void IServiceBehavior.AddBindingParameters(ServiceDescription serviceDescription,
         ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints,
         BindingParameterCollection bindingParameters)
        {
        }

        //método para cambiar los valores de propiedad en tiempo de ejecución o inserte objetos de extensión de encargo, 
        //como controladores de error, mensaje o parámetros interceptores, extensiones de seguridad, y otros objetos de extensión personalizado.
        void IServiceBehavior.ApplyDispatchBehavior(ServiceDescription desc, ServiceHostBase host)
        {
            foreach (ChannelDispatcher cDispatcher in host.ChannelDispatchers)
            {
                foreach (EndpointDispatcher eDispatcher in cDispatcher.Endpoints)
                {
                    eDispatcher.DispatchRuntime.MessageInspectors.Add(new CustomMessageInspector());
                }
            }
        }

        // método para examinar la descripción antes de construcciones al servicio de ejecución para confirmar que se puede ejecutar correctamente
        void IServiceBehavior.Validate(ServiceDescription desc, ServiceHostBase host) { }
        #endregion
    }
}

