﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace FrontWSSE.WSSEClases
{
    /// <summary>
    /// Clase CustomHeader se utiliza para crear encabezado personalizado para el servicio en el que queremos transmitir la información de cabecera 
    /// junto con el método de llamada. La clase CustomHeader contiene la información que queremos transmitir con el método de llamada. 
    /// Puede definir la estructura de acuerdo a sus necesidades.
    /// </summary>
    public class CustomHeader : MessageHeader
    {
        private const string CUSTOM_HEADER_NAME = "Security";
        private const string CUSTOM_HEADER_NAMESPACE = "wsse";

        /// <summary>
        /// Clase contiene los parametros del encabezado
        /// </summary>
        private ServiceHeader _customData;

        public ServiceHeader CustomData
        {
            get
            {
                return _customData;
            }
        }

        public CustomHeader()
        {
        }

        public CustomHeader(ServiceHeader customData)
        {
            _customData = customData;
        }

        public override string Name
        {
            get { return (CUSTOM_HEADER_NAME); }
        }

        public override string Namespace
        {
            get { return (CUSTOM_HEADER_NAMESPACE); }
        }

        /// <summary>
        /// OnWriteHeaderContents override, which is invoked by WCF infrastructure to serialize the SOAP Header
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="messageVersion"></param>
        protected override void OnWriteHeaderContents(
        System.Xml.XmlDictionaryWriter writer, MessageVersion messageVersion)
        {
            //XmlSerializer serializer = new XmlSerializer(typeof(ServiceHeader));
            //StringWriter textWriter = new StringWriter();
            //serializer.Serialize(textWriter, _customData);
            //textWriter.Close();

            //string text = textWriter.ToString();

            //writer.WriteElementString(CUSTOM_HEADER_NAME, "Key", text.Trim());
        }

        public static ServiceHeader ReadHeader(Message request)
        {
            try
            {
                Int32 headerPosition = 0;
                MessageHeaderInfo headerInfo = request.Headers[headerPosition];

                XmlNode[] content = request.Headers.GetHeader<XmlNode[]>(headerPosition);
                ServiceHeader customData = new ServiceHeader();
                if (content[0].ChildNodes.Count < 4)
                    throw new Exception("Error cabecera");

                customData.Username = content[0].ChildNodes[0].InnerText;
                customData.Password = content[0].ChildNodes[1].InnerText;
                customData.UsernameToken = content[0].ChildNodes[2].InnerText;
                customData.Nonce = content[0].ChildNodes[3].InnerText;
                return customData;
            }
            catch (Exception ex)
            {
                throw new FaultException(ex.Message);
            }
        }
    }
}