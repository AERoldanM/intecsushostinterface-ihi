﻿using FrontWSSE.ActiveDirectory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;

namespace FrontWSSE.WSSEClases
{
    /// <summary>
    /// Esta clase se utiliza para inspeccionar el mensaje y las cabeceras del lado del servidor,
    /// Esta clase también se utiliza para interceptar el mensaje en el 
    /// lado del cliente, antes / después de que se realice una petición al servidor.
    /// </summary>
    public class CustomMessageInspector : IClientMessageInspector, IDispatchMessageInspector
    {
        private ValidarDatos validarUsuario = new ValidarDatos();
        #region "Mensaje inspeccionar por el servicio"
        /// <summary>
        /// Este metodo es llamado en el servidor cuando un request es recibido desde el cliente
        /// </summary>
        /// <param name="request"></param>
        /// <param name="channel"></param>
        /// <param name="instanceContext"></param>
        /// <returns></returns>
        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            try
            {
                // Crear una copia del mensaje original, para interacuar con el.
                MessageBuffer buffer = request.CreateBufferedCopy(Int32.MaxValue);
                request = buffer.CreateMessage();
                Message messageCopy = buffer.CreateMessage();

                // Leer los datos de contexto personalizado de las cabeceras
                ServiceHeader customData = CustomHeader.ReadHeader(request);

                // Añadir una extensión para el contexto actual operación de modo
                // Que nuestro contexto personalizado puede llegar fácilmente a cualquier lugar.

                if (customData != null)
                {
                    if (validarUsuario.ValidarUsuario(customData) == false)
                        throw new FaultException("Error cabecera  -- Validando Datos");
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Este método se llama después de procesar a un método en el lado del servidor y justo
        /// Antes de enviar la respuesta al cliente.
        /// </summary>
        /// <param name="reply"></param>
        /// <param name="correlationState"></param>
        public void BeforeSendReply(ref Message reply, object correlationState)
        {
            // Limpiar el contexto
           // OperationContext.Current.Extensions.Remove(ServerContext.Current);
        }

        #endregion

        #region "Mensaje inspeccionar por el consumidor"

        /// <summary>
        /// Este método se llama desde el lado del cliente justo antes de se llama cualquier método.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="channel"></param>
        /// <returns></returns>
        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            return null;
        }

        /// <summary>
        /// Este método será llamado después de la finalización de una solicitud al servidor.
        /// </summary>
        /// <param name="reply"></param>
        /// <param name="correlationState"></param>
        public void AfterReceiveReply(ref Message reply, object correlationState)
        {

        }

        #endregion
    }
}
