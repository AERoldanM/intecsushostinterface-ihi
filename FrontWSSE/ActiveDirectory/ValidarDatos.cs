﻿using FrontWSSE.WSSEClases;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.DirectoryServices;
using System.DirectoryServices.ActiveDirectory;
using Intecusus.OperarBD;
using Intecsus.Entidades;
using Intecsus.Seguridad;

namespace FrontWSSE.ActiveDirectory
{
    public class ValidarDatos
    {
        public Boolean ValidarUsuario(ServiceHeader usuarioPeticion) 
        {
            try 
            {
                OperacionesBase operacionesBD = new OperacionesBase();
                String m_UserTUYA = operacionesBD.ObtenerValoresConfigurador("UserTUYA", ConfigurationManager.AppSettings["BD"]);
                String m_PassTUYA = operacionesBD.ObtenerValoresConfigurador("PassTUYA", ConfigurationManager.AppSettings["BD"]);
                HashEncriptar operar = new HashEncriptar("NOSENECESITA123");
                String m_Cifrado =  operar.SHA512(usuarioPeticion.Password);
                if (m_PassTUYA == m_Cifrado)
                {
                    return true;
                }
                else { return false; }

                //String path = ConfigurationManager.AppSettings["path"];
                //String domainAndUsername = ConfigurationManager.AppSettings["Dominio"] + @"\" + usuarioPeticion.Username;
                //DirectoryEntry entry = new DirectoryEntry(path, domainAndUsername, usuarioPeticion.Password);
                //Object obj = entry.NativeObject;

                //DirectorySearcher search = new DirectorySearcher(entry);

                //search.Filter = "(SAMAccountName=" + usuarioPeticion.Username + ")";
                //search.PropertiesToLoad.Add("cn");
                //SearchResult result = search.FindOne();

                //if (null == result)
                //{
                //    return false;
                //}

                //return true;
            }
            catch (Exception ex) 
            {
                throw new Exception (ex.Message + " Directorio Activo");
            }
        }
    }
}
