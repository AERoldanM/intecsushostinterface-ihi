﻿using FrontWSSE.WSSEClases;
using Intecsus.Entidades;
using Intecsus.MSMQ;
using Intecsus.OperarXML;
using Intecusus.OperarBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading;

namespace FrontWSSE
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ServicioIntegracion" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing operar service, please select ServicioIntegracion.svc or ServicioIntegracion.svc.cs at the Solution Explorer and start debugging.
    public class ServicioIntegracion : IServicioIntegracion
    {

        OperacionesServicio operar = new OperacionesServicio();

        public Respuesta Integrador(string numeroProducto, string numeroTarjeta, string tipoPersona, string tipoIdentificacion, string numeroIdentificacion, string primerApellido, string segundoApellido, string primerNombre, string segundoNombre, string nombreRealce,
        string nombreLargo, string fechaNacimiento, string sexo, string estadoCivil, string direccionResidencia1, string direccionResidencia2, string codigoDepartResidencia, string codigoCiudadResisdencia, string zonaPostal, string direccionCorresondencia1,
        string direccionCorresondencia2, string codigDepartDirCor, string codigoCiudadDirCor, string zonaPostalCorres, string oficinaRadicacion, string telResidencia, string telOficina, string afinidadTarjeta, string cupoAsignado, string fechaSolicitud,
        string tipoSolicitud, string manejoCuotas, string tipoTarjeta, string codigoVendedor, string estadoRegistroRecibido, string numeroSolicitud, string tarjetaAnterior, string codigoPuntoDistribucion, string codigoMercadeo, string tipoCuenta,
        string valorCuotaFija, string tipoIdAmparador, string numeroIDAmparador, string oficinaDistribucion, string indicativo, string correoElectronico, string actividadEconomica, string origenIngresos, string codigoBarras, string idenDebitoAut,
        string codBancoDebito, string numCuentaDebito, string tipoCuentaDebito, string indicadorCesacion, string fechaCesacion, string cicloFacturacion, string BIN, string NIT, string subtipo, string codigoCompensacion,
        string codigoNovedad, string codigoProceso, string filler1, string filler2, string filler3, string filler4, string printName, string idTransaccion, string tipoProducto)
        {
            Respuesta respuesta = new Respuesta();
            try
            {
                OperacionesBase operacionesBD = new OperacionesBase();
                operacionesBD.GuardarLogXNombreConectionString(19, 0, 2, "Peticionrecibida- " + idTransaccion,
                                                         ConfigurationManager.AppSettings["BD"]);
                Peticion peticion = new Peticion();
                peticion.numeroProducto = operar.validarDATO(numeroProducto);
                peticion.numeroTarjeta = operar.validarDATO(numeroTarjeta);
                peticion.tipoPersona = operar.validarDATO(tipoPersona);
                peticion.tipoPersona = operar.validarDATO(tipoPersona);
                peticion.primerApellido = operar.validarDATO(primerApellido);
                peticion.segundoApellido = operar.validarDATO(segundoApellido);
                peticion.primerNombre = operar.validarDATO(primerNombre);
                peticion.segundoNombre = operar.validarDATO(segundoNombre);
                peticion.nombreRealce = operar.validarDATO(nombreRealce);
                peticion.nombreLargo = operar.validarDATO(nombreLargo);
                peticion.fechaNacimiento = operar.validarDATO(fechaNacimiento);
                peticion.sexo = operar.validarDATO(sexo);
                peticion.direccionResidencia1 = operar.validarDATO(direccionResidencia1);
                peticion.direccionResidencia2 = operar.validarDATO(direccionResidencia2);
                peticion.telResidencia = operar.validarDATO(telResidencia);
                peticion.telOficina = operar.validarDATO(telOficina);
                peticion.fechaSolicitud = operar.validarDATO(fechaSolicitud);
                peticion.codigoVendedor = operar.validarDATO(codigoVendedor);
                peticion.tarjetaAnterior = operar.validarDATO(tarjetaAnterior);
                peticion.codigoPuntoDistribucion = operar.validarDATO(codigoPuntoDistribucion);
                peticion.codigoMercadeo = operar.validarDATO(codigoMercadeo);
                peticion.correoElectronico = operar.validarDATO(correoElectronico);
                peticion.codigoBarras = operar.validarDATO(codigoBarras);
                peticion.idenDebitoAut = operar.validarDATO(idenDebitoAut);
                peticion.numCuentaDebito = operar.validarDATO(numCuentaDebito);
                peticion.tipoCuentaDebito = operar.validarDATO(tipoCuentaDebito);
                peticion.indicadorCesacion = operar.validarDATO(indicadorCesacion);
                peticion.subtipo = operar.validarDATO(subtipo);
                peticion.codigoNovedad = operar.validarDATO(codigoNovedad);
                peticion.codigoProceso = operar.validarDATO(codigoProceso);
                peticion.tipoIdentificacion = operar.validarDATO(tipoIdentificacion);
                peticion.numeroIdentificacion = operar.validarDATO(numeroIdentificacion);
                peticion.estadoCivil = operar.validarDATO(estadoCivil);
                peticion.codigoDepartResidencia = operar.validarDATO(codigoDepartResidencia);
                peticion.codigoCiudadResisdencia = operar.validarDATO(codigoCiudadResisdencia);
                peticion.zonaPostal = operar.validarDATO(zonaPostal);
                peticion.codigoDepartamentoDirCorrespondencia = operar.validarDATO(codigDepartDirCor);
                peticion.codigoCiudadDirCorrespondencia = operar.validarDATO(codigoCiudadDirCor);
                peticion.zonaPostalDirCorres = operar.validarDATO(zonaPostalCorres);
                peticion.oficinaRadicacion = operar.validarDATO(oficinaRadicacion);
                peticion.afinidadTarjeta = operar.validarDATO(afinidadTarjeta);
                peticion.cupoAsignado = operar.validarDATO(cupoAsignado);
                peticion.tipoSolicitud = operar.validarDATO(tipoSolicitud);
                peticion.manejoCuotas = operar.validarDATO(manejoCuotas);
                peticion.tipoTarjeta = operar.validarDATO(tipoTarjeta);
                peticion.numeroSolicitud = operar.validarDATO(numeroSolicitud);
                peticion.tipoCuenta = operar.validarDATO(tipoCuenta);
                //00
                peticion.valorCuotaFija = operar.validarDATO(valorCuotaFija);
                peticion.tipoIdAmparador = operar.validarDATO(tipoIdAmparador);
                peticion.numeroIDAmparador = operar.validarDATO(numeroIDAmparador);
                //en ceros
                peticion.oficinaDistribucion = operar.validarDATO(oficinaDistribucion);
                peticion.indicativo = operar.validarDATO(indicativo);
                peticion.actividadEconomica = operar.validarDATO(actividadEconomica);
                peticion.origenIngresos = operar.validarDATO(origenIngresos);
                peticion.codBancoDebito = operar.validarDATO(codBancoDebito);
                peticion.fechaCesacion = operar.validarDATO(fechaCesacion);
                peticion.cicloFacturacion = operar.validarDATO(cicloFacturacion);
                peticion.BIN = operar.validarDATO(BIN);
                peticion.NIT = operar.validarDATO(NIT);
                peticion.codigoCompensacion = operar.validarDATO(codigoCompensacion);
                peticion.direccionCorrespondenciaLinea1 = operar.validarDATO(direccionCorresondencia1);
                peticion.direccionCorrespondenciaLinea2 = operar.validarDATO(direccionCorresondencia2);
                peticion.estadoRegistroRecibido = operar.validarDATO(estadoRegistroRecibido);
                peticion.codigoProceso = operar.validarDATO(codigoProceso);
                peticion.filler1 = operar.validarDATO(filler1);
                peticion.filler2 = operar.validarDATO(filler2);
                peticion.filler3 = operar.validarDATO(filler3);
                peticion.filler4 = operar.validarDATO(filler4);

                peticion.printName = operar.validarDATO(printName);
                peticion.idTransaccion = operar.validarDATO(idTransaccion);
                peticion.tipoProducto = operar.validarDATO(tipoProducto);
                peticion.cliente = "COLSUBSIDIO";
                //If(String.IsNullOrEmpty(cliente), "UNICO", cliente.ToUpper)  '"Unico"

                peticion.idTransaccion = operar.ValidarTransaccion(peticion);
                operar.ValidarCliente(peticion);
                respuesta = operar.AdjuntarACola(peticion);

                return respuesta;
            }
            catch (Exception ex)
            {

                respuesta = operar.RespuestaErronea("02", ex.Message);
            }

            return respuesta;
        }
    }
}
