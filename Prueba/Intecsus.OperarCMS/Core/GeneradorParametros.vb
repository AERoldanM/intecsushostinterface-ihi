﻿Imports Intecsus.Entidades
Imports Intecusus.OperarBD
Imports System.Configuration
Imports System.Xml
Imports Intecsus.OperarCMS.ServicioASNET
Imports System.Text

Public Class GeneradorParametros

    Private operacionesBD As OperacionesBase = New OperacionesBase()

#Region "Valores cliente"

    ''' <summary>
    ''' Metodo encargado de cargar los datos de inicializacion de PCNAME, CardName y XML
    ''' </summary>
    ''' <param name="peticion">Peticion inicial enviada por el cliente</param>
    ''' <param name="idTransaccion">Id de la transaccion</param>
    ''' <returns>Retorna respuestaWS con su respectivos datos de  PCNAME, CardName y XML</returns>
    ''' <remarks>AERM 29/07/2015</remarks>
    Public Function RetornarParametrosCliente(peticion As Peticion, varInicio As ConfInicio, idTransaccion As Integer) As RespuestaWS
        Try
            Dim m_respuestaWS = New RespuestaWS()
            Dim m_List As List(Of String) = operacionesBD.ObtenerCardFormatName(peticion.cliente, peticion.tipoProducto, ConfigurationManager.AppSettings("BD"))
            If (m_List.Count >= 2) Then
                m_respuestaWS.cardWizardName = m_List(0)
                m_respuestaWS.xmlPeticion = m_List(1)
            End If
            Dim m_BuscarImpresora As String = operacionesBD.ObtenerValoresConfigurador("BuscarImpresora", ConfigurationManager.AppSettings("BD"))
            ''Obtenemos el PCNAME deacuerdo a configuracion base de datos tabla Impresoras si lo tiene habilitado
            If (Convert.ToBoolean(Convert.ToInt32(m_BuscarImpresora)) = True) Then
                m_respuestaWS.pCName = operacionesBD.ObtenerPCNAME(peticion.printName, ConfigurationManager.AppSettings("BD"))
            Else
                m_respuestaWS.pCName = peticion.printName
            End If
            
            If (String.IsNullOrEmpty(m_respuestaWS.cardWizardName) Or String.IsNullOrEmpty(m_respuestaWS.pCName)) Then
                Dim m_RespuestaNotificar As RespuestaDeviceStatus = New RespuestaDeviceStatus()
                m_RespuestaNotificar.Transaccion = idTransaccion
                m_RespuestaNotificar.hostIdentifier = idTransaccion
                m_RespuestaNotificar.TransaccionCliente = peticion.idTransaccion
                m_RespuestaNotificar.Cliente = peticion.cliente
                m_RespuestaNotificar.Mensaje = "No se encuentra PCName y/ó CardFormat para la transacción "
                varInicio.operarCola.GuardarObjetoNotificacion(m_RespuestaNotificar, varInicio.colaNotificacion)
                Throw New Exception("No se encuentra PCName y/ó CardFormat para la transacción " + idTransaccion.ToString() + "-")
            End If

            Return m_respuestaWS
        Catch ex As Exception
            Throw New Exception("-RetornarParametrosCliente " + ex.Message)
        End Try

        Return Nothing
    End Function

#End Region

#Region "CrearXML"

    ''' <summary>
    ''' Metodo encagado de obtener los datos para el realce
    ''' </summary>
    ''' <param name="peticion">datos a procesar</param>
    ''' <param name="lineaArchivo">Liena archivo a tomar los datos</param>
    ''' <returns>Retorna lista de parametros de la peticion</returns>
    ''' <remarks>AERM 23/04/2015</remarks>
    Public Function ObtenerParametros(peticion As RespuestaWS, lineaArchivo As String) As List(Of ParametrosXML)
        Dim m_Parametros = New List(Of ParametrosXML)
        Try
            Dim m_xml As XmlDocument = New XmlDocument()
            m_xml.LoadXml(peticion.xmlPeticion)
            Dim m_xnList As XmlNodeList = m_xml.SelectNodes("/Parametros/Parametro")
            Dim m_valida As Validadores = New Validadores()
            For Each m_xnode As XmlNode In m_xnList
                Dim m_Inicio As Integer = CInt(m_xnode.Attributes("inicio").Value)
                Dim m_Tamano As Integer = CInt(m_xnode.Attributes("tamano").Value)
                Dim m_Valor As String = lineaArchivo.Substring(m_Inicio, m_Tamano).Trim()
                m_Parametros.Add(Me.ProcesoGeneralXParametro(m_Valor, m_xnode, m_valida))
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message + "La línea " + lineaArchivo)
        End Try

        Return m_Parametros
    End Function

    ''' <summary>
    ''' Metodo encagado de obtener los datos para el realce
    ''' </summary>
    ''' <param name="peticion">datos a procesar</param>
    ''' <param name="m_xmlData">Archivo XML </param>
    ''' <returns>Retorna lista de parametros de la peticion</returns>
    ''' <remarks>AERM 20/07/2015</remarks>
    Public Function ObtenerParametros(peticion As RespuestaWS, m_xmlData As XmlDocument) As List(Of ParametrosXML)
        Dim m_Parametros = New List(Of ParametrosXML)
        Try
            Dim m_xml As XmlDocument = New XmlDocument()
            m_xml.LoadXml(peticion.xmlPeticion)
            Dim m_xnList As XmlNodeList = m_xml.SelectNodes("/Parametros/Parametro")
            Dim m_valida As Validadores = New Validadores()
            For Each m_xnode As XmlNode In m_xnList
                Dim m_EtiquetaXML As String
                If (IsNothing(m_xnode.Attributes("ValoresS"))) Then
                    m_EtiquetaXML = String.Empty
                Else
                    m_EtiquetaXML = m_xnode.Attributes("ValoresS").Value
                End If

                Dim m_Valor As String = Me.ObtenerValorDato(m_EtiquetaXML, m_xmlData)
                m_Parametros.Add(Me.ProcesoGeneralXParametro(m_Valor, m_xnode, m_valida))
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message + "Para el servicio " + m_xmlData.ToString())
        End Try

        Return m_Parametros
    End Function

    ''' <summary>
    ''' Metodo encargado de generar el parametro XML
    ''' </summary>
    ''' <param name="m_Valor">valor inicial a enviar</param>
    ''' <param name="m_xnode">Nodo con los parametros de configuracion</param>
    ''' <param name="m_valida">Clase q se usa para validar los valores a enviar</param>
    ''' <returns>Parametro a enviar</returns>
    ''' <remarks>AERM 30/07/2015</remarks>
    Private Function ProcesoGeneralXParametro(m_Valor As String, m_xnode As XmlNode, m_valida As Validadores) As ParametrosXML
        Dim m_Parametro As ParametrosXML = New ParametrosXML()
        ''Obtengo valores XML
        Dim m_Nombre As String = m_xnode.Attributes("nombre").Value
        Dim m_Codifica As String = m_xnode.Attributes("codifica").Value
        Dim m_tipo As String = m_xnode.Attributes("tipo").Value
        Dim m_Validador As String = Me.ObtenerValidador(m_xnode)
        If m_valida.ValidadorExpresionesRegulares(m_Valor, m_Validador) = False Then
            Throw New Exception("Valor erroneo para  ")
        End If

        m_Parametro = New ParametrosXML()
        m_Parametro.Nombre = "DATAITEM"
        m_Parametro.NameCard = m_Nombre
        m_Parametro.Typo = m_tipo
        m_Parametro.Valor = Me.ObtenerValor(m_Valor, m_xnode)
        m_Parametro.Codificar = If(m_Codifica.ToUpper = "SI", True, False)
        m_Parametro.Derecha = If(m_xnode.Attributes("derecha") IsNot Nothing, CInt(m_xnode.Attributes("derecha").Value), 0)
        m_Parametro.Izquierda = If(m_xnode.Attributes("izquierda") IsNot Nothing, CInt(m_xnode.Attributes("izquierda").Value), 0)
        Return m_Parametro
    End Function

    ''' <summary>
    ''' Metodo encargado de obtener si se debe valdiar el dato del cliente
    ''' </summary>
    ''' <param name="m_xnode">Parametro que contiene los nodos del XML</param>
    ''' <returns>Retorna formato de validacion a realizar</returns>
    ''' <remarks>AERM 30/07/2015</remarks>
    Private Function ObtenerValidador(m_xnode As XmlNode) As String
        Try
            If m_xnode.Attributes("valida") IsNot Nothing Then
                Dim m_Validador As String = m_xnode.Attributes("valida").Value
                Return operacionesBD.ObtenerValoresConfigurador(m_Validador, ConfigurationManager.AppSettings("BD"))
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message + " ObtenerValidador")
        End Try
        Return String.Empty
    End Function

    ''' <summary>
    ''' Por optimizar
    ''' Metodo encargado de obtener un valor en cualquiera de sus variantes (En el xml de configuracion)
    ''' </summary>
    ''' <param name="m_Valor"> Valor inicial</param>
    ''' <param name="m_xnode">Parametro que contiene los nodos del XML</param>
    ''' <returns></returns>
    ''' <remarks>AERM 30/07/2015</remarks>
    Private Function ObtenerValor(m_Valor As String, m_xnode As XmlNode) As String
        Try
            If m_xnode.Attributes("sustituir") IsNot Nothing Then
                Dim m_Sustituir As String = m_xnode.Attributes("sustituir").Value
                Return m_Sustituir
            End If

            If m_xnode.Attributes("agregar") IsNot Nothing Then
                Dim m_Agregar As String = m_xnode.Attributes("agregar").Value
                m_Valor = m_Agregar + m_Valor
            End If

            If m_xnode.Attributes("hexa") IsNot Nothing Then
                Dim hexNumbers As System.Text.StringBuilder = New System.Text.StringBuilder
                Dim byteArray() As Byte
                byteArray = System.Text.ASCIIEncoding.ASCII.GetBytes(m_Valor)
                For i As Integer = 0 To byteArray.Length - 1
                    hexNumbers.Append(byteArray(i).ToString("x"))
                Next

                Return hexNumbers.ToString()
            End If

            If m_xnode.Attributes("fecha") IsNot Nothing Then
                Dim fecha As DateTime = DateTime.Now()
                Return fecha.ToString("MM/yy")
            End If

            If m_xnode.Attributes("replace") IsNot Nothing Then
                Dim m_Replace As String() = m_xnode.Attributes("replace").Value.Split(",")
                m_Valor = m_Valor.Replace(m_Replace(0), m_Replace(1))
                Return m_Valor
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return m_Valor
    End Function

    ''' <summary>
    ''' Metodo encargado en obtener el valor pro WS
    ''' </summary>
    ''' <param name="m_EtiquetaXML">COntiene los atributos que toma el valor (puede ser compuesto)</param>
    ''' <param name="m_xmlData">Xml que contiene la informacion a obtener</param>
    ''' <returns>El valor completo</returns>
    ''' <remarks>AERM 30/07/2015</remarks>
    Private Function ObtenerValorDato(m_EtiquetaXML As String, m_xmlData As XmlDocument) As String
        Dim m_valores As String() = m_EtiquetaXML.Split(",")
        Dim m_Valor As StringBuilder = New StringBuilder()
        For Each m_Param As String In m_valores
            m_Valor.Append(m_xmlData.SelectSingleNode(m_Param).InnerText)
        Next

        Return m_Valor.ToString()
    End Function
#End Region




End Class
