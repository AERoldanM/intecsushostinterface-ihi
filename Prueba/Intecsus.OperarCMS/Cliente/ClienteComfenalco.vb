﻿Imports Intecsus.Entidades
Imports Intecusus.OperarBD
Imports System.Configuration
Imports Intecsus.OperarArchivos

''' <summary>
''' Clase q representa las operaciones sobre 
''' </summary>
''' <remarks>AERM 19/03/2015</remarks>
Public Class ClienteComfenalco
    Implements IOperarcionesCliente

#Region "Variables"
    Private m_AdaptadorASNET As AdaptadorASNET = New AdaptadorASNET()
    Private servicio As ServicioASNET.EmisionTarjetasProxyClient = New ServicioASNET.EmisionTarjetasProxyClient()
    Private operacionesBD As OperacionesBase = New OperacionesBase()
#End Region


#Region "Operaciones Interface"

    Public Function ObtenerRealce(peticion As Peticion, transaccion As Integer, varInicio As ConfInicio) As RespuestaWS Implements IOperarcionesCliente.ObtenerRealce
        Return Me.EjecutarOperacion(peticion, transaccion, varInicio)
    End Function

    Public Sub NotificarCliente(notificacion As Entidades.RespuestaDeviceStatus, idTransaccion As String, varInicio As ConfInicio) Implements IOperarcionesCliente.NotificarCliente
        Try
            Dim m_operar As ICArchivos = New RetornarArchivo().IntanciarArchivo("TXT")
            Dim m_rutaArchivo = operacionesBD.ObtenerValoresConfigurador("RutaArchivoNotifica", ConfigurationManager.AppSettings("BD"))
            Dim m_mensaje As String = "Transacion : " + idTransaccion + " Mensaje " + notificacion.Estado.ToString() + "  " + notificacion.Mensaje
            m_operar.AgregarLineaArchivo(m_rutaArchivo, "Notificaciones.txt", m_mensaje)
            operacionesBD.GuardarLogXNombreConectionString(10, notificacion.hostIdentifier, 1, "OperarCMS.ClienteComfenalco.vb - NotificarCliente  ",
                                                        ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception

            operacionesBD.GuardarLogXNombreConectionString(10, notificacion.hostIdentifier, 2, "OperarCMS.ClienteLineaArchivo.vb - NotificarCliente : " + ex.Message,
                                                        ConfigurationManager.AppSettings("BD"))
            Throw ex
        End Try
    End Sub
#End Region


#Region "Operaciones de envio"

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="peticion"></param>
    ''' <param name="transaccion"></param>
    ''' <returns></returns>
    ''' <remarks>AERM 24/03/2015</remarks>
    Private Function EjecutarOperacion(ByVal peticion As Peticion, transaccion As Integer, varInicio As ConfInicio) As RespuestaWS
        Dim m_respuesta As RespuestaWS = New RespuestaWS()
        Try

            Dim m_PeticionWS As ServicioASNET.solicitarEmisionTarjetasRequest = m_AdaptadorASNET.ConvertirClienteTOServicio(peticion, transaccion)
            'Dim m_respuestaWS As ServicioASNET.solicitarEmisionTarjetasResponse
            Dim m_respuestaWS As ServicioASNET.solicitarEmisionTarjetasResponse = Me.servicio.ejecucionEmisionTarjetas(m_PeticionWS)
            Return m_AdaptadorASNET.RetornarRespuestaToService(m_respuestaWS, transaccion, peticion, varInicio)
            operacionesBD.GuardarLogXNombreConectionString(14, transaccion, 1, "ClienteComfenalco - EjecutarOperacion ",
                                                        ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(14, transaccion, 2, "ClienteComfenalco - EjecutarOperacion " + ex.Message,
                                                            ConfigurationManager.AppSettings("BD"))
            Throw ex
        End Try
        Return m_respuesta
    End Function
#End Region
   


End Class
