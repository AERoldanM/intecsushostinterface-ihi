﻿Imports Intecsus.Entidades
Imports Intecusus.OperarBD
Imports System.Configuration
Imports Intecsus.OperarArchivos
Imports Intecsus.OperarXML

''' <summary>
''' Clase q respesenta las operaciones sobre el clietne COlsubsidio
''' </summary>
''' <remarks></remarks>
Public Class ClienteColsubsidio
    Implements IOperarcionesCliente

#Region "Variables"
    Private m_AdaptadorASNET As AdaptadorASNET = New AdaptadorASNET()
    Private servicio As ServicioASNET.EmisionTarjetasProxyClient = New ServicioASNET.EmisionTarjetasProxyClient()
    Private operacionesBD As OperacionesBase = New OperacionesBase()
#End Region


#Region "Operaciones Interface"

    Public Function ObtenerRealce(peticion As Peticion, transaccion As Integer, varInicio As ConfInicio) As RespuestaWS Implements IOperarcionesCliente.ObtenerRealce
        Return Me.EjecutarOperacion(peticion, transaccion, varInicio)
    End Function

    Public Sub NotificarCliente(notificacion As Entidades.RespuestaDeviceStatus, idTransaccion As String, varInicio As ConfInicio) Implements IOperarcionesCliente.NotificarCliente
        Try

            Dim m_servicio As ServicioColsubsidio.ColsubsidioServiceClient = New ServicioColsubsidio.ColsubsidioServiceClient()
            Dim m_respuesta As ServicioColsubsidio.Mensaje
            Dim m_encabezado As ServicioColsubsidio.MyHeader = New ServicioColsubsidio.MyHeader()
            m_encabezado.UserName = operacionesBD.ObtenerValoresConfigurador("UserColsubsidio", ConfigurationManager.AppSettings("BD"))
            m_encabezado.Password = operacionesBD.ObtenerValoresConfigurador("PasswordColsubsidio", ConfigurationManager.AppSettings("BD"))

            Dim m_Tarjeta As ServicioColsubsidio.Tarjeta = New ServicioColsubsidio.Tarjeta()
            m_Tarjeta.fecha_entrega = Date.Now()
            m_Tarjeta.Estado_tarjeta = notificacion.Estado.ToString()
            m_Tarjeta.Mensaje = notificacion.Mensaje
            m_Tarjeta.numero_tarjeta = idTransaccion
            Dim objetoXML As String = varInicio.operarXML.Serializar(Of ServicioColsubsidio.Tarjeta)(m_Tarjeta)
            operacionesBD.GuardarLogXNombreConectionString(21, notificacion.hostIdentifier, 1, "-NotificarCliente Envio " + objetoXML,
                                                        ConfigurationManager.AppSettings("BD"))
            m_respuesta = m_servicio.ActualizacionEstadoTarjeta(m_encabezado, m_Tarjeta)
            objetoXML = varInicio.operarXML.Serializar(Of ServicioColsubsidio.Mensaje)(m_respuesta)
            operacionesBD.GuardarLogXNombreConectionString(21, notificacion.hostIdentifier, 1, "-NotificarCliente Resp: " + objetoXML,
                                                        ConfigurationManager.AppSettings("BD"))

            If (notificacion.Estado = ESTADO_REALCE.ST_ERROR <> notificacion.Estado = ESTADO_REALCE.ST_ERROR_DELETE) Then
                Dim m_Peticion As Peticion = m_AdaptadorASNET.RealizarRollBack(notificacion, idTransaccion)
                Me.EjecutarOperacion(m_Peticion, notificacion.hostIdentifier, varInicio)
            End If
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(10, notificacion.hostIdentifier, 2, "OperarCMS.ClienteLineaArchivo.vb - NotificarCliente : " + ex.Message,
                                                        ConfigurationManager.AppSettings("BD"))
            Throw ex
        End Try
    End Sub
#End Region


#Region "Operaciones de envio"

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="peticion"></param>
    ''' <param name="transaccion"></param>
    ''' <returns></returns>
    ''' <remarks>AERM 24/03/2015</remarks>
    Private Function EjecutarOperacion(ByVal peticion As Peticion, transaccion As Integer, varInicio As ConfInicio) As RespuestaWS
        Dim m_respuesta As RespuestaWS = New RespuestaWS()
        Try
            Dim m_PeticionWS As ServicioASNET.solicitarEmisionTarjetasRequest = m_AdaptadorASNET.ConvertirClienteTOServicio(peticion, transaccion)
            Dim objetoXML As String = varInicio.operarXML.Serializar(Of ServicioASNET.solicitarEmisionTarjetasRequest)(m_PeticionWS)
            operacionesBD.GuardarLogXNombreConectionString(14, transaccion, 1, "ClienteColsubsidio - EjecutarOperacion " + objetoXML,
                                                        ConfigurationManager.AppSettings("BD"))
            'Dim m_respuestaWS As ServicioASNET.solicitarEmisionTarjetasResponse
            servicio = New ServicioASNET.EmisionTarjetasProxyClient()
            servicio.ClientCredentials.UserName.UserName = operacionesBD.ObtenerValoresConfigurador("UserNameAsNet", ConfigurationManager.AppSettings("BD"))
            servicio.ClientCredentials.UserName.Password = operacionesBD.ObtenerValoresConfigurador("PasswordAsNet", ConfigurationManager.AppSettings("BD"))
            Dim m_respuestaWS As ServicioASNET.solicitarEmisionTarjetasResponse = Me.servicio.ejecucionEmisionTarjetas(m_PeticionWS)
            Return m_AdaptadorASNET.RetornarRespuestaToService(m_respuestaWS, transaccion, peticion, varInicio)
        Catch ex As Exception
            Dim operarXML As GeneradorXML = New GeneradorXML()
            Dim m_Peticion As String = operarXML.Serializar(Of Peticion)(peticion)
            Dim m_RespuestaFinal As String
            Dim m_Respuesta2 As Respuesta = New Respuesta()
            m_Respuesta2.codigo = "01"
            m_Respuesta2.mensaje = ex.Message
            m_Respuesta2.numeroAutorizacion = "No existe"
            m_RespuestaFinal = operarXML.Serializar(Of Respuesta)(m_Respuesta2)
            operacionesBD.GuardarRespuestaWS(peticion, m_Peticion, m_RespuestaFinal, m_Respuesta2, ConfigurationManager.AppSettings("BD"))
            operacionesBD.GuardarLogXNombreConectionString(14, transaccion, 2, "ClienteColsubsidio - EjecutarOperacion " + ex.Message,
                                                            ConfigurationManager.AppSettings("BD"))
            Throw ex
        End Try
        Return m_respuesta
    End Function


    'Public Overrides Sub SecureMessage(ByVal envelope As SoapEnvelope, ByVal security As Security)
    '    Dim userToken As UsernameToken
    '    userToken = New UsernameToken(userName, userName, PasswordOption.SendNone)
    '    ' Adds the token to the SOAP header.
    '    security.Tokens.Add(userToken)
    'End Sub 'SecureMessage
#End Region



End Class
