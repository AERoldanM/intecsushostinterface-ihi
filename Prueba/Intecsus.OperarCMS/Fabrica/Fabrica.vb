﻿''' <summary>
''' Clasre encargada de instanciar
''' </summary>
''' <remarks>AERM 19/03/2015</remarks>
Public Class Fabrica
    Public Function RetornarCliente(ByVal cliente As String) As IOperarcionesCliente
        Try
            Select Case cliente.ToUpper
                Case "UNICO"
                    Return New ClienteComfenalco()
                Case "PRUEBAINTECSUS"
                    Return New ClientePrueba()
                Case "ARCHIVO"
                    Return New ClienteLineaArchivo()
                Case "COLSUBSIDIO"
                    Return New ClienteColsubsidio()
                Case Else
                    Throw New Exception("No está disponible para este cliente. (Fabrica - RetornarCliente)")
            End Select
        Catch ex As Exception
            Throw New Exception("RetornarCliente " + ex.Message)
        End Try
    End Function
End Class
