﻿Imports Intecsus.MSMQ
Imports Intecusus.OperarBD
Imports Intecsus.OperarXML
Imports System.Configuration
Imports Intecsus.Entidades
Imports System.IO
Imports Intecsus.OperarArchivos

Public Class ProcesoBase

#Region "Variables"
    Private varInicio As ConfInicio
    Private integrador As OperarIntegrador
#End Region

#Region "Eventos"

    Event TerminoProceso As EventHandler(Of Integer)
#End Region

#Region "Inicializar variables"
    ''' <summary>
    ''' Metodo encargado de inicializar as variables base para comenzar los procesos
    ''' </summary>
    ''' <remarks>AERM 20/03/2015</remarks>
    Public Sub InicilizarVariables()
        Try
            varInicio = New ConfInicio()
            integrador = New OperarIntegrador(varInicio)
        Catch ex As Exception
            Throw New Exception("Procesar.vb - InicilizarVariables" + ex.Message)
        End Try
    End Sub
#End Region

#Region "Proceso"

    ''' <summary>
    ''' Metodo encargado de retornar el tiempo para comenzar los procesos
    ''' </summary>
    ''' <returns>Retorna Tiempo en segundos </returns>
    ''' <remarks>AERM 20/03/2015</remarks>
    Public Function RetornarTiempo() As Integer
        Return varInicio.tiempoProceso
    End Function

    ''' <summary>
    ''' Meetodo encargado de comenzar el proceso de envio de notificaciones
    ''' </summary>
    ''' <remarks>AERM 25/03/2015</remarks>
    Public Sub EnviarNotificaciones()
        Dim m_IdTransaccion As Integer = 0
        Try
            Dim m_respuesta As RespuestaDeviceStatus = New RespuestaDeviceStatus()
            Dim m_procesar As IOperarcionesCliente
            While varInicio.operarCola.RecibirObjetoNotificar(varInicio.colaNotificacion, m_respuesta)
                Try

                    m_IdTransaccion = If(IsNumeric(m_respuesta.hostIdentifier), CInt(m_respuesta.hostIdentifier), 0)
                    Dim m_valores As List(Of String) = varInicio.operacionesBD.ObtenerDatosCliente(m_respuesta.hostIdentifier, ConfigurationManager.AppSettings("BD"))
                    If (m_valores.Count >= 2) Then
                        m_respuesta.Cliente = m_valores(0)
                        m_respuesta.TransaccionCliente = m_valores(1)
                    End If
                    m_procesar = varInicio.fabrica.RetornarCliente(m_respuesta.Cliente)
                    m_procesar.NotificarCliente(m_respuesta, m_respuesta.TransaccionCliente, varInicio)
                    varInicio.operacionesBD.CambiarEstado(m_respuesta.hostIdentifier, ConfigurationManager.AppSettings("BD"), ESTADO_REALCE.ST_OK.ToString() + "Notificacion", True)
                    varInicio.operacionesBD.GuardarLogXNombreConectionString(10, m_IdTransaccion, 1, "OperarCMS.Procesos.vb - EnviarNotificaciones  ",
                                                       ConfigurationManager.AppSettings("BD"))
                    m_IdTransaccion = 0
                Catch ex As Exception
                    'reintentos Proceso 10- Notificar Proces
                    varInicio.operacionesBD.CambiarEstado(m_respuesta.hostIdentifier, ConfigurationManager.AppSettings("BD"), ESTADO_REALCE.ST_ERROR.ToString() + "Notificacion", True)
                    Dim serializar As String = varInicio.operarXML.Serializar(m_respuesta)
                    varInicio.operacionesBD.InsertarProceso(m_respuesta.Transaccion, ConfigurationManager.AppSettings("BD"), serializar, 10)
                    Throw ex
                End Try
            End While

        Catch ex As Exception
            varInicio.operacionesBD.GuardarLogXNombreConectionString(10, m_IdTransaccion, 2, "OperarCMS.Procesos.vb - EnviarNotificaciones : " + ex.Message,
                                                         ConfigurationManager.AppSettings("BD"))
        Finally
            Me.ReintentosNotificacion()
            RaiseEvent TerminoProceso(Me, varInicio.tiempoProceso)
        End Try
    End Sub

    ''' <summary>
    ''' Metodo usado para inicar el proceso de traer clientes
    ''' </summary>
    ''' <remarks>AERM 20/03/2015</remarks>
    Public Sub ComenzarProceso()
        Try
            Me.IniciaCargaCola()
            If (varInicio.procesaArchivo) Then
                Me.IniciarArchivo()
            End If
        Catch ex As Exception
            varInicio.operacionesBD.GuardarLogXNombreConectionString(11, 0, 2, "ProcesoBase.Procesos.vb - ComenzarProceso : " + ex.Message,
                                  ConfigurationManager.AppSettings("BD"))
        Finally
            Me.ReintentosTomaClientes()
        End Try
    End Sub
#End Region

#Region "Por Cola"

    ''' <summary>
    ''' Metodo encargado del proceso X cola
    ''' </summary>
    ''' <remarks>AERM 29/07/2015</remarks>
    Private Sub IniciaCargaCola()
        Dim m_IdTransaccion As Integer = 0
        Try
            varInicio.operacionesBD.GuardarLogXNombreConectionString(11, m_IdTransaccion, 1, "OperarCMS.Procesos.vb - ComenzarProcesoXCOLA ",
                                                          ConfigurationManager.AppSettings("BD"))
            Dim m_Cliente As Peticion = New Peticion()
            While varInicio.operarCola.RecibirPeticionCliente(varInicio.colaClientes, m_Cliente)
                If (Me.ValidarCola(m_Cliente)) Then
                    m_IdTransaccion = varInicio.operacionesBD.InsertarTransaccion(m_Cliente.cliente, m_Cliente.idTransaccion, ConfigurationManager.AppSettings("BD"))
                    Try
                        integrador.IniciarProceso(m_Cliente, m_IdTransaccion)
                    Catch ex As Exception
                        'reintentos Proceso 11- Tomar clientes
                        Dim serializar As String = varInicio.operarXML.Serializar(m_Cliente)
                        varInicio.operacionesBD.InsertarProceso(m_IdTransaccion, ConfigurationManager.AppSettings("BD"), serializar, 11)
                        ' Throw ex
                    End Try
                End If
                m_IdTransaccion = 0
            End While
        Catch ex As Exception
            varInicio.operacionesBD.GuardarLogXNombreConectionString(11, m_IdTransaccion, 2, "ProcesoBase.Procesos.vb - XCOLA : " + ex.Message,
                                  ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub


#End Region

#Region "Proceso Archivos"
    ''' <summary>
    ''' Metodo encargado de comenzar el proceso de carga de archivos
    ''' </summary>
    ''' <remarks>AERM 29/07/2015</remarks>
    Private Sub IniciarArchivo()
        Dim m_IdTransaccion As Integer = 0
        Try
            varInicio.operacionesBD.GuardarLogXNombreConectionString(11, m_IdTransaccion, 1, "OperarCMS.Procesos.vb - ComenzarProcesoXArchivo ",
                                                          ConfigurationManager.AppSettings("BD"))
            Dim m_Validar As Validadores = New Validadores()
            Dim m_Archivos As List(Of FileInfo) = m_Validar.RetornarArchivos(varInicio.ubicacionArchivo)
            Me.LecturaArchivos(m_Archivos)
        Catch ex As Exception
            varInicio.operacionesBD.GuardarLogXNombreConectionString(11, m_IdTransaccion, 2, "ProcesoBase.Procesos.vb - XArchivo : " + ex.Message,
                                  ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de la lectura de archivos a procesar
    ''' </summary>
    ''' <param name="m_Archivos">Archivo en formato FielInfo a procesar</param>
    ''' <remarks>AERM 20/03/2015</remarks>
    Private Sub LecturaArchivos(m_Archivos As List(Of FileInfo))
        Dim m_operar As ICArchivos = New RetornarArchivo().IntanciarArchivo(varInicio.extensionArchivo)
        For Each m_Archivo In m_Archivos
            Try
                Dim m_lineasArchivo As List(Of String) = m_operar.ObtenerInformacioArchivo(varInicio.ubicacionArchivo, m_Archivo.Name)
                Dim m_NombreArchivo As String = Date.Now.ToString()
                m_NombreArchivo = m_NombreArchivo.Replace("/", "-")
                m_NombreArchivo = m_NombreArchivo.Replace(":", "-")
                m_operar.CopiarArchivo(varInicio.ubicacionArchivo, m_Archivo.Name, varInicio.archivoProcesado, m_NombreArchivo + "procesado")
                m_operar.EliminarArchivo(varInicio.ubicacionArchivo + "\" + m_Archivo.Name)
                Me.ProcesoXLineasArchivo(m_lineasArchivo, 0)
            Catch ex As Exception
                varInicio.operacionesBD.GuardarLogXNombreConectionString(11, 0, 2, "OperarCMS.Procesos.vb - LecturaArchivos : " + ex.Message + " " + m_Archivo.Name,
                                                         ConfigurationManager.AppSettings("BD"))
            End Try
        Next
    End Sub

    ''' <summary>
    ''' Metodo encargado de procesar las lineas del archivo plano
    ''' </summary>
    ''' <param name="m_lineasArchivo">Lineas de archivo a procesar</param>
    ''' <param name="m_IdTransaccion">Identificador de la transaccion</param>
    ''' <remarks>AERM 20/03/2015</remarks>
    Private Sub ProcesoXLineasArchivo(m_lineasArchivo As List(Of String), m_IdTransaccion As Integer)
        Try
            For Each linea As String In m_lineasArchivo
                Dim rnd As New Random()
                Dim m_Cliente As Peticion = New Peticion()
                m_Cliente.cliente = "ARCHIVO"
                m_Cliente.nombreLargo = linea.Trim()
                m_Cliente.codigoNovedad = "03"
                ' m_Cliente.idTransaccion = rnd.[Next](0, 1000).ToString() + "Prueba"
                m_Cliente.tipoProducto = varInicio.cardWizardNameXArchivo
                m_Cliente.printName = varInicio.operacionesBD.ObtenerValoresConfigurador("PCNAMEXArchivo", ConfigurationManager.AppSettings("BD"))
                Try
                    m_Cliente.idTransaccion = linea.Substring(0, 4)
                    m_IdTransaccion = varInicio.operacionesBD.InsertarTransaccion("ARCHIVO", m_Cliente.idTransaccion, ConfigurationManager.AppSettings("BD"))
                    integrador.IniciarProceso(m_Cliente, m_IdTransaccion)
                    ' Me.txtRespuesta.Text += String.Format("{0} {1}", linea, System.Environment.NewLine)
                Catch ex As Exception
                    'Reintentos
                    ' Dim serializar As String = operarXML.Serializar(m_Cliente)
                    ' operacionesBD.InsertarProceso(m_IdTransaccion, ConfigurationManager.AppSettings("BD"), serializar, 11)
                    varInicio.operacionesBD.GuardarLogXNombreConectionString(11, m_IdTransaccion, 2, "OperarCMS.Procesos.vb - ProcesoXLineasArchivo - LeerLinea : " + ex.Message + " " + linea,
                                                          ConfigurationManager.AppSettings("BD"))
                End Try
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Reintentos"

    ''' <summary>
    ''' Metodo encargado de tomar los reintentos almacenados en B.D
    ''' </summary>
    ''' <remarks>AERM 27/03/2015</remarks>
    Private Sub ReintentosTomaClientes()
        Try
            Dim m_procesos As List(Of Reintentos) = varInicio.operacionesBD.ObtnerNotificacion(11, ConfigurationManager.AppSettings("BD"))
            For Each m_proceso In m_procesos
                Try
                    Dim m_Cliente As Peticion = varInicio.operarXML.Deserialize(Of Peticion)(m_proceso.Informacion)
                    integrador.IniciarProceso(m_Cliente, m_proceso.IDTransaccion)
                    varInicio.operacionesBD.EliminarProceso(m_proceso.IDProceso, ConfigurationManager.AppSettings("BD"))
                Catch ex As Exception
                    ''Aumentar el reintento
                    varInicio.operacionesBD.AumentarIntentosXProceso(m_proceso.IDProceso, ConfigurationManager.AppSettings("BD"))
                End Try
            Next
        Catch ex As Exception
            varInicio.operacionesBD.GuardarLogXNombreConectionString(11, 0, 2, "OperarCMS.Procesos.vb - ReintentosTomaClientes : " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de reintentar enviar la notificacion
    ''' </summary>
    ''' <remarks>AERM 27/03/2015</remarks>
    Private Sub ReintentosNotificacion()
        Try
            Dim m_procesos As List(Of Reintentos) = varInicio.operacionesBD.ObtnerNotificacion(10, ConfigurationManager.AppSettings("BD"))
            For Each m_proceso In m_procesos
                Try
                    Dim m_respuesta As RespuestaDeviceStatus = varInicio.operarXML.Deserialize(Of RespuestaDeviceStatus)(m_proceso.Informacion)
                    Dim m_procesar As IOperarcionesCliente
                    Dim valores As List(Of String) = varInicio.operacionesBD.ObtenerDatosCliente(m_respuesta.hostIdentifier, ConfigurationManager.AppSettings("BD"))
                    If (valores.Count >= 2) Then
                        m_procesar = varInicio.fabrica.RetornarCliente(valores(0))
                        m_procesar.NotificarCliente(m_respuesta, valores(1), varInicio)
                        varInicio.operacionesBD.CambiarEstado(m_respuesta.hostIdentifier, ConfigurationManager.AppSettings("BD"), ESTADO_REALCE.ST_OK.ToString() + "Notificacion", True)
                        varInicio.operacionesBD.EliminarProceso(m_proceso.IDProceso, ConfigurationManager.AppSettings("BD"))
                    Else
                        varInicio.operacionesBD.AumentarIntentosXProceso(m_proceso.IDProceso, ConfigurationManager.AppSettings("BD"))
                    End If
                Catch ex As Exception
                    ''Aumentar el reintento
                    varInicio.operacionesBD.AumentarIntentosXProceso(m_proceso.IDProceso, ConfigurationManager.AppSettings("BD"))
                    Throw ex
                End Try
            Next
        Catch ex As Exception
            varInicio.operacionesBD.GuardarLogXNombreConectionString(10, 0, 2, "OperarCMS.Procesos.vb - ReintentosNotificacion : " + ex.Message,
                                                        ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    Private Function ValidarCola(m_respuesta As Peticion) As Boolean
        Try
            Dim m_RespuestaWS As RespuestaWSASNET = varInicio.operacionesBD.ObtenerRespuesta(m_respuesta.cliente, m_respuesta.idTransaccion, ConfigurationManager.AppSettings("BD"))
            If (IsNothing(m_RespuestaWS) = True) Then
                Return True
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return False
    End Function
#End Region

End Class
