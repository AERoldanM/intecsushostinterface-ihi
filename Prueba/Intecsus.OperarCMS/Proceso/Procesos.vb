﻿Imports Intecsus.MSMQ
Imports System.Configuration
Imports Intecusus.OperarBD
Imports Intecsus.Entidades
Imports Intecsus.OperarXML
Imports Intecsus.OperarArchivos
Imports System.IO

Public Class Procesos

#Region "Variables"
    Private tiempoProceso As Integer
    Private colaRealce As String
    Private colaClientes As String
    Private colaNotificacion As String
    Private operarCola As ICola = New InstanciaCola().IntanciarCola(ConfigurationManager.AppSettings("ColaInstancia"))
    Private operacionesBD As OperacionesBase = New OperacionesBase()
    Private fabrica As Fabrica = New Fabrica()
    Private operarXML As GeneradorXML = New GeneradorXML()
    Private ubicacionArchivo As String = String.Empty
    Private nombreArchivo As String = String.Empty
    Private extensionArchivo As String = String.Empty
    Private cardWizardNameXArchivo As String = String.Empty
    Private archivoProcesado As String = String.Empty
    Private procesaArchivo As Boolean
#End Region

#Region "Inicializar variables"

    ''' <summary>
    ''' Metodo encargado de inicializar as variables base para comenzar los procesos
    ''' </summary>
    ''' <remarks>AERM 20/03/2015</remarks>
    Public Sub InicilizarVariables()
        Try
            ''Variable tiempos
            tiempoProceso = Convert.ToInt32(operacionesBD.ObtenerValoresConfigurador("EjecucionProcesoMiliSegundos", ConfigurationManager.AppSettings("BD")))
            ''Colas
            colaRealce = operacionesBD.ObtenerValoresConfigurador("ColaRealce", ConfigurationManager.AppSettings("BD"))
            colaClientes = operacionesBD.ObtenerValoresConfigurador("ColaCliente", ConfigurationManager.AppSettings("BD"))
            colaNotificacion = operacionesBD.ObtenerValoresConfigurador("ColaNotificacion", ConfigurationManager.AppSettings("BD"))
            ubicacionArchivo = operacionesBD.ObtenerValoresConfigurador("RutaArchivo", ConfigurationManager.AppSettings("BD"))
            extensionArchivo = operacionesBD.ObtenerValoresConfigurador("FormatoArchivo", ConfigurationManager.AppSettings("BD"))
            cardWizardNameXArchivo = operacionesBD.ObtenerValoresConfigurador("CardWizardNameXArchivo", ConfigurationManager.AppSettings("BD"))
            archivoProcesado = operacionesBD.ObtenerValoresConfigurador("RutaArchivoProcesado", ConfigurationManager.AppSettings("BD"))
            ''Procesa Archivo
            Dim m_procesaArchivo As String = operacionesBD.ObtenerValoresConfigurador("ProcesarArchivo", ConfigurationManager.AppSettings("BD"))
            If m_procesaArchivo.ToUpper = "ACTIVO" Then
                procesaArchivo = True
            Else
                procesaArchivo = False
            End If
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(1, 0, 2, "Procesar.vb - InicilizarVariables : " + ex.Message,
                                                           ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub
#End Region

#Region "Eventos"

    Event TerminoProceso As EventHandler(Of Integer)
#End Region

#Region "Proceso"

    ''' <summary>
    ''' Metodo encargado de retornar el tiempo para comenzar los procesos
    ''' </summary>
    ''' <returns>Retorna Tiempo en segundos </returns>
    ''' <remarks>AERM 20/03/2015</remarks>
    Public Function RetornarTiempo() As Integer
        Return tiempoProceso
    End Function

    ''' <summary>
    ''' Metodo encargado de ndicar si se procesa el archivo o no
    ''' </summary>
    ''' <returns>Retorna si se procesa el archivo</returns>
    ''' <remarks>AERM 09/04/2015</remarks>
    Public Function ProcesarArchivo() As Boolean
        Return procesaArchivo
    End Function

    ''' <summary>
    ''' Metodo usado para inicar el proceso de traer clientes
    ''' </summary>
    ''' <remarks>AERM 20/03/2015</remarks>
    Public Sub ComenzarProceso()
        Dim m_IdTransaccion As Integer = 0
        Try
            operacionesBD.GuardarLogXNombreConectionString(11, m_IdTransaccion, 1, "OperarCMS.Procesos.vb - ComenzarProceso ",
                                                          ConfigurationManager.AppSettings("BD"))
            Dim m_Cliente As Peticion = New Peticion()
            While operarCola.RecibirPeticionCliente(colaClientes, m_Cliente)
                m_IdTransaccion = operacionesBD.InsertarTransaccion(m_Cliente.cliente, m_Cliente.idTransaccion, ConfigurationManager.AppSettings("BD"))
                Try
                    Me.IniciarProceso(m_Cliente, m_IdTransaccion)
                Catch ex As Exception
                    'reintentos Proceso 11- Tomar clientes
                    Dim serializar As String = operarXML.Serializar(m_Cliente)
                    operacionesBD.InsertarProceso(m_IdTransaccion, ConfigurationManager.AppSettings("BD"), serializar, 11)
                    Throw ex
                End Try

                m_IdTransaccion = 0
            End While
        Catch ex As Exception
            'RaiseEvent TerminoProceso(Me, tiempoProceso)
            operacionesBD.GuardarLogXNombreConectionString(11, m_IdTransaccion, 2, "OperarCMS.Procesos.vb - ComenzarProceso : " + ex.Message,
                                  ConfigurationManager.AppSettings("BD"))
        Finally
            'Me.ReintentosTomaClientes()
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de comenzar el proceso de lectura del archivo
    ''' </summary>
    ''' <remarks>AERM 31/03/2015</remarks>
    Public Sub ComenzarLecturaArchivo()
        Dim m_IdTransaccion As Integer = 0
        Try
            operacionesBD.GuardarLogXNombreConectionString(11, m_IdTransaccion, 1, "OperarCMS.Procesos.vb - ComenzarLecturaArchivo ",
                                                        ConfigurationManager.AppSettings("BD"))
            Dim m_Validar As Validadores = New Validadores()
            Dim m_Archivos As List(Of FileInfo) = m_Validar.RetornarArchivos(ubicacionArchivo)
            Me.LecturaArchivos(m_Archivos)
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(11, m_IdTransaccion, 2, "OperarCMS.Procesos.vb - ComenzarLecturaArchivo : " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
        Finally
            Me.ReintentosTomaClientes()
        End Try
    End Sub

    ''' <summary>
    ''' Meetodo encargado de comenzar el proceso de envio de notificaciones
    ''' </summary>
    ''' <remarks>AERM 25/03/2015</remarks>
    Public Sub EnviarNotificaciones()
        Dim m_IdTransaccion As Integer = 0
        Try
            Dim m_respuesta As RespuestaDeviceStatus = New RespuestaDeviceStatus()
            Dim m_procesar As IOperarcionesCliente
            While operarCola.RecibirObjetoNotificar(colaNotificacion, m_respuesta)
                Try
                    m_IdTransaccion = If(IsNumeric(m_respuesta.hostIdentifier), CInt(m_respuesta.hostIdentifier), 0)
                    Dim m_valores As List(Of String) = operacionesBD.ObtenerDatosCliente(m_respuesta.hostIdentifier, ConfigurationManager.AppSettings("BD"))
                    If (m_valores.Count >= 2) Then
                        m_respuesta.Cliente = m_valores(0)
                        m_respuesta.TransaccionCliente = m_valores(1)
                    End If

                    m_procesar = fabrica.RetornarCliente(m_respuesta.Cliente)
                    m_procesar.NotificarCliente(m_respuesta, m_respuesta.TransaccionCliente, Nothing)
                    operacionesBD.CambiarEstado(m_respuesta.hostIdentifier, ConfigurationManager.AppSettings("BD"), ESTADO_REALCE.ST_OK.ToString() + "Notificacion", True)
                    operacionesBD.GuardarLogXNombreConectionString(10, m_IdTransaccion, 1, "OperarCMS.Procesos.vb - EnviarNotificaciones  ",
                                                       ConfigurationManager.AppSettings("BD"))
                    m_IdTransaccion = 0
                Catch ex As Exception
                    'reintentos Proceso 10- Notificar Proces
                    operacionesBD.CambiarEstado(m_respuesta.hostIdentifier, ConfigurationManager.AppSettings("BD"), ESTADO_REALCE.ST_ERROR.ToString() + "Notificacion", True)
                    Dim serializar As String = operarXML.Serializar(m_respuesta)
                    operacionesBD.InsertarProceso(m_respuesta.Transaccion, ConfigurationManager.AppSettings("BD"), serializar, 10)
                    Throw ex
                End Try
            End While

        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(10, m_IdTransaccion, 2, "OperarCMS.Procesos.vb - EnviarNotificaciones : " + ex.Message,
                                                         ConfigurationManager.AppSettings("BD"))
        Finally
            Me.ReintentosNotificacion()
            RaiseEvent TerminoProceso(Me, tiempoProceso)
        End Try
    End Sub

#End Region

#Region "Metodos Privados"

    ''' <summary>
    ''' Metodo encargado de analizar la respuesta de la peticion enviada al CMS
    ''' </summary>
    ''' <param name="respuesta">Objeto respuesta retornada por el adaptador</param>
    ''' <param name="m_Cliente">Cliente que realia la peticion</param>
    ''' <param name="idTransaccion">Identificador unico de la transccion</param>
    ''' <remarks>AERM 25/03/2015</remarks>
    Private Sub AnalizarRespuesta(respuesta As RespuestaWS, m_Cliente As Peticion, idTransaccion As Integer)
        Try
            ''Validamos si la respuesta es correcta
            If respuesta.codigo = 0 Then
                Select Case m_Cliente.codigoNovedad
                    ''Creacion Cliente
                    'Case "01"
                    '    respuesta.descripcion = "Creación Cliente exitoso- Num. Autorizacion" + respuesta.numAutorizacion
                    '    Me.NotificacionCliente(respuesta, m_Cliente, idTransaccion)
                    '    ''Reexpedicion
                    'Case "02"
                    '    respuesta.descripcion = "Reexpedición Cliente exitoso" + respuesta.descripcion
                    '    Me.NotificacionCliente(respuesta, m_Cliente, idTransaccion)
                    ''Realce
                    Case "03"
                        Me.EnviarImpresion(respuesta, m_Cliente, idTransaccion)
                        'Case Else
                        '    respuesta.descripcion = "Operacion NO Contemplada exitoso" + respuesta.descripcion
                        '    Me.NotificacionCliente(respuesta, m_Cliente, idTransaccion)
                End Select
            Else
                Throw New Exception("Respuesta incorrecta por el CMS Externo -Procesos.vb-AnalizarRespuesta " + respuesta.descripcion)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de almacenar notificaciones al cliente
    ''' </summary>
    ''' <param name="respuesta">Respuesta de la accion realizada en el CMS</param>
    ''' <param name="m_Cliente">Cliente q realiza la peticion</param>
    ''' <param name="idTransaccion">Identificador unico de la transaccion</param>
    ''' <remarks>AERM 25/03/2015</remarks>
    Private Sub NotificacionCliente(respuesta As RespuestaWS, m_Cliente As Peticion, idTransaccion As Integer)
        Try
            Dim m_RespuestaNotificar As RespuestaDeviceStatus = New RespuestaDeviceStatus()
            m_RespuestaNotificar.Transaccion = idTransaccion
            m_RespuestaNotificar.hostIdentifier = idTransaccion
            m_RespuestaNotificar.TransaccionCliente = m_Cliente.idTransaccion
            m_RespuestaNotificar.Cliente = m_Cliente.cliente
            m_RespuestaNotificar.Mensaje = respuesta.descripcion
            operarCola.GuardarObjetoNotificacion(m_RespuestaNotificar, colaNotificacion)
            operacionesBD.CambiarEstado(idTransaccion, ConfigurationManager.AppSettings("BD"), m_RespuestaNotificar.Mensaje)
        Catch ex As Exception
            operacionesBD.CambiarEstado(idTransaccion, ConfigurationManager.AppSettings("BD"), "Procesos.vb- NotificacionCliente Error ingresar notificacion Cliente")
            Throw ex
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de almacenar informacion para imprimir
    ''' </summary>
    ''' <param name="respuesta">Respuesta a imprimir</param>
    ''' <param name="m_Cliente">CLietne q realiza la peticion</param>
    ''' <param name="idTransaccion">Identificador Unico de la transaccion</param>
    ''' <remarks>AERM 25/03/2015</remarks>
    Private Sub EnviarImpresion(respuesta As RespuestaWS, m_Cliente As Peticion, idTransaccion As Integer)
        Try
            Dim m_datosRealce As DatosCardWizard = New DatosCardWizard()
            m_datosRealce.ID_Transaccion = idTransaccion
            m_datosRealce.Parametros = respuesta.parametros
            ''Obtenemos el cardformatname deacuerdo a configuracion base de datos tabla ProductoXCW
            m_datosRealce.CardWizardName = operacionesBD.ObtenerCardFormatName(m_Cliente.cliente, m_Cliente.tipoProducto, ConfigurationManager.AppSettings("BD"))(0)
            ''Obtenemos el PCNAME deacuerdo a configuracion base de datos tabla Impresoras
            m_datosRealce.PCName = operacionesBD.ObtenerPCNAME(m_Cliente.printName, ConfigurationManager.AppSettings("BD"))
            If (String.IsNullOrEmpty(m_datosRealce.CardWizardName) Or String.IsNullOrEmpty(m_datosRealce.PCName)) Then
                Dim m_RespuestaNotificar As RespuestaDeviceStatus = New RespuestaDeviceStatus()
                m_RespuestaNotificar.Transaccion = idTransaccion
                m_RespuestaNotificar.hostIdentifier = idTransaccion
                m_RespuestaNotificar.TransaccionCliente = m_Cliente.idTransaccion
                m_RespuestaNotificar.Cliente = m_Cliente.cliente
                m_RespuestaNotificar.Mensaje = "No se encuentra PCName y/ó CardFormat para la transacción "
                operarCola.GuardarObjetoNotificacion(m_RespuestaNotificar, colaNotificacion)
                Throw New Exception("No se encuentra PCName y/ó CardFormat para la transacción " + m_datosRealce.ID_Transaccion.ToString() + "-")
            End If
            operarCola.GuardarObjetoCardWizard(m_datosRealce, colaRealce)
            operacionesBD.CambiarEstado(idTransaccion, ConfigurationManager.AppSettings("BD"), "Proximo a Imprimir")
        Catch ex As Exception
            Throw New Exception("Procesos.vb- EnviarImpresion Error ingresar la impresion cola " + ex.Message)
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de tomar los reintentos almacenados en B.D
    ''' </summary>
    ''' <remarks>AERM 27/03/2015</remarks>
    Private Sub ReintentosTomaClientes()
        Try
            Dim m_procesos As List(Of Reintentos) = operacionesBD.ObtnerNotificacion(11, ConfigurationManager.AppSettings("BD"))
            For Each m_proceso In m_procesos
                Try
                    Dim m_Cliente As Peticion = operarXML.Deserialize(Of Peticion)(m_proceso.Informacion)
                    Me.IniciarProceso(m_Cliente, m_proceso.IDTransaccion)
                    operacionesBD.EliminarProceso(m_proceso.IDProceso, ConfigurationManager.AppSettings("BD"))
                Catch ex As Exception
                    ''Aumentar el reintento
                    operacionesBD.AumentarIntentosXProceso(m_proceso.IDProceso, ConfigurationManager.AppSettings("BD"))
                End Try
            Next
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(11, 0, 2, "OperarCMS.Procesos.vb - ReintentosTomaClientes : " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de reintentar enviar la notificacion
    ''' </summary>
    ''' <remarks>AERM 27/03/2015</remarks>
    Private Sub ReintentosNotificacion()
        Try
            Dim m_procesos As List(Of Reintentos) = operacionesBD.ObtnerNotificacion(10, ConfigurationManager.AppSettings("BD"))
            For Each m_proceso In m_procesos
                Try
                    Dim m_respuesta As RespuestaDeviceStatus = operarXML.Deserialize(Of RespuestaDeviceStatus)(m_proceso.Informacion)
                    Dim m_procesar As IOperarcionesCliente
                    Dim valores As List(Of String) = operacionesBD.ObtenerDatosCliente(m_respuesta.hostIdentifier, ConfigurationManager.AppSettings("BD"))
                    If (valores.Count >= 2) Then
                        m_procesar = fabrica.RetornarCliente(valores(0))
                        m_procesar.NotificarCliente(m_respuesta, valores(1), Nothing)
                        operacionesBD.CambiarEstado(m_respuesta.hostIdentifier, ConfigurationManager.AppSettings("BD"), ESTADO_REALCE.ST_OK.ToString() + "Notificacion", True)
                        operacionesBD.EliminarProceso(m_proceso.IDProceso, ConfigurationManager.AppSettings("BD"))
                    Else
                        operacionesBD.AumentarIntentosXProceso(m_proceso.IDProceso, ConfigurationManager.AppSettings("BD"))
                    End If
                Catch ex As Exception
                    ''Aumentar el reintento
                    operacionesBD.AumentarIntentosXProceso(m_proceso.IDProceso, ConfigurationManager.AppSettings("BD"))
                    Throw ex
                End Try
            Next
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(10, 0, 2, "OperarCMS.Procesos.vb - ReintentosNotificacion : " + ex.Message,
                                                        ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado del proceso de inicial de obtencion del realce
    ''' </summary>
    ''' <param name="m_Cliente">Cliente con los parametros de la peticion</param>
    ''' <param name="m_IdTransaccion">Identificador de la transaccion</param>
    ''' <remarks>AERM 31/03/2015</remarks>
    Private Sub IniciarProceso(m_Cliente As Peticion, m_IdTransaccion As Integer)
        Dim m_respuesta As RespuestaWS = New RespuestaWS()
        Dim m_procesar As IOperarcionesCliente
        Try
            m_procesar = fabrica.RetornarCliente(m_Cliente.cliente)
            m_respuesta = m_procesar.ObtenerRealce(m_Cliente, m_IdTransaccion, Nothing)
            Me.AnalizarRespuesta(m_respuesta, m_Cliente, m_IdTransaccion)
        Catch ex As Exception
            operacionesBD.CambiarEstado(m_IdTransaccion, ConfigurationManager.AppSettings("BD"), ex.Message)
            Throw New Exception("IniciarProceso " + ex.Message)
        End Try
    End Sub

    Private Sub ProcesoXLineasArchivo(m_lineasArchivo As List(Of String), m_IdTransaccion As Integer)
        Try
            For Each linea As String In m_lineasArchivo
                Dim rnd As New Random()
                Dim m_Cliente As Peticion = New Peticion()
                m_Cliente.cliente = "ARCHIVO"
                m_Cliente.nombreLargo = linea.Trim()
                m_Cliente.codigoNovedad = "03"
                ' m_Cliente.idTransaccion = rnd.[Next](0, 1000).ToString() + "Prueba"
                m_Cliente.tipoProducto = cardWizardNameXArchivo
                m_Cliente.printName = operacionesBD.ObtenerValoresConfigurador("PCNAMEXArchivo", ConfigurationManager.AppSettings("BD"))
                Try
                    m_Cliente.idTransaccion = linea.Substring(0, 4)
                    m_IdTransaccion = operacionesBD.InsertarTransaccion("ARCHIVO", m_Cliente.idTransaccion, ConfigurationManager.AppSettings("BD"))
                    Me.IniciarProceso(m_Cliente, m_IdTransaccion)
                    ' Me.txtRespuesta.Text += String.Format("{0} {1}", linea, System.Environment.NewLine)
                Catch ex As Exception
                    'Reintentos
                    ' Dim serializar As String = operarXML.Serializar(m_Cliente)
                    ' operacionesBD.InsertarProceso(m_IdTransaccion, ConfigurationManager.AppSettings("BD"), serializar, 11)
                    operacionesBD.GuardarLogXNombreConectionString(11, m_IdTransaccion, 2, "OperarCMS.Procesos.vb - ProcesoXLineasArchivo - LeerLinea : " + ex.Message + " " + linea,
                                                          ConfigurationManager.AppSettings("BD"))
                End Try
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LecturaArchivos(m_Archivos As List(Of FileInfo))
        Dim m_operar As ICArchivos = New RetornarArchivo().IntanciarArchivo(extensionArchivo)
        For Each m_Archivo In m_Archivos
            Try

                Dim m_lineasArchivo As List(Of String) = m_operar.ObtenerInformacioArchivo(ubicacionArchivo, m_Archivo.Name)
                Dim m_NombreArchivo As String = Date.Now.ToString()
                m_NombreArchivo = m_NombreArchivo.Replace("/", "-")
                m_NombreArchivo = m_NombreArchivo.Replace(":", "-")
                m_operar.CopiarArchivo(ubicacionArchivo, m_Archivo.Name, archivoProcesado, m_NombreArchivo + "procesado")
                m_operar.EliminarArchivo(ubicacionArchivo + "\" + m_Archivo.Name)
                Me.ProcesoXLineasArchivo(m_lineasArchivo, 0)
            Catch ex As Exception
                operacionesBD.GuardarLogXNombreConectionString(11, 0, 2, "OperarCMS.Procesos.vb - LecturaArchivos : " + ex.Message + " " + m_Archivo.Name,
                                                         ConfigurationManager.AppSettings("BD"))
            End Try
        Next
    End Sub
#End Region

  

End Class
