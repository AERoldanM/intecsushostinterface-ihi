﻿Imports System.Xml
Imports System.IO
Imports System.Configuration
Imports Intecusus.OperarBD
Imports Intecsus.Entidades

''' <summary>
''' Clase encargada de convertir Respuestas de CW
''' </summary>
''' <remarks>AERm 02-02-2015</remarks>
Public Class Respuesta

    Dim operacionesBD As OperacionesBase = New OperacionesBase()
    Dim xpath As String
    Dim xn As XmlNode

    ''' <summary>
    ''' Metodo encargado de procesar la repsuesta de una session
    ''' </summary>
    ''' <param name="respuesta">Respuesta obtenida</param>
    ''' <returns>Retorna la nueva session</returns>
    ''' <remarks>AERM 02-02-2015</remarks>
    Public Function ProcesarSession(ByRef respuesta As String) As String
        Try
            Dim m_doc As XmlDocument = Me.ProcessResponse(respuesta)
            xpath = "//SessionID"
            xn = m_doc.SelectSingleNode(xpath)
            operacionesBD.GuardarLogXNombreConectionString(2, 0, 1, "Respuesta - ProcesarSession : " + xn.InnerText,
                                                          ConfigurationManager.AppSettings("BD"))
            Return xn.InnerText
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(2, 0, 2, "Respuesta - ProcesarSession : " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
        End Try

        Return String.Empty
    End Function

    ''' <summary>
    ''' Metodo encargado de procesar la respuesta de una solicitud de esta card device
    ''' </summary>
    ''' <param name="respuesta">Respuesta obtenida</param>
    ''' <returns>Retorna la nueva session</returns>
    ''' <remarks>AERM 10-02-2015</remarks>
    Public Function ProcesarEstadoCardDevice(ByRef respuesta As String) As RespuestaEstado
        Try
            Dim m_RespuestaEstado As RespuestaEstado = New RespuestaEstado()
            Dim m_doc As XmlDocument = Me.ProcessResponse(respuesta)
            Dim m_respuesta As RespuestaEstado = New RespuestaEstado()
            Dim m_Estado As Integer
            Dim m_Evento As Integer = 1
            xpath = "//DeviceStatus"
            xn = m_doc.SelectSingleNode(xpath)
            m_Estado = CInt("0" & xn.InnerText)

            Select Case m_Estado
                Case 1
                    m_respuesta.estado = ESTADO_REALCE.ST_OK
                    m_respuesta.status = "OK"
                Case 4
                    m_respuesta.estado = ESTADO_REALCE.ST_OCUPADA
                    m_respuesta.status = "BUSY"
                    m_Evento = 2
                Case 0, 2, 3, 5 'Impresora desconectada, apagada, no lista (Not Ready)
                    m_respuesta.estado = ESTADO_REALCE.ST_ERROR
                    m_respuesta.status = "ERROR"
                    m_Evento = 2
            End Select

            xpath = "//DeviceDescription"
            xn = m_doc.SelectSingleNode(xpath)
            m_respuesta.mensaje = xn.InnerText

            xpath = "//DeviceLocation"
            xn = m_doc.SelectSingleNode(xpath)
            m_respuesta.mensaje += "  " + xn.InnerText

            operacionesBD.GuardarLogXNombreConectionString(8, 0, m_Evento, "Respuesta - ProcesarEstadoCardDevice : " + m_respuesta.status + " " + m_respuesta.mensaje,
                                                          ConfigurationManager.AppSettings("BD"))
            Return m_respuesta
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(8, 0, 2, "Respuesta - ProcesarEstadoCardDevice : " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
        End Try

        Return Nothing
    End Function

    ''' <summary>
    ''' Metodo encargado de procesar un mensaje enviado por el servidor CW
    ''' </summary>
    ''' <param name="respuesta">Respuesta enviado por el sevirdor</param>
    ''' <returns>Respuesta el objeto RespuestaDeviceStatus  </returns>
    ''' <remarks>AERM 16/03/2015</remarks>
    Public Function ProcesarMensage(ByRef respuesta As String) As RespuestaDeviceStatus
        Dim m_respuesta As RespuestaDeviceStatus = New RespuestaDeviceStatus()
        m_respuesta.Transaccion = 0
        Try
            Dim m_doc As XmlDocument = Me.ProcessResponse(respuesta)
            m_respuesta.Estado = InterpretHostMessage(m_doc, m_respuesta.hostIdentifier, m_respuesta.Mensaje)

            'TODO cerar función que elimine el request y llamarla desde aquí si el estadoRealce = ST_ERROR_DELETE
            If m_respuesta.Estado = ESTADO_REALCE.ST_ERROR_DELETE Then

               
                xpath = "//RequestCounter"
                xn = m_doc.SelectSingleNode(xpath)
                m_respuesta.Transaccion = xn.InnerText

            End If

            operacionesBD.GuardarLogXNombreConectionString(7, m_respuesta.Transaccion, 1, "Respuesta - RespuestaDeviceStatus Msj del servidor: hostIdentifier: " + m_respuesta.hostIdentifier +
                                                           " estado=" + m_respuesta.Estado.ToString + " - " + m_respuesta.Mensaje, ConfigurationManager.AppSettings("BD"))
           


        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(7, m_respuesta.Transaccion, 2, "Respuesta - RespuestaDeviceStatus : " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
        End Try

        Return m_respuesta
    End Function

    ''' <summary>
    ''' Metodo encargado de procesar la respuesta paravalidar q no tenga error.
    ''' </summary>
    ''' <param name="respuesta">Respuesta obtenida</param>
    ''' <returns>Retorna si se cerro bien</returns>
    ''' <remarks>AERM 02-02-2015</remarks>
    Public Function ProcesarRespuestaValida(ByRef respuesta As String) As Boolean
        Try
            If Me.ProcessResponse(respuesta) Is Nothing Then
                Return False
            End If
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(2, 0, 2, "Respuesta - ProcesarRespuestaValida : " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
            Return False
        End Try

        Return True
    End Function

    ''' <summary>
    ''' Processes the xml response from the server and displays the proper information to the output boxes.
    ''' </summary>
    ''' <param name="sResponse">The response to process</param>
    Public Function ProcessResponse(ByRef sResponse As String) As XmlDocument
        ' process the server's response to our request

        Dim iReturnCode As Int32
        ' if no data, exit early
        If sResponse.Length = 0 Then Return Nothing

        ' Create an Xml document instance and load XML data.
        Dim doc As XmlDocument = New XmlDocument

        ' convert the string (sResponse) to a memory stream
        Dim buffer As New MemoryStream(System.Text.Encoding.ASCII.GetBytes(sResponse))
        Dim mMethodName As String

        Try
            ' now load the XML document
            doc.Load(buffer)

            ' get the method name
            xpath = "//METHOD"
            xn = doc.SelectSingleNode(xpath)
            mMethodName = xn.Attributes("name").Value

            ' get the return code element (but only if method name is not "EventMessage")
            If mMethodName = "EventMessage" Then
                iReturnCode = 0
            Else
                xpath = "//ReturnCode"
                xn = doc.SelectSingleNode(xpath)

                If TypeName(xn) = "Nothing" Then
                    ' return code element is missing
                    Throw New Exception("ERROR missing return code in response " + xn.InnerText)
                    Exit Function
                End If

                iReturnCode = CInt(xn.InnerText)
            End If

            Select Case iReturnCode
                Case 0
                    Return doc
                Case Else
                    Select Case mMethodName
                        Case "HostGetMessage"
                            Select Case iReturnCode
                                Case 14         ' no messages
                                    Return Nothing
                                Case Else
                                    Throw New Exception("Error returned from server: code=" & iReturnCode.ToString & ", for method=" & mMethodName & ".  See CWEServer.log for more information.")
                            End Select

                        Case Else
                            Throw New Exception("Error returned from server: code=" & iReturnCode.ToString & ", for method=" & mMethodName & ".  See CWEServer.log for more information.")

                    End Select
            End Select

        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(4, 0, 2, "OperacionesBaseCW - Respuesta : " + ex.Message,
                                                           ConfigurationManager.AppSettings("BD"))
        End Try

        Return Nothing
    End Function

#Region "Metodos Privados"

    ''' <summary>
    ''' Helper function for ProcessResponse().  This function decodes Host and Event messages.
    ''' </summary>
    ''' <param name="doc">The XmlDocument containing the message received from the server</param>
    ''' <returns>The readable string that describes the message</returns>
    Private Function InterpretHostMessage(ByRef doc As XmlDocument, ByRef hostIdentifier As String, ByRef mensaje As String) As Integer

        Dim xpath As String
        Dim xn As XmlNode
        Dim sMessage As String = String.Empty
        Dim sHostIdentifier As String = String.Empty
        Dim iMessageType As MSG_TO_CLIENT
        Dim sDataString As String
        Dim iDataInteger As Integer
        Dim sIdentifier As String
        Dim iEstadoRealce As Integer = 0
        Dim iRequestCounter As Integer

        Try
            ' get "HostIdentifier"
            xpath = "//HostIdentifier"
            xn = doc.SelectSingleNode(xpath)
            If TypeName(xn) <> "Nothing" Then
                sHostIdentifier = xn.InnerText
            End If

            ' get "MessageType"
            xpath = "//MessageType"
            xn = doc.SelectSingleNode(xpath)
            iMessageType = CInt("0" & xn.InnerText)

            ' get "DataString"
            xpath = "//DataString"
            xn = doc.SelectSingleNode(xpath)
            sDataString = GetNodeContents(xn)

            ' get "DataInteger"
            xpath = "//DataInteger"
            xn = doc.SelectSingleNode(xpath)
            iDataInteger = CInt("0" & xn.InnerText)

            ' get "RequestCounter"
            xpath = "//RequestCounter"
            xn = doc.SelectSingleNode(xpath)
            If TypeName(xn) <> "Nothing" Then
                iRequestCounter = CInt("0" & xn.InnerText)
            End If

            ' get "Identifier"
            xpath = "//Identifier"
            xn = doc.SelectSingleNode(xpath)
            If TypeName(xn) <> "Nothing" Then
                sIdentifier = xn.InnerText
            End If

            ' now interpret the codes...
            Select Case iMessageType
                Case MSG_TO_CLIENT.CPR_FINAL_STATUS
                    ' final status
                    sMessage = "Final status: "

                    Select Case iDataInteger
                        Case FINAL_STATUS.PQ
                            sMessage += "placed into Production Queue"
                            iEstadoRealce = ESTADO_REALCE.ST_EN_PROCESO

                        Case FINAL_STATUS.NO_ERROR
                            sMessage += "completed"
                            iEstadoRealce = ESTADO_REALCE.ST_OK

                        Case FINAL_STATUS.DELETED
                            sMessage += "deleted"
                            iEstadoRealce = ESTADO_REALCE.ST_ERROR

                        Case FINAL_STATUS.REPIN_FAILED
                            sMessage += "repin failed"
                            iEstadoRealce = ESTADO_REALCE.ST_ERROR

                        Case FINAL_STATUS.SYS_DELETED
                            sMessage += "deleted by system"
                            iEstadoRealce = ESTADO_REALCE.ST_ERROR

                        Case FINAL_STATUS.ERROR_DELETED
                            sMessage += "error deleted"
                            iEstadoRealce = ESTADO_REALCE.ST_ERROR

                        Case Else
                            sMessage += "final status unknown"
                            iEstadoRealce = ESTADO_REALCE.ST_ERROR


                    End Select

                Case MSG_TO_CLIENT.PIN_OFFSET
                    ' PIN Offset
                    sMessage = "PIN Offset is " & sDataString.ToString
                    iEstadoRealce = ESTADO_REALCE.ST_EN_PROCESO

                Case MSG_TO_CLIENT.MESSAGE
                    ' Message for client
                    sMessage = sDataString.ToString
                    iEstadoRealce = ESTADO_REALCE.ST_ERROR

                Case MSG_TO_CLIENT.PRODUCTION_ERROR
                    ' Production Error
                    sMessage = "Production error: code=" & iDataInteger.ToString & ", " & sDataString.ToString
                    iEstadoRealce = ESTADO_REALCE.ST_ERROR_DELETE

                    ' save the request counter
                    xpath = "//RequestCounter"
                    xn = doc.SelectSingleNode(xpath)
                    If TypeName(xn) <> "Nothing" Then
                        iRequestCounter = CInt(xn.InnerText)
                    End If

                Case MSG_TO_CLIENT.CPS_CHANGE, MSG_TO_CLIENT.OP_ASSISTANCE
                    Select Case iDataInteger
                        Case Is > 99
                            Dim MessageType As MSG_CPS
                            MessageType = iDataInteger

                            Select Case MessageType
                                Case MSG_CPS.CARD_FAILED
                                    sMessage = "Unable to produce card due to emboss/encode failure. Bad format or data?"

                                Case MSG_CPS.CARD_POSITIONED
                                    sMessage = "Card Positioned in card machine, ready for next command"

                                Case MSG_CPS.CARD_WARNING
                                    sMessage = "card was produced but may not be in output stacker. Could be inside machine."

                                Case MSG_CPS.INSERT
                                    sMessage = "Insert card"

                                Case MSG_CPS.INSERT_EXCEPTION
                                    sMessage = "Insert exception card"

                                Case MSG_CPS.INSERT_READ
                                    sMessage = "Insert card to read"

                                Case MSG_CPS.INSERT_REPIN
                                    sMessage = "Insert card to repin"

                                Case MSG_CPS.INSERT_RETOP
                                    sMessage = "Insert card to retop"

                                Case MSG_CPS.READY_FOR_SC
                                    sMessage = "Card Positioned in card machine, ready for Smart Card operation"

                                Case MSG_CPS.REPIN_ERROR_BAD
                                    sMessage = "financial calc or build failure during repin attempt"

                                Case MSG_CPS.REPIN_ERROR_MISMATCH
                                    sMessage = "Card Number/Exp Date not the same"

                                Case MSG_CPS.REPIN_ERROR_PIN
                                    sMessage = "invalid current PIN"

                                Case MSG_CPS.REPIN_ERROR_TRK
                                    sMessage = "unable to repin card due to missing or bad track data on card."

                                Case MSG_CPS.REPIN_NOT_NEW
                                    sMessage = "unable to repin card. not a new card.  (e.g. PIN offset not 0000)"

                                Case MSG_CPS.WRONG_CARD
                                    sMessage = "unable to encode/emboss card. wrong card for step 2."

                                Case Else
                                    sMessage = "invalid code, " & MessageType.ToString

                            End Select
                            iEstadoRealce = ESTADO_REALCE.ST_ERROR
                    End Select
                Case MSG_TO_CLIENT.CUSTOM_MESSAGE
                    sMessage = "CWCustom: id=" & iDataInteger.ToString & " message=" & sDataString
                    iEstadoRealce = ESTADO_REALCE.ST_ERROR

                Case Else
                    sMessage = "unknown message"
                    iEstadoRealce = ESTADO_REALCE.ST_ERROR

            End Select

            mensaje = sMessage
            hostIdentifier = sHostIdentifier

            Return iEstadoRealce

        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(7, 0, 2, "respuesta - InterpretHostMessage : " + ex.Message,
                                                               ConfigurationManager.AppSettings("BD"))
            Return ESTADO_REALCE.ST_ERROR
        End Try

    End Function

    ''' <summary>
    ''' This function returns the contents of the xmlNode (InnerText).  If the contents are encrypted we will decrypt them.
    ''' </summary>
    ''' <param name="node">The node to get the contents of.</param>
    ''' <returns>The decrypted inner text.</returns>
    Private Function GetNodeContents(ByVal node As Xml.XmlNode) As String

        Dim decodingNoError As Boolean
        Dim decodingMsg As String = ""
        Dim retVal As String = ""

        ' make sure the node exists
        If Not node Is Nothing Then

            ' get the contents of the node
            retVal = node.InnerText

            ' see if we need to decrypt the contents
            If LCase(SafeSelectAttributeText(node, "encoding")) = "base64" Then
                retVal = Base64UnicodeDecode(retVal, decodingNoError, decodingMsg)
                If decodingNoError = False Then
                    Throw New System.Exception("Error decoding base64 node contents.  Error message: " & decodingMsg)
                End If
            ElseIf LCase(SafeSelectAttributeText(node, "encoding")) = "base64net" Then
                retVal = Base64ASCIIDecode(retVal, decodingNoError, decodingMsg)
                If decodingNoError = False Then
                    Throw New System.Exception("Error decoding base64NET node contents.  Error message: " & decodingMsg)
                End If
            End If

        End If

        Return retVal

    End Function

    ''' <summary>
    ''' Safely retrieves the text value of an attribute from the inpputed sourceNode.
    ''' </summary>
    ''' <param name="sourceNode">The node to get the attribute from</param>
    ''' <param name="attrName">The name of the attribute to look for</param>
    ''' <returns>The inner text of the attribute</returns>
    Private Function SafeSelectAttributeText(ByVal sourceNode As Xml.XmlNode, ByVal attrName As String) As String

        Dim attr As Xml.XmlAttribute
        Dim retVal As String = ""

        If Not sourceNode Is Nothing Then
            attr = sourceNode.Attributes(attrName)
            If Not attr Is Nothing Then
                retVal = attr.InnerText
            End If
        End If

        Return retVal

    End Function

    ''' <summary>
    ''' Does base64 decoding on the string "sInput" and returns the result
    ''' </summary>
    ''' <param name="sInput">The string to decode</param>
    ''' <param name="noError">If we run into an error this argument will be set to False</param>
    ''' <param name="errorMessage">If noError is returned False this argument will hold the error description</param>
    ''' <returns>The decoded string</returns>
    Private Function Base64UnicodeDecode(ByVal sInput As String, ByRef noError As Boolean, ByRef errorMessage As String) As String

        ' Convert the Base64 UUEncoded input into binary output.
        Dim binaryData() As Byte
        Dim sb As New System.Text.StringBuilder

        Try
            noError = True
            errorMessage = String.Empty

            ' convert the data
            binaryData = System.Convert.FromBase64String(sInput)

            ' return the decoded data to caller
            Return System.Text.Encoding.Unicode.GetString(binaryData)

        Catch exp As System.ArgumentNullException
            errorMessage = "Base 64 string is null."
            noError = False
            Return String.Empty

        Catch exp As System.FormatException
            errorMessage = "Base 64 length is not 4 or is not an even multiple of 4."
            noError = False
            Return String.Empty

        End Try

    End Function

    ''' <summary>
    ''' Does base64 decoding on the string "sInput" and returns the result
    ''' </summary>
    ''' <param name="sInput">The string to decode</param>
    ''' <param name="noError">If we run into an error this argument will be set to False</param>
    ''' <param name="errorMessage">If noError is returned False this argument will hold the error description</param>
    ''' <returns>The decoded string</returns>
    Private Function Base64ASCIIDecode(ByVal sInput As String, ByRef noError As Boolean, ByRef errorMessage As String) As String

        ' Convert the Base64 UUEncoded input into binary output.
        Dim binaryData() As Byte
        Dim sb As New System.Text.StringBuilder

        Try
            noError = True
            errorMessage = String.Empty

            ' convert the data
            binaryData = System.Convert.FromBase64String(sInput)

            ' return the decoded data to caller
            Return System.Text.Encoding.ASCII.GetString(binaryData)

        Catch exp As System.ArgumentNullException
            errorMessage = "Base 64 string is null."
            noError = False
            Return String.Empty

        Catch exp As System.FormatException
            errorMessage = "Base 64 length is not 4 or is not an even multiple of 4."
            noError = False
            Return String.Empty

        End Try

    End Function

#End Region

End Class
