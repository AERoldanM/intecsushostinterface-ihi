﻿Imports Intecsus.Entidades
Imports System.Configuration
Imports Intecusus.OperarBD
Imports System.Xml
Imports Intecsus.OperarXML

Public Class AdaptadorASNET
#Region "variables"

    Private operacionesBD As OperacionesBase = New OperacionesBase()
    Private gestorParametros As GeneradorParametros = New GeneradorParametros()
    Private track1 As String = operacionesBD.ObtenerValoresConfigurador("Track1", ConfigurationManager.AppSettings("BD"))
    Private track2 As String = operacionesBD.ObtenerValoresConfigurador("Track2", ConfigurationManager.AppSettings("BD"))
#End Region

#Region "Operaciones de Conversion"

    ''' <summary>
    ''' Metodo encargado de convertir el objeto aplicacion al obejto del cliente
    ''' </summary>
    ''' <param name="peticion">Peticion a convertir</param>
    ''' <param name="transaccion">Identificador de la transaccion</param>
    ''' <returns>Retorna obejto convertido</returns>
    ''' <remarks>AERM 24/03/2015</remarks>
    Public Function ConvertirClienteTOServicio(ByVal peticion As Peticion, transaccion As Integer) As ServicioASNET.solicitarEmisionTarjetasRequest
        Dim m_solicitud As ServicioASNET.solicitarEmisionTarjetasRequest = New ServicioASNET.solicitarEmisionTarjetasRequest()
        Try
            m_solicitud.solicitudEmisionTarjetas = New ServicioASNET.SolicitudEmisionTarjetasDTO()
            m_solicitud.solicitudEmisionTarjetas.numeroProducto = peticion.numeroProducto
            m_solicitud.solicitudEmisionTarjetas.numeroTarjetaAsignado = peticion.numeroTarjeta
            m_solicitud.solicitudEmisionTarjetas.tipoPersona = peticion.tipoPersona
            m_solicitud.solicitudEmisionTarjetas.tipoIdentificacion = peticion.tipoIdentificacion
            m_solicitud.solicitudEmisionTarjetas.numeroIdentificacion = peticion.numeroIdentificacion
            m_solicitud.solicitudEmisionTarjetas.primerApellido = peticion.primerApellido
            m_solicitud.solicitudEmisionTarjetas.segundoApellido = peticion.segundoApellido
            m_solicitud.solicitudEmisionTarjetas.direccionResidencialLinea_1 = peticion.direccionResidencia1
            m_solicitud.solicitudEmisionTarjetas.direccionResidencialLinea_2 = peticion.direccionResidencia2
            m_solicitud.solicitudEmisionTarjetas.primerNombre = peticion.primerNombre
            m_solicitud.solicitudEmisionTarjetas.segundoNombre = peticion.segundoNombre

            m_solicitud.solicitudEmisionTarjetas.nombreRealce = peticion.nombreRealce
            m_solicitud.solicitudEmisionTarjetas.nombreLargo = peticion.nombreLargo
            m_solicitud.solicitudEmisionTarjetas.fechaNacimiento = peticion.fechaNacimiento
            m_solicitud.solicitudEmisionTarjetas.sexo = peticion.sexo
            m_solicitud.solicitudEmisionTarjetas.estadoCivil = peticion.estadoCivil
            m_solicitud.solicitudEmisionTarjetas.codigoDepartamentoResidencial = peticion.codigoDepartResidencia
            m_solicitud.solicitudEmisionTarjetas.codigoCiudadResidencial = peticion.codigoCiudadResisdencia
            m_solicitud.solicitudEmisionTarjetas.zonaPostalResidencial = peticion.zonaPostal

            m_solicitud.solicitudEmisionTarjetas.oficinaRadicacionSolicitud = peticion.oficinaRadicacion
            m_solicitud.solicitudEmisionTarjetas.telefonoResidencia = peticion.telResidencia
            m_solicitud.solicitudEmisionTarjetas.telefonoOficina = peticion.telOficina
            m_solicitud.solicitudEmisionTarjetas.afinidadTarjeta = peticion.afinidadTarjeta
            m_solicitud.solicitudEmisionTarjetas.cupoAsignado = peticion.cupoAsignado
            m_solicitud.solicitudEmisionTarjetas.fechaSolicitud = peticion.fechaSolicitud
            m_solicitud.solicitudEmisionTarjetas.tipoSolicitud = peticion.tipoSolicitud
            m_solicitud.solicitudEmisionTarjetas.grupoManejo = peticion.manejoCuotas
            m_solicitud.solicitudEmisionTarjetas.tipoTarjeta = peticion.tipoTarjeta
            m_solicitud.solicitudEmisionTarjetas.codigoVendedor = peticion.codigoVendedor
            m_solicitud.solicitudEmisionTarjetas.numeroSolicitud = peticion.numeroSolicitud
            m_solicitud.solicitudEmisionTarjetas.tarjetaAnterior = peticion.tarjetaAnterior
            m_solicitud.solicitudEmisionTarjetas.codigoPuntoDistribucion = peticion.codigoPuntoDistribucion
            m_solicitud.solicitudEmisionTarjetas.convenioMercadeo = peticion.codigoMercadeo
            m_solicitud.solicitudEmisionTarjetas.tipoCuenta = peticion.tipoCuenta
            m_solicitud.solicitudEmisionTarjetas.valorCuotaFija = peticion.valorCuotaFija
            m_solicitud.solicitudEmisionTarjetas.tipoIdAmparador = peticion.tipoIdAmparador
            m_solicitud.solicitudEmisionTarjetas.numeroIdAmparador = peticion.numeroIDAmparador

            m_solicitud.solicitudEmisionTarjetas.oficinaDistribucionPlastico = peticion.oficinaDistribucion
            m_solicitud.solicitudEmisionTarjetas.indicativo = peticion.indicativo
            m_solicitud.solicitudEmisionTarjetas.correoElectronico = peticion.correoElectronico
            m_solicitud.solicitudEmisionTarjetas.actividadEconomica = peticion.actividadEconomica
            m_solicitud.solicitudEmisionTarjetas.origenIngresos = peticion.origenIngresos
            m_solicitud.solicitudEmisionTarjetas.codigoBarras = peticion.codigoBarras
            m_solicitud.solicitudEmisionTarjetas.indicadorDebitoAutomatico = peticion.idenDebitoAut
            m_solicitud.solicitudEmisionTarjetas.codigoBancoDebitoAutomatico = peticion.codBancoDebito
            m_solicitud.solicitudEmisionTarjetas.numeroCuentaDebitoAutomatico = peticion.numCuentaDebito
            m_solicitud.solicitudEmisionTarjetas.tipoCuentaDebitoAutomatico = peticion.tipoCuentaDebito
            m_solicitud.solicitudEmisionTarjetas.indicadorCesacionCausacion = peticion.indicadorCesacion
            m_solicitud.solicitudEmisionTarjetas.fechaCesacionCausacion = peticion.fechaCesacion
            m_solicitud.solicitudEmisionTarjetas.cicloFacturacion = peticion.cicloFacturacion
            m_solicitud.solicitudEmisionTarjetas.bin = peticion.BIN
            m_solicitud.solicitudEmisionTarjetas.nit = peticion.NIT
            m_solicitud.solicitudEmisionTarjetas.subtipo = peticion.subtipo
            m_solicitud.solicitudEmisionTarjetas.codigoCompensacion = peticion.codigoCompensacion
            m_solicitud.solicitudEmisionTarjetas.codigoNovedad = peticion.codigoNovedad

            m_solicitud.solicitudEmisionTarjetas.direccionCorrespondenciaLinea_1 = peticion.direccionCorrespondenciaLinea1
            m_solicitud.solicitudEmisionTarjetas.direccionCorrespondenciaLinea_2 = peticion.direccionCorrespondenciaLinea2
            m_solicitud.solicitudEmisionTarjetas.codigoDepartamentoCorrespondencia = peticion.codigoDepartamentoDirCorrespondencia
            m_solicitud.solicitudEmisionTarjetas.codigoCiudadCorrespondencia = peticion.codigoCiudadDirCorrespondencia
            m_solicitud.solicitudEmisionTarjetas.zonaPostalCorrespondencia = peticion.zonaPostalDirCorres
            m_solicitud.solicitudEmisionTarjetas.codigoProceso = peticion.codigoProceso
            m_solicitud.solicitudEmisionTarjetas.estadoRegistroRecibido = peticion.estadoRegistroRecibido
            m_solicitud.solicitudEmisionTarjetas.filler_1 = peticion.filler1
            m_solicitud.solicitudEmisionTarjetas.filler_2 = peticion.filler2
            m_solicitud.solicitudEmisionTarjetas.filler_3 = peticion.filler3
            m_solicitud.solicitudEmisionTarjetas.filler_4 = peticion.filler4
            operacionesBD.GuardarLogXNombreConectionString(12, transaccion, 1, "AdaptadorASNET - ConvertirClienteTOServicio ",
                                                          ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(12, transaccion, 2, "AdaptadorASNET - ConvertirClienteTOServicio " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
        End Try
        Return m_solicitud
    End Function

    ''' <summary>
    ''' Metodo encargado de convertir respuesta externa a objeto de las entidades
    ''' </summary>
    ''' <param name="respuesta">Respuesta externa</param>
    ''' <param name="transaccion">Identificador de la transaccion</param>
    ''' <param name="peticion"></param>
    ''' <returns></returns>
    ''' <remarks>AERM 24/03/2015</remarks>
    Public Function RetornarRespuestaToService(respuesta As ServicioASNET.solicitarEmisionTarjetasResponse, transaccion As Integer, peticion As Peticion, varInicio As ConfInicio) As RespuestaWS
        Dim m_RespuestaWS As RespuestaWS = New RespuestaWS
        Try
            m_RespuestaWS.codigo = respuesta.return.codigoRespuesta
            m_RespuestaWS.descripcion = respuesta.return.descripcionRespuesta
            m_RespuestaWS.numAutorizacion = respuesta.return.numeroAutorizacion
            Me.GuardarRespuesta(peticion, respuesta, transaccion)
            Dim objetoXML As String = varInicio.operarXML.Serializar(Of ServicioASNET.RespuestaRealceEmisionTarjetasDTO)(respuesta.return)
            m_RespuestaWS.parametros = Nothing
            If (CInt(respuesta.return.codigoRespuesta) = 0 And peticion.codigoNovedad = "03") Then
                m_RespuestaWS = gestorParametros.RetornarParametrosCliente(peticion, varInicio, transaccion)
                Dim m_xml As XmlDocument = New XmlDocument()
                m_xml.LoadXml(objetoXML)
                m_RespuestaWS.parametros = gestorParametros.ObtenerParametros(m_RespuestaWS, m_xml)
            End If
            operacionesBD.GuardarLogXNombreConectionString(13, transaccion, 1, "AdaptadorASNET - RetornarRespuestaToService " + objetoXML,
                                                        ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(13, transaccion, 2, "AdaptadorASNET - RetornarRespuestaToService " + ex.Message + " res" + respuesta.return.codigoRespuesta + respuesta.return.descripcionRespuesta,
                                                         ConfigurationManager.AppSettings("BD"))
        End Try
        Return m_RespuestaWS
    End Function

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="notificacion"></param>
    ''' <param name="idTransaccion"></param>
    ''' <remarks></remarks>
    Public Function RealizarRollBack(notificacion As RespuestaDeviceStatus, idTransaccion As String) As Peticion
        Dim m_Peticion As Peticion = New Peticion()
        Try
            Dim m_obtenerObjeto As GeneradorXML = New GeneradorXML()
            Dim m_RespuestaWS As RespuestaWSASNET = operacionesBD.ObtenerRespuestaRollBack(notificacion.hostIdentifier, ConfigurationManager.AppSettings("BD"))
            m_Peticion = m_obtenerObjeto.Deserialize(Of Peticion)(m_RespuestaWS.Peticion)
            m_Peticion.codigoNovedad = "04" ''Rollback codigo
            m_Peticion.codigoProceso = m_RespuestaWS.Notifica
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(10, notificacion.hostIdentifier, 2, "OperarCMS.AdaptadorASNET.vb - RealizarRollBack : " + ex.Message,
                                                        ConfigurationManager.AppSettings("BD"))
        End Try

        Return m_Peticion
    End Function

#End Region

#Region "Operadores Privados"

    ''' <summary>
    ''' Metodo encargado en convertir la informacion ASNET a parametros entendibles por CW
    ''' </summary>
    ''' <param name="respuesta">Obejto respuesta ASNET</param>
    ''' <returns>Retorna lista de parametros para CW</returns>
    ''' <remarks>AERM 26/03/2015</remarks>
    Private Function ConvertirTOParamentros(respuesta As ServicioASNET.solicitarEmisionTarjetasResponse) As List(Of ParametrosXML)
        Dim parametros As List(Of ParametrosXML) = New List(Of ParametrosXML)
        Try
            'Dim parametro As ParametrosXML = New ParametrosXML()
            'parametro.Nombre = "DATAITEM"
            'parametro.NameCard = "@PAN@"
            'parametro.Typo = "string"
            'parametro.Valor = "4738790000549711" 'respuesta.return.numeroTarjeta
            ' parametro.Codificar = True
            'parametros.Add(parametro)

            'parametro = New ParametrosXML()
            'parametro.Nombre = "DATAITEM"
            'parametro.NameCard = "@CardHolderName@"
            'parametro.Typo = "string"
            'parametro.Valor = "Pedro A Gomez R" 'respuesta.return.nombreCompleto
            'parametros.Add(parametro)

            'parametro = New ParametrosXML()
            'parametro.Nombre = "DATAITEM"
            'parametro.NameCard = "@MemberSince@"
            'parametro.Typo = "string"
            'Dim fecha As DateTime = DateTime.Now()
            'parametro.Valor = fecha.ToString("MM/yy")
            'parametros.Add(parametro)

            'parametro = New ParametrosXML()
            'parametro.Nombre = "DATAITEM"
            'parametro.NameCard = "@ExpirationDate@"
            'parametro.Typo = "string"
            'parametro.Valor = "08/17"
            'parametro.Codificar = True
            'parametros.Add(parametro)

            'parametro = New ParametrosXML()
            'parametro.Nombre = "DATAITEM"
            'parametro.NameCard = "@PIN@"
            'parametro.Typo = "string"
            'parametro.Valor = "0000"
            'parametros.Add(parametro)

            'parametro = New ParametrosXML()
            'parametro.Nombre = "DATAITEM"
            'parametro.NameCard = "@CVV2@"
            'parametro.Typo = "string"
            'parametro.Valor = "244" 'respuesta.return.trackII.cvc1
            'parametros.Add(parametro)
            '' Track1
            'parametro = New ParametrosXML()
            'parametro.Nombre = "DATAITEM"
            'parametro.NameCard = "@TRACK1@"
            'parametro.Typo = "string"
            'parametro.Valor = "%B4738790000549711^VISA TEST CARD^1708201122250000000000970000000?"
            'respuesta.return.trackI.caracterInicio + respuesta.return.trackI.formato +
            'respuesta.return.trackI.pan(+respuesta.return.trackI.caracterA + respuesta.return.trackI.beneficiario +
            'respuesta.return.trackI.caracterB + respuesta.return.trackI.fechaVencimiento + respuesta.return.trackI.caracteresControl +
            'respuesta.return.trackI.caracterC + respuesta.return.trackI.separador)
            'parametros.Add(parametro)

            'parametro = New ParametrosXML()
            'parametro.Nombre = "DATAITEM"
            'parametro.NameCard = "@TRACK2@"
            'parametro.Typo = "string"
            'parametro.Valor = ";4738790000549711=17082011222597000000?"
            'parametros.Add(parametro)

            Dim parametro As ParametrosXML = New ParametrosXML()
            parametro.Nombre = "DATAITEM"
            parametro.NameCard = "@PAN@"
            parametro.Typo = "string"
            parametro.Valor = respuesta.return.trackI.pan


            parametros.Add(parametro)

            parametro = New ParametrosXML()
            parametro.Nombre = "DATAITEM"
            parametro.NameCard = "@CardHolderName@"
            parametro.Typo = "string"
            parametro.Valor = respuesta.return.nombreCompleto
            parametros.Add(parametro)

            parametro = New ParametrosXML()
            parametro.Nombre = "DATAITEM"
            parametro.NameCard = "@MemberSince@"
            parametro.Typo = "string"
            Dim fecha As DateTime = DateTime.Now()
            parametro.Valor = fecha.ToString("MM/yy")
            parametros.Add(parametro)

            parametro = New ParametrosXML()
            parametro.Nombre = "DATAITEM"
            parametro.NameCard = "@ExpirationDate@"
            parametro.Typo = "string"
            parametro.Valor = "08/17"
            'parametro.Codificar = True
            parametros.Add(parametro)

            parametro = New ParametrosXML()
            parametro.Nombre = "DATAITEM"
            parametro.NameCard = "@PIN@"
            parametro.Typo = "string"
            parametro.Valor = "1234"
            parametros.Add(parametro)

            parametro = New ParametrosXML()
            parametro.Nombre = "DATAITEM"
            parametro.NameCard = "@CVV2@"
            parametro.Typo = "string"
            parametro.Valor = respuesta.return.trackII.cvc1
            parametros.Add(parametro)
            '' Track1
            parametro = New ParametrosXML()
            parametro.Nombre = "DATAITEM"
            parametro.NameCard = "@TRACK1@"
            parametro.Typo = "string"
            parametro.Valor = respuesta.return.trackI.caracterInicio + respuesta.return.trackI.formato +
                respuesta.return.trackI.pan + respuesta.return.trackI.caracterA + respuesta.return.trackI.beneficiario +
                respuesta.return.trackI.caracterB + respuesta.return.trackI.fechaVencimiento + respuesta.return.trackI.caracteresControl +
             respuesta.return.trackI.caracterC
            parametros.Add(parametro)
            parametro.Valor = parametro.Valor.Replace("[", "^")
            ''track2
            parametro = New ParametrosXML()
            parametro.Nombre = "DATAITEM"
            parametro.NameCard = "@TRACK2@"
            parametro.Typo = "string"
            parametro.Valor = respuesta.return.trackI.separador + respuesta.return.trackII.pan + respuesta.return.trackII.separadorCampo + respuesta.return.trackII.fechaVencimiento +
                respuesta.return.trackII.codigoServicio + respuesta.return.trackII.pvki + respuesta.return.trackII.pvv +
                respuesta.return.trackII.cvc1 + respuesta.return.trackII.separador
            parametros.Add(parametro)

            'parametros.AddRange(Me.ObtenerElementos(respuesta.return.trackI))
            'parametros.AddRange(Me.ObtenerElementosII(respuesta.return.trackII))
        Catch ex As Exception
            Throw New Exception("Parametros  vacios error servicio " + ex.Message)
        End Try

        Return parametros
    End Function

    ''' <summary>
    ''' Metodo encargado de retornar los parametros para el trackI
    ''' </summary>
    ''' <param name="respuestaTrackIDTO">Respuesta del servicio</param>
    ''' <returns>Retorna Lista con los objetos para agregar</returns>
    ''' <remarks>AERM 26/03/2015</remarks>
    Private Function ObtenerElementos(respuestaTrackIDTO As ServicioASNET.RespuestaTrackIDTO) As IEnumerable(Of ParametrosXML)
        Dim parametros As List(Of ParametrosXML) = New List(Of ParametrosXML)
        Try

            Dim parametro As ParametrosXML = New ParametrosXML()
            parametro.Nombre = "DATAITEM"
            parametro.NameCard = "@ExpirationDate@"
            parametro.Typo = "string"
            parametro.Valor = respuestaTrackIDTO.fechaVencimiento
            parametros.Add(parametro)

        Catch ex As Exception
            Throw ex
        End Try

        Return parametros
    End Function

    ''' <summary>
    ''' Metodo encargado de retornar los parametros para el trackII
    ''' </summary>
    ''' <param name="respuestaTrackIIDTO">Respuesta del servicio</param>
    ''' <returns>Retorna Lista con los objetos para agregar</returns>
    ''' <remarks>AERM 26/03/2015</remarks>
    Private Function ObtenerElementosII(respuestaTrackIIDTO As ServicioASNET.RespuestaTrackIIDTO) As IEnumerable(Of ParametrosXML)
        Dim parametros As List(Of ParametrosXML) = New List(Of ParametrosXML)
        Try

            Dim parametro As ParametrosXML = New ParametrosXML()
            parametro.Nombre = "DATAITEM"
            parametro.NameCard = "@ServiceCode@"
            parametro.Typo = "string"
            parametro.Valor = respuestaTrackIIDTO.codigoServicio
            parametros.Add(parametro)

            parametro = New ParametrosXML()
            parametro.Nombre = "DATAITEM"
            parametro.NameCard = "@PVV@"
            parametro.Typo = "string"
            parametro.Valor = respuestaTrackIIDTO.pvv
            parametros.Add(parametro)

            parametro = New ParametrosXML()
            parametro.Nombre = "DATAITEM"
            parametro.NameCard = "@CVV@"
            parametro.Typo = "string"
            parametro.Valor = respuestaTrackIIDTO.cvc1
            parametros.Add(parametro)

            parametro = New ParametrosXML()
            parametro.Nombre = "DATAITEM"
            parametro.NameCard = "@CVV2@"
            parametro.Typo = "string"
            parametro.Valor = respuestaTrackIIDTO.chipCVC
            parametros.Add(parametro)

        Catch ex As Exception
            Throw ex
        End Try

        Return parametros
    End Function

#End Region

    ''' <summary>
    ''' Metodo encargado de almacenar la respuesta de un servicio (Tanto transaccional como de log
    ''' </summary>
    ''' <param name="peticion">Peticion enviada</param>
    ''' <param name="respuesta">Respuesta recibida</param>
    ''' <remarks>AERM 14/08/2015</remarks>
    Private Sub GuardarRespuesta(peticion As Peticion, respuesta As ServicioASNET.solicitarEmisionTarjetasResponse, transaccion As Integer)
        Try
            Dim operarXML As GeneradorXML = New GeneradorXML()
            Dim m_Peticion As String = operarXML.Serializar(Of Peticion)(peticion)
            Dim m_RespuestaFinal As String
            Dim m_Respuesta As Respuesta = New Respuesta()
            m_Respuesta.codigo = respuesta.return.codigoRespuesta
            m_Respuesta.mensaje = respuesta.return.descripcionRespuesta
            m_Respuesta.numeroAutorizacion = If(respuesta.return.numeroAutorizacion = Nothing, "No existe", respuesta.return.numeroAutorizacion)
            m_RespuestaFinal = operarXML.Serializar(Of Respuesta)(m_Respuesta)
            operacionesBD.GuardarRespuestaWS(peticion, m_Peticion, m_RespuestaFinal, m_Respuesta, ConfigurationManager.AppSettings("BD"))
            operacionesBD.GuardarRespuestaPeticion(transaccion, m_Peticion, m_Respuesta.numeroAutorizacion, ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class
