﻿Public Enum MSG_TO_CLIENT
    CPS_CHANGE          ' 0 card production request status change
    CMS_CHANGE          ' 1 CMC Device/card machine status change
    MESSAGE             ' 2 user Message
    TRACK_DATA          ' 3 Mag Stripe track data
    PIN_OFFSET          ' 4 "DataString" contains the PIN offset for the new or repinned card
    CMC_CHANGE          ' 5 CMC Controller device status change (only for Enterprise Server)
    CMCQ_CHANGE         ' 6 CMC Device queue count change (only for Enterprise Server)
    PQ_CHANGE           ' 7 Production queue change (only for Enterprise Server)
    CMCD_CHANGE         ' 8 CMC Device status change (only for Enterprise. used by Admin component only.)
    CPR_FINAL_STATUS    ' 9 Final Status for card request (only for Enterprise)
    CUSTOM_MESSAGE      '10 message from CWcustom component
    OP_ASSISTANCE       '11 operator assistance message
    CARD_NUMBER         '12 card number (PAN) (if auto assigned by server)
    SUPER_CMC_CHANGE    '13 Super CMC Controller device status change (only for Enterprise Server)
    PRODUCTION_ERROR    '14 Production error (used for HOST sessions)
End Enum

Public Enum FINAL_STATUS
    NO_ERROR       ' 0 completed with no errors (deleted from queue)
    REPIN_FAILED   ' 1 Repin not completed. due to error(s)
    DELETED        ' 2 request was deleted by client before completion
    SYS_DELETED    ' 3 request was deleted by system before completion
    PQ             ' 4 production request placed into Production Queue for later production
    ERROR_DELETED  ' 5 Production request encountered errors and was not completed.
    '   The request was deleted by the system. iError indicates the error that occurred.
    ERROR_IN_Q     ' 6 Production request encountered errors and was not completed.
    '   The request remains in the queue. iError indicates the error that occurred.
End Enum

Public Enum ESTADO_REALCE
    ST_OK           ' 0 Realce completo con exito
    ST_EN_PROCESO   ' 1 Realce que aun no se ha finalizado, esta en proceso
    ST_OCUPADA      ' 2 El estado de la impresora es ocupada, se encola de nuevo
    ST_ERROR        ' 3 Realce sin exito debido a un error
    ST_ERROR_DELETE ' 4 Realce sin exito debido a ub error y requiere borrar el request
    ST_NOPROCESO    ' 5 Que se debe buscar un estado mas con otro numero en la BD
    ST_ROLLBACK     ' 6 QUe no realiza eliminacion tarjeta pero si rollback y notifica
End Enum

' requests for operator assistance or other client operation
' used with MSG_CPS_CHANGE
Public Enum MSG_CPS
    INSERT_EXCEPTION = 100 ' Insert Exception Card
    INSERT_READ = 101      ' Insert Card to Read
    INSERT = 102           ' Insert Card (generic)
    INSERT_REPIN = 103     ' Insert Card to RePIN
    REPIN_ERROR_MISMATCH = 104 ' Card Number/Exp Date not the same
    REPIN_ERROR_PIN = 105  ' invalid current PIN
    REPIN_ERROR_BAD = 106  ' financial calc or build failure during repin attempt
    CARD_POSITIONED = 107  ' Card Positioned in card machine, ready for next command
    INSERT_RETOP = 108     ' Insert Card to Retop
    CARD_FAILED = 109      ' unable to produce card due to emboss/encode failure. bad format or data?
    CARD_WARNING = 110     ' card was produced but may not be in output stacker. Could be inside machine.
    REPIN_ERROR_TRK = 111  ' unable to repin card due to missing or bad track data on card.
    READY_FOR_SC = 112     ' Card Positioned in card machine, ready for Smart Card operation
    REPIN_NOT_NEW = 113    ' unable to repin card. not a new card.  (e.g. PIN offset not 0000)
    WRONG_CARD = 114       ' unable to encode/emboss card. wrong card for step 2.
End Enum