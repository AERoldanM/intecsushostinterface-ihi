﻿''' <summary>
''' Clase que representa el objeto a enviar a cardwizard
''' </summary>
''' <remarks>AERM 30-01-2015</remarks>
Public Class DatosCardWizard
    Private m_BIN As String
    Private m_CardHolderName As String
    Private m_CardHolderName2 As String
    Private m_CardHolderName3 As String
    Private m_CardHolderName4 As String
    Private m_CardHolderName5 As String
    Private m_CardNumber As String
    Private m_CVV As String
    Private m_CVV2 As String
    Private m_DISCRETIONARY_DATA_1 As String
    Private m_DISCRETIONARY_DATA_2 As String
    Private m_EffectiveDate As String
    Private m_ExpirationDate As String
    Private m_FirtsName As String
    Private m_FirtsName2 As String
    Private m_FirtsName3 As String
    Private m_IsoName As String
    Private m_IsoName2 As String
    Private m_IsoName3 As String
    Private m_Mail_Addr1 As String
    Private m_Mail_Addr2 As String
    Private m_Mail_Addr3 As String
    Private m_Mail_Addr4 As String
    Private m_Mail_Addr5 As String
    Private m_MemberSince As String
    Private m_New_Pin As String
    Private m_Nwe_Pin_OffSet As String
    Private m_NumberCards As String
    Private m_OptionalInformation As String
    Private m_Override_User_Name As String
    Private m_Override_User_Password As String
    Private m_Pan As String
    Private m_Pick_Made As String '' Y/N
    Private m_Pin_Offset As String
    Private m_PrefixNumber As String
    Private m_ProductionOptions As String
    Private m_PVV As String
    Private m_PVKI As String
    Private m_SEQ_Num As String
    Private m_ServiceCode As String
    Private m_Start_Card_Num As String
    Private m_SurName As String
    Private m_SurName2 As String
    Private m_SurName3 As String
    Private m_Title As String
    Private m_Title2 As String
    Private m_Title3 As String
    Private m_Track1 As String
    Private m_Track2 As String
    Private m_Track3 As String
    Private m_PCName As String
    Private m_ID_Transaccion As Integer
    Private m_Tipo_Producto As String
    Private m_CardWizardName As String

    Public Property BIN() As String
        Get
            Return m_BIN
        End Get
        Set(value As String)
            m_BIN = value
        End Set
    End Property

    Public Property CardHolderName() As String
        Get
            Return m_CardHolderName
        End Get
        Set(value As String)
            m_CardHolderName = value
        End Set
    End Property

    Public Property CardHolderName2() As String
        Get
            Return m_CardHolderName2
        End Get
        Set(value As String)
            m_CardHolderName2 = value
        End Set
    End Property

    Public Property CardHolderName5() As String
        Get
            Return m_CardHolderName5
        End Get
        Set(value As String)
            m_CardHolderName5 = value
        End Set
    End Property
    Public Property CardHolderName4() As String
        Get
            Return m_CardHolderName4
        End Get
        Set(value As String)
            m_CardHolderName4 = value
        End Set
    End Property

    Public Property CardHolderName3() As String
        Get
            Return m_CardHolderName3
        End Get
        Set(value As String)
            m_CardHolderName3 = value
        End Set
    End Property

    Public Property CardNumber() As String
        Get
            Return m_CardNumber
        End Get
        Set(value As String)
            m_CardNumber = value
        End Set
    End Property

    Public Property CVV() As String
        Get
            Return m_CVV
        End Get
        Set(value As String)
            m_CVV = value
        End Set
    End Property


    Public Property CVV2() As String
        Get
            Return m_CVV2
        End Get
        Set(value As String)
            m_CVV2 = value
        End Set
    End Property

    Public Property DISCRETIONARY_DATA_1() As String
        Get
            Return m_DISCRETIONARY_DATA_1
        End Get
        Set(value As String)
            m_DISCRETIONARY_DATA_1 = value
        End Set
    End Property

    Public Property DISCRETIONARY_DATA_2() As String
        Get
            Return m_DISCRETIONARY_DATA_2
        End Get
        Set(value As String)
            m_DISCRETIONARY_DATA_2 = value
        End Set
    End Property

    Public Property EffectiveDate() As String
        Get
            Return m_EffectiveDate
        End Get
        Set(value As String)
            m_EffectiveDate = value
        End Set
    End Property

    Public Property ExpirationDate() As String
        Get
            Return m_ExpirationDate
        End Get
        Set(value As String)
            m_ExpirationDate = value
        End Set
    End Property

    Public Property FirtsName() As String
        Get
            Return m_FirtsName
        End Get
        Set(value As String)
            m_FirtsName = value
        End Set
    End Property

    Public Property FirtsName2() As String
        Get
            Return m_FirtsName2
        End Get
        Set(value As String)
            m_FirtsName2 = value
        End Set
    End Property

    Public Property FirtsName3() As String
        Get
            Return m_FirtsName3
        End Get
        Set(value As String)
            m_FirtsName3 = value
        End Set
    End Property

    Public Property IsoName() As String
        Get
            Return m_IsoName
        End Get
        Set(value As String)
            m_IsoName = value
        End Set
    End Property

    Public Property IsoName2() As String
        Get
            Return m_IsoName2
        End Get
        Set(value As String)
            m_IsoName2 = value
        End Set
    End Property

    Public Property IsoName3() As String
        Get
            Return m_IsoName3
        End Get
        Set(value As String)
            m_IsoName3 = value
        End Set
    End Property

    Public Property Mail_Addr1() As String
        Get
            Return m_Mail_Addr1
        End Get
        Set(value As String)
            m_Mail_Addr1 = value
        End Set
    End Property
    Public Property Mail_Addr2() As String
        Get
            Return m_Mail_Addr2
        End Get
        Set(value As String)
            m_Mail_Addr2 = value
        End Set
    End Property
    Public Property Mail_Addr3() As String
        Get
            Return m_Mail_Addr3
        End Get
        Set(value As String)
            m_Mail_Addr3 = value
        End Set
    End Property
    Public Property Mail_Add4() As String
        Get
            Return m_Mail_Addr4
        End Get
        Set(value As String)
            m_Mail_Addr4 = value
        End Set
    End Property
    Public Property Mail_Addr5() As String
        Get
            Return m_Mail_Addr5
        End Get
        Set(value As String)
            m_Mail_Addr5 = value
        End Set
    End Property
    Public Property MemberSince() As String
        Get
            Return m_MemberSince
        End Get
        Set(value As String)
            m_MemberSince = value
        End Set
    End Property
    Public Property New_Pin() As String
        Get
            Return m_New_Pin
        End Get
        Set(value As String)
            m_New_Pin = value
        End Set
    End Property
    Public Property Nwe_Pin_OffSet() As String
        Get
            Return m_Nwe_Pin_OffSet
        End Get
        Set(value As String)
            m_Nwe_Pin_OffSet = value
        End Set
    End Property
    Public Property NumberCards() As String
        Get
            Return m_NumberCards
        End Get
        Set(value As String)
            m_NumberCards = value
        End Set
    End Property
    Public Property OptionalInformation() As String
        Get
            Return m_OptionalInformation
        End Get
        Set(value As String)
            m_OptionalInformation = value
        End Set
    End Property
    Public Property Override_User_Name() As String
        Get
            Return m_Override_User_Name
        End Get
        Set(value As String)
            m_Override_User_Name = value
        End Set
    End Property
    Public Property Override_User_Password() As String
        Get
            Return m_Override_User_Password
        End Get
        Set(value As String)
            m_Override_User_Password = value
        End Set
    End Property
    Public Property Pan() As String
        Get
            Return m_Pan
        End Get
        Set(value As String)
            m_Pan = value
        End Set
    End Property
    Public Property Pick_Made() As String
        Get
            Return m_Pick_Made
        End Get
        Set(value As String)
            m_Pick_Made = value
        End Set
    End Property
    Public Property Pin_Offset() As String
        Get
            Return m_Pin_Offset
        End Get
        Set(value As String)
            m_Pin_Offset = value
        End Set
    End Property
    Public Property PrefixNumber() As String
        Get
            Return m_PrefixNumber
        End Get
        Set(value As String)
            m_PrefixNumber = value
        End Set
    End Property
    Public Property ProductionOptions() As String
        Get
            Return m_ProductionOptions
        End Get
        Set(value As String)
            m_ProductionOptions = value
        End Set
    End Property
    Public Property PVV() As String
        Get
            Return m_PVV
        End Get
        Set(value As String)
            m_PVV = value
        End Set
    End Property
    Public Property PVKI() As String
        Get
            Return m_PVKI
        End Get
        Set(value As String)
            m_PVKI = value
        End Set
    End Property
    Public Property SEQ_Num() As String
        Get
            Return m_SEQ_Num
        End Get
        Set(value As String)
            m_SEQ_Num = value
        End Set
    End Property
    Public Property ServiceCode() As String
        Get
            Return m_ServiceCode
        End Get
        Set(value As String)
            m_ServiceCode = value
        End Set
    End Property
    Public Property Start_Card_Num() As String
        Get
            Return m_Start_Card_Num
        End Get
        Set(value As String)
            m_Start_Card_Num = value
        End Set
    End Property
    Public Property SurName() As String
        Get
            Return m_SurName
        End Get
        Set(value As String)
            m_SurName = value
        End Set
    End Property
    Public Property SurName2() As String
        Get
            Return m_SurName2
        End Get
        Set(value As String)
            m_SurName2 = value
        End Set
    End Property
    Public Property SurName3() As String
        Get
            Return m_SurName3
        End Get
        Set(value As String)
            m_SurName3 = value
        End Set
    End Property
    Public Property Title() As String
        Get
            Return m_Title
        End Get
        Set(value As String)
            m_Title = value
        End Set
    End Property
    Public Property Title2() As String
        Get
            Return m_Title2
        End Get
        Set(value As String)
            m_Title2 = value
        End Set
    End Property
    Public Property Title3() As String
        Get
            Return m_Title3
        End Get
        Set(value As String)
            m_Title3 = value
        End Set
    End Property
    Public Property PCName() As String
        Get
            Return m_PCName
        End Get
        Set(value As String)
            m_PCName = value
        End Set
    End Property
    Public Property Tipo_Producto() As Integer
        Get
            Return m_Tipo_Producto
        End Get
        Set(value As Integer)
            m_Tipo_Producto = value
        End Set
    End Property
    Public Property CardWizardName() As Integer
        Get
            Return m_CardWizardName
        End Get
        Set(value As Integer)
            m_CardWizardName = value
        End Set
    End Property

End Class
