﻿''' <summary>
''' Clase que representa un valor en el parametro
''' </summary>
''' <remarks>AERM 17/09/2015</remarks>
Public Class Valor

    ''' <summary>
    ''' Convierte valor de 1214 a 14/12
    ''' </summary>
    Private m_FechaAsNet As Boolean = False
    ''' <summary>
    '''  Si es PCI debe aplciar el metodo de descifrar
    ''' </summary>
    Private m_MetodoPCI As Boolean = False

    ''' <summary>
    ''' valor que toma dentro del xml respuesta
    ''' </summary>
    Private m_ValorS As String

    Private m_Tamamio As Integer

    ''' <summary>
    ''' Digitos que se debe tomar a la derecha
    ''' </summary>
    Private m_ValoresDerecha As Integer

    Public Property ValoresDerecha() As Integer
        Get
            Return m_ValoresDerecha
        End Get
        Set(value As Integer)
            m_ValoresDerecha = value
        End Set
    End Property

    Public Property Tamamio() As Integer
        Get
            Return m_Tamamio
        End Get
        Set(value As Integer)
            m_Tamamio = value
        End Set
    End Property

    Public Property MetodoPCI() As Boolean
        Get
            Return m_MetodoPCI
        End Get
        Set(value As Boolean)
            m_MetodoPCI = value
        End Set
    End Property

    Public Property ValorS() As String
        Get
            Return m_ValorS
        End Get
        Set(value As String)
            m_ValorS = value
        End Set
    End Property

    Public Property FechaAsNet() As Boolean
        Get
            Return m_FechaAsNet
        End Get
        Set(value As Boolean)
            m_FechaAsNet = value
        End Set
    End Property
End Class
