﻿''' <summary>
''' Clase que representa el estado enviada por el servidor
''' </summary>
''' <remarks>AERM 10/02/2015</remarks>
Public Class RespuestaDeviceStatus
    Private m_Estado As ESTADO_REALCE
    Private m_EstadoF As Integer
    Private m_Transaccion As String
    Private m_Mensaje As String
    Private m_hostIdentifier As String
    Private m_TransaccionCliente As String
    Private m_Cliente As String
    Private m_TransaccionName As String
    Private m_MessageType As String
    Private m_DataInteger As String
    Private m_CodigoCliente As String
    Private m_IdChip As String
    Private m_IdTransaccion As Integer
    Public Property IdTransaccion() As Integer
        Get
            Return m_IdTransaccion
        End Get
        Set(ByVal value As Integer)
            m_IdTransaccion = value
        End Set
    End Property

    Public Property IdChip() As String
        Get
            Return m_IdChip
        End Get
        Set(value As String)
            m_IdChip = value
        End Set
    End Property


    Public Property EstadoF() As String
        Get
            Return m_EstadoF
        End Get
        Set(value As String)
            m_EstadoF = value
        End Set
    End Property

    Public Property CodigoCliente() As String
        Get
            Return m_CodigoCliente
        End Get
        Set(value As String)
            m_CodigoCliente = value
        End Set
    End Property

    Public Property MessageType() As String
        Get
            Return m_MessageType
        End Get
        Set(value As String)
            m_MessageType = value
        End Set
    End Property

    Public Property DataInteger() As String
        Get
            Return m_DataInteger
        End Get
        Set(value As String)
            m_DataInteger = value
        End Set
    End Property

    Public Property TransaccionName() As String
        Get
            Return m_TransaccionName
        End Get
        Set(value As String)
            m_TransaccionName = value
        End Set
    End Property

    Public Property Estado() As ESTADO_REALCE
        Get
            Return m_Estado
        End Get
        Set(value As ESTADO_REALCE)
            m_Estado = value
        End Set
    End Property

    Public Property TransaccionCliente() As String
        Get
            Return m_TransaccionCliente
        End Get
        Set(value As String)
            m_TransaccionCliente = value
        End Set
    End Property

    Public Property Cliente() As String
        Get
            Return m_Cliente
        End Get
        Set(value As String)
            m_Cliente = value
        End Set
    End Property

    Public Property Transaccion() As String
        Get
            Return m_Transaccion
        End Get
        Set(value As String)
            m_Transaccion = value
        End Set
    End Property

    Public Property Mensaje() As String
        Get
            Return m_Mensaje
        End Get
        Set(value As String)
            m_Mensaje = value
        End Set
    End Property

    ''' <summary>
    ''' Representa el mensaje de la peticion realizada
    ''' </summary>
    Public Property hostIdentifier() As String
        Get
            Return m_hostIdentifier
        End Get
        Set(value As String)
            m_hostIdentifier = value
        End Set
    End Property
End Class
