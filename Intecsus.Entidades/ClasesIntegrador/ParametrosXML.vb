﻿''' <summary>
''' Obejto q representa un valor a convertir en XML
''' </summary>
''' <remarks></remarks>
Public Class ParametrosXML

    ''' <summary>
    ''' Indica la cabecera del parametro para CW ejemplo DATAITEM
    ''' </summary>
    Private m_Nombre As String
    ''' <summary>
    ''' Indica el tipo parametro para CW ejemplo deberia ir String o Int
    ''' </summary>
    Private m_Typo As String
    ''' <summary>
    ''' Objeto que contiene los valores del parametro (Al final se unen)
    ''' </summary>
    Private m_Valor As List(Of Valor)
    ''' <summary>
    ''' Nmbre del campo que entiende CW ejemplo @PAN@
    ''' </summary>
    Private m_NameCard As String
    ''' <summary>
    ''' Si se debe enmascar el parametro cuando se almacene en la BD
    ''' </summary>
    Private m_Codificar As Boolean = False
    ''' <summary>
    ''' Digitos a la derecha q se debe en mascarar
    ''' </summary>
    Private m_Derecha As Integer
    ''' <summary>
    ''' Digitos a la izquierda q se debe enmascarar
    ''' </summary>
    Private m_Izquierda As Integer


    Public Property Nombre() As String
        Get
            Return m_Nombre
        End Get
        Set(value As String)
            m_Nombre = value
        End Set
    End Property

    Public Property Codificar() As Boolean
        Get
            Return m_Codificar
        End Get
        Set(value As Boolean)
            m_Codificar = value
        End Set
    End Property

    Public Property NameCard() As String
        Get
            Return m_NameCard
        End Get
        Set(value As String)
            m_NameCard = value
        End Set
    End Property

    Public Property Typo() As String
        Get
            Return m_Typo
        End Get
        Set(value As String)
            m_Typo = value
        End Set
    End Property

    Public Property Valor() As List(Of Valor)
        Get
            Return m_Valor
        End Get
        Set(value As List(Of Valor))
            m_Valor = value
        End Set
    End Property


    Public Property Derecha() As Integer
        Get
            Return m_Derecha
        End Get
        Set(value As Integer)
            m_Derecha = value
        End Set
    End Property

    Public Property Izquierda() As Integer
        Get
            Return m_Izquierda
        End Get
        Set(value As Integer)
            m_Izquierda = value
        End Set
    End Property
End Class
