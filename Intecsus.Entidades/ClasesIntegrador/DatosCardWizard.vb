﻿''' <summary>
''' Clase que representa el objeto a enviar a cardwizard
''' </summary>
''' <remarks>AERM 30-01-2015</remarks>
''' se modifica agregando usuario 19-10-2016
Public Class DatosCardWizard
    ''EN los parametros van los valores especificos de CardWizard ejemplo:
    '' parametro.Nombre = 'DATAITEM'
    '' Parametro.NameCard = '@PAN@'
    ''Parametro.Typo = 'string'
    ''Parametro.Valor = '23234'
    Private m_PCName As String
    Private m_ID_Transaccion As Integer
    Private m_Tipo_Producto As String
    Private m_CardWizardName As String
    Private m_Reintentos As Integer
    Private m_Parametros As List(Of ParametrosXML)
    Private m_Tiempo As Date
    Private m_Mensaje As String
    Private m_Usuario As String

    Public Property Mensaje() As String
        Get
            Return m_Mensaje
        End Get
        Set(value As String)
            m_Mensaje = value
        End Set
    End Property

    Public Property PCName() As String
        Get
            Return m_PCName
        End Get
        Set(value As String)
            m_PCName = value
        End Set
    End Property
    Public Property Reintentos() As Integer
        Get
            Return m_Reintentos
        End Get
        Set(value As Integer)
            m_Reintentos = value
        End Set
    End Property

    Public Property TiempoPRoceso() As Date
        Get
            Return m_Tiempo
        End Get
        Set(value As Date)
            m_Tiempo = value
        End Set
    End Property
    Public Property Tipo_Producto() As Integer
        Get
            Return m_Tipo_Producto
        End Get
        Set(value As Integer)
            m_Tipo_Producto = value
        End Set
    End Property

    Public Property ID_Transaccion() As Integer
        Get
            Return m_ID_Transaccion
        End Get
        Set(value As Integer)
            m_ID_Transaccion = value
        End Set
    End Property
    Public Property CardWizardName() As String
        Get
            Return m_CardWizardName
        End Get
        Set(value As String)
            m_CardWizardName = value
        End Set
    End Property

    Public Property Parametros() As List(Of ParametrosXML)
        Get
            Return m_Parametros
        End Get
        Set(value As List(Of ParametrosXML))
            m_Parametros = value
        End Set
    End Property

    Public Property Usuario() As String
        Get
            Return m_Usuario
        End Get
        Set(value As String)
            m_Usuario = value
        End Set
    End Property
End Class
