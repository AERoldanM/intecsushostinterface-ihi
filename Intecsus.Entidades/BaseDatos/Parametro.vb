﻿''' <summary>
'''  Representa un parametro dentro de la B.D
''' </summary>
''' <remarks>AERM 20150119</remarks>
Public Class Parametro(Of T)
    Private m_Nombre As String
    Private m_Tamano As Integer
    Private m_TipoDato As T
    Private m_ValorEnviar As String

    ''' <summary>
    ''' Nombre del usuario
    ''' </summary>
    Public Property nombre() As String
        Get
            Return m_Nombre
        End Get
        Set(value As String)
            m_Nombre = value
        End Set
    End Property

    ''' <summary>
    ''' Tamanño maximo dentro de la B.D
    ''' </summary>
    Public Property tamano() As Integer
        Get
            Return m_Tamano
        End Get
        Set(value As Integer)
            m_Tamano = value
        End Set
    End Property

    ''' <summary>
    ''' Representa el tipo de dato a enviar
    ''' </summary>
    Public Property tipoDato() As T
        Get
            Return m_TipoDato
        End Get
        Set(value As T)
            m_TipoDato = value
        End Set
    End Property

    Public Property valorEnviar() As String
        Get
            Return m_ValorEnviar
        End Get
        Set(value As String)
            m_ValorEnviar = value
        End Set
    End Property


End Class
