﻿Imports System.Runtime.Serialization

<DataContract>
Public Class Operaciones
    <DataMember>
    Public Property CrearUsuario() As [Boolean]
        Get
            Return m_CrearUsuario
        End Get
        Set(value As [Boolean])
            m_CrearUsuario = value
        End Set
    End Property
    Private m_CrearUsuario As [Boolean] = False

    <DataMember>
    Public Property ModUsuario() As [Boolean]
        Get
            Return m_ModUsuario
        End Get
        Set(value As [Boolean])
            m_ModUsuario = value
        End Set
    End Property
    Private m_ModUsuario As [Boolean] = False

    <DataMember>
    Public Property EliminarUsuario() As [Boolean]
        Get
            Return m_EliminarUsuario
        End Get
        Set(value As [Boolean])
            m_EliminarUsuario = value
        End Set
    End Property
    Private m_EliminarUsuario As [Boolean] = False

    <DataMember>
    Public Property ListarUsuario() As [Boolean]
        Get
            Return m_ListarUsuario
        End Get
        Set(value As [Boolean])
            m_ListarUsuario = value
        End Set
    End Property
    Private m_ListarUsuario As [Boolean] = False

    <DataMember>
    Public Property CambiarContrasena() As [Boolean]
        Get
            Return m_CambiarContrasena
        End Get
        Set(value As [Boolean])
            m_CambiarContrasena = value
        End Set
    End Property
    Private m_CambiarContrasena As [Boolean] = False

    <DataMember>
    Public Property AsignarPin() As [Boolean]
        Get
            Return m_AsignarPin
        End Get
        Set(value As [Boolean])
            m_AsignarPin = value
        End Set
    End Property
    Private m_AsignarPin As [Boolean] = False

    <DataMember>
    Public Property ListarPerfiles() As [Boolean]
        Get
            Return m_ListarPerfiles
        End Get
        Set(value As [Boolean])
            m_ListarPerfiles = value
        End Set
    End Property
    Private m_ListarPerfiles As [Boolean] = False

    <DataMember>
    Public Property ConsultarConfiguracion() As [Boolean]
        Get
            Return m_ConsultarConfiguracion
        End Get
        Set(value As [Boolean])
            m_ConsultarConfiguracion = value
        End Set
    End Property
    Private m_ConsultarConfiguracion As [Boolean] = False

    <DataMember>
    Public Property AsignarContrasena() As [Boolean]
        Get
            Return m_AsignarContrasena
        End Get
        Set(value As [Boolean])
            m_AsignarContrasena = value
        End Set
    End Property
    Private m_AsignarContrasena As [Boolean] = False

End Class

