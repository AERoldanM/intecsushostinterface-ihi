﻿Imports System.Runtime.Serialization

<DataContract>
Public Class Configuracion
    <DataMember>
    Public Property Nombre() As String
        Get
            Return m_Nombre
        End Get
        Set(value As String)
            m_Nombre = value
        End Set
    End Property
    Private m_Nombre As String
    <DataMember>
    Public Property Valor() As String
        Get
            Return m_Valor
        End Get
        Set(value As String)
            m_Valor = value
        End Set
    End Property
    Private m_Valor As String
End Class