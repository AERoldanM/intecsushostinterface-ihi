﻿Imports System.Runtime.Serialization

<DataContract>
Public Class UsuarioPIN
    Public Property IDUsuario() As Integer
        Get
            Return m_IDUsuario
        End Get
        Set(value As Integer)
            m_IDUsuario = value
        End Set
    End Property
    Private m_IDUsuario As Integer = 0

    <DataMember(Name:="Identificador")>
    Public Property Identificacion() As String
        Get
            Return m_Identificacion
        End Get
        Set(value As String)
            m_Identificacion = value
        End Set
    End Property
    Private m_Identificacion As String

    <DataMember>
    Public Property Nombre() As String
        Get
            Return m_Nombre
        End Get
        Set(value As String)
            m_Nombre = value
        End Set
    End Property
    Private m_Nombre As String

    <DataMember>
    Public Property Apellido() As String
        Get
            Return m_Apellido
        End Get
        Set(value As String)
            m_Apellido = value
        End Set
    End Property
    Private m_Apellido As String

    <DataMember>
    Public Property Email() As String
        Get
            Return m_Email
        End Get
        Set(value As String)
            m_Email = value
        End Set
    End Property
    Private m_Email As String

    <DataMember>
    Public Property Telefono() As String
        Get
            Return m_Telefono
        End Get
        Set(value As String)
            m_Telefono = value
        End Set
    End Property
    Private m_Telefono As String

    <DataMember>
    Public Property perfil() As Perfil
        Get
            Return m_perfil
        End Get
        Set(value As Perfil)
            m_perfil = value
        End Set
    End Property
    Private m_perfil As Perfil

    <DataMember>
    Public Property contrasena() As String
        Get
            Return m_contrasena
        End Get
        Set(value As String)
            m_contrasena = value
        End Set
    End Property
    Private m_contrasena As String

    <DataMember>
    Public Property Activo() As [Boolean]
        Get
            Return m_Activo
        End Get
        Set(value As [Boolean])
            m_Activo = value
        End Set
    End Property
    Private m_Activo As [Boolean]


    <DataMember>
    Public Property ContrasenaInicial() As [Boolean]
        Get
            Return m_ContrasenaInicial
        End Get
        Set(value As [Boolean])
            m_ContrasenaInicial = value
        End Set
    End Property
    Private m_ContrasenaInicial As [Boolean]
End Class
