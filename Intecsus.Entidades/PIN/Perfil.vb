﻿Imports System.Runtime.Serialization

<DataContract>
Public Class Perfil
    <DataMember>
    Public Property IDPerfil() As Integer
        Get
            Return m_IDPerfil
        End Get
        Set(value As Integer)
            m_IDPerfil = value
        End Set
    End Property
    Private m_IDPerfil As Integer

    <DataMember>
    Public Property NombrePerfil() As String
        Get
            Return m_NombrePerfil
        End Get
        Set(value As String)
            m_NombrePerfil = value
        End Set
    End Property
    Private m_NombrePerfil As String

    <DataMember>
    Public Property operaciones() As Operaciones
        Get
            Return m_operaciones
        End Get
        Set(value As Operaciones)
            m_operaciones = value
        End Set
    End Property
    Private m_operaciones As Operaciones
End Class
