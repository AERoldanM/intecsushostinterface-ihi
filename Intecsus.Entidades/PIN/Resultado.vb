﻿Imports System.Runtime.Serialization

<DataContract>
Public Class Resultado
    Private codigoRespuesta As String = "00"
    Private m_mensaje As String = "Mensaje Recibido "

    <DataMember>
    Public Property CodRespuesta() As String
        Get
            Return codigoRespuesta
        End Get
        Set(value As String)
            codigoRespuesta = value
        End Set
    End Property

    <DataMember>
    Public Property Mensaje() As String
        Get
            Return m_mensaje
        End Get
        Set(value As String)
            m_mensaje = value
        End Set
    End Property
End Class

<DataContract>
Public Class ResultadoUsuario
    Private codigoRespuesta As String = "00"
    Private m_mensaje As String = "Mensaje Recibido "
    Private m_ListaUsuarios As List(Of UsuarioPIN)

    <DataMember>
    Public Property CodRespuesta() As String
        Get
            Return codigoRespuesta
        End Get
        Set(value As String)
            codigoRespuesta = value
        End Set
    End Property

    <DataMember>
    Public Property Mensaje() As String
        Get
            Return m_mensaje
        End Get
        Set(value As String)
            m_mensaje = value
        End Set
    End Property

    <DataMember>
     Public Property ListaUsuarios() As List(Of UsuarioPIN)
        Get
            Return m_ListaUsuarios
        End Get
        Set(ByVal value As List(Of UsuarioPIN))
            m_ListaUsuarios = value
        End Set
    End Property

End Class

<DataContract>
Public Class RespuestaPIN
    <DataMember>
    Public Property resultado() As Resultado
        Get
            Return m_resultado
        End Get
        Set(value As Resultado)
            m_resultado = value
        End Set
    End Property
    Private m_resultado As Resultado

    <DataMember>
    Public Property usuario() As UsuarioPIN
        Get
            Return m_usuario
        End Get
        Set(value As UsuarioPIN)
            m_usuario = value
        End Set
    End Property
    Private m_usuario As UsuarioPIN
End Class

<DataContract>
Public Class RespuestaConfig
    <DataMember>
    Public Property resultado() As Resultado
        Get
            Return m_resultado
        End Get
        Set(value As Resultado)
            m_resultado = value
        End Set
    End Property
    Private m_resultado As Resultado

    <DataMember>
    Public Property configuracion() As List(Of Configuracion)
        Get
            Return m_configuracion
        End Get
        Set(value As List(Of Configuracion))
            m_configuracion = value
        End Set
    End Property
    Private m_configuracion As List(Of Configuracion)

    <DataMember>
    Public Property perfiles() As List(Of Perfil)
        Get
            Return m_perfiles
        End Get
        Set(value As List(Of Perfil))
            m_perfiles = value
        End Set
    End Property
    Private m_perfiles As List(Of Perfil)
End Class