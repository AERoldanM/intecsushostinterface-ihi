﻿Imports System.Runtime.Serialization

<DataContract>
Public Class PIN
    <DataMember>
    Public Property PinBlock() As String
        Get
            Return m_PinBlock
        End Get
        Set(value As String)
            m_PinBlock = value
        End Set
    End Property
    Private m_PinBlock As String

    <DataMember>
    Public Property KSN() As String
        Get
            Return m_KSN
        End Get
        Set(value As String)
            m_KSN = value
        End Set
    End Property
    Private m_KSN As String
End Class
