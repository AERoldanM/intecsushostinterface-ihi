﻿''' <summary>
''' Clase q representa la respuesta de una peticion al cardWizard
''' </summary>
''' <remarks>AERM 04/03/2015</remarks>
Public Class RespuestaEstado
    Private m_Estado As ESTADO_REALCE
    Private m_Status As String
    Private m_Mensaje As String
    Private m_hostIdentifier As String

    ''' <summary>
    ''' Estado de la impresion
    ''' </summary>
    Public Property estado() As ESTADO_REALCE
        Get
            Return m_Estado
        End Get
        Set(value As ESTADO_REALCE)
            m_Estado = value
        End Set
    End Property

    ''' <summary>
    ''' Representa el estado de la peticion realizada
    ''' </summary>
    Public Property status() As String
        Get
            Return m_Status
        End Get
        Set(value As String)
            m_Status = value
        End Set
    End Property

    ''' <summary>
    ''' Representa el mensaje de la peticion realizada
    ''' </summary>
    Public Property mensaje() As String
        Get
            Return m_Mensaje
        End Get
        Set(value As String)
            m_Mensaje = value
        End Set
    End Property

   


End Class
