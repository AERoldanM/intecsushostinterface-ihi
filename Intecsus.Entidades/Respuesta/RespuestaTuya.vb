﻿Public Class RespuestaTuya
    Private m_Codigo As String
    Private m_Mensaje As String
    Private m_NumeroAutorizacion As String
    Private m_NumeroTarjetaAsignado As String

    ''' <summary>
    ''' Codigo de respuesta
    ''' </summary>
    Public Property codigo() As String
        Get
            Return m_Codigo
        End Get
        Set(value As String)
            m_Codigo = value
        End Set
    End Property

    ''' <summary>
    ''' Mensaje con la solcitud realizada
    ''' </summary>
    Public Property mensaje() As String
        Get
            Return m_Mensaje
        End Get
        Set(value As String)
            m_Mensaje = value
        End Set
    End Property
    ''' <summary>
    ''' Mensaje con la solcitud realizada
    ''' </summary>
    Public Property numeroAutorizacion() As String
        Get
            Return m_NumeroAutorizacion
        End Get
        Set(value As String)
            m_NumeroAutorizacion = value
        End Set
    End Property

    ''' <summary>
    ''' Tarjeta con la solcitud realizada
    ''' </summary>
    Public Property numeroTarjetaAsignado() As String
        Get
            Return m_NumeroTarjetaAsignado
        End Get
        Set(value As String)
            m_NumeroTarjetaAsignado = value
        End Set
    End Property
End Class
