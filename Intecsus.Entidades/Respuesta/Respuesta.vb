﻿''' <summary>
''' Clase que representa la respuesta a las solicitudesd en servicio
''' </summary>
''' <remarks>AERM 22/01/2015</remarks>
Public Class Respuesta
    Private m_Codigo As String
    Private m_Mensaje As String
    Private m_NumeroAutorizacion As String

    ''' <summary>
    ''' Codigo de respuesta
    ''' </summary>
    Public Property codigo() As String
        Get
            Return m_Codigo
        End Get
        Set(value As String)
            m_Codigo = value
        End Set
    End Property

    ''' <summary>
    ''' Mensaje con la solcitud realizada
    ''' </summary>
    Public Property mensaje() As String
        Get
            Return m_Mensaje
        End Get
        Set(value As String)
            m_Mensaje = value
        End Set
    End Property
    ''' <summary>
    ''' Mensaje con la solcitud realizada
    ''' </summary>
    Public Property numeroAutorizacion() As String
        Get
            Return m_NumeroAutorizacion
        End Get
        Set(value As String)
            m_NumeroAutorizacion = value
        End Set
    End Property
End Class
