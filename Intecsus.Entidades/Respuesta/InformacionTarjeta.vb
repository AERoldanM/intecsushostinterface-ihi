﻿''' <summary>
''' Clase q representa la informacion que debemos obtener de la tarejta
''' </summary>
''' <remarks>AERM 22/09/2015</remarks>
Public Class InformacionTarjeta
    Private m_Codigo As String
    Private m_ShaIdentificador As String

    ''' <summary>
    ''' Representa el ID chip a obtener
    ''' </summary>
    Public Property IdChipContacto() As String
        Get
            Return m_Codigo
        End Get
        Set(value As String)
            m_Codigo = value
        End Set
    End Property

    ''' <summary>
    ''' Representa identificador Sha512
    ''' </summary>
    Public Property Identificador() As String
        Get
            Return m_ShaIdentificador
        End Get
        Set(value As String)
            m_ShaIdentificador = value
        End Set
    End Property

End Class

''' <summary>
''' Clase representa la respuesta del sistema
''' </summary>
''' <remarks>AERM 22/09/2015</remarks>
Public Class RespuestaInfo
    Private m_Codigo As String
    Private m_Mensaje As String

    ''' <summary>
    ''' Representa codigo
    ''' </summary>
    Public Property Codigo() As String
        Get
            Return m_Codigo
        End Get
        Set(value As String)
            m_Codigo = value
        End Set
    End Property

    ''' <summary>
    ''' Representa mensaje
    ''' </summary>
    Public Property Mensaje() As String
        Get
            Return m_Mensaje
        End Get
        Set(value As String)
            m_Mensaje = value
        End Set
    End Property

End Class
