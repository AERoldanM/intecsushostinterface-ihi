﻿Imports System.ComponentModel.DataAnnotations

Public Class EstadoXImpresoraC

    <Display(Name:="Identificador")>
    Public Property IdCodRespuesta() As Integer
        Get
            Return m_IdCodRespuesta
        End Get
        Set
            m_IdCodRespuesta = Value
        End Set
    End Property
    Private m_IdCodRespuesta As Integer

    <Required(ErrorMessage:="Campo  Obligatorio")>
    <RegularExpression("^[* 0-9]*$", ErrorMessage:="Solo se aceptan números o un *")>
    Public Property ReturnCode() As String
        Get
            Return m_ReturnCode
        End Get
        Set
            m_ReturnCode = Value
        End Set
    End Property
    Private m_ReturnCode As String

    <RegularExpression("^[* 0-9]*$", ErrorMessage:="Solo se aceptan números o un *")>
    Public Property DeviceType() As String
        Get
            Return m_DeviceType
        End Get
        Set
            m_DeviceType = Value
        End Set
    End Property
    Private m_DeviceType As String

    <RegularExpression("^[* 0-9]*$", ErrorMessage:="Solo se aceptan números o un *")>
    Public Property DeviceStatus() As String
        Get
            Return m_DeviceStatus
        End Get
        Set
            m_DeviceStatus = Value
        End Set
    End Property
    Private m_DeviceStatus As String

    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property Estado() As Estados
        Get
            Return m_Estado
        End Get
        Set
            m_Estado = Value
        End Set
    End Property
    Private m_Estado As Estados

    ''' <summary>
    ''' Parametros lista desplegable
    ''' </summary>
    Public Property SelectedItemId() As String
        Get
            Return m_SelectedItemId
        End Get
        Set
            m_SelectedItemId = Value
        End Set
    End Property
    Private m_SelectedItemId As String
    Public Property EstadosSelec() As IEnumerable(Of Estados)
        Get
            Return m_EstadosSelec
        End Get
        Set
            m_EstadosSelec = Value
        End Set
    End Property
    Private m_EstadosSelec As IEnumerable(Of Estados)


End Class
