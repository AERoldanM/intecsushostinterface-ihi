﻿Imports System.ComponentModel.DataAnnotations

Public Class EstadoXImpresion

    <Display(Name:="Identificador")>
    Public Property IdCodRespuesta() As Integer
        Get
            Return m_IdCodRespuesta
        End Get
        Set
            m_IdCodRespuesta = Value
        End Set
    End Property
    Private m_IdCodRespuesta As Integer

    <Display(Name:="MessageType")>
    <Required(ErrorMessage:="Campo  Obligatorio")>
    <RegularExpression("^[* 0-9]*$", ErrorMessage:="Solo se aceptan números o un *")>
    Public Property IdMessageType() As String
        Get
            Return m_IdMessageType
        End Get
        Set
            m_IdMessageType = Value
        End Set
    End Property
    Private m_IdMessageType As String

    <Display(Name:="DataInteger")>
    <RegularExpression("^[* 0-9]*$", ErrorMessage:="Solo se aceptan números o un *")>
    Public Property IdDataInteger() As String
        Get
            Return m_IdDataInteger
        End Get
        Set
            m_IdDataInteger = Value
        End Set
    End Property
    Private m_IdDataInteger As String

    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property Estado() As Estados
        Get
            Return m_Estado
        End Get
        Set
            m_Estado = Value
        End Set
    End Property
    Private m_Estado As Estados

    ''' <summary>
    ''' Parametros lista desplegable
    ''' </summary>
    Public Property SelectedItemId() As String
        Get
            Return m_SelectedItemId
        End Get
        Set
            m_SelectedItemId = Value
        End Set
    End Property
    Private m_SelectedItemId As String
    Public Property EstadosSelec() As IEnumerable(Of Estados)
        Get
            Return m_EstadosSelec
        End Get
        Set
            m_EstadosSelec = Value
        End Set
    End Property
    Private m_EstadosSelec As IEnumerable(Of Estados)


End Class
