﻿Imports System.ComponentModel.DataAnnotations

Public Class Estados
    <Display(Name:="Identificador")>
    Public Property IdEstado() As Integer
        Get
            Return m_IdEstado
        End Get
        Set
            m_IdEstado = Value
        End Set
    End Property
    Private m_IdEstado As Integer

    <Required(ErrorMessage:="Campo  Obligatorio")>
    <Display(Name:="Nombre")>
    Public Property NombreEstado() As String
        Get
            Return m_NombreEstado
        End Get
        Set
            m_NombreEstado = Value
        End Set
    End Property
    Private m_NombreEstado As String
End Class
