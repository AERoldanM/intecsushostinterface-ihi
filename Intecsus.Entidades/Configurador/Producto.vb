﻿Imports System.ComponentModel.DataAnnotations
Public Class Producto
    <Display(Name:="Identificador")>
    <RegularExpression("^[0-9]*$", ErrorMessage:="Solo se aceptan números")>
    Public Property IdProducto() As Integer
        Get
            Return m_IdProducto
        End Get
        Set
            m_IdProducto = Value
        End Set
    End Property
    Private m_IdProducto As Integer

    <Required(ErrorMessage:="Campo  Obligatorio")>
    <Display(Name:="Usuario")>
    Public Property Cliente() As String
        Get
            Return m_Cliente
        End Get
        Set
            m_Cliente = Value
        End Set
    End Property
    Private m_Cliente As String

    <Required(ErrorMessage:="Campo  Obligatorio")>
    <Display(Name:="Producto Recibido")>
    Public Property ProductoName() As String
        Get
            Return m_ProductoName
        End Get
        Set
            m_ProductoName = Value
        End Set
    End Property
    Private m_ProductoName As String

    <Required(ErrorMessage:="Campo  Obligatorio")>
    <Display(Name:="Producto Enviado")>
    Public Property CardName() As String
        Get
            Return m_CardName
        End Get
        Set
            m_CardName = Value
        End Set
    End Property
    Private m_CardName As String

    <Required(ErrorMessage:="*")>
    <Display(Name:="Configuración")>
    <DataType(DataType.MultilineText)>
    Public Property XML() As String
        Get
            Return m_XML
        End Get
        Set
            m_XML = Value
        End Set
    End Property
    Private m_XML As String

    <RegularExpression("^[0-9]*$", ErrorMessage:="*")>
    <Display(Name:="Tamaño")>
    Public Property Tamano() As Integer
        Get
            Return m_Tamano
        End Get
        Set
            m_Tamano = Value
        End Set
    End Property
    Private m_Tamano As Integer

End Class
