﻿Imports System.ComponentModel.DataAnnotations

Public Class UsuarioC


    Public Property IdUsuario() As Integer
        Get
            Return m_IdUsuario
        End Get
        Set
            m_IdUsuario = Value
        End Set
    End Property
    Private m_IdUsuario As Integer

    <Required(ErrorMessage:="Campo  Obligatorio")>
    <Display(Name:="Usuario")>
    Public Property UserID() As String
        Get
            Return m_UserID
        End Get
        Set
            m_UserID = Value
        End Set
    End Property
    Private m_UserID As String

    <Required(ErrorMessage:="Campo  Obligatorio")>
    <DataType(DataType.Password)>
    <Display(Name:="Contraseña")>
    Public Property Contrasena() As String
        Get
            Return m_Contrasena
        End Get
        Set
            m_Contrasena = Value
        End Set
    End Property
    Private m_Contrasena As String

    <DataType(DataType.Password)>
    <Display(Name:="Confirmar Contraseña")>
    <Compare("Contrasena", ErrorMessage:="La contraseña y la confirmación no coinciden.")>
    Public Property ContrasenaConfirm() As String
        Get
            Return m_ContrasenaConfirm
        End Get
        Set
            m_ContrasenaConfirm = Value
        End Set
    End Property
    Private m_ContrasenaConfirm As String


    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property Nombre() As String
        Get
            Return m_Nombre
        End Get
        Set
            m_Nombre = Value
        End Set
    End Property
    Private m_Nombre As String

    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property Apellido() As String
        Get
            Return m_Apellido
        End Get
        Set
            m_Apellido = Value
        End Set
    End Property
    Private m_Apellido As String

    Public ReadOnly Property Nombres() As String
        Get
            Return String.Format("{0} {1}", Me.Nombre, Me.Apellido)
        End Get
    End Property

    <Required(ErrorMessage:="Campo  Obligatorio")>
    <RegularExpression("^[0-9]*$", ErrorMessage:="Solo se aceptan números")>
    <Display(Name:="# Identificacion")>
    Public Property Identificacion() As String
        Get
            Return m_Identificacion
        End Get
        Set
            m_Identificacion = Value
        End Set
    End Property
    Private m_Identificacion As String

    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property RolAsociado() As Rol
        Get
            Return m_RolAsociado
        End Get
        Set
            m_RolAsociado = Value
        End Set
    End Property
    Private m_RolAsociado As Rol


    ''' <summary>
    ''' Parametros lista desplegable
    ''' </summary>
    Public Property SelectedItemId() As String
        Get
            Return m_SelectedItemId
        End Get
        Set
            m_SelectedItemId = Value
        End Set
    End Property
    Private m_SelectedItemId As String
    Public Property Roles() As IEnumerable(Of Rol)
        Get
            Return m_Roles
        End Get
        Set
            m_Roles = Value
        End Set
    End Property
    Private m_Roles As IEnumerable(Of Rol)

End Class
