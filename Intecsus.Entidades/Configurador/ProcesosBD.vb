﻿Imports System.ComponentModel.DataAnnotations

Public Class ProcesosBD

    Public Property IDProceso() As Integer
        Get
            Return m_IDProceso
        End Get
        Set
            m_IDProceso = Value
        End Set
    End Property
    Private m_IDProceso As Integer
    <Required(ErrorMessage:="Campo  Obligatorio")>
    <Display(Name:="Proceso")>
    Public Property NombreProceso() As String
        Get
            Return m_NombreProceso
        End Get
        Set
            m_NombreProceso = Value
        End Set
    End Property
    Private m_NombreProceso As String


End Class
