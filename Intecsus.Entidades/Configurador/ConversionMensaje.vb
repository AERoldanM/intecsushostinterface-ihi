﻿Imports System.ComponentModel.DataAnnotations

Public Class ConversionMensaje
    <Display(Name:="Identificador")>
    <Required(ErrorMessage:="Campo  Obligatorio")>
    <RegularExpression("^[0-9]*$", ErrorMessage:="Solo se aceptan números")>
    Public Property IdConversion() As Integer
        Get
            Return m_IdConversion
        End Get
        Set
            m_IdConversion = Value
        End Set
    End Property
    Private m_IdConversion As Integer

    <Display(Name:="Mensaje Recibido")>
    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property MensajeCW() As String
        Get
            Return m_MensajeCW
        End Get
        Set
            m_MensajeCW = Value
        End Set
    End Property
    Private m_MensajeCW As String

    <Display(Name:="Mensaje Enviado")>
    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property Mensaje() As String
        Get
            Return m_Mensaje
        End Get
        Set
            m_Mensaje = Value
        End Set
    End Property
    Private m_Mensaje As String

    <Display(Name:="Codigo Enviado")>
    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property CodigoCliente() As String
        Get
            Return m_CodigoCliente
        End Get
        Set
            m_CodigoCliente = Value
        End Set
    End Property
    Private m_CodigoCliente As String


End Class
