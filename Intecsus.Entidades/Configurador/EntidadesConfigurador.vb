﻿
Imports System.ComponentModel.DataAnnotations
''' <summary>
''' Clase que representa un argumento genérico para el manejo de eventos
''' </summary>
''' <typeparam name="T">Tipo de argumento genérico</typeparam>
Public Class Generico(Of T)
    Inherits EventArgs
    ''' <summary>
    ''' Obtiene o establece la propiedad a notificar a travez del argumento genérico del evento
    ''' </summary>
    Public Property Argumento() As T
        Get
            Return m_Argumento
        End Get
        Set(value As T)
            m_Argumento = value
        End Set
    End Property
    Private m_Argumento As T
End Class


''' <summary>
''' Objeto q representa una configuracion de la BD
''' </summary>
''' <remarks>AERm 06/04/2015</remarks>
Public Class Configurar

    <Display(Name:="Identificador")>
    Public Property IDValor() As String
        Get
            Return m_IDValor
        End Get
        Set
            m_IDValor = Value
        End Set
    End Property
    Private m_IDValor As String
    <Required(ErrorMessage:="Campo  Obligatorio")>
    <Display(Name:="Tipo")>
    Public Property TipoDato() As String
        Get
            Return m_TipoDato
        End Get
        Set
            m_TipoDato = Value
        End Set
    End Property
    Private m_TipoDato As String
    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property Valor() As String
        Get
            Return m_Valor
        End Get
        Set
            m_Valor = Value
        End Set
    End Property
    Private m_Valor As String

End Class
''' <summary>
''' Objeto q representa una impresora configurada de la BD
''' </summary>
''' <remarks>AERm 07/04/2015</remarks>
Public Class Impresora

    <Display(Name:="Identificador")>
    <RegularExpression("^[0-9]*$", ErrorMessage:="Solo se aceptan números")>
    Public Property IdImpresora() As Integer
        Get
            Return m_IdImpresora
        End Get
        Set
            m_IdImpresora = Value
        End Set
    End Property
    Private m_IdImpresora As Integer

    <Required(ErrorMessage:="Campo  Obligatorio")>
    <Display(Name:="Nombre Recibido")>
    Public Property NombreRecibido() As String
        Get
            Return m_NombreRecibido
        End Get
        Set
            m_NombreRecibido = Value
        End Set
    End Property
    Private m_NombreRecibido As String

    <Required(ErrorMessage:="Campo  Obligatorio")>
    <Display(Name:="Nombre a Enviar")>
    Public Property PCNAME() As String
        Get
            Return m_PCNAME
        End Get
        Set
            m_PCNAME = Value
        End Set
    End Property
    Private m_PCNAME As String

    Public Property IP() As String
        Get
            Return m_IP
        End Get
        Set
            m_IP = Value
        End Set
    End Property
    Private m_IP As String

End Class

''' <summary>
''' Objeto q representa un producto configurado de la BD
''' </summary>
''' <remarks>AERm 07/04/2015</remarks>
Public Class ProductoXCW
    Private m_ID As String
    Private m_Cliente As String
    Private m_Producto As String
    Private m_CardName As String

    Public Property ID() As String
        Get
            Return m_ID
        End Get
        Set(value As String)
            m_ID = value
        End Set
    End Property

    Public Property Cliente() As String
        Get
            Return m_Cliente
        End Get
        Set(value As String)
            m_Cliente = value
        End Set
    End Property

    Public Property Producto() As String
        Get
            Return m_Producto
        End Get
        Set(value As String)
            m_Producto = value
        End Set
    End Property

    Public Property CardName() As String
        Get
            Return m_CardName
        End Get
        Set(value As String)
            m_CardName = value
        End Set
    End Property
End Class

''' <summary>
''' Objeto q representa un procesoXAprobar de la BD
''' </summary>
''' <remarks>AERM 07/04/2015</remarks>
Public Class ProcesoXAP
    Private m_IDProcesoXAprobar As Integer
    Private m_IdProceso As Integer
    Private m_Fecha As String
    Private m_Informacion As String
    Private m_Intentos As Integer
    Private m_IDTransaccion As Integer

    Public Property ID() As Integer
        Get
            Return m_IDProcesoXAprobar
        End Get
        Set(value As Integer)
            m_IDProcesoXAprobar = value
        End Set
    End Property

    Public Property IDProceso() As Integer
        Get
            Return m_IdProceso
        End Get
        Set(value As Integer)
            m_IdProceso = value
        End Set
    End Property

    Public Property Fecha() As String
        Get
            Return m_Fecha
        End Get
        Set(value As String)
            m_Fecha = value
        End Set
    End Property

    Public Property Informacion() As String
        Get
            Return m_Informacion
        End Get
        Set(value As String)
            m_Informacion = value
        End Set
    End Property


    Public Property Intentos() As Integer
        Get
            Return m_Intentos
        End Get
        Set(value As Integer)
            m_Intentos = value
        End Set
    End Property
    Public Property IDTransaccion() As Integer
        Get
            Return m_IDTransaccion
        End Get
        Set(value As Integer)
            m_IDTransaccion = value
        End Set
    End Property
End Class

''' <summary>
''' Objeto q representa un log de la BD
''' </summary>
''' <remarks>AERM 08/04/2015</remarks>
Public Class LOG
    Private m_IdLog As Integer
    Private m_IdProceso As Integer
    Private m_Fecha As String
    Private m_IDTransaccion As Integer
    Private m_Evento As Integer
    Private m_DescripcionEvento As String

    Public Property IdLog() As Integer
        Get
            Return m_IdLog
        End Get
        Set(value As Integer)
            m_IdLog = value
        End Set
    End Property

    Public Property IdProceso() As Integer
        Get
            Return m_IdProceso
        End Get
        Set(value As Integer)
            m_IdProceso = value
        End Set
    End Property

    Public Property Fecha() As String
        Get
            Return m_Fecha
        End Get
        Set(value As String)
            m_Fecha = value
        End Set
    End Property

    Public Property IDTransaccion() As Integer
        Get
            Return m_IDTransaccion
        End Get
        Set(value As Integer)
            m_IDTransaccion = value
        End Set
    End Property


    Public Property Evento() As Integer
        Get
            Return m_Evento
        End Get
        Set(value As Integer)
            m_Evento = value
        End Set
    End Property
    Public Property DescripcionEvento() As String
        Get
            Return m_DescripcionEvento
        End Get
        Set(value As String)
            m_DescripcionEvento = value
        End Set
    End Property
End Class


''' <summary>
''' Objeto q representa un estado de la transaccion en la BD
''' </summary>
''' <remarks>AERM 08/04/2015</remarks>
Public Class EstadoTransaccion
    Private m_IdTransaccionCl As String
    Private m_Final As Boolean

    <Display(Name:="Identificador")>
    <RegularExpression("^[0-9]*$", ErrorMessage:="Solo se aceptan números")>
    Public Property IdTransaccion() As String
        Get
            Return m_IdTransaccion
        End Get
        Set
            m_IdTransaccion = Value
        End Set
    End Property
    Private m_IdTransaccion As String
    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property Cliente() As String
        Get
            Return m_Cliente
        End Get
        Set
            m_Cliente = Value
        End Set
    End Property
    Private m_Cliente As String

    <Required(ErrorMessage:="Campo  Obligatorio")>
    <Display(Name:="Iden Cliente")>
    Public Property IdtransaCliente() As String
        Get
            Return m_IdtransaCliente
        End Get
        Set
            m_IdtransaCliente = Value
        End Set
    End Property
    Private m_IdtransaCliente As String
    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property Estado() As String
        Get
            Return m_Estado
        End Get
        Set
            m_Estado = Value
        End Set
    End Property
    Private m_Estado As String

    Public Property Fecha() As DateTime
        Get
            Return m_Fecha
        End Get
        Set
            m_Fecha = Value
        End Set
    End Property
    Private m_Fecha As DateTime




    Public Property Final() As Boolean
        Get
            Return m_Final
        End Get
        Set(value As Boolean)
            m_Final = value
        End Set
    End Property


    Public Property IdTransaccionCl() As String
        Get
            Return m_IdTransaccionCl
        End Get
        Set(value As String)
            m_IdTransaccionCl = value
        End Set
    End Property

End Class
