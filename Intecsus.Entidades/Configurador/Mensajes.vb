﻿Imports System.ComponentModel.DataAnnotations

Public Class Mensajes



    <Display(Name:="Codigo Mensaje")>
    <Required(ErrorMessage:="Campo  Obligatorio")>
    <RegularExpression("^[0-9]*$", ErrorMessage:="Solo se aceptan números")>
    Public Property CodigoMensaje() As String
        Get
            Return m_CodigoMensaje
        End Get
        Set
            m_CodigoMensaje = Value
        End Set
    End Property
    Private m_CodigoMensaje As String

    <Display(Name:="Mensaje")>
    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property MensajeEnviado() As String
        Get
            Return m_MensajeEnviado
        End Get
        Set
            m_MensajeEnviado = Value
        End Set
    End Property
    Private m_MensajeEnviado As String

    <Display(Name:="Codigo Enviado")>
    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property CodigoCliente() As String
        Get
            Return m_CodigoCliente
        End Get
        Set
            m_CodigoCliente = Value
        End Set
    End Property
    Private m_CodigoCliente As String

End Class
