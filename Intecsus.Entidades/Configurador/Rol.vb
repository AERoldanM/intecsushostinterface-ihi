﻿Imports System.ComponentModel.DataAnnotations

Public Class Rol

    Public Property IdRol() As Integer
        Get
            Return m_IdRol
        End Get
        Set
            m_IdRol = Value
        End Set
    End Property
    Private m_IdRol As Integer

    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property NombreRol() As String
        Get
            Return m_NombreRol
        End Get
        Set
            m_NombreRol = Value
        End Set
    End Property
    Private m_NombreRol As String
    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property Configuracion() As Boolean
        Get
            Return m_Configuracion
        End Get
        Set
            m_Configuracion = Value
        End Set
    End Property
    Private m_Configuracion As Boolean
    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property Impresoras() As Boolean
        Get
            Return m_Impresoras
        End Get
        Set
            m_Impresoras = Value
        End Set
    End Property
    Private m_Impresoras As Boolean
    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property CardFormat() As Boolean
        Get
            Return m_CardFormat
        End Get
        Set
            m_CardFormat = Value
        End Set
    End Property
    Private m_CardFormat As Boolean
    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property Procesos() As Boolean
        Get
            Return m_Procesos
        End Get
        Set
            m_Procesos = Value
        End Set
    End Property
    Private m_Procesos As Boolean
    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property Log() As Boolean
        Get
            Return m_Log
        End Get
        Set
            m_Log = Value
        End Set
    End Property
    Private m_Log As Boolean
    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property Transacciones() As Boolean
        Get
            Return m_Transacciones
        End Get
        Set
            m_Transacciones = Value
        End Set
    End Property
    Private m_Transacciones As Boolean

    <Required(ErrorMessage:="Campo  Obligatorio")>
    Public Property Usuarios() As Boolean
        Get
            Return m_Usuarios
        End Get
        Set
            m_Usuarios = Value
        End Set
    End Property
    Private m_Usuarios As Boolean

End Class
