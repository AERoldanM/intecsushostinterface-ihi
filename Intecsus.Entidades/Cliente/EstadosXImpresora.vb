﻿Public Class EstadosXImpresora
    ''' <summary>
    ''' Crea el objeto de la instancia
    ''' </summary>
    ''' <remarks>AERM 20/06/2016</remarks>
    Private Shared _instancia As EstadosXImpresora

    Private m_Estados As List(Of EstadoXImpresora) = Nothing

    ''' <summary>
    ''' No causar problemas si es multihilo (Si tiene varias peticiones a  la vez)
    ''' </summary>
    ''' <remarks>AERM 20/06/2016</remarks>
    Private Shared padlock As New Object()

    ''' <summary>
    ''' Método que instancia para poder tener todos los codigos
    ''' </summary>
    ''' <remarks>AERM 20/06/2016</remarks>
    Private Sub New(estados As List(Of EstadoXImpresora))
        'Aqui podrian ir un facade con la Conexión
        m_Estados = estados
    End Sub

    ''' <summary>
    ''' Método que realiza la instancia mediante una propiedad
    ''' </summary>
    Public Shared ReadOnly Property Instancia(estados As List(Of EstadoXImpresora)) As EstadosXImpresora
        Get
            'Hasta que no termine la petición no recibe la otra (Caso multihilo)
            SyncLock padlock
                ' valida que no exista una instancia si ya existe, no la realiza
                If (_instancia Is Nothing) Then
                    _instancia = New EstadosXImpresora(estados)
                End If
            End SyncLock

            Return _instancia
        End Get
    End Property

    ''' <summary>
    ''' Devuelve la instacia ya creada con el singleton
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>AERM 20/01/2015</remarks>
    Public Function obtenerEstados() As List(Of EstadoXImpresora)
        Return m_Estados
    End Function
End Class



Public Class EstadoXImpresora
    Public ReturnCode As String
    Public DeviceType As String
    Public DeviceStatus As String
    Public estadoFinalCW As Int32
End Class
