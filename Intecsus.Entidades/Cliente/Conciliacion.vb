﻿Public Class Conciliacion
    Public IdConciliacion As Int32
    Public IdTransaccion As Int32
    Public Cliente As String
    Public IDTransaccionCliente As String
    Public TipoRegistro As String
    Public CodigoEntidad As String
    Public PAN As String
    Public CODFUMO As String
    Public INDES As String
    Public Usuario As String
    Public Canal As String
    Public Fecha As DateTime
    Public CodMen As String
    Public Descripcion1 As String
    Public Descripcion2 As String
    Public CENALTA As String ''Valor Opcional para enviarlo al cliente
    Public PIN As Boolean

End Class
