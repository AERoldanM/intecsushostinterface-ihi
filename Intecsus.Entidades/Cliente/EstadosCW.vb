﻿Public Class EstadosCW

    ''' <summary>
    ''' Crea el objeto de la instancia
    ''' </summary>
    ''' <remarks>AERM 20/01/2015</remarks>
    Private Shared _instancia As EstadosCW

    Private m_Estados As List(Of EstadoCW) = Nothing

    ''' <summary>
    ''' No causar problemas si es multihilo (Si tiene varias peticiones a  la vez)
    ''' </summary>
    ''' <remarks>AERM 20/01/2015</remarks>
    Private Shared padlock As New Object()

    ''' <summary>
    ''' Método que instancia para poder tener todos los codigos
    ''' </summary>
    ''' <remarks>AERM 20/01/2015</remarks>
    Private Sub New(estados As List(Of EstadoCW))
        'Aqui podrian ir un facade con la Conexión
        m_Estados = estados
    End Sub

    ''' <summary>
    ''' Método que realiza la instancia mediante una propiedad
    ''' </summary>
    Public Shared ReadOnly Property Instancia(estados As List(Of EstadoCW)) As EstadosCW
        Get
            'Hasta que no termine la petición no recibe la otra (Caso multihilo)
            SyncLock padlock
                ' valida que no exista una instancia si ya existe, no la realiza
                If (_instancia Is Nothing) Then
                    _instancia = New EstadosCW(estados)
                End If
            End SyncLock

            Return _instancia
        End Get
    End Property

    ''' <summary>
    ''' Devuelve la instacia ya creada con el singleton
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks>AERM 20/01/2015</remarks>
    Public Function obtenerEstados() As List(Of EstadoCW)
        Return m_Estados
    End Function


    '' Public Shared estadosCWFInal As List(Of EstadoCW)
End Class

Public Class EstadoCW
    Public IdMessageType As String
    Public IdDataInteger As String
    Public estadoFinalCW As Int32
End Class
