﻿''' <summary>
''' Representa la respuesta que deberia devlver el adaptador
''' </summary>
''' <remarks>AERM 24/03/2015</remarks>
''' Modificación se agrega atributo usuario
Public Class RespuestaWS
    Public codigo As String
    Public descripcion As String
    Public numAutorizacion As String
    Public parametros As List(Of ParametrosXML)
    Public xmlPeticion As String
    Public pCName As String
    Public cardWizardName As String
    Public numeroTarjeta As String
    Public usuario As String
End Class
