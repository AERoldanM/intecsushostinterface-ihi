﻿''' <summary>
''' Clase q representa una peticion de un cliente
''' </summary>
''' <remarks>AERM 19/03/2015</remarks>
Public Class Peticion
    Public numeroProducto As String
    Public numeroTarjeta As String
    Public tipoPersona As String
    Public tipoIdentificacion As String
    Public numeroIdentificacion As String
    Public primerApellido As String
    Public segundoApellido As String
    Public primerNombre As String
    Public segundoNombre As String
    Public nombreRealce As String
    Public nombreLargo As String
    Public fechaNacimiento As String
    Public sexo As String
    Public estadoCivil As String
    Public direccionResidencia1 As String
    Public direccionResidencia2 As String
    Public codigoDepartResidencia As String
    Public codigoCiudadResisdencia As String
    Public zonaPostal As String
    Public direccionCorrespondenciaLinea1 As String
    Public direccionCorrespondenciaLinea2 As String
    Public codigoDepartamentoDirCorrespondencia As String
    Public codigoCiudadDirCorrespondencia As String
    Public zonaPostalDirCorres As String
    Public oficinaRadicacion As String
    Public telResidencia As String
    Public telOficina As String
    Public afinidadTarjeta As String
    Public cupoAsignado As String
    Public fechaSolicitud As String
    Public tipoSolicitud As String
    Public manejoCuotas As String
    Public tipoTarjeta As String
    Public codigoVendedor As String
    Public estadoRegistroRecibido As String
    Public numeroSolicitud As String
    Public tarjetaAnterior As String
    Public codigoPuntoDistribucion As String
    Public codigoMercadeo As String
    Public tipoCuenta As String
    Public valorCuotaFija As String
    Public tipoIdAmparador As String
    Public numeroIDAmparador As String
    Public oficinaDistribucion As String
    Public indicativo As String
    Public correoElectronico As String
    Public actividadEconomica As String
    Public origenIngresos As String
    Public codigoBarras As String
    Public idenDebitoAut As String
    Public codBancoDebito As String
    Public numCuentaDebito As String
    Public tipoCuentaDebito As String
    Public indicadorCesacion As String
    Public fechaCesacion As String
    Public cicloFacturacion As String
    Public BIN As String
    Public NIT As String
    Public subtipo As String
    Public codigoCompensacion As String
    Public codigoNovedad As String
    Public codigoProceso As String
    Public filler1 As String
    Public filler2 As String
    Public filler3 As String
    Public filler4 As String

    Public printName As String
    Public idTransaccion As String
    Public tipoProducto As String
    Public cliente As String

    Public IDproceso As Int32
End Class
