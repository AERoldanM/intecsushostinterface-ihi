﻿Public Class VisorEventos

    ''' <summary>
    ''' Metodo encargado de almacenar informacion en el visor de evento
    ''' </summary>
    ''' <param name="nombreLog">Nombre del log--- Application </param>
    ''' <param name="nombreSource">Nombre q le pones al Source --- Error base de datos</param>
    ''' <param name="mensaje">Mensaje q se quiere reportar</param>
    ''' <param name="tipoEvento">Tipo de evento a almacenar EventLogEntryType.Warning</param>
    ''' <remarks>AERM 18/02/2016</remarks>
    Public Sub GuardarLog(ByVal nombreLog As String, ByVal nombreSource As String, ByVal mensaje As String, ByVal tipoEvento As EventLogEntryType)
        Try
            Dim evento As EventLog = New EventLog()
            If Not EventLog.SourceExists(nombreSource) Then
                EventLog.CreateEventSource(nombreSource, nombreLog)
            End If

            evento.Log = nombreLog
            evento.Source = nombreSource
            evento.EnableRaisingEvents = True
            evento.WriteEntry(mensaje, tipoEvento)
        Catch ex As Exception

        End Try
    End Sub
End Class
