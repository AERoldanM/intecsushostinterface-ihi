﻿Imports System.Text

''' <summary>
''' Clase q implementa operaciones base de las cadenas de texto
''' </summary>
''' <remarks>AERM 07/07/2016</remarks>
Public Class CadenasTexto
    ''' <summary>
    ''' Metodo encargado de rellenar una cadena de texto a la derecha
    ''' </summary>
    ''' <param name="cadena">Cadena inicial a rellenar</param>
    ''' <param name="tamano">Tamano de la cadena</param>
    ''' <param name="relleno">Caracteres con los cuales se va aumentando la cadena</param>
    ''' <returns>Retorna cadena del tamaño indicado</returns>
    ''' <remarks>AERM 07/07/2016</remarks>
    Public Function RellenarCadenaDerecha(cadena As String, tamano As Int32, relleno As String) As String
        Dim m_CadenaFinal As StringBuilder = New StringBuilder()
        Try
            m_CadenaFinal.Append(Me.RecortarTamanoCadena(cadena, tamano))
            While (m_CadenaFinal.Length() < tamano)
                m_CadenaFinal.Append(relleno)
            End While
        Catch ex As Exception
            Throw New Exception("CadenasTexto - RellenarCadenaDerecha" + ex.Message)
        End Try

        Return m_CadenaFinal.ToString()
    End Function

    ''' <summary>
    '''  Metodo encargado de rellenar una cadena de texto a la izquierda
    ''' </summary>
    ''' <param name="cadena">Cadena inicial a rellenar</param>
    ''' <param name="tamano">Tamano de la cadena</param>
    ''' <param name="relleno">Caracteres con los cuales se va aumentando la cadena</param>
    ''' <returns>Retorna cadena del tamaño indicado</returns>
    ''' <remarks>AERM 07/07/2016</remarks>
    Public Function RellenarCadenaIzquierda(cadena As String, tamano As Int32, relleno As String) As String
        Dim m_cadenaFinal As String = String.Empty
        Try
            m_cadenaFinal = Me.RecortarTamanoCadena(cadena, tamano)
            While (m_cadenaFinal.Length() < tamano)
                m_cadenaFinal = relleno + m_cadenaFinal
            End While
        Catch ex As Exception
            Throw New Exception("CadenasTexto - RellenarCadenaIzquierda" + ex.Message)
        End Try

        Return m_cadenaFinal
    End Function

    ''' <summary>
    ''' Metodo encargado de recortqar una cadena de texto 
    ''' </summary>
    ''' <param name="cadena">Texto a recortar</param>
    ''' <param name="tamano">Tamañp texto</param>
    ''' <returns>Cadena del tamano menor o igual al solicitado</returns>
    ''' <remarks>AERM 07/07/2016</remarks>
    Public Function RecortarTamanoCadena(cadena As String, tamano As Int32) As String
        Try
            If (cadena.Length > tamano) Then
                Return cadena.Substring(0, tamano)
            End If
        Catch ex As Exception
            Throw New Exception("CadenasTexto - ValidarTamanoCadena" + ex.Message)
        End Try

        Return cadena
    End Function

    ''' <summary>
    ''' Metodo encargado de obtener los valores finales de una cadena
    ''' </summary>
    ''' <param name="cadena">Cadena completa</param>
    ''' <param name="cantidad">Cantidad caracteres a retornar</param>
    ''' <returns>Retorna cadena con los caracteres previstos</returns>
    ''' <remarks>AERM 08/07/2016</remarks>
    Public Function UltimosCadena(cadena As String, cantidad As Int32) As String
        Try
            If (cadena.Length > cantidad) Then
                cadena = cadena.Substring(cadena.Length - cantidad, cantidad)
            End If
        Catch ex As Exception
            Throw New Exception("CadenasTexto - UltimosCadena" + ex.Message)
        End Try

        Return cadena
    End Function

End Class
