﻿Public Class BarcodeConverter128
    Public Function StringToBarcode(chaine As String) As String
        Dim i As Integer
        Dim checksum As Integer
        Dim dummy As Integer
        Dim tableB As Boolean
        Dim mini As Integer
        Dim m_code128 As String = String.Empty
        If (chaine.Length > 0) Then

            'verifique caracteres validos
            For i = 1 To chaine.Length
                Select Case Asc(Mid(chaine, i, 1))
                    Case 32 To 126, 203
                    Case Else
                        i = 0
                        Exit For
                End Select
            Next
            'Cálculo de la cadena de código con el uso optimizado de las tablas B y C
            'code128 = String.Empty
            tableB = True
            If i > 0 Then
                i = 1 'Comienza con el indice del string
                Do While i <= Len(chaine)
                    If tableB Then
                        'A ver si interesante para cambiar a la tabla C
                        'sí para 4 dígitos al principio o al final, o más si 6 dígitos
                        mini = IIf(i = 0 Or i + 3 = Len(chaine), 4, 6)
                        mini = Me.TestNum(mini, chaine, i)

                        If mini < 0 Then 'Choice of table C
                            If i = 1 Then ' Comienza con la tabla C
                                m_code128 = Chr(210)
                            Else 'Commuter sur table C / Switch to table C
                                m_code128 = m_code128 & Chr(204)
                            End If
                            tableB = False
                        Else
                            If i = 1 Then m_code128 = Chr(209) '/ Comenzar con la table B
                        End If

                    End If
                    If Not tableB Then
                        'Estamos en la tabla C, tratamos de procesar 2 dígitos
                        mini = 2
                        mini = Me.TestNum(mini, chaine, i)
                        If mini < 0 Then ' OK  para dos digitos, procesamos eso 
                            dummy = Val(Mid(chaine, i, 2))
                            dummy = IIf(dummy < 95, dummy + 32, dummy + 105)
                            m_code128 = m_code128 & Chr(dummy)
                            i = i + 2
                        Else 'No tiene 2 digitos , cambiar  a la tabla B
                            m_code128 = m_code128 & Chr(205)
                            tableB = True
                        End If
                    End If
                    If tableB Then
                        '/ Procesa un digito para la tabla B
                        m_code128 = m_code128 & Mid(chaine, i, 1)
                        i = i + 1
                    End If
                Loop

                checksum = Me.CalcularCheckSum(checksum, m_code128, dummy)

                'Añador checksum y el stop 
                m_code128 = m_code128 & Chr(checksum) & Chr(211)
            End If
        End If

        Return m_code128
    End Function

    Private Function TestNum(mini As Integer, chaine As String, i As Integer) As Integer
        mini = mini - 1
        If i + mini <= Len(chaine$) Then
            Do While mini >= 0
                If Asc(Mid(chaine, i + mini, 1)) < 48 Or Asc(Mid(chaine, i + mini, 1)) > 57 Then Exit Do
                mini = mini - 1
            Loop
        End If

        Return mini
    End Function

    Private Function CalcularCheckSum(checksum As Integer, m_code128 As String, dummy As Integer) As String
        'Calcular CheckSum ASCII codigo
        For i = 1 To Len(m_code128)
            dummy = Asc(Mid(m_code128, i, 1))
            dummy = IIf(dummy < 127, dummy - 32, dummy - 105)
            If i = 1 Then checksum = dummy
            checksum = (checksum + (i - 1) * dummy) Mod 103
        Next
        'Calcular CheckSum ASCII codigo
        Return IIf(checksum < 95, checksum + 32, checksum + 105)
    End Function
End Class
