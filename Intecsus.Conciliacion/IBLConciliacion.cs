﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intecsus.Conciliacion
{
    public interface IBLConciliacion
    {
        /// <summary>
        /// Metodo encargado de inicializar las variales para poder realizar la operacion de conciliaciòn
        /// <remarks>AERM 07/07/2016</remarks>
        /// </summary>
        void InicializarVariables();

        /// <summary>
        /// Metodo encargado de comenzar con el proceso de Conciliar
        /// <remarks>AERM 07/07/2016</remarks>
        /// </summary>
        void ProcesoConciliar();
    }
}
