﻿using Intecusus.OperarBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intecsus.Conciliacion.Proceso
{
    public class OperacionesConciliacion
    {
        /// <summary>
        /// Metodo encargado de realizar las validaciones para saber si se debe realizar el proceso de conciliacion
        /// </summary>
        /// <param name="ultimaFecha"Ultima fecha en la cual se realizo la conciliacion</param>
        /// <param name="hora">Indica cuando se debe realizar la conciliacion</param>
        /// <param name="operarBD">Operar la BD</param>
        /// <returns>Retorna si se debe comenzar con el proceso  o no</returns>
        /// <remarks>AERM 07/07/2016 </remarks>
        public bool RealizarConciliacion(string ultimaFecha, string hora, string minuto, OperacionesBase operarBD)
        {
            try
            {
                DateTime m_FechaActual = DateTime.Now;
                DateTime m_FechaConciliacion;
                if (!String.IsNullOrEmpty(ultimaFecha)) 
                {
                    m_FechaConciliacion = Convert.ToDateTime(ultimaFecha);
                    if ((m_FechaConciliacion.Day >= m_FechaActual.Day) && (m_FechaConciliacion.Month >= m_FechaActual.Month) 
                        && (m_FechaConciliacion.Year >= m_FechaActual.Year))
                    {
                        return false;
                    }
                }

                if ((m_FechaActual.Hour >= Convert.ToInt32(hora)) && (m_FechaActual.Minute >= Convert.ToInt32(minuto))) 
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                operarBD.GuardarLogXNombreConectionString(24, 0, 2, "OperacionesConciliacion- RealizarConciliacion  " + ex.Message,
                    ConfigurationManager.AppSettings["BD"]);
            }

            return false;
        }

        public List<Int32> ValoresConciliaciones(List<Entidades.Conciliacion> m_ListaConciliaciones, OperacionesBase operarBD)
        {
            List<Int32> m_ValoresBase = new List<Int32>();
            try 
            {
                m_ValoresBase.Add(m_ListaConciliaciones.Count());
                Int32 m_TotalOK = (from c in m_ListaConciliaciones
                                   where c.INDES == "OK"
                                   select c).Count();
                Int32 m_TotalNOK = (from c in m_ListaConciliaciones
                                   where c.INDES == "NOK"
                                   select c).Count();
                m_ValoresBase.Add(m_TotalOK);
                m_ValoresBase.Add(m_TotalNOK);
            }
            catch (Exception ex)
            {
                operarBD.GuardarLogXNombreConectionString(24, 0, 2, "OperacionesConciliacion- ValoresConciliaciones  " + ex.Message,
                    ConfigurationManager.AppSettings["BD"]);
            }

            return m_ValoresBase;

        }
    }
}
