﻿using Intecsus.Conciliacion.Proceso;
using Intecusus.OperarBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Intecsus.Entidades;
using Intecsus.OperarArchivos;
using Intecsus.Utilidades;

namespace Intecsus.Conciliacion
{
    public class BLConciliacion : IBLConciliacion
    {

        #region "Variables"

        private string m_UltimaFechaConciliacion = string.Empty;
        private string m_HoraConciliacion = string.Empty;
        private string m_MinutoConciliacion = string.Empty;
        private string m_rutaConciliacion = string.Empty;
        private string m_ValoresPredefinidos = string.Empty;
        public string m_CODENT = string.Empty;
        public string m_TIPREG = string.Empty;
        public string m_CANAL = string.Empty;
        public string m_CODENTSumario = string.Empty;
        public string m_TIPREGSumario = string.Empty;
        public string m_fecha = string.Empty;
        private OperacionesConciliacion m_OperacionesConciliacion = new OperacionesConciliacion();
        private OperacionesBase operarBD = new OperacionesBase();
        private CadenasTexto m_CadenasTexto = new CadenasTexto();
        private ICArchivos m_operar;

        #endregion

        #region "Metodos Interface"

        public void InicializarVariables()
        {
            try
            {
                m_UltimaFechaConciliacion = operarBD.ObtenerValoresConfigurador("FechaConciliacionSantander", ConfigurationManager.AppSettings["BD"]);
                m_HoraConciliacion = operarBD.ObtenerValoresConfigurador("HoraConciliacionSantander", ConfigurationManager.AppSettings["BD"]);
                m_MinutoConciliacion = operarBD.ObtenerValoresConfigurador("MinutoConciliacionSantander", ConfigurationManager.AppSettings["BD"]);
                m_rutaConciliacion = operarBD.ObtenerValoresConfigurador("RutaArchivoConciliacion", ConfigurationManager.AppSettings["BD"]);
                m_ValoresPredefinidos = operarBD.ObtenerValoresConfiguracionSegura("ValoresPredefinidos", ConfigurationManager.AppSettings["BD"]);
                var m_Valores = operarBD.ObtenerValoresConfigurador("ValoresCabecera", ConfigurationManager.AppSettings["BD"]).Split('-');
                var m_ValoreSumario = operarBD.ObtenerValoresConfigurador("ValoresSumario", ConfigurationManager.AppSettings["BD"]).Split('-');
                m_CODENT = m_Valores[0];
                m_TIPREG = m_Valores[1];
                m_CANAL = m_Valores[2];
                m_CODENTSumario = m_ValoreSumario[0];
                m_TIPREGSumario = m_ValoreSumario[1];
                m_operar = new RetornarArchivo().IntanciarArchivo(operarBD.ObtenerValoresConfigurador("FormatoArchivo", ConfigurationManager.AppSettings["BD"]));
            }
            catch (Exception ex)
            {
                operarBD.GuardarLogXNombreConectionString(23, 0, 2, "IBLConciliacion- InicializarVariables  " + ex.Message,
                    ConfigurationManager.AppSettings["BD"]);
            }
        }

        public void ProcesoConciliar()
        {
            try
            {
                DateTime m_fechaActual = DateTime.Now;
                if (m_OperacionesConciliacion.RealizarConciliacion(m_UltimaFechaConciliacion, m_HoraConciliacion, m_MinutoConciliacion, operarBD) == false)
                    return;
                List<Intecsus.Entidades.Conciliacion> m_ListaConciliaciones = operarBD.ObtenerListaConciliacionesXDIA(ConfigurationManager.AppSettings["BD"]);
                List<Int32> m_ValoresBase = m_OperacionesConciliacion.ValoresConciliaciones(m_ListaConciliaciones, operarBD);
                string m_NombreArchivo = "CONCILIACION" + m_fechaActual.ToString("ddMMyyyy") + ".txt";
                this.AlmacenarCabecera(m_NombreArchivo, m_ValoresBase, m_ListaConciliaciones, m_fechaActual);
                this.AgregarTransacciones(m_NombreArchivo, m_ListaConciliaciones);
                this.AgregarSumario(m_NombreArchivo, m_ValoresBase);
                m_UltimaFechaConciliacion = m_fechaActual.ToString();
                Configurar m_Dato = new Configurar();
                m_Dato.IDValor = "FechaConciliacionSantander";
                m_Dato.Valor = m_UltimaFechaConciliacion;
                m_Dato.TipoDato = "ALFA";
                operarBD.ActualizarConfiguracion(m_Dato, ConfigurationManager.AppSettings["BD"]);
            }
            catch (Exception ex)
            {
                operarBD.GuardarLogXNombreConectionString(23, 0, 2, "IBLConciliacion- ProcesoConciliar  " + ex.Message,
                    ConfigurationManager.AppSettings["BD"]);
            }
        }

        #endregion

        #region "Metodos privados"


        /// <summary>
        /// Metodo encargado de almacenar la cabecera de un archivo
        /// </summary>
        /// <param name="m_NombreArchivo">Nombre del archivo</param>
        /// <param name="m_ValoresBase">Valores base obtenidos anteriormente</param>
        /// <param name="m_ListaConciliaciones">Listado de transacciones</param>
        /// <param name="m_fechaActual">Fecha actual</param>
        /// <remarks>AERM 07/07/2016</remarks>
        private void AlmacenarCabecera(string m_NombreArchivo, List<int> m_ValoresBase, List<Entidades.Conciliacion> m_ListaConciliaciones, DateTime m_fechaActual)
        {
            try
            {
                m_fecha = m_fechaActual.ToString("yyyy-MM-dd");
                string m_TotalRegistros = "0";
                if (m_ValoresBase.Count() >= 3)
                {
                    m_TotalRegistros = m_ValoresBase[0].ToString();
                    if (m_ValoresPredefinidos != "0" && m_ListaConciliaciones.Count() > 0)
                    {
                        m_CODENT = m_ListaConciliaciones[0].CodigoEntidad;
                        m_TIPREG = m_ListaConciliaciones[0].TipoRegistro;
                        m_CANAL = m_ListaConciliaciones[0].Canal;
                    }
                }

                m_CODENT = m_CadenasTexto.RellenarCadenaDerecha(m_CODENT, 4, " ");
                m_TIPREG = m_CadenasTexto.RellenarCadenaDerecha(m_TIPREG, 1, " ");
                m_CANAL = m_CadenasTexto.RellenarCadenaDerecha(m_CANAL, 4, " ");
                m_fecha = m_CadenasTexto.RellenarCadenaDerecha(m_fecha, 10, " ");
                m_TotalRegistros = m_CadenasTexto.RellenarCadenaIzquierda(m_TotalRegistros, 9, "0");
                m_operar.AgregarLineaArchivo(m_rutaConciliacion, m_NombreArchivo, m_CODENT + m_TIPREG + m_CANAL + m_fecha + m_TotalRegistros);
            }
            catch (Exception ex)
            {
                throw new Exception("- AlmacenarCabecera " + ex.Message);
            }
        }

        /// <summary>
        /// Metodo encargado de agregar las transacciones al archivo
        /// </summary>
        /// <param name="m_NombreArchivo">nombre archivo</param>
        /// <param name="m_ListaConciliaciones">Lista de transacciones</param>
        /// <param name="m_fechaActual">Fecha actual</param>
        /// <remarks>AERM 07/07/2016</remarks>
        private void AgregarTransacciones(string m_NombreArchivo, List<Entidades.Conciliacion> m_ListaConciliaciones)
        {
            try
            {

                foreach (Entidades.Conciliacion m_Conciliacion in m_ListaConciliaciones)
                {
                    string m_Entidad = "0004";
                    string m_TipoRegistro = "2";
                    string m_CLAVOPEALTA = m_CadenasTexto.RellenarCadenaDerecha(m_Conciliacion.IDTransaccionCliente, 42, " ");
                    string m_PAN = m_CadenasTexto.RellenarCadenaDerecha(m_Conciliacion.PAN, 4, " ");
                    string m_CODOFIUMO = m_CadenasTexto.RellenarCadenaDerecha(m_Conciliacion.CODFUMO, 4, " ");
                    string m_INDEST = m_CadenasTexto.RellenarCadenaDerecha(m_Conciliacion.INDES, 3, " ");
                    string m_FECOPER = m_CadenasTexto.RellenarCadenaDerecha(m_fecha, 10, " ");
                    string m_Usuario = m_CadenasTexto.RellenarCadenaDerecha(m_Conciliacion.Usuario, 8, " ");
                    string m_CODMEN = m_CadenasTexto.RellenarCadenaDerecha(m_Conciliacion.CodMen, 5, " ");
                    string m_Desc1 = m_CadenasTexto.RellenarCadenaDerecha(m_Conciliacion.Descripcion1, 20, " ");
                    string m_Desc2 = string.Empty;
                    if (m_Desc1.Length > 20)
                        m_Desc2 = m_CadenasTexto.RellenarCadenaDerecha(m_Desc1.Substring(19, m_Desc1.Length - 20) + m_Conciliacion.Descripcion2, 20, " ");
                    else
                        m_Desc2 = m_CadenasTexto.RellenarCadenaDerecha(m_Conciliacion.Descripcion2, 20, " ");

                    if (m_ValoresPredefinidos != "0")
                    {
                        m_Entidad = m_CadenasTexto.RellenarCadenaDerecha(m_Conciliacion.CodigoEntidad, 4, " ");
                        m_TipoRegistro = m_CadenasTexto.RellenarCadenaDerecha(m_Conciliacion.TipoRegistro, 1, " ");
                        m_FECOPER = m_CadenasTexto.RellenarCadenaDerecha(m_Conciliacion.Fecha.ToString("yyyy-MM-dd"), 10, " ");
                    }

                    m_operar.AgregarLineaArchivo(m_rutaConciliacion, m_NombreArchivo, m_Entidad + m_TipoRegistro + m_CLAVOPEALTA + m_PAN +
                                               m_CODOFIUMO + m_INDEST + m_FECOPER + m_Usuario + m_CODMEN + m_Desc1 + m_Desc2);

                }
            }
            catch (Exception ex)
            {
                throw new Exception("- AgregarTransacciones " + ex.Message);
            }
        }

        /// <summary>
        /// Metodo engardo de agregar la linea final del archivo conciliacion
        /// </summary>
        /// <param name="m_NombreArchivo">Nombre del archivo</param>
        /// <param name="m_ValoresBase">Valores int donde estan los totales</param>
        private void AgregarSumario(string m_NombreArchivo, List<int> m_ValoresBase)
        {
            try
            {
                string m_TotalRegistros = "0";
                string m_totalOk = "0";
                string m_totalNOK = "0";
                if (m_ValoresBase.Count >= 3)
                {
                    m_TotalRegistros = m_ValoresBase[0].ToString();
                    m_totalOk = m_ValoresBase[1].ToString();
                    m_totalNOK = m_ValoresBase[2].ToString();
                }

                m_CODENTSumario = m_CadenasTexto.RellenarCadenaDerecha(m_CODENTSumario, 4, " ");
                m_TIPREGSumario = m_CadenasTexto.RellenarCadenaDerecha(m_TIPREGSumario, 1, " ");
                m_TotalRegistros = m_CadenasTexto.RellenarCadenaIzquierda(m_TotalRegistros, 9, "0");
                m_totalOk = m_CadenasTexto.RellenarCadenaIzquierda(m_totalOk, 9, "0");
                m_totalNOK = m_CadenasTexto.RellenarCadenaIzquierda(m_totalNOK, 9, "0");
                m_operar.AgregarLineaArchivo(m_rutaConciliacion, m_NombreArchivo, m_CODENTSumario + m_TIPREGSumario + m_TotalRegistros +
                                                                  m_totalOk + m_totalNOK);
            }
            catch (Exception ex)
            {
                throw new Exception("- AgregarSumario " + ex.Message);
            }
        }

        #endregion 
    }
}
