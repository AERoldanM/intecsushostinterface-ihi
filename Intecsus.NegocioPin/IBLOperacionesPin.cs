﻿using Intecsus.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Intecsus.NegocioPin
{
    public interface IBLOperacionesPin
    {
        /// <summary>
        /// Metodo encargado de realizar login
        /// </summary>
        /// <param name="id">Identificador del usuario</param>
        /// <param name="contrasena">Contraseña de usuario</param>
        /// <returns>Retorna objeto con respuesta y permisos del usuario</returns>
        /// <remarks> AERM 11/07/2016</remarks>
        RespuestaPIN Ingresar(string id, string contrasena);

        /// <summary>
        /// Metodo encargado de obtener parametros de configuracion y pergiles
        /// </summary>
        /// <param name="userID">Usuario que realiza la operacion</param>
        /// <param name="seguridad">Codigod e seguridad</param>
        /// <returns>Retorna respuesta con los objetos de configuracion y perfiles</returns>
        /// <remarks> AERM 11/07/2016</remarks>
        RespuestaConfig Configuracion(string userID, string seguridad);

        /// <summary>
        /// Metodo encargado de listar los usuarios del sistema
        /// </summary>
        /// <param name="userIDAdmin"></param>
        /// <param name="seguridad"></param>
        /// <returns></returns>
        /// <remarks> AERM 11/07/2016</remarks>
        ResultadoUsuario ListarUsuario(string userIDAdmin, string seguridad);

        /// <summary>
        /// Metodo encargado de crear un usuario en el sistema
        /// </summary>
        /// <param name="userIDAdmin">Id Usuario q realiza operacion</param>
        /// <param name="user">Usuario a crear</param>
        /// <param name="seguridad">Codigo de seguridad</param>
        /// <returns>Retorna si fue creado o no</returns>
        /// <remarks> AERM 11/07/2016</remarks>
        Resultado CrearUsuario(string userIDAdmin,UsuarioPIN user, string seguridad);

        /// <summary>
        /// Metodo encargado de eliminar un usuario 
        /// </summary>
        /// <param name="userIDAdmin">Id usuario que realiza la operacion</param>
        /// <param name="userIDDelete">Id usuario que  se desea eliminar </param>
        /// <param name="seguridad">Codigo de seguridad</param>
        /// <returns>Retorna el resultado de la operacion</returns>
        /// <remarks> AERM 11/07/2016</remarks>
        Resultado EliminarUsuario(string userIDAdmin, string userIDDelete, string seguridad);

        /// <summary>
        /// Metodo encargado de editar un usuario 
        /// </summary>
        /// <param name="userIDAdmin">Id usuario que realiza la operacion</param>
        /// <param name="userIDDelete">Id usuario que  se desea eliminar </param>
        /// <param name="seguridad">Codigo de seguridad</param>
        /// <returns>Retorna el resultado de la operacion</returns>
        /// <remarks> AERM 11/07/2016</remarks>
        Resultado EditarUsuario(string userIDAdmin, UsuarioPIN userEditar, string seguridad);

        /// <summary>
        /// Metodo encargado de asignar contraseña
        /// </summary>
        /// <param name="userIDAdmin">Identificador del usuario que realiza la accion</param>
        /// <param name="contrasenaNueva">Contraseña nueva a almacenar</param>
        /// <param name="confirmacionContrasena">Confirmacion de la contraseña</param>
        /// <param name="userID">Identificador del usuario a cambiar contraseña</param>
        /// <param name="seguridad">Codigo seguridad</param>
        /// <returns>Retorna el resultado de la operacion</returns>
        /// <remarks> AERM 11/07/2016</remarks>
        Resultado AsignarContrasena(string userIDAdmin, string contrasenaNueva, string confirmacionContrasena, string userID, string seguridad);

        /// <summary>
        /// Metodo encargado de cambiar la contraseña del usuario
        /// </summary>
        /// <param name="userID">Identificador del usuario</param>
        /// <param name="contrasena">Contraseña a cmabiar</param>
        /// <param name="seguridad">Codigo de seguridad</param>
        /// <returns>Retorna resultado de la operacion</returns>
        /// <remarks> AERM 11/07/2016</remarks>
        Resultado CambiarContrena(string userID, string contrasenaAntigua, string contrasena, string contrasenaConfirmacion, string userIDAdmin, string seguridad);

        /// <summary>
        /// Metodo encargado de asignar un pin 
        /// </summary>
        /// <param name="CODENT">Codigo de la entidad</param>
        /// <param name="CANAL">COdigo del canal</param>
        /// <param name="userIDAdmin">Usuario admin que realiza la operacion</param>
        /// <param name="USUARIO">Identificador del usuario</param>
        /// <param name="NOMUSR">Nombre de usuario</param>
        /// <param name="pin1">PIn 1</param>
        /// <param name="pin2">PIN 2</param>
        /// <param name="EPAN">Numero de la tarjeta cifrado</param>
        /// <param name="PANSHA">Pan bajo el sha 512</param>
        /// <param name="cliente">Cliente q envia la peticion</param>
        /// <param name="seguridad">Codigo de seguridad</param>
        /// <returns>Retorna resultado de la operacion</returns>
        /// <remarks> AERM 11/07/2016</remarks>
        Resultado AsignacionPin(string CODENT, string CANAL, string userIDAdmin, string USUARIO, string NOMUSR, PIN pin1, PIN pin2,  string EPAN,  string PANSHA, string cliente, string seguridad);

        /// <summary>
        /// Metodo encargado de almacenar en la tabla log
        /// </summary>
        /// <param name="evento">Tipo 1 o 2 segun si es error o no</param>
        /// <param name="descripcion">Desripcion del evento</param>
        /// <remarks>AERM 26/07/2016</remarks>
        Resultado GuardarLOG(int evento, string descripcion);
       
    }
}
