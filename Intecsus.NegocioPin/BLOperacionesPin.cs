﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Intecsus.Entidades;
using Intecsus.DatosPIN;
using System.Configuration;
using Intecsus.ValidadorDUKPT;
using Intecsus.OperarWS;
using Intecsus.Seguridad;

namespace Intecsus.NegocioPin
{
    public class BLOperacionesPin : IBLOperacionesPin
    {
        #region "Variables"
        IDALOperacionesPin m_Operaciones = new DALOperacionesPin();
        #endregion
        #region "Interface"

        public RespuestaPIN Ingresar(string id, string contrasena)
        {
            RespuestaPIN m_Respuesta = new RespuestaPIN();
            m_Respuesta.resultado = new Resultado();
            try
            {
                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- Ingresar usuario " + id, ConfigurationManager.AppSettings["BDPIN"]);
                m_Respuesta.usuario = m_Operaciones.Ingresar(id, contrasena, ConfigurationManager.AppSettings["BDPIN"]);
                if (m_Respuesta.usuario == null)
                {
                    m_Respuesta.resultado.CodRespuesta = "2";
                    m_Respuesta.resultado.Mensaje = this.ObtenerMensaje(m_Respuesta.resultado.CodRespuesta);
                }
                else
                {
                    m_Respuesta.resultado.CodRespuesta = "00";
                    m_Respuesta.usuario.perfil.operaciones = m_Operaciones.OperacionesXUsuario(id, ConfigurationManager.AppSettings["BDPIN"]);
                }

                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- Ingresar usuario Resp " + m_Respuesta.resultado.CodRespuesta + " Mensaje " +
                                            m_Respuesta.resultado.Mensaje, ConfigurationManager.AppSettings["BDPIN"]);
            }
            catch (Exception ex)
            {
                m_Operaciones.GuardarLOG(2, "BLOperacionesPin- Ingresar" + ex.Message, ConfigurationManager.AppSettings["BDPIN"]);
                m_Respuesta.resultado.CodRespuesta = "1";
                m_Respuesta.resultado.Mensaje = this.ObtenerMensaje(m_Respuesta.resultado.CodRespuesta);
            }

            return m_Respuesta;
        }

        public RespuestaConfig Configuracion(string userID, string seguridad)
        {
            RespuestaConfig m_Respuesta = new RespuestaConfig();
            m_Respuesta.resultado = new Resultado();
            try
            {
                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- Configuracion usuario " + userID, ConfigurationManager.AppSettings["BDPIN"]);
                if (this.ValidarSeguridad(userID, userID, seguridad))
                {
                    Operaciones m_Operacion = m_Operaciones.OperacionesXUsuario(userID, ConfigurationManager.AppSettings["BDPIN"]);
                    m_Respuesta.resultado.CodRespuesta = "00";
                    if (m_Operacion.ConsultarConfiguracion)
                        m_Respuesta.configuracion = m_Operaciones.Configuracion(ConfigurationManager.AppSettings["BDPIN"]);
                    else
                    {
                        m_Respuesta.resultado.CodRespuesta = "04";
                        m_Respuesta.resultado.Mensaje = this.ObtenerMensaje(m_Respuesta.resultado.CodRespuesta) + " ConsultarConfiguracion";
                    }

                    if (m_Operacion.ListarPerfiles)
                        m_Respuesta.perfiles = m_Operaciones.Perfiles(ConfigurationManager.AppSettings["BDPIN"]);
                    else
                    {
                        m_Respuesta.resultado.CodRespuesta = "4";
                        m_Respuesta.resultado.Mensaje = this.ObtenerMensaje(m_Respuesta.resultado.CodRespuesta) + " Perfiles";
                    }
                }
                else
                {
                    m_Respuesta.resultado.CodRespuesta = "3";
                    m_Respuesta.resultado.Mensaje = this.ObtenerMensaje(m_Respuesta.resultado.CodRespuesta);
                }

                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- Configuracion Resp " + m_Respuesta.resultado.CodRespuesta + " Mensaje " +
                                           m_Respuesta.resultado.Mensaje, ConfigurationManager.AppSettings["BDPIN"]);
            }
            catch (Exception ex)
            {
                m_Operaciones.GuardarLOG(2, "BLOperacionesPin- Configuracion" + ex.Message, ConfigurationManager.AppSettings["BDPIN"]);
                m_Respuesta.resultado.CodRespuesta = "1";
                m_Respuesta.resultado.Mensaje = this.ObtenerMensaje(m_Respuesta.resultado.CodRespuesta);
            }

            return m_Respuesta;
        }

        public ResultadoUsuario ListarUsuario(string userIDAdmin, string seguridad)
        {
            ResultadoUsuario m_Respuesta = new ResultadoUsuario();
            try
            {
                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- ListarUsuario usuario " + userIDAdmin, ConfigurationManager.AppSettings["BDPIN"]);
                if (this.ValidarSeguridad(userIDAdmin, userIDAdmin, seguridad))
                {
                    Operaciones m_Operacion = m_Operaciones.OperacionesXUsuario(userIDAdmin, ConfigurationManager.AppSettings["BDPIN"]);
                    m_Respuesta.CodRespuesta = "00";
                    if (m_Operacion.ListarUsuario)
                        m_Respuesta.ListaUsuarios = m_Operaciones.ListarUsuario(ConfigurationManager.AppSettings["BDPIN"]);
                    else
                    {
                        m_Respuesta.CodRespuesta = "4";
                        m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta) + " ListarUsuario";
                    }
                }
                else
                {
                    m_Respuesta.CodRespuesta = "3";
                    m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta);
                }

                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- ListarUsuario Resp " + m_Respuesta.CodRespuesta + " Mensaje " +
                                          m_Respuesta.Mensaje, ConfigurationManager.AppSettings["BDPIN"]);
            }
            catch (Exception ex)
            {
                m_Operaciones.GuardarLOG(2, "BLOperacionesPin- ListarUsuario" + ex.Message, ConfigurationManager.AppSettings["BDPIN"]);
                m_Respuesta.CodRespuesta = "1";
                m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta);
            }

            return m_Respuesta;
        }

        public Resultado CrearUsuario(string userIDAdmin, UsuarioPIN user, string seguridad)
        {
            Resultado m_Respuesta = new Resultado();
            try
            {
                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- CrearUsuario userIDAdmin " + userIDAdmin, ConfigurationManager.AppSettings["BDPIN"]);

                string m_Seguridad = user.Nombre + user.Apellido + user.Identificacion + user.Email +
                                     user.perfil.IDPerfil.ToString() + user.perfil.NombrePerfil + user.Telefono + user.contrasena +
                                     userIDAdmin;
                if (this.ValidarSeguridad(userIDAdmin, m_Seguridad, seguridad))
                {
                    Operaciones m_Operacion = m_Operaciones.OperacionesXUsuario(userIDAdmin, ConfigurationManager.AppSettings["BDPIN"]);
                    m_Respuesta.CodRespuesta = "00";
                    if (m_Operacion.CrearUsuario)
                    {
                        if (!this.UsuarioExiste(user.Identificacion))
                            m_Operaciones.CrearUsuario(user, ConfigurationManager.AppSettings["BDPIN"]);
                        else
                        {
                            m_Respuesta.CodRespuesta = "8";
                            m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta);
                        }
                    }

                    else
                    {
                        m_Respuesta.CodRespuesta = "4";
                        m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta) + " CrearUsuario";
                    }
                }
                else
                {
                    m_Respuesta.CodRespuesta = "3";
                    m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta);
                }

                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- CrearUsuario Resp " + m_Respuesta.CodRespuesta + " Mensaje " +
                                          m_Respuesta.Mensaje, ConfigurationManager.AppSettings["BDPIN"]);

            }
            catch (Exception ex)
            {
                m_Operaciones.GuardarLOG(2, "BLOperacionesPin- CrearUsuario" + ex.Message, ConfigurationManager.AppSettings["BDPIN"]);
                m_Respuesta.CodRespuesta = "1";
                m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta);
            }

            return m_Respuesta;
        }


        public Resultado EliminarUsuario(string userIDAdmin, string userIDDelete, string seguridad)
        {
            Resultado m_Respuesta = new Resultado();

            try
            {
                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- EliminarUsuario userIDAdmin " + userIDAdmin, ConfigurationManager.AppSettings["BDPIN"]);
                string m_Seguridad = userIDDelete + userIDAdmin;
                if (this.ValidarSeguridad(userIDAdmin, m_Seguridad, seguridad))
                {
                    Operaciones m_Operacion = m_Operaciones.OperacionesXUsuario(userIDAdmin, ConfigurationManager.AppSettings["BDPIN"]);
                    m_Respuesta.CodRespuesta = "00";
                    if (m_Operacion.EliminarUsuario)
                        m_Operaciones.EliminarUsuario(userIDDelete, ConfigurationManager.AppSettings["BDPIN"]);
                    else
                    {
                        m_Respuesta.CodRespuesta = "4";
                        m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta) + " EliminarUsuario";
                    }
                }
                else
                {
                    m_Respuesta.CodRespuesta = "3";
                    m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta);
                }

                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- EliminarUsuario Resp " + m_Respuesta.CodRespuesta + " Mensaje " +
                                          m_Respuesta.Mensaje, ConfigurationManager.AppSettings["BDPIN"]);
            }
            catch (Exception ex)
            {
                m_Operaciones.GuardarLOG(2, "BLOperacionesPin- EliminarUsuario" + ex.Message, ConfigurationManager.AppSettings["BDPIN"]);
                m_Respuesta.CodRespuesta = "1";
                m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta);
            }

            return m_Respuesta;
        }

        public Resultado EditarUsuario(string userIDAdmin, UsuarioPIN user, string seguridad)
        {
            Resultado m_Respuesta = new Resultado();

            try
            {
                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- EditarUsuario userIDAdmin " + userIDAdmin, ConfigurationManager.AppSettings["BDPIN"]);
                string m_Seguridad = user.Nombre + user.Apellido + user.Email +
                                     user.perfil.IDPerfil.ToString() + user.perfil.NombrePerfil + user.Identificacion + user.contrasena +
                                     userIDAdmin;
                if (this.ValidarSeguridad(userIDAdmin, m_Seguridad, seguridad))
                {
                    Operaciones m_Operacion = m_Operaciones.OperacionesXUsuario(userIDAdmin, ConfigurationManager.AppSettings["BDPIN"]);
                    m_Respuesta.CodRespuesta = "00";
                    if (m_Operacion.ModUsuario)
                        m_Operaciones.EditarUsuario(user, ConfigurationManager.AppSettings["BDPIN"]);
                    else
                    {
                        m_Respuesta.CodRespuesta = "4";
                        m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta) + " EditarUsuario";
                    }
                }
                else
                {
                    m_Respuesta.CodRespuesta = "3";
                    m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta);
                }

                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- EditarUsuario Resp " + m_Respuesta.CodRespuesta + " Mensaje " +
                                          m_Respuesta.Mensaje, ConfigurationManager.AppSettings["BDPIN"]);
            }
            catch (Exception ex)
            {
                m_Operaciones.GuardarLOG(2, "BLOperacionesPin- EditarUsuario" + ex.Message, ConfigurationManager.AppSettings["BDPIN"]);
                m_Respuesta.CodRespuesta = "1";
                m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta);
            }

            return m_Respuesta;
        }

        public Resultado AsignarContrasena(string userIDAdmin, string contrasenaNueva, string confirmacionContrasena, string userID, string seguridad)
        {
            Resultado m_Respuesta = new Resultado();
            try
            {
                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- AsignarContrasena userIDAdmin " + userIDAdmin, ConfigurationManager.AppSettings["BDPIN"]);
                string m_Seguridad = userID + contrasenaNueva + confirmacionContrasena + userIDAdmin;
                if (this.ValidarSeguridad(userIDAdmin, m_Seguridad, seguridad))
                {
                    Operaciones m_Operacion = m_Operaciones.OperacionesXUsuario(userIDAdmin, ConfigurationManager.AppSettings["BDPIN"]);
                    m_Respuesta.CodRespuesta = "00";
                    if (contrasenaNueva != confirmacionContrasena)
                    {
                        m_Respuesta.CodRespuesta = "5";
                        m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta);
                    }
                    else if (m_Operacion.AsignarContrasena)
                        m_Operaciones.AsignarContrasena(contrasenaNueva, userID, ConfigurationManager.AppSettings["BDPIN"]);
                    else
                    {
                        m_Respuesta.CodRespuesta = "4";
                        m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta) + " AsignarContrasena";
                    }
                }
                else
                {
                    m_Respuesta.CodRespuesta = "3";
                    m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta);
                }

                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- AsignarContrasena Resp " + m_Respuesta.CodRespuesta + " Mensaje " +
                                         m_Respuesta.Mensaje, ConfigurationManager.AppSettings["BDPIN"]);
            }
            catch (Exception ex)
            {
                m_Operaciones.GuardarLOG(2, "BLOperacionesPin- AsignarContrasena" + ex.Message, ConfigurationManager.AppSettings["BDPIN"]);
                m_Respuesta.CodRespuesta = "1";
                m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta);
            }

            return m_Respuesta;
        }

        public Resultado CambiarContrena(string userID, string contrasenaAntigua, string contrasena, string contrasenaConfirmacion, string userIDAdmin, string seguridad)
        {
            Resultado m_Respuesta = new Resultado();
            try
            {
                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- CambiarContrasena userID " + userID, ConfigurationManager.AppSettings["BDPIN"]);
                string m_Seguridad = userID + contrasenaAntigua + contrasena + contrasenaConfirmacion + userIDAdmin;
                if (this.ValidarSeguridad(userIDAdmin, m_Seguridad, seguridad))
                {
                    Operaciones m_Operacion = m_Operaciones.OperacionesXUsuario(userID, ConfigurationManager.AppSettings["BDPIN"]);
                    m_Respuesta.CodRespuesta = "00";
                    if (m_Operacion.CambiarContrasena)
                    {
                        RespuestaPIN m_RespuestaI = this.Ingresar(userID, contrasenaAntigua);
                        if (m_RespuestaI.resultado.CodRespuesta == "00")
                        {
                            if (contrasena == contrasenaConfirmacion)
                                m_Operaciones.CambiarContrena(userID, contrasena, ConfigurationManager.AppSettings["BDPIN"]);
                            else
                            {
                                m_Respuesta.CodRespuesta = "5";
                                m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta);
                            }
                        }
                        else
                        {
                            m_Respuesta.CodRespuesta = "7";
                            m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta);
                        }
                    }
                    else
                    {
                        m_Respuesta.CodRespuesta = "4";
                        m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta) + " CambiarContrena";
                    }
                }
                else
                {
                    m_Respuesta.CodRespuesta = "3";
                    m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta);
                }

                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- CambiarContrasena Resp " + m_Respuesta.CodRespuesta + " Mensaje " +
                                        m_Respuesta.Mensaje, ConfigurationManager.AppSettings["BDPIN"]);
            }
            catch (Exception ex)
            {
                m_Operaciones.GuardarLOG(2, "BLOperacionesPin- CambiarContrasena" + ex.Message, ConfigurationManager.AppSettings["BDPIN"]);
                m_Respuesta.CodRespuesta = "1";
                m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta);
            }

            return m_Respuesta;
        }

        public Resultado AsignacionPin(string CODENT, string CANAL, string userIDAdmin, string USUARIO, string NOMUSR, PIN pin1, PIN pin2, string EPAN, string PANSHA, string cliente, string seguridad)
        {
            Resultado m_Respuesta = new Resultado();
            try
            {
                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- AsignacionPin userID " + userIDAdmin, ConfigurationManager.AppSettings["BDPIN"]);
                string m_Seguridad = pin1.PinBlock + pin1.KSN + pin2.PinBlock + pin2.KSN + PANSHA + EPAN + userIDAdmin;
                if (this.ValidarSeguridad(userIDAdmin, m_Seguridad, seguridad))
                {
                    Operaciones m_Operacion = m_Operaciones.OperacionesXUsuario(userIDAdmin, ConfigurationManager.AppSettings["BDPIN"]);
                    m_Respuesta.CodRespuesta = "00";
                    if (m_Operacion.AsignarPin)
                    {
                        return this.EnviarSolicitudPin(CODENT, CANAL, userIDAdmin, USUARIO, NOMUSR, pin1, pin2, EPAN, PANSHA, cliente);

                    }
                    else
                    {
                        m_Respuesta.CodRespuesta = "4";
                        m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta) + " AsignarPin";
                    }

                }
                else
                {
                    m_Respuesta.CodRespuesta = "3";
                    m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta);
                }

                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- AsignacionPin Resp " + m_Respuesta.CodRespuesta + " Mensaje " +
                                        m_Respuesta.Mensaje, ConfigurationManager.AppSettings["BDPIN"]);
            }
            catch (Exception ex)
            {
                m_Operaciones.GuardarLOG(2, "BLOperacionesPin- AsignacionPin" + ex.Message, ConfigurationManager.AppSettings["BDPIN"]);
                m_Respuesta.CodRespuesta = "1";
                m_Respuesta.Mensaje = ex.Message;
            }

            return m_Respuesta;
        }

        public Resultado GuardarLOG(int evento, string descripcion)
        {
            Resultado m_Respuesta = new Resultado();
            try
            {
                m_Operaciones.GuardarLOG(evento, descripcion, ConfigurationManager.AppSettings["BDPIN"]);
                m_Respuesta.CodRespuesta = "00";
            }
            catch (Exception ex)
            {
                m_Operaciones.GuardarLOG(2, "BLOperacionesPin- GuardarLOG" + ex.Message, ConfigurationManager.AppSettings["BDPIN"]);
                m_Respuesta.CodRespuesta = "1";
                m_Respuesta.Mensaje = ex.Message;
            }

            return m_Respuesta;
        }

        #endregion

        #region "Metodos privados"

        /// <summary>
        /// Metodo encargado de encontrar un mensaje de error
        /// </summary>
        /// <param name="codigoMensaje">Codigo del mensaje</param>
        /// <returns>Retorna mensaje</returns>
        private string ObtenerMensaje(string codigoMensaje)
        {
            try
            {
                return m_Operaciones.ObtenerMensaje(codigoMensaje, ConfigurationManager.AppSettings["BDPIN"]);
            }
            catch (Exception ex)
            {
                return "Error a realizar la acción revisar log APP";
            }
        }

        /// <summary>
        /// Metodo encargado de validar si cumple con la seguridad indicada
        /// </summary>
        /// <param name="idUsuario">Usuario el cual se debe buscar la contrasena</param>
        /// <param name="parametros">Parametros enviados por el cliente</param>
        /// <param name="seguridad">Seguridad enviada por el cliente</param>
        /// <returns>Retorna si la validacion fue correcta</returns>
        /// <remarks>AERM 12/07/2016</remarks>
        private bool ValidarSeguridad(string idUsuario, string parametros, string seguridad)
        {
            try
            {
                HashEncriptar m_Cifrado = new HashEncriptar("NoImporta");
                string m_Seguridad = m_Cifrado.SHA512(parametros + m_Operaciones.ObtenerContrasenaXUsuario(idUsuario, ConfigurationManager.AppSettings["BDPIN"]));
                if (m_Seguridad.ToUpper() == seguridad)
                    return true;
            }
            catch (Exception ex)
            {
                m_Operaciones.GuardarLOG(2, "BLOperacionesPin- ValidarSeguridad" + ex.Message, ConfigurationManager.AppSettings["BDPIN"]);
            }

            return false;
        }

        /// <summary>
        /// Metodo encargado de validar si un usuario existe
        /// </summary>
        /// <param name="identificadorUsuario">Identificador del usuario (Parametro de busqueda)</param>
        /// <returns>Retorna si el usuario existe o no</returns>
        private bool UsuarioExiste(string identificadorUsuario)
        {
            try
            {
                string m_Contrasena = m_Operaciones.ObtenerContrasenaXUsuario(identificadorUsuario, ConfigurationManager.AppSettings["BDPIN"]);
                if (!String.IsNullOrEmpty(m_Contrasena))
                    return true;
            }
            catch (Exception ex)
            {
                m_Operaciones.GuardarLOG(2, "BLOperacionesPin- UsuarioExiste" + ex.Message, ConfigurationManager.AppSettings["BDPIN"]);
            }

            return false;
        }


        /// <summary>
        /// Metodo encargado de asignar un pin 
        /// </summary>
        /// <param name="CODENT">Codigo de la entidad</param>
        /// <param name="CANAL">COdigo del canal</param>
        /// <param name="userIDAdmin">Usuario admin que realiza la operacion</param>
        /// <param name="USUARIO">Identificador del usuario</param>
        /// <param name="NOMUSR">Nombre de usuario</param>
        /// <param name="pin1">PIn 1</param>
        /// <param name="pin2">PIN 2</param>
        /// <param name="EPAN">Numero de la tarjeta cifrado</param>
        /// <param name="PANSHA">Pan bajo el sha 512</param>
        /// <param name="cliente">Cliente q envia la peticion</param>
        /// <returns>Retorna resultado de la operacion</returns>
        /// <remarks> AERM 11/07/2016</remarks>
        private Resultado EnviarSolicitudPin(string CODENT, string CANAL, string userIDAdmin, string USUARIO, string NOMUSR, PIN pin1, PIN pin2, string EPAN, string PANSHA, string cliente)
        {
            Resultado m_Respuesta = null;
            try
            {
                int m_IdTransaccion = 0;
                string m_CodEntidadOpAlta = string.Empty;
                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- EnviarSolicitudPin " + CODENT + CANAL + userIDAdmin + EPAN + "-" + PANSHA, ConfigurationManager.AppSettings["BDPIN"]);
                m_Respuesta = this.ValidarDUKPT(EPAN, pin1, pin2);
                Conciliacion m_Conciliacion = this.ObtenerConciliacion(PANSHA.ToLower(), cliente);
                if (Convert.ToInt32(m_Respuesta.CodRespuesta) == 0)
                {
                    if (m_Conciliacion == null)
                    {
                        m_IdTransaccion = m_Operaciones.InsertarTransaccion(CODENT, EPAN, ConfigurationManager.AppSettings["BD"]);
                        m_CodEntidadOpAlta = EPAN;
                    }
                    else
                    {
                        m_IdTransaccion = m_Conciliacion.IdTransaccion;
                        m_CodEntidadOpAlta = m_Conciliacion.IDTransaccionCliente;
                    }

                    m_Respuesta = this.EnviarNotificacionPIN(CODENT, CANAL, m_IdTransaccion.ToString(), m_CodEntidadOpAlta, USUARIO.ToString(), NOMUSR, pin1.PinBlock, pin1.KSN, 0);


                    if (m_Respuesta != null && m_Conciliacion != null)
                    {
                        m_Operaciones.ActializarEstadoAsignacionPIN(m_Conciliacion.IdTransaccion.ToString(), ConfigurationManager.AppSettings["BD"]);
                    }
                    else if (m_Respuesta == null)
                    {
                        m_Respuesta = new Resultado();
                        m_Respuesta.CodRespuesta = "6";
                        m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta);
                    }
                }

                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- EnviarSolicitudPin - RESPUESTA " + m_Respuesta.CodRespuesta + "- " + m_Respuesta.Mensaje
                       , ConfigurationManager.AppSettings["BDPIN"]);
            }
            catch (Exception ex)
            {
                throw new Exception("-EnviarSolicitudPin " + ex.Message);
            }

            return m_Respuesta;
        }

        /// <summary>
        /// Metodo encargado de obtener la conciliacion de un cliente y validar si cumple para asignar PIN
        /// </summary>
        /// <param name="SHAPan">Sha del pan en minusculas</param>
        /// <param name="cliente">Cliente que realiza la peticion</param>
        /// <returns>Retorna si existe conciliacion</returns>
        /// <remarks>AERM 28/09/2016</remarks>
        private Conciliacion ObtenerConciliacion(string SHAPan, string cliente)
        {
            try
            {
                Conciliacion m_Conciliacion = m_Operaciones.ObtenerConciliacion(SHAPan, cliente, ConfigurationManager.AppSettings["BD"]);
                if (m_Conciliacion != null ) 
                {
                    if (m_Conciliacion.INDES == "OK" && m_Conciliacion.PIN == false)
                        return m_Conciliacion;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("-ObtenerConciliacion " + ex.Message);
            }

            return null;
        }

        /// <summary>
        /// Metodo que consume el WS cliente para asignar un pin
        /// </summary>
        /// <param name="CODENT">Entidad Bancaria</param>
        /// <param name="CANAL">Canal segun especificacion 390</param>
        /// <param name="CLAVEOP">Clave operacion Cliente</param>
        /// <param name="CLAVEOPA">Clave Operacion CW (id nuestro)</param>
        /// <param name="USUARIO">Numero de usuario</param>
        /// <param name="DESNOMUSR">Nombre usuario</param>
        /// <param name="EPINBLOCK">PINBLOCK con encripcion</param>
        /// <param name="KSN">Serial de la transaccion (consecutivo) del pin</param>
        /// <param name="idTransaccion">Identificador de la transaccion</param>
        /// <returns>Retorna si la peticion fue enviada correctamente</returns>
        private Resultado EnviarNotificacionPIN(string CODENT, string CANAL, string CLAVEOP, string m_CodEntidadOpAlta, string USUARIO, string NOMUSR, string EPINBLOCK, string KSN, int idTransaccion)
        {
            Resultado m_Respuesta = null;
            try
            {
                Int32 m_Reintentos = Convert.ToInt32(m_Operaciones.ObtenerValorConfiguracion("ReintentosAsignacionPIN", ConfigurationManager.AppSettings["BDPIN"]));
                while (m_Reintentos > 0)
                {
                    Intecsus.OperarWS.Implementaciones.ImplementacionSantander m_WSExterno = new Intecsus.OperarWS.Implementaciones.ImplementacionSantander();
                    try
                    {
                        m_Respuesta = m_WSExterno.AsignarPIN(CODENT, CANAL, CLAVEOP, m_CodEntidadOpAlta, USUARIO, NOMUSR, EPINBLOCK, KSN, idTransaccion);
                        break;
                    }
                    catch (Exception ex)
                    {
                        m_Operaciones.GuardarLOG(1, "BLOperacionesPin- EnviarNotificacionPIN - errorWS  reintento " + ex.Message, ConfigurationManager.AppSettings["BDPIN"]);
                    }

                    m_Reintentos--;
                }
            }
            catch (Exception ex)
            {
                m_Operaciones.GuardarLOG(1, "BLOperacionesPin- EnviarNotificacionPIN - error " + ex.Message, ConfigurationManager.AppSettings["BDPIN"]);
            }

            return m_Respuesta;
        }


        /// <summary>
        /// Metodo encargado de realizar la validación de DUKPT
        /// </summary>
        /// <param name="EPAN">Pan cifrado</param>
        /// <param name="pin1">Pin creado</param>
        /// <param name="pin2">COnfifrmacion del pin</param>
        /// <returns>Retorna resultado de la validacion</returns>
        /// <remarks>AERM 18/08/2016</remarks>
        private Resultado ValidarDUKPT(string EPAN, PIN pin1, PIN pin2)
        {
            Resultado m_Respuesta = new Resultado();
            Int32 respuesta = 0;
            try
            {
                string m_Validar = ConfigurationManager.AppSettings["EnviarDUKPT"].ToUpper();
                if (m_Validar.Equals("SI"))
                {
                    FactoryMethod m_frabrica = new FactoryMethod();
                    Intecsus.ValidadorDUKPT.ValidadorDUKPT m_Validador = m_frabrica.DevolverClase(ConfigurationManager.AppSettings["DUKPT"]);
                    respuesta = m_Validador.ValidarDUKPT(EPAN, pin1.PinBlock, pin1.KSN, pin2.PinBlock, pin2.KSN);
                    m_Operaciones.GuardarLOG(1, "BLOperacionesPin-  -ValidadorDUKPT respuestaCodigo " + respuesta.ToString(), ConfigurationManager.AppSettings["BDPIN"]);
                }

                m_Respuesta.CodRespuesta = respuesta.ToString();
                m_Respuesta.Mensaje = this.ObtenerMensaje(m_Respuesta.CodRespuesta);
            }
            catch (Exception ex)
            {
                throw new Exception("-ValidarDUKPT " + ex.Message);
            }

            return m_Respuesta;
        }

        #endregion
    }
}
