﻿Imports System.Messaging
Imports Intecsus.Entidades
Imports System.Configuration

''' <summary>
''' Implenta la interfaz  de colas para MSMQ
''' </summary>
''' <remarks>AERM 20/01/2015</remarks>
Public Class ColaMSMQ
    Implements ICola

#Region "Implementacion Interface"

    Public Function CrearCola(colaNombre As String) As Boolean Implements ICola.CrearCola
        Try
            Me.ValidarCola(colaNombre)
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function EnviarMensajeCola(colaNombre As String, mensaje As Object) As Boolean Implements ICola.EnviarMensajeCola
        Dim _msMq As MessageQueue = Nothing
        Try
            If Me.ValidarCola(colaNombre) Then
                _msMq = New MessageQueue(colaNombre)
                _msMq.Send(mensaje)
                Return True
            End If
        Catch ex As Exception
            Throw ex
        Finally
            Me.CerrarCola(_msMq)
        End Try

        Return False
    End Function

    Public Function EviarMensajeCola(colaNombre As String, Mensaje As String) As Boolean Implements ICola.EviarMensajeCola
        Dim _msMq As MessageQueue = Nothing
        Try
            If Me.ValidarCola(colaNombre) Then
                _msMq = New MessageQueue(colaNombre)
                _msMq.DefaultPropertiesToSend.Recoverable = True
                _msMq.Send(Mensaje)
                Return True
            End If
        Catch ex As Exception
            Throw ex
        Finally
            Me.CerrarCola(_msMq)
        End Try

        Return False
    End Function

    Public Function EliminarCola(colaNombre As String) As Boolean Implements ICola.EliminarCola
        Try
            If Me.ValidarCola(colaNombre) Then
                MessageQueue.Delete(colaNombre)
                Return True
            End If

            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function EviarMensajeCola1(colaNombre As String, Mensaje As String, label As String) As Boolean Implements ICola.EviarMensajeCola

        Dim _msMq As MessageQueue = Nothing
        Try
            If Me.ValidarCola(colaNombre) Then
                _msMq = New MessageQueue(colaNombre)
                _msMq.DefaultPropertiesToSend.Recoverable = True
                _msMq.Send(Mensaje, label)
                Return True
            End If
        Catch ex As Exception
            Throw ex
        Finally
            Me.CerrarCola(_msMq)
        End Try

        Return False
    End Function

    Public Function RecibirMensaje(colaNombre As String) As String Implements ICola.RecibirMensaje

        Dim _msMq As MessageQueue = Nothing
        Try
            If Me.ValidarCola(colaNombre, True) Then
                _msMq = New MessageQueue(colaNombre)
                Dim _Message As Message = _msMq.Receive(New TimeSpan(1000))
                _Message.Formatter = New XmlMessageFormatter(New [String]() {"System.String,mscorlib"})
                Return _Message.Body
            End If
        Catch ex As MessageQueueException
            Throw ex
        Catch ex As Exception
            Throw ex
        Finally
            Me.CerrarCola(_msMq)
        End Try

        Return String.Empty
    End Function


#Region "Objeto especiales"
    Public Function GuardarObjetoCardWizard(cardWizard As DatosCardWizard, colaNombre As String) As Boolean Implements ICola.GuardarObjetoCardWizard
        Dim _msMq As MessageQueue = Nothing
        Try
            If Me.ValidarCola(colaNombre) Then
                _msMq = New MessageQueue(colaNombre)
                _msMq.DefaultPropertiesToSend.Recoverable = True
                _msMq.Send(cardWizard)
                Return True
            End If
        Catch ex As Exception
            Throw ex
        Finally
            Me.CerrarCola(_msMq)
        End Try

        Return False
    End Function

    Public Function RecibirObjetoCardWizard(colaNombre As String, ByRef respuesta As DatosCardWizard) As Boolean Implements ICola.RecibirObjetoCardWizard
        Dim _msMq As MessageQueue = Nothing
        Try
            Dim targetTypes() As Type
            targetTypes = New Type() {GetType(DatosCardWizard)}

            If Me.ValidarCola(colaNombre, True) Then
                _msMq = New MessageQueue(colaNombre)
                _msMq.Formatter = New XmlMessageFormatter(targetTypes)
                Dim _Message As Message = _msMq.Receive(New TimeSpan(1000))
                respuesta = CType(_Message.Body, DatosCardWizard)
                Return True
            End If
        Catch ex As MessageQueueException
            Throw ex
        Catch ex As Exception
            Throw ex
        Finally
            Me.CerrarCola(_msMq)
        End Try

        Return False
    End Function

    Public Function GuardarObjetoNotificacion(notificacion As RespuestaDeviceStatus, colaNombre As String) As Boolean Implements ICola.GuardarObjetoNotificacion
        Dim _msMq As MessageQueue = Nothing
        Try
            If Me.ValidarCola(colaNombre) Then
                _msMq = New MessageQueue(colaNombre)
                _msMq.DefaultPropertiesToSend.Recoverable = True
                _msMq.Send(notificacion)
                Return True
            End If
        Catch ex As Exception
            Throw ex
        Finally
            Me.CerrarCola(_msMq)
        End Try

        Return False
    End Function

    Public Function RecibirObjetoNotificar(colaNombre As String, ByRef respuesta As RespuestaDeviceStatus) As Boolean Implements ICola.RecibirObjetoNotificar
        Dim _msMq As MessageQueue = Nothing
        Try
            Dim targetTypes() As Type
            targetTypes = New Type() {GetType(RespuestaDeviceStatus)}

            If Me.ValidarCola(colaNombre, True) Then
                _msMq = New MessageQueue(colaNombre)
                _msMq.Formatter = New XmlMessageFormatter(targetTypes)
                Dim _Message As Message = _msMq.Receive(New TimeSpan(1000))
                respuesta = CType(_Message.Body, RespuestaDeviceStatus)
                Return True
            End If
        Catch ex As MessageQueueException
            Throw ex
        Catch ex As Exception
            Throw ex
        Finally
            Me.CerrarCola(_msMq)
        End Try

        Return False
    End Function

    Public Function GuardarObjetoPeticionCliente(peticion As Peticion, colaNombre As String) As Boolean Implements ICola.GuardarObjetoPeticionCliente
        Dim _msMq As MessageQueue = Nothing
        Try
            If Me.ValidarCola(colaNombre) Then
                _msMq = New MessageQueue(colaNombre)
                _msMq.DefaultPropertiesToSend.Recoverable = True
                _msMq.Send(peticion)
                Return True
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message + "- " + colaNombre + "- " + peticion.BIN)
        Finally
            Me.CerrarCola(_msMq)
        End Try

        Return False
    End Function

    Public Function RecibirPeticionCliente(colaNombre As String, ByRef respuesta As Peticion) As Boolean Implements ICola.RecibirPeticionCliente
        Dim _msMq As MessageQueue = Nothing
        Try
            Dim targetTypes() As Type
            targetTypes = New Type() {GetType(Peticion)}

            If Me.ValidarCola(colaNombre, True) Then
                _msMq = New MessageQueue(colaNombre, True)
                _msMq.Formatter = New XmlMessageFormatter(targetTypes)
                Dim _Message As Message = _msMq.Receive(New TimeSpan(1000))
                respuesta = CType(_Message.Body, Peticion)
                Return True
            End If
        Catch ex As MessageQueueException
            Throw ex
        Catch ex As Exception
            Throw ex
        Finally
            Me.CerrarCola(_msMq)
        End Try

        Return False
    End Function

#End Region

#End Region

#Region "Metodos Privados"

    ''' <summary>
    ''' Metodo encargado de validar si la cola existe
    ''' </summary>
    ''' <param name="colaNombre">nombre de la cola a validar</param>
    ''' <returns>Retorna si la cola existe o fue creada</returns>
    ''' <remarks>AERM  21/01/2015</remarks>
    Private Function ValidarCola(colaNombre As String, Optional esperaMensaje As Boolean = False) As Boolean
        Try
            Dim m_Respuesta = True
            'Valida q la cola exista si no la crea
            If Not MessageQueue.Exists(colaNombre) Then
                MessageQueue.Create(colaNombre)
                Try
                    Me.AgregarPermisos(colaNombre, "EVERYONE")
                Catch ex As Exception
                    Me.AgregarPermisos(colaNombre, "TODOS")
                End Try
            End If

            If (esperaMensaje) Then
                Dim m_Cola = New MessageQueue(colaNombre)
                m_Respuesta = Me.MensajesXCola(m_Cola)
                m_Cola.Close()
            End If

            Return m_Respuesta
        Catch ex As Exception
            Throw New Exception(ex.Message + " " + If(Not MessageQueue.Exists(colaNombre), "No existe", "Si existe"))
        End Try
    End Function

    Private Sub AgregarPermisos(colaNombre As String, usuario As String)
        Try
            If ConfigurationManager.AppSettings("PermisosCola").ToUpper() <> "EVERYONE" Then
                Return
            End If

            Dim _msMq As MessageQueue = New MessageQueue(colaNombre)
            Dim list As New AccessControlList()
            ' Create a new trustee to represent the "Everyone" user group.
            Dim tr As New Trustee(usuario)
            ' Create an AccessControlEntry, granting the trustee read access to
            ' the queue.
            Dim entry As New AccessControlEntry(tr, GenericAccessRights.Read, StandardAccessRights.Read, AccessControlEntryType.Allow)
            ' Add the AccessControlEntry to the AccessControlList.
            list.Add(entry)
            _msMq.SetPermissions(list)
        Catch ex As Exception
            Throw New Exception(ex.Message + " agregar permisos")
        End Try
    End Sub

    ''' <summary>
    ''' Metodo encargado de retornar si una cola tiene mensajes pentientes por procesar
    ''' </summary>
    ''' <param name="_msMq">Cola a mirar</param>
    ''' <returns>Retorna true si tiene uno o mas peticiones pendientes</returns>
    ''' <remarks>AERM 24/09/2015</remarks>
    Private Function MensajesXCola(_msMq As MessageQueue) As Boolean
        Dim m_NumPeticiones As Integer = _msMq.GetAllMessages().Length()
        If (m_NumPeticiones > 0) Then
            Return True
        End If

        Return False
    End Function

    ''' <summary>
    ''' Metodo encargado de cerrar la cola (Si se deja abierta es excepción porq no puede ingresar)
    ''' </summary>
    ''' <param name="msMq">Cola a cerrar</param>
    ''' <remarks>AERM 24/09/2015</remarks>
    Private Sub CerrarCola(msMq As MessageQueue)
        If (msMq Is Nothing) = False Then
            msMq.Close()
        End If
    End Sub

#End Region
End Class
