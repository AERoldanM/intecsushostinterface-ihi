﻿''' <summary>
''' Clase encargada de instanciar las operacione segun programa de almacenamiento de colas
''' </summary>
''' <remarks>AERM 20/01/2015</remarks>
Public Class InstanciaCola

    ''' <summary>
    ''' Metodo que se encarga de instaciar la cola
    ''' </summary>
    ''' <param name="colaProgram">Progrmaa de la cola</param>
    ''' <returns>Objeto con las operaciones de la cola</returns>
    ''' <remarks>AERM 20/01/2015</remarks>
    Public Function IntanciarCola(colaProgram As String) As ICola
        Try
            Select Case colaProgram
                Case "MSMQ"
                    Return New ColaMSMQ()
                Case Else
                    Throw New Exception("No está disponible para ese archivo. (Gestorarchivo - utilidades)")
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class
