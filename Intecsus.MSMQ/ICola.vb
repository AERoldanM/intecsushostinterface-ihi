﻿Imports Intecsus.Entidades

''' <summary>
''' Representa la operaciones sobre las colas
''' </summary>
''' <remarks>AERM 20/01/2015</remarks>
Public Interface ICola

    ''' <summary>
    ''' Metodo que se encarga de crear una cola
    ''' </summary>
    ''' <param name="colaNombre">Nombre de la cola ejm: @".\private$\TestQueue"</param>
    ''' <returns>Retorna true si la cola fue creada o false si la cola ya existe</returns>
    Function CrearCola(colaNombre As String) As Boolean

    ''' <summary>
    ''' Metodo que se encarga de enviar un mensaje a la cola
    ''' </summary>
    ''' <param name="colaNombre">Nombre de la cola</param>
    ''' <param name="Mensaje">String con el mensaje</param>
    ''' <returns>Retorna si el mensaje se creo satisfactoriamente</returns>
    ''' <remarks>AERM 10/01/2015</remarks>
    Function EviarMensajeCola(colaNombre As String, Mensaje As String) As Boolean

    ''' <summary>
    ''' Metodo encarga de enviar un mesaje a la cola recibiendo un objeto
    ''' </summary>
    ''' <param name="colaNombre">Nombre de la cola</param>
    ''' <param name="mensaje">Mensjae a enviar en formato Object</param>
    ''' <returns>Retorna si el mensaje se creo satisfactoriamente</returns>
    ''' <remarks>AERM 10/01/2015</remarks>
    Function EnviarMensajeCola(colaNombre As String, mensaje As Object) As Boolean

    ''' <summary>
    ''' Metodo que se encarga de enviar un mensaje a la cola
    ''' </summary>
    ''' <param name="colaNombre">Nombre de la cola</param>
    ''' <param name="Mensaje">String con el mensaje</param>
    ''' <param name="label">label o nombre de la cola</param>
    ''' <returns>Retorna si el mensaje se creo satisfactoriamente</returns>
    ''' <remarks>AERM 21/01/2015</remarks>
    Function EviarMensajeCola(colaNombre As String, Mensaje As String, label As String) As Boolean

    ''' <summary>
    ''' Metodo encargado de retornar informacion que tenga la cola
    ''' </summary>
    ''' <param name="colaNombre">Nombre de la cola</param>
    ''' <returns>Retorna un string con el mensaje de la cola</returns>
    Function RecibirMensaje(colaNombre As String) As String


    ''' <summary>
    ''' Metodo encaragdo de Eliminar una cola
    ''' </summary>
    ''' <param name="colaNombre">Nombre de la cola a Eliminar</param>
    ''' <returns>Retorna si la cola fue Eliminada</returns>
    ''' <remarks>AERM 21/01/2015</remarks>
    Function EliminarCola(colaNombre As String) As Boolean

#Region "Objeto especiales"

    ''' <summary>
    '''  Metodo encargado de guardar informacion en la cola
    ''' </summary>
    ''' <param name="cardWizard">Objeto cardWizard a guardar</param>
    ''' <param name="colaNombre">Nombre de la cola</param>
    ''' <returns>Retorna si se almaceno el valor</returns>
    ''' <remarks>AERM 02/02/2015</remarks>
    Function GuardarObjetoCardWizard(cardWizard As DatosCardWizard, colaNombre As String) As Boolean

    ''' <summary>
    ''' Metodo encargadode retornar mensaje almacenado en la cola Objeto CW
    ''' </summary>
    ''' <param name="colaNombre">Nombre de la cola</param>
    ''' <param name="respuesta">Objeto respuesta de la cola</param>
    ''' <returns>Retorna el objeto alamcenado</returns>
    ''' <remarks>AERM 02/02/2015</remarks>
    Function RecibirObjetoCardWizard(colaNombre As String, ByRef respuesta As DatosCardWizard) As Boolean

    ''' <summary>
    ''' Metodo encargado de guardar en la cola una notificacion
    ''' </summary>
    ''' <param name="notificacion">Objeto con los valores necesarios</param>
    ''' <param name="colaNombre">Nombre de la cola </param>
    ''' <returns>retorna si fue almacenada la notificacion</returns>
    ''' <remarks>AERM 18/03/2015</remarks>
    Function GuardarObjetoNotificacion(notificacion As RespuestaDeviceStatus, colaNombre As String) As Boolean

    ''' <summary>
    ''' Metodi encargado de recibir las notificaciones pertinentes
    ''' </summary>
    ''' <param name="colaNombre">Nombre de la cola</param>
    ''' <param name="respuesta">Objeto notificacion</param>
    ''' <returns>Retorna si el proceso fue correcto</returns>
    ''' <remarks>AERM 18/03/2015</remarks>
    Function RecibirObjetoNotificar(colaNombre As String, ByRef respuesta As RespuestaDeviceStatus) As Boolean

    ''' <summary>
    ''' Metodo encargado de recibir un peticion del cliente
    ''' </summary>
    ''' <param name="peticion">Objeto a guardar</param>
    ''' <param name="colaNombre">Nombre de la cola</param>
    ''' <returns>Si se agrego la peticion</returns>
    ''' <remarks>AERM 19/03/2015</remarks>
    Function GuardarObjetoPeticionCliente(peticion As Peticion, colaNombre As String) As Boolean

    ''' <summary>
    ''' Metodo encargado de enviar una peticion guardada en la cola
    ''' </summary>
    ''' <param name="colaNombre">Nombre de la cola cliente</param>
    ''' <param name="respuesta">Respuesta obejto peticion</param>
    ''' <returns>Retorna Si se creo la cola</returns>
    ''' <remarks>AERM 19/03/2015</remarks>
    Function RecibirPeticionCliente(colaNombre As String, ByRef respuesta As Peticion) As Boolean

#End Region

End Interface
