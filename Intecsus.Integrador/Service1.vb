﻿Imports Intecsus.OperarCMS
Imports System.Timers
Imports Intecusus.OperarBD
Imports System.Configuration
Imports Intecsus.Conciliacion


Public Class Service1

    Private WithEvents proceso As ProcesoBase = New ProcesoBase()
    Private timer As Timer = New Timer()
    Private conciliar As IBLConciliacion
    Private operacionesBD As OperacionesBase

    Public Sub TestStartupAndStop(args As String())
        Me.OnStart(args)
        Console.ReadLine()
        ''Me.OnStop()
    End Sub

    Protected Overrides Sub OnStart(ByVal args() As String)
        ' Add code here to start your service. This method should set things
        ' in motion so your service can do its work.
        operacionesBD = New OperacionesBase()
        Try
            conciliar = New BLConciliacion()
            conciliar.InicializarVariables()
            proceso.InicilizarVariables()
            timer.Interval = proceso.RetornarTiempo()
            AddHandler timer.Elapsed, AddressOf timer_Elapsed
            timer.Start()
            operacionesBD.GuardarLogXNombreConectionString(1, 0, 1, "Service1.vb Integrador- OnStart  ",
                                                          ConfigurationManager.AppSettings("BD"))
        Catch ex As Exception

            operacionesBD.GuardarLogXNombreConectionString(1, 0, 2, "Service1.vb Integrador- OnStart : " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        operacionesBD.GuardarLogXNombreConectionString(1, 0, 1, "Service1.vb Integrador- OnSTOP : ",
                                                      ConfigurationManager.AppSettings("BD"))
    End Sub

#Region "Eventos"

    Private Sub timer_Elapsed(sender As Object, e As ElapsedEventArgs)
        Try
            timer.Stop()
            conciliar.ProcesoConciliar()
            proceso.InicilizarVariables()
            proceso.ComenzarProceso()
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(1, 0, 2, "Service1.vb Integrador- timer_Elapsed : " + ex.Message,
                                                          ConfigurationManager.AppSettings("BD"))
        End Try
    End Sub

    Private Sub InicializarTimer(ByVal sender As Object, ByVal tiempo As Integer) Handles proceso.TerminoProceso
        timer = New Timer()
        timer.Interval = proceso.RetornarTiempo()
        AddHandler timer.Elapsed, AddressOf timer_Elapsed
        timer.Start()
    End Sub

#End Region

End Class
