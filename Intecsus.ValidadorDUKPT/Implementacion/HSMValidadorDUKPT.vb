﻿Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Configuration

Public Class HSMValidadorDUKPT
    Implements ValidadorDUKPT

    <DllImport("NShieldDUKPTValidator.dll",
        BestFitMapping:=True,
        PreserveSig:=True,
        EntryPoint:="INICIAR_PKCS11", SetLastError:=False,
        CharSet:=CharSet.Ansi, ExactSpelling:=False,
        CallingConvention:=CallingConvention.Cdecl)>
    Private Shared Function INICIAR_PKCS11() As Integer
    End Function

    <DllImport("NShieldDUKPTValidator.dll",
        BestFitMapping:=True,
        PreserveSig:=True,
        EntryPoint:="INICIAR_RECURSOS", SetLastError:=False,
        CharSet:=CharSet.Ansi, ExactSpelling:=False,
        CallingConvention:=CallingConvention.Cdecl)>
    Private Shared Function INICIAR_RECURSOS(ByVal token_name As StringBuilder, ByVal passphrase As StringBuilder, ByVal DBK_label As StringBuilder, ByVal PANEK_label As StringBuilder, ByVal out_hSession As StringBuilder, ByVal out_hBDK As StringBuilder, ByVal out_hPANEK As StringBuilder) As Integer
    End Function

    <DllImport("NShieldDUKPTValidator.dll",
            BestFitMapping:=True,
            PreserveSig:=True,
            EntryPoint:="FINALIZAR_RECURSOS", SetLastError:=False,
            CharSet:=CharSet.Ansi, ExactSpelling:=False,
            CallingConvention:=CallingConvention.Cdecl)>
    Private Shared Function FINALIZAR_RECURSOS(ByVal in_hSession As StringBuilder) As Integer
    End Function

    <DllImport("NShieldDUKPTValidator.dll",
            BestFitMapping:=True,
            PreserveSig:=True,
            EntryPoint:="VALIDAR_DUKPT", SetLastError:=False,
            CharSet:=CharSet.Ansi, ExactSpelling:=False,
            CallingConvention:=CallingConvention.Cdecl)>
    Private Shared Function VALIDAR_DUKPT(ByVal hSession As StringBuilder, ByVal hBDK As StringBuilder, ByVal hPANEK As StringBuilder, EPAN As String, PB1 As String, KSN1 As String, PB2 As String, KSN2 As String) As Integer
    End Function

    Private Shared instanciaPrivada As HSMValidadorDUKPT = Nothing
    Private Shared LockThis As New Object
    Private Shared INITIALIZED As Boolean = False
    Private hSession As StringBuilder
    Private hBDK As StringBuilder
    Private hPANEK As StringBuilder

    Public Shared ReadOnly Property Instancia() As HSMValidadorDUKPT
        Get
            ' Hasta que no termine la petición no recibe la otra (Caso multihilo)
            SyncLock LockThis
                ' Valida que no exista una instancia, si ya existe, no la instancia
                If (instanciaPrivada Is Nothing) Then
                    instanciaPrivada = New HSMValidadorDUKPT()
                End If
            End SyncLock

            Return instanciaPrivada
        End Get
    End Property

    Private Sub New()
        Try
            SyncLock LockThis
                Dim rv As Integer
                If Not INITIALIZED Then
                    ' Carga las funciones PKSC#11 directamente de la DLL cryptoki e inicializa las funciones PKCS#11
                    ' Solo se hace esta operacion una vez por proceso
                    rv = INICIAR_PKCS11()
                    If rv <> 0 Then
                        Throw New Exception("HSMValidadorDUKPT Constructor - error inciando PKCS#11: " & rv.ToString())
                    End If
                    INITIALIZED = True
                End If

                ' Inicializamos los handler de sesión y de llave
                Me.hSession = New StringBuilder("", 32)
                Me.hBDK = New StringBuilder("", 32)
                Me.hPANEK = New StringBuilder("", 32)

                ' Se inicializan los recursos para esta instancia de HSMTDES (sesion, login y busqueda de llave)
                rv = INICIAR_RECURSOS(
                    New StringBuilder(ConfigurationManager.AppSettings("TokenName")),
                    New StringBuilder(ConfigurationManager.AppSettings("TokenPassphrase")),
                    New StringBuilder(ConfigurationManager.AppSettings("NameBDKHSM")),
                    New StringBuilder(ConfigurationManager.AppSettings("NamePANEKHSM")),
                    hSession,
                    hBDK,
                    hPANEK)

                If rv <> 0 Then
                    Throw New Exception("HSMValidadorDUKPT Constructor - error inciando recursos: " & rv.ToString())
                End If
            End SyncLock
        Catch ex As Exception
            ' Se vuelve a lanzar excepción
            Throw ex
        End Try
    End Sub

    Public Function ValidarDUKPT(EPAN As String, PB1 As String, KSN1 As String, PB2 As String, KSN2 As String) As Integer Implements ValidadorDUKPT.ValidarDUKPT
        Dim rv As Integer
        SyncLock LockThis
            rv = VALIDAR_DUKPT(
                Me.hSession,
                Me.hBDK,
                Me.hPANEK,
                EPAN,
                PB1,
                KSN1,
                PB2,
                KSN2)
        End SyncLock

        Return rv
    End Function
End Class
