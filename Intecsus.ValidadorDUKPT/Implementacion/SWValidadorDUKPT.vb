﻿Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Configuration
Imports DukptNet
Imports System.Numerics

Public Class SWValidadorDUKPT
    Implements ValidadorDUKPT

    Public Sub New()
    End Sub

    Public Function ValidarDUKPT(EPAN As String, PB1 As String, KSN1 As String, PB2 As String, KSN2 As String) As Integer Implements ValidadorDUKPT.ValidarDUKPT
        Dim rv As Integer = 0
        Dim bdk As String = ConfigurationManager.AppSettings("BDK")
        Dim panek As String = ConfigurationManager.AppSettings("PANEK")
        Dim pin1 As String = ""
        Dim pin2 As String = ""
        Dim pan As String = ""

        Try
            pan = Dukpt.CalculateClearPAN(panek, EPAN)
            pin1 = Dukpt.CalculateClearPIN(bdk, KSN1, PB1, pan)
            pin2 = Dukpt.CalculateClearPIN(bdk, KSN2, PB2, pan)
        Catch ex As Exception
            ' Error criptográfico
            Return 2005
        End Try

        rv = hacerValidaciones(pin1, pin2)

        Return rv
    End Function

    Private Function hacerValidaciones(pin1 As String, pin2 As String) As Integer
        Dim rv As Integer = 0

        rv = validacion1(pin1, pin2)
        If (rv <> 0) Then
            Return rv
        End If

        rv = validacion2(pin1)
        If (rv <> 0) Then
            Return rv
        End If

        rv = validacion3(pin1)
        If (rv <> 0) Then
            Return rv
        End If

        rv = validacion4(pin1)
        If (rv <> 0) Then
            Return rv
        End If

        Return rv
    End Function

    Private Function validacion1(pin1 As String, pin2 As String) As Integer
        Dim rv As Integer = 0
        If pin1 <> pin2 Then
            rv = 2004
        End If
        Return rv
    End Function

    Private Function validacion2(pin As String) As Integer
        Dim rv As Integer = 0
        If pin.Length <> 4 Then
            rv = 2003
        End If
        Return rv
    End Function

    Private Function validacion3(pin As String) As Integer
        Dim rv As Integer = 0
        Dim digit = pin.Chars(0)
        Dim index As Integer
        For index = 1 To pin.Length - 1
            If pin.Chars(index) <> digit Then
                Exit For
            End If
        Next

        If index >= pin.Length Then
            rv = 2002
        End If

        Return rv
    End Function

    Private Function validacion4(pin As String) As Integer
        Dim fail As Integer = 2001
        Dim digit As Integer
        Dim index As Integer

        digit = Convert.ToInt32(pin(0))
        For index = 1 To pin.Length - 1
            digit = digit + 1
            If Convert.ToInt32(pin(index)) <> digit Then
                Exit For
            End If
        Next

        If index >= pin.Length Then
            Return fail
        End If

        digit = Convert.ToInt32(pin(0))
        For index = 1 To pin.Length - 1
            digit = digit - 1
            If Convert.ToInt32(pin(index)) <> digit Then
                Exit For
            End If
        Next

        If index >= pin.Length Then
            Return fail
        End If

        Return 0
    End Function
End Class
