﻿Public Interface ValidadorDUKPT
    ''' <summary>
    ''' Método encargado de hacer validaciones sobre los PINs digitados por el cliente
    ''' Validación 1: Longitud igual a cuatro (4)
    ''' Validación 2: PIN no consecutivo {0123, ... , 6789} - {9876, ... , 3210}
    ''' Validación 3: PIN no sea el mismo valor repetido: {0000, 1111, ..., 9999}
    ''' </summary>
    ''' <param name="EPAN">PAN ecriptado</param>
    ''' <param name="PB1">PINBLOCK1</param>
    ''' <param name="KSN1">KSN1</param>
    ''' <param name="PB2">PINBLOCK2</param>
    ''' <param name="KSN2">KSN2</param>
    ''' <returns></returns>
    Function ValidarDUKPT(EPAN As String, PB1 As String, KSN1 As String, PB2 As String, KSN2 As String) As Integer
End Interface
