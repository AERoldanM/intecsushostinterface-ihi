﻿Public Class HexUtils
    ''' <summary>
    ''' Convierte un hexa string en un arreglo de bytes
    ''' </summary>
    ''' <param name="hex">hexa string</param>
    ''' <returns>arreglo de bytes</returns>
    Public Shared Function HexToByteArray(ByVal hex As String) As Byte()
        Dim NumberChars As Integer = hex.Length
        Dim bytes As Byte() = New Byte(NumberChars / 2 - 1) {}
        For i As Integer = 0 To NumberChars - 1 Step 2
            bytes(i / 2) = Convert.ToByte(hex.Substring(i, 2), 16)
        Next
        Return bytes
    End Function

    ''' <summary>
    ''' Convierte un arreglo de bytes en un hexa string
    ''' </summary>
    ''' <param name="hex">arreglo de bytes</param>
    ''' <returns>hexa string</returns>
    Public Shared Function ByteArrayToHex(ByVal hex() As Byte) As String
        Return BitConverter.ToString(hex).Replace("-", "")
    End Function
End Class
