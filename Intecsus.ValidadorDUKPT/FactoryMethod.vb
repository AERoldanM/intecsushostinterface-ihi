﻿''' <summary>
''' Clase encargada de instanciar el validador dukpt
''' </summary>
Public Class FactoryMethod

    ''' <summary>
    ''' Metodo que se encarga de instaciar el validador dukpt
    ''' </summary>
    ''' <param name="tipo">Clase que debe instanciar</param>
    ''' <returns>Retorna implementación de la interface</returns>
    Public Function DevolverClase(tipo As String) As ValidadorDUKPT
        Try
            Select Case tipo
                Case "Hardware"
                    Return HSMValidadorDUKPT.Instancia()
                Case Else
                    Return New SWValidadorDUKPT()
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
