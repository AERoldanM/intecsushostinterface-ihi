﻿Imports Intecsus.Entidades
Imports Intecusus.OperarBD
Imports System.Configuration
Imports System.Xml
Imports Intecsus.Seguridad
Imports Intecsus.Utilidades

Public Class GeneradorParametros
    Private operacionesBD As OperacionesBase = New OperacionesBase()
    Private codigoBarras As BarcodeConverter128 = New BarcodeConverter128()
#Region "Valores cliente"

    ''' <summary>
    ''' Metodo encargado de cargar los datos de inicializacion de PCNAME, CardName y XML
    ''' </summary>
    ''' <param name="peticion">Peticion inicial enviada por el cliente</param>
    ''' <param name="idTransaccion">Id de la transaccion</param>
    ''' <returns>Retorna respuestaWS con su respectivos datos de  PCNAME, CardName y XML</returns>
    ''' <remarks>AERM 29/07/2015</remarks>
    Public Function RetornarParametrosCliente(peticion As Peticion, varInicio As ConfInicio, idTransaccion As Integer, m_respuestaWS As RespuestaWS) As RespuestaWS
        Try
            Dim m_List As List(Of String) = operacionesBD.ObtenerCardFormatName(peticion.cliente, peticion.tipoProducto, ConfigurationManager.AppSettings("BD"))
            If (m_List.Count >= 2) Then
                m_respuestaWS.cardWizardName = m_List(0)
                m_respuestaWS.xmlPeticion = m_List(1)
            End If
            Dim m_BuscarImpresora As String = operacionesBD.ObtenerValoresConfigurador("BuscarImpresora" + peticion.cliente, ConfigurationManager.AppSettings("BD"))
            ''Obtenemos el PCNAME deacuerdo a configuracion base de datos tabla Impresoras si lo tiene habilitado
            If (Convert.ToBoolean(Convert.ToInt32(m_BuscarImpresora)) = True) Then
                m_respuestaWS.pCName = operacionesBD.ObtenerPCNAME(peticion.printName, ConfigurationManager.AppSettings("BD"))
            Else
                m_respuestaWS.pCName = peticion.printName
            End If

            If (String.IsNullOrEmpty(m_respuestaWS.cardWizardName) Or String.IsNullOrEmpty(m_respuestaWS.pCName)) Then
                Dim m_RespuestaNotificar As RespuestaDeviceStatus = New RespuestaDeviceStatus()
                m_RespuestaNotificar.Transaccion = idTransaccion
                m_RespuestaNotificar.hostIdentifier = idTransaccion
                m_RespuestaNotificar.TransaccionCliente = peticion.idTransaccion
                Dim m_Mensaje As List(Of String) = Me.ObtenerMensaje("205", idTransaccion)
                m_RespuestaNotificar.CodigoCliente = m_Mensaje(1)
                m_RespuestaNotificar.DataInteger = m_Mensaje(1) ''SANTANDER
                m_RespuestaNotificar.Cliente = peticion.cliente
                m_RespuestaNotificar.Mensaje = m_Mensaje(0)
                m_RespuestaNotificar.EstadoF = "1"
                varInicio.operarCola.GuardarObjetoNotificacion(m_RespuestaNotificar, varInicio.colaNotificacion)
                Throw New Exception(m_Mensaje(0) + " " + idTransaccion.ToString() + "-")
            End If

            Return m_respuestaWS
        Catch ex As Exception
            Throw New Exception("-RetornarParametrosCliente " + ex.Message)
        End Try

        Return Nothing
    End Function

#End Region

#Region "CrearXML"

    ''' <summary>
    ''' Metodo encagado de obtener los datos para el realce
    ''' </summary>
    ''' <param name="peticion">datos a procesar</param>
    ''' <param name="lineaArchivo">Liena archivo a tomar los datos</param>
    ''' <returns>Retorna lista de parametros de la peticion</returns>
    ''' <remarks>AERM 23/04/2015</remarks>
    Public Function ObtenerParametros(peticion As RespuestaWS, lineaArchivo As String) As List(Of ParametrosXML)
        Dim m_Parametros = New List(Of ParametrosXML)
        Try
            Dim m_valida As Validadores = New Validadores()
            For Each m_xnode As XmlNode In Me.ObtenerXMList("/Parametros/Parametro", Nothing, True, peticion.xmlPeticion)
                Dim m_Valores As List(Of Valor) = New List(Of Valor)
                For Each m_xnodeValor As XmlNode In Me.ObtenerXMList("Valor", m_xnode)
                    Dim m_Inicio As Integer = CInt(m_xnodeValor.Attributes("inicio").Value)
                    Dim m_Tamano As Integer = CInt(m_xnodeValor.Attributes("tamano").Value)
                    Dim m_Valor As String = lineaArchivo.Substring(m_Inicio, m_Tamano).Trim()
                    m_Valores.Add(Me.ObtenerValorDato(m_Valor, m_xnodeValor, m_valida))
                Next

                m_Parametros.Add(Me.ProcesoGeneralXParametro(m_Valores, m_xnode))
            Next
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(14, 0, 2, "GeneradorParametros- ObtenerParametros FINAL línea " + lineaArchivo + " error: " + ex.Message,
                                                           ConfigurationManager.AppSettings("BD"))
            If (ex.Message.Length > 4) Then
                Throw New Exception("7002")
            End If

            Throw New Exception(ex.Message)
        End Try

        Return m_Parametros
    End Function

    ''' <summary>
    ''' Metodo encagado de obtener los datos para el realce
    ''' </summary>
    ''' <param name="peticion">datos a procesar</param>
    ''' <param name="m_xmlData">Archivo XML </param>
    ''' <returns>Retorna lista de parametros de la peticion</returns>
    ''' <remarks>AERM 20/07/2015</remarks>
    Public Function ObtenerParametros(peticion As RespuestaWS, m_xmlData As XmlDocument) As List(Of ParametrosXML)
        Dim m_Parametros = New List(Of ParametrosXML)
        Try
            Dim m_valida As Validadores = New Validadores()
            For Each m_xnode As XmlNode In Me.ObtenerXMList("/Parametros/Parametro", Nothing, True, peticion.xmlPeticion)
                Dim m_Valores As List(Of Valor) = New List(Of Valor)
                For Each m_xnodeValor As XmlNode In Me.ObtenerXMList("Valor", m_xnode)
                    Dim m_EtiquetaXML As String
                    Dim m_valor As String
                    If (IsNothing(m_xnodeValor.Attributes("ValoresS"))) Then
                        m_valor = String.Empty
                    Else
                        m_EtiquetaXML = m_xnodeValor.Attributes("ValoresS").Value
                        m_valor = m_xmlData.SelectSingleNode(m_EtiquetaXML).InnerText
                    End If

                    m_Valores.Add(Me.ObtenerValorDato(m_valor, m_xnodeValor, m_valida))
                Next

                m_Parametros.Add(Me.ProcesoGeneralXParametro(m_Valores, m_xnode))
            Next
        Catch ex As Exception
            Throw New Exception(ex.Message + "Para el servicio " + m_xmlData.ToString())
        End Try

        Return m_Parametros
    End Function

    ''' <summary>
    ''' Funcion encargada de retornar una lista de dentro de un XML
    ''' </summary>
    ''' <param name="m_xnode">XML a procesar</param>
    ''' <param name="nodo">Nodo a operar ejem /Parametros/Parametro</param>
    ''' <param name="obtenerRespuesta">Por defecto viene en false de ser true carga el XML inicial en base a el objeto peticion</param>
    ''' <param name="xmlPeticion">Objeto donde se optiene el XML dado el caso q el anterior parametro este en true</param>
    ''' <returns>Retorna lista de nodos</returns>
    ''' <remarks>AERM 17/09/2015</remarks>
    Public Function ObtenerXMList(nodo As String, Optional m_xnode As XmlNode = Nothing, Optional obtenerRespuesta As Boolean = False, Optional xmlPeticion As String = "") As XmlNodeList
        If (obtenerRespuesta) Then
            Dim m_xml = New XmlDocument()
            m_xml.LoadXml(xmlPeticion)
            Return m_xml.SelectNodes(nodo)
        Else
            Return m_xnode.SelectNodes(nodo)
        End If
    End Function

    ''' <summary>
    ''' Metodo encargado de retornar un mensaje por COdigo NO traduccion
    ''' </summary>
    ''' <param name="codigoMensaje">Codigo de mensaje a buscar</param>
    ''' <param name="idTransaccion">Identificador de la transaccion</param>
    ''' <returns>Retorna mensaje respectivo</returns>
    ''' <remarks>AERM 29/06/2016</remarks>
    Public Function ObtenerMensaje(codigoMensaje As String, idTransaccion As Int32) As List(Of String)
        Dim m_Mensaje As List(Of String) = New List(Of String)()
        Try
            operacionesBD.GuardarLogXNombreConectionString(17, idTransaccion, 1, "GeneradorParametros- ObtenerMensaje  codigo" + codigoMensaje,
                                                               ConfigurationManager.AppSettings("BD"))

            Dim m_RespuestaString As List(Of String) = operacionesBD.ObtenerMensaje(codigoMensaje, ConfigurationManager.AppSettings("BD"))
            If m_RespuestaString.Count >= 2 Then
                m_Mensaje.Add(m_RespuestaString(0))
                m_Mensaje.Add(m_RespuestaString(1))
            Else
                m_Mensaje.Add("Error genérico")
                m_Mensaje.Add("300")
            End If
        Catch ex As Exception
            operacionesBD.GuardarLogXNombreConectionString(17, idTransaccion, 2, "GeneradorParametros- ObtenerMensaje  " + ex.Message,
                                                              ConfigurationManager.AppSettings("BD"))
            m_Mensaje.Add("Error genérico")
            m_Mensaje.Add("300")
        End Try

        Return m_Mensaje
    End Function
    ''' <summary>
    ''' Metodo encargado de generar el parametro XML
    ''' </summary>
    ''' <param name="m_Valor">valor inicial a enviar</param>
    ''' <param name="m_xnode">Nodo con los parametros de configuracion</param>
    ''' <returns>Parametro a enviar</returns>
    ''' <remarks>AERM 30/07/2015</remarks>
    Private Function ProcesoGeneralXParametro(m_Valor As List(Of Valor), m_xnode As XmlNode) As ParametrosXML
        Dim m_Parametro As ParametrosXML = New ParametrosXML()
        ''Obtengo valores XML
        Dim m_Nombre As String = m_xnode.Attributes("nombre").Value
        Dim m_Codifica As String = m_xnode.Attributes("codifica").Value
        Dim m_tipo As String = m_xnode.Attributes("tipo").Value

        m_Parametro = New ParametrosXML()
        m_Parametro.Nombre = "DATAITEM"
        m_Parametro.NameCard = m_Nombre
        m_Parametro.Typo = m_tipo
        m_Parametro.Valor = m_Valor
        m_Parametro.Codificar = If(m_Codifica.ToUpper = "SI", True, False)
        m_Parametro.Derecha = If(m_xnode.Attributes("derecha") IsNot Nothing, CInt(m_xnode.Attributes("derecha").Value), 0)
        m_Parametro.Izquierda = If(m_xnode.Attributes("izquierda") IsNot Nothing, CInt(m_xnode.Attributes("izquierda").Value), 0)
        Return m_Parametro
    End Function

    ''' <summary>
    ''' Metodo encargado de obtener si se debe valdiar el dato del cliente
    ''' </summary>
    ''' <param name="m_xnode">Parametro que contiene los nodos del XML</param>
    ''' <returns>Retorna formato de validacion a realizar</returns>
    ''' <remarks>AERM 30/07/2015</remarks>
    Private Function ObtenerValidador(m_xnode As XmlNode) As String
        Try
            If m_xnode.Attributes("valida") IsNot Nothing Then
                Return m_xnode.Attributes("valida").Value
            End If
        Catch ex As Exception
            Throw New Exception(ex.Message + " ObtenerValidador")
        End Try
        Return String.Empty
    End Function

    ''' <summary>
    ''' Por optimizar
    ''' Metodo encargado de obtener un valor en cualquiera de sus variantes (En el xml de configuracion)
    ''' </summary>
    ''' <param name="m_Valor"> Valor inicial</param>
    ''' <param name="m_xnode">Parametro que contiene los nodos del XML</param>
    ''' <returns></returns>
    ''' <remarks>AERM 30/07/2015</remarks>
    Private Function ObtenerValor(m_Valor As String, m_xnode As XmlNode) As String
        Try
            If m_xnode.Attributes("sustituir") IsNot Nothing Then
                Dim m_Sustituir As String = m_xnode.Attributes("sustituir").Value
                Return m_Sustituir
            End If

            If m_xnode.Attributes("agregar") IsNot Nothing Then
                Dim m_Agregar As String = m_xnode.Attributes("agregar").Value
                m_Valor = m_Agregar + m_Valor
            End If

            If m_xnode.Attributes("hexa") IsNot Nothing Then
                Dim hexNumbers As System.Text.StringBuilder = New System.Text.StringBuilder
                Dim byteArray() As Byte
                byteArray = System.Text.ASCIIEncoding.ASCII.GetBytes(m_Valor)
                For i As Integer = 0 To byteArray.Length - 1
                    hexNumbers.Append(byteArray(i).ToString("x"))
                Next

                Return hexNumbers.ToString()
            End If

            If m_xnode.Attributes("fecha") IsNot Nothing Then
                Dim fecha As DateTime = DateTime.Now()
                Return fecha.ToString("MM/yy")
            End If

            If m_xnode.Attributes("replace") IsNot Nothing Then
                Dim m_Replace As String() = m_xnode.Attributes("replace").Value.Split(",")
                m_Valor = m_Valor.Replace(m_Replace(0), m_Replace(1))
                Return m_Valor
            End If

            If m_xnode.Attributes("codigoBarras") IsNot Nothing Then
                m_Valor = codigoBarras.StringToBarcode(m_Valor)
                Return m_Valor
            End If

            If m_xnode.Attributes("trim") IsNot Nothing Then
                Return m_Valor.Trim()
            End If
        Catch ex As Exception
            Throw ex
        End Try

        Return m_Valor
    End Function

    ''' <summary>
    ''' Metodo encargado en obtener el valor pro WS
    ''' </summary>
    ''' <param name="m_valor">COntiene los atributos que toma el valor (puede ser compuesto)</param>
    ''' <param name="m_valida">Clase q se usa para validar los valores a enviar</param>
    ''' <returns>El valor completo</returns>
    ''' <remarks>AERM 30/07/2015</remarks>
    Private Function ObtenerValorDato(m_valor As String, m_xnode As XmlNode, m_valida As Validadores) As Valor
        Dim m_ValorF As Valor = New Valor()
        Dim m_Validador As String = Me.ObtenerValidador(m_xnode)
        If m_valida.ValidadorExpresionesRegulares(m_valor, m_Validador) = False Then
            Throw New Exception("7009")
        End If

        m_ValorF.ValorS = Me.ObtenerValor(m_valor, m_xnode)
        m_ValorF.FechaAsNet = If(IsNothing(m_xnode.Attributes("FechaAsNet")), False, True)
        m_ValorF.MetodoPCI = If(IsNothing(m_xnode.Attributes("ISPCI")), False, True)
        m_ValorF.Tamamio = If(IsNothing(m_xnode.Attributes("tamano")), 0, Convert.ToInt32(m_xnode.Attributes("tamano").Value))
        m_ValorF.ValoresDerecha = If(IsNothing(m_xnode.Attributes("ValoresDerecha")), 0, Convert.ToInt32(m_xnode.Attributes("ValoresDerecha").Value))
        Return m_ValorF
    End Function


#End Region
End Class
