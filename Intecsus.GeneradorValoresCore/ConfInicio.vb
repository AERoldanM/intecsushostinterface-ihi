﻿Imports Intecsus.MSMQ
Imports Intecusus.OperarBD
Imports Intecsus.OperarXML
Imports System.Configuration
Public Class ConfInicio

#Region "Variables Iniciales"

    Public tiempoProceso As Integer
    Public colaRealce As String
    Public colaClientes As String
    Public colaNotificacion As String
    Public operarCola As ICola = New InstanciaCola().IntanciarCola(ConfigurationManager.AppSettings("ColaInstancia"))
    Public operacionesBD As OperacionesBase = New OperacionesBase()
    Public operarXML As GeneradorXML = New GeneradorXML()
    Public ubicacionArchivo As String = String.Empty
    Public nombreArchivo As String = String.Empty
    Public extensionArchivo As String = String.Empty
    Public cardWizardNameXArchivo As String = String.Empty
    Public archivoProcesado As String = String.Empty
    Public procesaArchivo As Boolean

#End Region

#Region "Metodo Instancia"

    ''' <summary>
    ''' Metodo encargado de instaciar la clase una vez se ha instanciado el objeto
    ''' </summary>
    ''' <remarks>AERM 28/07/2015</remarks>
    Public Sub New()
        Try
            ''Variable tiempos
            tiempoProceso = Convert.ToInt32(operacionesBD.ObtenerValoresConfigurador("EjecucionProcesoMiliSegundosI", ConfigurationManager.AppSettings("BD")))
            ''Colas
            colaRealce = operacionesBD.ObtenerValoresConfigurador("ColaRealce", ConfigurationManager.AppSettings("BD"))
            colaClientes = operacionesBD.ObtenerValoresConfigurador("ColaCliente", ConfigurationManager.AppSettings("BD"))
            colaNotificacion = operacionesBD.ObtenerValoresConfigurador("ColaNotificacion", ConfigurationManager.AppSettings("BD"))
            ubicacionArchivo = operacionesBD.ObtenerValoresConfigurador("RutaArchivo", ConfigurationManager.AppSettings("BD"))
            extensionArchivo = operacionesBD.ObtenerValoresConfigurador("FormatoArchivo", ConfigurationManager.AppSettings("BD"))
            cardWizardNameXArchivo = operacionesBD.ObtenerValoresConfigurador("CardWizardNameXArchivo", ConfigurationManager.AppSettings("BD"))
            archivoProcesado = operacionesBD.ObtenerValoresConfigurador("RutaArchivoProcesado", ConfigurationManager.AppSettings("BD"))
            ''Procesa Archivo
            Dim m_procesaArchivo As String = operacionesBD.ObtenerValoresConfigurador("ProcesarArchivo", ConfigurationManager.AppSettings("BD"))
            If m_procesaArchivo.ToUpper = "ACTIVO" Then
                procesaArchivo = True
            Else
                procesaArchivo = False
            End If
        Catch ex As Exception
            Throw New Exception("ConfInicio.vb - InicilizarVariables " + ex.Message)
        End Try
    End Sub

#End Region
End Class
