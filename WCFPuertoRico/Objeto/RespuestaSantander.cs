﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WCFPuertoRico.Objeto
{
   
    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class Respuesta
    {
        string codigoRespuesta = "WSR001";
        string mensaje = "SE RECIBIO LA SOLICITUD DE ESTAMPADO ";

        [DataMember]
        public string CodRespuesta
        {
            get { return codigoRespuesta; }
            set { codigoRespuesta = value; }
        }

        [DataMember]
        public string Mensaje
        {
            get { return mensaje; }
            set { mensaje = value; }
        }
    }
}
