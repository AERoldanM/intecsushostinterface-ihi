﻿using Intecsus.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCFPuertoRico.Objeto;
using Intecsus.NegocioPin;

namespace WCFPuertoRico.Servicio
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "OperacionesPIN" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select OperacionesPIN.svc or OperacionesPIN.svc.cs at the Solution Explorer and start debugging.
     [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class OperacionesPIN : IOperacionesPIN
    {

         public RespuestaPIN Ingresar(string id, string contrasena)
        {
            IBLOperacionesPin m_Negocio = new BLOperacionesPin();
            return m_Negocio.Ingresar(id, contrasena);
        }

        public Resultado CrearUsuario(string userIDAdmin, UsuarioPIN user, string seguridad)
        {
            IBLOperacionesPin m_Negocio = new BLOperacionesPin();
            return m_Negocio.CrearUsuario(userIDAdmin, user, seguridad);
        }

        public Resultado EliminarUsuario(string userIDAdmin, string userIDDelete, string seguridad)
        {
            IBLOperacionesPin m_Negocio = new BLOperacionesPin();
            return m_Negocio.EliminarUsuario(userIDAdmin, userIDDelete, seguridad);
        }

        public ResultadoUsuario ListarUsuario(string userIDAdmin, string seguridad)
        {
            IBLOperacionesPin m_Negocio = new BLOperacionesPin();
            return m_Negocio.ListarUsuario(userIDAdmin, seguridad);
        }

        public Resultado EditarUsuario(string userIDAdmin, UsuarioPIN userEditar, string seguridad)
        {
            IBLOperacionesPin m_Negocio = new BLOperacionesPin();
            return m_Negocio.EditarUsuario(userIDAdmin, userEditar, seguridad);
        }

        public Resultado CambiarContrasena(string userID, string contrasenaAntigua, string contrasena, string contrasenaConfirmacion, string userIDAdmin, string seguridad)
        {
            IBLOperacionesPin m_Negocio = new BLOperacionesPin();
            return m_Negocio.CambiarContrena(userID, contrasenaAntigua,   contrasena,  contrasenaConfirmacion,  userIDAdmin, seguridad);
        }

        public Resultado AsignacionPin(string CODENT, string CANAL,  string userIDAdmin, string USUARIO, string NOMUSR, PIN pin1, PIN pin2,  string EPAN,  string PANSHA, string seguridad)
        {
            IBLOperacionesPin m_Negocio = new BLOperacionesPin();
            return m_Negocio.AsignacionPin(CODENT, CANAL, userIDAdmin, USUARIO, NOMUSR, pin1, pin2, EPAN, PANSHA, "SANTANDER", seguridad);
        }


        public RespuestaConfig Configuracion(string userID, string seguridad)
        {
            IBLOperacionesPin m_Negocio = new BLOperacionesPin();
            return m_Negocio.Configuracion(userID, seguridad);
        }

        public Resultado AsignarContrasena(string userIDAdmin, string contrasenaNueva, string confirmacionContrasena, string userID, string seguridad)
        {
            IBLOperacionesPin m_Negocio = new BLOperacionesPin();
            return m_Negocio.AsignarContrasena(userIDAdmin, contrasenaNueva, confirmacionContrasena, userID, seguridad);
        }

        public Resultado GuardarEvento(Int32 evento, string mensaje) 
        {
            IBLOperacionesPin m_Negocio = new BLOperacionesPin();
            return m_Negocio.GuardarLOG(evento, mensaje);
        }
    }
}
