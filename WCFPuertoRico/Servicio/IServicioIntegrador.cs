﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCFPuertoRico.Objeto;

namespace WCFPuertoRico.Servicio
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISolicitudTarjetaSantander" in both code and config file together.
    [ServiceContract]
    public interface IServicioIntegrador
    {
        [OperationContract]
        WCFPuertoRico.Objeto.Respuesta GetData(string codEnt, string claveOpe, string fiest01);
    }
}
