﻿using Intecsus.Entidades;
using Intecsus.MSMQ;
using Intecusus.OperarBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCFPuertoRico.Objeto;

namespace WCFPuertoRico.Servicio
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SolicitudTarjetaSantander" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SolicitudTarjetaSantander.svc or SolicitudTarjetaSantander.svc.cs at the Solution Explorer and start debugging.
    public class ServicioIntegrador : IServicioIntegrador
    {
        public WCFPuertoRico.Objeto.Respuesta GetData(string codEnt, string claveOpe, string fiest01)
        {
            WCFPuertoRico.Objeto.Respuesta respuesta = new WCFPuertoRico.Objeto.Respuesta();
            try
            {
                Peticion peticion = new Peticion();
                OperacionesBase operacionesBD = new OperacionesBase();
                peticion.idTransaccion = claveOpe;
                peticion.cliente = "SANTANDER";
                peticion.codigoNovedad = "03";
                peticion.oficinaRadicacion = codEnt; //banco
                peticion.nombreLargo = fiest01; //OBJETO REALCE

                operacionesBD.GuardarLogXNombreConectionString(19, 0, 1, "Recibe Peticion SANTANDER : - " + codEnt + " idcliente " + claveOpe + " FIEST " + fiest01,
                    ConfigurationManager.AppSettings["BD"]);

                respuesta = this.AdjuntarAColaT(peticion);
                operacionesBD.GuardarLogXNombreConectionString(19, 0, 1, "respuesta Peticion SANTANDER : - " + respuesta.CodRespuesta + "  - " + respuesta.Mensaje,
                   ConfigurationManager.AppSettings["BD"]);
            }
            catch (Exception ex)
            {
                respuesta.CodRespuesta  = "001";
                respuesta.Mensaje = ex.Message;
            }

            return respuesta;
        }

        /// <summary>
        /// Metood encargado de adjuntar una peticion a la cola
        /// </summary>
        /// <param name="peticion">Objeto de peticion que se agrega a la cola</param>
        /// <remarks>AERM 19/03/2015</remarks>
        public WCFPuertoRico.Objeto.Respuesta AdjuntarAColaT(Peticion peticion)
        {
            WCFPuertoRico.Objeto.Respuesta respuesta = new WCFPuertoRico.Objeto.Respuesta();
            OperacionesBase operacionesBD = new OperacionesBase();
            try
            {
                ICola m_OperarCOla = new InstanciaCola().IntanciarCola(ConfigurationManager.AppSettings["ColaInstancia"]);
                m_OperarCOla.GuardarObjetoPeticionCliente(peticion, 
                    operacionesBD.ObtenerValoresConfigurador("ColaCliente", ConfigurationManager.AppSettings["BD"]));
            }
            catch (Exception ex)
            {
                operacionesBD.GuardarLogXNombreConectionString(19, 0, 2, "SolicitudTarjetaSantander- AdjuntarAColaT : - " +
                    peticion.idTransaccion + "  " + ex.Message, ConfigurationManager.AppSettings["BD"]);
                var m_RespuestaString = operacionesBD.ObtenerMensaje("7005", ConfigurationManager.AppSettings["BD"]);
                if (m_RespuestaString.Count >= 2)
                {
                    respuesta.CodRespuesta = m_RespuestaString[0];
                    respuesta.Mensaje = m_RespuestaString[1];
                }
                else
                {
                    respuesta.CodRespuesta = "7005";
                    respuesta.Mensaje = "Error al guardar petición en la cola";
                }
            }

            return respuesta;
        }
    }
}
