﻿using Intecsus.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using WCFPuertoRico.Objeto;

namespace WCFPuertoRico.Servicio
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IOperacionesPIN" in both code and config file together.
    [ServiceContract]
    public interface IOperacionesPIN
    {
        [OperationContract]
        RespuestaPIN Ingresar(string id, string contrasena);

        [OperationContract]
        Resultado CrearUsuario(string userIDAdmin, UsuarioPIN user, string seguridad);

        [OperationContract]
        Resultado EliminarUsuario(string userIDAdmin, string userIDDelete, string seguridad);

        [OperationContract]
        ResultadoUsuario ListarUsuario(string userIDAdmin, string seguridad);

        [OperationContract]
        Resultado EditarUsuario(string userIDAdmin, UsuarioPIN userEditar, string seguridad);

        [OperationContract]
        Resultado CambiarContrasena(string userID, string contrasenaAntigua, string contrasena, string contrasenaConfirmacion, string userIDAdmin, string seguridad);

        [OperationContract]
        Resultado AsignacionPin(string CODENT, string CANAL, string userIDAdmin, string USUARIO, string NOMUSR, PIN pin1, PIN pin2, string EPAN,  string PANSHA, string seguridad);

         [OperationContract]
        RespuestaConfig Configuracion(string userID, string seguridad);

        [OperationContract]
         Resultado AsignarContrasena(string userIDAdmin, string contrasenaNueva, string confirmacionContrasena, string userID, string seguridad);

        [OperationContract]
        Resultado GuardarEvento(Int32 evento, string mensaje);


    }
}
